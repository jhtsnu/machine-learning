clear

load('MNIST_Dataset.mat');

train_x = trainingData.Images(:,1:1000)'; 
train_c = trainingData.Labels(1:1000,:); 
test_x = testingData.Images(:,1:100)'; 
test_c = testingData.Labels(1:100,:); 

p_test_x = zeros(1, 10);

y = zeros(100, 10);
for i = [1:10]
    w1_m = inv(train_x'*train_x  + 0.1*eye(size(train_x,2)))*train_x'*(train_c==i); 
    y(:,i) = test_x*w1_m;
end

confusion_matrix = zeros(10,10);
for i = [1:100]
    [value index] = max(y(i,:));
    
    if test_c(i) == 0
        real_c = 10;
    else
        real_c = test_c(i);
    end
    
    confusion_matrix(index, real_c) = confusion_matrix(index, real_c) + 1;
end

confusion_matrix
accuracy = sum(diag(confusion_matrix))/size(test_x,1)