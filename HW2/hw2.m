clear
tic

load('MNIST_Dataset.mat');

train_x = trainingData.Images(:,1:1000)'; 
train_c = trainingData.Labels(1:1000,:); 
test_x = testingData.Images(:,1:100)'; 
test_c = testingData.Labels(1:100,:); 

p_class = zeros(1,10);
for j = [1:10]
    train_subx{j} = train_x(train_c==mod(j,10),:);
    mu{j} = mean(train_subx{j}(:,1:end));
    sigma{j} = cov(train_subx{j}(:,1:end))+0.1*eye(size(train_x,2));
    p_class(j) = size(train_subx{j},1)/size(train_x,1);
end 

p_test_x = zeros(1, 10);
confusion_matrix = zeros(10,10);

for i = [1:100]
    for j = [1:10]
        p_test_x(j) = log(p_class(j)) + logmvnpdf(test_x(i,1:end),mu{j},sigma{j});
    end
    
    [value index] = max(p_test_x);
    
    if test_c(i) == 0
        real_c = 10;
    else
        real_c = test_c(i);
    end
    
    confusion_matrix(index, real_c) = confusion_matrix(index, real_c) + 1;
end

confusion_matrix

accuracy = sum(diag(confusion_matrix))/size(test_x,1) 
toc 
