disp 'Load data and set constant variable';
tic;

load('../mat/cifar-10.mat');

% Independent Variables

Const.NumOfTrainData = 500; %size(data_train,1);
Const.NumOfTestData = 100; %size(data_test,1);

Const.NumOfCentroids = 1600;
Const.KindOfLabel = 0:9;

Const.IsWhitening = false;

Const.SizeOfCifar = 32;
Const.NumOfChannel = 3;
Const.PatchPerData = 32;

Const.SizeOfPatch = 3;

Const.PCA.NumOfEigenVector = 1000;

% SVM
Const.SVM.Method = 'SMO';
Const.SVM.Torlerance = 0.0001;
Const.SVM.TradeOffWeight = Inf;
Const.SVM.SMO.BoxConstraint = Inf;
Const.SVM.SMO.TolKKT = 0.01;
Const.SVM.SMO.MaxIter = 1500000;
Const.SVM.SMO.KKTViolationLevel = 0.1;
Const.SVM.SMO.KernelCacheLimit = 50000;

Const.SVM.QP.MaxIter = 1000;
Const.SVM.QP.Algorithm = 'active-set';
Const.SVM.kernel = @linear_kernel;

% Dependent Variables

data_train = data_train(1:Const.NumOfTrainData,:);
data_test = data_test(1:Const.NumOfTestData,:);
labels_train = labels_train(1:Const.NumOfTrainData,:);
labels_test = labels_test(1:Const.NumOfTestData,:);

Const.SizeOfCifarSquare = Const.SizeOfCifar * Const.SizeOfCifar;
Const.SizeOfPatchSquare = Const.SizeOfPatch * Const.SizeOfPatch;
Const.NumOfPatches = Const.PatchPerData * Const.NumOfTrainData;

Const.OffsetOfPatch = Const.SizeOfPatch - 1;
Const.LengthOfPatch = Const.SizeOfCifar - Const.OffsetOfPatch;

Const.LengthOfPatchSquare = Const.LengthOfPatch * Const.LengthOfPatch;
Const.SizeOfPatchWithChannel = Const.SizeOfPatchSquare * Const.NumOfChannel;
toc;

