function [Feature] = extract_features(InputImg, Centroid, Const)
  disp 'Extract features';

  NumOfInputData = size(InputImg, 1);
  Feature = zeros(NumOfInputData, 4 * Const.NumOfCentroids);
  
  tCentroid = Centroid';
  tCentroidSquareSum = sum(tCentroid.^2, 1);
  halfLength = round(Const.LengthOfPatch / 2);

  tic;
  for i = 1 : NumOfInputData
    if (mod(i,1000) == 0)
      fprintf('Extracting features: %d / %d\n', i, NumOfInputData);
      toc; if i == NumOfInputData tic; end;
    end
    Patches = im2col(InputImg(i,:), Const);
    Patches = preprocessing(Patches, Const);
    act = activate(Patches, tCentroid, tCentroidSquareSum);
    reshaped_act = reshape(act, ...
                           Const.LengthOfPatch, ...
                           Const.LengthOfPatch, ...
                           Const.NumOfCentroids);
    Feature(i,:) = quadsum(reshaped_act, halfLength);
  end

function [Y] = quadsum(X, halfLength)
  % pool over quadrants
  q1 = sum(sum(X(1:halfLength, 1:halfLength, :), 1),2);
  q2 = sum(sum(X(halfLength + 1:end, 1:halfLength, :), 1),2);
  q3 = sum(sum(X(1:halfLength, halfLength + 1:end, :), 1),2);
  q4 = sum(sum(X(halfLength + 1:end, halfLength + 1:end, :), 1),2);
  % concatenate into feature vector
  Y = [q1(:);q2(:);q3(:);q4(:)]';

function [Y] = activate(InputImg, tCentroid, tCentroidSquareSum)
  InputImgSquareSum = sum(InputImg.^2, 2);
  % Calculate distances
  z = sqrt(bsxfun(@plus, bsxfun(@minus, InputImgSquareSum, 2 * InputImg * tCentroid), tCentroidSquareSum));
  mu = mean(z, 2);
  Y = max(bsxfun(@minus, mu, z), 0);

function [Patches] = im2col(Img, Const)
  ParsedImg = reshape(Img, Const.SizeOfCifar, Const.SizeOfCifar, Const.NumOfChannel);
  patchRows = zeros(Const.LengthOfPatch, Const.SizeOfPatch, Const.SizeOfCifar, Const.NumOfChannel);
  Patches = zeros(Const.LengthOfPatchSquare, Const.SizeOfPatchSquare * Const.NumOfChannel);
  for idx = 1 : Const.LengthOfPatch
    patchRows(idx,:,:,:) = ParsedImg(idx:idx + Const.OffsetOfPatch,:, :);
  end
  
  for idx = 1 : Const.LengthOfPatch
    Patches((idx - 1) * Const.LengthOfPatch + 1:idx * Const.LengthOfPatch,:) = reshape((patchRows(:, :, idx:idx + Const.OffsetOfPatch, :)), Const.LengthOfPatch, Const.SizeOfPatchWithChannel);
  end
