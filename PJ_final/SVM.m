function [ SV, AlphaHat, bias, SVIndex ] = SVM( Feature, Target, SVMConst, method )
  %SVM Support Vector Machine
  % Feature : Basic two dimensional matrix
  % Target : A column vector of labels. +1 or -1 for each data point
  % method : method to solve Lagrangian optimization
  %

  if nargin < 4
    method = 'QP';
  end
  tic;
  if strcmpi(method, 'QP') == 1
    disp 'SVM using QP';
    [ SV, AlphaHat, bias, SVIndex ] = SolveSVMUsingQP(Feature, Target, SVMConst);
  elseif strcmpi(method, 'SMO') == 1
    disp 'SVM using SMO';
    [ SV, AlphaHat, bias, SVIndex ] = SolveSVMUsingSMO(Feature, Target, SVMConst);
  end
  toc;
end

function [ SV, AlphaHat, bias, SVIndex ] = SolveSVMUsingSMO(Feature, Target, SVMConst)
  NumOfFeatureData = size(Feature, 1);
  
  smo_opts = svmsmoset('MaxIter',SVMConst.SMO.MaxIter, ...
                       'TolKKT', SVMConst.SMO.TolKKT, ...
                       'KKTViolationLevel', SVMConst.SMO.KKTViolationLevel, ...
                       'KernelCacheLimit', SVMConst.SMO.KernelCacheLimit);

  boxconstraint = ones(NumOfFeatureData, 1);
  boxconstraint(Target==1) = 0.5 * SVMConst.SMO.BoxConstraint * NumOfFeatureData / length(find(Target==1));
  boxconstraint(Target==-1) = 0.5 * SVMConst.SMO.BoxConstraint * NumOfFeatureData / length(find(Target==-1));
  boxconstraint = min(boxconstraint,repmat(1/sqrt(eps(class(boxconstraint))),...
                                           size(boxconstraint)));
  [Alpha, bias] = seqminopt(Feature, Target, ...
                            boxconstraint, SVMConst.kernel, smo_opts);
  SVIndex = find(Alpha > sqrt(eps));
  SV = Feature(SVIndex,:);
  AlphaHat = Target(SVIndex).* Alpha(SVIndex);
end

function [ SV, AlphaHat, bias, SVIndex ] = SolveSVMUsingQP(Feature, Target, SVMConst)
  NumOfFeatureData = size(Feature, 1);
  H = feval(SVMConst.kernel, Feature, Feature) .* (Target * Target');
  H = H + 1e-10 * eye(NumOfFeatureData, NumOfFeatureData);
  F = -ones(NumOfFeatureData, 1);
  A = Target'; b = 0;
  TradeOffWeight = SVMConst.TradeOffWeight;
  Tolerance = SVMConst.Torlerance;
  LowerBoundary = zeros(NumOfFeatureData, 1);
  UpperBoundary = TradeOffWeight * ones(NumOfFeatureData, 1);
  Alpha0 = zeros(NumOfFeatureData, 1);

  qp_opts = optimset('MaxIter',SVMConst.QP.MaxIter, 'Algorithm', SVMConst.QP.Algorithm);
  Alpha = quadprog(H, F, [], [], A, b, LowerBoundary, UpperBoundary, Alpha0, qp_opts);
  SVIndex = find(Alpha > sqrt(eps));
  % Determine Support Vector
  SV = Feature(SVIndex,:);
  % Determine Alpha Hat
  AlphaHat = Target(SVIndex) .* Alpha(SVIndex);
  % Determine bias
  [~,MaxIdx] = max(Alpha);
  bias = Target(MaxIdx) - sum(AlphaHat .* feval(SVMConst.kernel, SV, Feature(MaxIdx,:)));
end
