function [ Output ] = linear_kernel(In1, In2)
  Output = In1 * In2';
end
