function [coeff,latent] = PCA(V, PCAConst)
  disp 'Principal Component Analysis';
  disp 'Get Covariance';
  tic;
  covariance = cov(V);
  toc;
  disp 'Get Eigenvector using SVD';
  tic;
  % Reference: stats/stats/pcacov.m 
  [~,latent,coeff] = svd(covariance);
  latent = diag(latent);
  % Enforce a sign convention on the coefficients -- the largest element in each
  % column will have a positive sign.
  [p,d] = size(coeff);
  [~,maxind] = max(abs(coeff),[],1);
  colsign = sign(coeff(maxind + (0:p:(d-1)*p)));
  coeff = bsxfun(@times,coeff,colsign);
  toc;
