function [Y,L,MSE] = kmeans(X,K,Y0)
  %KMEANS K-means clustering
  % X 데이터 벡터
  % K 클러스터 갯수
  % Y0 초기 중심값(옵션)
  %
  %출력 인자:
  % Y 출력 열 벡터(K 개의 열)
  % L 각 데이터 벡터의 색인된 라벨링 값
  % MSE mean square error
  disp 'Get centroids using K-means algorithm';
  
  NumOfTrainData = size(X,1);
  if nargin<3
    R = randperm(NumOfTrainData);
    randidx = R(1:K);
    Y = X(randidx,:);
  else
    Y = Y0;
  end

  % References: runkmeans.m
  MSE = 1e+250;
  XSquareSum = sum(X.^2, 2);
  for loop = [1:1000]
    tic;
    YSquareSum = sum(Y.^2, 2);
    % ||X_i - C_k1|| < ||X_i - C_k2||
    % <-> (X_i - C_k1)^2 < (X_i - C_k2)^2
    % <-> 2 * X_i * C_k1 - C_k1^2 > 2 * X_i * C_k2 - C_k2^2
    [val, L] = max(bsxfun(@minus, 2 * Y * X', YSquareSum));
    S = sparse(1:NumOfTrainData, L, 1, NumOfTrainData, K, NumOfTrainData);
    counts = sum(S, 1)';
    Y = bsxfun(@rdivide, S' * X, counts); % labels as indicator matrix
    % just zap empty centroids so they don't introduce NaNs everywhere.
    Y(find(counts == 0), :) = 0;

    prevMSE = MSE;
    MSE = sum(XSquareSum - val');
    DeltaDistortion = (prevMSE - MSE) / prevMSE;
    fprintf('K-means> Iteration: %d, delta distortion: %f\n', loop, DeltaDistortion);
    toc;
    
    if (prevMSE - MSE) / prevMSE < 0.0001
      disp('K-means clustering converged');
      break;
    end
  end
