classdef (Sealed) Lsqnonlin < optim.options.Lsqncommon
%

%Lsqnonlin Options for LSQNONLIN
%
%   The OPTIM.OPTIONS.LSQNONLIN class allows the user to create a set of
%   options for the LSQNONLIN solver. For a list of options that can be set,
%   see the documentation for LSQNONLIN.
%
%   OPTS = OPTIM.OPTIONS.LSQNONLIN creates a set of options for LSQNONLIN
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQNONLIN(PARAM, VAL, ...) creates a set of options
%   for LSQNONLIN with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQNONLIN(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS

%   Copyright 2012 The MathWorks, Inc.    
       
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%         
        SolverName = 'lsqnonlin';
    end       

    methods (Hidden)
        
        function obj = Lsqnonlin(varargin)
%Lsqnonlin Options for LSQNONLIN
%
%   The OPTIM.OPTIONS.LSQNONLIN class allows the user to create a set of
%   options for the LSQNONLIN solver. For a list of options that can be set,
%   see the documentation for LSQNONLIN.
%
%   OPTS = OPTIM.OPTIONS.LSQNONLIN creates a set of options for LSQNONLIN
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQNONLIN(PARAM, VAL, ...) creates a set of options
%   for LSQNONLIN with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQNONLIN(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS

            % Call the superclass constructor
            obj = obj@optim.options.Lsqncommon(varargin{:});
               
            % Record the class version
            obj.Version = 1;
        end
        
    end
    
end
    