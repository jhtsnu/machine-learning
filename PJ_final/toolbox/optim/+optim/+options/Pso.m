classdef (Sealed) Pso < optim.options.SingleAlgorithm
%

%Pso Options for PSO
%
%   The OPTIM.OPTIONS.PSO class allows the user to create a set of options
%   for the PSO solver. For a list of options that can be set, see the
%   documentation for sbioparamestim.
%
%   OPTS = OPTIM.OPTIONS.PSO creates a set of options for PSO
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.PSO(PARAM, VAL, ...) creates a set of options
%   for PSO with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.PSO(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.SINGLEALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS

%   Copyright 2012 The MathWorks, Inc.
    
    properties (Dependent)
%CREATIONFCN Handle to the function that creates the initial particles
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        CreationFcn
        
%DISPLAY Level of display
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        Display
        
%DISPLAYINTERVAL Interval for iterative display
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        DisplayInterval
        
%FUNVALCHECK Check whether the objective function values are finite.
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        FunValCheck
        
%HYBRIDFCN Run HybridFcn at the end of iterations of the solver
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        HybridFcn
        
%INERTIARANGE Two-element vector of the adaptive interia factor range
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        InertiaRange
        
%INITIALPOSITIONS Initial particle positions used to seed the algorithm
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        InitialPositions
        
%MAXFUNEVALS Maximum number of function evaluations allowed
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        MaxFunEvals
        
%MAXITER Maximum number of iterations allowed
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        MaxIter
        
%MINNEIGHBORHOODFRACTION Minimum neighborhood size as fraction of numParticles
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        MinNeighborhoodFraction
        
%NUMPARTICLES Number of particles in the swarm
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        NumParticles
        
%OBJECTIVELIMIT Minimum objective function value desired
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        ObjectiveLimit
        
%OUTPUTFCNS Functions that get iterative data and can change options
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        OutputFcns
        
%POSITIONINITSPAN Scalar or vector specifying range of initial positions
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        PositionInitSpan
        
%SELFRECOGNITIONCOEFFICIENT PSO self-recognition coefficient
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        SelfRecognitionCoefficient
        
%SOCIALRECOGNITIONCOEFFICIENT PSO social-recognition coefficient
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        SocialRecognitionCoefficient
        
%STALLITERLIMIT Maximum number of stalled iterations
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        StallIterLimit
        
%STALLTIMELIMIT Maximum time of stalled iterations
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        StallTimeLimit
        
%TIMELIMIT The algorithm stop after running for TimeLimit seconds
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        TimeLimit
        
%TOLFUN Termination tolerance on function value
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        TolFun
        
%USEPARALLEL Compute the objective function of particles in parallel
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        UseParallel
        
%VECTORIZED Compute the objective function with a vectorized function call
%
%   For more information, type "doc sbioparamestim" and see the "PSO
%   Options" section.
        Vectorized
    end

    properties (Hidden, Access = protected)
        
%OPTIONSSTORE Contains the option values and meta-data for the class
%
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
        
%SOLVERNAME Name of the solver that the options are intended for
%
        SolverName = 'pso';
    end
    
    methods (Hidden)
        
        function obj = Pso(varargin)
%Pso Options for PSO
%
%   The OPTIM.OPTIONS.PSO class allows the user to create a set of options
%   for the PSO solver. For a list of options that can be set, see the
%   documentation for PSO.
%
%   OPTS = OPTIM.OPTIONS.PSO creates a set of options for PSO with the
%   options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.PSO(PARAM, VAL, ...) creates a set of options for
%   PSO with the named parameters altered with the specified values.
%
%   OPTS = OPTIM.OPTIONS.PSO(OLDOPTS, PARAM, VAL, ...) creates a copy of
%   OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.SINGLEALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
            
            % Call the superclass constructor
            obj = obj@optim.options.SingleAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
            
        end
        
    end
    
    % Set/get methods
    methods
        function obj = set.CreationFcn(obj, value)
            obj = setProperty(obj, 'CreationFcn', value);
        end

        function obj = set.Display(obj, value)
            obj = setProperty(obj, 'Display', value, ...
                {'none','off','iter','final'});
        end
        
        function obj = set.DisplayInterval(obj, value)
            obj = setProperty(obj, 'DisplayInterval', value);
        end

        function obj = set.FunValCheck(obj, value)
            obj = setProperty(obj, 'FunValCheck', value);
        end

        function obj = set.HybridFcn(obj, value)
            obj = setProperty(obj, 'HybridFcn', value);
        end

        function obj = set.InertiaRange(obj, value)
            obj = setProperty(obj, 'InertiaRange', value);
        end

        function obj = set.InitialPositions(obj, value)
            obj = setProperty(obj, 'InitialPositions', value);
        end

        function obj = set.MaxFunEvals(obj, value)
            obj = setProperty(obj, 'MaxFunEvals', value, ...
                obj.OptionsStore.Defaults.MaxFunEvals);
        end

        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value, ...
                obj.OptionsStore.Defaults.MaxIter);
        end

        function obj = set.MinNeighborhoodFraction(obj, value)
            obj = setProperty(obj, 'MinNeighborhoodFraction', value);
        end

        function obj = set.NumParticles(obj, value)
            obj = setProperty(obj, 'NumParticles', value);
        end

        function obj = set.ObjectiveLimit(obj, value)
            obj = setProperty(obj, 'ObjectiveLimit', value);
        end

        function obj = set.OutputFcns(obj, value)
            obj = setProperty(obj, 'OutputFcns', value);
        end

        function obj = set.PositionInitSpan(obj, value)
            obj = setProperty(obj, 'PositionInitSpan', value);
        end

        function obj = set.SelfRecognitionCoefficient(obj, value)
            obj = setProperty(obj, 'SelfRecognitionCoefficient', value);
        end

        function obj = set.SocialRecognitionCoefficient(obj, value)
            obj = setProperty(obj, 'SocialRecognitionCoefficient', value);
        end

        function obj = set.StallIterLimit(obj, value)
            obj = setProperty(obj, 'StallIterLimit', value);
        end

        function obj = set.StallTimeLimit(obj, value)
            obj = setProperty(obj, 'StallTimeLimit', value);
        end

        function obj = set.TimeLimit(obj, value)
            obj = setProperty(obj, 'TimeLimit', value);
        end

        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end

        function obj = set.UseParallel(obj, value)
            obj = setProperty(obj, 'UseParallel', value);
        end

        function obj = set.Vectorized(obj, value)
            obj = setProperty(obj, 'Vectorized', value);
        end

        function value = get.CreationFcn(obj)
            value = obj.OptionsStore.Options.CreationFcn;
        end

        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end

        function value = get.DisplayInterval(obj)
            value = obj.OptionsStore.Options.DisplayInterval;
        end

        function value = get.FunValCheck(obj)
            value = obj.OptionsStore.Options.FunValCheck;
        end

        function value = get.HybridFcn(obj)
            value = obj.OptionsStore.Options.HybridFcn;
        end

        function value = get.InertiaRange(obj)
            value = obj.OptionsStore.Options.InertiaRange;
        end

        function value = get.InitialPositions(obj)
            value = obj.OptionsStore.Options.InitialPositions;
        end

        function value = get.MaxFunEvals(obj)
            value = obj.OptionsStore.Options.MaxFunEvals;
        end

        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end

        function value = get.MinNeighborhoodFraction(obj)
            value = obj.OptionsStore.Options.MinNeighborhoodFraction;
        end

        function value = get.NumParticles(obj)
            value = obj.OptionsStore.Options.NumParticles;
        end

        function value = get.ObjectiveLimit(obj)
            value = obj.OptionsStore.Options.ObjectiveLimit;
        end

        function value = get.OutputFcns(obj)
            value = obj.OptionsStore.Options.OutputFcns;
        end

        function value = get.PositionInitSpan(obj)
            value = obj.OptionsStore.Options.PositionInitSpan;
        end

        function value = get.SelfRecognitionCoefficient(obj)
            value = obj.OptionsStore.Options.SelfRecognitionCoefficient;
        end

        function value = get.SocialRecognitionCoefficient(obj)
            value = obj.OptionsStore.Options.SocialRecognitionCoefficient;
        end

        function value = get.StallIterLimit(obj)
            value = obj.OptionsStore.Options.StallIterLimit;
        end

        function value = get.StallTimeLimit(obj)
            value = obj.OptionsStore.Options.StallTimeLimit;
        end

        function value = get.TimeLimit(obj)
            value = obj.OptionsStore.Options.TimeLimit;
        end

        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end

        function value = get.UseParallel(obj)
            value = obj.OptionsStore.Options.UseParallel;
        end

        function value = get.Vectorized(obj)
            value = obj.OptionsStore.Options.Vectorized;
        end
    end
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instatntiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing all the options in a
%   field of OS called Defaults. This structure must then be passed to the
%   optim.options.generateSingleAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Pso.

% Define the option defaults for the solver
OS.Defaults.CreationFcn = @psocreationuniform;
OS.Defaults.Display = 'final';
OS.Defaults.DisplayInterval = 1;
OS.Defaults.FunValCheck = 'off';
OS.Defaults.HybridFcn = [];
OS.Defaults.InertiaRange = [0.1 1.1];
OS.Defaults.InitialPositions = [];
OS.Defaults.MaxFunEvals = '3000*numberofvariables';
OS.Defaults.MaxIter = '200*numberofvariables';
OS.Defaults.MinNeighborhoodFraction = 0.25;
OS.Defaults.NumParticles = 'min(100,10*numberofvariables)';
OS.Defaults.ObjectiveLimit = -inf;
OS.Defaults.OutputFcns = [];
OS.Defaults.PositionInitSpan = 2e3;
OS.Defaults.SelfRecognitionCoefficient = 1.49;
OS.Defaults.SocialRecognitionCoefficient = 1.49;
OS.Defaults.StallIterLimit = 20;
OS.Defaults.StallTimeLimit = inf;
OS.Defaults.TimeLimit = inf;
OS.Defaults.TolFun = 1e-6;
OS.Defaults.UseParallel = 'never';
OS.Defaults.Vectorized = 'off';

% Call the package function to generate the OptionsStore
OS = optim.options.generateSingleAlgorithmOptionsStore(OS);
end
