classdef (Sealed) Lsqcurvefit < optim.options.Lsqncommon
%

%Lsqcurvefit Options for LSQCURVEFIT
%
%   The OPTIM.OPTIONS.LSQCURVEFIT class allows the user to create a set of
%   options for the LSQCURVEFIT solver. For a list of options that can be set,
%   see the documentation for LSQCURVEFIT.
%
%   OPTS = OPTIM.OPTIONS.LSQCURVEFIT creates a set of options for LSQCURVEFIT
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQCURVEFIT(PARAM, VAL, ...) creates a set of options
%   for LSQCURVEFIT with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQCURVEFIT(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
    
%   Copyright 2012 The MathWorks, Inc.    
       
    properties (Hidden)
        SolverName = 'lsqcurvefit';
    end       

    methods (Hidden)
        
        function obj = Lsqcurvefit(varargin)
%Lsqcurvefit Options for LSQCURVEFIT
%
%   The OPTIM.OPTIONS.LSQCURVEFIT class allows the user to create a set of
%   options for the LSQCURVEFIT solver. For a list of options that can be set,
%   see the documentation for LSQCURVEFIT.
%
%   OPTS = OPTIM.OPTIONS.LSQCURVEFIT creates a set of options for LSQCURVEFIT
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQCURVEFIT(PARAM, VAL, ...) creates a set of options
%   for LSQCURVEFIT with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQCURVEFIT(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
            
            % Call the superclass constructor
            obj = obj@optim.options.Lsqncommon(varargin{:});
               
            % Record the class version
            obj.Version = 1;
        end
        
    end
    
end
    
