classdef (Sealed) Quadprog < optim.options.MultiAlgorithm
%

%Quadprog Options for QUADPROG
%
%   The OPTIM.OPTIONS.QUADPROG class allows the user to create a set of
%   options for the QUADPROG solver. For a list of options that can be set,
%   see the documentation for QUADPROG.
%
%   OPTS = OPTIM.OPTIONS.QUADPROG creates a set of options for QUADPROG
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.QUADPROG(PARAM, VAL, ...) creates a set of options
%   for QUADPROG with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.QUADPROG(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS

%   Copyright 2012 The MathWorks, Inc.    
    
    properties (Dependent)
%DIAGNOSTICS Display diagnostic information about the function
%
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.        
        Diagnostics
        
%DISPLAY Level of display
%
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.        
        Display
        
%HESSMULT Function handle for a Hessian multiply function
%
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.        
        HessMult
        
%MAXITER Maximum number of iterations allowed
%
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.               
        MaxIter
        
%MAXPCGITER Maximum number of PCG (preconditioned conjugate gradient)
%           iterations        
%
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.               
        MaxPCGIter

%PRECONDBANDWIDTH Upper bandwidth of the preconditioner for PCG    
%
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.
        PrecondBandWidth
        
%TOLCON Tolerance on the constraint violation
% 
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.        
        TolCon
        
%TOLFUN Termination tolerance on the function value
% 
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.        
        TolFun
        
%TOLPCG Termination tolerance on the PCG iteration
% 
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.        
        TolPCG
        
%TOLX Termination tolerance on x
% 
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.          
        TolX
        
%TYPICALX Typical x values        
% 
%   For more information, type "doc quadprog" and see the "Options" section
%   in the QUADPROG documentation page.        
        TypicalX
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%          
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%          
        SolverName = 'quadprog';
    end
    
    methods (Hidden)
        
        function obj = Quadprog(varargin)
%Fmincon Options for QUADPROG
%
%   The OPTIM.OPTIONS.QUADPROG class allows the user to create a set of
%   options for the QUADPROG solver. For a list of options that can be set,
%   see the documentation for QUADPROG.
%
%   OPTS = OPTIM.OPTIONS.QUADPROG creates a set of options for QUADPROG
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.QUADPROG(PARAM, VAL, ...) creates a set of options
%   for QUADPROG with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.QUADPROG(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS    

            % Call the superclass constructor
            obj = obj@optim.options.MultiAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
        end
        
        function optionFeedback = createOptionFeedback(obj)
%createOptionFeedback Create option feedback string 
%
%   optionFeedback = createOptionFeedback(obj) creates an option feedback
%   strings that are required by the extended exit messages. OPTIONFEEDBACK
%   is a structure containing strings for the options that appear in the
%   extended exit messages. These strings indicate whether the option is at
%   its 'default' value or has been 'selected'.   

            % It is possible for a user to pass in a vector of options to
            % the solver. Silently use the first element in this array.
            obj = obj(1);
            
            % Check if TolFun is the default value
            if obj.OptionsStore.SetByUser.TolFun
                optionFeedback.TolFun = 'selected';
            else
                optionFeedback.TolFun = 'default';
            end
            
            % Check if TolX is the default value
            if obj.OptionsStore.SetByUser.TolX
                optionFeedback.TolX = 'selected';
            else
                optionFeedback.TolX = 'default';
            end
            
            % Check if MaxIter is the default value
            if obj.OptionsStore.SetByUser.MaxIter
                optionFeedback.MaxIter = 'selected';
            else
                optionFeedback.MaxIter = 'default';
            end
                        
            % Check if TolCon is the default value
            if obj.OptionsStore.SetByUser.TolCon
                optionFeedback.TolCon = 'selected';
            else
                optionFeedback.TolCon = 'default';
            end
            
        end
        
    end
    
    % Set/get methods
    methods
        
        function obj = set.HessMult(obj, value)
            obj = setProperty(obj, 'HessMult', value);
        end
        
        function obj = set.MaxPCGIter(obj, value)
            % MaxPCGIter option can take a string that is equal to one of
            % the strings specified in the algorithm defaults. We specify a
            % unique list of these strings via the fourth input of
            % setProperty.                                    
            obj = setProperty(obj, 'MaxPCGIter', value, ...
                obj.OptionsStore.AlgorithmDefaults{1}.MaxPCGIter);
        end
        
        function obj = set.PrecondBandWidth(obj, value)
            obj = setProperty(obj, 'PrecondBandWidth', value);
        end
        
        function obj = set.TolPCG(obj, value)
            obj = setProperty(obj, 'TolPCG', value);
        end
        
        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value);
        end
        
        function obj = set.TolX(obj, value)
            obj = setProperty(obj, 'TolX', value);
        end
        
        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.                                                                        
            obj = setProperty(obj, 'Display', value, ...
                {'off','none','final', ...
                'final-detailed','iter','iter-detailed','testing'});
        end
        
        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function obj = set.TolCon(obj, value)
            obj = setProperty(obj, 'TolCon', value);
        end
        
        function obj = set.TypicalX(obj, value)
            obj = setProperty(obj, 'TypicalX', value);
        end
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.HessMult(obj)
            value = obj.OptionsStore.Options.HessMult;
        end
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxPCGIter(obj)
            value = obj.OptionsStore.Options.MaxPCGIter;
        end
        
        function value = get.PrecondBandWidth(obj)
            value = obj.OptionsStore.Options.PrecondBandWidth;
        end
        
        function value = get.TolCon(obj)
            value = obj.OptionsStore.Options.TolCon;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
        function value = get.TolPCG(obj)
            value = obj.OptionsStore.Options.TolPCG;
        end
        
        function value = get.TolX(obj)
            value = obj.OptionsStore.Options.TolX;
        end
        
        function value = get.TypicalX(obj)
            value = obj.OptionsStore.Options.TypicalX;
        end
        
    end
    
    % Hidden utility methods
    methods (Hidden)
        function OptionsStruct = extractOptionsStructure(obj)
%extractOptionsStructure Extract options structure from OptionsStore
%
%   OptionsStruct = extractOptionsStructure(obj) extracts a plain structure
%   containing the options from obj.OptionsStore. The solver calls
%   convertForSolver, which in turn calls this method to obtain a plain
%   options structure.
%
%   This method also maps the TolFun and MaxIter options to empty if they
%   are currently marked as having a 'default dependent on problem'.

            % Call superclass method
            OptionsStruct = extractOptionsStructure@optim.options.MultiAlgorithm(obj);
            
            % Call method to map optimoptions onto the old optimset
            % structure
            OptionsStruct = mapOptionsToOptimset(obj, OptionsStruct);
            
        end
        
        function OptionsStruct = mapOptionsToOptimset(~, OptionsStruct)
%mapOptionsToOptimset Map structure to an optimset one
%
%   OptionsStruct = mapOptionsToOptimset(obj, OptionsStruct) maps the
%   specified structure to appear that it has been generated by OPTIMSET.

            if isfield(OptionsStruct, 'TolFun') && ...
                    strcmp(OptionsStruct.TolFun, 'default dependent on problem')
                OptionsStruct.TolFun = [];
            end
            
            if isfield(OptionsStruct, 'MaxIter') && ...
                    strcmp(OptionsStruct.MaxIter, 'default dependent on problem')
                OptionsStruct.MaxIter = [];
            end            
        end
        
    end
    
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing the following fields:-
%
%   AlgorithmNames   : Cell array of algorithm names for the solver
%   DefaultAlgorithm : String containing the name of the default algorithm
%   AlgorithmDefaults: Cell array of structures. AlgorithmDefaults{i}
%                      holds a structure containing the defaults for 
%                      AlgorithmNames{i}.
%
%   This structure must then be passed to the
%   optim.options.generateMultiAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Quadprog.

% Define the algorithm names
OS.AlgorithmNames = {'trust-region-reflective', 'interior-point-convex', 'active-set'};

% Define the default algorithm
OS.DefaultAlgorithm = 'trust-region-reflective';

% Define the defaults for each algorithm
% trust-region-reflective
OS.AlgorithmDefaults{1}.Diagnostics = 'off';
OS.AlgorithmDefaults{1}.Display = 'final';
OS.AlgorithmDefaults{1}.MaxIter = 'default dependent on problem';
OS.AlgorithmDefaults{1}.TolFun = 'default dependent on problem';
OS.AlgorithmDefaults{1}.TolX = 100*eps;
OS.AlgorithmDefaults{1}.HessMult = [];
OS.AlgorithmDefaults{1}.MaxPCGIter = 'max(1,floor(numberOfVariables/2))';
OS.AlgorithmDefaults{1}.PrecondBandWidth = 0;
OS.AlgorithmDefaults{1}.TolPCG = 0.1;
OS.AlgorithmDefaults{1}.TypicalX = 'ones(numberOfVariables,1)';

% interior-point-convex
OS.AlgorithmDefaults{2}.Diagnostics = 'off';
OS.AlgorithmDefaults{2}.Display = 'final';
OS.AlgorithmDefaults{2}.MaxIter = 200;
OS.AlgorithmDefaults{2}.TolFun = 1e-8;
OS.AlgorithmDefaults{2}.TolX = 1e-8;
OS.AlgorithmDefaults{2}.TolCon = 1e-8;

% active-set
OS.AlgorithmDefaults{3}.Diagnostics = 'off';
OS.AlgorithmDefaults{3}.Display = 'final';
OS.AlgorithmDefaults{3}.MaxIter = 200;

% Call the package function to generate the OptionsStore
OS = optim.options.generateMultiAlgorithmOptionsStore(OS);

end
