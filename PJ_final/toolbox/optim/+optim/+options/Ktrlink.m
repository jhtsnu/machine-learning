classdef (Sealed) Ktrlink < optim.options.MultiAlgorithm
%

%Ktrlink Options for KTRLINK
%
%   The OPTIM.OPTIONS.KTRLINK class allows the user to create a set of
%   options for the KTRLINK solver. For a list of options that can be set,
%   see the documentation for KTRLINK.
%
%   OPTS = OPTIM.OPTIONS.KTRLINK creates a set of options for KTRLINK
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.KTRLINK(PARAM, VAL, ...) creates a set of options
%   for KTRLINK with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.KTRLINK(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
    
%   Copyright 2012 The MathWorks, Inc.    
    
    properties (Dependent)
        
%ALWAYSHONORCONSTRAINTS Determine whether bounds are satisfied at every
%                       iteration
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        AlwaysHonorConstraints
        
%DISPLAY Level of display
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        Display
        
%FINDIFFTYPE Finite difference type
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        FinDiffType
        
%FUNVALCHECK Check whether objective function and constraints values are
%            valid
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        FunValCheck
        
%GRADCONSTR Gradient for nonlinear constraint functions defined by the user
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        GradConstr
        
%GRADOBJ Gradient for the objective function defined by the user
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        GradObj
        
%HESSFCN Function handle to a user-supplied Hessian
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        HessFcn
        
%HESSIAN Specify whether a user-supplied Hessian will be supplied
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        Hessian
        
%HESSMULT Function handle for Hessian multiply function        
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        HessMult
        
%HESSPATTERN Sparsity pattern of the Hessian for finite differencing
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        HessPattern
        
%INITBARRIERPARAM Initial barrier value
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        InitBarrierParam
        
%INITTRUSTREGIONRADIUS Initial radius of the trust region
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        InitTrustRegionRadius
        
%JACOBPATTERN Sparsity pattern of the Jacobian        
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        JacobPattern
        
%MAXITER Maximum number of iterations allowed 
%
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        MaxIter
        
%MAXPROJCGITER A tolerance for the number of projected conjugate gradient 
%              iterations      
% 
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        MaxProjCGIter
        
%OBJECTIVELIMIT Lower limit on the objective function
% 
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        ObjectiveLimit
        
%SCALEPROBLEM Determine whether all constraints and the objective function
%             are normalized
% 
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        ScaleProblem
        
%SUBPROBLEMALGORITHM Determines how the iteration step is calculated
% 
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        SubproblemAlgorithm
        
%TOLCON Tolerance on the constraint violation
% 
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        TolCon
        
%TOLFUN Termination tolerance on the function value
% 
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.        
        TolFun
        
% TODO: May be able to remove this        
        TolGradCon
        
%TOLX Termination tolerance on x
% 
%   For more information, type "doc ktrlink" and see the "Options" section
%   in the KTRLINK documentation page.           
        TolX
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%         
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%          
        SolverName = 'ktrlink';
    end
    
    methods (Hidden)
        
        function obj = Ktrlink(varargin)
%Ktrlink Options for KTRLINK
%
%   The OPTIM.OPTIONS.KTRLINK class allows the user to create a set of
%   options for the KTRLINK solver. For a list of options that can be set,
%   see the documentation for KTRLINK.
%
%   OPTS = OPTIM.OPTIONS.KTRLINK creates a set of options for KTRLINK
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.KTRLINK(PARAM, VAL, ...) creates a set of options
%   for KTRLINK with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.KTRLINK(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS            
            
            % Call the superclass constructor
            obj = obj@optim.options.MultiAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
        end
        
    end
    
    % Set/get methods
    methods
        
        
        function obj = set.Hessian(obj, value)
            obj = setProperty(obj, 'Hessian', value);
        end
        
        function obj = set.HessMult(obj, value)
            obj = setProperty(obj, 'HessMult', value);
        end
        
        function obj = set.HessPattern(obj, value)
            obj = setProperty(obj, 'HessPattern', value);
        end
        
        function obj = set.AlwaysHonorConstraints(obj, value)
            obj = setProperty(obj, 'AlwaysHonorConstraints', value);
        end
        
        function obj = set.HessFcn(obj, value)
            obj = setProperty(obj, 'HessFcn', value);
        end
        
        function obj = set.InitBarrierParam(obj, value)
            obj = setProperty(obj, 'InitBarrierParam', value);
        end
        
        function obj = set.InitTrustRegionRadius(obj, value)
            obj = setProperty(obj, 'InitTrustRegionRadius', value);
        end
        
        function obj = set.JacobPattern(obj, value)
            obj = setProperty(obj, 'JacobPattern', value);
        end
        
        function obj = set.MaxProjCGIter(obj, value)
            obj = setProperty(obj, 'MaxProjCGIter', value);
        end
        
        function obj = set.ObjectiveLimit(obj, value)
            obj = setProperty(obj, 'ObjectiveLimit', value);
        end
        
        function obj = set.ScaleProblem(obj, value)
            % Pass the possible values that the ScaleProblem option can take via
            % the fourth input of setProperty.                                                            
            obj = setProperty(obj, 'ScaleProblem', value, ...
                {'none' ; 'obj-and-constr' });
        end
        
        function obj = set.SubproblemAlgorithm(obj, value)
            obj = setProperty(obj, 'SubproblemAlgorithm', value);
        end
        
        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value);
        end

        function obj = set.TolGradCon(obj, value)
            obj = setProperty(obj, 'TolGradCon', value);
        end
        
        function obj = set.TolX(obj, value)
            obj = setProperty(obj, 'TolX', value);
        end
        
        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.                                                
            obj = setProperty(obj, 'Display', value, ...
                {'off','none','notify','notify-detailed','final', ...
                'final-detailed','iter','iter-detailed'});
        end
        
        function obj = set.FinDiffType(obj, value)
            obj = setProperty(obj, 'FinDiffType', value);
        end
        
        function obj = set.FunValCheck(obj, value)
            obj = setProperty(obj, 'FunValCheck', value);
        end
        
        function obj = set.GradConstr(obj, value)
            obj = setProperty(obj, 'GradConstr', value);
        end
        
        function obj = set.GradObj(obj, value)
            obj = setProperty(obj, 'GradObj', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function obj = set.TolCon(obj, value)
            obj = setProperty(obj, 'TolCon', value);
        end
        
        function value = get.AlwaysHonorConstraints(obj)
            value = obj.OptionsStore.Options.AlwaysHonorConstraints;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.FinDiffType(obj)
            value = obj.OptionsStore.Options.FinDiffType;
        end
        
        function value = get.FunValCheck(obj)
            value = obj.OptionsStore.Options.FunValCheck;
        end
        
        function value = get.GradConstr(obj)
            value = obj.OptionsStore.Options.GradConstr;
        end
        
        function value = get.GradObj(obj)
            value = obj.OptionsStore.Options.GradObj;
        end
        
        function value = get.HessFcn(obj)
            value = obj.OptionsStore.Options.HessFcn;
        end
        
        function value = get.Hessian(obj)
            value = obj.OptionsStore.Options.Hessian;
        end
        
        function value = get.HessMult(obj)
            value = obj.OptionsStore.Options.HessMult;
        end
        
        function value = get.HessPattern(obj)
            value = obj.OptionsStore.Options.HessPattern;
        end
        
        function value = get.InitBarrierParam(obj)
            value = obj.OptionsStore.Options.InitBarrierParam;
        end
        
        function value = get.InitTrustRegionRadius(obj)
            value = obj.OptionsStore.Options.InitTrustRegionRadius;
        end

        function value = get.JacobPattern(obj)
            value = obj.OptionsStore.Options.JacobPattern;
        end        
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxProjCGIter(obj)
            value = obj.OptionsStore.Options.MaxProjCGIter;
        end
        
        function value = get.ObjectiveLimit(obj)
            value = obj.OptionsStore.Options.ObjectiveLimit;
        end
                
        function value = get.ScaleProblem(obj)
            value = obj.OptionsStore.Options.ScaleProblem;
        end
        
        function value = get.SubproblemAlgorithm(obj)
            value = obj.OptionsStore.Options.SubproblemAlgorithm;
        end
        
        function value = get.TolCon(obj)
            value = obj.OptionsStore.Options.TolCon;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end

       function value = get.TolGradCon(obj)
            value = obj.OptionsStore.Options.TolGradCon;
       end
        
        function value = get.TolX(obj)
            value = obj.OptionsStore.Options.TolX;
        end
        
    end
    
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing the following fields:-
%
%   AlgorithmNames   : Cell array of algorithm names for the solver
%   DefaultAlgorithm : String containing the name of the default algorithm
%   AlgorithmDefaults: Cell array of structures. AlgorithmDefaults{i}
%                      holds a structure containing the defaults for 
%                      AlgorithmNames{i}.
%
%   This structure must then be passed to the
%   optim.options.generateMultiAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Ktrlink.

% Define the algorithm names
OS.AlgorithmNames = {'interior-point', 'active-set'};

% Define the default algorithm
OS.DefaultAlgorithm = 'interior-point';

% Define the defaults for each algorithm
% Interior-point
OS.AlgorithmDefaults{1}.AlwaysHonorConstraints = 'bounds';
OS.AlgorithmDefaults{1}.Display = 'final';
OS.AlgorithmDefaults{1}.FinDiffType = 'forward';
OS.AlgorithmDefaults{1}.FunValCheck = 'off';
OS.AlgorithmDefaults{1}.GradConstr = 'off';
OS.AlgorithmDefaults{1}.GradObj = 'off';
OS.AlgorithmDefaults{1}.HessFcn = [];
OS.AlgorithmDefaults{1}.Hessian = 'bfgs';
OS.AlgorithmDefaults{1}.HessMult = [];
OS.AlgorithmDefaults{1}.HessPattern = 'sparse(ones(numberOfVariables))';
OS.AlgorithmDefaults{1}.InitBarrierParam = 0.1;
OS.AlgorithmDefaults{1}.InitTrustRegionRadius = 'sqrt(numberOfVariables)';
OS.AlgorithmDefaults{1}.JacobPattern = 'sparse(ones(Jrows,Jcols))';
OS.AlgorithmDefaults{1}.MaxIter = 10000;
OS.AlgorithmDefaults{1}.MaxProjCGIter = '2*(numberOfVariables-numberOfEqualities)';
OS.AlgorithmDefaults{1}.ObjectiveLimit = -1e20;
OS.AlgorithmDefaults{1}.ScaleProblem = 'obj-and-constr';
OS.AlgorithmDefaults{1}.SubproblemAlgorithm = 'ldl-factorization';
OS.AlgorithmDefaults{1}.TolCon = 1e-6;
OS.AlgorithmDefaults{1}.TolFun = 1e-6;
OS.AlgorithmDefaults{1}.TolGradCon = 1e-6;
OS.AlgorithmDefaults{1}.TolX = 1e-15;

% Active-set
OS.AlgorithmDefaults{2}.AlwaysHonorConstraints = 'bounds';
OS.AlgorithmDefaults{2}.Display = 'final';
OS.AlgorithmDefaults{2}.FinDiffType = 'forward';
OS.AlgorithmDefaults{2}.FunValCheck = 'off';
OS.AlgorithmDefaults{2}.GradConstr = 'off';
OS.AlgorithmDefaults{2}.GradObj = 'off';
OS.AlgorithmDefaults{2}.HessFcn = [];
OS.AlgorithmDefaults{2}.Hessian = 'bfgs';
OS.AlgorithmDefaults{2}.HessMult = [];
OS.AlgorithmDefaults{2}.HessPattern = 'sparse(ones(numberOfVariables))';
OS.AlgorithmDefaults{2}.InitBarrierParam = 0.1;
OS.AlgorithmDefaults{2}.InitTrustRegionRadius = 'sqrt(numberOfVariables)';
OS.AlgorithmDefaults{2}.JacobPattern = 'sparse(ones(Jrows,Jcols))';
OS.AlgorithmDefaults{2}.MaxIter = 10000;
OS.AlgorithmDefaults{2}.MaxProjCGIter = '2*(numberOfVariables-numberOfEqualities)';
OS.AlgorithmDefaults{2}.ObjectiveLimit = -1e20;
OS.AlgorithmDefaults{2}.ScaleProblem = 'obj-and-constr';
OS.AlgorithmDefaults{2}.SubproblemAlgorithm = 'ldl-factorization';
OS.AlgorithmDefaults{2}.TolCon = 1e-6;
OS.AlgorithmDefaults{2}.TolFun = 1e-6;
OS.AlgorithmDefaults{2}.TolGradCon = 1e-6;
OS.AlgorithmDefaults{2}.TolX = 1e-15;

% Call the package function to generate the OptionsStore
OS = optim.options.generateMultiAlgorithmOptionsStore(OS);

end
