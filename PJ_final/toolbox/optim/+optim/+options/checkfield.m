function [validvalue, errmsg, errid, validfield] = checkfield(field, value, validStrings)
%

%CHECKFIELD Check validity of optimization options
%

%   [VALIDVALUE, ERRMSG, ERRID, VALIDFIELD] =
%   OPTIMOPTIONCHECKFIELD('field',V) checks the contents of the specified
%   value V to be valid for the field 'field'.
%
%   [VALIDVALUE, ERRMSG, ERRID, VALIDFIELD] =
%   OPTIMOPTIONCHECKFIELD('field',V, VALIDSTRINGS) checks the contents of
%   the specified value V to be valid for the field 'field'. In the case
%   where V can be a string, VALIDSTRINGS contains the possible strings
%   that V can take. VALIDSTRINGS can be a string or a cell array.

%   Copyright 2012 The MathWorks, Inc.

% To check strings, checkfield specifies the known string in lower case.
% Convert validStrings to lower case if specified. 
if nargin > 2
    validStrings = lower(validStrings);
end

% Some fields are checked in optimset/checkfield: Display, MaxFunEvals, MaxIter,
% OutputFcn, TolFun, TolX. Some are checked in both (e.g., MaxFunEvals).
validfield = true;
switch field
    case {'OutputFcn','OutputFcns','PlotFcns','CreationFcn','HybridFcn'}% function or empty
        if isempty(value)
            validvalue = true;
            errmsg = '';
            errid = '';
        else
            [validvalue, errmsg, errid] = functionOrCellArray(field,value);
        end
    case {'Display'}
        [validvalue, errmsg, errid] = stringsType(field,value,validStrings);
    case {'FunValCheck', 'Vectorized'} % off,on
        [validvalue, errmsg, errid] = stringsType(field,value,{'on';'off'});    
    case {'RelLineSrchBnd'}
        if isempty(value)
            validvalue = true;
            errmsg = '';
            errid = '';
        else
            [validvalue, errmsg, errid] = nonNegReal(field,value);
        end        
    case {'TolFun','TolX','TolCon','TolPCG','ActiveConstrTol',...
            'DiffMaxChange','DiffMinChange','TolXInteger','MaxTime', ...
            'TolProjCGAbs', ...
            'TolProjCG','TolGradCon','TolConSQP'}
        % non-negative real scalar
        [validvalue, errmsg, errid] = nonNegReal(field,value);
    case {'TolRLPFun'}
        % real scalar in the range [1.0e-9, 1.0e-5]
        [validvalue, errmsg, errid] = boundedReal(field,value,[1e-9, 1e-5]);
    case {'ObjectiveLimit'}
        [validvalue, errmsg, errid] = realLessThanPlusInf(field,value);
    case {'MaxFunEvals'}
        [validvalue, errmsg, errid] = nonNegInteger(field,value,validStrings);
    case {'NumParticles'}
        [validvalue, errmsg, errid] = boundedInteger(field, value, [2,realmax], ...
            {'min(100,10*numberofvariables)'});
    case {'MinNeighborhoodFraction'}
        [validvalue, errmsg, errid] = boundedReal(field,value,[0,1]);
    case {'MaxIter'}
        [validvalue, errmsg, errid] = nonNegInteger(field,value,validStrings); 
    case {'LargeScale','DerivativeCheck','Diagnostics','GradConstr','GradObj',...
            'Jacobian','Simplex','NoStopIfFlatInfeas','PhaseOneTotalScaling'}
        % off, on
        [validvalue, errmsg, errid] = stringsType(field,value,{'on';'off'});
    case {'PrecondBandWidth','MinAbsMax','GoalsExactAchieve', ...
            'RelLineSrchBndDuration', 'NodeDisplayInterval', 'DisplayInterval'}
        % integer including inf
        [validvalue, errmsg, errid] = nonNegInteger(field,value);
    case {'StallIterLimit'}
        % non-negative integer excluding inf
        [validvalue, errmsg, errid] = boundedInteger(field,value,[0,realmax]);
    case {'MaxPCGIter'}
        % integer including inf or default string
        [validvalue, errmsg, errid] = nonNegInteger(field,value,validStrings);
    case {'MaxProjCGIter'}
        % integer including inf or default string
        [validvalue, errmsg, errid] = nonNegInteger(field,value,'2*(numberofvariables-numberofequalities)');        
    case {'MaxSQPIter'}
        % integer including inf or default
        [validvalue, errmsg, errid] = nonNegInteger(field,value,'10*max(numberofvariables,numberofinequalities+numberofbounds)');
    case {'InitialPositions'}
        % matrix
        [validvalue, errmsg, errid] = twoDimensionalMatrixType(field,value);
    case {'JacobPattern'}
        % matrix or default string
        [validvalue, errmsg, errid] = matrixType(field,value,'sparse(ones(jrows,jcols))');
    case {'HessPattern'}
        % matrix or default string
        [validvalue, errmsg, errid] = matrixType(field,value,'sparse(ones(numberofvariables))');
    case {'TypicalX'}
        % matrix or default string
        [validvalue, errmsg, errid] = matrixType(field,value,'ones(numberofvariables,1)');
        % If an array is given, check for zero values and warn
        if validvalue && isa(value,'double') && any(value(:) == 0)
            error('optim:options:checkfield:zeroInTypicalX', ...
                getString(message('MATLAB:optimoptioncheckfield:zeroInTypicalX')));
        end
    case {'HessMult', 'HessFcn', 'JacobMult'}
        % function or empty
        if isempty(value)
            validvalue = true;
            errmsg = '';
            errid = '';
        else
            [validvalue, errmsg, errid] = functionType(field,value);
        end
    case {'HessUpdate'}
        % dfp, bfgs, steepdesc
        [validvalue, errmsg, errid] = stringsType(field,value,{'dfp' ; 'steepdesc';'bfgs'});
    case {'MeritFunction'}
        % singleobj, multiobj
        [validvalue, errmsg, errid] = stringsType(field,value,{'singleobj'; 'multiobj' });
    case {'InitialHessType'}
        % identity, scaled-identity, user-supplied
        [validvalue, errmsg, errid] = stringsType(field,value,{'identity' ; 'scaled-identity'; 'user-supplied'});
    case {'UseParallel'}
        % 'always' or 'never'
        [validvalue, errmsg, errid] = stringsType(field,value,{'always' ; 'never'});
    case {'Algorithm'}
        % active-set, trust-region-reflective, interior-point, interior-point-convex,
        % levenberg-marquardt, trust-region-dogleg, lm-line-search(undocumented)
        if ~iscell(value)
            [validvalue, errmsg, errid] = stringsType(field,value,validStrings);
        else
            % Must be {'levenberg-marquardt',positive real}
            [validvalue, errmsg, errid] = stringPosRealCellType(field,value, ...
                {'levenberg-marquardt'});
        end
    case {'AlwaysHonorConstraints'}
        % none, bounds
        [validvalue, errmsg, errid] = ...
            stringsType(field,value,{'none' ; 'bounds'});
    case {'ScaleProblem'}
        % none, obj-and-constr, jacobian
        if isempty(validStrings)
            validStrings = {'none' ; 'obj-and-constr' ; 'jacobian'};
        end
        [validvalue, errmsg, errid] = stringsType(field,value,validStrings);
    case {'FinDiffType'}
        % forward, central
        [validvalue, errmsg, errid] = stringsType(field,value,{'forward' ; 'central'});
    case 'FinDiffRelStep'
        % Although this option is documented to be a strictly positive
        % vector, matrices are implicitly supported because linear indexing
        % is used. Therefore, posVectorType is called with a reshaped
        % value.
        [validvalue, errmsg, errid] = posVectorType(field,value(:));
    case {'Hessian'}
        if ~iscell(value)
            % If character string, has to be user-supplied, bfgs, lbfgs,
            % fin-diff-grads, on, off
            [validvalue, errmsg, errid] = ...
                stringsType(field,value,{'user-supplied' ; 'bfgs'; 'lbfgs'; 'fin-diff-grads'; ...
                'on' ; 'off'});
        else
            % If cell-array, has to be {'lbfgs',positive integer}
            [validvalue, errmsg, errid] = stringPosIntegerCellType(field,value,'lbfgs');
        end
    case {'SubproblemAlgorithm'}
        if ~iscell(value)
            % If character string, has to be 'ldl-factorization' or 'cg',
            [validvalue, errmsg, errid] = ...
                stringsType(field,value,{'ldl-factorization' ; 'cg'});
        else
                % Either {'ldl-factorization',positive integer} or {'cg',positive integer}
                [validvalue, errmsg, errid] = stringPosRealCellType(field,value,{'ldl-factorization' ; 'cg'});
        end
    case {'InitialHessMatrix', 'PositionInitSpan'}
        % strictly positive matrix
        [validvalue, errmsg, errid] = posVectorType(field,value);
    case {'MaxRLPIter'}
        % integer including inf or default string
        [validvalue, errmsg, errid] = nonNegInteger(field,value,'100*numberofvariables');
    case {'MaxNodes'}
        % integer including inf or default string
        [validvalue, errmsg, errid] = nonNegInteger(field,value,'1000*numberofvariables');
    case {'BranchStrategy'}
        % mininfeas, maxinfeas
        [validvalue, errmsg, errid] = stringsType(field,value,{'mininfeas' ; 'maxinfeas'});
    case  {'NodeSearchStrategy'}
        % df, bn
        [validvalue, errmsg, errid] = stringsType(field,value,{'df' ; 'bn'});
    case {'InitTrustRegionRadius'}
        % sqrt(numberOfVariables), positive real
        [validvalue, errmsg, errid] = posReal(field,value,'sqrt(numberofvariables)');
    case {'InitBarrierParam', 'StallTimeLimit', 'TimeLimit'}
        % positive real
        [validvalue, errmsg, errid] = posReal(field,value);
    case {'SelfRecognitionCoefficient', 'SocialRecognitionCoefficient'}
        % pso
        [validvalue, errmsg, errid] = boundedReal(field,value,[-realmax,realmax]);
    case {'InertiaRange'}
        % pso
        [validvalue, errmsg, errid] = sameSignRange(field,value);
    otherwise
        validfield = false;
        validvalue = false;
        % No need to set an error. If the field isn't valid for MATLAB or Optim,
        % will have already errored in optimset. If field is valid for MATLAB,
        % then the error will be an invalid value for MATLAB.
        errid = '';
        errmsg = '';
end

%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = nonNegReal(field,value,string)
% Any nonnegative real scalar or sometimes a special string
valid =  isreal(value) && isscalar(value) && (value >= 0) ;
if nargin > 2
    valid = valid || isequal(value,string);
end
if ~valid
    if ischar(value)
        msgid = 'MATLAB:optimoptioncheckfield:nonNegRealStringType';
        errid = 'optim:options:checkfield:nonNegRealStringType';
    else
        msgid = 'MATLAB:optimoptioncheckfield:notAnonNegReal';
        errid = 'optim:options:checkfield:notAnonNegReal';
    end
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end
%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = nonNegInteger(field,value,strings)
% Any nonnegative real integer scalar or sometimes a special string
valid =  isreal(value) && isscalar(value) && (value >= 0) && value == floor(value) ;
if nargin > 2
    valid = valid || any(strcmp(value,strings));
end
if ~valid
    if ischar(value)
        msgid = 'MATLAB:optimoptioncheckfield:nonNegIntegerStringType';
        errid = 'optim:options:checkfield:nonNegIntegerStringType';
    else
        msgid = 'MATLAB:optimoptioncheckfield:notANonNegInteger';
        errid = 'optim:options:checkfield:notANonNegInteger';
    end
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = boundedInteger(field,value,bounds,strings)
% Any positive real integer scalar or sometimes a special string
valid =  isreal(value) && isscalar(value) && value == floor(value) && ...
    (value >= bounds(1)) && (value <= bounds(2));
if nargin > 3
    valid = valid || any(strcmp(value,strings));
end
if ~valid
    if ischar(value)
        errid = 'optim:options:checkfield:boundedIntegerStringType';
    else
        errid = 'optim:options:checkfield:notABoundedInteger';
    end
    errmsg = getString(message(errid, field, sprintf('[%6.3g, %6.3g]', bounds(1), bounds(2))));
else
    errid = '';
    errmsg = '';
end

%--------------------------------------------------------------------------------

function [valid, errmsg, errid] = sameSignRange(field,value)
% A two-element vector in ascending order; cannot mix positive and negative
% numbers.
valid = isreal(value) && numel(value) == 2 && value(1) <= value(2) && ...
    (all(value>=0) || all(value<=0));
if ~valid
    errid = 'optim:options:checkfield:notSameSignRange';
    errmsg = getString(message(errid, field));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = twoDimensionalMatrixType(field,value,strings)
% Any matrix
valid =  isa(value,'double') && ismatrix(value);
if nargin > 2
    valid = valid || any(strcmp(value,strings));
end
if ~valid
    if ischar(value)
        errid = 'optim:options:checkfield:twoDimTypeStringType';
    else
        errid = 'optim:options:checkfield:notATwoDimMatrix';
    end
    errmsg = getString(message(errid, field));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = matrixType(field,value,strings)
% Any non-empty double (this "matrix" can have more 2 dimensions)
valid = ~isempty(value) && ismatrix(value) && isa(value,'double');
if nargin > 2
    valid = valid || any(strcmp(value,strings));
end
if ~valid
    if ischar(value)
        msgid = 'MATLAB:optimoptioncheckfield:matrixTypeStringType';
        errid = 'optim:options:checkfield:matrixTypeStringType';
    else
        msgid = 'MATLAB:optimoptioncheckfield:notAMatrix';
        errid = 'optim:options:checkfield:notAMatrix';
    end
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = posVectorType(field,value)
% Any non-empty positive scalar or all positive vector
valid = ~isempty(value) && isa(value,'double') && isvector(value) && all(value > 0);
if ~valid
    msgid = 'MATLAB:optimoptioncheckfield:notAPosMatrix';
    errid = 'optim:options:checkfield:notAPosMatrix';
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = functionType(field,value)
% Any function handle or string (we do not test if the string is a function name)
valid =  ischar(value) || isa(value, 'function_handle');
if ~valid
    msgid = 'MATLAB:optimoptioncheckfield:notAFunction';
    errid = 'optim:options:checkfield:notAFunction';
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end
%-----------------------------------------------------------------------------------------
function [valid, errmsg, errid] = stringsType(field,value,strings)
% One of the strings in cell array strings
valid =  ischar(value) && any(strcmp(value,strings));

if ~valid
    % Format strings for error message
    allstrings = formatCellArrayOfStrings(strings);

    msgid = 'MATLAB:optimoptioncheckfield:notAStringsType';
    errid = 'optim:options:checkfield:notAStringsType';
    errmsg = getString(message(msgid, field, allstrings));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------
function [valid, errmsg, errid] = boundedReal(field,value,bounds)
% Scalar in the bounds
valid =  isa(value,'double') && isscalar(value) && ...
    (value >= bounds(1)) && (value <= bounds(2));
if ~valid
    msgid = 'MATLAB:optimoptioncheckfield:notAboundedReal';
    errid = 'optim:options:checkfield:notAboundedReal';
    errmsg = getString(message(msgid, field, sprintf('[%6.3g, %6.3g]', bounds(1), bounds(2))));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------
function [valid, errmsg, errid] = stringPosIntegerCellType(field,value,strings)
% A cell array that is either {strings,positive integer} or {strings}
valid = numel(value) == 1 && any(strcmp(value{1},strings)) || numel(value) == 2 && ...
    any(strcmp(value{1},strings)) && isreal(value{2}) && isscalar(value{2}) && value{2} > 0 && value{2} == floor(value{2});

if ~valid  
    msgid = 'MATLAB:optimoptioncheckfield:notAStringPosIntegerCellType';
    errid = 'optim:options:checkfield:notAStringPosIntegerCellType';
    errmsg = getString(message(msgid, field, strings));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------
function [valid, errmsg, errid] = stringPosRealCellType(field,value,strings)
% A cell array that is either {strings,positive real} or {strings}
valid = (numel(value) >= 1) && any(strcmpi(value{1},strings));
if (numel(value) == 2) 
   valid = valid && isreal(value{2}) && (value{2} >= 0);
end

if ~valid  
    % Format strings for error message
    allstrings = formatCellArrayOfStrings(strings);

    msgid = 'MATLAB:optimoptioncheckfield:notAStringPosRealCellType';
    errid = 'optim:options:checkfield:notAStringPosRealCellType';
    errmsg = getString(message(msgid, field,allstrings));
else
    errid = '';
    errmsg = '';
end
%-----------------------------------------------------------------------------------------
function [valid, errmsg, errid] = posReal(field,value,string)
% Any positive real scalar or sometimes a special string
valid =  isreal(value) && isscalar(value) && (value > 0) ;
if nargin > 2
   valid = valid || strcmpi(value,string);
end
if ~valid
    if ischar(value)
        msgid = 'MATLAB:optimoptioncheckfield:posRealStringType';
        errid = 'optim:options:checkfield:posRealStringType';
    else
        msgid = 'MATLAB:optimoptioncheckfield:nonPositiveNum';
        errid = 'optim:options:checkfield:nonPositiveNum';
    end
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end

%-----------------------------------------------------------------------------------------

function [valid, errmsg, errid] = realLessThanPlusInf(field,value,string)
% Any real scalar that is less than +Inf, or sometimes a special string
valid =  isreal(value) && isscalar(value) && (value < +Inf);
if nargin > 2
    valid = valid || strcmpi(value,string);
end
if ~valid
    if ischar(value)
        msgid = 'MATLAB:optimoptioncheckfield:realLessThanPlusInfStringType';
        errid = 'optim:options:checkfield:realLessThanPlusInfStringType';
    else
        msgid = 'MATLAB:optimoptioncheckfield:PlusInfReal';
        errid = 'optim:options:checkfield:PlusInfReal';
    end
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end

%---------------------------------------------------------------------------------
function allstrings = formatCellArrayOfStrings(strings)
%formatCellArrayOfStrings converts cell array of strings "strings" into an 
% array of strings "allstrings", with correct punctuation and "or" depending
% on how many strings there are, in order to create readable error message.

% To print out the error message beautifully, need to get the commas and "or"s
% in all the correct places while building up the string of possible string values.
    allstrings = ['''',strings{1},''''];
    for index = 2:(length(strings)-1)
        % add comma and a space after all but the last string
        allstrings = [allstrings, ', ''', strings{index},''''];
    end
    if length(strings) > 2
        allstrings = [allstrings,', or ''',strings{end},''''];
    elseif length(strings) == 2
        allstrings = [allstrings,' or ''',strings{end},''''];
    end

%--------------------------------------------------------------------------------

function [valid, errmsg, errid] = functionOrCellArray(field,value)
% Any function handle, string or cell array of functions 
valid = ischar(value) || isa(value, 'function_handle') || iscell(value);
if ~valid
    msgid = 'MATLAB:optimset:notAFunctionOrCellArray';
    errid = 'optim:options:checkfield:notAFunctionOrCellArray';
    errmsg = getString(message(msgid, field));
else
    errid = '';
    errmsg = '';
end
