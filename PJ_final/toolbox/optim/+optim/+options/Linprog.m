classdef (Sealed) Linprog < optim.options.MultiAlgorithm
%

%Linprog Options for LINPROG
%
%   The OPTIM.OPTIONS.LINPROG class allows the user to create a set of
%   options for the LINPROG solver. For a list of options that can be set,
%   see the documentation for LINPROG.
%
%   OPTS = OPTIM.OPTIONS.LINPROG creates a set of options for LINPROG
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LINPROG(PARAM, VAL, ...) creates a set of options
%   for LINPROG with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LINPROG(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
    
%   Copyright 2012 The MathWorks, Inc.    
    
    properties (Dependent)
%DIAGNOSTICS Display diagnostic information about the function
%
%   For more information, type "doc linprog" and see the "Options" section
%   in the LINPROG documentation page.               
        Diagnostics
        
%DISPLAY Level of display
%
%   For more information, type "doc linprog" and see the "Options" section
%   in the LINPROG documentation page.       
        Display

%MAXITER Maximum number of iterations allowed
%
%   For more information, type "doc linprog" and see the "Options" section
%   in the LINPROG documentation page.        
        MaxIter
        
%TOLFUN Termination tolerance on the function value
%
%   For more information, type "doc linprog" and see the "Options" section
%   in the LINPROG documentation page.        
        TolFun
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%        
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%          
        SolverName = 'linprog';
    end
    
    methods (Hidden)
        
        function obj = Linprog(varargin)
%Linprog Options for LINPROG
%
%   The OPTIM.OPTIONS.LINPROG class allows the user to create a set of
%   options for the LINPROG solver. For a list of options that can be set,
%   see the documentation for LINPROG.
%
%   OPTS = OPTIM.OPTIONS.LINPROG creates a set of options for LINPROG
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LINPROG(PARAM, VAL, ...) creates a set of options
%   for LINPROG with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LINPROG(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS         

            % Call the superclass constructor
            obj = obj@optim.options.MultiAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
        end
        
    end
    
    % Set/get methods
    methods
        
        function obj = set.MaxIter(obj, value)
            % MaxIter option can take a string that is equal to one of
            % the strings specified in the algorithm defaults. We specify a
            % unique list of these strings via the fourth input of
            % setProperty.            
            obj = setProperty(obj, 'MaxIter', value, ...
                {obj.OptionsStore.AlgorithmDefaults{2}.MaxIter, ...
                obj.OptionsStore.AlgorithmDefaults{3}.MaxIter});
        end
        
        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.                                                            
            obj = setProperty(obj, 'Display', value, ...
                {'off','none','final', ...
                'final-detailed','iter','iter-detailed'});
        end
        
        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
    end

    methods (Hidden)
        function [obj, OptimsetStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
%mapOptimsetToOptions Map optimset structure to optimoptions
%
%   obj = mapOptimsetToOptions(obj, OptimsetStruct) maps specified optimset
%   options, OptimsetStruct, to the equivalent options in the specified
%   optimization object, obj.
%
%   [obj, OptimsetStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
%   additionally returns an options structure modified with any conversions
%   that were performed on the options object.

            lgScale = isfield(OptimsetStruct,'LargeScale') && ...
                (isempty(OptimsetStruct.LargeScale) || strcmpi(OptimsetStruct.LargeScale,'on'));
            psx = isfield(OptimsetStruct,'Simplex') && ...
                (isempty(OptimsetStruct.Simplex) || strcmpi(OptimsetStruct.Simplex,'on'));
            if lgScale && ~psx
                obj.Algorithm = 'interior-point';
            elseif ~lgScale && ~psx
                obj.Algorithm = 'active-set';
            elseif ~lgScale && psx
                obj.Algorithm = 'simplex';
            end
            % Also modify the incoming structure.
            if nargout > 1
                OptimsetStruct.Algorithm = obj.Algorithm;
                OptimsetStruct = rmfield(OptimsetStruct,'LargeScale');
                OptimsetStruct = rmfield(OptimsetStruct,'Simplex');
            end
        end
    end
    
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing the following fields:-
%
%   AlgorithmNames   : Cell array of algorithm names for the solver
%   DefaultAlgorithm : String containing the name of the default algorithm
%   AlgorithmDefaults: Cell array of structures. AlgorithmDefaults{i}
%                      holds a structure containing the defaults for 
%                      AlgorithmNames{i}.
%
%   This structure must then be passed to the
%   optim.options.generateMultiAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Linprog.

% Define the algorithm names
OS.AlgorithmNames = {'interior-point', 'active-set', 'simplex'};

% Define the default algorithm
OS.DefaultAlgorithm = 'interior-point';

% Define the defaults for each algorithm
% interior-point
OS.AlgorithmDefaults{1}.Diagnostics = 'off';
OS.AlgorithmDefaults{1}.Display = 'final';
OS.AlgorithmDefaults{1}.MaxIter = 85;
OS.AlgorithmDefaults{1}.TolFun = 1e-8;

% active-set
OS.AlgorithmDefaults{2}.Diagnostics = 'off';
OS.AlgorithmDefaults{2}.Display = 'final';
OS.AlgorithmDefaults{2}.MaxIter = '10*max(numberOfVariables,numberOfInequalities+numberOfBounds)';

% simplex
OS.AlgorithmDefaults{3}.Diagnostics = 'off';
OS.AlgorithmDefaults{3}.Display = 'final';
OS.AlgorithmDefaults{3}.MaxIter = '100*numberOfVariables';
OS.AlgorithmDefaults{3}.TolFun = 1e-6;

% Call the package function to generate the OptionsStore
OS = optim.options.generateMultiAlgorithmOptionsStore(OS);

end
