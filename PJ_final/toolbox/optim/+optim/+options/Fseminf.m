classdef (Sealed) Fseminf < optim.options.SingleAlgorithm
%

%Fminimax Options for FSEMINF
%
%   The OPTIM.OPTIONS.FSEMINF class allows the user to create a set of
%   options for the FSEMINF solver. For a list of options that can be set,
%   see the documentation for FSEMINF.
%
%   OPTS = OPTIM.OPTIONS.FSEMINF creates a set of options for FSEMINF
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.FSEMINF(PARAM, VAL, ...) creates a set of options
%   for FSEMINF with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.FSEMINF(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.SINGLEALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
    
%   Copyright 2012 The MathWorks, Inc.

    properties (Dependent)

%DERIVATIVECHECK Compare user-supplied derivatives to finite-differencing 
%                derivatives
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.                
        DerivativeCheck
        
%DIAGNOSTICS Display diagnostic information 
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.                
        Diagnostics
        
%DIFFMAXCHANGE Maximum change in variables for finite-difference gradients        
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.             
        DiffMaxChange
        
%DIFFMINCHANGE Minimum change in variables for finite-difference gradients        
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        DiffMinChange
        
%DISPLAY Level of display
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        Display
        
%FINDIFFRELSTEP Scalar or vector step size factor
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.         
        FinDiffRelStep
        
%FINDIFFTYPE Finite difference type
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        FinDiffType
        
%FUNVALCHECK Check whether objective function and constraints values are
%            valid
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        FunValCheck
        
%GRADOBJ Gradient for the objective function defined by the user
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        GradObj
        
%MAXFUNEVALS Maximum number of function evaluations allowed   
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        MaxFunEvals
        
%MAXITER Maximum number of iterations allowed 
%
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        MaxIter
        
%MAXSQPITER Maximum number of SQP iterations allowed
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        MaxSQPIter
        
%OUTPUTFCN User-defined functions that are called at each iteration
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        OutputFcn
        
%PLOTFCNS Plots various measures of progress while the algorithm executes
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.                
        PlotFcns
        
%RELLINESRCHBND Relative bound on the line search step length
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        RelLineSrchBnd
        
%RELLINESRCHBNDDURATION Number of iterations for which the bound specified 
%                       in RelLineSrchBnd should be active
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        RelLineSrchBndDuration
        
%TOLCON Tolerance on the constraint violation
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        TolCon
        
%TOLCONSQP Termination tolerance on inner iteration SQP constraint violation
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        TolConSQP
        
%TOLFUN Termination tolerance on the function value
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        TolFun
        
%TOLX Termination tolerance on x
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        TolX
        
%TYPICALX Typical x values        
% 
%   For more information, type "doc fseminf" and see the "Options" section
%   in the FSEMINF documentation page.        
        TypicalX
        
    end
    
    % Hidden properties
    properties (Hidden, Dependent)
%NOSTOPIFFLATINFEAS If objective appears flat, only stop if feasible
%         
        NoStopIfFlatInfeas
        
%PHASEONETOTALSCALING Scale the slack variable in phase 1 of qpsub
%         
        PhaseOneTotalScaling        
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%          
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%         
        SolverName = 'fseminf';
    end
    
    methods (Hidden)
        
        function obj = Fseminf(varargin)
%Fminimax Options for FSEMINF
%
%   The OPTIM.OPTIONS.FSEMINF class allows the user to create a set of
%   options for the FSEMINF solver. For a list of options that can be set,
%   see the documentation for FSEMINF.
%
%   OPTS = OPTIM.OPTIONS.FSEMINF creates a set of options for FSEMINF
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.FSEMINF(PARAM, VAL, ...) creates a set of options
%   for FSEMINF with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.FSEMINF(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.SINGLEALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
                
            % Call the superclass constructor
            obj = obj@optim.options.SingleAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
            
        end
        
        function optionFeedback = createOptionFeedback(obj)
%createOptionFeedback Create option feedback string 
%
%   optionFeedback = createOptionFeedback(obj) creates an option feedback
%   strings that are required by the extended exit messages. OPTIONFEEDBACK
%   is a structure containing strings for the options that appear in the
%   extended exit messages. These strings indicate whether the option is at
%   its 'default' value or has been 'selected'. 

            % It is possible for a user to pass in a vector of options to
            % the solver. Silently use the first element in this array.
            obj = obj(1);
            
            % Check if TolFun is the default value
            if obj.OptionsStore.SetByUser.TolFun
                optionFeedback.TolFun = 'selected';
            else
                optionFeedback.TolFun = 'default';
            end
            
            % Check if TolX is the default value
            if obj.OptionsStore.SetByUser.TolX
                optionFeedback.TolX = 'selected';
            else
                optionFeedback.TolX = 'default';
            end
            
            % Check if MaxIter is the default value
            if obj.OptionsStore.SetByUser.MaxIter
                optionFeedback.MaxIter = 'selected';
            else
                optionFeedback.MaxIter = 'default';
            end
            
            % Check if MaxFunEvals is the default value
            if obj.OptionsStore.SetByUser.MaxFunEvals
                optionFeedback.MaxFunEvals = 'selected';
            else
                optionFeedback.MaxFunEvals = 'default';
            end
            
            % Check if TolCon is the default value
            if obj.OptionsStore.SetByUser.TolCon
                optionFeedback.TolCon = 'selected';
            else
                optionFeedback.TolCon = 'default';
            end
            
            % Check if MaxSQPIter is the default value
            if obj.OptionsStore.SetByUser.MaxSQPIter
                optionFeedback.MaxSQPIter = 'selected';
            else
                optionFeedback.MaxSQPIter = 'default';
            end
            
        end
        
        
    end
    
    % Set/get methods
    methods

        function obj = set.PhaseOneTotalScaling(obj, value)
            obj = setProperty(obj, 'PhaseOneTotalScaling', value);
        end
        
        function obj = set.NoStopIfFlatInfeas(obj, value)
            obj = setProperty(obj, 'NoStopIfFlatInfeas', value);
        end        
        
        function obj = set.FinDiffRelStep(obj, value)
            obj = setProperty(obj, 'FinDiffRelStep', value);
        end
        
        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value);
        end
        
        function obj = set.MaxFunEvals(obj, value)
            % MaxFunEvals option can take a string that is equal to the
            % default string. We specify this via the fourth input of
            % setProperty.            
            obj = setProperty(obj, 'MaxFunEvals', value, ...
                obj.OptionsStore.Defaults.MaxFunEvals);
        end
        
        function obj = set.TolX(obj, value)
            obj = setProperty(obj, 'TolX', value);
        end
        
        function obj = set.MaxSQPIter(obj, value)
            obj = setProperty(obj, 'MaxSQPIter', value);
        end
        
        function obj = set.RelLineSrchBnd(obj, value)
            obj = setProperty(obj, 'RelLineSrchBnd', value);
        end
        
        function obj = set.RelLineSrchBndDuration(obj, value)
            obj = setProperty(obj, 'RelLineSrchBndDuration', value);
        end
        
        function obj = set.TolConSQP(obj, value)
            obj = setProperty(obj, 'TolConSQP', value);
        end
        
        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.                                                
            obj = setProperty(obj, 'Display', value, ...
                {'off','none','notify','notify-detailed','final', ...
                'final-detailed','iter','iter-detailed'});
        end
        
        function obj = set.DerivativeCheck(obj, value)
            obj = setProperty(obj, 'DerivativeCheck', value);
        end
        
        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.DiffMinChange(obj, value)
            obj = setProperty(obj, 'DiffMinChange', value);
        end
        
        function obj = set.DiffMaxChange(obj, value)
            obj = setProperty(obj, 'DiffMaxChange', value);
        end
        
        function obj = set.FinDiffType(obj, value)
            obj = setProperty(obj, 'FinDiffType', value);
            % If we get here, the property set has been successful and we
            % can update the OptionsStore
            if ~obj.OptionsStore.SetByUser.FinDiffRelStep
                switch value
                    case 'forward'
                        obj.OptionsStore.Options.FinDiffRelStep = sqrt(eps);
                    case 'central'
                        obj.OptionsStore.Options.FinDiffRelStep = eps^(1/3);
                end
            end
        end
        
        function obj = set.FunValCheck(obj, value)
            obj = setProperty(obj, 'FunValCheck', value);
        end
        
        function obj = set.GradObj(obj, value)
            obj = setProperty(obj, 'GradObj', value);
        end
        
        function obj = set.OutputFcn(obj, value)
            obj = setProperty(obj, 'OutputFcn', value);
        end
        
        function obj = set.PlotFcns(obj, value)
            obj = setProperty(obj, 'PlotFcns', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function obj = set.TolCon(obj, value)
            obj = setProperty(obj, 'TolCon', value);
        end
        
        function obj = set.TypicalX(obj, value)
            obj = setProperty(obj, 'TypicalX', value);
        end
        
        function value = get.DerivativeCheck(obj)
            value = obj.OptionsStore.Options.DerivativeCheck;
        end
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.DiffMaxChange(obj)
            value = obj.OptionsStore.Options.DiffMaxChange;
        end
        
        function value = get.DiffMinChange(obj)
            value = obj.OptionsStore.Options.DiffMinChange;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.FinDiffRelStep(obj)
            value = obj.OptionsStore.Options.FinDiffRelStep;
        end
        
        function value = get.FinDiffType(obj)
            value = obj.OptionsStore.Options.FinDiffType;
        end
        
        function value = get.FunValCheck(obj)
            value = obj.OptionsStore.Options.FunValCheck;
        end
        
        function value = get.GradObj(obj)
            value = obj.OptionsStore.Options.GradObj;
        end
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxFunEvals(obj)
            value = obj.OptionsStore.Options.MaxFunEvals;
        end
        
        function value = get.MaxSQPIter(obj)
            value = obj.OptionsStore.Options.MaxSQPIter;
        end
        
        function value = get.OutputFcn(obj)
            value = obj.OptionsStore.Options.OutputFcn;
        end
        
        function value = get.PlotFcns(obj)
            value = obj.OptionsStore.Options.PlotFcns;
        end
        
        function value = get.RelLineSrchBnd(obj)
            value = obj.OptionsStore.Options.RelLineSrchBnd;
        end
        
        function value = get.RelLineSrchBndDuration(obj)
            value = obj.OptionsStore.Options.RelLineSrchBndDuration;
        end
        
        function value = get.TolCon(obj)
            value = obj.OptionsStore.Options.TolCon;
        end
        
        function value = get.TolConSQP(obj)
            value = obj.OptionsStore.Options.TolConSQP;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
        function value = get.TolX(obj)
            value = obj.OptionsStore.Options.TolX;
        end
        
        function value = get.TypicalX(obj)
            value = obj.OptionsStore.Options.TypicalX;
        end
        
        function value = get.NoStopIfFlatInfeas(obj)
            value = obj.OptionsStore.Options.NoStopIfFlatInfeas;
        end     

        function value = get.PhaseOneTotalScaling(obj)
            value = obj.OptionsStore.Options.PhaseOneTotalScaling;
        end     
        
    end    
    
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing all the options in a
%   field of OS called Defaults. This structure must then be passed to the
%   optim.options.generateSingleAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Fseminf.

% Define the option defaults for the solver
OS.Defaults.DerivativeCheck = 'off';
OS.Defaults.Diagnostics = 'off';
OS.Defaults.DiffMaxChange = Inf;
OS.Defaults.DiffMinChange = 0;
OS.Defaults.Display = 'final';
OS.Defaults.FinDiffRelStep = [];
OS.Defaults.FinDiffType = 'forward';
OS.Defaults.FunValCheck = 'off';
OS.Defaults.GradObj = 'off';
OS.Defaults.MaxFunEvals = '100*numberOfVariables';
OS.Defaults.MaxIter = 400;
OS.Defaults.MaxSQPIter = '10*max(numberOfVariables,numberOfInequalities+numberOfBounds)';
OS.Defaults.NoStopIfFlatInfeas = 'off';
OS.Defaults.OutputFcn = [];
OS.Defaults.PhaseOneTotalScaling = 'off';
OS.Defaults.PlotFcns = [];
OS.Defaults.RelLineSrchBnd = [];
OS.Defaults.RelLineSrchBndDuration = 1;
OS.Defaults.TolCon = 1e-6;
OS.Defaults.TolConSQP = 1e-6;
OS.Defaults.TolFun = 1e-6;
OS.Defaults.TolX = 1e-6;
OS.Defaults.TypicalX = 'ones(numberOfVariables,1)';

% Call the package function to generate the OptionsStore
OS = optim.options.generateSingleAlgorithmOptionsStore(OS);
end

