classdef (Sealed) Bintprog < optim.options.SingleAlgorithm
%

%Bintprog Options for BINTPROG
%
%   The OPTIM.OPTIONS.BINTPROG class allows the user to create a set of
%   options for the BINTPROG solver. For a list of options that can be set,
%   see the documentation for BINTPROG.
%
%   OPTS = OPTIM.OPTIONS.BINTPROG creates a set of options for BINTPROG
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.BINTPROG(PARAM, VAL, ...) creates a set of options
%   for BINTPROG with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.BINTPROG(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.SINGLEALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS

%   Copyright 2012 The MathWorks, Inc.

    properties (Dependent)
%DISPLAY Level of display
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.
        Display
        
%DIAGNOSTICS Display diagnostic information about the function
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        Diagnostics
        
%MAXITER Maximum number of iterations allowed
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        MaxIter
        
%MAXRLPITER Maximum number of iterations the LP-solver performs to solve
%           the LP-relaxation problem at each node
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        MaxRLPIter
        
%MAXNODES Maximum number of solutions, or nodes, the function searches
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        MaxNodes
        
%MAXTIME Maximum amount of CPU time in seconds the function runs
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        MaxTime
        
%NODEDISPLAYINTERVAL Node display interval
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        NodeDisplayInterval
        
%TOLXINTEGER Tolerance within which the value of a variable is considered
%            to be integral
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        TolXInteger
        
%TOLFUN Termination tolerance on the function value
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        TolFun
        
%TOLRLPFUN Termination tolerance on the function value of a linear programming relaxation problem         
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        TolRLPFun
        
%NODESEARCHSTRATEGY Strategy the algorithm uses to select the next node to
%                   search in the search tree        
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        NodeSearchStrategy
        
%BRANCHSTRATEGY Strategy the algorithm uses to select the branch variable
%               in the search tree        
%
%   For more information, type "doc bintprog" and see the "Options" section
%   in the BINTPROG documentation page.       
        BranchStrategy
    end
    
    properties (Hidden, Access = protected)

%OPTIONSSTORE Contains the option values and meta-data for the class
%
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
        
%SOLVERNAME Name of the solver that the options are intended for
%        
        SolverName = 'bintprog';
    end
    
    methods (Hidden)
        
        function obj = Bintprog(varargin)
%Bintprog Options for BINTPROG
%
%   The OPTIM.OPTIONS.BINTPROG class allows the user to create a set of
%   options for the BINTPROG solver. For a list of options that can be set,
%   see the documentation for BINTPROG.
%
%   OPTS = OPTIM.OPTIONS.BINTPROG creates a set of options for BINTPROG
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.BINTPROG(PARAM, VAL, ...) creates a set of options
%   for BINTPROG with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.BINTPROG(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.SINGLEALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS
           
            % Call the superclass constructor
            obj = obj@optim.options.SingleAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
            
        end
        
    end
    
    % Set/get methods
    methods

        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.
            obj = setProperty(obj, 'Display', value, ...
                {'none','off','iter',...
                'iter-detailed','final','final-detailed'});
        end

        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.MaxIter(obj, value)
            % MaxIter option can take a string that is equal to the default
            % string. We specify this via the fourth input of setProperty.
            obj = setProperty(obj, 'MaxIter', value, ...
                obj.OptionsStore.Defaults.MaxIter);
        end
        
        function obj = set.MaxRLPIter(obj, value)
            obj = setProperty(obj, 'MaxRLPIter', value);
        end
        
        function obj = set.MaxNodes(obj, value)
            obj = setProperty(obj, 'MaxNodes', value);
        end
        
        function obj = set.MaxTime(obj, value)
            obj = setProperty(obj, 'MaxTime', value);
        end
        
        function obj = set.NodeDisplayInterval(obj, value)
            obj = setProperty(obj, 'NodeDisplayInterval', value);
        end
        
        function obj = set.TolXInteger(obj, value)
            obj = setProperty(obj, 'TolXInteger', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function obj = set.TolRLPFun(obj, value)
            obj = setProperty(obj, 'TolRLPFun', value);
        end
        
        function obj = set.NodeSearchStrategy(obj, value)
            obj = setProperty(obj, 'NodeSearchStrategy', value);
        end
        
        function obj = set.BranchStrategy(obj, value)
            obj = setProperty(obj, 'BranchStrategy', value);
        end

        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxRLPIter(obj)
            value = obj.OptionsStore.Options.MaxRLPIter;
        end
        
        function value = get.MaxNodes(obj)
            value = obj.OptionsStore.Options.MaxNodes;
        end
        
        function value = get.MaxTime(obj)
            value = obj.OptionsStore.Options.MaxTime;
        end
        
        function value = get.NodeDisplayInterval(obj)
            value = obj.OptionsStore.Options.NodeDisplayInterval;
        end
        
        function value = get.TolXInteger(obj)
            value = obj.OptionsStore.Options.TolXInteger;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
        function value = get.TolRLPFun(obj)
            value = obj.OptionsStore.Options.TolRLPFun;
        end
        
        function value = get.NodeSearchStrategy(obj)
            value = obj.OptionsStore.Options.NodeSearchStrategy;
        end

        function value = get.BranchStrategy(obj)
            value = obj.OptionsStore.Options.BranchStrategy;
        end
        
    end
    
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing all the options in a
%   field of OS called Defaults. This structure must then be passed to the
%   optim.options.generateSingleAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Bintprog.

% Define the option defaults for the solver
OS.Defaults.Display = 'final';
OS.Defaults.Diagnostics ='off';
OS.Defaults.MaxIter = '100000*numberOfVariables';
OS.Defaults.MaxRLPIter = '100*numberOfVariables';
OS.Defaults.MaxNodes = '1000*numberOfVariables';
OS.Defaults.MaxTime = 7200;
OS.Defaults.NodeDisplayInterval = 20;
OS.Defaults.TolXInteger = 1e-8;
OS.Defaults.TolFun = 1e-3;
OS.Defaults.TolRLPFun = 1e-6;
OS.Defaults.NodeSearchStrategy = 'bn';
OS.Defaults.BranchStrategy = 'maxinfeas';

% Call the package function to generate the OptionsStore
OS = optim.options.generateSingleAlgorithmOptionsStore(OS);

end

