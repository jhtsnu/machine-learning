function options = createSolverOptions(solverName, varargin)
%

%CREATESOLVEROPTIONS Create a solver options object
%
%   OPTIONS = CREATESOLVEROPTIONS(SOLVERNAME, 'PARAM', VALUE, ...) creates
%   the specified solver options object. Any specified parameters are set
%   in the object.
%
%   See also OPTIMOPTIONS

%   Copyright 2012 The MathWorks, Inc.

switch lower(solverName)
    case 'bintprog'
        options = optim.options.Bintprog(varargin{:});
    case 'linprog'
        % Throw a more detailed error if a user tries to set old
        % options LargeScale or Simplex. Loop through the parameter names
        % and not the values.
        for i = 1:2:length(varargin)
            if ischar(varargin{i})
                if strcmpi(varargin{i}, 'LargeScale')
                    error(message('optim:options:createSolverOptions:LargeScaleUnsupported'));
                elseif strcmpi(varargin{i}, 'Simplex')
                    error(message('optim:options:createSolverOptions:SimplexUnsupported'));
                end
            end
        end
        options = optim.options.Linprog(varargin{:});
    case 'quadprog'
        options = optim.options.Quadprog(varargin{:});
    case 'fminimax'
        options = optim.options.Fminimax(varargin{:});
    case 'fmincon'
        options = optim.options.Fmincon(varargin{:});
    case 'fgoalattain'
        options = optim.options.Fgoalattain(varargin{:});
    case 'fseminf'
        options = optim.options.Fseminf(varargin{:});
    case 'lsqnonlin'
        options = optim.options.Lsqnonlin(varargin{:});
    case 'lsqcurvefit'
        options = optim.options.Lsqcurvefit(varargin{:});
    case 'fminunc'
        options = optim.options.Fminunc(varargin{:});
    case 'fsolve'
        options = optim.options.Fsolve(varargin{:});
    case 'lsqlin'
        options = optim.options.Lsqlin(varargin{:});
    case 'ktrlink'
        options = optim.options.Ktrlink(varargin{:});
    case 'pso'
        options = optim.options.Pso(varargin{:});
    case {'fminsearch', 'fzero', 'fminbnd', 'lsqnonneg'}
        upperSolver = upper(solverName);
        error(message('optim:options:createSolverOptions:MatlabSolversUnsupported', ...
            upperSolver, upperSolver));
    otherwise
        error(message('optim:options:createSolverOptions:InvalidSolver'));
end
