classdef (Sealed) Fmincon < optim.options.MultiAlgorithm
%

%Fmincon Options for FMINCON
%
%   The OPTIM.OPTIONS.FMINCON class allows the user to create a set of
%   options for the FMINCON solver. For a list of options that can be set,
%   see the documentation for FMINCON.
%
%   OPTS = OPTIM.OPTIONS.FMINCON creates a set of options for FMINCON
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.FMINCON(PARAM, VAL, ...) creates a set of options
%   for FMINCON with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.FMINCON(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS

%   Copyright 2012 The MathWorks, Inc.

    properties (Dependent)
        
%ALWAYSHONORCONSTRAINTS Determine whether bounds are satisfied at every
%                       iteration
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        AlwaysHonorConstraints
        
%DERIVATIVECHECK Compare user-supplied derivatives to finite-differencing 
%                derivatives
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        DerivativeCheck
        
%DIAGNOSTICS Display diagnostic information 
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        Diagnostics
        
%DIFFMAXCHANGE Maximum change in variables for finite-difference gradients        
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        DiffMaxChange

%DIFFMINCHANGE Minimum change in variables for finite-difference gradients        
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        DiffMinChange
        
%DISPLAY Level of display
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        Display
        
%FINDIFFRELSTEP Scalar or vector step size factor
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        FinDiffRelStep
        
%FINDIFFTYPE Finite difference type
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        FinDiffType
        
%FUNVALCHECK Check whether objective function and constraints values are
%            valid
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        FunValCheck
        
%GRADCONSTR Gradient for nonlinear constraint functions defined by the user
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        GradConstr
        
%GRADOBJ Gradient for the objective function defined by the user
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        GradObj
        
%HESSFCN Function handle to a user-supplied Hessian
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        HessFcn
        
%HESSIAN Specify whether a user-supplied Hessian will be supplied
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        Hessian
        
%HESSMULT Function handle for Hessian multiply function        
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        HessMult
        
%HESSPATTERN Sparsity pattern of the Hessian for finite differencing
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        HessPattern
        
%INITBARRIERPARAM Initial barrier value
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        InitBarrierParam
        
%INITTRUSTREGIONRADIUS Initial radius of the trust region
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        InitTrustRegionRadius
        
%MAXITER Maximum number of iterations allowed 
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        MaxIter
        
%MAXFUNEVALS Maximum number of function evaluations allowed   
%
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        MaxFunEvals
        
%MAXPCGITER Maximum number of PCG (preconditioned conjugate gradient) 
%           iterations        
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        MaxPCGIter
        
%MAXPROJCGITER A tolerance for the number of projected conjugate gradient 
%              iterations      
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        MaxProjCGIter
        
%MAXSQPITER Maximum number of SQP iterations allowed
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        MaxSQPIter
        
%OBJECTIVELIMIT Lower limit on the objective function
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        ObjectiveLimit
        
%OUTPUTFCN User-defined functions that are called at each iteration
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        OutputFcn
        
%PLOTFCNS Plots various measures of progress while the algorithm executes
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        PlotFcns
        
%PRECONDBANDWIDTH Upper bandwidth of preconditioner for PCG
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        PrecondBandWidth
        
%RELLINESRCHBND Relative bound on the line search step length
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        RelLineSrchBnd
        
%RELLINESRCHBNDDURATION Number of iterations for which the bound specified 
%                       in RelLineSrchBnd should be active
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        RelLineSrchBndDuration

%SCALEPROBLEM Determine whether all constraints and the objective function
%             are normalized
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        ScaleProblem
        
%SUBPROBLEMALGORITHM Determines how the iteration step is calculated
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        SubproblemAlgorithm
        
%TOLCON Tolerance on the constraint violation
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        TolCon
        
%TOLCONSQP Termination tolerance on inner iteration SQP constraint violation
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        TolConSQP
        
%TOLFUN Termination tolerance on the function value
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        TolFun
        
%TOLPCG Termination tolerance on the PCG iteration
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        TolPCG
        
%TOLPCG Termination tolerance on the PCG iteration
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.
        TolProjCG
        
%TOLPROJCGABS Absolute tolerance for projected conjugate gradient algorithm
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.        
        TolProjCGAbs
        
%TOLX Termination tolerance on x
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.               
        TolX
        
%TYPICALX Typical x values        
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.               
        TypicalX
        
%USEPARALLEL Estimate gradients in parallel
% 
%   For more information, type "doc fmincon" and see the "Options" section
%   in the FMINCON documentation page.               
        UseParallel
    end

    % Hidden properties
    properties (Hidden, Dependent)
%NOSTOPIFFLATINFEAS If objective appears flat, only stop if feasible
% 
        NoStopIfFlatInfeas
        
%PHASEONETOTALSCALING Scale the slack variable in phase 1 of qpsub
%         
        PhaseOneTotalScaling        
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%               
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%        
        SolverName = 'fmincon';
    end
    
    methods (Hidden)
        
        function obj = Fmincon(varargin)
%Fmincon Options for FMINCON
%
%   The OPTIM.OPTIONS.FMINCON class allows the user to create a set of
%   options for the FMINCON solver. For a list of options that can be set,
%   see the documentation for FMINCON.
%
%   OPTS = OPTIM.OPTIONS.FMINCON creates a set of options for FMINCON
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.FMINCON(PARAM, VAL, ...) creates a set of options
%   for FMINCON with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.FMINCON(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS            
            
            % Call the superclass constructor
            obj = obj@optim.options.MultiAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
        end
        
        function optionFeedback = createOptionFeedback(obj)
%createOptionFeedback Create option feedback string 
%
%   optionFeedback = createOptionFeedback(obj) creates an option feedback
%   strings that are required by the extended exit messages. OPTIONFEEDBACK
%   is a structure containing strings for the options that appear in the
%   extended exit messages. These strings indicate whether the option is at
%   its 'default' value or has been 'selected'. 

            % It is possible for a user to pass in a vector of options to
            % the solver. Silently use the first element in this array.
            obj = obj(1);
            
            % Check if TolFun is the default value
            if obj.OptionsStore.SetByUser.TolFun
                optionFeedback.TolFun = 'selected';
            else
                optionFeedback.TolFun = 'default';
            end
            
            % Check if TolX is the default value
            if obj.OptionsStore.SetByUser.TolX
                optionFeedback.TolX = 'selected';
            else
                optionFeedback.TolX = 'default';
            end
            
            % Check if MaxIter is the default value
            if obj.OptionsStore.SetByUser.MaxIter
                optionFeedback.MaxIter = 'selected';
            else
                optionFeedback.MaxIter = 'default';
            end
            
            % Check if MaxFunEvals is the default value
            if obj.OptionsStore.SetByUser.MaxFunEvals
                optionFeedback.MaxFunEvals = 'selected';
            else
                optionFeedback.MaxFunEvals = 'default';
            end
            
            % Check if TolCon is the default value
            if obj.OptionsStore.SetByUser.TolCon
                optionFeedback.TolCon = 'selected';
            else
                optionFeedback.TolCon = 'default';
            end
            
            % Check if MaxSQPIter is the default value
            if obj.OptionsStore.SetByUser.MaxSQPIter
                optionFeedback.MaxSQPIter = 'selected';
            else
                optionFeedback.MaxSQPIter = 'default';
            end
            
            % Check if ObjectiveLimit is the default value
            if obj.OptionsStore.SetByUser.ObjectiveLimit
                optionFeedback.ObjectiveLimit = 'selected';
            else
                optionFeedback.ObjectiveLimit = 'default';
            end
        end
    end
    
    % Set/get methods
    methods
        
        function obj = set.PhaseOneTotalScaling(obj, value)
            obj = setProperty(obj, 'PhaseOneTotalScaling', value);
        end
        
        function obj = set.NoStopIfFlatInfeas(obj, value)
            obj = setProperty(obj, 'NoStopIfFlatInfeas', value);
        end                
        
        function obj = set.FinDiffRelStep(obj, value)
            obj = setProperty(obj, 'FinDiffRelStep', value);
        end
        
        function obj = set.Hessian(obj, value)
            obj = setProperty(obj, 'Hessian', value);
        end
        
        function obj = set.HessMult(obj, value)
            obj = setProperty(obj, 'HessMult', value);
        end
        
        function obj = set.HessPattern(obj, value)
            obj = setProperty(obj, 'HessPattern', value);
        end
        
        function obj = set.MaxPCGIter(obj, value)
            % MaxPCGIter option can take a string that is equal to one of
            % the strings specified in the algorithm defaults. We specify
            % this via the fourth input of setProperty.
            obj = setProperty(obj, 'MaxPCGIter', value, ...
                obj.OptionsStore.AlgorithmDefaults{4}.MaxPCGIter);
        end
        
        function obj = set.PrecondBandWidth(obj, value)
            obj = setProperty(obj, 'PrecondBandWidth', value);
        end
        
        function obj = set.TolPCG(obj, value)
            obj = setProperty(obj, 'TolPCG', value);
        end
        
        function obj = set.AlwaysHonorConstraints(obj, value)
            obj = setProperty(obj, 'AlwaysHonorConstraints', value);
        end
        
        function obj = set.HessFcn(obj, value)
            obj = setProperty(obj, 'HessFcn', value);
        end
        
        function obj = set.InitBarrierParam(obj, value)
            obj = setProperty(obj, 'InitBarrierParam', value);
        end
        
        function obj = set.InitTrustRegionRadius(obj, value)
            obj = setProperty(obj, 'InitTrustRegionRadius', value);
        end
        
        function obj = set.MaxProjCGIter(obj, value)
            obj = setProperty(obj, 'MaxProjCGIter', value);
        end
        
        function obj = set.ObjectiveLimit(obj, value)
            obj = setProperty(obj, 'ObjectiveLimit', value);
        end
        
        function obj = set.ScaleProblem(obj, value)
            obj = setProperty(obj, 'ScaleProblem', value);
        end
        
        function obj = set.SubproblemAlgorithm(obj, value)
            obj = setProperty(obj, 'SubproblemAlgorithm', value);
        end
        
        function obj = set.TolProjCG(obj, value)
            obj = setProperty(obj, 'TolProjCG', value);
        end
        
        function obj = set.TolProjCGAbs(obj, value)
            obj = setProperty(obj, 'TolProjCGAbs', value);
        end
        
        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value);
        end
        
        function obj = set.MaxFunEvals(obj, value)
            % MaxFunEvals option can take a string that is equal to one of
            % the strings specified in the algorithm defaults. We specify a
            % unique list of these strings via the fourth input of
            % setProperty.
            obj = setProperty(obj, 'MaxFunEvals', value, ...
                obj.OptionsStore.AlgorithmDefaults{1}.MaxFunEvals);
        end
        
        function obj = set.TolX(obj, value)
            obj = setProperty(obj, 'TolX', value);
        end
        
        function obj = set.MaxSQPIter(obj, value)
            obj = setProperty(obj, 'MaxSQPIter', value);
        end
        
        function obj = set.RelLineSrchBnd(obj, value)
            obj = setProperty(obj, 'RelLineSrchBnd', value);
        end
        
        function obj = set.RelLineSrchBndDuration(obj, value)
            obj = setProperty(obj, 'RelLineSrchBndDuration', value);
        end
        
        function obj = set.TolConSQP(obj, value)
            obj = setProperty(obj, 'TolConSQP', value);
        end
        
        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.            
            obj = setProperty(obj, 'Display', value, ...
                {'off','none','notify','notify-detailed','final', ...
                'final-detailed','iter','iter-detailed','testing'});
        end
        
        function obj = set.DerivativeCheck(obj, value)
            obj = setProperty(obj, 'DerivativeCheck', value);
        end
        
        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.DiffMinChange(obj, value)
            obj = setProperty(obj, 'DiffMinChange', value);
        end
        
        function obj = set.DiffMaxChange(obj, value)
            obj = setProperty(obj, 'DiffMaxChange', value);
        end
        
        function obj = set.FinDiffType(obj, value)
            obj = setProperty(obj, 'FinDiffType', value);
            % If we get here, the property set has been successful and we
            % can update the OptionsStore
            if ~obj.OptionsStore.SetByUser.FinDiffRelStep
                switch value
                    case 'forward'
                        obj.OptionsStore.Options.FinDiffRelStep = sqrt(eps);
                    case 'central'
                        obj.OptionsStore.Options.FinDiffRelStep = eps^(1/3);
                end
            end
        end
        
        function obj = set.FunValCheck(obj, value)
            obj = setProperty(obj, 'FunValCheck', value);
        end
        
        function obj = set.GradConstr(obj, value)
            obj = setProperty(obj, 'GradConstr', value);
        end
        
        function obj = set.GradObj(obj, value)
            obj = setProperty(obj, 'GradObj', value);
        end
        
        function obj = set.OutputFcn(obj, value)
            obj = setProperty(obj, 'OutputFcn', value);
        end
        
        function obj = set.PlotFcns(obj, value)
            obj = setProperty(obj, 'PlotFcns', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function obj = set.TolCon(obj, value)
            obj = setProperty(obj, 'TolCon', value);
        end
        
        function obj = set.TypicalX(obj, value)
            obj = setProperty(obj, 'TypicalX', value);
        end
        
        function obj = set.UseParallel(obj, value)
            obj = setProperty(obj, 'UseParallel', value);
        end
        
        function value = get.AlwaysHonorConstraints(obj)
            value = obj.OptionsStore.Options.AlwaysHonorConstraints;
        end
        
        function value = get.DerivativeCheck(obj)
            value = obj.OptionsStore.Options.DerivativeCheck;
        end
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.DiffMaxChange(obj)
            value = obj.OptionsStore.Options.DiffMaxChange;
        end
        
        function value = get.DiffMinChange(obj)
            value = obj.OptionsStore.Options.DiffMinChange;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.FinDiffRelStep(obj)
            value = obj.OptionsStore.Options.FinDiffRelStep;
        end
        
        function value = get.FinDiffType(obj)
            value = obj.OptionsStore.Options.FinDiffType;
        end
        
        function value = get.FunValCheck(obj)
            value = obj.OptionsStore.Options.FunValCheck;
        end
        
        function value = get.GradConstr(obj)
            value = obj.OptionsStore.Options.GradConstr;
        end
        
        function value = get.GradObj(obj)
            value = obj.OptionsStore.Options.GradObj;
        end
        
        function value = get.HessFcn(obj)
            value = obj.OptionsStore.Options.HessFcn;
        end
        
        function value = get.Hessian(obj)
            value = obj.OptionsStore.Options.Hessian;
        end
        
        function value = get.HessMult(obj)
            value = obj.OptionsStore.Options.HessMult;
        end
        
        function value = get.HessPattern(obj)
            value = obj.OptionsStore.Options.HessPattern;
        end
        
        function value = get.InitBarrierParam(obj)
            value = obj.OptionsStore.Options.InitBarrierParam;
        end
        
        function value = get.InitTrustRegionRadius(obj)
            value = obj.OptionsStore.Options.InitTrustRegionRadius;
        end
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxFunEvals(obj)
            value = obj.OptionsStore.Options.MaxFunEvals;
        end
        
        function value = get.MaxPCGIter(obj)
            value = obj.OptionsStore.Options.MaxPCGIter;
        end
        
        function value = get.MaxProjCGIter(obj)
            value = obj.OptionsStore.Options.MaxProjCGIter;
        end
        
        function value = get.MaxSQPIter(obj)
            value = obj.OptionsStore.Options.MaxSQPIter;
        end
        
        function value = get.ObjectiveLimit(obj)
            value = obj.OptionsStore.Options.ObjectiveLimit;
        end
        
        function value = get.OutputFcn(obj)
            value = obj.OptionsStore.Options.OutputFcn;
        end
        
        function value = get.PlotFcns(obj)
            value = obj.OptionsStore.Options.PlotFcns;
        end
        
        function value = get.PrecondBandWidth(obj)
            value = obj.OptionsStore.Options.PrecondBandWidth;
        end
        
        function value = get.RelLineSrchBnd(obj)
            value = obj.OptionsStore.Options.RelLineSrchBnd;
        end
        
        function value = get.RelLineSrchBndDuration(obj)
            value = obj.OptionsStore.Options.RelLineSrchBndDuration;
        end
        
        function value = get.ScaleProblem(obj)
            value = obj.OptionsStore.Options.ScaleProblem;
        end
        
        function value = get.SubproblemAlgorithm(obj)
            value = obj.OptionsStore.Options.SubproblemAlgorithm;
        end
        
        function value = get.TolCon(obj)
            value = obj.OptionsStore.Options.TolCon;
        end
        
        function value = get.TolConSQP(obj)
            value = obj.OptionsStore.Options.TolConSQP;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
        function value = get.TolPCG(obj)
            value = obj.OptionsStore.Options.TolPCG;
        end
        
        function value = get.TolProjCG(obj)
            value = obj.OptionsStore.Options.TolProjCG;
        end
        
        function value = get.TolProjCGAbs(obj)
            value = obj.OptionsStore.Options.TolProjCGAbs;
        end
        
        function value = get.TolX(obj)
            value = obj.OptionsStore.Options.TolX;
        end
        
        function value = get.TypicalX(obj)
            value = obj.OptionsStore.Options.TypicalX;
        end
        
        function value = get.UseParallel(obj)
            value = obj.OptionsStore.Options.UseParallel;
        end
        
        function value = get.NoStopIfFlatInfeas(obj)
            value = obj.OptionsStore.Options.NoStopIfFlatInfeas;
        end     

        function value = get.PhaseOneTotalScaling(obj)
            value = obj.OptionsStore.Options.PhaseOneTotalScaling;
        end     
        
    end
            
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing the following fields:-
%
%   AlgorithmNames   : Cell array of algorithm names for the solver
%   DefaultAlgorithm : String containing the name of the default algorithm
%   AlgorithmDefaults: Cell array of structures. AlgorithmDefaults{i}
%                      holds a structure containing the defaults for 
%                      AlgorithmNames{i}.
%
%   This structure must then be passed to the
%   optim.options.generateMultiAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Fmincon.

% Define the algorithm names
OS.AlgorithmNames = {'active-set', 'interior-point', 'sqp', 'trust-region-reflective'};

% Define the default algorithm
OS.DefaultAlgorithm = 'trust-region-reflective';

% Define the defaults for each algorithm
% Active-set
OS.AlgorithmDefaults{1}.DerivativeCheck= 'off';
OS.AlgorithmDefaults{1}.Diagnostics= 'off';
OS.AlgorithmDefaults{1}.DiffMaxChange= Inf;
OS.AlgorithmDefaults{1}.DiffMinChange= 0;
OS.AlgorithmDefaults{1}.Display = 'final';
OS.AlgorithmDefaults{1}.FinDiffRelStep = sqrt(eps);
OS.AlgorithmDefaults{1}.FinDiffType= 'forward';
OS.AlgorithmDefaults{1}.FunValCheck= 'off';
OS.AlgorithmDefaults{1}.GradConstr= 'off';
OS.AlgorithmDefaults{1}.GradObj= 'off';
OS.AlgorithmDefaults{1}.OutputFcn= [];
OS.AlgorithmDefaults{1}.PlotFcns= [];
OS.AlgorithmDefaults{1}.TolFun= 1.0000e-06;
OS.AlgorithmDefaults{1}.TolCon= 1.0000e-06;
OS.AlgorithmDefaults{1}.TypicalX= 'ones(numberOfVariables,1)';
OS.AlgorithmDefaults{1}.UseParallel= 'never';
OS.AlgorithmDefaults{1}.MaxIter = 400;
OS.AlgorithmDefaults{1}.MaxFunEvals = '100*numberOfVariables';
OS.AlgorithmDefaults{1}.TolX = 1e-6;
OS.AlgorithmDefaults{1}.MaxSQPIter = '10*max(numberOfVariables,numberOfInequalities+numberOfBounds)';
OS.AlgorithmDefaults{1}.RelLineSrchBnd = [];
OS.AlgorithmDefaults{1}.RelLineSrchBndDuration = 1;
OS.AlgorithmDefaults{1}.TolConSQP = 1.0000e-06;
OS.AlgorithmDefaults{1}.NoStopIfFlatInfeas = 'off';
OS.AlgorithmDefaults{1}.PhaseOneTotalScaling = 'off';

% Interior-point
OS.AlgorithmDefaults{2}.DerivativeCheck= 'off';
OS.AlgorithmDefaults{2}.Diagnostics= 'off';
OS.AlgorithmDefaults{2}.DiffMaxChange= Inf;
OS.AlgorithmDefaults{2}.DiffMinChange= 0;
OS.AlgorithmDefaults{2}.Display = 'final';
OS.AlgorithmDefaults{2}.FinDiffRelStep = sqrt(eps);
OS.AlgorithmDefaults{2}.FinDiffType= 'forward';
OS.AlgorithmDefaults{2}.FunValCheck= 'off';
OS.AlgorithmDefaults{2}.GradConstr= 'off';
OS.AlgorithmDefaults{2}.GradObj= 'off';
OS.AlgorithmDefaults{2}.OutputFcn= [];
OS.AlgorithmDefaults{2}.PlotFcns= [];
OS.AlgorithmDefaults{2}.TolFun= 1.0000e-06;
OS.AlgorithmDefaults{2}.TolCon= 1.0000e-06;
OS.AlgorithmDefaults{2}.TypicalX= 'ones(numberOfVariables,1)';
OS.AlgorithmDefaults{2}.UseParallel= 'never';
OS.AlgorithmDefaults{2}.MaxIter = 1000;
OS.AlgorithmDefaults{2}.MaxFunEvals = 3000;
OS.AlgorithmDefaults{2}.TolX = 1e-10;
OS.AlgorithmDefaults{2}.AlwaysHonorConstraints= 'bounds';
OS.AlgorithmDefaults{2}.HessFcn= [];
OS.AlgorithmDefaults{2}.Hessian= 'bfgs';
OS.AlgorithmDefaults{2}.HessMult= [];
OS.AlgorithmDefaults{2}.InitBarrierParam= 0.1000;
OS.AlgorithmDefaults{2}.InitTrustRegionRadius= 'sqrt(numberOfVariables)';
OS.AlgorithmDefaults{2}.MaxProjCGIter= '2*(numberOfVariables-numberOfEqualities)';
OS.AlgorithmDefaults{2}.ObjectiveLimit= -1.0000e+20;
OS.AlgorithmDefaults{2}.ScaleProblem= 'none';
OS.AlgorithmDefaults{2}.SubproblemAlgorithm= 'ldl-factorization';
OS.AlgorithmDefaults{2}.TolProjCG = 0.0100;
OS.AlgorithmDefaults{2}.TolProjCGAbs = 1.0000e-10;

% sqp
OS.AlgorithmDefaults{3}.DerivativeCheck= 'off';
OS.AlgorithmDefaults{3}.Diagnostics= 'off';
OS.AlgorithmDefaults{3}.DiffMaxChange= Inf;
OS.AlgorithmDefaults{3}.DiffMinChange= 0;
OS.AlgorithmDefaults{3}.Display = 'final';
OS.AlgorithmDefaults{3}.FinDiffRelStep = sqrt(eps);
OS.AlgorithmDefaults{3}.FinDiffType= 'forward';
OS.AlgorithmDefaults{3}.FunValCheck= 'off';
OS.AlgorithmDefaults{3}.GradConstr= 'off';
OS.AlgorithmDefaults{3}.GradObj= 'off';
OS.AlgorithmDefaults{3}.OutputFcn= [];
OS.AlgorithmDefaults{3}.PlotFcns= [];
OS.AlgorithmDefaults{3}.TolFun= 1.0000e-06;
OS.AlgorithmDefaults{3}.TolCon= 1.0000e-06;
OS.AlgorithmDefaults{3}.TypicalX= 'ones(numberOfVariables,1)';
OS.AlgorithmDefaults{3}.UseParallel= 'never';
OS.AlgorithmDefaults{3}.MaxIter = 400;
OS.AlgorithmDefaults{3}.MaxFunEvals = '100*numberOfVariables';
OS.AlgorithmDefaults{3}.TolX = 1e-6;
OS.AlgorithmDefaults{3}.ObjectiveLimit= -1.0000e+20;
OS.AlgorithmDefaults{3}.ScaleProblem= 'none';

% trust-region-reflective
OS.AlgorithmDefaults{4}.DerivativeCheck= 'off';
OS.AlgorithmDefaults{4}.Diagnostics= 'off';
OS.AlgorithmDefaults{4}.DiffMaxChange= Inf;
OS.AlgorithmDefaults{4}.DiffMinChange= 0;
OS.AlgorithmDefaults{4}.Display = 'final';
OS.AlgorithmDefaults{4}.FinDiffRelStep = sqrt(eps);
OS.AlgorithmDefaults{4}.FinDiffType= 'forward';
OS.AlgorithmDefaults{4}.FunValCheck= 'off';
OS.AlgorithmDefaults{4}.GradConstr= 'off';
OS.AlgorithmDefaults{4}.GradObj= 'off';
OS.AlgorithmDefaults{4}.OutputFcn= [];
OS.AlgorithmDefaults{4}.PlotFcns= [];
OS.AlgorithmDefaults{4}.TolFun= 1.0000e-06;
OS.AlgorithmDefaults{4}.TolCon= 1.0000e-06;
OS.AlgorithmDefaults{4}.TypicalX= 'ones(numberOfVariables,1)';
OS.AlgorithmDefaults{4}.UseParallel= 'never';
OS.AlgorithmDefaults{4}.MaxIter = 400;
OS.AlgorithmDefaults{4}.MaxFunEvals = '100*numberOfVariables';
OS.AlgorithmDefaults{4}.TolX = 1e-6;
OS.AlgorithmDefaults{4}.Hessian = 'off';
OS.AlgorithmDefaults{4}.HessMult = [];
OS.AlgorithmDefaults{4}.HessPattern = 'sparse(ones(numberOfVariables))';
OS.AlgorithmDefaults{4}.MaxPCGIter = 'max(1,floor(numberOfVariables/2))';
OS.AlgorithmDefaults{4}.PrecondBandWidth = 0;
OS.AlgorithmDefaults{4}.TolPCG = 0.1000;

% Call the package function to generate the OptionsStore
OS = optim.options.generateMultiAlgorithmOptionsStore(OS);

end
