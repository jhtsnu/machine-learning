classdef (Abstract) SingleAlgorithm < optim.options.SolverOptions
%

%SingleAlgorithm Options for Optimization Toolbox solvers with only one
%                algorithm
%
%   SingleAlgorithm is an abstract class representing a set of options for
%   an Optimization Toolbox solver, where the solver only has one
%   algorithm. You cannot create instances of this class directly. You must
%   create an instance of a derived class such optim.options.Fgoalattain.
%
%   See also OPTIM.OPTIONS.SOLVEROPTIONS

%   Copyright 2012 The MathWorks, Inc.

    % Constructor
    methods (Hidden)
        function obj = SingleAlgorithm(varargin)
%SINGLEALGORITHM Construct a new SingleAlgorithm object
%
%   OPTS = OPTIM.OPTIONS.SINGLEALGORITHM creates a set of options with each
%   option set to its default value.
%
%   OPTS = OPTIM.OPTIONS.SINGLEALGORITHM(PARAM, VAL, ...) creates a set of
%   options with the named parameters altered with the specified values.
%
%   OPTS = OPTIM.OPTIONS.SINGLEALGORITHM(OLDOPTS, PARAM, VAL, ...) creates
%   a copy of OLDOPTS with the named parameters altered with the specified
%   value

            % Call the superclass constructor
            obj = obj@optim.options.SolverOptions(varargin{:});
               
        end
    end
    
    methods (Hidden, Access = protected)
        
        function allOptionsAtDefault = needExtraHeader(obj) 
%NEEDEXTRAHEADER Determine whether an extra header is needed
%
%   ALLOPTIONSATDEFAULT = NEEDEXTRAHEADER(OBJ) returns whether an extra
%   header is needed.
            
            allOptionsAtDefault = (numOptionsSetByUser(obj) == 0);
        end
        
        function allOptionsSetByUser = needExtraFooter(obj)
%NEEDEXTRAFOOTER Determine whether an extra footer is needed
%
%   ALLOPTIONSSETBYUSER = NEEDEXTRAFOOTER(OBJ) returns whether an extra
%   footer is needed.

            allOptionsSetByUser = (numOptionsSetByUser(obj) == length(properties(obj)));
        end
              
        function algOptions = getDisplayOptions(obj)
%GETDISPLAYOPTIONS Get the options to be displayed
%
%   ALGOPTIONS = GETDISPLAYOPTIONS(OBJ) returns a cell array of options to
%   be displayed. For solver objects that inherit from SingleAlgorithm, all
%   options are displayed.
            
            algOptions = properties(obj);
        end
        
    end
    
    methods (Hidden)
        function OptionsStruct = extractOptionsStructure(obj)
%EXTRACTOPTIONSSTRUCTURE Extract options structure from OptionsStore
%
%   OPTIONSSTRUCT = EXTRACTOPTIONSSTRUCTURE(OBJ) extracts a plain structure
%   containing the options from obj.OptionsStore. The solver calls
%   convertForSolver, which in turn calls this method to obtain a plain
%   options structure.            
            
            OptionsStruct = obj.OptionsStore.Options;
            
        end
    end
    
    methods (Hidden, Access = private)
        
        function numSetByUser = numOptionsSetByUser(obj)
%NUMOPTIONSSETBYUSER Return number of options set by user
%
%   NUMSETBYUSER = NUMOPTIONSSETBYUSER(OBJ) returns the number of options
%   that have been set by the user.
            
            % Get names of all the properties for the current algorithm
            allOptions = properties(obj);
            
            % Determine the number of properties set by the user
            numSetByUser = 0;
            for i = 1:length(allOptions)
                if obj.OptionsStore.SetByUser.(allOptions{i})
                    numSetByUser = numSetByUser + 1;
                end
            end
        end
        
    end
    
end