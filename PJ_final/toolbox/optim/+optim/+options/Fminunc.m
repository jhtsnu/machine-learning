classdef (Sealed) Fminunc < optim.options.MultiAlgorithm
%

%Fminunc Options for FMINUNC
%
%   The OPTIM.OPTIONS.FMINUNC class allows the user to create a set of
%   options for the FMINUNC solver. For a list of options that can be set,
%   see the documentation for FMINUNC.
%
%   OPTS = OPTIM.OPTIONS.FMINUNC creates a set of options for FMINUNC
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.FMINUNC(PARAM, VAL, ...) creates a set of options
%   for FMINUNC with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.FMINUNC(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS    

%   Copyright 2012 The MathWorks, Inc.
    
    properties (Dependent)
 
%DERIVATIVECHECK Compare user-supplied derivatives to finite-differencing 
%                derivatives
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        DerivativeCheck
        
%DIAGNOSTICS Display diagnostic information 
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        Diagnostics
        
%DIFFMAXCHANGE Maximum change in variables for finite-difference gradients        
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        DiffMaxChange
        
%DIFFMINCHANGE Minimum change in variables for finite-difference gradients        
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        DiffMinChange
        
%DISPLAY Level of display
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        Display
        
%FINDIFFRELSTEP Scalar or vector step size factor
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        FinDiffRelStep
        
%FINDIFFTYPE Finite difference type
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        FinDiffType
        
%FUNVALCHECK Check whether objective function and constraints values are
%            valid
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        FunValCheck
        
%GRADOBJ Gradient for the objective function defined by the user
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        GradObj
        
%HESSIAN Specify whether a user-supplied Hessian will be supplied
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        Hessian
        
%HESSMULT Function handle for Hessian multiply function        
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        HessMult
        
%HESSPATTERN Sparsity pattern of the Hessian for finite differencing
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        HessPattern
        
%HESSUPDATE Method for choosing the search direction in the Quasi-Newton algorithm        
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.       
        HessUpdate
        
%INITIALHESSTYPE Initial quasi-Newton matrix type
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.              
        InitialHessType
        
%INITIALHESSMATRIX Initial quasi-Newton matrix
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.                      
        InitialHessMatrix
        
%MAXFUNEVALS Maximum number of function evaluations allowed   
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        MaxFunEvals
        
%MAXITER Maximum number of iterations allowed 
%
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        MaxIter
        
%MAXPCGITER Maximum number of PCG (preconditioned conjugate gradient) 
%           iterations        
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        MaxPCGIter
        
%OBJECTIVELIMIT Lower limit on the objective function
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.       
        ObjectiveLimit
        
%OUTPUTFCN User-defined functions that are called at each iteration
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        OutputFcn
        
%PLOTFCNS Plots various measures of progress while the algorithm executes
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        PlotFcns
        
%PRECONDBANDWIDTH Upper bandwidth of preconditioner for PCG
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        PrecondBandWidth
        
%TOLFUN Termination tolerance on the function value
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        TolFun
        
%TOLPCG Termination tolerance on the PCG iteration
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        TolPCG
        
%TOLX Termination tolerance on x
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.         
        TolX
        
%TYPICALX Typical x values        
% 
%   For more information, type "doc fminunc" and see the "Options" section
%   in the FMINUNC documentation page.        
        TypicalX
        
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%          
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%         
        SolverName = 'fminunc';
    end       

    methods (Hidden)
        
        function obj = Fminunc(varargin)
%Fminunc Options for FMINUNC
%
%   The OPTIM.OPTIONS.FMINUNC class allows the user to create a set of
%   options for the FMINUNC solver. For a list of options that can be set,
%   see the documentation for FMINUNC.
%
%   OPTS = OPTIM.OPTIONS.FMINUNC creates a set of options for FMINUNC
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.FMINUNC(PARAM, VAL, ...) creates a set of options
%   for FMINUNC with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.FMINUNC(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS    
            
            % Call the superclass constructor
            obj = obj@optim.options.MultiAlgorithm(varargin{:});
               
            % Record the class version
            obj.Version = 1;
        end
        
        function optionFeedback = createOptionFeedback(obj)
%createOptionFeedback Create option feedback string 
%
%   optionFeedback = createOptionFeedback(obj) creates an option feedback
%   strings that are required by the extended exit messages. OPTIONFEEDBACK
%   is a structure containing strings for the options that appear in the
%   extended exit messages. These strings indicate whether the option is at
%   its 'default' value or has been 'selected'. 
           
            % It is possible for a user to pass in a vector of options to
            % the solver. Silently use the first element in this array.
            obj = obj(1);
            
            % Check if TolFun is the default value
            if obj.OptionsStore.SetByUser.TolFun
                optionFeedback.TolFun = 'selected';
            else
                optionFeedback.TolFun = 'default';
            end
            
            % Check if TolX is the default value
            if obj.OptionsStore.SetByUser.TolX
                optionFeedback.TolX = 'selected';
            else
                optionFeedback.TolX = 'default';
            end
            
            % Check if MaxIter is the default value
            if obj.OptionsStore.SetByUser.MaxIter
                optionFeedback.MaxIter = 'selected';
            else
                optionFeedback.MaxIter = 'default';
            end
            
            % Check if MaxFunEvals is the default value
            if obj.OptionsStore.SetByUser.MaxFunEvals
                optionFeedback.MaxFunEvals = 'selected';
            else
                optionFeedback.MaxFunEvals = 'default';
            end
            
            % Check if ObjectiveLimit is the default value
            if obj.OptionsStore.SetByUser.ObjectiveLimit
                optionFeedback.ObjectiveLimit = 'selected';
            else
                optionFeedback.ObjectiveLimit = 'default';
            end
        end
        
        
    end
    
    % Set/get methods
    methods

        function obj = set.HessUpdate(obj, value)
            obj = setProperty(obj, 'HessUpdate', value);
        end        
        
        function obj = set.FinDiffRelStep(obj, value)
            obj = setProperty(obj, 'FinDiffRelStep', value);
        end
        
        function obj = set.Hessian(obj, value)
            obj = setProperty(obj, 'Hessian', value);
        end
        
        function obj = set.HessMult(obj, value)
            obj = setProperty(obj, 'HessMult', value);
        end
        
        function obj = set.HessPattern(obj, value)
            obj = setProperty(obj, 'HessPattern', value);
        end
        
        function obj = set.InitialHessMatrix(obj, value)
            obj = setProperty(obj, 'InitialHessMatrix', value);
        end
        
        function obj = set.InitialHessType(obj, value)
            obj = setProperty(obj, 'InitialHessType', value);
        end        
        
        function obj = set.MaxPCGIter(obj, value)
            % MaxPCGIter option can take a string that is equal to one of
            % the strings specified in the algorithm defaults. We specify a
            % unique list of these strings via the fourth input of
            % setProperty.            
            obj = setProperty(obj, 'MaxPCGIter', value, ...
                obj.OptionsStore.AlgorithmDefaults{1}.MaxPCGIter);
        end
        
        function obj = set.PrecondBandWidth(obj, value)
            obj = setProperty(obj, 'PrecondBandWidth', value);
        end
        
        function obj = set.TolPCG(obj, value)
            obj = setProperty(obj, 'TolPCG', value);
        end
        
        function obj = set.ObjectiveLimit(obj, value)
            obj = setProperty(obj, 'ObjectiveLimit', value);
        end
        
        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value);
        end
        
        function obj = set.MaxFunEvals(obj, value)
            % MaxFunEvals option can take a string that is equal to one of
            % the strings specified in the algorithm defaults. We specify a
            % unique list of these strings via the fourth input of
            % setProperty.                        
            obj = setProperty(obj, 'MaxFunEvals', value, ...
                obj.OptionsStore.AlgorithmDefaults{1}.MaxFunEvals);
        end
        
        function obj = set.TolX(obj, value)
            obj = setProperty(obj, 'TolX', value);
        end
        
        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.                                    
            obj = setProperty(obj, 'Display', value, ...
                {'off','none','notify','notify-detailed','final', ...
                'final-detailed','iter','iter-detailed','testing'});
        end
        
        function obj = set.DerivativeCheck(obj, value)
            obj = setProperty(obj, 'DerivativeCheck', value);
        end
        
        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.DiffMinChange(obj, value)
            obj = setProperty(obj, 'DiffMinChange', value);
        end
        
        function obj = set.DiffMaxChange(obj, value)
            obj = setProperty(obj, 'DiffMaxChange', value);
        end
        
        function obj = set.FinDiffType(obj, value)
            obj = setProperty(obj, 'FinDiffType', value);
            % If we get here, the property set has been succesful and we
            % can update the OptionsStore
            if ~obj.OptionsStore.SetByUser.FinDiffRelStep
                switch value
                    case 'forward'
                        obj.OptionsStore.Options.FinDiffRelStep = sqrt(eps);
                    case 'central'
                        obj.OptionsStore.Options.FinDiffRelStep = eps^(1/3);
                end
            end
        end
        
        function obj = set.FunValCheck(obj, value)
            obj = setProperty(obj, 'FunValCheck', value);
        end
        
        function obj = set.GradObj(obj, value)
            obj = setProperty(obj, 'GradObj', value);
        end
        
        function obj = set.OutputFcn(obj, value)
            obj = setProperty(obj, 'OutputFcn', value);
        end
        
        function obj = set.PlotFcns(obj, value)
            obj = setProperty(obj, 'PlotFcns', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function obj = set.TypicalX(obj, value)
            obj = setProperty(obj, 'TypicalX', value);
        end
        
        function value = get.DerivativeCheck(obj)
            value = obj.OptionsStore.Options.DerivativeCheck;
        end
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.DiffMaxChange(obj)
            value = obj.OptionsStore.Options.DiffMaxChange;
        end
        
        function value = get.DiffMinChange(obj)
            value = obj.OptionsStore.Options.DiffMinChange;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.FinDiffRelStep(obj)
            value = obj.OptionsStore.Options.FinDiffRelStep;
        end
        
        function value = get.FinDiffType(obj)
            value = obj.OptionsStore.Options.FinDiffType;
        end
        
        function value = get.FunValCheck(obj)
            value = obj.OptionsStore.Options.FunValCheck;
        end
               
        function value = get.GradObj(obj)
            value = obj.OptionsStore.Options.GradObj;
        end
        
        function value = get.Hessian(obj)
            value = obj.OptionsStore.Options.Hessian;
        end
        
        function value = get.HessMult(obj)
            value = obj.OptionsStore.Options.HessMult;
        end
        
        function value = get.HessPattern(obj)
            value = obj.OptionsStore.Options.HessPattern;
        end
        
        function value = get.HessUpdate(obj)
            value = obj.OptionsStore.Options.HessUpdate;
        end
        
        function value = get.InitialHessMatrix(obj)
            value = obj.OptionsStore.Options.InitialHessMatrix;
        end        
        
        function value = get.InitialHessType(obj)
            value = obj.OptionsStore.Options.InitialHessType;
        end        
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxFunEvals(obj)
            value = obj.OptionsStore.Options.MaxFunEvals;
        end
        
        function value = get.MaxPCGIter(obj)
            value = obj.OptionsStore.Options.MaxPCGIter;
        end
        
        function value = get.ObjectiveLimit(obj)
            value = obj.OptionsStore.Options.ObjectiveLimit;
        end
        
        function value = get.OutputFcn(obj)
            value = obj.OptionsStore.Options.OutputFcn;
        end
        
        function value = get.PlotFcns(obj)
            value = obj.OptionsStore.Options.PlotFcns;
        end
        
        function value = get.PrecondBandWidth(obj)
            value = obj.OptionsStore.Options.PrecondBandWidth;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
        function value = get.TolPCG(obj)
            value = obj.OptionsStore.Options.TolPCG;
        end
        
        function value = get.TolX(obj)
            value = obj.OptionsStore.Options.TolX;
        end
        
        function value = get.TypicalX(obj)
            value = obj.OptionsStore.Options.TypicalX;
        end
        
    end
    
    % Hidden utility methods
    methods (Hidden)
        function OptionsStruct = extractOptionsStructure(obj)
%extractOptionsStructure Extract options structure from OptionsStore
%
%   OptionsStruct = extractOptionsStructure(obj) extracts a plain structure
%   containing the options from obj.OptionsStore. The solver calls
%   convertForSolver, which in turn calls this method to obtain a plain
%   options structure.
%
%   This method maps the Algorithm option back to LargeScale and removes
%   the Algorithm option from the extracted structure.

            % Call superclass method
            OptionsStruct = extractOptionsStructure@optim.options.MultiAlgorithm(obj);
            
            % Call method to map optimoptions onto the old optimset
            % structure
            OptionsStruct = mapOptionsToOptimset(obj, OptionsStruct);
        end
        
        function OptionsStruct = mapOptionsToOptimset(~, OptionsStruct)
%mapOptionsToOptimset Map structure to an optimset one
%
%   OptionsStruct = mapOptionsToOptimset(obj, OptionsStruct) maps the
%   specified structure to appear that it has been generated by OPTIMSET.
            
            % Current fminunc code does not support 'Algorithm' option. For now, we
            % need to translate 'Algorithm' back to 'LargeScale'. Note at this
            % point, options will not contain LargeScale option, as this has not
            % been ported over to optimoptions.            
            if strcmp(OptionsStruct.Algorithm, 'trust-region')
                OptionsStruct.LargeScale = 'on';
            else
                OptionsStruct.LargeScale = 'off';
            end
            if isfield(OptionsStruct,'Algorithm')
                OptionsStruct = rmfield(OptionsStruct, 'Algorithm');
            end
        end
        
        function [obj, OptimsetStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
%mapOptimsetToOptions Map optimset structure to optimoptions
%
%   obj = mapOptimsetToOptions(obj, OptimsetStruct) maps specified optimset
%   options, OptimsetStruct, to the equivalent options in the specified
%   optimization object, obj.
%
%   [obj, OptimsetStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
%   additionally returns an options structure modified with any conversions
%   that were performed on the options object.
            
            if isfield(OptimsetStruct,'LargeScale') && ~isempty(OptimsetStruct.LargeScale)
                if strcmp(OptimsetStruct.LargeScale, 'on')
                    obj.Algorithm = 'trust-region';
                else
                    obj.Algorithm = 'quasi-newton';
                end
                % Also modify the incoming structure.
                if nargout > 1
                    OptimsetStruct.Algorithm = obj.Algorithm;
                    OptimsetStruct = rmfield(OptimsetStruct,'LargeScale');
                end
            end
        end
        
    end
    
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing the following fields:-
%
%   AlgorithmNames   : Cell array of algorithm names for the solver
%   DefaultAlgorithm : String containing the name of the default algorithm
%   AlgorithmDefaults: Cell array of structures. AlgorithmDefaults{i}
%                      holds a structure containing the defaults for 
%                      AlgorithmNames{i}.
%
%   This structure must then be passed to the
%   optim.options.generateMultiAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Fminunc.

% Define the algorithm names
OS.AlgorithmNames = {'trust-region', 'quasi-newton'};

% Define the default algorithm
OS.DefaultAlgorithm = 'trust-region';

% Define the defaults for each algorithm       
% trust-region-reflective
OS.AlgorithmDefaults{1}.DerivativeCheck = 'off';
OS.AlgorithmDefaults{1}.Diagnostics = 'off';
OS.AlgorithmDefaults{1}.DiffMaxChange = Inf;
OS.AlgorithmDefaults{1}.DiffMinChange = 0;
OS.AlgorithmDefaults{1}.Display = 'final';
OS.AlgorithmDefaults{1}.FinDiffRelStep = sqrt(eps);
OS.AlgorithmDefaults{1}.FinDiffType = 'forward';
OS.AlgorithmDefaults{1}.FunValCheck = 'off';
OS.AlgorithmDefaults{1}.GradObj = 'off';
OS.AlgorithmDefaults{1}.Hessian = 'off';
OS.AlgorithmDefaults{1}.HessMult = [];
OS.AlgorithmDefaults{1}.HessPattern = 'sparse(ones(numberOfVariables))';
OS.AlgorithmDefaults{1}.MaxFunEvals = '100*numberOfVariables';
OS.AlgorithmDefaults{1}.MaxIter = 400;
OS.AlgorithmDefaults{1}.MaxPCGIter = 'max(1,floor(numberOfVariables/2))';
OS.AlgorithmDefaults{1}.OutputFcn= [];
OS.AlgorithmDefaults{1}.PlotFcns= [];
OS.AlgorithmDefaults{1}.PrecondBandWidth = 0;
OS.AlgorithmDefaults{1}.TolFun = 1.0000e-06;
OS.AlgorithmDefaults{1}.TolPCG = 0.1;
OS.AlgorithmDefaults{1}.TolX = 1e-6;
OS.AlgorithmDefaults{1}.TypicalX = 'ones(numberOfVariables,1)';

% line-search
OS.AlgorithmDefaults{2}.DerivativeCheck = 'off';
OS.AlgorithmDefaults{2}.Diagnostics = 'off';
OS.AlgorithmDefaults{2}.DiffMaxChange = Inf;
OS.AlgorithmDefaults{2}.DiffMinChange = 0;
OS.AlgorithmDefaults{2}.Display = 'final';
OS.AlgorithmDefaults{2}.FinDiffRelStep = sqrt(eps);
OS.AlgorithmDefaults{2}.FinDiffType = 'forward';
OS.AlgorithmDefaults{2}.FunValCheck = 'off';
OS.AlgorithmDefaults{2}.GradObj = 'off';
OS.AlgorithmDefaults{2}.HessUpdate = 'bfgs';
OS.AlgorithmDefaults{2}.InitialHessType = 'scaled-identity';
OS.AlgorithmDefaults{2}.InitialHessMatrix = [];
OS.AlgorithmDefaults{2}.MaxFunEvals = '100*numberOfVariables';
OS.AlgorithmDefaults{2}.MaxIter = 400;
OS.AlgorithmDefaults{2}.ObjectiveLimit = -1e20;
OS.AlgorithmDefaults{2}.OutputFcn= [];
OS.AlgorithmDefaults{2}.PlotFcns= [];
OS.AlgorithmDefaults{2}.TolFun = 1.0000e-06;
OS.AlgorithmDefaults{2}.TolX = 1e-6;
OS.AlgorithmDefaults{2}.TypicalX = 'ones(numberOfVariables,1)';

% Call the package function to generate the OptionsStore
OS = optim.options.generateMultiAlgorithmOptionsStore(OS);

end
