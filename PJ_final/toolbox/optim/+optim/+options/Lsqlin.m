classdef (Sealed) Lsqlin < optim.options.MultiAlgorithm
%

%Lsqlin Options for LSQLIN
%
%   The OPTIM.OPTIONS.LSQLIN class allows the user to create a set of
%   options for the LSQLIN solver. For a list of options that can be set,
%   see the documentation for LSQLIN.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN creates a set of options for LSQLIN
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(PARAM, VAL, ...) creates a set of options
%   for LSQLIN with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS   

%   Copyright 2012 The MathWorks, Inc.    
    
    properties (Dependent)
        
%DIAGNOSTICS Display diagnostic information 
%
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.
        Diagnostics
        
%DISPLAY Level of display
%
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        Display
        
%JACOBMULT Function handle for Jacobian multiply function    
%
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.
        JacobMult
        
%MAXITER Maximum number of iterations allowed 
%
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        MaxIter
        
%MAXPCGITER Maximum number of PCG (preconditioned conjugate gradient) 
%           iterations        
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        MaxPCGIter
        
%PRECONDBANDWIDTH Upper bandwidth of preconditioner for PCG
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        PrecondBandWidth
        
%TOLFUN Termination tolerance on the function value
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        TolFun
        
%TOLPCG Termination tolerance on the PCG iteration
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        TolPCG
        
%TYPICALX Typical x values        
% 
%   For more information, type "doc lsqlin" and see the "Options" section
%   in the LSQLIN documentation page.        
        TypicalX
    end
    
    properties (Hidden, Access = protected)
%OPTIONSSTORE Contains the option values and meta-data for the class
%          
        OptionsStore = createOptionsStore;
    end
    
    properties (Hidden)
%SOLVERNAME Name of the solver that the options are intended for
%          
        SolverName = 'lsqlin';
    end
    
    methods (Hidden)
        
        function obj = Lsqlin(varargin)
%Lsqlin Options for LSQLIN
%
%   The OPTIM.OPTIONS.LSQLIN class allows the user to create a set of
%   options for the LSQLIN solver. For a list of options that can be set,
%   see the documentation for LSQLIN.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN creates a set of options for LSQLIN
%   with the options set to their default values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(PARAM, VAL, ...) creates a set of options
%   for LSQLIN with the named parameters altered with the specified
%   values.
%
%   OPTS = OPTIM.OPTIONS.LSQLIN(OLDOPTS, PARAM, VAL, ...) creates a copy
%   of OLDOPTS with the named parameters altered with the specified values.
%
%   See also OPTIM.OPTIONS.MULTIALGORITHM, OPTIM.OPTIONS.SOLVEROPTIONS   
            
            % Call the superclass constructor
            obj = obj@optim.options.MultiAlgorithm(varargin{:});
            
            % Record the class version
            obj.Version = 1;
        end
        
    end
    
    % Set/get methods
    methods
        
        function obj = set.MaxPCGIter(obj, value)
            % MaxPCGIter option can take a string that is equal to one of
            % the strings specified in the algorithm defaults. We specify a
            % unique list of these strings via the fourth input of
            % setProperty.            
            obj = setProperty(obj, 'MaxPCGIter', value, ...
                obj.OptionsStore.AlgorithmDefaults{1}.MaxPCGIter);
        end
        
        function obj = set.PrecondBandWidth(obj, value)
            obj = setProperty(obj, 'PrecondBandWidth', value);
        end
        
        function obj = set.TolPCG(obj, value)
            obj = setProperty(obj, 'TolPCG', value);
        end
        
        function obj = set.MaxIter(obj, value)
            obj = setProperty(obj, 'MaxIter', value);
        end
        
        function obj = set.Display(obj, value)
            % Pass the possible values that the Display option can take via
            % the fourth input of setProperty.                                                            
            obj = setProperty(obj, 'Display', value, ...
                {'off','none','final', ...
                'final-detailed','iter','iter-detailed'});
        end
        
        function obj = set.Diagnostics(obj, value)
            obj = setProperty(obj, 'Diagnostics', value);
        end
        
        function obj = set.JacobMult(obj, value)
            obj = setProperty(obj, 'JacobMult', value);
        end
        
        function obj = set.TolFun(obj, value)
            obj = setProperty(obj, 'TolFun', value);
        end
        
        function obj = set.TypicalX(obj, value)
            obj = setProperty(obj, 'TypicalX', value);
        end
        
        function value = get.Diagnostics(obj)
            value = obj.OptionsStore.Options.Diagnostics;
        end
        
        function value = get.Display(obj)
            value = obj.OptionsStore.Options.Display;
        end
        
        function value = get.JacobMult(obj)
            value = obj.OptionsStore.Options.JacobMult;
        end
        
        function value = get.MaxIter(obj)
            value = obj.OptionsStore.Options.MaxIter;
        end
        
        function value = get.MaxPCGIter(obj)
            value = obj.OptionsStore.Options.MaxPCGIter;
        end
        
        function value = get.PrecondBandWidth(obj)
            value = obj.OptionsStore.Options.PrecondBandWidth;
        end
        
        function value = get.TolFun(obj)
            value = obj.OptionsStore.Options.TolFun;
        end
        
        function value = get.TolPCG(obj)
            value = obj.OptionsStore.Options.TolPCG;
        end
        
        function value = get.TypicalX(obj)
            value = obj.OptionsStore.Options.TypicalX;
        end
        
    end
    
    % Hidden utility methods
    methods (Hidden)
        function OptionsStruct = extractOptionsStructure(obj)
%   OptionsStruct = extractOptionsStructure(obj) extracts a plain structure
%   containing the options from obj.OptionsStore. The solver calls
%   convertForSolver, which in turn calls this method to obtain a plain
%   options structure.
%
%   This method also maps Algorithm back to the LargeScale option.
            
            % Call superclass method
            OptionsStruct = extractOptionsStructure@optim.options.MultiAlgorithm(obj);
            
            % Call method to map optimoptions onto the old optimset
            % structure
            OptionsStruct = mapOptionsToOptimset(obj, OptionsStruct);
            
        end
        
        function OptionsStruct = mapOptionsToOptimset(~, OptionsStruct)
%mapOptionsToOptimset Map structure to an optimset one
%
%   OptionsStruct = mapOptionsToOptimset(obj, OptionsStruct) maps the
%   specified structure to appear that it has been generated by OPTIMSET.
            
            % Current lsqlin code does not support 'Algorithm' option. For now, we
            % need to translate 'Algorithm' back to 'LargeScale'. Note at this
            % point, options will not contain LargeScale option, as this has not
            % been ported over to optimoptions.
            if strcmp(OptionsStruct.Algorithm, 'trust-region-reflective')
                OptionsStruct.LargeScale = 'on';
            else
                OptionsStruct.LargeScale = 'off';
            end
            if isfield(OptionsStruct,'Algorithm')
                OptionsStruct = rmfield(OptionsStruct, 'Algorithm');
            end
            
        end
        
        function [obj, OptimsetStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
%mapOptimsetToOptions Map optimset structure to optimoptions
%
%   obj = mapOptimsetToOptions(obj, OptimsetStruct) maps specified optimset
%   options, OptimsetStruct, to the equivalent options in the specified
%   optimization object, obj.
%
%   [obj, OptionsStruct] = mapOptimsetToOptions(obj, OptimsetStruct)
%   additionally returns an options structure modified with any conversions
%   that were performed on the options object.
            
            if isfield(OptimsetStruct,'LargeScale') && ~isempty(OptimsetStruct.LargeScale)
                if strcmp(OptimsetStruct.LargeScale, 'on')
                    obj.Algorithm = 'trust-region-reflective';
                else
                    obj.Algorithm = 'active-set';
                end
            end
            % Also modify the incoming structure.
            if nargout > 1
                OptimsetStruct.Algorithm = obj.Algorithm;
                OptimsetStruct = rmfield(OptimsetStruct,'LargeScale');
            end
        end
        
    end
    
end

function OS = createOptionsStore
%CREATEOPTIONSSTORE Create the OptionsStore
%
%   OS = createOptionsStore creates the OptionsStore structure. This
%   structure contains the options and meta-data for option display, e.g.
%   data determining whether an option has been set by the user. This
%   function is only called when the class is first instantiated to create
%   the OptionsStore structure in its default state. Subsequent
%   instantiations of this class pick up the default OptionsStore from the
%   MCOS class definition.
%
%   Class authors must create a structure containing the following fields:-
%
%   AlgorithmNames   : Cell array of algorithm names for the solver
%   DefaultAlgorithm : String containing the name of the default algorithm
%   AlgorithmDefaults: Cell array of structures. AlgorithmDefaults{i}
%                      holds a structure containing the defaults for 
%                      AlgorithmNames{i}.
%
%   This structure must then be passed to the
%   optim.options.generateMultiAlgorithmOptionsStore function to create
%   the full OptionsStore. See below for an example for Lsqlin.

% Define the algorithm names
OS.AlgorithmNames = {'trust-region-reflective', 'active-set'};

% Define the default algorithm
OS.DefaultAlgorithm = 'trust-region-reflective';

% Define the defaults for each algorithm
% large-scale
OS.AlgorithmDefaults{1}.Diagnostics = 'off';
OS.AlgorithmDefaults{1}.Display = 'final';
OS.AlgorithmDefaults{1}.JacobMult = [];
OS.AlgorithmDefaults{1}.MaxIter = 200;
OS.AlgorithmDefaults{1}.MaxPCGIter = 'max(1,floor(numberOfVariables/2))';
OS.AlgorithmDefaults{1}.PrecondBandWidth = 0;
OS.AlgorithmDefaults{1}.TolFun = 100*eps;
OS.AlgorithmDefaults{1}.TolPCG = 0.1;
OS.AlgorithmDefaults{1}.TypicalX = 'ones(numberOfVariables,1)';

% medium-scale
OS.AlgorithmDefaults{2}.Diagnostics = 'off';
OS.AlgorithmDefaults{2}.Display = 'final';
OS.AlgorithmDefaults{2}.MaxIter = 200;

% Call the package function to generate the OptionsStore
OS = optim.options.generateMultiAlgorithmOptionsStore(OS);

end
