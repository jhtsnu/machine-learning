function optionsStruct = optimoptions2optimset(optionsObj)
%   Create a valid optimset type structure from a optimoptions object.

%   Private to OPTIMTOOL

%   Copyright 2012 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2012/11/15 14:01:58 $

if ~isa(optionsObj,'optim.options.SolverOptions')
  optionsStruct = optionsObj;
  return;
else
  % Empty structure
  optionsStruct = struct;
end

% Copy fields from the object to the flat structure.
if ~isempty(optionsObj) && isa(optionsObj,'optim.options.SolverOptions')
  % Get fields that are different from their default value.
  fnames = getSetByUserOptions(optionsObj);
  for i = 1: length(fnames)
    optionsStruct.(fnames{i}) = optionsObj.(fnames{i});
  end
  
  % Solver specific mappings
  optionsStruct = mapOptionsToOptimset(optionsObj, optionsStruct);
  
  % The options structure inside optimtool does not use the Algorithm
  % option for linprog, but it is used by optimset. As such, the Algorithm
  % option is mapped to LargeScale and Simplex here.
  if strcmpi(optionsObj.SolverName, 'linprog')
      switch optionsObj.Algorithm
        case 'interior-point'
          optionsStruct.LargeScale = 'on';
          optionsStruct.Simplex = 'off';
        case 'active-set'
          optionsStruct.LargeScale = 'off';
          optionsStruct.Simplex = 'off';
        case 'simplex'
          optionsStruct.LargeScale = 'off';
          optionsStruct.Simplex = 'on';
      end
      if isfield(optionsStruct,'Algorithm')
        optionsStruct = rmfield(optionsStruct, 'Algorithm');
      end
  end
end

function setByUserOptions = getSetByUserOptions(obj)
temp = getOptionsStore(obj);
allOptions = fieldnames(temp.SetByUser);
setByUser = struct2cell(temp.SetByUser);
setByUserOptions = allOptions([setByUser{:}]);

