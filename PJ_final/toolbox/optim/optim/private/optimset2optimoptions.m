function [optionsObj, optionsStruct] = optimset2optimoptions(solver,optionsStruct)
%   Create a valid optimoptions object from a optimset type structure.

%   Private to OPTIMTOOL

%   Copyright 2012 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2012/11/15 14:01:59 $

% These solvers still use flat structure.
solversWithStructOpts = {'fminsearch','fzero', 'fminbnd','lsqnonneg', ...
  'patternsearch','simulannealbnd','ga','gamultiobj'};
% No work to be done for flat structure case.
if any(strcmp(solver,solversWithStructOpts))
  optionsObj = optionsStruct;
  return;
end
% Initialize the return argument for the requested solver.
optionsObj = optimoptions(solver);
if ~isempty(optionsStruct) && isa(optionsStruct,'struct')
  StructFieldNames = fieldnames(optionsStruct);
  ObjectFieldNames = fieldnames(optionsObj);
  % Copy fields from the structure to the object if they are common
  for i = 1: length(StructFieldNames)
    fname = optionsStruct.(StructFieldNames{i});
    if ~isempty(fname) && any(strcmp(StructFieldNames{i},ObjectFieldNames))
      optionsObj.(StructFieldNames{i}) = optionsStruct.(StructFieldNames{i});
    end
  end
  
  % Solver specific mappings required by optimoptions.
  [optionsObj, optionsStruct] = mapOptimsetToOptions(optionsObj, optionsStruct);
end
