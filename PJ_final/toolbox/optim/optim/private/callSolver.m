function results = callSolver(probStruct,optStruct)
%callSolver Call appropriate solver and return the results structure.
%   The input arguments 'probStruct' and 'optStruct' are problem and options structure.
%   The output 'results' is a structure containing all the outputs from solver.
%
%   Private to OPTIMTOOL

%   Copyright 2005-2012 The MathWorks, Inc.
%   $Revision: 1.1.6.10 $  $Date: 2012/10/24 04:26:25 $

solver = probStruct.solver;
% Update the problem structure 'probStruct' for the solver
probStruct = createProblemStruct(solver,[],probStruct);
% Add options to the problem structure (which is a required field)
probStruct.options = createOptionsStruct(solver,optStruct); 
% Create result structure for the solver
results = createResultsStruct(solver);
resultsFields = fieldnames(results);
numfields = length(resultsFields);
% Initialize cell array to capture the output from the solver
solverOutput = cell(1,numfields);
% Set 'optimization running' message in the GUI
startMessage = getString(message('optim:optimtool:OptimtoolStatusRunning'));
optimtoolGui = javaMethodEDT('getOptimGUI','com.mathworks.toolbox.optim.OptimGUI'); % Get a handle to the GUI
if ~isempty(optimtoolGui)
    javaMethodEDT('appendResults',optimtoolGui,startMessage);
end

% Special code to handle warnings regarding Algorithm default change (g812213)
if strcmpi(probStruct.solver,'fmincon') && strcmpi(probStruct.options.Algorithm,'trust-region-reflective')
    % Check if constraints are such that TRR can handle them
    TrrCanHandleConstraints = isempty(probStruct.nonlcon) && isempty(probStruct.Aineq) && ...
        (isempty(probStruct.Aeq) || ~isempty(probStruct.Aeq) && isempty(probStruct.lb) && isempty(probStruct.ub));
    if ~TrrCanHandleConstraints
        % TRR can't handle constraints: warn and switch to AS
                warning(message('optimlib:fmincon:SwitchingToMediumScale', ...
                    addLink( 'Choosing the Algorithm', 'choose_algorithm' )))
        probStruct.options.Algorithm = 'active-set';
    elseif ~strcmpi(probStruct.options.GradObj,'on')
        % TRR can handle constraints but no gradients provided: warn and switch to AS
        warning(message('optimlib:fmincon:SwitchingToMediumScaleBecauseNoGrad', ...
            addLink( 'Choosing the Algorithm', 'choose_algorithm' )))
        probStruct.options.Algorithm = 'active-set';
    end
end
if strcmpi(probStruct.solver,'quadprog') && strcmpi(probStruct.options.Algorithm,'trust-region-reflective')
    % Check if constraints are such that TRR can handle them
    TrrCanHandleConstraints = isempty(probStruct.Aineq) && ...
        (isempty(probStruct.Aeq) || ~isempty(probStruct.Aeq) && isempty(probStruct.lb) && isempty(probStruct.ub));
    if ~TrrCanHandleConstraints
        % TRR can't handle constraints: warn and switch to AS
        arg0 = 'Trust-region-reflective';
        arg1 = 'active-set';
        arg2 = addLink('Choosing the Algorithm','choose_algorithm');
        warning(message('optim:quadprog:SwitchToMedScale',arg0, arg1, arg2));
        probStruct.options.Algorithm = 'active-set';
    end
end
% Convert to optimoptions
probStruct.options = optimset2optimoptions(probStruct.solver,probStruct.options);
            
% Call solver and put results in the structure
[solverOutput{:}] = feval(str2func(solver),probStruct);
for i = 1:numfields
    results.(resultsFields{i}) = solverOutput{i};
end
