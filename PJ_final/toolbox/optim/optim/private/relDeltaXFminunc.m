function relDelta = relDeltaXFminunc(x,deltaX)
%relDeltaXFminunc Helper function for fminunc.
% 
% Calculates the relative displacement, as used in fminunc medium-scale
% stopping test.

%   Copyright 2011 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2012/08/21 01:09:00 $

relDelta = norm(deltaX ./ (1 + abs(x)),inf);
