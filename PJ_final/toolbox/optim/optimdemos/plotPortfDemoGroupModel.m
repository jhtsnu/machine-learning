function plotPortfDemoGroupModel(x1,x2)
% plotPortfDemoGroupModel Helper function for portfolio optimization demo 

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2012/08/21 01:09:40 $

% Create figure
figure2 = figure;

% Create axes
axes1 = axes('Parent',figure2,'XTick',[1 75 150 225]);
xlim(axes1,[1 225]);
ylim(axes1,[0 0.3]);
box(axes1,'on');
grid(axes1,'on');
hold(axes1,'all');

% Plot bars (standard model); increase width a little so that bars can be
% seen behind the group model's bars
bar(x1,'FaceColor',[0 0 1],'EdgeColor',[0 0 1],'BarWidth',2.5,...
    'Parent',axes1,...
    'DisplayName','Standard');

% Plot bars (model with group constraints)
idx = find(x2>.001); % don't plot negligiblely small elements of solution
bar(idx,x2(idx),'FaceColor',[1 0 0],'EdgeColor',[1 0 0],'BarWidth',1.5,...
    'Parent',axes1,...
    'DisplayName','With group constraints');

% Create xlabel
xlabel('Assets');
% Create ylabel
ylabel('Fraction of investment');
% Create title
title('225-asset problem')

% Create legend
legend1 = legend(axes1,'show');
set(legend1,...
    'Position',[0.454285714285709 0.714285714285715 0.417857142857143 0.1]);

% Create textbox
annotation(figure2,'textbox',...
    [0.195642857142857 0.863920537540176 0.107928571428571 0.0595238095238109],...
    'String',{'Group 1'},...
    'HorizontalAlignment','center',...
    'FitBoxToText','off',...
    'LineStyle','none');

% Create textbox
annotation(figure2,'textbox',...
    [0.465642857142857 0.863920537540176 0.107928571428571 0.0595238095238109],...
    'String',{'Group 2'},...
    'HorizontalAlignment','center',...
    'FitBoxToText','off',...
    'LineStyle','none');

% Create textbox
annotation(figure2,'textbox',...
    [0.722655844155843 0.863920537540176 0.107928571428571 0.0595238095238109],...
    'String',{'Group 3'},...
    'HorizontalAlignment','center',...
    'FitBoxToText','off',...
    'LineStyle','none');

% Create doublearrow
annotation(figure2,'doublearrow',[0.132142857142857 0.380952380952381],...
    [0.870428571428572 0.871165644171779],'Color',[0 0.498039215803146 0]);

% Create doublearrow
annotation(figure2,'doublearrow',[0.39142857142857 0.641071428571428],...
    [0.870428571428572 0.871165644171779],'Color',[0 0.498039215803146 0]);

% Create doublearrow
annotation(figure2,'doublearrow',[0.648928571428569 0.898571428571426],...
    [0.870428571428572 0.871165644171779],'Color',[0 0.498039215803146 0]);
end
