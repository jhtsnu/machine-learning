function [c,ceq] = expensive_confun(x)
%EXPENSIVE_CONFUN An expensive constraint function used in optimparfor example.

%   Copyright 2007-2012 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2012/08/21 01:09:28 $

% Simulate an expensive function by performing an expensive computation
eig(magic(450));
% Evaluate constraints
c = [1.5 + x(1)*x(2)*x(3) - x(1) - x(2) - x(4); 
     -x(1)*x(2) + x(4) - 10];
% No nonlinear equality constraints:
ceq = [];
