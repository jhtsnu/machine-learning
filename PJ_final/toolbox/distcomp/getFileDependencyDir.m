function dependencyDirectory = getFileDependencyDir
%getFileDependencyDir get the directory into which FileDependencies are written
%
% getFileDependencyDir will be removed in a future release.
% Use getAttachedFilesFolder instead.
%
% dependencyDirectory = getFileDependencyDir returns a string which is
% the local directory into which FileDependencies are written. This
% function will return an empty array if not called on a worker matlab
%
% Example:
%     % Find the current FileDependency directory 
%     ddir = getFileDependencyDir;
%     % Change to that directory to invoke an executable 
%     cdir = cd(ddir);
%     % Invoke the executable
%     [OK, output] = ]system('myexecutable');
%     % Change back to the original directory
%     cd(cdir);
%
% See also getAttachedFilesFolder, getCurrentJobmanager, getCurrentWorker, 
%          getCurrentJob, getCurrentTask, FileDependencies

%  Copyright 2006-2012 The MathWorks, Inc.

cleanup = parallel.internal.apishared.apiDeprecationMessage( 'getFileDependencyDir', 'getAttachedFilesFolder' ); %#ok<NASGU>

try
    root = distcomp.getdistcompobjectroot;
    dependencyDirectory = root.DependencyDirectory;
catch E %#ok<NASGU>
    warning('distcomp:getFileDependencyDir:InvalidState', 'Unexpected error trying to invoke getFileDependencyDir');
    dependencyDirectory = '';
end
