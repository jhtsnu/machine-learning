// Copyright 2010 The MathWorks, Inc.
// $Revision: 1.1.8.2 $   $Date: 2010/11/01 19:22:59 $

// A simple function to map co-ordinates to linear indices, with periodic
// boundary constraints. Inputs "row" and "column" can be outside the valid
// bounds, they will be wrapped. This function is similar to MATLAB's "sub2ind".
__device__ unsigned int sub2ind( int row, int col, 
                                 const unsigned int m, const unsigned int n ) {
    row = ( row <  0 ? row + m : row );
    row = ( row >= m ? row - m : row );
 
    col = ( col <  0 ? col + n : col );
    col = ( col >= n ? col - n : col );
    return col * m + row;
}

// Shared memory used to communicate between threads for stencil calculation. We
// shall treat this as a (blockDim.y + 2) x (blockDim.x + 2) array.
extern __shared__ unsigned char block[];

// One generation of the game of life. Calculates "newboard" from "board". Both
// arrays are of size m x n.
__global__ void life( unsigned char * newboard, const unsigned char * board, 
                      const unsigned int m, const unsigned int n ) {

    // Local thread co-ordinates
    const unsigned int tiy    = threadIdx.y;
    const unsigned int tix    = threadIdx.x;
    // dimensions of shared memory block
    const unsigned int blockm = 2 + blockDim.y;
    const unsigned int blockn = 2 + blockDim.x;
    // co-ords in global board. Store ix and iy as "int" because we offset them
    // and may wrap
    const int iy              = blockIdx.y * blockDim.y + tiy;
    const int ix              = blockIdx.x * blockDim.x + tix;
    const unsigned int linidx = sub2ind( iy, ix, m, n );

    // Load up the shared memory - plus ghost cells using toroidal boundary
    // conditions. 

    // First, load each threads "main" element
    block[sub2ind( tiy + 1, tix + 1, blockm, blockn )] = board[linidx];

    // Next, left and right edge ghost cells
    if ( tix == 0 || tix == blockDim.x - 1 ) {
        const int xOffset = tix == 0 ? -1 : 1;
        block[ sub2ind( tiy + 1, tix + 1 + xOffset, blockm, blockn ) ] = 
            board[ sub2ind( iy, ix + xOffset, m, n ) ];
    }

    // Top/bottom ghost cells
    if ( tiy == 0 || tiy == blockDim.y - 1 ) {
        const int yOffset = tiy == 0 ? -1 : 1;
        block[sub2ind( tiy + 1 + yOffset, tix + 1, blockm, blockn )] = 
            board[ sub2ind( iy + yOffset, ix, m, n ) ];
    }

    // Corner ghost cells
    if ( (tix == 0 || tix == blockDim.x - 1) &&
         (tiy == 0 || tiy == blockDim.y - 1) ) {
        const int xOffset = tix == 0 ? -1 : 1;
        const int yOffset = tiy == 0 ? -1 : 1;
        block[sub2ind( tiy + yOffset + 1, tix + xOffset + 1, blockm, blockn )] = 
            board[ sub2ind( iy + yOffset, ix + xOffset, m, n ) ];
    }

    // Make sure all shared memory is loaded
    __syncthreads();
    
    // Game of life stencil computation
    const int imAlive        = block[sub2ind( tiy + 1, tix + 1, blockm, blockn )];
    int liveNeighbours       = 0;
    for ( int xoff = 0; xoff <= 2; xoff++ ) {
        for ( int yoff = 0; yoff <= 2; yoff++ ) {
            if ( ! ( xoff == 1 && yoff == 1 ) ) {
                liveNeighbours += block[sub2ind( tiy + yoff, tix + xoff, blockm, blockn )];
            }
        }
    }
    
    // Finally, set the element of "newboard".
    if ( ix < n && iy < m ) {
        newboard[linidx] = ( imAlive && ( liveNeighbours == 2 ) ) ||
            ( liveNeighbours == 3 );
    }
}
