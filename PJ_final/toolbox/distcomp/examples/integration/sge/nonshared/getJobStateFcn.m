function state = getJobStateFcn(cluster, job, state)
%GETJOBSTATEFCN Gets the state of a job from SGE
%
% Set your cluster's GetJobStateFcn to this function using the following
% command:
%     set(cluster, 'GetJobStateFcn', @getJobStateFcn);

% Copyright 2010-2012 The MathWorks, Inc.

% Store the current filename for the errors, warnings and dctSchedulerMessages
currFilename = mfilename;
if ~isa(cluster, 'parallel.Cluster')
    error('parallelexamples:GenericSGE:SubmitFcnError', ...
        'The function %s is for use with clusters created using the parcluster command.', currFilename)
end
if cluster.HasSharedFilesystem
    error('parallelexamples:GenericSGE:SubmitFcnError', ...
        'The submit function %s is for use with nonshared filesystems.', currFilename)
end

 % Get the information about the actual cluster used
data = cluster.getJobClusterData(job);
if isempty(data)
    % This indicates that the job has not been submitted, so just return
    dctSchedulerMessage(1, '%s: Job cluster data was empty for job with ID %d.', currFilename, job.ID);
    return
end
try
    hasDoneLastMirror = data.HasDoneLastMirror;
catch err
    ex = MException('parallelexamples:GenericSGE:FailedToRetrieveRemoteParameters', ...
        'Failed to retrieve remote parameters from the job cluster data.');
    ex = ex.addCause(err);
    throw(ex);
end
% Shortcut if the job state is already finished or failed
jobInTerminalState = strcmp(state, 'finished') || strcmp(state, 'failed');
% and we have already done the last mirror
if jobInTerminalState && hasDoneLastMirror
    return;
end
try
    clusterHost = data.RemoteHost;
    remoteJobStorageLocation = data.RemoteJobStorageLocation;
catch err
    ex = MException('parallelexamples:GenericSGE:FailedToRetrieveRemoteParameters', ...
        'Failed to retrieve remote parameters from the job cluster data.');
    ex = ex.addCause(err);
    throw(ex);
end
remoteConnection = getRemoteConnection(cluster, clusterHost, remoteJobStorageLocation);
try
    jobIDs = data.ClusterJobIDs;
catch err
    ex = MException('parallelexamples:GenericSGE:FailedToRetrieveJobID', ...
        'Failed to retrieve clusters''s job IDs from the job cluster data.');
    ex = ex.addCause(err);
    throw(ex);
end
  
% Get the full xml from qstat so that we can look for 
% <job_list state="pending">
% <job_list state="running">
commandToRun = sprintf('qstat -xml');
dctSchedulerMessage(4, '%s: Querying cluster for job state using command:\n\t%s', currFilename, commandToRun);

try
    % We will ignore the status returned from the state command because
    % a non-zero status is returned if the job no longer exists
    % Execute the command on the remote host.
    [~, cmdOut] = remoteConnection.runCommand(commandToRun);
catch err
    ex = MException('parallelexamples:GenericSGE:FailedToGetJobState', ...
        'Failed to get job state from cluster.');
    ex = ex.addCause(err);
    throw(ex);
end

% write the xml in cmdOut to a temp file first so that we can use xmlread
filename = tempname;
fid = fopen(filename, 'w');
% If we couldn't write to the file, then just return state unknown
if fid < 0
    state = 'unknown';
    return;
end
fprintf(fid, cmdOut);
fclose(fid);
clusterState = iExtractJobState(filename, jobIDs);
dctSchedulerMessage(6, '%s: State %s was extracted from cluster output:\n', currFilename, clusterState);


% If we could determine the cluster's state, we'll use that, otherwise
% stick with MATLAB's job state.
if ~strcmp(clusterState, 'unknown')
    state = clusterState;
end
% Decide what to do with mirroring based on the cluster's version of job state and whether or not
% the job is currently being mirrored:
% If job is not being mirrored, and job is not finished, resume the mirror
% If job is not being mirrored, and job is finished, do the last mirror
% If the job is being mirrored, and job is finished, do the last mirror.
% Otherwise (if job is not finished, and we are mirroring), do nothing
isBeingMirrored = remoteConnection.isJobUsingConnection(job.ID);
isJobFinished = strcmp(state, 'finished') || strcmp(state, 'failed');
if ~isBeingMirrored && ~isJobFinished
    % resume the mirror
    dctSchedulerMessage(4, '%s: Resuming mirror for job %d.', currFilename, job.ID);
    try
        remoteConnection.resumeMirrorForJob(job);
    catch err
        warning('parallelexamples:GenericSGE:FailedToResumeMirrorForJob', ...
            'Failed to resume mirror for job %d.  Your local job files may not be up-to-date.\nReason: %s', ...
            err.getReport);
    end
elseif isJobFinished
    dctSchedulerMessage(4, '%s: Doing last mirror for job %d.', currFilename, job.ID);
    try
        remoteConnection.doLastMirrorForJob(job);
        % Store the fact that we have done the last mirror so we can shortcut in the future
        data.HasDoneLastMirror = true;
        cluster.setJobClusterData(job, data);
    catch err
        warning('parallelexamples:GenericSGE:FailedToDoFinalMirrorForJob', ...
            'Failed to do last mirror for job %d.  Your local job files may not be up-to-date.\nReason: %s', ...
            err.getReport);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function state = iExtractJobState(filename, requestedJobIDs)
% Function to extract the job state for the requested jobs from the 
% output of qstat -xml

% use xmlread to read in the file
try
    xmlFileDOM = xmlread(filename);
catch err
    currFilename = mfilename;
    dctSchedulerMessage(4, '%s: Error while parsing the output of ''qstat -xml'':\n\n%s\n\n', currFilename, err.getReport());
    % Try to log the contents of the file that contains the job state
    try
        fid = fopen(filename, 'r'); 
        cleanupObj = onCleanup(@() fclose(fid));
        fileContents = fread(fid, inf, '*char');
        dctSchedulerMessage(4, '%s: Contents of file ''%s'': \n\n%s\n\n', currFilename, filename, fileContents);
    catch err2 %#ok<NASGU>
        % It is OK if we can't log the contents of the file.
    end
    warning('parallelexamples:GenericSGE:FailedToReadQstatXmlFile', ...
        ['Unable to retrieve job state. Try again in 60 seconds. ', ...
        'If the issue persists contact MathWorks Technical Support.']);
    state = 'unknown';
    return
end
% now delete the temporary file
delete(filename);

% We expect the XML to be in the following format:
%  <queue_info>
%    <job_list state="running">
%      <JB_job_number>535</JB_job_number>
%      <JAT_prio>0.55500</JAT_prio>
%      <JB_name>Job1.1</JB_name>
%      <JB_owner>elwinc</JB_owner>
%      <state>r</state>
%      <JAT_start_time>2010-01-28T05:56:27</JAT_start_time>
%      <queue_name>all.q@dct13glnxa64.mathworks.com</queue_name>
%      <slots>1</slots>
%    </job_list>
%
% Find the correct JB_job_number node and get the parent to find out 
% the job's state
% the job's state
allJobNodes = xmlFileDOM.getElementsByTagName('JB_job_number');

numJobsFound = allJobNodes.getLength();
if numJobsFound == 0
    % No jobs in the qstat output, so the one we are interested in
    % must be finished
    state = 'finished';
    return;
end

stateParseError = MException('parallelexamples:GenericSGE:StateParseError', ...
    'Failed to parse XML output from qstat.  Could not find "state" attribute for job_list node.');

for ii = 0:allJobNodes.getLength() - 1
    jobNode = allJobNodes.item(ii);
    jobNumber = str2double(jobNode.getFirstChild.getData);
    
    isEqualFcn = @(x) isequal(jobNumber, x);
    if any(cellfun(isEqualFcn, requestedJobIDs))
        % Only get the parent node if the current node is a
        % job that we are interested in.
        jobParentNode = jobNode.getParentNode;
        % We were expecting the state attribute
        if ~jobParentNode.hasAttributes
            throw(stateParseError);
        end
        
        stateAttribute = jobParentNode.getAttributes.getNamedItem('state');
        if isempty(stateAttribute)
            throw(stateParseError);
        end
        
        % We've got the state for the job that we are interested in.
        jobState = char(stateAttribute.getValue);
        if strcmpi(jobState, 'running');
            % If any of the requested jobs are running, then the whole
            % job is still running, so we can stop searching and just
            %return with state running
            state = 'running';
            return;
        end
    end
end

state = 'unknown';
