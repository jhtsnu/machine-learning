Copyright 2010 The MathWorks, Inc.
$Revision: 1.1.6.1 $  $Date: 2011/10/11 15:43:42 $

This folder contains a number of files to allow Parallel Computing Toolbox
to be used with PBS via the generic scheduler interface.

The files in this folder assume that the client and cluster share a file system
and that the client is able to submit directly to the cluster using the
qsub command.

Note that all the files in this directory will work only for clusters that are 
running on UNIX.

Instructions for Use
=====================

   
On the Client Host
------------------
1. The files in
$MATLABROOT/toolbox/distcomp/examples/integration/pbs/shared
must be present on the MATLAB path. Copy them to $MATLABROOT/toolbox/local or modify
the MATLAB path from within MATLAB.


2. Read the documentation for using the generic scheduler interface with
the Parallel Computing Toolbox and familiarize yourself with the different 
properties that can be set for a generic scheduler.

In the MATLAB Client
--------------------
1. Create a generic scheduler object for your cluster.  For distributed jobs, 
you must use distributedSubmitFcn as your submit function.  For parallel jobs, you must 
use parallelSubmitFcn as your submit function.  


Example:
sched = findResource('scheduler', 'type', 'generic');
set(sched, 'ClusterMatlabRoot', '/apps/matlab');
set(sched, 'ClusterOsType', 'unix');
% Use a folder that both the client and cluster can access
% as the DataLocation.  If your cluster and client use 
% different operating systems, you should specify DataLocation 
% to be a structure.  Refer to the documentation on 
% genericscheduler for more information.
set(sched, 'DataLocation', '/home/DATA_LOCATION');
set(sched, 'HasSharedFilesystem', true);

set(sched, 'SubmitFcn', @distributedSubmitFcn);
% If you want to run parallel jobs (including matlabpool), you must specify a ParallelSubmitFcn
set(sched, 'ParallelSubmitFcn', @parallelSubmitFcn);
set(sched, 'GetJobStateFcn', @getJobStateFcn);
set(sched, 'DestroyJobFcn', @destroyJobFcn);


2. Create a job and some tasks, submit the job, and wait for it to finish before
getting the results. Do the same for parallel jobs if so desired.  



As an alternative to these steps, create a parallel configuration that defines the 
appropriate properties and run configuration validation to verify that the configuration
works correctly.


Description of Files
====================
For more detail about these files, please refer to the help and comments contained in the 
files themselves.

MATLAB Functions Required for genericscheduler 
----------------------------------------------
distributedSubmitFcn.m
    Submit function for distributed jobs.  Use this as the SubmitFcn for your genericscheduler object.
parallelSubmitFcn.m
    Submit function for parallel jobs.  Use this as the ParallelSubmitFcn for your genericscheduler object.
destroyJobFcn.m 
    Destroy a job on the scheduler.  Use this as the DestroyJobFcn for your genericscheduler object.
getJobStateFcn.m
    Get the job state from the scheduler.  Use this as the GetJobStateFcn for your genericscheduler object.

Other MATLAB Functions
-----------------------
extractJobId.m
    Get the scheduler's job ID from the submission output.
getSubmitString.m
    Get the submission string for the scheduler.


Executable Scripts
-------------------
distributedJobWrapper.sh
    Script used by the scheduler to launch the MATLAB worker processes for distributed jobs.
parallelJobWrapper.sh
    Script used by the scheduler to launch the MATLAB worker processes for parallel jobs.


Optional Customizations
========================
The code customizations listed in this section are clearly marked in the relevant files.

distributedSubmitFcn.m
----------------------
distributedSubmitFcn provides the ability to supply additional submit arguments to the 
qsub command.  You may wish to modify the additionalSubmitArgs variable to include additional
submit arguments that are appropriate to your cluster.  For more information, refer to the 
qsub documentation provided with your scheduler.
 
parallelSubmitFcn.m
--------------------
parallelSubmitFcn calculates the number of nodes to request from the scheduler from the 
MaximumNumberOfWorkers property of the parallel job.  You may wish to customize the number of
nodes requested to suit your cluster's requirements.

parallelSubmitFcn provides the ability to supply additional submit arguments to the 
qsub command.  You may wish to modify the additionalSubmitArgs variable to include additional
submit arguments that are appropriate to your cluster.  For more information, refer to the 
qsub documentation provided with your scheduler.

parallelJobWrapper.sh
---------------------
parallelJobWrapper.sh uses the StrictHostKeyChecking=no and UserKnownHostsFile=/dev/null options
for ssh.  You may wish to customize the ssh options to suit your cluster's requirements.  For 
more information, refer to your operating system the ssh documentation.





