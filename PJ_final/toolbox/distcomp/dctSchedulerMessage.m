function dctSchedulerMessage(levelNum, messageString, varargin)
; %#ok Undocumented
%DCTSCHEDULERMESSAGE sends a message to the scheduler
%
%  DCTSCHEDULERMESSAGE(MESSAGE_STRING)
%

%  Copyright 2005-2012 The MathWorks, Inc.


if nargin < 1
    levelNum = 0;
end

if nargin < 2 || ~iIsValidLogLevel(levelNum) || ~ischar(messageString)
    warning(message('parallel:cluster:DctSchedulerMessageInvalidInputs')); 
    messageString = 'WARNING - The inputs to dctSchedulerMessage must be an integer value and a string.';
elseif nargin > 2
    try
        messageString = sprintf(messageString, varargin{:});
    catch err
        % Use unformatted messageString if sprintf fails
        warning(message('parallel:cluster:DctSchedulerMessageBadFormatString', err.message));
    end
end

try 
    dctSchedulerMessageHandler(messageString, levelNum);
catch err %#ok<NASGU>
    % Do Nothing - It's OK
end

end



function valid = iIsValidLogLevel(value)
    % Check that the log level is a 1x1 integer.
    valid = (isnumeric(value) &&  (numel(value) == 1) ...
        && (uint16(value) == value));
end
