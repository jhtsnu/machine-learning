function oldHandlerFcn = setSchedulerMessageHandler(handlerFcn)
; %#ok Undocumented
%SETSCHEDULERMESSAGEHANDLER set the default message handler on an engine
%
%  OLD_HANDLER_FCN = SETSCHEDULERMESSAGEHANDLER(HANDLER_FCN)
%
% Where HANDLER_FCN is a function handle to a function which takes a string
% input and logs the message for the particular scheduler. To deliver a
% message to the scheduler you should call dctSchedulerMessage.
% The old handler function is returned.

%  Copyright 2005-2012 The MathWorks, Inc.




if nargin == 1 && (isa(handlerFcn, 'function_handle') || isempty(handlerFcn))
    % Set the actual message handler in the private function
    oldHandlerFcn = dctSchedulerMessageHandler(handlerFcn);
else
    error(message('parallel:cluster:MessageHandlerNotFunctionHandle')); 
end

