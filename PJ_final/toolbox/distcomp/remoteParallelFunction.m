function [ERROR, out] = remoteParallelFunction(init, data, isFinalInterval)
; %#ok Undocumented

% Copyright 2007-2012 The MathWorks, Inc.

persistent channel;
mlock;

try
    notFound = false;
    % Channel construction data supplied
    if ~isempty( init ) && init(1).get.capacity() > 0
        
        % Make sure that numlabs and labindex are 1 during a parfor, and that
        % Matlab files are re-interpreted as necessary.
        pctPreRemoteEvaluation( 'mpi_mi' );
        
        % If there are 2 inputs then the first represents a serialized
        % cell array that gives the relevant information to reconstruct
        % the channel - the form is @func = {@func, args}
        [C, notFound] = iDeserializeByteBuffer(init);
        channel = feval(C{1}, C{2:end});
    end

    % Iterate data supplied
    if ~isempty(data) && data(1).get.capacity() > 0
        channelArgs = parallel.internal.pool.deserialize(data);
        out = parallel.internal.pool.serialize(feval(channel, channelArgs{:}));
    end
    
    % Indicate that no error occurred
    ERROR = false;
catch err
    ERROR = true;
    if notFound
        % Build the Source Code Not Available error and throw it so that we get
        % some error stack associated with it.
        try
            sourceCodeErr = MException(message('parallel:lang:parfor:SourceCodeNotAvailable'));
            sourceCodeErr = addCause(sourceCodeErr, err);
            throw(sourceCodeErr);
        catch theErr
        end
    else
        theErr = err;
    end
    stackToIgnore = 'parallel_function';
    errToSerialize = ParallelException.hBuildFromRemoteException(theErr, stackToIgnore);
    out = parallel.internal.pool.serialize(errToSerialize);
end

if isFinalInterval || ERROR
    channel = [];
    % Reset the MPI layer after a parfor
    dctRegisterMpiFunctions('mwmpi');
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function [data, functionNotFound] = iDeserializeByteBuffer(buffer)
% Make sure that we turn off the UnresolvedFunctionHandle warning whilst
% loading data that is likely to throw this warning. Also reset the
% lastwarn to nothing so that we know the deserialization actually threw
% this issue
state = warning('off', 'MATLAB:dispatcher:UnresolvedFunctionHandle');
try
    [lastMsg, lastID] = lastwarn('');
    % Deserialize the byte buffer
    data = parallel.internal.pool.deserialize(buffer);
    % Check lastwarn to see if the function was not found?
    [~, anID] = lastwarn;
    functionNotFound = strcmp(anID, 'MATLAB:dispatcher:UnresolvedFunctionHandle');
    % Reset the lastwarn and warning state 
    lastwarn(lastMsg, lastID);
catch E
    warning(state);
    rethrow(E);
end
warning(state);
