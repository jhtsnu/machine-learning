function currentJob = getCurrentJob
%getCurrentJob Get the job to which the current task belongs
%
%    job = getCurrentJob returns a reference to the job that contains the
%    current task. This function will return an empty array if executed in a
%    MATLAB session that is not a worker.  This function can be used to access
%    the job data associated with a number of tasks.
%
%    Example:
%    % Find the current job
%    job = getCurrentJob;
%    % Get job data
%    jobData = get(job, 'JobData');
%
% See also findResource, getCurrentJobmanager, getCurrentWorker, getCurrentTask

%  Copyright 2000-2012 The MathWorks, Inc.

try
    root = distcomp.getdistcompobjectroot;
    currentJob = root.CurrentJob;
catch err %#ok<NASGU>
    warning(message('parallel:cluster:GetCurrentJobFailed'));
    currentJob = [];
end
