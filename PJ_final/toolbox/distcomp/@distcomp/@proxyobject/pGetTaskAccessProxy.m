function taskAccessProxy = pGetTaskAccessProxy(obj)
; %#ok Undocumented
%pGetTaskAccessProxy 
%
%  proxy = pGetTaskAccessProxy(jm)

%  Copyright 2005-2012 The MathWorks, Inc.


try
    taskAccessProxy = obj.up.pGetTaskAccessProxy;
catch %#ok<CTCH> swallow errors
    taskAccessProxy = [];
end
