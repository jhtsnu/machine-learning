function info = pGetDataStoreInfo(obj)
; %#ok Undocumented

%   Copyright 2011 The MathWorks, Inc.

info = struct('DataStoreSize', NaN, 'DataStoreExportPort', NaN);
proxy = obj.ProxyObject;
if ~isempty(proxy)
    try
        info.DataStoreSize = proxy.getDataStoreSize;
        info.DataStoreExportPort = proxy.getDataStoreExportPort;
    catch err
        distcomp.handleJavaException(obj, err);
    end
end
