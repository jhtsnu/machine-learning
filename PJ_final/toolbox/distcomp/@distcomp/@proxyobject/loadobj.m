function obj = loadobj(~)
; %#ok Undocumented
%loadobj
%
%  obj = loadobj(obj)

%  Copyright 2007-2012 The MathWorks, Inc.


obj = handle(-1);
