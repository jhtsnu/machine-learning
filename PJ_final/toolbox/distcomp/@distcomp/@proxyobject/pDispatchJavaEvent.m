function pDispatchJavaEvent(~, ~, ~)
; %#ok Undocumented
%pDispatchJavaEvent private function to dispatch java events to udd objects
%
%  

%  Copyright 2000-2012 The MathWorks, Inc.


warning('distcomp:proxyobject:InvalidCall', 'This method should have been overloaded by derived classes');
