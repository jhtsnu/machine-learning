function [primary, extras] = mpiLibConfs( option )
%MPILIBCONFS - internal switch yard to pick MPI implementation

% Copyright 2009-2012 The MathWorks, Inc.
%    

narginchk(1, 1);

if strcmp( option, 'default' )
    % Different default for MAC because ssm not available.
    if ismac
        option = 'sock';
    elseif isunix
        option = 'nem';
    else
        option = 'ssm';
    end
end

switch option
  case { 'ssm' }
    if ismac
        error(message('parallel:cluster:NoSsmMPILibraryOnMac'));
    end
    [primary, extras] = iMpich( 'ssm' );
  case { 'nem' }
    if ismac || ispc
        error(message('parallel:cluster:NoNemMPILibraryOnPlatform'));
    end
    [primary, extras] = iMpich( 'nem' );
  case { 'sock' }
    [primary, extras] = iMpich( 'sock' );
  case 'msmpi'
    [primary, extras] = iMSMPI();
  otherwise
    error(message('parallel:cluster:UnknownMPILibraryOption', option));
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [primary, extras] = iMSMPI()
if strcmp( computer, 'PCWIN64' )
    primary = 'msmpi.dll';
    extras  = {};
else
    error(message('parallel:cluster:NoMSMPISupportForComputer', computer));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [primary, extras] = iMpich( variant )
extras = {};
% MPICH-specific port configuration
iPortConfiguration;

switch computer
  case {'PCWIN', 'PCWIN64'}
    switch variant
      case 'sock'
        primary = 'mpich2.dll';
      case 'ssm'
        primary = 'mpich2ssm.dll';
      otherwise
        error(message('parallel:internal:cluster:UnknownMPIVariant', variant));
    end
  case {'GLNXA64', 'GLNX86'}
    primary = sprintf( 'libmpich%s.so', variant );
  case {'MAC', 'MACI', 'MACI64'}
    extras  = {sprintf( 'libmpich%s.dylib', variant )};
    primary = sprintf( 'libpmpich%s.dylib', variant );
  otherwise
    error(message('parallel:cluster:NoMPISupportForComputer', computer));
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iPortConfiguration - set up MPICH_PORT_RANGE based on our BASE_PORT
% environment variable.
function iPortConfiguration

base_port = str2double( getenv( 'BASE_PORT' ) );
if ~isnan( base_port )
    % MPI Communications start at BASE_PORT+1000, and we leave ourselves another
    % 1000 ports. If there are more than 1000 workers on a single machine,
    % MPI may therefore fail to open a port.
    bottom_port = base_port + 1000; 
    top_port = base_port + 2000;
    % format the env var for MPICH
    port_str = sprintf( '%d:%d', bottom_port, top_port );
    setenv( 'MPICH_PORT_RANGE', port_str );
end
end
