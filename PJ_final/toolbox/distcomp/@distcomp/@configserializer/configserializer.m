function obj = configserializer() %#ok<STOUT> method errors unconditionally
; %#ok Undocumented

%   Copyright 2007-2012 The MathWorks, Inc.
    
error('distcomp:configserializer:InvalidConstruction', ...
      ['Call the static methods on distcomp.configserializer instead \n', ...
       'of instantiating the object.']);
