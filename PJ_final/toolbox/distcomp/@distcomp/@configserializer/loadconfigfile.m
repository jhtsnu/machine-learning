function [name, values] = loadconfigfile( filename )
; %#ok Undocumented
%Loads and verifies a configuration from the specified file.
%

%  Copyright 2009-2011 The MathWorks, Inc.

import parallel.internal.apishared.ConfigurationsUtils
[name, values] = ConfigurationsUtils.loadConfigurationFile(filename);
