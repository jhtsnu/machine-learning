function modified = pMaintainCacheInvariants(obj)
; %#ok Undocumented
%Maintain configuration invariants.
%   Return true if and only if we modified obj.Cache.

%   Copyright 2007-2010 The MathWorks, Inc.

% Invariants:
% - Configuration names are non-empty strings.
% - The local configuration exists
% - The current configuration is set
% - The current configuration exists
% - The configuration names are unique

% Define 2 variables updated and used in a number of checks
modified = false;
allNames = {};

% These are used if any problems are found with the data in the cache
defaultConfigurations = struct( 'Name', 'local', 'Values', {{}} );
defaultCurrent = 'local';

% The invariants are currently the same whether we are deployed or not
nEnsureNotEmpty( defaultConfigurations );
nEnsureValidConfigNames( defaultConfigurations );
nEnsureLocalConfigExists;
nEnsureValidCurrentConfig( defaultCurrent );

    function nEnsureNotEmpty( defaultConfigStruct )
        % Make sure that we have at least one configuration.
        % If empty, uses input as new struct.
        if isempty(obj.Cache.configurations)
            % The user doesn't have any configurations.
            obj.Cache.configurations = defaultConfigStruct;
            modified = true;
        end
    end

    function nEnsureValidConfigNames( defaultConfigStruct )
        % Check configuration names:
        % - can be read (resets to passed in struct if can't)
        % - are strings (removes if not)
        % - not empty   (removes if empty)
        % - are unique  (removes duplicates)
        
        % Make sure the configuration names can be read.
        try
            allNames = {obj.Cache.configurations.Name};
        catch err %#ok<NASGU>
            % This means that the configurations were so broken that we
            % couldn't collect the names into a cell array.  We erase them all!
            warning('distcomp:configuration:InvalidConfigurations', ...
                ['Cannot read the configuration names. ',...
                'Resetting configurations.']);
            obj.Cache.configurations = defaultConfigStruct;
            allNames = {obj.Cache.configurations.Name};
            modified = true;
        end

        % Make sure the configuration names are all strings.
        ind = ~cellfun(@iIsString, allNames);
        if any(ind)
            warning('distcomp:configuration:InvalidConfigurations', ...
                'Removing %d configuration(s) with invalid names.', nnz(ind));
            obj.Cache.configurations(ind) = [];
            allNames = {obj.Cache.configurations.Name};
        end

        % Make sure that the configuration names are non-empty.
        ind = cellfun(@isempty, allNames);
        if any(ind)
            warning('distcomp:configuration:EmptyName', ...
                'Found a configuration with an empty name.  Removing it.');
            obj.Cache.configurations(ind) = [];
            allNames = {obj.Cache.configurations.Name};
            modified = true;
        end

        % Make sure that the configuration names are unique.
        [uniqueNames, ind] = unique(allNames);
        if length(uniqueNames) ~= length(allNames)
            warning('distcomp:configuration:DuplicateNames', ...
                ['There are duplicates in the configuration names.  ', ...
                'Removing the duplicates.']);
            obj.Cache.configurations = obj.Cache.configurations(ind);
            modified = true;
        end
    end

    function nEnsureLocalConfigExists()
        % Make sure we have the local configuration.
        if ~any(strcmp(allNames, 'local'))
            obj.Cache.configurations(end + 1).Name = 'local';
            obj.Cache.configurations(end).Values = {};
            allNames = {obj.Cache.configurations.Name};
            modified = true;
        end
    end

    function nEnsureValidCurrentConfig( defaultCurrentConfig )
        % Checks current configuration
        % - is a string
        % - exists
        % and sets it to passed in default if not.

        % Make sure the current configuration is set to a string value.
        try
            current = obj.Cache.current;
            if ~iIsString(current)
                obj.Cache.current = defaultCurrentConfig;
                modified = true;
            end
        catch err %#ok<NASGU>
            % There was no current field in the cache.
            obj.Cache.current = defaultCurrentConfig;
            modified = true;
        end

        % Make sure that the current configuration
        % exists.
        if ~any(strcmp(allNames, obj.Cache.current))
            obj.Cache.current = defaultCurrentConfig;
            modified = true;
        end
    end
end

% Define helper function used in a number of checks
function out = iIsString(str)
out = ischar(str) && size(str, 2) == length(str);
end
