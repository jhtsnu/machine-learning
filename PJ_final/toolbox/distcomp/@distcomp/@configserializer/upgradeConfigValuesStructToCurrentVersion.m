function valuesStruct = upgradeConfigValuesStructToCurrentVersion(valuesStruct, originalVersionNum)
; %#ok Undocumented
% Static method to upgrade a configuration structure from its original version to the 
% current version of the toolbox.  See pSetFromStruct to get a vague idea of the 
% format of the structure.

%  Copyright 2009-2011 The MathWorks, Inc.

import parallel.internal.apishared.ConfigurationsUtils

valuesStruct = ConfigurationsUtils.upgradeConfigStruct(valuesStruct, originalVersionNum);
