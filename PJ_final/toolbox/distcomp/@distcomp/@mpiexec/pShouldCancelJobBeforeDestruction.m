function shouldCancel = pShouldCancelJobBeforeDestruction( ~, job )
; %#ok Undocumented
%pShouldCancelJobBeforeDestruction - do we need to attempt job cancellation
%before allowing destruction. This is based solely on the job state.

%  Copyright 2005-2011 The MathWorks, Inc.


% LOGIC: if the job is queued or running, we must cancel the job before
% destruction. Additionally, if the job has been marked finished in the last
% TIMEOUT, then we should kill the mpiexec process in any case to make sure
% that no-one is trying to write into the job directory. This is not
% foolproof.

import parallel.internal.apishared.MpiexecUtils

jobState = job.State;

switch jobState
  case {'pending', 'unavailable', 'destroyed', 'failed'}
    % never cancel these states
    shouldCancel = false;
  case {'queued', 'running'}
    % always cancel
    shouldCancel = true;
  case 'finished'
    shouldCancel = MpiexecUtils.shouldCancelFinishedJob( job.FinishTime );
end
