function fullName = pJobSpecificFile( scheduler, job, shortPart )
; %#ok Undocumented
% pJobSpecificFile - Choose a job-specific file - assumes file storage, else uses the temp directory.

% Copyright 2006-2011 The MathWorks, Inc.

okIfNoClusterStorage = true; % only want client name
fullName = parallel.internal.apishared.FilenameUtils.jobSpecificFilename( ...
    scheduler.ClusterOsType, job.pReturnStorage().getStorageLocationStruct(), ...
    job.pGetEntityLocation, shortPart, okIfNoClusterStorage );
