function pSubmitParallelJob(mpiexec, job)
; %#ok Undocumented
%pSubmitParallelJob - submit a job for mpiexec
%
%  pSubmitParallelJob(SCHEDULER, JOB)

%  Copyright 2005-2011 The MathWorks, Inc.

% mpiexec cannot handle a non-shared filesystem
if ~mpiexec.HasSharedFilesystem
    error( 'distcomp:mpiexec:nonsharedfs', ...
           'MPIEXEC scheduler cannot support non-shared file systems' );
end

% mpiexec cannot work with 'mixed' workers
if strcmp( mpiexec.ClusterOsType, 'mixed' )
    error( 'distcomp:mpiexec:badworkertype', ...
           'The mpiexec scheduler cannot operate with "mixed" worker types' );
end

% Check the consistency of min workers and cluster size
mpiexec.pMinWorkersClusterSizeConsistentCheck(job);

% Choose a quote character
if ispc
    quote = '"';  % Quote for PC systems
else
    quote = ''''; % protect $ with single quotes
end

% Duplicate the tasks for parallel execution
job.pDuplicateTasks;

% Ensure that the job has been prepared
job.pPrepareJobForSubmission;

% Create a string for the environment variables
env_string = '';

% Define the function that will be used to decode the environment variables
env_string = iEnvSetting( mpiexec.EnvironmentSetMethod, ...
                          env_string, 'MDCE_DECODE_FUNCTION', 'decodeMpiexecSingleTask', ...
                          quote );
% Forward the MDCE_DEBUG value if it is set.  Need to do this explicitly in case the 
% environment set method is '-env'
mdceDebugName = 'MDCE_DEBUG';
mdceDebug = getenv(mdceDebugName);
if ~isempty( mdceDebug )
    env_string = iEnvSetting( mpiexec.EnvironmentSetMethod, ...
                              env_string, mdceDebugName, mdceDebug, ...
                              quote );
end

% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end
storage = job.pReturnStorage;
[stringLocation, stringConstructor] = storage.getSubmissionStrings;
env_string = iEnvSetting( mpiexec.EnvironmentSetMethod, ...
                          env_string, 'MDCE_STORAGE_LOCATION', stringLocation, ...
                          quote );
env_string = iEnvSetting( mpiexec.EnvironmentSetMethod, ...
                          env_string, 'MDCE_STORAGE_CONSTRUCTOR', stringConstructor, ...
                          quote );

% Get the location of the storage
jobLocation = job.pGetEntityLocation;
env_string = iEnvSetting( mpiexec.EnvironmentSetMethod, ...
                          env_string, 'MDCE_JOB_LOCATION', jobLocation, ...
                          quote );

% Store the expected size of MPI_COMM_WORLD
env_string = iEnvSetting( mpiexec.EnvironmentSetMethod, ...
                          env_string, 'MDCE_WORLD_SIZE', ...
                          num2str( job.MaximumNumberOfWorkers ), ...
                          quote );

[~, mlcmd, args] = mpiexec.pCalculateMatlabCommandForJob(job);

% Change bad slashes to good ones - fullfile might have given us bad ones
matlabCommand = sprintf( '%s%s%s%s', quote, mlcmd, quote, args );
                     
% Create the mpiexec command string with appropriate quoting 
mpiexecCommand = sprintf( '%s%s%s', quote, mpiexec.MpiExecFileName, quote); 
                     
% We only support the "MaximumNumberOfWorkers" property
numTasks = job.MaximumNumberOfWorkers;

% Build the full submission string
submitString = sprintf( ...
    '%s %s -n %d %s %s', ...
    mpiexecCommand, mpiexec.SubmitArguments, numTasks, env_string, matlabCommand );

% Create a file for writing MPIEXEC's stdout to. Put the full command line in
% there too.
stdout_fname = mpiexec.pJobSpecificFile( job, sprintf( 'Job%d.out', job.ID ) );

% Create a temporary BAT file name - used only on Windows
batName = mpiexec.pJobSpecificFile( job, sprintf( 'Job%d.bat', job.ID ) );

import parallel.internal.apishared.MpiexecUtils
schedulerData = MpiexecUtils.submit( submitString, stdout_fname, batName, ...
                                     mpiexec.ClientHostName, ...
                                     mpiexec.WorkerMachineOsType );

job.pSetJobSchedulerData( schedulerData );



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% i_EnvSetting - set up the environment - either append to a string or call
% setenv
function str = iEnvSetting( env_method, old_str, varname, varval, quote )

% Always set the output argument
str = old_str;

switch env_method
    case '-env'
        str = sprintf( '%s -env %s %s%s%s ', old_str, varname, quote, varval, quote );
    case 'setenv'
        setenv( varname, varval );
    otherwise
        % Can't happen because of the enum!
        error( 'distcomp:mpiexec:unknownenvironmentmethod', ...
               'Unknown EnvironmentSetMethod: %s (must be either ''-env'' or ''setenv'')', env_method );
end
