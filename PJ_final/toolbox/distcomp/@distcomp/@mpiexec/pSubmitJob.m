function pSubmitJob(~, job)
; %#ok Undocumented
%pSubmitJob - submit a job for mpiexec
%
%  pSubmitJob(SCHEDULER, JOB)

%  Copyright 2005-2012 The MathWorks, Inc.

% mpiexec cannot handle a non-shared filesystem
if ~isa( job, 'distcomp.simpleparalleljob' )
    error( 'distcomp:mpiexec:notsupported', ...
           'MPIEXEC scheduler only supports parallel jobs' );
end
