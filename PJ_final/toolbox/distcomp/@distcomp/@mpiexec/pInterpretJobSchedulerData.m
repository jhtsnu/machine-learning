function [wasSubmitted, isMpi, isAlive, pid, whyNotAlive] = pInterpretJobSchedulerData( obj, job )
; %#ok Undocumented
% pInterpretJobSchedulerData - interpret the job scheduler data and return status flags
%
% - wasSubmitted is true iff it looks like the job was submitted
% - isMpi is true iff wasSubmitted is true AND the job has valid
%   MPIEXEC scheduler data (returns "false" for old jobs) 
% - isAlive is true iff isMpi is true AND the PID is alive AND the PID has
%   the correct name AND we're on the right client
% - pid is the pid if the job if isAlive is true, else -1
% - whyNotAlive.reason is one of {'wrongclient', 'wrongpidname', 'pidnotalive', 'wrongjobtype', 'notsubmitted'}
% - whyNotAlive.description is a textual description of why the process is not considered to be alive

% Copyright 2006-2011 The MathWorks, Inc.

import parallel.internal.apishared.MpiexecUtils

[wasSubmitted, isMpi, isAlive, pid, whyNotAlive] = MpiexecUtils.interpretJobSchedulerData( ...
    obj.ClientHostName, job.pGetJobSchedulerData, job.ID );
