function val = pSetHasSharedFilesystem( ~, val )
; %#ok Undocumented
%pSetHasSharedFilesystem - always returns true for mpiexec

%  Copyright 2000-2012 The MathWorks, Inc.

if ~val
    warning( 'distcomp:mpiexec:sharedfilesystem',...
             'MPIEXEC only supports shared file systems' );
end
val = true;
