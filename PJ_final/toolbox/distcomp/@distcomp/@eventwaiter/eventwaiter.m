function obj = eventwaiter(l, varargin)
; %#ok Undocumented

%   Copyright 1984-2012 The MathWorks, Inc.

% Create the object
obj = distcomp.eventwaiter;

% Copy the relevant properties from the listener passed in.
if isa( l, 'handle.listener' )
    listeners = { ...
        handle.listener( l.Container,     l.SourceObject, l.EventType, @obj.eventTriggered );...
        handle.listener( l.SourceObject, 'ObjectBeingDestroyed',       @obj.eventTriggered );
        };
    
elseif isa( l, 'event.proplistener' )
    listeners = { ...
        event.proplistener( l.Object, l.Source, l.EventName,  @obj.eventTriggered ); ...
            event.listener( l.Object, 'ObjectBeingDestroyed', @obj.eventTriggered );
        };
    
elseif isa( l, 'event.listener' )
    listeners = { ...
        event.listener( l.Source, l.EventName,            @obj.eventTriggered ); ...
        event.listener( l.Source, 'ObjectBeingDestroyed', @obj.eventTriggered );
        };
    
else
    % The first argument is not a listener of any type.
    error(message('parallel:internal:cluster:EventWaiterNotListener'));
end

% Store everything 
obj.set ('Listeners', listeners, varargin{:} );
