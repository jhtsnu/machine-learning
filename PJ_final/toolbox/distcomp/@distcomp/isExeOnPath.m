function onPath = isExeOnPath( baseName )
; %#ok Undocumented

% Checks to see if a given program is on the executable PATH. On Windows,
% this will append extensions ".exe", ".bat" and ".cmd" and check those too.

% Copyright 2007-2012 The MathWorks, Inc.

exePath = textscan( getenv( 'PATH' ), '%s', 'delimiter', pathsep );
exePath = exePath{1};

onPath = any( cellfun( @(x)( iExeHere( fullfile( x, baseName ) ) ), exePath ) );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iExeHere - is there an executable file at this location? Handles PC
% executable extensions
function isHere = iExeHere( nameNoExt )

if ispc
    % On PC, handle the case where the input argument already specifies .exe or
    % whatever, and also append known likely executable extensions
    trail = {'', '.exe', '.bat', '.cmd', '.com'};
else
    trail = {''};
end

isHere = any( cellfun( @(x)( exist( [nameNoExt, x], 'file' ) ), trail ) == 2 );
