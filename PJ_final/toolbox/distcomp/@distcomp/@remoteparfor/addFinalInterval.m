function OK = addFinalInterval(obj, tag, varargin)
; %#ok Undocumented

%   Copyright 2007-2012 The MathWorks, Inc.


OK = obj.ParforController.addFinalInterval(tag, ...
                                           parallel.internal.pool.serialize(varargin));
