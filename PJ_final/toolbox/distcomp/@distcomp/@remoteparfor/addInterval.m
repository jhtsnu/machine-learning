function OK = addInterval(obj, tag, varargin)
; %#ok Undocumented

%   Copyright 2007-2012 The MathWorks, Inc.


OK = obj.ParforController.addInterval(tag, ...
                                      parallel.internal.pool.serialize(varargin));







