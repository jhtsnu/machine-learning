function complete(obj)
; %#ok Undocumented

%   Copyright 2007-2012 The MathWorks, Inc.

% We get into this function both during normal parfor execution as well as when
% the user code throws an error.  In both cases, this is our one and only chance
% to flush partial lines that may be pending in the command window output.

% Delete the ObjectBeingDestroyedListener to stop the workers from being
% interrupted.
delete(obj.ObjectBeingDestroyedListener);
obj.ObjectBeingDestroyedListener = [];

% Function to display the Strings in a Java String array.
dispStringArray = @(msgs) cellfun(@(msg) disp(char(msg)), cell(msgs));
output = obj.ParforController.getDrainableOutput;

% Make sure we wait for the parfor to complete and continue displaying command 
% window output whilst we are doing so.
while ~obj.ParforController.awaitCompleted(100, java.util.concurrent.TimeUnit.MILLISECONDS) ...
        &&  obj.session.isSessionRunning
    dispStringArray(output.drainOutput());    
end

dispStringArray(output.drainAllOutput());
