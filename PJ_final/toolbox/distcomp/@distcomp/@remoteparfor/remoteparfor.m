function obj = remoteparfor(maxLabsToAcquire, varargin)
; %#ok Undocumented

%   Copyright 2007-2012 The MathWorks, Inc.


obj = distcomp.remoteparfor;

session = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession;
if isempty(session) || ~session.isPoolManagerSession
    error(message('parallel:lang:parfor:NoSession'));
end
try
    % Make a new parfor controller - this might throw a
    % SessionDestroyedException
    p = session.createParforController;
catch err
    [~, exceptionType] = isJavaException(err);
    if ~isempty(regexp(exceptionType, 'SessionDestroyedException', 'once'))
        error(message('parallel:lang:parfor:NoRunningSession'));
    elseif isequal( exceptionType, 'com.mathworks.toolbox.distcomp.pmode.CannotAcquireLabsException' )
        error(message('parallel:lang:parfor:NoLabsAvailable'));
    else
        rethrow(err);
    end
end
try
    obj.Session = session;
    obj.ParforController = p;
    % Get the IntervalReturnQueue
    obj.IntervalCompleteQueue = p.getIntervalCompleteQueue;
    % Try to acquire some labs
    obj.NumWorkers = p.acquireLabs(int32(maxLabsToAcquire));
    % Serialize the initialization data and store it locally
    spmdlang.BaseRemote.saveLoadCount( 'clear' );
    obj.SerializedInitData = parallel.internal.pool.serialize(varargin);
    p.beginLoop(obj.SerializedInitData);
    % Listen for object destruction to interrupt if we exit the parfor loop prematurely
    obj.ObjectBeingDestroyedListener = handle.listener(obj, 'ObjectBeingDestroyed', {@pObjectBeingDestroyed});
    
    % Error if we're about to broadcast Composites to the labs
    if spmdlang.BaseRemote.saveLoadCount( 'get' ) ~= 0        
        error(message('parallel:lang:parfor:IllegalComposite'));
    end
catch err
    % If we catch an error then we need to free the data we were going to
    % send to the remote labs.
    if ~isempty( obj.SerializedInitData )
        arrayfun( @free, obj.SerializedInitData );
    end
    % If anything goes wrong during construction of this interface then we
    % need to clean up the parfor controller, otherwise all subsequent
    % parfor statements will revert to running locally.
    p.interrupt;
    rethrow(err);
end
