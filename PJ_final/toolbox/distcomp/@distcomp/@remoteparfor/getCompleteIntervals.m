function [tags,  results] = getCompleteIntervals(obj, numIntervals)
; %#ok Undocumented

%   Copyright 2007-2012 The MathWorks, Inc.


q = obj.IntervalCompleteQueue;
% Function to display the Strings in a Java String array.
dispStringArray = @(msgs) cellfun(@(msg) disp(char(msg)), cell(msgs));
output = obj.ParforController.getDrainableOutput;

tags = ones(numIntervals, 1);
results = cell(numIntervals, 1);
for i = 1:numIntervals
    r = [];
    while isempty(r)
        r = q.poll(1, java.util.concurrent.TimeUnit.SECONDS);
        dispStringArray(output.drainOutput());
        % Only test to see if the session is failing if we didn't get a
        % results from the queue
        if isempty(r) && ~obj.session.isSessionRunning
            error(message('parallel:lang:parfor:SessionShutDown'));
        end
    end
    % Check to see if the interval result has an error
    if r.hasError
        % Java code has already interrupted the remote execution of the parfor 
        % body and halted the receipt of further IO.  Before throwing an error,
        % we perform the normal cleanup activities.
        obj.complete();
        intervalError = r.getError;
        % Todo:MessageCatalog
        if ischar(intervalError) 
            error('distcomp:remoteparfor:UnexpectedError', '%s', intervalError);
        else
            origErr = parallel.internal.pool.deserialize(intervalError);
            clientStackToIgnore = 'parallel_function';
            % Create a new exception that stitches together the client and worker stack.
            newEx = ParallelException.hBuildRemoteParallelException(...
                origErr, clientStackToIgnore);
            throw( newEx );                
        end
    else
        tags(i) = r.getTag;
        data = r.getResult;
        results{i} = parallel.internal.pool.deserialize(data(2));
    end
end