function val = pGetCurrentJob(obj, val)
; %#ok Undocumented
%pGetCurrentJob 
%
%  VAL = pGetCurrentJob(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    proxies = proxyWorker.getCurrentJobAndTask;
    jobProxy = proxies.getJobID();
    [found, val] = findObjectInHashtable(distcomp.getdistcompobjectroot, jobProxy);
    val = val(found);
catch %#ok<CTCH> swallow errors
    % TODO
end
