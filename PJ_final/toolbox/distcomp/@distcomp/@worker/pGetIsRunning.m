function val = pGetIsRunning(~, val)
; %#ok Undocumented
%PGETISRUNNING private function to get running state from java object
%
%  VAL = PGETISRUNNING(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


