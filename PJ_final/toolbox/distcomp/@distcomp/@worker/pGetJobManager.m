function val = pGetJobManager(obj, val)
; %#ok Undocumented
%pGetJobManager 
%
%  VAL = pGetJobManager(OBJ, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    proxy = proxyWorker.getJobManager;
    val = distcomp.createObjectsFromProxies(proxy, @distcomp.jobmanager, distcomp.getdistcompobjectroot);
catch %#ok<CTCH> swallow errors
    % TODO
end
