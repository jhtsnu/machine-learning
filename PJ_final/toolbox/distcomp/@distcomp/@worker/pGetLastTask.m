function val = pGetLastTask(obj, val)
; %#ok Undocumented
%pGetLastJob 
%
%  VAL = pGetLastJob(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    proxies = proxyWorker.getLastJobAndTask;
    jobProxy  = proxies.getJobID();
    taskProxy = proxies.getTaskID();
    [found, lastJob] = findObjectInHashtable(distcomp.getdistcompobjectroot, jobProxy);
    if found
        val = distcomp.createObjectsFromProxies(taskProxy, @distcomp.task, lastJob);
    end
catch %#ok<CTCH> swallow errors
    % TODO
end
