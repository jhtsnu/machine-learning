function val = pGetName(obj, val)
; %#ok Undocumented
%PGETNAME private function to get jobmanager name from java object
%
%  VAL = PGETNAME(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    val = char(proxyWorker.getName);
catch %#ok<CTCH> swallow errors
	% TODO
end
