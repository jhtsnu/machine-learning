function val = pGetComputer(obj, ~)
; %#ok Undocumented
%PGETCOMPUTER private function to get computer type
%
%  VAL = PGETCOMPUTER(OBJ, VAL)

%  Copyright 2007-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    val = char(proxyWorker.getComputerMLType);
catch %#ok<CTCH> swallow errors
    val = '';
end
