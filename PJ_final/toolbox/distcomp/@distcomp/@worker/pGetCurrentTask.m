function val = pGetCurrentTask(obj, val)
; %#ok Undocumented
%pGetCurrentJob 
%
%  VAL = pGetCurrentJob(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    proxies = proxyWorker.getCurrentJobAndTask;
    jobProxy  = proxies.getJobID();
    taskProxy = proxies.getTaskID();
    [found, currentJob] = findObjectInHashtable(distcomp.getdistcompobjectroot, jobProxy);
    if found
        val = distcomp.createObjectsFromProxies(taskProxy, @distcomp.task, currentJob);
    end
catch %#ok<CTCH> swallow errors
    % TODO
end
