function val = pGetLastJob(obj, val)
; %#ok Undocumented
%pGetLastJob 
%
%  VAL = pGetLastJob(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    proxies = proxyWorker.getLastJobAndTask;
    jobProxy = proxies.getJobID();
    [found, val] = findObjectInHashtable(distcomp.getdistcompobjectroot, jobProxy);
    val = val(found);
catch %#ok<CTCH> swallow errors
    % TODO
end
