function val = pGetGroup(obj, val)
; %#ok Undocumented
%PGETGROUP private function to get worker group from java object
%
%  VAL = PGETGROUP(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyWorker = obj.ProxyObject;
try
    val = char(proxyWorker.getLookupGroup);
catch %#ok<CTCH> swallow errors
    % TODO
end
