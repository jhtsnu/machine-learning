function pause(~, delay)

%   Copyright 2012 The MathWorks, Inc.

pause(delay);