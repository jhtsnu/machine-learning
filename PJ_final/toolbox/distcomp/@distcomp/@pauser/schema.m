function schema
%SCHEMA defines the distcomp.pauser class
%

%   Copyright 2012 The MathWorks, Inc.

%   $Revision: 1.1.6.1 $  $Date: 2012/04/14 03:50:46 $


hThisPackage = findpackage('distcomp');
schema.class(hThisPackage, 'pauser');