function err = handleJavaException(obj, caughtException)
; %#ok Undocumented
    % function to return an appropriate error message for a variety of java
    % exceptions that the proxy layer could throw.  This function does
    % nothing if the exception is not one of the ones it recognizes.
    % objectType should be either 'jobmanager' or 'worker' as appropriate.

% Copyright 2004-2012 The MathWorks, Inc.

nExpectArgs = 2;
assert(nargin == nExpectArgs, ...
       'parallel:internal:cluster:HandleJavaExceptionIncorrectNargin', ...
       'handleJavaException was given %d args instead of %d.', nargin, nExpectArgs);

    [isJavaError, exceptionType] = isJavaException(caughtException);
    
    if ~isJavaError
        err = caughtException;
        return;
    end
    
    errMessage = caughtException.message;
    
    % Try and unravel a java.rmi.ServerException which will wrap the original exception
    if strcmp(exceptionType, 'java.rmi.ServerException')
        [hasCause, causeExceptionType, causeMessage] = dct_getJavaExceptionCause(errMessage);
        if hasCause
            exceptionType = causeExceptionType;
            errMessage = causeMessage;
        end
    end

    % Try and unravel a DistcompException which will wrap the original exception
    if strcmp(exceptionType,...
            'com.mathworks.toolbox.distcomp.distcompobjects.DistcompException')
        [hasCause, causeExceptionType] = dct_getJavaExceptionCause(errMessage);
        if hasCause
            exceptionType = causeExceptionType;
        end
    end
    
    % Try and unravel an ExecutionException which will wrap the original exception
    if strcmp(exceptionType, 'java.util.concurrent.ExecutionException')
        [hasCause, causeExceptionType] = dct_getJavaExceptionCause(errMessage);
        if hasCause
            exceptionType = causeExceptionType;
        end
    end
    
    % Otherwise do the old handling.
    msg = iFindErrorMessage(obj, exceptionType);

    % Create an MException from the message
    if isempty(msg)
        err = MException(caughtException.identifier, '%s', caughtException.message);
    else
        err = MException(msg);
    end

    if isa(caughtException, 'MException')
        err.addCause(caughtException);
    end

end

function msg = iFindErrorMessage(obj, exceptionType)
    msg = [];
    % Some error messages need to tell the user which JVM this is (either worker or local) so
    % define a string based on isdmlworker
    if system_dependent('isdmlworker')
        jvmType = 'worker';
    else
        jvmType = 'local';
    end

    if isa(obj, 'distcomp.jobmanager') || isa( obj, 'parallel.Cluster' )
        switch exceptionType
            case 'com.mathworks.toolbox.distcomp.storage.JobNotFoundException'
                msg = message('parallel:job:MJSJobNotFound');
            case 'com.mathworks.toolbox.distcomp.storage.WorkUnitNotFoundException'
                msg = message('parallel:cluster:MJSTaskOrJobNotFound');
            case 'com.mathworks.toolbox.distcomp.workunit.JobStateException'
                msg = message('parallel:job:MJSInvalidState');
            case 'com.mathworks.toolbox.distcomp.workunit.WorkUnitStateException'
                msg = message('parallel:job:MJSInvalidState');
            case 'com.mathworks.toolbox.distcomp.jobmanager.SavePausedStateException'
                msg = message('parallel:cluster:MJSPausedStateNotSaved');
            case 'com.mathworks.toolbox.distcomp.jobmanager.SaveRunningStateException'
                msg = message('parallel:cluster:MJSRunningStateNotSaved');
            case 'com.mathworks.toolbox.distcomp.util.PortUnavailableException'
                msg = message('parallel:cluster:MJSPortUnavailable');
            case 'java.net.ConnectException' 
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.ConnectException' 
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.RemoteException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.ConnectIOException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.NoSuchObjectException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'com.mathworks.toolbox.distcomp.distcompobjects.DistcompProxy$SerializeProxyException'
                msg = message('parallel:cluster:MJSReachableInvalidData');
           case 'com.mathworks.toolbox.distcomp.storage.CredentialStorageException'
               msg = message('parallel:cluster:MJSCredentialStorageFailed');
           case 'com.mathworks.toolbox.distcomp.jobmanager.LogRetrievalException'
               msg = message('parallel:cluster:MJSLogRetrievalFailed');
           case 'com.mathworks.toolbox.distcomp.mjs.datastore.NullLargeDataInvokerException'
               msg = message('parallel:cluster:MJSNoLargeDataInvoker');
       end
    elseif isa(obj, 'distcomp.worker') || isa( obj, 'parallel.cluster.MJSWorker' )
        switch exceptionType
            case 'java.net.ConnectException' 
                msg = message('parallel:cluster:MJSWorkerUnavailable'); 
            case 'java.rmi.ConnectException' 
                msg = message('parallel:cluster:MJSWorkerUnavailable'); 
            case 'java.rmi.RemoteException'
                msg = message('parallel:cluster:MJSWorkerUnavailable'); 
            case 'java.rmi.ConnectIOException'
                msg = message('parallel:cluster:MJSWorkerUnavailable'); 
            case 'java.rmi.NoSuchObjectException'
                msg = message('parallel:cluster:MJSWorkerUnavailable'); 
        end
    elseif isa(obj, 'distcomp.task') || isa( obj, 'parallel.Task' )
        switch exceptionType
            case 'com.mathworks.toolbox.distcomp.storage.TaskNotFoundException'
                msg = message('parallel:task:MJSTaskNotFound');
            case 'com.mathworks.toolbox.distcomp.storage.JobNotFoundException'
                msg = message('parallel:job:MJSJobNotFound');
            case 'com.mathworks.toolbox.distcomp.storage.WorkUnitNotFoundException'
                msg = message('parallel:cluster:MJSTaskOrJobNotFound');
            case 'com.mathworks.toolbox.distcomp.workunit.TaskStateException'
                msg = message('parallel:task:MJSInvalidTaskStateForOperation');
            case 'com.mathworks.toolbox.distcomp.workunit.JobStateException'
                msg = message('parallel:task:MJSInvalidJobStateForOperation');
            case 'com.mathworks.toolbox.distcomp.workunit.WorkUnitStateException'
                msg = message('parallel:task:MJSInvalidTaskStateForOperation');
            case 'com.mathworks.toolbox.distcomp.workunit.TaskDispatchInProgressException'
                msg = message('parallel:task:MJSCannotModifyDispatchInProgress');
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.MaxDataLimitExceededException'
                msg = message('parallel:cluster:MJSHeadnodeNotEnoughJavaMemory');
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.CallerDataStoreExceededException'
                storeSize = iGetDataStoreSizeInMB(obj);
                msg = message('parallel:cluster:MJSNotEnoughJavaMemoryToTransfer', jvmType, storeSize);
            case 'com.mathworks.toolbox.distcomp.distcompobjects.DataTransferException'
                msg = message('parallel:cluster:MJSNotEnoughJavaMemoryToAllocate', jvmType);
            case 'java.lang.OutOfMemoryError'
                msg = message('parallel:cluster:MJSNotEnoughJavaMemoryToAllocate', jvmType);
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.NullLargeDataInvokerException'
                msg = message('parallel:cluster:MJSNoLargeDataInvoker');
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.DataStoreException'
                port = iGetDataStoreExportPort(obj);
                % if the port is 0 then there is no RMI server on the
                % client. parallel:cluster:MJSCannotContactMATLAB's advice 
                % will not make sense.
                if port==0
                    msg = message('parallel:cluster:MJSCannotContactMATLABNetworkDrop');
                else
                    msg = message('parallel:cluster:MJSCannotContactMATLAB', ...
                        char(java.lang.System.getProperty('java.rmi.server.hostname')), port, ...
                        char(java.lang.System.getProperty('java.rmi.server.hostname')), port);
                end
            case 'com.mathworks.toolbox.distcomp.util.PortUnavailableException'
                msg = message('parallel:cluster:MJSPortUnavailable');
            case 'java.rmi.ServerException'
                msg = message('parallel:cluster:MJSCannotContactClient');
            case 'java.net.ConnectException' 
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.ConnectException' 
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.RemoteException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.ConnectIOException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.NoSuchObjectException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'com.mathworks.toolbox.distcomp.util.UnavailableEphemeralPortsException'
                msg = message('parallel:cluster:MJSUnavailableEphemeralPorts');
            case 'com.mathworks.toolbox.distcomp.storage.DataStorageException'
                msg = message('parallel:task:MJSDataStorageFailed');
        end           
    elseif isa(obj, 'distcomp.job') || isa( obj, 'parallel.Job' )
        switch exceptionType
            case 'com.mathworks.toolbox.distcomp.storage.TaskNotFoundException'
                msg = message('parallel:task:MJSTaskNotFound');
            case 'com.mathworks.toolbox.distcomp.storage.JobNotFoundException'
                msg = message('parallel:job:MJSJobNotFound');
            case 'com.mathworks.toolbox.distcomp.storage.WorkUnitNotFoundException'
                msg = message('parallel:cluster:MJSTaskOrJobNotFound');
            case 'com.mathworks.toolbox.distcomp.workunit.TaskStateException'
                msg = message('parallel:task:MJSInvalidTaskStateForOperation');
            case 'com.mathworks.toolbox.distcomp.workunit.JobStateException'
                msg = message('parallel:task:MJSInvalidJobStateForOperation');
            case 'com.mathworks.toolbox.distcomp.workunit.WorkUnitStateException'
                msg = message('parallel:job:MJSInvalidJobStateForOperation');
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.MaxDataLimitExceededException'
                msg = message('parallel:cluster:MJSHeadnodeNotEnoughJavaMemory');
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.CallerDataStoreExceededException'
                storeSize = iGetDataStoreSizeInMB(obj);
                msg = message('parallel:cluster:MJSNotEnoughJavaMemoryToTransfer', jvmType, storeSize);
            case 'com.mathworks.toolbox.distcomp.distcompobjects.DataTransferException'
                msg = message('parallel:cluster:MJSNotEnoughJavaMemoryToAllocate', jvmType);
            case 'java.lang.OutOfMemoryError'
                msg = message('parallel:cluster:MJSNotEnoughJavaMemoryToAllocate', jvmType);
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.NullLargeDataInvokerException'
                msg = message('parallel:cluster:MJSNoLargeDataInvoker');
            case 'com.mathworks.toolbox.distcomp.mjs.datastore.DataStoreException'
                msg = message('parallel:cluster:MJSCannotContactMATLAB',  ...
                    char(java.lang.System.getProperty('java.rmi.server.hostname')), iGetDataStoreExportPort(obj), ...
                    char(java.lang.System.getProperty('java.rmi.server.hostname')), iGetDataStoreExportPort(obj));
            case 'com.mathworks.toolbox.distcomp.util.PortUnavailableException'
                msg = message('parallel:cluster:MJSPortUnavailable');
            case 'java.rmi.ServerException'
                msg = message('parallel:cluster:MJSCannotContactClient');
            case 'java.net.ConnectException' 
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.ConnectException' 
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.RemoteException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.ConnectIOException'
                msg = message('parallel:cluster:MJSUnreachable'); 
            case 'java.rmi.NoSuchObjectException'
                msg = message('parallel:cluster:MJSUnreachable'); 
                % ParallelJob exceptions here - subclass of job
            case 'com.mathworks.toolbox.distcomp.pml.ParallelJobSingleTaskException'
                msg = message('parallel:job:CommunicatingJobOneTask');
            case 'com.mathworks.toolbox.distcomp.util.UnavailableEphemeralPortsException'
                msg = message('parallel:cluster:MJSUnavailableEphemeralPorts');
            case 'com.mathworks.toolbox.distcomp.storage.DataStorageException'
                msg = message('parallel:job:MJSDataStorageFailed');
        end
    else
        switch exceptionType
            case 'com.mathworks.toolbox.distcomp.util.PortUnavailableException'
                msg = message('parallel:cluster:MJSPortUnavailable');
            case 'com.mathworks.toolbox.distcomp.util.LookupDiscoveryException'
                msg = message('parallel:cluster:MJSMulticastLookupFailed');
            case 'java.net.MalformedURLException'
                msg = message('parallel:cluster:MalformedLookupURL');
        end        
    end        
end

function storeSize = iGetDataStoreSizeInMB(workUnit)
    dataStoreInfo = workUnit.pGetDataStoreInfo;
    storeSize =  fix(dataStoreInfo.DataStoreSize / (1024 * 1024));
end

function exportPort = iGetDataStoreExportPort(workUnit)
    dataStoreInfo = workUnit.pGetDataStoreInfo;
    exportPort = dataStoreInfo.DataStoreExportPort;
end
