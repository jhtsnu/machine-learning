function val = pSetSchedulerHostname(obj, val)
%pSetServerHostname
%
%  val = pSetServerHostname(JOB, val)

%  Copyright 2006-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.7 $    $Date: 2010/12/09 21:06:39 $

% NB It is very important that if AutoConnectToScheduler is set to false, this
% function should not error.

if ~obj.HaveTestedForMicrosoftClientUtilities
    % If we haven't tested for client utilities yet, do nothing.
    return;
end

% If we should not connect to the scheduler, then do nothing
if ~obj.AutoConnectToScheduler
    return;
end

% Replace the current server connection with one that is using the correct API and
% is connected to the correct scheduler.
try
    [obj.ServerConnection, newConnectionMade] = obj.pGetTempConnectionToScheduler(val, obj.ClusterVersion);
catch err
    % We don't allow scheduler objects that aren't connected to a scheduler
    ex = MException('distcomp:ccsscheduler:UnableToConnect', ...
        'Unable to contact a %s scheduler on machine ''%s''', obj.ClusterVersion, val);
    ex = ex.addCause(err);
    throw(ex);
end

if ~newConnectionMade
    return;
end

try
    % If this was a brand-new connection, we need to get the cluster environment
    % and set it on the scheduler.
    obj.pSetValuesFromClusterEnvironment();
catch err
    warning('distcomp:ccsscheduler:UnableToSetClusterProperties', ...
        'Unable to set properties defined in the cluster properties file.  Reason:\n%s', err.message);
end
