function schema
%SCHEMA defines the distcomp.ccsscheduler class
%

%   Copyright 2006-2010 The MathWorks, Inc.

%   $Revision: 1.1.6.9 $  $Date: 2010/09/20 14:34:38 $


hThisPackage = findpackage('distcomp');
hParentClass = hThisPackage.findclass('abstractscheduler');
hThisClass   = schema.class(hThisPackage, 'ccsscheduler', hParentClass);


p = schema.prop(hThisClass, 'SchedulerHostname', 'string');
p.AccessFlags.init = 'on';
p.FactoryValue = '';
p.SetFunction = @pSetSchedulerHostname;

% Private property that is the object that connects to a scheduler.
% Note that it is possible for this to not be correctly connected, based on
% the value of SchedulerHostname
p = schema.prop(hThisClass, 'ServerConnection', 'MATLAB array');
p.AccessFlags.PublicSet = 'off';
p.AccessFlags.PublicGet = 'off';

% Note that this defaults to false - which is essential
p = schema.prop(hThisClass, 'Initialized', 'bool');
p.AccessFlags.PublicSet = 'off';
p.AccessFlags.PublicGet = 'off';

% Public property to allow user to choose if SOA API should be used for simple jobs
% Note that this is supported only by v2
p = schema.prop(hThisClass, 'UseSOAJobSubmission', 'bool');
p.AccessFlags.init = 'on';
p.FactoryValue = false;
p.SetFunction = @pSetUseSOAJobSubmission;
p.GetFunction = @pGetUseSOAJobSubmission;
p.AccessFlags.AbortSet = 'off';

% Public property to allow user to set the job template
% Note that this is supported only by v2
p = schema.prop(hThisClass, 'JobTemplate', 'string');
p.AccessFlags.init = 'on';
p.FactoryValue = '';
p.SetFunction = @pSetJobTemplate;
p.GetFunction = @pGetJobTemplate;
p.AccessFlags.AbortSet = 'off';

% Public property to allow user to set the job XML Description file
p = schema.prop(hThisClass, 'JobDescriptionFile', 'string');
p.AccessFlags.init = 'on';
p.FactoryValue = '';
p.SetFunction = @pSetJobDescriptionFile;
p.GetFunction = @pGetJobDescriptionFile;
p.AccessFlags.AbortSet = 'off';

% Enum to indicate which cluster version we are using for ccsscheduler
% NB It is important that the enum values correspond to the actual version
% of the scheduler because we need to be able to get the versions in order 
% of decreasing version number. 
if isempty(findtype('distcomp.microsoftclusterversion'))
    schema.EnumType('distcomp.microsoftclusterversion', ...
        {'CCS', 'HPCServer2008'}, [1 2]);
end

% Public property to allow user to select which type of cluster they wish to use
p = schema.prop(hThisClass, 'ClusterVersion', 'distcomp.microsoftclusterversion'); % 'CCS' or 'HPC Server 2008'
p.AccessFlags.AbortSet  = 'on';
p.AccessFlags.init = 'on';
p.FactoryValue = 'HPCServer2008'; % initialize to the latest cluster version
p.SetFunction = @pSetClusterVersion;

% Private property that indicates whether or not we have tested that
% this really is a CCS/HPC Server Client machine
p = schema.prop(hThisClass, 'HaveTestedForMicrosoftClientUtilities', 'bool');
p.AccessFlags.PublicSet = 'off';
p.AccessFlags.PublicGet = 'off';
p.AccessFlags.AbortSet = 'off';
p.AccessFlags.init = 'on';
p.FactoryValue = false;

% Private property that indicates whether or not we should automatically
% reconnect to the scheduler whenever the SchedulerHostname or ClusterVersion
% are changed.  Note that changing this value to false will mean that the 
% ServerConnection will not correspond to the cluster given by ClusterVersion 
% and SchedulerHostname.
% The default is ALWAYS to reconnect whenever these two properties change value.  
%
% The purpose of this flag is purely to allow the ClusterVersion and SchedulerHostname
% to be changed in a single operation without re-connecting in between (in 
% pSetHostnameAndVersion)
p = schema.prop(hThisClass, 'AutoConnectToScheduler', 'bool');
p.AccessFlags.PublicSet = 'off';
p.AccessFlags.PublicGet = 'off';
p.AccessFlags.AbortSet = 'off';
p.AccessFlags.init = 'on';
p.FactoryValue = true;

%%%
% Declare static methods
schema.method(hThisClass, 'pGetDefaultClientAPIVersion', 'static');
schema.method(hThisClass, 'pFindClusterHeadNodes', 'static');
schema.method(hThisClass, 'pGetDefaultSchedulerHostname', 'static');
