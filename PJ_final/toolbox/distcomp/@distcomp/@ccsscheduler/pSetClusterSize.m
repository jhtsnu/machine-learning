function val = pSetClusterSize(obj, val)
; %#ok Undocumented

%  Copyright 2009-2012 The MathWorks, Inc.


if isempty(obj.ServerConnection)
    return;
end

% Tell the server connection what the MaximumNumberOfWorkersPerJob should be.
try
    obj.ServerConnection.MaximumNumberOfWorkersPerJob = val;
catch err
    throw(err);
end
