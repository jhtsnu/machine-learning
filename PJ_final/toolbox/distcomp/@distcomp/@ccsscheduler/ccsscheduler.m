function obj = ccsscheduler(proxyScheduler)
%CCSSCHEDULER concrete constructor for this class
%
%  OBJ = CCSSCHEDULER(OBJ)

%  Copyright 2006-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.8 $    $Date: 2010/09/02 13:30:09 $

% Don't go any further if we aren't running on a PC.
if ~ispc
    error('distcomp:ccsscheduler:IncorrectPlatform', 'The HPC Server scheduler is supported only on windows');
end

obj = distcomp.ccsscheduler;

set(obj, ...
    'Type', 'hpcserver', ...
    'Storage', handle(proxyScheduler.getStorageLocation), ...
    'HasSharedFilesystem', true);

% Let's test that we are able to create a link to the scheduler
clientAPIVersion = distcomp.ccsscheduler.pGetDefaultClientAPIVersion();

% Indicate that we have tested for Microsoft Client Utilities
obj.HaveTestedForMicrosoftClientUtilities = true;
% It is very important that the ClusterVersion is set prior to the 
% the SchedulerHostname.  
obj.ClusterVersion = clientAPIVersion;

% This class accepts configurations and uses the scheduler section.
sectionName = 'scheduler';
obj.pInitializeForConfigurations(sectionName);

% Indicate that we have finished initializing the object
obj.Initialized = true;