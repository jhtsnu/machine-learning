function clientAPIVersion = pGetDefaultClientAPIVersion
% pGetDefaultClientAPIVersion
%
%   apiVersion = pGetDefaultClientAPIVersion()

%  Copyright 2010-2011 The MathWorks, Inc.


import parallel.internal.apishared.HPCServerUtils

clientAPIVersion = HPCServerUtils.getDefaultClientVersion();
