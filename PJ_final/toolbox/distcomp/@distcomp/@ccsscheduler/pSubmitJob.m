function pSubmitJob(ccs, job)
%pSubmitJob A short description of the function
%
%  pSubmitJob(SCHEDULER, JOB)

%  Copyright 2006-2012 The MathWorks, Inc.


% Check the job is in a state to be submitted
ccs.pPreSubmissionChecks(job);

% Ensure that the job has been prepared
job.pPrepareJobForSubmission;

% Get the submission arguments that are common to all jobs
jobSubmissionArguments =  ccs.pGetCommonJobSubmissionArguments(job);

% Add the correct decode function to the job environment variables
if ccs.UseSOAJobSubmission
    jobEnvironmentVariables = [jobSubmissionArguments.jobEnvironmentVariables; ...
        {'MDCE_DECODE_FUNCTION',             'decodeCcsSoaTask'; ...
         'MDCE_SYNCHRONOUS_TASK_EVALUATION', 'true'}];
else
    jobEnvironmentVariables = [jobSubmissionArguments.jobEnvironmentVariables; ...
        {'MDCE_DECODE_FUNCTION',             'decodeCcsSingleTask'; ...
         'MDCE_SYNCHRONOUS_TASK_EVALUATION', 'false'}];
end
% The name of the environment variable that specifies the task location
taskLocationEnvironmentVariableName = 'MDCE_TASK_LOCATION';

[~, matlabExe, matlabArgs] = ccs.pCalculateMatlabCommandForJob(job);

taskIDs = get(job.Tasks, {'ID'});
taskIDs = [taskIDs{:}];
taskLocations = arrayfun(@(x) x.pGetEntityLocation, job.Tasks, 'UniformOutput', false);
taskLogFcn = @(x) strrep(jobSubmissionArguments.fullLogLocation,jobSubmissionArguments.logTaskIDToken, num2str(x));
taskLogLocations = arrayfun(taskLogFcn, taskIDs, 'UniformOutput', false);

try
    % Ask the server connection to submit the job.
    [schedulerJobIDs, schedulerTaskIDs, schedulerJobName] = ccs.ServerConnection.submitJob(matlabExe, matlabArgs, ...
        jobEnvironmentVariables, taskLocationEnvironmentVariableName, ...
        job.ID, jobSubmissionArguments.fullJobLocation, taskIDs, taskLocations, taskLogLocations, ...
        jobSubmissionArguments.username, jobSubmissionArguments.password);
catch err
    throw(err);
end

% Now store the returned values in the job scheduler data
if length(schedulerJobIDs) == 2
    schedulerAdditionalJobID = schedulerJobIDs(2);
else
    schedulerAdditionalJobID = [];
end
matlabTaskIDs = cell2mat(get(job.Tasks, {'ID'}));
schedulerData = distcomp.MicrosoftJobSchedulerData(ccs.ServerConnection.getAPIVersion, ...
    ccs.ServerConnection.SchedulerVersion, ccs.SchedulerHostname, ...
    ccs.UseSOAJobSubmission, schedulerJobName, schedulerJobIDs(1), schedulerAdditionalJobID, ...
    schedulerTaskIDs, matlabTaskIDs, jobSubmissionArguments.logRelativeToRoot, jobSubmissionArguments.logTaskIDToken);
job.pSetJobSchedulerData(schedulerData);
