function pSetHostnameAndVersion(ccs, hostname, version)
% pSetHostnameAndVersion
%
%   pSetHostnameAndVersion(ccs, hostname, version)

%  Copyright 2010 The MathWorks, Inc.

% $Revision: 1.1.6.2 $  $Date: 2010/12/09 21:06:38 $


% See if the supplied hostname and version are valid by trying to 
% get a connection to that cluster
try
    [serverConnection, newConnectionMade] = ccs.pGetTempConnectionToScheduler(hostname, version);
catch err
    ex = MException('distcomp:ccsscheduler:UnableToConnect', ...
        'Cannot connect to a scheduler on machine ''%s'' using ClusterVersion ''%s''', ...
        hostname, version);
    ex = ex.addCause(err);
    throw(ex);
end

% We can get a connection, so it's now OK to change the actual version and 
% hostname properties on the object.  
originalHostname = ccs.SchedulerHostname;
originalVersion = ccs.ClusterVersion;

% Turn off auto-connection so that we can change values without reconnecting.  
% Note that we must also manually set the ServerConnection to "connect" the ccsscheduler.
%
% Nothing should go wrong with setting SchedulerHostname and ClusterVersion
% if AutoConnectToScheduler is false, so putting the following code in try-catch
% is purely for the sake of paranoia.
ccs.AutoConnectToScheduler = false;
try
    ccs.SchedulerHostname = hostname;
catch err
    % Make sure we set AutoConnectToScheduler back to true
    % if anything went wrong.
    ccs.AutoConnectToScheduler = true;
    throw(err);
end

try
    ccs.ClusterVersion = version;
catch err
    % Make sure we set AutoConnectToScheduler back to true
    % and restore the SchedulerHostname to what it was before
    % if anything went wrong
    ccs.SchedulerHostname = originalHostname;
    ccs.AutoConnectToScheduler = true;
    throw(err);
end

try
    ccs.ServerConnection = serverConnection;
catch err
    % Make sure we set AutoConnectToScheduler back to true
    % and restore the SchedulerHostname and ClusterVersion
    % to what they were before
    % if anything went wrong
    ccs.SchedulerHostname = originalHostname;
    ccs.ClusterVersion = originalVersion;
    ccs.AutoConnectToScheduler = true;
    throw(err);
end

% Set AutoConnectToScheduler back to true
ccs.AutoConnectToScheduler = true;

% If we had made a new connection when deducing the cluster version, then 
% set the scheduler values based on the cluster environment.
if newConnectionMade
    try
        ccs.pSetValuesFromClusterEnvironment();
    catch err
        warning('distcomp:ccsscheduler:UnableToSetClusterProperties', ...
            'Unable to set properties defined in the cluster properties file.  Reason:\n%s', err.message);
    end
end
