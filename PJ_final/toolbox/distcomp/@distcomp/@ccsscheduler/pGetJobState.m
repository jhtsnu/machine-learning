function state = pGetJobState(ccs, job, state)
%pGetJobState - deferred call to ask the scheduler for state information
%
%  STATE = pGetJobState(SCHEDULER, JOB, STATE)

%  Copyright 2006-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.4 $    $Date: 2010/07/19 12:49:34 $

% Only ask the scheduler what the job state is if it is queued or running
if strcmp(state, 'queued') || strcmp(state, 'running')
    % Get the information about the actual scheduler used
    data = job.pGetJobSchedulerData;
    if isempty(data)
        return
    end
    % Is the job actually a Microsoft scheduler job?
    if ~isa(data, 'distcomp.MicrosoftJobSchedulerData')
        return
    end
    s = ccs.pGetTempConnectionToScheduler(data.SchedulerName, data.APIVersion);
    try
        schedulerState = s.getJobStateByID(data.SchedulerJobID);
    catch err %#ok<NASGU>
        % Failed to retrieve the schedulerState, so leave the state alone
        return;
    end

    state = schedulerState;
    % The scheduler may return state failed for workers that 
    % exited with a non-zero exit code.  However, the tasks may
    % actually have run to completion, so we should double-check
    % the job state from the tasks, and fall back to this if it
    % is the finished state.  If number of tasks == 0 (e.g. because they
    % were destroyed), then just stick with the scheduler's state.
    if (numel(job.Tasks) > 0) && strcmp(state, 'failed')
        jobState = job.pGetStateFromTasks;
        if strcmp(jobState, 'finished')
            state = jobState;
        end
    end

    job.pSetTerminalStateFromScheduler(state);
end
