function headNodes = pFindClusterHeadNodes(clusterVersion)
%pFindClusterHeadNodes
%
%  val = pFindClusterHeadNodes(clusterVersion)

%  Copyright 2010-2011 The MathWorks, Inc.


import parallel.internal.apishared.HPCServerUtils

% Persist the list of head nodes we get from doing an AD search.  
% We don't really want to have to do this, but AD search may
% take a long time and we don't want to keep doing it.  This will
% make its way into discoverClusters() eventually, at which point  
% the results of the AD search will not need to be cached since it
% is acceptable for discoverCluster() to take a long time.
%
% We can't just rely on headNodesFromAD not being empty, since the
% AD search may actually yield no head nodes, so keep track of 
% whether we have searched AD at least once.
%
% Note that this file is not mlocked.
persistent headNodesFromAD haveSearchedADOnce

headNodes = HPCServerUtils.findHeadNodeFromEnvironment();
if ~isempty(headNodes)
    return;
end

% Use the persisted values if available.
if ~isempty(haveSearchedADOnce)
    headNodes = headNodesFromAD;
end

try
    % Try and use Active Directory to search for head nodes for the specified cluster version
    headNodesFromAD = HPCServerUtils.findHeadNodesFromAD(clusterVersion);
    haveSearchedADOnce = true;
catch err %#ok<NASGU>
    % Return an empty hostname if something failed when searching AD for a cluster.
    return;
end

if isempty(headNodesFromAD)
    % Return an empty hostname if no head nodes were found.
    return;
end

if numel(headNodesFromAD) == 1
    headNodesFromAD = headNodesFromAD{1};
end

headNodes = headNodesFromAD;