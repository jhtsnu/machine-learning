function pDeduceVersionAndSetHostname(ccs, schedulerHostname, configName)
% pDeduceVersionAndSetHostname
%
%   pDeduceVersionAndSetHostname(hostname, configName)

%  Copyright 2010 The MathWorks, Inc.

% $Revision: 1.1.6.1 $  $Date: 2010/09/20 14:34:34 $

% If a ClusterVersion is specified in the configuration, then we will 
% always use that version to connect to the cluster.  
% If we can't get a ClusterVersion out of the config, then we just
% have to try until we do manage to establish a connection.

if ~isempty(configName)
    configStruct = distcompConfigSection(configName, 'scheduler');
    % Check to see if the configuration contains a ClusterVersion
    if ~isempty(configStruct) && isfield(configStruct, 'ClusterVersion')
        clusterVersion = configStruct.ClusterVersion;
        ccs.pSetHostnameAndVersion(schedulerHostname, clusterVersion);
        return;
    end
end

% Try the different cluster versions in order of decreasing version so that
% we are guaranteed to use the most appropriate version of the API.
clusterVersionType = findtype('distcomp.microsoftclusterversion');
sortedClusterVersionValues = sort(clusterVersionType.Values(:), 1, 'descend'); 
sortedClusterVersionStrings = clusterVersionType.Strings(sortedClusterVersionValues);
    
numVersions = numel(sortedClusterVersionStrings);
for ii = 1:numVersions
    try
        ccs.pSetHostnameAndVersion(schedulerHostname, sortedClusterVersionStrings{ii});
        % Success, so use this hostname and version
        return;
    catch err %#ok<NASGU>
        % If we've exhausted all the remaining cluster versions, then 
        % error, otherwise, just try the next cluster version.
        if ii == numVersions
            error('distcomp:ccsscheduler:CannotDeduceClusterVersion', ...
                'Unable to deduce the correct cluster version to use for a scheduler on machine ''%s''', ...
                schedulerHostname);
        end
    end
end


