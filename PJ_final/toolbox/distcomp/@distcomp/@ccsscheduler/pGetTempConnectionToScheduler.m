function [conn, newConnectionMade] = pGetTempConnectionToScheduler(ccs, name, version)
%pGetTempConnectionToScheduler -
% Get a connection to a given scheduler - this might be the one cached in
% the CCS object or a new one that will be disconnected afterwards.
%
% We maintain a map of the connections to ensure that scheduler connection properties 
% (e.g. UseSOAJobSubmission, JobTemplate, MaximumNumberOfWorkersPerJob) are not changed
% when we need a temp connection to a scheduler with a different hostname/version.  We don't
% want these properties to be reset to the default values every time pGetTempConnectionToScheduler
% is called (e.g. from getDebugLog.m).  
%
%  Copyright 2006-2011 The MathWorks, Inc.


import parallel.internal.apishared.HPCServerUtils

newConnectionMade = false;
% Lets get a connection - if the one we have is already connected
% to the specified hostname using the correct version (as is likely)
% then use it
conn = ccs.ServerConnection;
if ~isempty(conn) && strcmpi(conn.getAPIVersion, version) && conn.isConnectedToScheduler(name)
    return;
end

[conn, newConnectionMade] = HPCServerUtils.getServerConnection(name, version);