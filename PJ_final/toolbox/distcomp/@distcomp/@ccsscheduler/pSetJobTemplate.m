function val = pSetJobTemplate(ccs, val)
; %#ok Undocumented
% Set the job template on the server connection

%  Copyright 2009-2012 The MathWorks, Inc.


if isempty(ccs.ServerConnection)
    return;
end

try
    % Ask the server connection to set the job template.
    ccs.ServerConnection.JobTemplate = val;
catch err
    % NB set will throw a MATLAB:noPublicFieldForClass, but 
    % get will throw a MATLAB:noSuchMethodOrField error;
    if strcmpi(err.identifier, 'MATLAB:noPublicFieldForClass')
        % Job templates aren't supported on this type of scheduler.
        if ~isempty(val)
            % Only default to '' and warn if the user was attempting 
            % to set job template to something other than empty
            val = '';
            warning('distcomp:ccsscheduler:JobTemplatesUnsupported', ...
                'Job Templates are not supported for this type of scheduler.');
        end
    else
        throw(err);
    end
end