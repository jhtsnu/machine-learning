function val = pSetJobDescriptionFile(ccs, val)
; %#ok Undocumented
% Sets the job description file on the server connection

%  Copyright 2009-2012 The MathWorks, Inc.


if isempty(ccs.ServerConnection)
    return;
end

% Ask the server connection to set the job description file.
% Both versions 1 and 2 support XML Description files
try 
    ccs.ServerConnection.JobDescriptionFile = val;
catch err
    throw(err);
end
