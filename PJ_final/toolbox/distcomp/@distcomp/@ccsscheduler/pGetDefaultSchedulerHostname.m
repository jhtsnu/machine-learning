function headNodes = pGetDefaultSchedulerHostname(configName)
% pGetDefaultSchedulerHostname
%
%   headNodes = pGetDefaultSchedulerHostname(configName)

%  Copyright 2010 The MathWorks, Inc.

% $Revision: 1.1.6.1 $  $Date: 2010/09/02 13:30:12 $

clusterVersion = '';
headNodes = '';
if ~isempty(configName)
    configStruct = distcompConfigSection(configName, 'scheduler');
    % Check to see if the configuration contains a SchedulerHostname
    if ~isempty(configStruct) && isfield(configStruct, 'SchedulerHostname')
        headNodes = configStruct.SchedulerHostname;
        return;
    end
    
    % We need to know which cluster version to use before finding 
    % the head nodes.  See if the configuration contains a cluster
    % version for us to use.  Otherwise, use the default one.
    if ~isempty(configStruct) && isfield(configStruct, 'ClusterVersion')
        clusterVersion = configStruct.ClusterVersion;
    end
end

if isempty(clusterVersion)
    try
        clusterVersion = distcomp.ccsscheduler.pGetDefaultClientAPIVersion();
    catch err %#ok<NASGU>
        % do nothing, just return empty
        return;
    end
end

% Otherwise see if we can find a head node on the network
headNodes = distcomp.ccsscheduler.pFindClusterHeadNodes(clusterVersion);

