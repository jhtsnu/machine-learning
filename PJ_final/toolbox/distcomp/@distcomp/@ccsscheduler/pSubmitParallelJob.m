function pSubmitParallelJob(ccs, job)
%pSubmitParallelJob A short description of the function
%
%  pSubmitParallelJob(SCHEDULER, JOB)

%  Copyright 2006-2012 The MathWorks, Inc.


% Check the job is in a state to be submitted
ccs.pPreSubmissionChecks(job);

% Check the consistency of min workers and cluster size
ccs.pMinWorkersClusterSizeConsistentCheck(job);

% Duplicate the tasks for parallel execution
job.pDuplicateTasks;
% Ensure that the job has been prepared
job.pPrepareJobForSubmission;

% Get the submission arguments that are common to all jobs
jobSubmissionArguments = ccs.pGetCommonJobSubmissionArguments(job);
% Add the correct decode function to the job environment variables
jobEnvironmentVariables = [jobSubmissionArguments.jobEnvironmentVariables; ...
    {'MDCE_DECODE_FUNCTION',     'decodeCcsSingleParallelTask'; ...
     'MDCE_FORCE_MPI_OPTION',    'msmpi'}];

% Work out which command line we need to use for this job
[~, matlabExe, matlabArgs] = ccs.pCalculateMatlabCommandForJob(job);
matlabCommand = sprintf('"%s" %s', matlabExe,  matlabArgs);
genvlist = '-genvlist MDCE_DECODE_FUNCTION,MDCE_FORCE_MPI_OPTION,MDCE_STORAGE_LOCATION,MDCE_STORAGE_CONSTRUCTOR,MDCE_JOB_LOCATION,CCP_NODES,CCP_JOBID';
commandLine = ['mpiexec -l ' genvlist ' -hosts %CCP_NODES% ' matlabCommand];

workersRange = [job.MinimumNumberOfWorkers, job.MaximumNumberOfWorkers];
try
    % Ask the server connection to submit the job
    [schedulerJobIDs, schedulerTaskIDs] = ccs.ServerConnection.submitParallelJob(commandLine, ...
        workersRange, numel(job.Tasks), ...
        jobEnvironmentVariables, jobSubmissionArguments.fullLogLocation, ...
        jobSubmissionArguments.username, jobSubmissionArguments.password);
catch err
    throw(err);
end

% Now store the returned values in the job scheduler data
% Parallel jobs never need to know the job name
schedulerJobName = '';
% Parallel jobs are never SOA jobs
isSOAJob = false;
% non-SOA jobs never have an additional job ID
schedulerAdditionalJobID = [];
matlabTaskIDs = cell2mat(get(job.Tasks, {'ID'}));
schedulerData = distcomp.MicrosoftJobSchedulerData(ccs.ServerConnection.getAPIVersion, ...
    ccs.ServerConnection.SchedulerVersion, ccs.ServerConnection.SchedulerHostname, ...
    isSOAJob, schedulerJobName, schedulerJobIDs(1), schedulerAdditionalJobID, ...
    schedulerTaskIDs, matlabTaskIDs, jobSubmissionArguments.logRelativeToRoot, jobSubmissionArguments.logTaskIDToken);
job.pSetJobSchedulerData(schedulerData);
