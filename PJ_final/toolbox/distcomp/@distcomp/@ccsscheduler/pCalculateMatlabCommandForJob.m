function [worker, mlcmd, args] = pCalculateMatlabCommandForJob( obj, job )
; %#ok Undocumented
%pCalculateMatlabCommandForJob - calculate MatlabCommandToRun
%   This uses knowledge about the job type and the useSoa property on the scheduler

%  Copyright 2008-2011 The MathWorks, Inc.

[worker, mlcmd, args] = parallel.internal.apishared.WorkerCommand.hpcServerCommand( ...
    obj.ClusterOsType, obj.ClusterMatlabRoot, obj.UseSOAJobSubmission, ...
    isa( job, 'distcomp.simpleparalleljob' ) );
