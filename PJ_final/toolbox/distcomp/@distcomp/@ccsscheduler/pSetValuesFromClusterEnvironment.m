function pSetValuesFromClusterEnvironment(ccs)
%pSetValuesFromPropertiesFile
%
% pSetValuesFromPropertiesFile(ccs)

%  Copyright 2010-2012 The MathWorks, Inc.

import parallel.internal.apishared.ConversionContext
import parallel.internal.apishared.HPCServerUtils

[api2PropNames, vals, propertyFilename] = HPCServerUtils.getValuesFromClusterEnvironment(ccs.ServerConnection);
props = ConversionContext.HPCServer.convertToApi1(api2PropNames);
% Make sure props and values are rows
if iscolumn(props)
    props = props';
end
if iscolumn(vals)
    vals = vals';
end

% Get rid of API2 only properties
[props, propsIdx] = setdiff(props, 'Name');
vals = vals(propsIdx);

mhlmIdx = strcmp(props, 'RequiresMathWorksHostedLicensing');
if any(mhlmIdx) 
    if vals{mhlmIdx}
        if feature('hotlinks')
            parclusterLink = '<a href="matlab:help parcluster">parcluster</a>';
            constructorLink = '<a href="matlab:help parallel.cluster.HPCServer">parallel.cluster.HPCServer</a>';
        else
            parclusterLink = 'parcluster';
            constructorLink = 'parallel.cluster.HPCServer';
        end
        warning('distcomp:ccsscheduler:MHLMNotSupportedFromThisAPI', ...
            ['This cluster''s worker licenses are managed online and cannot ', ...
            'be accessed using schedulers created using findResource.  ', ...
            'Use %s or the %s constructor to get a cluster object instead.'], ...
            parclusterLink, constructorLink);
    end
    props(mhlmIdx) = [];
    vals(mhlmIdx) = [];
end

try
    ccs.set(props, vals);
catch err %#ok<NASGU>
    warning('distcomp:ccsscheduler:FailedToSetProperties', ...
        ['Some of the cluster environment properties defined in ''%s'' were not set. ', ...
        'Contact your system administrator for more information.'], ...
        propertyFilename);
end
