function val = pGetDataLocation(obj, ~)
; %#ok Undocumented

%  Copyright 2000-2012 The MathWorks, Inc.


% This is a string representation of the actual storage property
val = char(obj.Storage);
