function pSubmitParallelJob(~, ~)
%pSubmitJob A short description of the function
%
%  pSubmitJob(SCHEDULER, JOB)

%  Copyright 2005-2012 The MathWorks, Inc.


error('distcomp:abstractscheduler:AbstractMethodCall', 'Scheduler sub-classes MUST override this method');

