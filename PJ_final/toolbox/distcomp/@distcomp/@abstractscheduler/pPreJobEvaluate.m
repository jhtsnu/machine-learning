function pPreJobEvaluate( ~, ~, ~ ) % obj, job, task
%pPreJobEvaluate - perform scheduler-specific pre-evaluation

% Copyright 2012 The MathWorks, Inc.

% Default implementation: do nothing

