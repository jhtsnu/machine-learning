function [clientName, clusterName] = pJobSpecificFile( obj, job, nameInsideDir, isOkIfNoClusterStorage )
; %#ok Undocumented
%pJobSpecificFile - return the full path to a file within the job subdirectory
% with correct slashes etc.

% Copyright 2006-2011 The MathWorks, Inc.

[clientName, clusterName] = parallel.internal.apishared.FilenameUtils.jobSpecificFilename( ...
    obj.ClusterOsType, job.pReturnStorage().getStorageLocationStruct(), ...
    job.pGetEntityLocation, nameInsideDir, isOkIfNoClusterStorage );
