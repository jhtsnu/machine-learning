function val = pGetJobs(scheduler, val)
; %#ok Undocumented
%PGETJOBS A short description of the function
%
%  VAL = PGETJOBS(SCHEDULER, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


try
    % Get the parent string information from the object
    schedulerLocation = scheduler.pGetEntityLocation;
    % Find proxies for objects parented by scheduler location
    [proxies, constructors] = scheduler.Storage.findProxies(schedulerLocation);
    % Read the requested constructor from the locations
    % Create a wrapper around the new location
    val = distcomp.createObjectsFromProxies(...
        proxies, constructors, scheduler, 'rootsearch');
catch %#ok<CTCH> swallow errors finding jobs
    % TODO
end
