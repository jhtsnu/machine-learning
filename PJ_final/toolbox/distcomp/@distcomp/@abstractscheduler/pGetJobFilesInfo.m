function result = pGetJobFilesInfo( sched, job )
; %#ok undocumented

% Copyright 2011 The MathWorks, Inc.

assert( job.Parent == sched );

if ~isa( sched.Storage, 'distcomp.filestorage' )
    % Never get here.
    error( 'distcomp:abstractscheduler:FileStorageRequired', ...
           'pGetJobFilesInfo requires that the scheduler is using filestorage.' );
end

result = parallel.internal.apishared.JobFilesInfo( sched.Storage, ...
                                                  job.pGetEntityLocation(), ...
                                                  numel( job.Tasks ) );
