function [worker, mlcmd, args] = pCalculateMatlabCommandForJob( obj, job )
; %#ok Undocumented
%pCalculateMatlabCommandForJob - calculate MatlabCommandToRun
%   This uses knowledge about the ClusterOsType and the job type

% Copyright 2006-2011 The MathWorks, Inc.

[worker, mlcmd, args] = parallel.internal.apishared.WorkerCommand.defaultCommand( ...
    obj.ClusterOsType, obj.ClusterMatlabRoot, isa( job, 'distcomp.simpleparalleljob' ) );
