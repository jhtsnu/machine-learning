function pDestroyTask(~, ~)
; %#ok Undocumented
%pDestroyTask allow scheduler to kill task
%
%  pDestroyTask(SCHEDULER, TASK)

%  Copyright 2005-2012 The MathWorks, Inc.

