function state = pGetJobState(~, ~, state)
; %#ok Undocumented
%pGetJobState - deferred call to ask the scheduler for state information
%
%  STATE = pGetJobState(SCHEDULER, JOB, STATE)

%  Copyright 2005-2012 The MathWorks, Inc.

