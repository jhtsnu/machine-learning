function matlabpool(obj, varargin)
%matlabpool  Open a pool of MATLAB sessions with scheduler for parallel computation
% 
%   MATLABPOOL enables the parallel language features within the MATLAB
%   language (e.g., parfor) by starting a parallel job that connects this
%   MATLAB client session with a number of labs.
%
%   MATLABPOOL(scheduler) or MATLABPOOL(scheduler, 'OPEN') starts a worker
%   pool using the identified scheduler or job manager.  You can also
%   specify the pool size using MATLABPOOL(scheduler, 'open', <poolsize>).
%   The poolsize cannot exceed the cluster size specified on the scheduler.
%
%   MATLABPOOL(scheduler, <poolsize>) is the same as MATLABPOOL(scheduler,
%   'open', <poolsize>) and is provided for convenience.
%
%   MATLABPOOL(... , 'FileDependencies', FILEDEPENDENCIES) starts a worker
%   pool and concatenates the cell array FILEDEPENDENCIES with the
%   FileDependencies specified in the scheduler's configuration, should one
%   exist. Note that 'FileDependencies' is case-sensitive.
%
%   Examples: 
%
%   1. Start a MATLAB pool on a scheduler, using a pool size specified
%      by that scheduler's configuration: 
%
%      >> jm = findResource('scheduler', 'configuration', defaultParallelConfig);
%      >> matlabpool(jm)
%
%   2. Start a pool of 16 MATLAB workers 
%      >> matlabpool(jm, 16)
%
%   See also   matlabpool, findResource, distcomp.jobmanager/batch

%  Copyright 2010-2011 The MathWorks, Inc.

import parallel.internal.cluster.MatlabpoolHelper;

import parallel.internal.cluster.MatlabpoolHelper
import parallel.internal.apishared.ProfileConfigHelper

try
    pch = ProfileConfigHelper.buildApi1();
    parsedArgs = MatlabpoolHelper.parseInputsAndCheckOutputsForMethod( ...
        pch, nargout, varargin{:});
catch err
    if strcmpi(err.identifier, MatlabpoolHelper.FoundNoParseActionErrorIdentifier)
        error('parallel:lang:matlabpool:InvalidMatlabpoolAction', ... 
              ['The matlabpool method on a scheduler object accepts only the ''open'' action.  ', ...
               'Use the matlabpool function for all other actions.'] );
    end
    throw(err);
end

% Do the actual matlabpool bit
try
    % we never expect any output args from doMatlabpool on a scheduler
    MatlabpoolHelper.doMatlabpool(parsedArgs, obj);
catch err
    if strcmp(parsedArgs.Action, MatlabpoolHelper.OpenAction)
        ex = iGetRunValidationException(err, obj.Configuration);
        throw(ex);
    else
        % Make all errors appear from matlabpool
        throw(err);
    end
end

end


% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function ex = iGetRunValidationException(err, configName)
messagePrefix = 'Failed to open matlabpool. (For information in addition to the causing error,';
if isempty(configName)
    ex = MException('distcomp:matlabpool:CreateConfigRunValidation', ...
        ['%s create a profile for your scheduler and validate ', ...
        'the profile in the Cluster Profile Manager.)'], ...
        messagePrefix);
else
    ex = MException('distcomp:matlabpool:RunValidation', ...
        '%s validate the profile ''%s'' in the Cluster Profile Manager.)', ...
        messagePrefix, configName);
end
ex = ex.addCause(err);
end
