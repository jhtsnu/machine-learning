function connMgr = pCreateConnectionManager( ~ )
; %#ok Undocumented
% pCreateConnectionManager returns a newly created ConnectionManager object
% using ConnMgrBuilder.

% Copyright 2007-2011 The MathWorks, Inc.

connMgr = parallel.internal.apishared.ConnMgrBuilder.buildForCJS();
