function dependencyMap = pAddDependenciesFromZipfile(zipfileName, dependencyList, dependencyPaths, dependencyDir)
% pAddDependenciesFromZipFile - Unzip the zip archive into the attached files folder.
%
%   zipFileName - the name of the zip file containing the attached files.
%
%   dependencyList - a cell array of strings containing the original paths to the
%       attached files on the worker.
%
%   dependencyPaths -
%       In API2: EITHER a cell array of strings containing the paths
%       to the attached files relative to the zip archive root OR the AttachedFileMap
%       which maps original path roots to the truncated path roots in the zip archive.
%
%       In API1: this should be identical to dependencyList.
%
%   dependencyDir - path to the attached files folder on the worker.
%
%   dependencyMap - a 2-column cell array of strings of original path on the client
%       to new path on the worker.
%
%   Copyright 2009-2012 The MathWorks, Inc.

unzip(zipfileName, dependencyDir);
if ischar(dependencyList)
    dependencyList = {dependencyList};
end
if ischar(dependencyPaths)
    dependencyPaths = {dependencyPaths};
end

% convert '\' to '/' in case the path is coming from a PC - NOTE PC
% MATLAB can deal with '/' whereas unix cannot deal with '\' so ALWAYS
% convert to unix.
dependencyList = cellfun( @(x) strrep( x, '\', '/' ), dependencyList, ...
    'UniformOutput', false );

dctSchedulerMessage( 6, ...
    sprintf( 'About to add dependencies to path:\n%s', ...
        sprintf( '%s\n', dependencyList{:} ) ) );

% In API-2 the dependencyPaths will be an AttachedFileMap  which we'll use to create
% the dependency map.
if isa( dependencyPaths, 'parallel.internal.apishared.AttachedFileMap' )
    pathsToAdd = cellfun(@(x) fullfile( dependencyDir, x ), dependencyPaths.values(), ...
        'UniformOutput', false );
    dctSchedulerMessage( 6, ...
        sprintf( 'Adding dependency paths:\n%s', sprintf( '%s\n', pathsToAdd{:} ) ) );
    % Search through the dependencyList and convert paths using the dependencyPaths map.
    dependencyMap = iCreateDependencyMap( dependencyList, dependencyPaths, dependencyDir );
    dctSchedulerMessage( 6, ...
        sprintf( 'Created dependency map:\n%s', iDependencyMapToString( dependencyMap ) ) );
else
    dctSchedulerMessage( 6, 'Calculating dependency paths' );
    % Need to replace slashes in the dependency paths if we don't have an AttachedFileMap.
    dependencyPaths = cellfun( @(x) strrep( x, '\', '/' ), dependencyPaths, ...
        'UniformOutput', false );
    [dependencyMap, pathsToAdd] = ...
        iCalculatePathsToAdd(dependencyDir, dependencyList, dependencyPaths);
end

iAddPaths( pathsToAdd );


%--------------------------------------------------------------------------
% Helpers
%--------------------------------------------------------------------------

function dependencyMap = iCreateDependencyMap( dependencyList, pathMap, dependencyDir )
% iCreateDependencyMap - convert paths from their original client path to the
%   equivalent local path on the worker, and return a map of the conversion.

dependencyMap = cell( length( dependencyList ), 2 );
originalPaths = pathMap.keys();
% Loop over the attached files in the dependency list and convert their path
% based on the path map passed in.
for ii = 1:numel( dependencyList )
    % For the current attached file in the dependency list, search through the
    % original paths to see if any of them are contained within the attached
    % file's path, and take the longest one.
    matchIdxs = cellfun( @(x) ~isempty( strfind( dependencyList{ii}, x ) ), originalPaths );
    if ~any( matchIdxs )
        % We should always have the the original path listed, so error if we
        % can't find it.
        error( message( 'parallel:internal:NoLocalPathFoundForAttachedFile', ...
            dependencyList{ii} ) );
    end
    matchingPaths = originalPaths( matchIdxs );
    [~, maxPathIdx] = max( cellfun( @length, matchingPaths ) );
    maxPath = matchingPaths( maxPathIdx );
    maxPath = maxPath{:};

    % Replace the matching path based on the path conversion, and prepend
    % the local dependency directory to get the absolute path to the local
    % version of the file.
    newPath = pathMap.get( maxPath );
    dctSchedulerMessage( 6, ...
        sprintf( 'Replacing %s with %s in %s', maxPath, newPath, dependencyList{ii} ) );
    localRelativePath = ...
        strrep( dependencyList{ii}, maxPath, newPath );
    % The dependency map maps the file name on the client to the file name
    % on the worker.
    dependencyMap{ii, 1} = dependencyList{ii};
    dependencyMap{ii, 2} = fullfile( dependencyDir, localRelativePath );
end

function dependencyMapString = iDependencyMapToString( dependencyMap )
% iDependencyMapToString - Return a string that represents the dependency map,
%   which is a 2-column cell array of strings with keys in column 1 and values
%   in column 2.
dependencyMapString = '';
for ii = 1:size( dependencyMap, 1 )
    dependencyMapString = [ dependencyMapString, ...
        sprintf( '  %s -> %s\n', dependencyMap{ii, 1}, dependencyMap{ii, 2} ) ]; %#ok<AGROW>
end

function [dependencyMap, pathsToAdd] = iCalculatePathsToAdd(rootdir, filelist, relativefilelist)
% filelist input is a cell array of strings that we need to match up to
% files or directories on the local machine. They might be relative or
% absolute and we need to distinguish.

dependencyMap = cell(numel(filelist), 2);
% Create a cell array of all the paths that we will need to add to the
% current MATLAB path in one go
pathsToAdd = cell(size(filelist));

for i = numel(filelist):-1:1
    name = relativefilelist{i};
    localName = [rootdir filesep name];
    dctSchedulerMessage(6, ['Searching for ' localName ' ...']);
    % check for a relative path to a file or directory
    if exist(localName,'dir')
        % it is a relative path to a directory
        pathsToAdd{i} = localName;
    elseif exist(localName,'file')
        % it is a relative path to a file
        pathsToAdd{i} = fileparts(localName);
    else
        % This is here for compatibility with API-1 and
        % NewDirectoryDependencyCommand.java.
        %
        % it is an absolute path to a file or directory
        % OR
        % it is a relative path to a directory that was empty.
        % A directory could look empty because either,
        %  - a relative or absolute path to a actual empty directory
        %    was specified as a file dependency, or
        %  - a directory was specified as a file dependency, and all
        %    the files in the directory were also specified as file
        %    dependencies by their absolute path.

        % if the directory name ends in a filesep, then the
        % directory's contents will be placed by UNZIP in rootdir,
        % otherwise the directory will be placed in a subdirectory
        % of the appropriate name

        % Get the last part of the filename - this is everything after
        % the last '/', or the whole string if there are no slashes.
        seplocations = strfind(name,'/');
        if ~isempty(seplocations)
            nameEnd = name(seplocations(end)+1:end);
        else
            nameEnd = name;
        end
        localName = [rootdir filesep nameEnd];
        if name(end) == '/'
            % THIS CODE PATH SHOULD NEVER BE CALLED - whilst setting the
            % file dependencies on the job we are checking to see if the
            % element ends with a file separator and removing it.
            dctSchedulerMessage(1, 'File dependency with trailing file separator found - this was NOT expected.');
            % there was a filesep on the end of the directory name
            pathsToAdd{i} = rootdir;
        elseif exist(localName,'dir')
            % there was no filesep on the end of the directory name
            pathsToAdd{i} = localName;
        elseif exist(localName,'file')
            % it is an absolute file name
            pathsToAdd{i} = rootdir;
        else 
            % To get here the filelist entry can not be found locally.
            % This is probably because it was an empty dir and so didn't
            % get added to the zip file. We need to create it here so
            % any files added to this dir can get updated correctly.
            dctSchedulerMessage(1, 'File dependency can not be found - creating dir %s', localName);
            mkdir(localName);
            pathsToAdd{i} = localName;            
        end
    end
    dependencyMap(i, :) = { filelist{i}  localName };
end

function iAddPaths( pathsToAdd )
% Remove any empty elements
pathsToAdd(cellfun(@isempty, pathsToAdd)) = [];

[~, index] = unique(pathsToAdd, 'first');
pathsToAdd = pathsToAdd(sort(index));
if ~isempty(pathsToAdd)
    addpath(pathsToAdd{:});
end
