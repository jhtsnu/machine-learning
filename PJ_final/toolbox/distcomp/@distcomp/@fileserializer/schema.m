function schema
%SCHEMA defines the distcomp.fileserializer class
%

%   Copyright 2005-2012 The MathWorks, Inc.


hThisPackage = findpackage('distcomp');
hParentClass = hThisPackage.findclass('serializer');
hThisClass   = schema.class(hThisPackage, 'fileserializer', hParentClass);
mlock

jobEnum  = findtype('distcomp.jobexecutionstate');
taskEnum = findtype('distcomp.taskexecutionstate');

p = schema.prop(hThisClass, 'ValidStateStrings', 'string vector');
p.AccessFlags.Listener = 'off';
p.AccessFlags.Init = 'on';
p.FactoryValue = union(jobEnum.Strings, taskEnum.Strings);
p.AccessFlags.AbortSet  = 'off';
