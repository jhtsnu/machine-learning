function pPutFields(obj, entities, names, values, saveFlags)
; %#ok Undocumented
%pPutFields private put field function which has flags to save
%
%  PPUTFIELDS(SERIALIZER, LOCATION, NAMES, VALUES)

% Copyright 2004-2012 The MathWorks, Inc.


numEntities = numel(entities);
if numel(names) ~= numel(values)
    error( message( 'parallel:cluster:FileSerializerNumFieldsValuesNotEqual' ) );
end
% Return early if there is nothing to put into
if numEntities == 0
    return
end
% Ensure the names and values are columns
names = reshape(names, numel(names), 1);
values = reshape(values, numel(values), 1);

storage = obj.Storage;
% Ensure that we have a location plus a file separator
locationStart = fullfile(storage.StorageLocation, filesep);

% Need to decide which names go in which internal files
[extensions, fileFormats] = storage.pGetExtensionsForFields(entities(1).pGetEntityType, names);

% Order by common extension - the output index is the index into the new
% extensions array for the equivalent name
[extensions, I, J] = unique(extensions);
fileFormats = fileFormats(I);

% Get all entityLocations outside the loop
entityLocations = cell(numEntities, 1);
for i = 1:numEntities
    entityLocations{i} = entities(i).pGetEntityLocation;
end

% Now actually save the fields and values
for i = 1:numel(extensions)
    % Get this extension
    extension = extensions{i};
    % Which names are saved in this file
    indexToSave = find(J == i);
    % Construct a structure to save from
    structToSave = cell2struct(values(indexToSave), names(indexToSave));
    % Define the load function to use
    switch fileFormats{i}
        case 'MAT_FILE'
            saveFunction = @iSaveMat;
        case 'STATE_FILE'
            saveFunction = @iSaveState;
        case 'DATE_FILE'
            saveFunction = @iSaveDate;
        case 'TEXT_FILE'
            saveFunction = @iSaveText;
    end
    for j = 1:numEntities
        % Get the complete name of this location
        thisLocation = [locationStart entityLocations{j} extension];        
        saveFunction(thisLocation, structToSave, saveFlags);
    end

end

%--------------------------------------------------------------------------
% Definition of the State File
%--------------------------------------------------------------------------
function iSaveState(filename, data, ~)
% If we get in here then the data to be saved has a state field
[fid, msg] = fopen(filename, 'wt');
if fid > 0
    handleCloser = onCleanup( @() fclose( fid ) );
    fprintf(fid, '%s\n', data.state);
else
    error( message( 'parallel:cluster:CouldNotOpenFile', filename, msg ) );
end

%--------------------------------------------------------------------------
% Definition of the Mat File 
%--------------------------------------------------------------------------
function iSaveMat(filename, data, saveFlags) %#ok<INUSL>
save(filename, '-struct', 'data', saveFlags);

%--------------------------------------------------------------------------
% Definition of the Date File 
%--------------------------------------------------------------------------
function iSaveDate( filename, data, ~ ) 

toWrite = '';
if isstruct( data )
    fn  = fieldnames( data );
    % only ever pick starttime or finishtime. Ugh.
    if ismember( 'starttime', fn ) && ischar( data.starttime )
        toWrite = data.starttime;
    elseif ismember( 'finishtime', fn ) && ischar( data.finishtime )
        toWrite = data.finishtime;
    else
        warning( message('parallel:cluster:FileSerializerUnknownField', ...
                 filename ) );
    end
end

[fid, msg] = fopen( filename, 'wt' );
if fid > 0
    handleCloser = onCleanup( @() fclose( fid ) );
    fprintf( fid, '%s', toWrite );
else
    % only warn - might get here (especially on windows) if a concurrent write
    % is in progress.
    warning( message( 'parallel:cluster:CouldNotOpenFile', filename, msg ) );
end

function iSaveText( filename, data, ~ )

fid = fopen( filename, 'a' );
if fid > 0
    handleCloser = onCleanup( @() fclose( fid ) );
    % We can only write one thing at a time, so assert that there is only
    % one field of the struct and write it to the file.
    fn = fieldnames( data );
    assert(numel(fn) == 1, 'More than one field in the struct provided to iSaveText');
    toWrite = data.(fn{:});
    fprintf( fid, '%s', toWrite );
else
    % If we can't open the file just give up, we don't want the task
    % to fail just because we couldn't write some of the diary output.
    return
end
