function val = pGetTimeout(task, val)
; %#ok Undocumented
%PGETTIMEOUT A short description of the function
%
%  VAL = PGETTIMEOUT(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    lVal = proxyTask.getTimeout(task.UUID); 
    % Check if the number is INTMAX for int64
    if isequal(lVal, intmax('int64'))
        val = Inf;
    else
        val = double(lVal) / 1000; % convert to seconds
    end
catch %#ok<CTCH> swallow errors
	% TODO
end
