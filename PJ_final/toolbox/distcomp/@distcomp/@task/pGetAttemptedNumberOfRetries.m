function val = pGetAttemptedNumberOfRetries(task, val)
; %#ok Undocumented
%PGETATTEMPTEDNUMBEROFRETRIES Retrieves the number of task rerun attempts
%
%  VAL = PGETATTEMPTEDNUMBEROFRETRIES(TASK, VAL)

%  Copyright 2008-2012 The MathWorks, Inc.


try    
    if task.HasProxyObject
        val = task.ProxyObject.getAttemptedNumberOfRetries(task.UUID);
    end
catch %#ok<CTCH> swallow errors
    % Do not throw any errors.
end
