function val = pGetRunningDuration(task)

%   Copyright 2010 The MathWorks, Inc.

; %#ok Undocumented
%PGETRUNNINGDURATION
%
%  VAL = PGETRUNNINGDURATION(TASK)

% This is not a true get function as running duration is not yet a property
% of jobs and tasks, so val is not passed in.
proxyTask = task.ProxyObject;
if ~isempty(proxyTask)
    try
        val = double(proxyTask.getRunningDuration(task.UUID));
    catch err %#ok<NASGU>
        % If there was a problem on the remote side we have no information
        % about the running duration and so we return empty
        val = [];
    end
end
