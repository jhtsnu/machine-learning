function val = pGetLogLevel(task, ~)
; %#ok Undocumented
%PGETLOGLEVEL The level at which the task execution will be logged
%
%  VAL = PGETLOGLEVEL(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    val = proxyTask.getLogLevel(task.UUID);
catch err
    throw(distcomp.handleJavaException(task, err));
end
