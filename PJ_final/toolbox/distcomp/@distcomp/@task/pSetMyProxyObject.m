function pSetMyProxyObject(task, event)
; %#ok Undocumented
%pSetMyProxyObject 
%
%  pSetMyProxyObject(src, event)

%  Copyright 2005-2010 The MathWorks, Inc.

%  $Revision: 1.1.10.4 $    $Date: 2010/12/27 01:10:25 $ 

job = event.NewParent;
proxy = job.pGetTaskAccessProxy;
task.ProxyObject = proxy;
task.HasProxyObject = true;

% If there's a new job manager proxy, there will be a new remote event
% listener, and we will need to re-register for events with it.
if task.ProxyToUddAdaptorRegistrationCount > 0
    jobmanager = job.Parent;
    jobmanager.pRegisterForEvents(task.ProxyObject, task.UUID);
end
