function val = pGetError(task, val)
; %#ok Undocumented
%PGETERROR A short description of the function
%
%  VAL = PGETERROR(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.

try
    smallItems = task.pGetWorkUnitInfoSmallItems();
    errorBytes = smallItems.getErrorStruct();
    errID = char(smallItems.getErrorIdentifier());
    errMessage = char(smallItems.getErrorMessage());
    if ~isempty(errorBytes)
        val = distcompdeserialize(errorBytes);
    end
    % If the error itself is empty, but either of the ErrorMessage
    % or ErrorIdentifier are non-empty, then build the appropriate
    % MException from these
    if isempty(val) && ~(isempty(errID) && isempty(errMessage))
        val = MException(errID, '%s', errMessage);
    end
catch err %#ok<NASGU>
end
