function val = pGetCaptureCommandWindowOutput(task, val)
; %#ok Undocumented
%PGETCAPTURECOMMANDWINDOWOUTPUT A short description of the function
%
%  VAL = PGETCAPTURECOMMANDWINDOWOUTPUT(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    val = proxyTask.getCaptureCommandWindowOutput(task.UUID);
catch %#ok<CTCH> swallow errors
	% TODO
end
