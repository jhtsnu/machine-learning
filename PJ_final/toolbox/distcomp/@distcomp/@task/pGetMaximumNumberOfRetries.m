function val = pGetMaximumNumberOfRetries(task, val)
; %#ok Undocumented
%PGETMAXIMUMNUMBEROFRETRIES Retrieves the maximum number of task rerun attempts
%
%  VAL = PGETMAXIMUMNUMBEROFRETRIES(TASK, VAL)

%  Copyright 2008-2012 The MathWorks, Inc.


try
    if task.HasProxyObject
        val = task.ProxyObject.getMaximumNumberOfRetries(task.UUID);
    end
catch %#ok<CTCH> swallow errors
    % Do not throw any errors.
end
