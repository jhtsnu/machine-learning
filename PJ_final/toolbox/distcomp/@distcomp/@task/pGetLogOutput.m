function val = pGetLogOutput(task, ~)
; %#ok Undocumented
%PGETLOGOUTPUT Get the logged output from task execution
%
%  VAL = PGETLOGOUTPUT(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    val = char(proxyTask.getLogOutput(task.UUID));
catch err
    throw(distcomp.handleJavaException(task, err));
end
