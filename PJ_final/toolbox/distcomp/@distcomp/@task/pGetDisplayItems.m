function cellargout = pGetDisplayItems(obj, inStruct)
; %#ok Undocumented
% gets the common display structure. outputs a cell arrays of inStructs

% Copyright 2006-2011 The MathWorks, Inc.

import parallel.internal.cluster.DisplayFormatter

% initialize number of output arguments
cellargout = cell(3, 1);
% initialize output structures based on same format
mainStruct = inStruct;
taskResultStruct = inStruct;
specificStruct = inStruct;

% Get the task information in one RMI call via the WorkUnitInfoSmallItems
info = obj.pGetWorkUnitInfoSmallItems;
objectState = iConvertState(info.getState);

if strcmp(objectState, 'unavailable') || strcmp(objectState, 'destroyed')
    mainStruct.Header = 'Task ID';
else
    mainStruct.Header = ['Task ID ' num2str(info.getNum) ' from Job ID ' num2str(obj.Parent.ID)];
end

mainStruct.Type = 'task';
mainStruct.Names = {'State', 'Function', 'StartTime', 'Running Duration'};

runningDuration = double(info.getRunningDuration);
formattedRunningDuration = DisplayFormatter.formatRunningDuration(runningDuration);

mainStruct.Values = {objectState, char(info.getMLFunctionName), ...
    char(info.getStartTime), formattedRunningDuration};

% Length of output list of parameters and values must of the same length
% Need to know how long the error message is to define the
% taskResultsStruct
errorID  = char(info.getErrorIdentifier);
errorMsg = char(info.getErrorMessage);
splitErrorMessage = iGetStringCell(errorMsg);
nerrorlines = numel(splitErrorMessage);

taskResultStruct.Header = 'Task Result Properties';
% preallocate additional store if error message is large
taskResultStruct.Names  = cell(nerrorlines+1, 1);
taskResultStruct.Values = cell(nerrorlines+1, 1);
taskResultStruct.Names(1:2) = {'ErrorIdentifier','ErrorMessage'};


taskResultStruct.Values{1}  = errorID;
% error message is broken up before it is output
taskResultStruct.Values(2:end) = splitErrorMessage;
errorStruct = iConvertErrorBytes(info.getErrorStruct);
if ~isempty(errorStruct)
    numStackItems = numel(errorStruct.stack);
else
    numStackItems = 0;
end

% ignores first stackFramesToIgnore as they are DCT functions not user code
stackFramesToIgnore = 7;
numStackItems = max(0, numStackItems - stackFramesToIgnore);
% only show the Error Stack if there is an error stack to display
if ~isempty(errorStruct) && numStackItems > 0
    taskResultStruct.Names{end+1} = 'Error Stack';
    taskResultStruct.Values{end+1} = '';
    startIndex = numel(taskResultStruct.Values)-1;

    % allocate number of additional lines required
    if numStackItems > 1
        taskResultStruct.Names{end+numStackItems-1} = [];
        taskResultStruct.Values{end+numStackItems-1} = [];
    end

    for ii = 1:numStackItems
        thisItem = errorStruct.stack(ii);
        % Get filename from full path
        [~, displayFile] = fileparts(thisItem.file);
        % Is it a subfunction, nested function etc.
        if ~strcmp(displayFile, thisItem.name)
            displayFileItem = [displayFile '>' thisItem.name];
        else
            displayFileItem = [displayFile '.m'];
        end
        taskResultStruct.Values{startIndex+ii} = [displayFileItem ' at ' num2str(thisItem.line)];
    end
end

workername = '';
workerProxy = info.getWorker;
if ~isempty(workerProxy)
    workername = DisplayFormatter.formatWorkerName(char(workerProxy.getName), ...
        char(workerProxy.getHostName));
end

specificStruct.Header = 'Jobmanager Dependent Properties';
specificStruct.Names = {'Worker Location', 'RunningFcn', 'FinishedFcn', ...
                    'Timeout', 'MaximumNumberOfRetries', 'AttemptedNumberOfRetries'};
specificStruct.Values = {workername, obj.RunningFcn, obj.FinishedFcn, ...
                    iConvertTimeout(info.getTimeout), ...
                    info.getMaximumNumberOfRetries, ...
                    iGetAttemptedNumberOfRetries(info)};


cellargout{1} = mainStruct;   % all three categories are sent to top level pDefaultSingleObjDisplay
cellargout{2} = taskResultStruct;
cellargout{3} = specificStruct;
end

%--------------------------------------------------------------------------
% internal helper function returns the attempted number of retries
%--------------------------------------------------------------------------
function retries = iGetAttemptedNumberOfRetries(info)

% The retry information is a vector of WorkUnitInfo objects.
attemptInfo = info.getFailedAttemptInformation;

% The length of this vector is the attempted number of retries.
if ~isempty(attemptInfo)
    retries = attemptInfo.size();
else
    retries = 0;
end
end
%--------------------------------------------------------------------------
% internal helper function returns a empty space if item is empty
% and breaks up very long character strings into cell arrays
%--------------------------------------------------------------------------
function y = iGetStringCell(str)  

% Number of characters before a break up ( for wrap around) of error message
% string - because we want the total line to wrap at the command window
% length provided by external mex function
N = iGetTruncationLength();

if isempty(str)
    y = {''};
    return;
end
% break up based on linefeed (returns original if none found)
y = textscan(str, '%s', 'delimiter', '\n');
y = y{1};

% otherwise if there is no linefeed then break up based on the number of characters
if (numel(y) == 1) && numel(str) > N
    divisible = ceil(numel(str)/N); % guaranteed to be 1 or greater
    y{divisible} = [];%preallocate
    startI = 0;
    for ic = 1:divisible-1
        startI = (ic-1)*N;
        y{ic} = str(startI+1:startI+N);
    end
    y{divisible} = str(startI+N+1:end);  % last bit of item
end
end


%--------------------------------------------------------------------------
%--------------------------------------------------------------------------
function STR_LEN_THRESH = iGetTruncationLength()
% STR_LEN_THRESH defines the cut off  threshold for truncating string values of properties
% call c mex function to get command window size
STR_LEN_THRESH = 46;
headerSize = 25;
try
    cmdWSize = distcomp.dctCmdWindowSize();
    % make sure cmdWSize is something sensible
    if cmdWSize > (STR_LEN_THRESH + headerSize) && cmdWSize < 5000
        STR_LEN_THRESH = cmdWSize - headerSize;
    end
catch %#ok<CTCH>
end
end

function errorStruct = iConvertErrorBytes(errorBytes)
% Deserialize the error byte array and return the error struct
errorStruct = MException.empty();
if ~isempty(errorBytes)
    errorStruct = distcompdeserialize(errorBytes);
end
end

function timeout = iConvertTimeout(timeout)
% Convert the timeout from java to double or inf if the timeout is
% intmax('int64')

% timeout has probably already been automatically converted to double, but
% double(intmax('int64')) does NOT equal intmax('int64') as it is not
% representable accurately as a double. We must therefore test if timeout
% is equal to double(intmax('int64')) to see if the timeout has not been
% set.
timeout = double(timeout);
if isequal(timeout, double(intmax('int64')))
    timeout = Inf;
else
    timeout = timeout / 1000; % convert to seconds
end
end

function outState = iConvertState(inState)
    
persistent Values Strings
if isempty(Values)
    types = findtype('distcomp.taskexecutionstate');
    Values = types.Values;
    Strings = types.Strings;
end 

outState = Strings{inState == Values};
end
