function val = pGetNumberOfOutputArguments(task, val)
; %#ok Undocumented
%PGETNUMBEROFOUTPUTARGUMENTS A short description of the function
%
%  VAL = PGETNUMBEROFOUTPUTARGUMENTS(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    val = proxyTask.getNumOutArgs(task.UUID);
catch %#ok<CTCH> swallow errors
	% TODO
end
