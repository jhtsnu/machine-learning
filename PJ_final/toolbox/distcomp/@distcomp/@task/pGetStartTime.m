function val = pGetStartTime(task, val)
; %#ok Undocumented
%PGETSTARTTIME A short description of the function
%
%  VAL = PGETSTARTTIME(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    val = char(proxyTask.getStartTime(task.UUID));
catch %#ok<CTCH> swallow errors
	% TODO
end
