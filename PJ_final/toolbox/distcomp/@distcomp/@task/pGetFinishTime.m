function val = pGetFinishTime(task, val)
; %#ok Undocumented
%PGETFINISHTIME A short description of the function
%
%  VAL = PGETFINISHTIME(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    val = char(proxyTask.getFinishTime(task.UUID));
catch %#ok<CTCH> swallow errors
	% TODO
end
