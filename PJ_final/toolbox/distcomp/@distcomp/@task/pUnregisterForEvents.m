function pUnregisterForEvents(obj)
; %#ok Undocumented
%pUnregisterForEvents unregister events with the root object
%
%  pUnregisterForEvents(OBJ)

%  Copyright 2007-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.2 $    $Date: 2010/12/27 01:10:26 $ 

% We are going to have to do something a little different if we are filling
% up a TaskInfo object rather than a proxyobject
HAS_TASKINFO = ~isempty(obj.TaskInfo);

registrationCount = obj.ProxyToUddAdaptorRegistrationCount;
% First check we aren't decrementing a zero event count
if registrationCount < 1
    warning('distcomp:proxyobject:InvalidState', 'Attempting to unregister for events before registering'); 
    return
end

registrationCount = registrationCount - 1;
% If the registration count has reached zero then unregister with the root
if registrationCount == 0
    job = obj.Parent;
    jobmanager = job.Parent;
    if HAS_TASKINFO
        obj.TaskInfo.setListenerInfo([]);
    else
        jobmanager.pUnregisterForEvents(obj.ProxyObject, obj.UUID);
    end
end

obj.ProxyToUddAdaptorRegistrationCount = registrationCount;
