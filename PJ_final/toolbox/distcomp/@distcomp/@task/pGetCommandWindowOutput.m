function val = pGetCommandWindowOutput(task, ~)
; %#ok Undocumented
%PGETCOMMANDWINDOWOUTPUT A short description of the function
%
%  VAL = PGETCOMMANDWINDOWOUTPUT(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    dataItem = proxyTask.getCommandWindowOutput(task.UUID);
    data = dataItem(1).getData();
    if ~isempty(data) && data.limit > 0
        dataItem(1).rewind;
        data = dataItem(1).getData(dataItem(1).getNumBytes)';
        % We're now storing the bytes using the native encoding
        % for compatibility with API-2 and streaming command
        % window output.
        % Java's bytes are signed, and native2unicode expects
        % uint8.
        val = native2unicode(typecast(data, 'uint8'), 'UTF-16LE');
    else
        val = '';
    end
    dataItem(1).delete();
catch err
    distcomp.handleGetLargeDataError(task, err);
    val = '';
end
