function val = pGetWorker(task, val)
; %#ok Undocumented
%pGetWorker A short description of the function
%
%  VAL = pGetWorker(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    proxyWorker = proxyTask.getWorker(task.UUID);
    if ~isempty(proxyWorker(1))
        val = distcomp.createObjectsFromProxies(proxyWorker(1), @distcomp.worker, distcomp.getdistcompobjectroot);
    end
catch %#ok<CTCH> swallow errors
    % TODO
end
