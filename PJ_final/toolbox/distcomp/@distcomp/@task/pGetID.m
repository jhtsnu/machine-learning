function val = pGetID(task, val)
; %#ok Undocumented
%PGETID A short description of the function
%
%  VAL = PGETID(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
if ~isempty(proxyTask)
    try
        val = proxyTask.getNum(task.UUID);
    catch %#ok<CTCH> swallow errors
    end
end
