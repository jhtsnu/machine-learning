function val = pGetErrorMessage(task, val)
; %#ok Undocumented
%PGETERRORMESSAGE A short description of the function
%
%  VAL = PGETERRORMESSAGE(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyTask = task.ProxyObject;
try
    val = char(proxyTask.getErrorMessage(task.UUID));
catch %#ok<CTCH> swallow errors
	% TODO
end
