function pVectorObjectDisplay(tasks)
; %#ok Undocumented
%pDefaultVectorObjDisplay - display for vector output

% Copyright 2006-2011 The MathWorks, Inc.

% Allows user configuration of end of output spacing.
LOOSE = strcmp(get(0, 'FormatSpacing'), 'loose');

persistent Strings Values
if isempty(Values)
    types = findtype('distcomp.taskexecutionstate');
    Values = types.Values;
    Strings = types.Strings;
end

desc(1) = iCreateCol('Task ID',       @(o)num2str(o.getNum()),            7, false);
desc(2) = iCreateCol('State',         @(o)Strings{o.getState()==Values}, 11, false);
desc(3) = iCreateCol('FinishTime',    @iTimeHelper,                      15, false);
desc(4) = iCreateCol('Worker',        @iWorkerHelper,                    15,  true);
desc(5) = iCreateCol('Function Name', @(o)char(o.getMLFunctionName()),   15,  true);
desc(6) = iCreateCol('Error',         @iErrorHelper,                      6, false);

title = parallel.internal.createDimensionDisplayString(tasks, 'Tasks');
disp(parallel.internal.createVectorObjectDisplayTable(iGetWorkUnitInfos(tasks), desc, title));

if LOOSE
    disp(' ');
end
end


function col = iCreateCol(title, func, width, adjust)
col = struct('title', title, 'function', func, ...
             'width', width, 'adjust', adjust);
end

function workUnitInfos = iGetWorkUnitInfos(workUnits)
% Retrieve the workUnitInfo (with no large data items)
% for all workUnits (one at a time as some of them might fail).
infoCell = cell(numel(workUnits), 1);
for i = 1:numel(workUnits)
    try 
        id = workUnits(i).pReturnUUID;
        infos = workUnits(i).pReturnProxyObject.getWorkUnitInfoSmallItems(id);
        infoCell{i} = infos(1);
    catch err %#ok<NASGU>
    end
end
workUnitInfos = [infoCell{:}];
end

function timeStr = iTimeHelper(obj)
date = obj.getFinishTime();
if isempty(date)
    timeStr = '-';
else
    formatStr = 'MMM dd HH:mm:ss';
    timeStr = java.text.SimpleDateFormat(formatStr).format(date);
end
end

function workerStr = iWorkerHelper(obj)
workerStr = '';
if ~isempty(obj.getWorker())
    workerStr = char(obj.getWorker().getName());
end
end

function errorStr = iErrorHelper(obj)
taskError = MException.empty();
errorBytes = obj.getErrorStruct();
if ~isempty(errorBytes)
    taskError = distcompdeserialize(errorBytes);
end

if ~isempty(taskError) 
    errorStr = 'Error';
else
    errorStr = '';
end
end
