function pSubmitResult(task, output, m_exception, cmd_window_output)
; %#ok Undocumented
%pSubmitResult Submit result to jobmanager.
%
%  pSubmitResult(TASK, OUTPUT, mException, cmdWindowOutput)

%  Copyright 2005-2012 The MathWorks, Inc.

% Get information about the current worker that is needed to submit 
% a result to the jobmanager.
root = distcomp.getdistcompobjectroot;
worker = root.CurrentWorker;

% Do not submit a result if the task has already been cancelled.
if ~worker.pOkayToSubmitResult
    return;
end
worker.pNotifyFunctionEvaluationComplete();

try
    % Submit the task result. The job manager can send a new task to the worker as soon as this call returns successfully.
    iSubmitResult(task, output, m_exception, cmd_window_output);
catch err
    % Behave differently depending on the actual error thrown
    switch err.identifier
        case 'distcomp:task:TooMuchData'   
            % Send an empty result with an error message to indicate that the output data 
            % cannot be transferred to the job manager.
            output = [];
            iSubmitResult(task, output, err, cmd_window_output);
        case 'parallel:task:MJSTaskNotFound'
            % No action should be taken.
            % It is possible that the task has been cancelled or destroyed whilst the 
            % worker was working on it. This worker will become idle and work on the 
            % next task sent to it by the job manager.
        otherwise
            % This is a case that shouldn't be hit in practice. Try to submit an empty result. 
            % If it fails, just error out instead of retrying the submit result.
            output = [];
            newErr = MException('distcomp:task:UnableToSubmitResult', ...
                'Unable to return results to jobmanager. The reason is :\n%s', err.message);
            newErr.addCause(err);
            iSubmitResultNoRetry(task, output, newErr, cmd_window_output);
    end
end


%--------------------------------------------------------------------------
% Internal function to submit the result of a task
%--------------------------------------------------------------------------
function iSubmitResult(task, output, exception, cmd_window_output)
% Remember to call the special ByteBufferItem constructor that checks the
% size of the input and throws an appropriate error if it can get the
% results into the JVM
results = distcompserialize(output);
% Convert the command window output to bytes in the native encoding. We do
% not serialize here because we may be appending to the command window output
% and there's no way to append to a serialized array.
cmdWinOutBytes = unicode2native(cmd_window_output, 'UTF-16LE');
% Put the data into a ByteBufferItem[] to pass to the proxy
import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem;
outputItemArray = dctJavaArray(ByteBufferItem(distcompMxArray2ByteBuffer(results)), ...
                               'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData');
cmdWinOutItemArray = dctJavaArray(ByteBufferItem(distcompMxArray2ByteBuffer(cmdWinOutBytes)), ...
                               'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData');
% Get the error identifier and message
if isempty(exception)
    error_id = '';
    error_message = '';
else
    error_id = exception.identifier;
    error_message = exception.message;
end
% Serialize the error struct
errorBytes = distcompserialize(exception);

% Loop until either the result is submitted or we detect that it is not possible to submit it.
while true
    proxyTask = task.ProxyObject;
    try
        proxyTask.submitResult(task.UUID, ...
                               outputItemArray, errorBytes, error_message, error_id, ...
                               cmdWinOutItemArray);
        % Break out of the loop if the result was submitted successfully.
        break;
    catch err
        % Try to see what went wrong and obtain the error struct.
        err = distcomp.handleJavaException(task, err);
        if strcmp(err.identifier, 'parallel:cluster:MJSUnreachable')
            % The worker should wait until the job manager becomes available.
            task.pGetManager.pWaitForJobManager();
        else
            % The job manager is reachable but we cannot submit the result.
            throw(err);
        end
    end
end


%--------------------------------------------------------------------------
% Internal function to submit an empty result after a failure
%--------------------------------------------------------------------------
function iSubmitResultNoRetry(task, output, exception, cmd_window_output)
% Remember to call the special ByteBufferItem constructor that checks the
% size of the input and throws an appropriate error if it can get the
% results into the JVM
results = distcompserialize(output);
cmdWinOutBytes = unicode2native(cmd_window_output, 'UTF-16LE');
% Put the data into a ByteBufferItem[] to pass to the proxy
import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem;
outputItemArray = dctJavaArray(ByteBufferItem(distcompMxArray2ByteBuffer(results)), ...
                               'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData');
cmdWinOutItemArray = dctJavaArray(ByteBufferItem(distcompMxArray2ByteBuffer(cmdWinOutBytes)), ...
                               'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData');
% Get the error identifier and message
error_id = exception.identifier;
error_message = exception.message;
% Serialize the error struct
errorBytes = distcompserialize(exception);

% Submit the result
proxyTask = task.ProxyObject;
proxyTask.submitResult(task.UUID, ...
                       outputItemArray, errorBytes, error_message, error_id, ...
                       cmdWinOutItemArray);

