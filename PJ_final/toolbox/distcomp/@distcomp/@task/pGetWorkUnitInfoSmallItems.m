function val = pGetWorkUnitInfoSmallItems(task)
; %#ok Undocumented
% Returns a WorkUnitInfoSmallItems object.

%   Copyright 2010-2011 The MathWorks, Inc.

try
    info = task.ProxyObject.getWorkUnitInfoSmallItems(task.UUID);
    val = info(1);
catch err
    [isJavaError, exceptionType, causes] = isJavaException(err);
    if isJavaError
        if isempty(causes)
            cause = exceptionType;
        else
            cause = causes{end};
        end
        if strcmp(cause, 'com.mathworks.toolbox.distcomp.storage.WorkUnitNotFoundException')
            objectState = -1; % destroyed
        else
            objectState = -2; % unavailable
        end
        % If we can't get the task information, set the info to default
        % values
        val = iCreateDefaultTaskInfo(objectState);
    else
        rethrow(err);
    end
end

function info = iCreateDefaultTaskInfo(objectState)
    info = com.mathworks.toolbox.distcomp.workunit.TaskInfo();
    info.setState(objectState);
