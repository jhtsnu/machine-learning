function schema
%SCHEMA defines the distcomp.setprop class
%

%   Copyright 2005-2012 The MathWorks, Inc.


hThisPackage = findpackage('distcomp');
hThisClass   = schema.class(hThisPackage, 'setprop');

% Define the fields that are needed to run a task - most of these fields
% need to be setup before we can instantiate a real task runner
p = schema.prop(hThisClass, 'StorageConstructor', 'string');
p.AccessFlags.PublicSet = 'off';

p = schema.prop(hThisClass, 'StorageLocation', 'string');
p.AccessFlags.PublicSet = 'off';

p = schema.prop(hThisClass, 'JobLocation', 'string');
p.AccessFlags.PublicSet = 'off';

p = schema.prop(hThisClass, 'TaskLocations', 'string vector');
p.AccessFlags.PublicSet = 'off';

p = schema.prop(hThisClass, 'NumberOfTasks', 'double');
p.AccessFlags.PublicSet = 'off';
p.GetFunction = @iGetNumberOfTasks;

% These two fields will hold the executable string and the arguments
% for the executable. They are separated this way to allow for easier
% quoting when being passed to third party objects.
p = schema.prop(hThisClass, 'MatlabExecutable', 'string');
p.AccessFlags.PublicSet = 'off';

p = schema.prop(hThisClass, 'MatlabArguments', 'string');
p.AccessFlags.PublicSet = 'off';

% Fields related to MHLM for MDCS.  
p = schema.prop(hThisClass, 'UseMathworksHostedLicensing', 'string');
p.AccessFlags.PublicSet = 'off';
p.FactoryValue = 'false';

p = schema.prop(hThisClass, 'UserToken', 'string');
p.AccessFlags.PublicSet = 'off';

p = schema.prop(hThisClass, 'LicenseWebID', 'string');
p.AccessFlags.PublicSet = 'off';

p = schema.prop(hThisClass, 'LicenseNumber', 'string');
p.AccessFlags.PublicSet = 'off';

function val = iGetNumberOfTasks(obj, val) %#ok
val = numel(obj.TaskLocations);
