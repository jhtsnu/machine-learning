function pSetLicensingInformation(obj, usingMHLM, userToken, webLicenseID, licenseNumber)
% pSetLicensingInformation private function to set values that need to be passed
% to workers.

%  Copyright 2012 The MathWorks, Inc.

if usingMHLM
    obj.UseMathworksHostedLicensing = 'true';
    obj.UserToken                   = userToken;
    obj.LicenseWebID                = webLicenseID;
    obj.LicenseNumber               = licenseNumber;
else
    obj.UseMathworksHostedLicensing = 'false';
    obj.UserToken                   = '';
    obj.LicenseWebID                = '';
    obj.LicenseNumber               = '';
end

