function val = pGetMinimumNumberOfWorkers(job, val)
; %#ok Undocumented
%PGETMINIMUMNUMBEROFWORKERS A short description of the function
%
%  VAL = PGETMINIMUMNUMBEROFWORKERS(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'minworkers');
    catch %#ok<CTCH> swallow errors
    end
end
