function val = pGetMaximumNumberOfWorkers(job, val)
; %#ok Undocumented
%PGETMAXIMUMNUMBEROFWORKERS A short description of the function
%
%  VAL = PGETMAXIMUMNUMBEROFWORKERS(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'maxworkers');
    catch %#ok<CTCH> swallow errors
    end
end
