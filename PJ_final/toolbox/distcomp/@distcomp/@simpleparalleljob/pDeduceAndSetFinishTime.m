function pDeduceAndSetFinishTime(job, state)
; %#ok Undocumented
%pDeduceAndSetFinishTime - deduce and set the job and task finish times if 
%                          they are not set correctly
%
%  PDEDUCEANDSETFINISHTIME(JOB, STATE)

%  Copyright 2010 The MathWorks, Inc.

%  $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:49:43 $

% Only set finish times if the job state is actually finished.
% Failed jobs will not have a finish time
if ~strcmp(state, 'finished')
    return;
end

% Ensure that the job finish time is set appropriately (it may not be set 
% if the task that was supposed to set it failed before it was able to set 
% the job finish time.  
% For parallel and matlabpool jobs, this task is always task 1.
if isempty(job.FinishTime)
    finishTimeFromTasks = job.pGetFinishTimeFromTasks();
    job.pSetFinishTime(finishTimeFromTasks);
end

jobFinishTime = job.FinishTime;

% Check that a finish time has been set for all other tasks, since all the tasks
% depend on one another and ought to have the same state.  
tasks = job.Tasks;
for ii = 1:numel(tasks)
    currentTask = tasks(ii);
    if strcmp(currentTask.State, 'finished') && isempty(currentTask.FinishTime)
        currentTask.pSetFinishTime(jobFinishTime);
    end
end

