function schema
%SCHEMA defines the distcomp.fileserializer class
%

%   Copyright 2005-2012 The MathWorks, Inc.


hThisPackage = findpackage('distcomp');
hParentClass = hThisPackage.findclass('lockedobject');
hThisClass   = schema.class(hThisPackage, 'serializer', hParentClass);
mlock

p = schema.prop(hThisClass, 'Storage', 'handle');
p.AccessFlags.PublicSet = 'off';