function pMpiInit( job, task )
; %#ok Undocumented
%pMpiInit - initialize MPI for a parallel job

%  Copyright 2000-2012 The MathWorks, Inc.

% Force the "sock" option, as this is the only one that will work under the
% jobmanager.
mpiInit( 'sock' );
dctSchedulerMessage(4, 'mpiInit(''sock'') completed ok');

% This is a new parallel session
mpiParallelSessionStarting;

% "Leading task" is assigned by the JM during dispatch of the job.
% Find out if this task is the leading task.
isleadingtask = job.pIsLeadingTask( task );

if isleadingtask
    USING_LINEAR_CONN = strcmpi(getenv('MDCE_CONNECTACCEPT'), 'linear');
    USING_BIN_TREE_CONN = ~USING_LINEAR_CONN;
    
    if USING_LINEAR_CONN
        % Open the port and set the parallel tag
        port = mpigateway( 'openport' );
        connectMethod = 'linear';        
    elseif USING_BIN_TREE_CONN
        port = 'no port';
        connectMethod = 'binary';        
    end
    % Set the parallel tag 
    job.pSetParallelTag( sprintf( '%s\n%s\n%s', connectMethod, port, computer() ) );
    dctSchedulerMessage(4, 'Leading task parallel tag ... method : %s, port : %s, comp : %s', connectMethod, port, computer());
end

% All wait until the jobmanager has dispatched all the tasks - so that we
% know how many there are in the parallel job.

% Extract parallel job setup info
pinfo = job.pGetParallelInfo;
% Wait until setup info is retrieved from the job manager.
while isempty( pinfo )
    pause( 0.5 ); % A shortish wait
    pinfo = job.pGetParallelInfo;
end

ptag = char(pinfo.getParallelTag);
try
    [connectMethod, port, comp] = iDecodeParallelTag( ptag );
    dctSchedulerMessage(4, 'parallel tag decode: method : %s, port : %s, comp : %s', connectMethod, port, comp);
catch err
    dctSchedulerMessage(4, 'Error in iDecodeParallelTag:\n %s', err.getReport() );
    rethrow(err);
end
if ~isleadingtask
    iCompatCheck( comp, ptag );
end
% Based on the parallel tag we decide if we are doing a binary or linear
% connection method.
if strcmpi(connectMethod, 'binary')
    iBinaryTreeConnectAccept(job, task, pinfo);
elseif strcmpi(connectMethod, 'linear')
    iLinearConnectAccept(job, task, pinfo, port, isleadingtask);
else
    error('distcomp:mpi:UnknownConnectionMethod', 'Unknown connection method (%s)', connectMethod);
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iBinaryTreeConnectAccept(job, task, pinfo)
dctSchedulerMessage(4, 'Doing a binary tree connection');

N = pinfo.getNumLabs;
dctSchedulerMessage(4, 'Got parallel info with num labs: %d', N);

% Create a CancelWatchdog to make sure it doesn't take us too long to
% connect/accept. Choose a timeout based on the expected number of labs.
timeout = max( 120, 4 * N );
cw = com.mathworks.toolbox.distcomp.util.CancelWatchdog( job.ProxyObject, ...
                     job.UUID, ...
                     'Timeout exceeded during parallel connection', timeout );

dctSchedulerMessage(4, 'MPI connect watchdog started with timeout %d seconds', timeout);
try
    % Get my ID
    ID = task.ID;
    
    % Get this worker proxy and set
    thisW = task.Worker.pReturnProxyObject();
    jobToken = thisW.getCurrentJobAndTask();
    
    % Now get the worker to my left
    if ID > 1
        prevW = iPollForWorkerOnTask( job.Tasks(ID - 1) );
    else
        prevW = [];
    end

    iJoinRing( ID, N, prevW, thisW, jobToken );
    cw.ok();
catch err
    dctSchedulerMessage(2, 'Caught error during iBinaryTreeConnectAccept. Report is:\n%s', err.getReport() );
    cw.ok();
    rethrow(err)
end

function iJoinRing(myID, N, prevW, thisW, jobToken )
% This function joins an MPI ring together using a binary tree approach.
% The only external communication needed is to the worker with an ID one
% less than our ID (i.e. lab N only needs to join in to the ring that lab
% N-1 is part of). A pictorial version of the join is as follows ...
%
%    1   2   3   4   5   6   7   8   9   10   11   12   13
% 1.  <->     <->     <->     <->     <->       <->     
% 2.      <->             <->              <->
% 3.              <->                                <->
% 4.                              <->
%
% The algorithm is as follows. Firstly transform the one-based myID of the
% task to be zero-based ID for convenience. Also compute how many rounds of
% joining will occur ( ceil(log2(N)) ). Then for a given round (i) of 
% connection an ID will CONNECT if the i'th bit in the ID is set. Hence the 
% iConnectMask. An ID will ACCEPT if it is not CONNECTING and there is
% someone to connect to it (which can be deduced from the total number of
% IDs trying to join). Hence iAcceptMask and shouldAccept.
%
% It is possible that during a round of communication a given ID is neither
% ACCEPTING or CONNECTING. It is NOT possible to CONNECT and ACCEPT in a
% round. Hence the ~shouldConnect in the definition of shouldAccept.
%
% Once a given ID in a given round has decided that it will be ACCEPTING or
% CONNECTING it needs to decide if it is LEADER in this round. Given that
% after a round of communication we have a working MPI communicator this
% question is relatively simple. We are a CONNECTION LEADER if our labindex
% is 1 and an ACCEPTING LEADER if our labindex is numlabs.
%
% If we are an ACCEPTING LEADER then we create an MPI port and tell all the
% labs in our group what it is. They all then call accept on that port,
% whilst we post the port to our worker (setParallelPortInfo) and then also
% accept.
%
% If we are a CONNECTING LEADER then we wait for the worker with ID-1 to
% post the parallelPortInfo (see WorkerImpl for the wait code), and when we
% have received it we broadcast it around to our current peers and then
% connect.
% 
% At the end of the accept/connect round we have a new MPI communicator for
% which is the size of the 2 that joined together, with the ACCEPTORS being
% the low indices and the CONNECTORS the high indices.
%
% NOTE (VERY IMPORTANT) - do not change this algorithm to one where we
% first find out the port to our (ID-1) and then go into a connect/accept
% phase. There is a bug in MPICH2 which manifests as a SEG-V - the trigger
% is the following:
%   1. Thus process opens an MPI port to accept on
%   2. Another pair of processes try to connect to this port (jointly)
%   3. This process then tries to connect to a single process
%   4. This process SEG-V's
%
% The above represents a race-condition on the binary tree connection if
% all processes know their (ID-1) port in advance. However by introducing
% the worker synchronization into the algorithm below we happen to eliminate
% the above race condition, since once you know you are an ACCEPTOR you are
% all NOT connecting, and you can't get the connection info until ALL
% ACCEPTORS are ready.

% Make the ID's zero-based for simplicity
ID = myID - 1;

% How many connect / accept rounds are going to be needed
nRounds = ceil(log2(N));
dctSchedulerMessage(5, 'Expecting %d rounds of connect/accept', nRounds);

for i = 1:nRounds    
    % Are we connecting or accepting this round?
    iConnectMask = bitset(0, i);
    iAcceptMask  = iConnectMask - 1;
    shouldConnect = logical(bitand(ID, iConnectMask));
    % If I am not connecting then compute the (zero based) ID of the worker
    % that would be connecting to me - this is the OR of my ID and
    % iAcceptMask (which is the ID of the worker that is root ACCEPTING)
    % plus 1 - giving the ID of the worker CONNECTING
    myRootConnectingID = bitor(ID, iAcceptMask) + 1;
    % I should accept if I am NOT CONNECTING and there are workers that
    % should connect. '>' is the correct test here (EE & JM agree!) because
    % an ID can only exist if it is strictly < N; so N must be strictly '>'
    % an ID for that ID to exist.
    shouldAccept  = ~shouldConnect && N > myRootConnectingID;    
    
    dctSchedulerMessage(5, 'In round %d (A:%d,C:%d)... \n', i, shouldAccept, shouldConnect);
    % Define once since it is used as a dummy input to labBroadcast in 
    portToUse = '';
    
    if shouldConnect
        % During the connect phase the lab with index 1 will find the port
        % to connect to by looking for the parallel port info from the
        % worker with ID-1
        leaderIndex = 1;
        isLeader = labindex == leaderIndex;
        if isLeader
            dctSchedulerMessage(5, 'Connect leader:\n  %s', char(jobToken));
            portToUse = iPollForParallelPortInfo( prevW, jobToken );
            dctSchedulerMessage(5, 'Received parallel port info:\n  %s', portToUse);
        end
        
        dctSchedulerMessage(5, 'About to labBroadcast ... ');
        portToUse = labBroadcast(leaderIndex, portToUse);
        
        dctSchedulerMessage(5, 'About to clientconnone on:\n  %s', portToUse);
        mpigateway( 'clientconnone', portToUse, leaderIndex );
    end
    
    if shouldAccept
        leaderIndex = numlabs;
        isLeader = labindex == leaderIndex;
        if isLeader
            portToUse = mpigateway('openport');                        
            dctSchedulerMessage(5, 'Accept leader : opened port:\n  %s\n  %s', portToUse, char(jobToken));
        end
        
        dctSchedulerMessage(4, 'About to labBroadcast ... ');
        portToUse = labBroadcast(leaderIndex, portToUse);
        
        if isLeader
            % It is IMPORTANT to set this AFTER the broadcast above. This
            % ensures that all ACCEPTORS are NOT connecting at the moment.
            thisW.setParallelPortInfo( jobToken, portToUse );
        end
        
        dctSchedulerMessage(5, 'About to servacceptone on:\n  %s', portToUse);
        mpigateway( 'servacceptone', portToUse, leaderIndex );
    end  
    
end
dctSchedulerMessage(5, 'Finished %d rounds of connect/accept', nRounds);

function worker = iPollForWorkerOnTask(task)
while isempty(task.Worker)
    pause(0.5);
end
worker = task.Worker.pReturnProxyObject();

function port = iPollForParallelPortInfo(worker, jobToken)
port = '';
while isempty(port)
    % This function times out after 10s - ensure 
    port = char(worker.waitforParallelPortInfo( jobToken, 10, java.util.concurrent.TimeUnit.SECONDS ));
    if isempty(port)
        dctSchedulerMessage(5, 'Still waiting for port info on (%s) ...', char(jobToken) );
    end
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iLinearConnectAccept(job, task, pinfo, port, isleadingtask)
dctSchedulerMessage(4, 'Doing a linear connection');
% Everyone needs to know the expected world size and the parallel tag.
N = pinfo.getNumLabs;

% Create a CancelWatchdog to make sure it doesn't take us too long to
% connect/accept. Choose a timeout based on the expected number of labs.
timeout = max( 120, 4 * N );
cw = com.mathworks.toolbox.distcomp.util.CancelWatchdog( job.ProxyObject, ...
                     job.UUID, ...
                     'Timeout exceeded during parallel connection', timeout );
err = [];
try
    if isleadingtask
        % accept the other connections - with timeout
        mpigateway( 'servaccept', port, N-1 );
    else
        % Connect up
        mpigateway( 'clientconn', port, N-1 );
    end
catch exception
    err = exception;
end
% We're OK - so call off the watchdog.
cw.ok();

% If connect/accept threw an error, rethrow that.
if ~isempty( err )
    rethrow( err );
end

% Set the labindex as desired
tasksByIndex = gcat( task.ID );
[junk, desOrder] = sort( tasksByIndex ); %#ok
mpigateway( 'desiredlabordering', desOrder );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iCompatCheck - check compatibility of computers. The criteria are that the
% word sizes and endianness must match. 
function iCompatCheck( comp, ptag )

% We should never get a badly formatted tag, since the job is locked when
% being modified.
if isempty( comp )
    error( 'distcomp:mpi:init', 'Badly formatted parallel tag: <%s>', ptag );
end

import com.mathworks.toolbox.distcomp.pml.LabCompatibilityChecker;

mycomp = computer;
compatible = LabCompatibilityChecker.instance.areComputersCompatible(comp, mycomp);

if ~compatible
   error( 'distcomp:mpi:incompatible', ...
          'Compatibility check failed: worker type %s is not compatible with leading worker type %s', ...
          mycomp, comp );
end


function [connectMethod, port, comp] = iDecodeParallelTag( ptag )
% Check that the this computer is compatible with the computer which opened
% the port
strs = regexp(ptag, '\n+', 'split');
[connectMethod, port, comp] = deal(strs{:});
