function str = pFunc2Str(obj, fcn) %#ok<INUSL>
; %#ok Undocumented
%pFunc2Str - return a string from a function handle or char
%
%  STR = pFunc2Str(SCHEDULER, FCN)

%  Copyright 2008-2012 The MathWorks, Inc.


if ischar( fcn )
    str = fcn;
elseif isa( fcn, 'function_handle' )
    str = func2str( fcn );
else
    % Should never get here because of the "MATLAB Callback" check from UDD
    str = sprintf( 'object of type "%s"', class( fcn ) );
end
