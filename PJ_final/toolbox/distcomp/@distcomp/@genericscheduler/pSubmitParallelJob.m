function pSubmitParallelJob(scheduler, job)
; %#ok Undocumented
%pSubmitJob A short description of the function
%
%  pSubmitJob(SCHEDULER, JOB)

%  Copyright 2005-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.3 $    $Date: 2010/11/08 02:19:31 $ 

% Ensure we have a submit function to try
if isempty(scheduler.ParallelSubmitFcn)
    error('distcomp:genericscheduler:InvalidState', ...
          ['You must define a ParallelSubmitFcn to ', ...
           'use a generic scheduler with parallel jobs'] );
end

% Check the consistency of min workers and cluster size
scheduler.pMinWorkersClusterSizeConsistentCheck(job);

% Duplicate the tasks for parallel execution
job.pDuplicateTasks;

scheduler.pSubmitJobCommon( job, scheduler.ParallelSubmitFcn );