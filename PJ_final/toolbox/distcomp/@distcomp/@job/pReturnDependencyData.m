function [fileList, pathList, zipData] = pReturnDependencyData(job)
; %#ok Undocumented
%pReturnDependencyData 
%
%  [FILELIST, ZIPDATA] = pReturnDependencyData(JOB)

%  Copyright 2005-2012 The MathWorks, Inc.

%  $Revision: 1.1.10.7 $    $Date: 2012/10/24 04:12:22 $ 

proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        fileList = cell(proxyJob.getFileDepPathList(job.UUID));
        % This is required for compatibility with API-2, which requires an
        % extra output argument - the list of paths to the attached files
        % relative to the zip archive root. This is irrelevant in API-1 so
        % we just duplicate the output arguments.
        pathList = fileList;
        itemList = proxyJob.getFileDepData(job.UUID);
        data = itemList(1).getData;
        if ~isempty(data) && data.limit > 0
            % Define the type of the output as int8 to conform with the
            % input type required by zip.
            zipData = distcompByteBuffer2MxArray(data, int8([]));
        else
            zipData = int8([]);
        end
        itemList(1).delete();
    catch err
        throw(distcomp.handleJavaException(job, err));
    end
end
