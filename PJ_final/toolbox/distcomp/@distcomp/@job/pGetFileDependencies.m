function val = pGetFileDependencies(job, val)
; %#ok Undocumented
%PGETTIMEOUT A short description of the function
%
%  VAL = PGETTIMEOUT(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = cell(proxyJob.getFileDepPathList(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
