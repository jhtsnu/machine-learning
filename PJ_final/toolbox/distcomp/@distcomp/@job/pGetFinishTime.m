function val = pGetFinishTime(job, val)
; %#ok Undocumented
%PGETFINISHTIME A short description of the function
%
%  VAL = PGETFINISHTIME(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = char(proxyJob.getFinishTime(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
