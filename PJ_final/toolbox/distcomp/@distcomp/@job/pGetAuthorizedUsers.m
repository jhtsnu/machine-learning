function val = pGetAuthorizedUsers(job, ~)
; %#ok Undocumented
%pGetAuthorizedUsers Retrieve all users to perform privileged actions on
%                    this job.

%  Copyright 2009-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
try
    val = cell(proxyJob.getAuthorisedUsers(job.UUID));
catch err %#ok<NASGU>
	val = cell(1, 0);
end
