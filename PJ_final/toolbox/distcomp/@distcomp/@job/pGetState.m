function val = pGetState(job, val)
; %#ok Undocumented
%pGetState A short description of the function
%
%  VAL = pGetState(JOB, VAL)

%  Copyright 2000-2010 The MathWorks, Inc.

%  $Revision: 1.1.8.7 $    $Date: 2010/12/27 01:10:02 $ 

persistent Values Strings
if isempty(Values)
    types = findtype('distcomp.jobexecutionstate');
    Values = types.Values;
    Strings = types.Strings;
end

proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = Strings{double(proxyJob.getState(job.UUID)) == Values}; % pending, queued, running, or finished
    catch err
        [isJavaError, exceptionType, causes] = isJavaException(err);
        if isJavaError 
            % Find the original cause of the exception
            if isempty(causes)
                cause = exceptionType;
            else
                cause = causes{end};
            end
            if strcmp(cause, 'com.mathworks.toolbox.distcomp.storage.WorkUnitNotFoundException')
                val = Strings{-1 == Values}; % destroyed
            else
                val = Strings{-2 == Values}; % unavailable
            end
        end
    end
end
