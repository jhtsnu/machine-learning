function val = pGetTasks(job, val)
; %#ok Undocumented
%PGETTASKS A short description of the function
%
%  VAL = PGETTASKS(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


if ~isempty(job.ProxyObject)
    try
        % Get the java tasks from the job
        proxyTasks = job.ProxyObject.getTasks(job.UUID);
        val = distcomp.createObjectsFromProxies(proxyTasks(1), @distcomp.task, job);
    catch %#ok<CTCH> swallow errors
        % TODO - error thrown in here?        
    end
else
    % TODO - what if ProxyObject is empty?
end
