function cellargout = pGetDisplayItems(obj, inStruct)
; %#ok Undocumented
%   Gets the common display structure for a job object. outputs at least
%   ONE display structure with a header as entries in the output cell array.

% Copyright 2006-2010 The MathWorks, Inc.

% $Revision: 1.1.6.6 $    $Date: 2010/12/27 01:10:01 $

import parallel.internal.cluster.DisplayFormatter

cellargout = cell(4, 1);
% initialize number of output arguments
mainStruct = inStruct;
outputDataDependencyStruct = inStruct;
outputTasksStruct = inStruct;
specificStruct = inStruct;

mainStruct.Type = 'job';
% Get the task information in one RMI call via the WorkUnitInfoSmallItems
info = obj.pGetWorkUnitInfoSmallItems();
% Convert the object state from java to matlab enum
objectState = iConvertState(info.getState);

if strcmp(objectState, 'unavailable') || strcmp(objectState, 'destroyed')
    % If we can't get the information to display set to default values
    jID = '';
else
    jID = num2str(obj.ID);
end
mainStruct.Header = [iGetJobType(obj) 'Job ID ' jID];
% gets a generic header so that the subclasses can append their own name
% without actually overwriting pGetDisplayItems. Used for identifying Parallel Jobs.

% gets the running duration as a string
runningDuration = double(info.getRunningDuration);
formattedRunningDuration = DisplayFormatter.formatRunningDuration(runningDuration);

% There is one extra RMI call here to get the authorized users. This is
% because WorkUnitInfo does not have an authorized users field. This could
% be added in the future.
mainStruct.Names = {'UserName', 'AuthorizedUsers', 'State', 'SubmitTime', 'StartTime', 'Running Duration'};
mainStruct.Values = {char(info.getUserName), obj.AuthorizedUsers, objectState, char(info.getSubmitTime), char(info.getStartTime), formattedRunningDuration};

% pDispArgs will truncate these values if very large (>20 cell items and also per line)
outputDataDependencyStruct.Header = 'Data Dependencies';
outputDataDependencyStruct.Names = {'FileDependencies', 'PathDependencies'};
% the values here will be truncated by top level function in pDispArgs
outputDataDependencyStruct.Values = {obj.FileDependencies, obj.PathDependencies};


outputTasksStruct.Header = 'Associated Task(s)';
outputTasksStruct.Names = {'Number Pending ', ...
    'Number Running ', ...
    'Number Finished', ...
    'TaskID of errors'};

try
    [p, r, f] = obj.findTask;
    taskIDWithError = [];
    if ~isempty( f )
        % based on Narfi feedback ErrorMessage could be empty so need to check both fields
        dde = ~cellfun( @isempty, get( f, {'ErrorIdentifier','ErrorMessage'}) );
        didError = or(dde(:,1), dde(:,2));
        taskIDWithErrorCell = get( f( didError ), {'ID'} );
        taskIDWithError = [taskIDWithErrorCell{:}];
    end
    % Length of output list of parameters and values must of the same length
    outputTasksStruct.Values = {length( p ), ...
        length( r ),...
        length( f ),...
        iDisplayNothingIfEmpty(taskIDWithError)};

catch err %#ok<NASGU>
    % DONE - need to display [] here
    outputTasksStruct.Values = {[], ...
        [],...
        [],...
        ''};
end



% ***********************************************************************
% JOBMANAGER SPECIFIC PROPERTIES
specificStruct.Header = 'Jobmanager Dependent Properties';

specificStruct.Names = {'MaximumNumberOfWorkers', ...
    'MinimumNumberOfWorkers',...
    'Timeout', ...
    'RestartWorker', ...
    'QueuedFcn', ...
    'RunningFcn', ...
    'FinishedFcn'};
specificStruct.Values = {iConvertMinMaxWorkers(info.getMaxWorkers), ...
    iConvertMinMaxWorkers(info.getMinWorkers), ...
    iConvertTimeout(info.getTimeout), ...
    info.isRestartWorker, ...
    iDisplayNothingIfEmpty(obj.QueuedFcn),...
    iDisplayNothingIfEmpty(obj.RunningFcn), ...
    iDisplayNothingIfEmpty(obj.FinishedFcn)};

cellargout{1} = mainStruct;
% all four categories are sent to top level pDefaultSingleObjDisplay
cellargout{2} = outputDataDependencyStruct;
cellargout{3} = outputTasksStruct;
cellargout{4} = specificStruct;
end


%--------------------------------------------------------------------------
% internal helper function returns a empty space if item is empty
%--------------------------------------------------------------------------
function val = iDisplayNothingIfEmpty(val)
% Force the output to an empty string if it is empty
if isempty(val)
    val = '';
end
end


%--------------------------------------------------------------------------
% iGetJobType(obj) returns a string for Parallel/MatlabPool Jobs
%--------------------------------------------------------------------------
function str = iGetJobType(obj)
if isa(obj, 'distcomp.matlabpooljob')
    str = 'MatlabPool ';
elseif isa(obj, 'distcomp.paralleljob')
    str = 'Parallel ';
else
    str = '';
end

end

function minMaxWorkers = iConvertMinMaxWorkers(val)
% Convert the number of minimum or maximum workers from java to double
% or inf if the number is intmax('int32')
if isequal(val, intmax('int32'))
    minMaxWorkers = Inf;
else
    minMaxWorkers = double(val);
end
end

function timeout = iConvertTimeout(timeout)
% Convert the timeout from java to double or inf if the timeout is
% intmax('int64')

% timeout has probably already been automatically converted to double, but
% double(intmax('int64')) does NOT equal intmax('int64') as it is not
% representable accurately as a double. We must therefore test if timeout
% is equal to double(intmax('int64')) to see if the timeout has not been
% set.
timeout = double(timeout);
if isequal(timeout, double(intmax('int64')))
    timeout = Inf;
else
    timeout = timeout / 1000; % convert to seconds
end
end

function outState = iConvertState(inState)
    
persistent Values Strings
if isempty(Values)
    types = findtype('distcomp.jobexecutionstate');
    Values = types.Values;
    Strings = types.Strings;
end 

outState = Strings{inState == Values};
end
