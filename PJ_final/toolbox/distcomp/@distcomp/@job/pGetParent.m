function val = pGetParent(task, ~)
; %#ok Undocumented
%pGetParent A short description of the function
%
%  VAL = pGetParent(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.

val = task.up;
