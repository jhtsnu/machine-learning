function pUpdateProxyObject(~, ~)
; %#ok Undocumented
%pUpdateProxyObject insert a new java proxy object into this UDD wrapper
%
%  pUpdateProxyObject(OBJS, PROXIES)

%  Copyright 2004-2012 The MathWorks, Inc.


% Jobs and Tasks will have their proxies updated if the jobmanager gets
% updated

