function val = pGetID(job, val)
; %#ok Undocumented
%PGETID A short description of the function
%
%  VAL = PGETID(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = proxyJob.getNum(job.UUID);
    catch %#ok<CTCH> swallow errors
    end
end
