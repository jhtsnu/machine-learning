function val = pGetName(job, val)
; %#ok Undocumented
%PGETNAME A short description of the function
%
%  VAL = PGETNAME(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = char(proxyJob.getName(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
