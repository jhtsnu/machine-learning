function val = pGetMaximumNumberOfWorkers(job, val)
; %#ok Undocumented
%PGETMAXIMUMNUMBEROFWORKERS A short description of the function
%
%  VAL = PGETMAXIMUMNUMBEROFWORKERS(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        iVal = proxyJob.getMaxWorkers(job.UUID);
        % Check if the number is INTMAX for int32
        if isequal(iVal, intmax('int32'))
            val = Inf;
        else
            val = double(iVal);
        end
    catch %#ok<CTCH> swallow errors
    end
end
