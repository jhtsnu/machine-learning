function val = pGetTag(job, val)
; %#ok Undocumented
%PGETTAG A short description of the function
%
%  VAL = PGETTAG(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = char(proxyJob.getTag(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
