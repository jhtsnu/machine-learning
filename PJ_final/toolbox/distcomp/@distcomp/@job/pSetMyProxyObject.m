function pSetMyProxyObject(job, event)
; %#ok Undocumented
%pSetMyProxyObject 
%
%  pSetMyProxyObject(src, event)

%  Copyright 2005-2010 The MathWorks, Inc.

%  $Revision: 1.1.10.4 $    $Date: 2010/12/27 01:10:05 $ 

parent = event.NewParent;
proxy = parent.pGetJobAccessProxy;
job.ProxyObject = proxy;
job.HasProxyObject = true;

% If there's a new job manager proxy, there will be a new remote event
% listener, and we will need to re-register for events with it.
if job.ProxyToUddAdaptorRegistrationCount > 0
    parent.pRegisterForEvents(job.ProxyObject, job.UUID);
end
