function val = pGetRestartWorker(job, val)
; %#ok Undocumented
%PGETRESTARTWORKER A short description of the function
%
%  VAL = PGETRESTARTWORKER(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = proxyJob.isRestartWorker(job.UUID);
    catch %#ok<CTCH> swallow errors
    end
end
