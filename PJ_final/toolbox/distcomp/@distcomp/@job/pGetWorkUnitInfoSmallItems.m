function val = pGetWorkUnitInfoSmallItems(job)
; %#ok Undocumented
% Returns a WorkUnitInfoSmallItems object.

%   Copyright 2010-2011 The MathWorks, Inc.

try
    info = job.ProxyObject.getWorkUnitInfoSmallItems(job.UUID);
    val = info(1);
catch err
    [isJavaError, exceptionType, causes] = isJavaException(err);
    if isJavaError
        if isempty(causes)
            cause = exceptionType;
        else
            cause = causes{end};
        end
        if strcmp(cause, 'com.mathworks.toolbox.distcomp.storage.WorkUnitNotFoundException')
            objectState = -1; % destroyed
        else
            objectState = -2; % unavailable
        end
        % If we can't get the task information, set the info to default
        % values
        val = iCreateDefaultJobInfo(objectState);
    else
        rethrow(err);
    end
end

function info = iCreateDefaultJobInfo(objectState)
    info = com.mathworks.toolbox.distcomp.workunit.JobInfo();
    info.setState(objectState);
