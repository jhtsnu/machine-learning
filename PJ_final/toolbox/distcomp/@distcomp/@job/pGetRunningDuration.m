function val = pGetRunningDuration(job)

%   Copyright 2010 The MathWorks, Inc.

; %#ok Undocumented
%PGETRUNNINGDURATION
%
%  VAL = PGETRUNNINGDURATION(JOB)

% This is not a true get function as running duration is not yet a property
% of jobs and tasks, so val is not passed in.
proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = double(proxyJob.getRunningDuration(job.UUID));
    catch err %#ok<NASGU>
        % If there was a problem on the remote side we have no information
        % about the running duration and so we return empty
        val = [];
    end
end

