function val = pGetStartTime(job, val)
; %#ok Undocumented
%PGETSTARTTIME A short description of the function
%
%  VAL = PGETSTARTTIME(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = char(proxyJob.getStartTime(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
