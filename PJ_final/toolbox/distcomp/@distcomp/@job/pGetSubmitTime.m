function val = pGetSubmitTime(job, val)
; %#ok Undocumented
%pGetSubmitTime A short description of the function
%
%  VAL = pGetSubmitTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = char(proxyJob.getSubmitTime(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
