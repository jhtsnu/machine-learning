function val = pGetUserName(job, val)
; %#ok Undocumented
%PGETUSERNAME A short description of the function
%
%  VAL = PGETUSERNAME(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
try
    val = char(proxyJob.getUserName(job.UUID));
catch %#ok<CTCH> swallow errors
	% TODO
end
