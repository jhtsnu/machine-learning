function diary(job, varargin)
%DIARY Display or save text of batch job.
%   DIARY(JOB) displays the command window output from the batch job 
%   in the MATLAB command window. The command window output will only be
%   captured if the batch job had the CaptureDiary property set to true.
%
%   DIARY(JOB, FILENAME) causes a copy of the command window output from
%   the batch job to be appended to the named file.
%
%   See also: BATCH

%  Copyright 2007 The MathWorks, Inc.

% $Revision: 1.1.6.2 $  $Date: 2011/09/19 17:39:45 $

% Bail from here if the job is not a batch job
try 
    distcomp.errorIfNotBatchJob(job);
catch exception
    throw(exception)
end

diaryOut = job.Tasks(1).CommandWindowOutput;
try
    parallel.internal.apishared.BatchJobMethods.diary( ...
        diaryOut, varargin{:} );
catch E
    throw( E );
end
