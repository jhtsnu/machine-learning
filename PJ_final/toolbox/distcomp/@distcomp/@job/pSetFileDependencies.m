function val = pSetFileDependencies(job, val)
; %#ok Undocumented
%PSETTIMEOUT A short description of the function
%
%  VAL = PSETFILEDEPENDENCIES(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


if job.IsBeingConstructed
    % Whilst the job is being constructed we need to ensure that we concatenate
    % the FileDependencies rather than overwrite the existing ones. We can safely
    % store the values in the actual field until after construction.
    
    % The post function is stored with the value to set 
    postFcn = @iPostConstructionSetFileDependencies;
    % Have we already set this post construction function once?
    [index, oldVal, oldConfig] = job.pFindPostConstructionFcn(postFcn);
    % Which configuration is being used to set us
    newConfig = job.ConfigurationCurrentlyBeingSet;
    if isempty(index)
        job.pAddPostConstructionFcn(postFcn, val, newConfig);
    else
        newConfig = distcomp.configurableobject.pGetConfigNameFromConfigPair(oldConfig, newConfig);
        job.pModifyPostConstructionFcn(index, postFcn, [oldVal ; val], newConfig);
    end
    val = {};
    return
end

import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem
import parallel.internal.apishared.AttachedFiles

proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    % We need to resolve the file paths first as zipFiles() no longer has
    % this side effect after the implementation of auto-attached files.
    val = cellfun( @parallel.internal.apishared.FilenameUtils.safeWhich, val, ...
            'UniformOutput', false );
    [filedata, val] = AttachedFiles.zipFiles( val );
    if ~isempty(val)
        val = reshape(val, 1, numel(val));
    else
        filedata = int8([]);
        % Need to make a 1 x 0 array of java.lang.String[][]
        val = javaArray('java.lang.String', 1, 1);
        val(1) = '';
    end
    try
        itemArray = dctJavaArray(ByteBufferItem(distcompMxArray2ByteBuffer(filedata)), ...
            'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData');
        proxyJob.setFileDepData(job.UUID, itemArray);
        proxyJob.setFileDepPathList(job.UUID, val);
    catch err
    	throw(distcomp.handleJavaException(job, err));
    end
end
% Do not hold anything locally
val = {};

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iPostConstructionSetFileDependencies(job, val, config)
% Actually set the file dependencies
pSetPropertyAndConfiguration(job, 'FileDependencies', val, config);
