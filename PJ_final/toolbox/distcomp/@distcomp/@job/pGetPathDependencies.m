function val = pGetPathDependencies(job, val)
; %#ok Undocumented
%pGetPathDependencies A short description of the function
%
%  VAL = pGetPathDependencies(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = cell(proxyJob.getPathList(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
