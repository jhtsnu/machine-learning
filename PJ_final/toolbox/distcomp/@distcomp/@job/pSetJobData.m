function val = pSetJobData(job, val)
; %#ok Undocumented
%PSETJOBDATA A short description of the function
%
%  VAL = PSETJOBDATA(JOB, VAL)

%  Copyright 2000-2011 The MathWorks, Inc.


import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem

proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        s = distcompserialize(val);
        % Put the data into a ByteBufferItem[] to pass to the proxy
        itemArray = dctJavaArray(ByteBufferItem(distcompMxArray2ByteBuffer(s)),...
            'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData');
        proxyJob.setJobScopeData(job.UUID, itemArray);
    catch err
        throw(distcomp.handleJavaException(job, err));
    end
end
% Do not hold anything locally
val = [];
