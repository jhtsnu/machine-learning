function val = pGetCreateTime(job, val)
; %#ok Undocumented
%PGETCREATETIME A short description of the function
%
%  VAL = PGETCREATETIME(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        val = char(proxyJob.getCreateTime(job.UUID));
    catch %#ok<CTCH> swallow errors
    end
end
