function pRegisterForEvents(obj)
; %#ok Undocumented
%pRegisterForEvents register for events with the remote event listener
%
%  pRegisterForEvents(OBJ)

%  Copyright 2000-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.1 $    $Date: 2010/12/27 01:10:04 $ 

registrationCount = obj.ProxyToUddAdaptorRegistrationCount;
% Check to see if we think we have already registered for events - if we
% haven't then really register
if registrationCount == 0
    jobmanager = obj.Parent;
    jobmanager.pRegisterForEvents(obj.ProxyObject, obj.UUID);
end
% Increment the number of times we have registered
obj.ProxyToUddAdaptorRegistrationCount = registrationCount + 1;

