function pUnregisterForEvents(obj)
; %#ok Undocumented
%pUnregisterForEvents unregister events with the root object
%
%  pUnregisterForEvents(OBJ)

%  Copyright 2000-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.1 $    $Date: 2010/12/27 01:10:06 $ 

registrationCount = obj.ProxyToUddAdaptorRegistrationCount;
% First check we aren't decrementing a zero event count
if registrationCount < 1
    warning('distcomp:proxyobject:InvalidState', 'Attempting to unregister for events before registering'); 
    return
end

registrationCount = registrationCount - 1;
% If the registration count has reached zero then unregister with the root
if registrationCount == 0
    jobmanager = obj.Parent;
    jobmanager.pUnregisterForEvents(obj.ProxyObject, obj.UUID);
end

obj.ProxyToUddAdaptorRegistrationCount = registrationCount;
