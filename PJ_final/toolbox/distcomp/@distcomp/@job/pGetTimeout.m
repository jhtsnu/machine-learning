function val = pGetTimeout(job, val)
; %#ok Undocumented
%PGETTIMEOUT A short description of the function
%
%  VAL = PGETTIMEOUT(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        lVal = proxyJob.getTimeout(job.UUID);
        % Check if the number is INTMAX for int64
        if isequal(lVal, intmax('int64'))
            val = Inf;
        else
            val = double(lVal) / 1000; % convert to seconds
        end
    catch %#ok<CTCH> swallow errors
    end
end
