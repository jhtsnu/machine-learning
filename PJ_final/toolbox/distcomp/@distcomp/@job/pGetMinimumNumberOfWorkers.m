function val = pGetMinimumNumberOfWorkers(job, val)
; %#ok Undocumented
%PGETMINIMUMNUMBEROFWORKERS A short description of the function
%
%  VAL = PGETMINIMUMNUMBEROFWORKERS(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyJob = job.ProxyObject;
if ~isempty(proxyJob)
    try
        iVal = proxyJob.getMinWorkers(job.UUID);
        % Check if the number is INTMAX for int32
        if isequal(iVal, intmax('int32'))
            val = Inf;
        else
            val = double(iVal);
        end
    catch %#ok<CTCH> swallow errors
    end
end
