function isPoolJob = pCurrentTaskIsPartOfAPool(~)
; %#ok Undocumented
%pCurrentTaskIsPartOfAPool 
%
%  isPoolJob = pCurrentTaskIsPartOfAPool(JOB)

%  Copyright 2008-2012 The MathWorks, Inc.

isPoolJob = false;
