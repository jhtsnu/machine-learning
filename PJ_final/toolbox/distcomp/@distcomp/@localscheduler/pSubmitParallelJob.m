function pSubmitParallelJob(local, job)
; %#ok Undocumented
%pSubmitParallelJob - submit a job
%
%  pSubmitParallelJob(SCHEDULER, JOB)

%  Copyright 2006-2012 The MathWorks, Inc.

import parallel.internal.apishared.FilenameUtils;
import parallel.internal.apishared.SmpdGateway;
import parallel.internal.apishared.EnvVars;

USE_MPIEXEC = SmpdGateway.canUseSmpd();

numTasks = numel(job.Tasks);
if numTasks < 1
    error('distcomp:localscheduler:InvalidState', 'A job must have at least one task to submit to a local scheduler');
end
% Get the maximum and minimum number of workers
minW = job.MinimumNumberOfWorkers;
maxW = min(job.MaximumNumberOfWorkers, local.MaximumNumberOfWorkers);

if minW > local.MaximumNumberOfWorkers
    error('distcomp:localscheduler:InvalidArgument',...
        ['You requested a minimum of %d workers but only %d workers '...
         'are allowed with the local scheduler.'], ...
          minW, local.MaximumNumberOfWorkers);

end
% Check the consistency of min workers and cluster size
try
    local.pMinWorkersClusterSizeConsistentCheck(job);
catch err
    % Tell the user that the default value is to use the number of cores.
    error(err.identifier, ...
        ['%s  The default value for ClusterSize with a local scheduler is the ' ...
         'number of cores on the local machine.'], err.message);
end
if maxW < 1
    error('distcomp:localscheduler:InvalidArgument',...
        ['The job was not submitted to the local scheduler because the MaximumNumberOfWorkers\n' ...
         'property was set to 0. The job would never start running with this value.'], '');
end
% Duplicate the tasks for parallel execution
job.pDuplicateTasks;
% Ensure that the job has been prepared
job.pPrepareJobForSubmission;
% Get all the duplicated tasks
tasks = job.Tasks;
numTasks = numel(tasks);

storage = job.pReturnStorage;
logRoot = storage.StorageLocation;
% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end
[stringLocation, stringConstructor] = storage.getSubmissionStrings;

% URLEncode the stringLocation. Ultimately, we'd like to do this everywhere.
stringLocation = parallel.internal.urlencode( stringLocation );

% Get the location of the storage
jobLocation = FilenameUtils.fixSlashes(job.pGetEntityLocation);

% Store the root directory of the job
jobRoot = fullfile( logRoot, jobLocation );

% Communicating jobs need the license information in the command if they're
% being launched directly. The SmpdGateway handles licenses for mpiexec jobs.
needLicenseArgInCommand = ~USE_MPIEXEC;

% Get the full matlab command to run
[~, mlcommand, mlargsCell] = local.pCalculateMatlabCommandForJob(job, needLicenseArgInCommand);
% Prepend the matlab command to create the array of strings for java to execute
commandArray = [{mlcommand}, mlargsCell];
% Define where to write the logs to
logRelativeToRoot = fullfile(jobLocation, [jobLocation '.log']);
logLocation = fullfile(logRoot, logRelativeToRoot);

% envNames and values are all shared names AND values
envNames = {...
    'MDCE_STORAGE_LOCATION' ...
    'MDCE_STORAGE_CONSTRUCTOR' ...
    'MDCE_JOB_LOCATION' ...
    'MDCE_PID_TO_WATCH' ...
    'MDCE_PARALLEL' ...
    'MDCE_USE_ML_LICENSING', ....
    };

envValues = {...
    stringLocation ...
    stringConstructor ...
    jobLocation ...
    sprintf('%d',feature('getpid')) ...
    '1' ...
    'true' ...
    };

% Forward MDCE_DEBUG if necessary
mdceDebugName = 'MDCE_DEBUG';
mdceDebug = getenv(mdceDebugName);
if ~isempty(mdceDebug)
    envNames{end+1}  = mdceDebugName;
    envValues{end+1} = mdceDebug;
end

% Tell the ConnectionManager for pmode/matlabpool to connect us directly on
% the numeric localhost address
if isempty( getenv( 'MDCE_OVERRIDE_CLIENT_HOST' ) )
    envNames{end+1}  = 'MDCE_OVERRIDE_CLIENT_HOST'; % append these to envNames
    envValues{end+1} = '127.0.0.1';
end

% Tell MPICH2 to use 127.0.0.1 directly - thereby avoiding any problems that
% may occur on a machine that cannot resolve its own host name. We even do
% this for mpiexec, even though I'm not 100% sure if it's used.
if isempty( getenv( 'MPICH_INTERFACE_HOSTNAME' ) )
    envNames{end+1} = 'MPICH_INTERFACE_HOSTNAME';
    envValues{end+1} = '127.0.0.1';
end

% MPIEXEC or not variance here
if USE_MPIEXEC
    % Just define extraNames /values in one go here
    extraNames  = { 'MDCE_DECODE_FUNCTION' };
    extraValues = { 'decodeLocalMpiexecParallelTask' };
else
    % Use the connect/accept decode function, and force the "sock" MPI build.
    extraNames  = { 'MDCE_DECODE_FUNCTION', ...
                    'MDCE_SENTINAL_LOCATION', ...
                    'MDCE_FORCE_MPI_OPTION' };

    extraValues = { 'decodeLocalParallelTask', ...
                    fullfile(logRoot, jobLocation, 'mpi'), ...
                    'sock' };
end

% The full environment to inject
envNames  = [ envNames, extraNames ];   envNames  = envNames(:);
envValues = [ envValues, extraValues ]; envValues = envValues(:);

% Fill out the environment with other stuff that has changed.
[clientEnvNames, clientEnvValues] = EnvVars.createLocalClusterEnvironment( envNames );

if USE_MPIEXEC
    import com.mathworks.toolbox.distcomp.local.MpiexecJobCommand;
    mpiexecCmdArray = SmpdGateway.getMpiexecArgs( envNames );
    javaTask = MpiexecJobCommand.getNewInstance( mpiexecCmdArray, commandArray, envNames, envValues, ...
                                                              jobRoot, logLocation, [minW maxW] );
else
    import com.mathworks.toolbox.distcomp.local.ParallelJobCommand;
    javaTask = ParallelJobCommand.getNewInstance(commandArray, envNames, envValues, logLocation, [minW maxW]);
end

% Data returned from local -
% localTaskUUIDs : the local task ID for each task - NOTE for parallel jobs where
%                  there is only one actual task every real task will map to
%                  the same local task
% taskIDs        : the ID of the distcomp.abstracttask associated with a local
%                  task in the field above. It is an error for taskIDs and
%                  localTaskUUIDs to be of different lengths.
schedulerData = struct('type', 'local', ...
                       'taskUUIDs', {repmat({javaTask.getUUID}, numTasks, 1)} , ...
                       'taskIDs', {1:numTasks}, ...
                       'submitProcInfo', local.ProcessInformation, ...
                       'logRelToStorage', {logRelativeToRoot}, ...
                       'clientEnvNames', {clientEnvNames}, ...
                       'clientEnvValues', {clientEnvValues});
job.pSetJobSchedulerData(schedulerData);

javaTask.submit;

end
