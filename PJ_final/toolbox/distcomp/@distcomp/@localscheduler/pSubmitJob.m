function pSubmitJob(local, job)
; %#ok Undocumented
%pSubmitJob - submit a job for local scheduler
%
%  pSubmitJob(SCHEDULER, JOB)

%  Copyright 2006-2012 The MathWorks, Inc.

import parallel.internal.apishared.FilenameUtils;
import parallel.internal.apishared.EnvVars;

tasks = job.Tasks;
numTasks = numel(tasks);
if numTasks < 1
    error('distcomp:localscheduler:InvalidState', 'A job must have at least one task to submit to a local scheduler');
end
% Ensure that the job has been prepared
job.pPrepareJobForSubmission;
storage = job.pReturnStorage;

logRoot = storage.StorageLocation;
% Define the logLocationTemplate to use - remove the ID from the end of
% the first tasks name
logRelativeToRootTemplate = FilenameUtils.fixSlashes(regexprep(tasks(1).pGetEntityLocation, '[0-9]*$', ''));

% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end
[stringLocation, stringConstructor] = storage.getSubmissionStrings;
% Get the location of the storage
jobLocation = FilenameUtils.fixSlashes(job.pGetEntityLocation);

% Independent jobs always need the license information in the command
needLicenseArgInCommand = true;

[~, mlcommand, mlargsCell] = local.pCalculateMatlabCommandForJob(job, needLicenseArgInCommand);
% Prepend the matlab command to create the array of strings for java to execute
commandArray = [{mlcommand}, mlargsCell];

% Non-overridable variables
envNames = {...
    'MDCE_DECODE_FUNCTION'; ...
    'MDCE_STORAGE_LOCATION'; ...
    'MDCE_STORAGE_CONSTRUCTOR'; ...
    'MDCE_JOB_LOCATION'; ...
    'MDCE_TASK_LOCATION'; ...
    'MDCE_PID_TO_WATCH'; ...
    'MDCE_USE_ML_LICENSING'
    };

envValues = {...
    'decodeLocalSingleTask'; ...
    stringLocation; ...
    stringConstructor; ...
    jobLocation; ...
    ''; ...
    sprintf('%d',feature('getpid')); ...
    'true' ...
    };

% Forward MDCE_DEBUG if necessary
mdceDebugName = 'MDCE_DEBUG';
mdceDebug = getenv(mdceDebugName);
if ~isempty(mdceDebug)
    envNames{end+1}  = mdceDebugName;
    envValues{end+1} = mdceDebug;
end

taskLocationIndex = strcmp(envNames, 'MDCE_TASK_LOCATION');

[clientEnvNames, clientEnvValues] = EnvVars.createLocalClusterEnvironment( envNames );

taskUUIDs = cell(numTasks, 1);
javaTasks = cell(numTasks, 1);
taskIDs = zeros(numTasks, 1);

for i = 1:numTasks
    logRelativeToRoot = sprintf('%s%d.log', logRelativeToRootTemplate, tasks(i).ID);
    logLocation = fullfile(logRoot, logRelativeToRoot);
    % Set the task location environment variable correctly
    taskLocation = FilenameUtils.fixSlashes(tasks(i).pGetEntityLocation);
    envValues{taskLocationIndex} = taskLocation;

    javaTask = com.mathworks.toolbox.distcomp.local.TaskCommand.getNewInstance(commandArray, envNames, envValues, logLocation);
    javaTasks{i} = javaTask;
    % Mechanism to track the UUID against the actual task
    taskUUIDs{i} = javaTask.getUUID;
    taskIDs(i) = tasks(i).ID;    
end

% Data returned from local -
% localTaskUUIDs : the local task ID for each task - NOTE for parallel jobs where
%                  there is only one actual task every real task will map to
%                  the same local task
% taskIDs        : the ID of the distcomp.abstracttask associated with a local
%                  task in the field above. It is an error for taskIDs and
%                  localTaskUUIDs to be of different lengths.
schedulerData = struct('type', 'local', ...
                       'taskUUIDs', {taskUUIDs} , ...
                       'taskIDs', {taskIDs}, ...
                       'submitProcInfo', local.ProcessInformation, ...
                       'logRelToStorage', {logRelativeToRootTemplate}, ...
                       'clientEnvNames', {clientEnvNames}, ...
                       'clientEnvValues', {clientEnvValues});
job.pSetJobSchedulerData(schedulerData);

% Now submit the actual java tasks
for i = 1:numTasks
    javaTasks{i}.submit;
end
