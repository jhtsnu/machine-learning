function obj = localscheduler( proxyScheduler ) %#ok<INUSD>
; %#ok Undocumented
%LOCALSCHEDULER - local scheduler constructor

%  Copyright 2006-2012 The MathWorks, Inc.


obj = distcomp.localscheduler;
% Subdirectory to put data in - specific to a release of the tools
subDir = fullfile('local_cluster_jobs', ['R' version('-release')]);
try
    % Get the directory one up from the prefdir
    baseDir = fileparts(prefdir);
    % Try making a new directory for the data location
    dirName = fullfile(baseDir, subDir);
catch exception %#ok<NASGU> This is a conceivable failure that we deal with
    dirName = fullfile(tempdir, subDir);
end
% Should we remove the dirName afterwards - it is unlikely
% that this will get called because JVM shutdown hooks and
% UDD destructors don't get called when matlab exits
obj.RemoveDataLocation = ~exist(dirName, 'dir');
% If the dirName doesn't exist we need to create it here
if obj.RemoveDataLocation
    OK = mkdir(dirName);
    if ~OK
        % Fallback to different directory name
        dirName = fullfile(tempdir, subDir);
        if ~exist(dirName, 'dir')
            [OK, msg, id] = mkdir(dirName);
            if ~OK
                % Failed to make the DataLocation is a bad sign - rethrow
                % to the user
                error(id, '%s', msg);
            end
        end
    end
end
% Make a new storage object here that uses the specified dirName
% ClusterSize must be equal to 4 for the local scheduler.
storage = distcomp.filestorage(dirName);

import com.mathworks.toolbox.distcomp.local.LocalConstants
import parallel.internal.apishared.LocalUtils

localScheduler = LocalUtils.getJavaScheduler();
pid = feature('getpid');
procInfo = struct('pid', pid, 'pidname', dct_psname(pid), ...
                  'hostname', char( java.net.InetAddress.getLocalHost.getCanonicalHostName ) );
set( obj, ...
     'Type', 'local', ...
     'Storage', storage, ...
     'ClusterMatlabRoot', matlabroot, ...
     'HasSharedFilesystem', true, ...
     'LocalScheduler', localScheduler,...
     'MaximumNumberOfWorkers', LocalConstants.sMAX_NUMBER_OF_WORKERS, ...
     'ProcessInformation', procInfo);

% This class accepts configurations and uses the scheduler section.
sectionName = 'scheduler';
obj.pInitializeForConfigurations(sectionName);

% Indicate that we have finished initializing the object
obj.Initialized = true;

