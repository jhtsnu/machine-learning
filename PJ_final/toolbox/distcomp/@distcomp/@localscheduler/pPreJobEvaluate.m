function pPreJobEvaluate( ~, job, ~ ) % obj, job, task
%pPreJobEvaluate - perform scheduler-specific pre-evaluation

% Copyright 2012 The MathWorks, Inc.

jobSchedulerData = job.pGetJobSchedulerData();

% Make our environment match the client
cellfun( @(n,v) setenv( n, v ), ...
         jobSchedulerData.clientEnvNames, ...
         jobSchedulerData.clientEnvValues );
