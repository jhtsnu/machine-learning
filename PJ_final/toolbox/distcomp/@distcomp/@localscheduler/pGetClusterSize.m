function val = pGetClusterSize( obj, ~ )
; %#ok Undocumented

% Check with the java LocalScheduler

% Copyright 2011 The MathWorks, Inc.

try
    val = double( obj.LocalScheduler.getMaximumNumberOfWorkers() );
catch err
    throw( err );
end
