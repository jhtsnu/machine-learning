function [worker, mlcmd, argsCell] = pCalculateMatlabCommandForJob( obj, ~, needLicenseArg )
; %#ok Undocumented
%pCalculateMatlabCommandForJob - calculate MatlabCommandToRun
%
% [worker, mlcmd, argsCell] = sched.pCalculateMatlabCommandForJob( job );
%
% This uses knowledge about the ClusterOsType and the job type.

% Copyright 2006-2012 The MathWorks, Inc.

[mlcmd, argsCell] = parallel.internal.apishared.WorkerCommand.localCommand( ...
    obj.ClusterMatlabRoot, needLicenseArg );

% We can now create the old-style MatlabCommandToRun - but lets not since
% this scheduler shouldn't use this form
worker = '';
