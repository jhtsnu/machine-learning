function name = createNew(type)
; %#ok Undocumented
%createNew Static method to create a new configuration for a specific
%scheduler type.
%

%  Copyright 2007-2011 The MathWorks, Inc.

import parallel.internal.settings.ConfigurationsHelper;

obj = distcomp.configuration;

% Return the return value and just use the error checking.  This will throw an
% error if the type is invalid.
obj.pMapFRTypeToSchedClassType(type);

% Let the names be jobmanagerconfig1, localconfig1, etc.
% The serializer will add the numeric suffix.
proposedName = sprintf('%sconfig', type);
ser = ConfigurationsHelper.getCurrentSerializer();
try
    obj.ActualName = ser.createNew(proposedName, type);
    obj.pConstructFromClassTypes(type);
    obj.save();
catch err
    try
        ser.deleteConfig(obj.ActualName);
    catch %#ok<CTCH>
    end
    rethrow(err);
end
name = obj.ActualName;
