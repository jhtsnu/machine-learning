function name = clone(obj)
; %#ok Undocumented
%clones this object using an available name for the new configuration.

%   Copyright 2007-2012 The MathWorks, Inc.

import parallel.internal.settings.ConfigurationsHelper;
% Let the names be name.copy1, name.copy2, etc.
% The serializer will add the numeric suffix.
% Profiles Support: ".copy" is not a valid name 
% for a Settings node. Use "_" instead.
proposedName = sprintf('%s_copy', obj.Name);
% Copy all the data of this object.
ser = ConfigurationsHelper.getCurrentSerializer();
newName = ser.clone(obj.Name, proposedName);

% Create a new object based on the copy.
conf = distcomp.configuration;
conf.pInitializeFromName(newName);

name = conf.ActualName;
