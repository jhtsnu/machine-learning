function save(obj)
; %#ok Undocumented
%save Saves all the properties in this configuration.
%

%  Copyright 2007-2012 The MathWorks, Inc.

import parallel.internal.settings.ConfigurationsHelper;
allValues = obj.pGetStruct();
ser = ConfigurationsHelper.getCurrentSerializer();
ser.saveConfigurationStruct(obj.ActualName, allValues);
