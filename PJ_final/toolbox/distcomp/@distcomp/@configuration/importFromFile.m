function name = importFromFile(filename)
; %#ok Undocumented
%Constructs a new configuration using the information in the specified file.
%

%  Copyright 2007-2011 The MathWorks, Inc.

import parallel.internal.settings.ConfigurationsHelper;
ser = ConfigurationsHelper.getCurrentSerializer();

[name, nameInFile, values] = ser.createNewFromFile( filename );

obj = distcomp.configuration;

try
    obj.ActualName = name;
    findResourceType = obj.pGetTypeFromStruct(values, nameInFile);
    obj.pConstructFromClassTypes(findResourceType);
    obj.pSetFromStruct(values);
catch err
    try
        ser.deleteConfig(obj.ActualName);
    catch err2 %#ok<NASGU>
        % Failed to delete.
    end
    error('distcomp:configuration:importFailed', ...
          ['Could not import configuration from the file %s\n', ...
           'due to the following error:\n%s'], filename, err.message);
end

obj.save();
name = obj.Name;
