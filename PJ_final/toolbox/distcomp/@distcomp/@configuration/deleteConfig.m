function deleteConfig(name)
; %#ok Undocumented
%Deletes the configuration with the specified name.

%   Copyright 2007-2012 The MathWorks, Inc.

import parallel.internal.settings.ConfigurationsHelper;
% This will throw an error if the name is invalid, or if no such configuration
% exists.
ser = ConfigurationsHelper.getCurrentSerializer();
ser.deleteConfig(name);

