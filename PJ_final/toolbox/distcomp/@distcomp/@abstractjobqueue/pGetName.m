function val = pGetName(~, ~) %#ok<STOUT> method errors unconditionally
; %#ok Undocumented
%PGETNAME private function to get jobmanager name from java object
%
%  VAL = PGETNAME(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


error('distcomp:abstractjobqueue:AbstractClassError', 'This method should be overloaded by subclasses');
