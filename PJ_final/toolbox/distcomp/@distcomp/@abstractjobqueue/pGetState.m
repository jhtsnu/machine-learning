function val = pGetState(~, ~) %#ok<STOUT> method errors unconditionally
; %#ok Undocumented
%PGETState private function to get running state from java object
%
%  VAL = PGETSTATE(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


error('distcompy:abstractjobqueue:AbstractClassError', 'This method should be overloaded by subclasses');
