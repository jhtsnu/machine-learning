function val = pGetIsRunning(~, ~) %#ok<STOUT> method errors unconditionally
; %#ok Undocumented
%PGETISRUNNING private function to get running state from java object
%
%  VAL = PGETISRUNNING(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


error('distcomp:abstractjobqueue:AbstractClassError', 'This method should be overloaded by subclasses');
