function val = pGetHostname(~, ~) %#ok<STOUT> method errors unconditionally
; %#ok Undocumented
%PGETHOSTNAME private function to get hostname from java object
%
%  VAL = PGETHOSTNAME(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


error('distcomp:abstractjobqueue:AbstractClassError', 'This method should be overloaded by subclasses');
