function val = pGetHostAddress(~, ~) %#ok<STOUT> method errors unconditionally
; %#ok Undocumented
%PGETHOSTADDRESS private function to get host IP address from java object
%
%  VAL = PGETHOSTADDRESS(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


error('distcomp:abstractjobqueue:AbstractClassError', 'This method should be overloaded by subclasses');
