function obj = objectroot
; %#ok Undocumented
%OBJECTROOT A short description of the function
%
%  OBJ = OBJECTROOT

%  Copyright 2000-2010 The MathWorks, Inc.

%  $Revision: 1.1.8.10 $    $Date: 2010/12/27 01:10:15 $

obj = distcomp.objectroot;

% Create the properties for this object
obj.ProxyHashtable = java.util.Hashtable;
