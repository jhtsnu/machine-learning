function putObjectInHashtable(root, proxies, objects)
; %#ok Undocumented
%putObjectInHashtable attempt to put the proxies in the hash table
%
%  putObjectInHashtable(ROOT, PROXIES, OBJECTS)
% 

%  Copyright 2000-2012 The MathWorks, Inc.

% How many are there
numObjects = numel(proxies);

if ~isequal(numObjects, numel(objects))
    error(message('parallel:cluster:ObjectRootNumProxiesAndObjectsNotEqual'));
end

% Get the hash table from the root
proxyHashtable = root.ProxyHashtable;
% Convert the proxies to keys for use in the hash table
keys = root.pConvertProxiesToKeys(proxies);
% Iterate over each Proxy in turn
for i = 1:numObjects
    % Put this one in
    proxyHashtable.put( keys(i), java(objects(i)) );
end