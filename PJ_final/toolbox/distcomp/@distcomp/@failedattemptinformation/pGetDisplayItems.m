function cellargout = pGetDisplayItems(obj, inStruct)
; %#ok Undocumented
% gets the common display structure. Outputs a cell arrays of inStructs.

% Copyright 2008-2010 The MathWorks, Inc.

% $Revision: 1.1.6.2 $  $Date: 2010/12/09 21:06:41 $
import parallel.internal.cluster.DisplayFormatter

cellargout = cell(1, 1); % initialize number of output arguments
mainStruct = inStruct;

workername = '';
if ~isempty(obj.Worker) && ~isempty(obj.WorkerHostname)
    workername = DisplayFormatter.formatWorkerName(...
        obj.Worker, obj.WorkerHostname);
end

mainStruct.Type = 'failedattemptinformation';
mainStruct.Header = 'Failed Attempt';
mainStruct.Names = {'StartTime', 'ErrorIdentifier', 'ErrorMessage', ...
                    'CommandWindowOutput', 'Worker'};
mainStruct.Values = {obj.StartTime, obj.ErrorIdentifier, obj.ErrorMessage, ...
                     obj.CommandWindowOutput, workername};

cellargout{1} = mainStruct;
