function fai = failedattemptinformation(taskInfo)
; %#ok Undocumented
% Constructor for failedattemptinformation

% Copyright 2008-2012 The MathWorks, Inc.


fai = distcomp.failedattemptinformation;

fai.StartTime = char(taskInfo.getStartTime());
fai.ErrorIdentifier = char(taskInfo.getErrorIdentifier());
fai.ErrorMessage = char(taskInfo.getErrorMessage());

% getData returns a ByteBuffer as in
% @task.pGetCommandWindowOutput.
data = taskInfo.getCommandWindowOutput().getData();
if ~isempty(data) && data.limit > 0
    % We're now storing the bytes using UTF-16
    % for compatibility with API-2 and streaming command
    % window output. Java's bytes are signed, and
    % native2unicode expects uint8, so convert.
    fai.CommandWindowOutput = native2unicode(typecast(data, 'uint8'), 'UTF-16LE');
else
    fai.CommandWindowOutput = '';
end

worker = taskInfo.getWorker();
if ~isempty(worker)
    fai.Worker = char(worker.getName());
    fai.WorkerHostname = char(worker.getHostName());
else
    fai.Worker = '';
    fai.WorkerHostname = '';
end
end
