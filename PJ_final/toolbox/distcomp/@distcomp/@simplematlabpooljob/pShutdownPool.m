function OK = pShutdownPool(job) 
; %#ok Undocumented
%pShutdownPool 
%
%  pShutdownPool(JOB)

% Copyright 2009-2011 The MathWorks, Inc.


import parallel.internal.apishared.Pool

job.PoolShutdownSuccessful = Pool.shutdown( job.IsPoolTask );
OK = job.PoolShutdownSuccessful;
