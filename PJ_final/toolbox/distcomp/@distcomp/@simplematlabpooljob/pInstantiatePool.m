function pInstantiatePool(job, ~, ~, ~, args) 
; %#ok Undocumented
%pInstantiatePool 
%
%  pInstantiatePool(JOB, TASK)

% Copyright 2008-2011 The MathWorks, Inc.

import parallel.internal.apishared.Pool

job.IsPoolTask = Pool.instantiate( job.pIsInteractivePool(), args );
