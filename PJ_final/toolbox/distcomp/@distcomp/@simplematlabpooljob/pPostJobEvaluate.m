function pPostJobEvaluate(job)
; %#ok Undocumented
%pPostJobEvaluate
%
%  pPostJobEvaluate(JOB)

%  Copyright 2005-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.6 $    $Date: 2010/07/19 12:49:42 $


% We shall try and determine if this job should be treated as finished.


task = getCurrentTask;
if ~( isempty( task.ErrorMessage ) && job.PoolShutdownSuccessful)
    serializer = job.Serializer;
    % This is a race for all the workers to try and put the job state
    % and finish time in the job file - so we need to lock the serializer
    % beforehand
    aLock = serializer.lock(job);
    if ~strcmp(job.State, 'finished')
        % An error occurred - abort!
        serializer.putFields(job, {'state' 'finishtime'}, ...
            {'finished' char(java.util.Date)});
    end
    serializer.release(aLock);
    % This quits MATLAB hard on all the workers, providing workers were started
    % using mpiexec.
    mpigateway( 'abort' );
end

% Wait until all of the labs reach this point.
labBarrier;

% Get the parent of this job and make sure it's an abstract scheduler
scheduler = job.Parent;
if ~isa(scheduler, 'distcomp.abstractscheduler')
    return
end


% Ensure all tasks get here, with error detection
mpigateway( 'setidle' );
mpigateway( 'setrunning' );
labBarrier;

% Decide now whether this task should modify the job state, because after
% we've called mpiParallelSessionEnding, we wont be able to tell (all
% tasks will then think that they have labindex==1)
shouldSetJobState = (scheduler.HasSharedFilesystem && job.pCanModifyJobOnWorker);

% This is the end of a parallel session
mpiParallelSessionEnding;
mpiFinalize;

% Now that all tasks are here, we can define the job to be finished. (This is
% different to how a standard job behaves)
if shouldSetJobState
    dctSchedulerMessage(4, 'About to set job state to finished');
    job.Serializer.putFields( job, {'state', 'finishtime'}, ...
        {'finished', char( java.util.Date )} );
else
    % If we aren't setting the job state then we may want to wait until
    % the task that is setting it has done so. Particularly this fixes an
    % issue on the mac with mpiexec not working correctly. To decide if
    % we want to go down this path ensure that the MDCS_MPIEXEC_WAIT_FOR_JOB_FINISHED
    % environment variable is set to the value 'true' and then we will
    % wait. We will only wait for 30s for this transition to happen.
    if strcmpi( getenv('MDCS_MPIEXEC_WAIT_FOR_JOB_FINISHED'), 'true' )
        dctSchedulerMessage(4, 'About to wait for job to be finished before proceeding');
        job.waitForState( 'finished', 30 );
        dctSchedulerMessage(4, 'Finished waiting');
    end
end


