function val = pGetTask(job, val)
; %#ok Undocumented
%pGetTask Return a handle to the first task

% Copyright 2007-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        % Get the parent string information from the object
        jobLocation = job.pGetEntityLocation;
        % Find proxies for objects parented by scheduler location
        proxies = job.Serializer.Storage.findProxies(jobLocation);
        % Create a wrapper around the new location
        if isempty(proxies)
            val = [];
        else
            val = distcomp.createObjectsFromProxies(...
                proxies(1), job.DefaultTaskConstructor, job, 'rootsearch');
        end
    catch %#ok<CTCH> swallowing errors
        %TODO
    end
end
