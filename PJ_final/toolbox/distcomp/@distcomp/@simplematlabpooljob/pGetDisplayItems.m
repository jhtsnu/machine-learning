function cellargout = pGetDisplayItems(obj,inStruct)
; %#ok undocumented
% gets the common display structure for a job object. outputs at least ONE
% display structure with a header as entries in the output cell array.

% Copyright 2006-2010 The MathWorks, Inc.

% $Revision: 1.1.6.2 $  $Date: 2010/12/09 21:06:49 $

cellargout = cell(4, 1); % initialize number of output arguments
%initialize input structures
mainStruct = inStruct;
dataDependencyStruct = inStruct;
outputTasksStruct = inStruct;
specificStruct = inStruct;

mainStruct.Type = 'paralleljob';
objState = obj.State;
% We don't display ID when ID is invalid 
if strcmp(objState, 'unavailable')
    jID = '';
else
    jID = num2str(obj.ID);
end
mainStruct.Header = ['MatlabPool Job ID ', jID];

% CalculateRunningDuration optionally returns the startTime to prevent a
% second access of that property.
import parallel.internal.cluster.DisplayFormatter;
[runningDuration, startTime] = parallel.internal.cluster.calculateRunningDuration(obj);
formattedRunningDuration = DisplayFormatter.formatRunningDuration(runningDuration);

mainStruct.Names = {'UserName', 'State', 'SubmitTime', 'StartTime', 'Running Duration'};
mainStruct.Values = {obj.UserName, objState, obj.SubmitTime, startTime, formattedRunningDuration};

% The FileDependencies ( and Path) values are truncated by top level
% pDispArgs. Only 20 lines are currently displayed if they are cell Array.
dataDependencyStruct.Header = 'Data Dependencies';
dataDependencyStruct.Names = {'FileDependencies', 'PathDependencies'};
dataDependencyStruct.Values = {obj.FileDependencies, obj.PathDependencies};

outputTasksStruct.Header = 'Associated Task(s)';
outputTasksStruct.Names = {'Number Pending ', ...
    'Number Running ', ...
    'Number Finished', ...
    'TaskID of errors'};

try
    [p, r, f] = obj.findTask;
    taskIDWithError = [];
    if ~isempty( f )
        % based on Narfi feedback ErrorMessage could be empty so need to check both fields
        dde = ~cellfun( @isempty, get( f, {'ErrorIdentifier','ErrorMessage'} ) );
        didError = or(dde(:,1), dde(:,2));
        taskIDWithErrorCell = get( f( didError ), {'ID'}  );
        taskIDWithError = [taskIDWithErrorCell{:}];
    end
    % Length of output list of parameters and values must of the same length
    outputTasksStruct.Values = {length( p ), ...
        length( r ),...
        length( f ),...
        iDisplayNothingIfEmpty(taskIDWithError)};
catch %#ok<CTCH>
    % DONE - need to display [] here
    outputTasksStruct.Values = {[], ...
        [],...
        [],...
        ''};
end

% THIRD PARTY JOBS SPECIFIC PROPERTIES
%************************************************************************
specificStruct.Header = 'Scheduler Dependent (MatlabPool Job)';
specificStruct.Names = {'MaximumNumberOfWorkers', 'MinimumNumberOfWorkers'};
specificStruct.Values = {obj.MaximumNumberOfWorkers, obj.MinimumNumberOfWorkers};

% all four categories are sent to top level pSingleObjectDisplay
cellargout{1} = mainStruct;
cellargout{2} = dataDependencyStruct;
cellargout{3} = outputTasksStruct;
cellargout{4} = specificStruct;
end


%--------------------------------------------------------------------------
% internal helper function returns a empty char if item is empty
%--------------------------------------------------------------------------
function val = iDisplayNothingIfEmpty(val)
% Force the output to an empty string if it is empty
if isempty(val)
    val = '';
end
end



