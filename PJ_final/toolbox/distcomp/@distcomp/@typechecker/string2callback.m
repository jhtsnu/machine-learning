function callback = string2callback(str)
; %#ok Undocumented
% Matlab callbacks are passed to us as strings.  Must be as a function name, a
% function handle, an anonymous function or a cell array.  We only support cell
% arrays that we know we can convert back to strings.

%   Copyright 2007-2012 The MathWorks, Inc.

if isa(str, 'java.lang.String')
    str = char(str);
end
if ~ischar(str)
    error(message('parallel:internal:cluster:TypeCheckerExpectedString', class( str )));
end
if ~isrow(str)
    error(message('parallel:internal:cluster:TypeCheckerNotRowVector'));
end

str = strtrim(char(str));

if ~isempty(str)
    switch str(1)
        case '@'
            callback = iHandleFcn(str);
        case '{'
            callback = iHandleCellArray(str);
        otherwise
            if strcmp(str,'[]')
                callback = [];
            else
                iCheckIfValidCode(str);
                % Just a regular string that we should eval
                callback = str;
            end
    end
else
    % Just a regular string that we should eval.
    callback = str;
end


function callback = iHandleFcn(str)
% Str is either a function handle or an anonymous function.
% In either case, we eval it to convert to a function handle.

% The following evaluation may well throw an error.  We can't use str2func
% because it only handles 'foo' --> @foo, and not '@foo' --> @foo.

try
    callback = eval(str);
catch err
    error(message('parallel:cluster:TypeCheckerToCallbackFailed', str, err.message));
end
if ~isa(callback, 'function_handle');
    error(message('parallel:cluster:TypeCheckerFailedNotFunctionHandle', str));
end

function callback = iHandleCellArray(str)
%Str is a cell array whose first elem is either a function handle or a function
%name.

% The following evaluation may well throw an error.  We pass that error
% unmodified to the caller.
try
    callback = eval(str);
catch err
    error(message('parallel:cluster:TypeCheckerToCellFailed', str, err.message));
end

if ~iscell(callback)
    error(message('parallel:cluster:TypeCheckerFailedNotCell', str));
end

if ~isempty(callback) && ~isa(callback{1}, 'function_handle') && ~isvarname(callback{1})
    error(message('parallel:cluster:TypeCheckerFailedFirstElementNotFunction', str));
end
% Need to verify that the remaining elements of the cell array are such that
% callback2string can handle them.  We do that by calling callback2string to see
% whether it errors.
distcomp.typechecker.callback2string(callback);

function iCheckIfValidCode(str)
% Checks if the code is a valid MATLAB code

import com.mathworks.widgets.text.mcode.MLint
messages = MLint.getMessages(str,[]);

if ~isempty(messages)
    arrayMessages = messages.toArray;
    
    for iLoop = 1: length(arrayMessages)
        eachMessage = arrayMessages(iLoop);
        if eachMessage.getSeverity > MLint.SEVERITY_THRESHOLD
            try
                % Evaluate the expression to get the exact error message
                eval(str);
            catch Mexp
                error(message('parallel:cluster:TypeCheckerToCallbackFailed', str, Mexp.message))
            end            
        end
    end
end
