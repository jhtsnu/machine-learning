function obj = typechecker() %#ok Object never returned.
; %#ok Undocumented
% Constructor for distcomp.typechecker.  Use the static methods instead of
% constructing an instance of this object.

%   Copyright 2007-2012 The MathWorks, Inc.
    
error(message('parallel:internal:cluster:TypeCheckerDoNotCallConstructor'));
