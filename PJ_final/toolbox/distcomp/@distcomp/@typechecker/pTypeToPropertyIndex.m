function index = pTypeToPropertyIndex(obj, datatype)

%   Copyright 2007-2012 The MathWorks, Inc.

; %#ok Undocumented
%Maps a datatype to an index into obj.PropertyInfo.

index = find(strcmp({obj.PropertyInfo.Type}, datatype));
if isempty(index)
    error(message('parallel:internal:cluster:TypeCheckerNoRecordFound', datatype));
end    
if length(index) > 1
    error(message('parallel:internal:cluster:TypeCheckerMultipleMatches', datatype));
end
