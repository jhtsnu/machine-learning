function oldValue = feature( name, optValue )
;%#ok<NOSEM> undocumented
%FEATURE - distcomp-specific features

% Copyright 2009-2012 The MathWorks, Inc.

persistent FEATURE_STRUCT

if isempty( FEATURE_STRUCT )
    % Add new features here.

    % Only use MPIEXEC for local scheduler on non-Mac
    shouldUseMpiexec = ~ismac;

    FEATURE_STRUCT = struct( 'LocalUseMpiexec', ...
                             shouldUseMpiexec );
    % Protect persistent data
    mlock;
end

% Get value and return, set if necessary
if isfield( FEATURE_STRUCT, name )
    oldValue = FEATURE_STRUCT.(name);
else
    error(message('parallel:internal:cluster:UnknownFeature', name));
end

if nargin == 2
    FEATURE_STRUCT.(name) = optValue;
end
