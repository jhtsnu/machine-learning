function val = pSetUserName(jm, val)
; %#ok Undocumented
% Set the user identity of the current user in the job manager proxy.

%  Copyright 2009-2012 The MathWorks, Inc.

%       

import com.mathworks.toolbox.distcomp.auth.credentials.UserIdentity;
if isa(val, 'com.mathworks.toolbox.distcomp.auth.credentials.UserIdentity')
    userIdentity = val;
else
    userIdentity = UserIdentity(val);
end

proxyManager = jm.ProxyObject;
if ~isempty(proxyManager)
    if isempty(val)
        error('distcomp:auth:InvalidUsername', ...
              'The username must not be empty.');
    end

    try
        proxyManager.setCurrentUser(userIdentity);
    catch err
        throw(distcomp.handleJavaException(jm, err));
    end
end
val = '';
