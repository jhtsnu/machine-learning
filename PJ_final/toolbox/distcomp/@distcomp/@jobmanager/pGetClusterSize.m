function val = pGetClusterSize(obj, ~)
; %#ok Undocumented
%pGetClusterSize private function to get total number of workers from java object
%
%  VAL = pGetClusterSize(OBJ, VAL)

%  Copyright 2007-2012 The MathWorks, Inc.


proxyManager = obj.ProxyObject;
try
    % Read the service info from the job manager
    serviceInfo = proxyManager.getServiceInfo;
    % Add the number of idle and number of busy workers.
    val = double(serviceInfo.getNumBusyWorkers) + double(serviceInfo.getNumIdleWorkers);
catch err
    throw(distcomp.handleJavaException(obj, err));
end
