function pUnregisterForEvents(obj, proxyObject, UUID)
; %#ok Undocumented

% Copyright 2010 The MathWorks, Inc.

proxyManager = obj.ProxyObject;
if ~isempty(proxyManager)
    % This makes a remote call, so it might error.
    try
        proxyManager.detachFromListenableObject(proxyObject, UUID);
    catch err
        throw(distcomp.handleJavaException(obj, err));
    end
end
