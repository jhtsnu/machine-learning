function connMgr = pCreateConnectionManager( jm )
; %#ok Undocumented
% pCreateConnectionManager returns a newly created ConnectionManager object
% using the ConnMgrBuilder.

% Copyright 2007-2011 The MathWorks, Inc.

brokerConnectInfo = jm.ProxyObject.getBrokerServerSocketConnectInfo;
workerAcceptInfoTemplate = jm.ProxyObject.getWorkerAcceptInfoTemplate;
useSecureMatlabPool = jm.IsUsingSecureCommunication;

connMgr = parallel.internal.apishared.ConnMgrBuilder.buildForMJS( ...
    brokerConnectInfo, workerAcceptInfoTemplate, ...
    useSecureMatlabPool );
