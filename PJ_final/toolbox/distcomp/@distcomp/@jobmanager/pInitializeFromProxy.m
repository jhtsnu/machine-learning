function pInitializeFromProxy(obj, proxy)
; %#ok Undocumented
% Runs the initialization tasks required when the job manager receives a 
% new proxy.

%   Copyright 2010-2011 The MathWorks, Inc.

try    
    
    % Get the new access objects
    jmClient          = iCreateClient(proxy);     
    jobAccess         = jmClient.getJobAccess;
    parallelJobAccess = jmClient.getParallelJobAccess;
    taskAccess        = jmClient.getTaskAccess;

    % Now we have got everything we need, update the properties
    obj.ProxyObject            = jmClient;
    obj.JobAccessProxy         = jobAccess;
    obj.ParallelJobAccessProxy = parallelJobAccess;
    obj.TaskAccessProxy        = taskAccess;   
    obj.CachedName             = char(proxy.getName);
    obj.LookupURL              = char(proxy.getLookupURL);
    
    % Add the event adaptor to the new remote event listener
    proxy.addEventAdaptor(obj.ProxyToUddAdaptor);
    % The proxy manager must be cleaned up at the end of the lifetime of
    % the job manager. Create a shutdown hook to call the dispose method
    % of the proxy manager.
    proxy.addShutdownHookForCurrentMCR();
catch err
    % Catch a possible ProxySerialization exception
    throw(distcomp.handleJavaException(obj, err));
end
end

function client = iCreateClient( proxy )

import com.mathworks.toolbox.distcomp.auth.credentials.consumer.CredentialConsumerFactory;
import com.mathworks.toolbox.distcomp.auth.credentials.store.SingleUserCredentialStore;
import com.mathworks.toolbox.distcomp.mjs.client.JobManagerClient;

if system_dependent('isdmlworker')
    % On a Worker we don't want to use the default CredentialStore or CredentialConsumerFactory
    consumerFactory = CredentialConsumerFactory.TRIVIAL_FACTORY;
    credentialStore = SingleUserCredentialStore();
    client = JobManagerClient( proxy, consumerFactory, credentialStore );
else
    client = JobManagerClient( proxy );
end

end

