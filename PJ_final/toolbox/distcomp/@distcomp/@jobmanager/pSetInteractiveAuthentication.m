function val = pSetInteractiveAuthentication(jm, val)
; %#ok Undocumented
% Set the flag for interactive authentication.
% If set to true the user might get prompted for passwords.
% If set to false no password prompts appear and any action requiring
% authentication will fail.

%  Copyright 2009-2011 The MathWorks, Inc.

%       

proxyManager = jm.ProxyObject;
if ~isempty(proxyManager)
    try
        proxyManager.getCredentialConsumerFactory().setInteractive(val);
    catch err
        throw(distcomp.handleJavaException(jm, err));
    end
end
val = false;
