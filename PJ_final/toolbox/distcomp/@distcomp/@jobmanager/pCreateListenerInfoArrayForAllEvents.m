function listenerInfoArray = pCreateListenerInfoArrayForAllEvents(obj)
; %#ok Undocumented

%   Copyright 2010 The MathWorks, Inc.

proxyManager = obj.pReturnProxyObject;
listenerInfoArray = proxyManager.createListenerInfoArrayForAllEvents;
