function val = pGetHostAddress(obj, val)
; %#ok Undocumented
%PGETHOSTADDRESS private function to get host IP address from java object
%
%  VAL = PGETHOSTADDRESS(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


import java.net.InetAddress

proxyManager = obj.ProxyObject;
try
    addresses = proxyManager.getAllHostAddresses;
    validAddresses = cell(1, numel(addresses));
    for i = 1:numel(addresses)
        jAddress = InetAddress.getByName(addresses(i));
        if ~jAddress.isLoopbackAddress
            validAddresses{i} = char(jAddress.getHostAddress);
        end
    end
    validAddresses(cellfun('isempty', validAddresses)) = [];
    val = validAddresses;
catch %#ok<CTCH> swallow errors
    % TODO
end
