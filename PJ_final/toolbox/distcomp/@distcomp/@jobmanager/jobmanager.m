function obj = jobmanager(proxyManager)
; %#ok Undocumented
%Protected constructor for jobmanager matlab objects that is called by
%findResource to create appropriate objects

% Copyright 2004-2012 The MathWorks, Inc.

% Construct the base object
obj = distcomp.jobmanager;
% Call abstract base class constructor
obj.abstractjobqueue(proxyManager);
% Set the Type
obj.Type = 'jobmanager';

% This class accepts configurations and uses the scheduler section.
sectionName = 'scheduler';
obj.pInitializeForConfigurations(sectionName);

iCreateProxyToUddAdaptor(obj);
obj.pInitializeFromProxy(proxyManager);

% If web licensing is required, force login here
if proxyManager.requireWebLicensing && ~system_dependent('isdmlworker')
    if isdeployed
        error(message('parallel:cluster:CannotDeployWithMHLM', ...
                char(proxyManager.getName), char(proxyManager.getHostName)));
    end
    desktopClient = parallel.internal.webclients.currentDesktopClient();
    token = desktopClient.LoginToken;
    if isempty( token )
        E = MException( 'parallel:cluster:mjs:NoMDCSLicense', ...
                ['This cluster requires that your MathWorks account is ', ...
                 'associated with a MATLAB Distributed Computing Server ', ...
                 'license that is managed online.'] );
        error(E);
    end
end

% Make sure the proxy is disposed of when this object is destroyed.
obj.DestructionListener = handle.listener(obj, ...
    'ObjectBeingDestroyed', @iObjectBeingDestroyed);
end

function iCreateProxyToUddAdaptor(obj)
    % Get a UDD version of the java object that will dispatch callback events
    % into the UDD objects. This java object is exported such that the jobmanager
    % can send us events.
    obj.ProxyToUddAdaptor = handle(com.mathworks.toolbox.distcomp.uddadaptor.ProxyToUddAdaptor);
    % We need a mechanism to get the actual events from a java RMI call directly
    % into matlab - this is done via the java Bean interface of this java object
    % However we also need to be careful of the lifetime of this java object, so
    % We are very explicit in decoupling using listeners rather than matlab 
    % Callbacks because this means that we don't store a reference to objectroot
    % in the actual java layer - and hence clear classes will continue to work
    obj.ProxyToUddAdaptorListener = handle.listener(...
        obj.ProxyToUddAdaptor, ...  
        'ProxyToUddAdaptorEvent', ...
        @iDispatchJavaEvent);
    % Undocumented option to set the recursion limit on callbacks to 1024 - this
    % is a silly number that will be bounded by the RecursionLimit in MATLAB. 
    % Hopefully our demos will continue to work provided the main RecursionLimit
    % is upped appropriately
    set(obj.ProxyToUddAdaptorListener, 'RecursionLimit', 1024);
end

function iDispatchJavaEvent(src, event)
    % Make sure that once this function is finished we let the java layer
    % know that we are finished
    o = onCleanup(@() src.matlabEventFinished(event.javaEvent));
    try
        % Get the actual event from the UDD wrapped event
        event = event.JavaEvent;
        % Find the udd object which this event is sourced by - the UUID of
        % the source is held in event.getSource which should be found in
        % the proxyHashtable.
        root = distcomp.getdistcompobjectroot;
        uddObj = handle(root.proxyHashtable.get(event.getSource));
        % If we found a valid object then tell it to dispatch the
        % appropriate event as indicated in event.getID
        if ~isempty(uddObj)
            uddObj.pDispatchJavaEvent(src, event);
        end
    catch err %#ok<NASGU>
        % Do nothing - swallow the error silently
    end
end

function iObjectBeingDestroyed(obj, ~)
    % Dispose of the job manager proxy when this object is destroyed.
    proxyManager = obj.pReturnProxyObject;
    proxyManager.dispose();
end
