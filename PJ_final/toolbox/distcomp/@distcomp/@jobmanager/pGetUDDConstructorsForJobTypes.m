function ctors = pGetUDDConstructorsForJobTypes(jm, jobTypes) %#ok<INUSL>
; %#ok Undocumented
%pGetUDDConstructorsForJobTypes Return the correct constructors for jobs

% Copyright 2005-2007 The MathWorks, Inc.

% $Revision: 1.1.10.4 $    $Date: 2011/08/27 18:34:02 $

import com.mathworks.toolbox.distcomp.workunit.JobMLType;

% Deal with the empty input first
if isempty(jobTypes)
    ctors = {};
    return
end

% Check if all jobTypes are identical
if all(jobTypes == jobTypes(1))
    % Return a single constructor in this case
    jobTypes = jobTypes(1);
end

numJobs = numel(jobTypes);
% Construct the output cell array
ctors = cell(numJobs, 1);

for i = 1:numJobs
    if JobMLType.isStandardJob( jobTypes(i) )
        ctors{i} = @distcomp.job;
    elseif JobMLType.isParallelJob( jobTypes(i) )
        ctors{i} = @distcomp.paralleljob;
    elseif JobMLType.isMatlabpoolJob( jobTypes(i) )
        ctors{i} = @distcomp.matlabpooljob;
    else
        % Do nothing?
    end
end

% Unwrap the single output
if numJobs == 1
    ctors = ctors{1};
end
