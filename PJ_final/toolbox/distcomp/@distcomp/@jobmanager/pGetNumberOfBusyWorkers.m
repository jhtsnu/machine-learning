function val = pGetNumberOfBusyWorkers(obj, val)
; %#ok Undocumented
%pGetNumberOfBusyWorkers private function to get number of workers from java object
%
%  VAL = pGetNumberOfBusyWorkers(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyManager = obj.ProxyObject;
try
    val = double(proxyManager.getNumBusyWorkers);
catch %#ok<CTCH> swallow errors
    % TODO
end
