function val = pGetCredentialStore(jm, ~)
; %#ok Undocumented
%PGETCREDENTIALSTORE
%
%  VAL = PGETCREDENTIALSTORE(JM, VAL)

%  Copyright 2011 The MathWorks, Inc.

proxyManager = jm.ProxyObject;
try
    val = proxyManager.getCredentialStore();
catch err %#ok<NASGU>
    % Do nothing.
    val = [];
end
