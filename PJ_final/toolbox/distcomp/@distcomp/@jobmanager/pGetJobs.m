function val = pGetJobs(jm, val)
; %#ok Undocumented
%PGETJOBS A short description of the function
%
%  VAL = PGETJOBS(JM, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


try
    % Get the java tasks from the job
    [proxyJobs, jobTypes] = jm.pGetJobsAndTypesFromProxy;
    % Need to get the correct constructor for a job
    constructors = jm.pGetUDDConstructorsForJobTypes(jobTypes);
    % Now try and construct the jobs
    val = distcomp.createObjectsFromProxies(proxyJobs, constructors, jm);
catch %#ok<CTCH> swallow errors
    % TODO
end
