function val = pGetGroup(obj, val)
; %#ok Undocumented
%PGETGROUP private function to get jobmanager group from java object
%
%  VAL = PGETGROUP(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyManager = obj.ProxyObject;
try
    val = char(proxyManager.getLookupGroup);
catch %#ok<CTCH> swallow errors
    % TODO
end
