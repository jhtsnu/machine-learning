function val = pGetHostname(obj, val)
; %#ok Undocumented
%PGETHOSTNAME private function to get hostname from java object
%
%  VAL = PGETHOSTNAME(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyManager = obj.ProxyObject;
try
    val = char(proxyManager.getHostName);
catch %#ok<CTCH> swallow errors
    % TODO
end
