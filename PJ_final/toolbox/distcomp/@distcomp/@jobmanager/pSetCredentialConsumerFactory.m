function val = pSetCredentialConsumerFactory(jm, val)
; %#ok Undocumented
% Set the consumer factory in the job manager proxy.

%  Copyright 2010-2011 The MathWorks, Inc.

%       

proxyManager = jm.ProxyObject;
if ~isempty(proxyManager)
    try
        proxyManager.setCredentialConsumerFactory(val);
    catch err
        throw(distcomp.handleJavaException(jm, err));
    end
end
val = [];
