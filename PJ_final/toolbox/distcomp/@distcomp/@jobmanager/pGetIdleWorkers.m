function val = pGetIdleWorkers(obj, val)
; %#ok Undocumented
%pGetIdleWorkers A short description of the function
%
%  VAL = pGetIdleWorkers(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyManager = obj.ProxyObject;
try
    proxyWorkers = proxyManager.getIdleWorkers;
    val = distcomp.createObjectsFromProxies(proxyWorkers, @distcomp.worker, distcomp.getdistcompobjectroot);
catch %#ok<CTCH> swallow errors
    % TODO
end
