function val = pGetNumberOfIdleWorkers(obj, val)
; %#ok Undocumented
%pGetNumberOfIdleWorkers private function to get number of worksers from java object
%
%  VAL = pGetNumberOfIdleWorkers(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyManager = obj.ProxyObject;
try
    % Get the java tasks from the job
    val = double(proxyManager.getNumIdleWorkers);
catch %#ok<CTCH> swallow errors
    % TODO
end
