function val = pGetBusyWorkers(obj, val)
; %#ok Undocumented
%pGetBusyWorkers A short description of the function
%
%  VAL = pGetBusyWorkers(OBJ, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


proxyManager = obj.ProxyObject;
try
    proxyWorkers = proxyManager.getBusyWorkers;
    val = distcomp.createObjectsFromProxies(proxyWorkers, @distcomp.worker, distcomp.getdistcompobjectroot);
catch %#ok<CTCH> swallow errors
    % TODO
end
