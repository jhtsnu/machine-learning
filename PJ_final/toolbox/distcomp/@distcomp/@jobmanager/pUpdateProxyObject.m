function pUpdateProxyObject(objs, proxies)
; %#ok Undocumented
%pUpdateProxyObject insert a new java proxy object into this UDD wrapper
%
%  pUpdateProxyObject(OBJS, PROXIES)

%  Copyright 2004-2011 The MathWorks, Inc.


for i = 1:numel(objs)
    thisJM = objs(i);
    thisProxy = proxies(i);
    % Let's try a trick to see if the current proxy is working
    try
        thisJM.ProxyObject.getState;
        PROXY_INACTIVE = false;
    catch err %#ok<NASGU>
        PROXY_INACTIVE = true;
        INACTIVE_PROXY = thisJM.ProxyObject;
    end        
    % Update the cached proxies
    if PROXY_INACTIVE
        thisJM.pInitializeFromProxy(thisProxy);

        % No longer using the inactive proxy, so we can dispose of it
        if ~isempty(INACTIVE_PROXY)
            INACTIVE_PROXY.dispose();
            INACTIVE_PROXY = [];
        end
                
        % Now propagate the changes to the children correctly
        nextJob = thisJM.down;
        % Define a fake parent changed event to send to each job
        jobEvent.NewParent = thisJM;
        while ~isempty(nextJob)
            % Call the event callback directly to update the job proxies
            nextJob.pSetMyProxyObject(jobEvent);
            % And iterate over the jobs tasks
            nextTask = nextJob.down;
            % Define a fake parent changed event to pass to each task
            taskEvent.NewParent = nextJob;
            while ~isempty(nextTask)
                % Call the event callback directly to update the task proxies
                nextTask.pSetMyProxyObject(taskEvent);
                % Get the next task
                nextTask = nextTask.right;
            end
            % Get the next job
            nextJob = nextJob.right;
        end
    else
        % dispose of the proxy we chose not to use
        thisProxy.dispose();
    end
end
