function string = pMakeTaskIDString(~, tasks)
; %#ok Undocumented
%pMakeTaskIDString make the LSF job array string for the correct task ID
%
% If a task has been destroyed prior to be submitted then it is possible
% that the list of filenames we will use is not appropriate to the job
% index array. Thus we need to ensure that the list of indices we pass to
% lsf is correct.

%  Copyright 2005-2011 The MathWorks, Inc.

import parallel.internal.apishared.LsfUtils

numTasks = numel(tasks);
% We know that ID's are monotonically increasing so firstly lets check if
% no tasks have been deleted - probably the most common case
if tasks(end).ID == numTasks
    string = sprintf('1-%d', numTasks);
else
    % Get all the IDs as a cell array
    IDs = get(tasks, {'ID'});
    % Convert to standard array
    IDs = [IDs{:}];
    string = LsfUtils.makeTaskIdString( IDs );
end
