function pSubmitNonSharedJob(lsf, job, tasks)
; %#ok Undocumented
%pSubmitNonSharedJob 
%
%  pSubmitNonSharedJob(SCHEDULER, JOB)

%  Copyright 2005-2011 The MathWorks, Inc.

import parallel.internal.apishared.FilenameUtils
import parallel.internal.apishared.LsfUtils

storage = lsf.Storage;
% Ensure we are only doing this with file storage 
if ~isa(storage, 'distcomp.filestorage')
    error('distcomp:lsfscheduler:InvalidStorage', 'Currently the only supported storage type for non-shared filesystems is filestorage');
end


% Define the function that will be used to decode the environment variables
setenv('MDCE_DECODE_FUNCTION', 'decodeLsfSingleZippedTask');

% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end - note that the storage object will be
% reconstructed in CWD at the far end
[~, stringConstructor] = storage.getSubmissionStrings;
setenv('MDCE_STORAGE_CONSTRUCTOR', stringConstructor);

% Get the location of the storage
jobLocation = job.pGetEntityLocation;
setenv('MDCE_JOB_LOCATION', jobLocation);


% Submit all the tasks in one go using a job array - the far end will pick
% up the task number automatically using the LSB_JOBINDEX environment
% variable. The submit string below says
% -H hold the job in the PSUSP state until we call bresume
% -J "name[1-N]" create a job array with 1-N sub tasks
% Add the lsf SubmitArguments as required
% Execute the command matlab -dmlworker -nodisplay -r distcomp_evaluate_filetask > logfile
[~, matlabExe, matlabArgs] = lsf.pCalculateMatlabCommandForJob(job);
matlabExe = FilenameUtils.quoteForClient( matlabExe );

% Reduce the job in question to a series of zip files for compactness
storageLocation = storage.StorageLocation;

% Currently LSF doesn't correctly transfer files if we place a space in
% the -f argument to bsub so we need to throw an error rather than 
% proceed if this is the case - see GECK 322231
if any(isspace(storageLocation))
    error('distcomp:lsfscheduler:UnableToSubmit', ...
    ['LSF cannot copy files to the remote machine if the DataLocation contains any whitespace characters\n' ...
    'Please use a DataLocation that does not contain spaces or whitespace%s'], '.');
end

[filesToCopy, remoteTaskLoc, logRelToStorage] = ...
    LsfUtils.calcFilesToCopy( storageLocation, jobLocation );
taskIDString = lsf.pMakeTaskIDString(tasks);

% Check for non-empty logLocation and add the -o in front 
logLocation = ['-o ' FilenameUtils.quoteForClient( [remoteTaskLoc '.log'] ) ];

submitString = sprintf(...
    'bsub -H -J "%s[%s]" %s  %s  %s  %s %s'  , ...
    jobLocation, ...
    taskIDString, ...
    logLocation, ...
    filesToCopy, ...
    lsf.SubmitArguments, ...
    matlabExe, matlabArgs);

lsfID = LsfUtils.callBsub( submitString );

schedulerData = struct('type', 'lsf', 'lsfID', lsfID, 'logRelToStorage', logRelToStorage);
job.pSetJobSchedulerData(schedulerData);

% Zip up the out going files after we have finished modifying the common file
storage.serializeForSubmission(job);

LsfUtils.callBresume( lsfID, taskIDString )
