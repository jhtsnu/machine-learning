function OK = pIsAcceptableBkillError(~, bkillString)
; %#ok Undocumented
%pIsAcceptableBkillError is this an expected bkill error message
%
%  pIsAcceptableBkillError(SCHEDULER, BKILLOUTPUT)

%  Copyright 2005-2011 The MathWorks, Inc.

import parallel.internal.apishared.LsfUtils
OK = LsfUtils.isAcceptableBkillError( bkillString );

