function obj = lsfscheduler(proxyScheduler)
; %#ok Undocumented
%LSFSCHEDULER concrete constructor for this class
%
%  OBJ = LSFSCHEDULER(OBJ)

%  Copyright 2000-2011 The MathWorks, Inc.

import parallel.internal.apishared.LsfUtils

obj = distcomp.lsfscheduler;

LsfUtils.environmentSetup();

[masterName, clusterName] = LsfUtils.getMasterAndClusterNames();

set(obj, ...
    'Type', 'lsf', ...
    'Storage', handle(proxyScheduler.getStorageLocation), ...
    'ClusterName', clusterName, ...
    'MasterName', masterName, ...
    'HasSharedFilesystem', true );

% Set up the object for parallel execution - default to assuming that the
% cluster is the same type as the client.
if ispc
    workerType = 'pc';
else
    workerType = 'unix';
end
obj.setupForParallelExecution( workerType );

% This class accepts configurations and uses the scheduler section.
sectionName = 'scheduler';
obj.pInitializeForConfigurations(sectionName);

end
