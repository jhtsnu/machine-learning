function state = pGetJobState(scheduler, job, state) %#ok<INUSL>
; %#ok Undocumented
%pGetJobState - deferred call to ask the scheduler for state information
%
%  STATE = pGetJobState(SCHEDULER, JOB, STATE)

%  Copyright 2005-2011 The MathWorks, Inc.

import parallel.internal.apishared.LsfUtils

% Only ask the scheduler what the job state is if it is queued or running
if strcmp(state, 'queued') || strcmp(state, 'running')
    % Get the information about the actual scheduler used
    data = job.pGetJobSchedulerData;
    if isempty(data)
        return
    end
    % Is the job actually an LSF job?
    if ~strcmp(data.type, 'lsf')
        return
    end
    % Finally let's get the actual jobID
    jobID = data.lsfID;

    [FAILED, numPendRunFail, failedLsfIndex] = ...
        LsfUtils.getStateInformation( jobID );
    if FAILED
        % Already warned
        return
    end
    % Now deal with the logic surrounding these
    % Any running indicates that the job is running
    if numPendRunFail(2) > 0
        state = 'running';
        return
    end
    % We know numRunning == 0 so if there are some still pending then the
    % job must be queued again, even if there are some finished
    if numPendRunFail(1) > 0
        state = 'queued';
        return
    end

    % Deal with any tasks that have failed - ensure that we write the
    % failed string to the task
    if numPendRunFail(3) > 0
        % Set this job to be failed
        state = 'failed';
        % Find the correct tasks with those IDs
        tasks = job.Tasks;
        failedTasks = handle(-ones(numel(failedLsfIndex), 1));
        failedTasksIndex = 0;
        for i = 1:numel(tasks)
            if ismember(tasks(i).ID, failedLsfIndex)
                failedTasksIndex = failedTasksIndex + 1;
                failedTasks(failedTasksIndex) = tasks(i);
            end
        end
        % For each failed task write the failed state correctly
        for i = 1:failedTasksIndex
            thisTask = failedTasks(i);
            if ~strcmp(thisTask.State, 'finished')
                thisTask.pSetState('failed');
            end
        end
    else
        % numPending and numRunning are 0, so there could be some finished or
        % there could be none finished. If none, this is because the job has
        % fallen off the recent LSF list. In either case it is likely that all
        % parts of the job are finished. However, lets quickly check that all
        % tasks are finished if numFinished == 0. If all tasks have actually
        % finished then mark the job as finished
        state = job.pGetStateFromTasks;
    end
    % Now set the terminal state on the job.
    job.pSetTerminalStateFromScheduler(state);
end

