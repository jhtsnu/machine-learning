function pSubmitJob(lsf, job)
; %#ok Undocumented
%pSubmitJob A short description of the function
%
%  pSubmitJob(SCHEDULER, JOB)

%  Copyright 2005-2011 The MathWorks, Inc.

import parallel.internal.apishared.FilenameUtils
import parallel.internal.apishared.LsfUtils

tasks = job.Tasks;
numTasks = numel(tasks);
if numTasks < 1
    error('distcomp:lsfscheduler:InvalidState', 'A job must have at least one task to submit to an LSF scheduler.');
end

% Ensure that the job has been prepared
job.pPrepareJobForSubmission;

% Do something radically different when the storage is not shared.
if ~lsf.HasSharedFilesystem
    lsf.pSubmitNonSharedJob(job, tasks);
    return
end

% Define the function that will be used to decode the environment variables
setenv('MDCE_DECODE_FUNCTION', 'decodeLsfSingleTask');

storage = job.pReturnStorage;
% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end
[stringLocation, stringConstructor] = storage.getSubmissionStrings;
setenv('MDCE_STORAGE_LOCATION', stringLocation);
setenv('MDCE_STORAGE_CONSTRUCTOR', stringConstructor);

% Get the location of the storage
jobLocation = job.pGetEntityLocation;

setenv('MDCE_JOB_LOCATION', jobLocation);

% Define the location for logs to be returned
logLocation = '';
if isa(storage, 'distcomp.filestorage')
    [clientLog, logLocation] = lsf.pJobSpecificFile( job, 'Task%I.log', true ); %#ok
    logRelToStorage = [job.pGetEntityLocation, '/Task%I.log'];
end
% Submit all the tasks in one go using a job array - the far end will pick
% up the task number automatically using the LSB_JOBINDEX environment
% variable. The submit string below says
% -H hold the job in the PSUSP state until we call bresume
% -J "name[1-N]" create a job array with 1-N sub tasks
% Add the lsf SubmitArguments as required
% Execute the command matlab -dmlworker -nodisplay -r distcomp_evaluate_filetask > logfile
[~, matlabExe, matlabArgs] = lsf.pCalculateMatlabCommandForJob(job);

matlabExe = FilenameUtils.quoteForClient( matlabExe );
% Check for non-empty logLocation and add the -o in front 
if ~isempty(logLocation)
    logLocation = ['-o ' FilenameUtils.quoteForClient( logLocation )];
end
% Create taskIDString from the ID's of the tasks in this job
taskIDString = lsf.pMakeTaskIDString(tasks);

submitString = sprintf(...
    'bsub -H -J "%s[%s]" %s  %s  %s  %s'  , ...
    jobLocation, ...
    taskIDString, ...
    logLocation, ...
    lsf.SubmitArguments, ...
    matlabExe, ...
    matlabArgs);

lsfID = LsfUtils.callBsub( submitString );

schedulerData = struct('type', 'lsf', 'lsfID', lsfID, 'logRelToStorage', logRelToStorage);
job.pSetJobSchedulerData(schedulerData);
LsfUtils.callBresume( lsfID, taskIDString )

