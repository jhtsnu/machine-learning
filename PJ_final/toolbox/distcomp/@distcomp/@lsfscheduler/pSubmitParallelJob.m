function pSubmitParallelJob( lsf, job )
; %#ok Undocumented
%pSubmitParallelJob submit a parallel job to LSF
%
%  pSubmitParallelJob(SCHEDULER, JOB)

%  Copyright 2005-2011 The MathWorks, Inc.

import parallel.internal.apishared.LsfUtils
import parallel.internal.apishared.FilenameUtils

if ~lsf.HasSharedFilesystem
    error( 'distcomp:lsfscheduler:parallelshared', ...
           'Parallel execution requires a shared filesystem' );
end

% Check the consistency of min workers and cluster size
lsf.pMinWorkersClusterSizeConsistentCheck(job);

% Duplicate the tasks for parallel execution
job.pDuplicateTasks;

% Ensure that the job has been prepared
job.pPrepareJobForSubmission;

% Define the function that will be used to decode the environment variables
setenv('MDCE_DECODE_FUNCTION', 'decodeLsfSimpleParallelTask');
storage = job.pReturnStorage;
% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end
[stringLocation, stringConstructor] = storage.getSubmissionStrings;
setenv('MDCE_STORAGE_LOCATION', stringLocation);
setenv('MDCE_STORAGE_CONSTRUCTOR', stringConstructor);

% Get the location of the storage
jobLocation = job.pGetEntityLocation;
setenv('MDCE_JOB_LOCATION', jobLocation);

% For parallel execution, we simply forward ClusterMatlabRoot to the cluster
% using an environment variable. This means we don't need to worry about
% quoting it here.
if ~isempty( lsf.ClusterMatlabRoot )
    setenv( 'MDCE_CMR', lsf.ClusterMatlabRoot );
else
    setenv( 'MDCE_CMR', '' );
end

% For parallel execution, the wrapper script chooses which matlab executable
% to launch via mpiexec, so we only pass the options that need to be added
% to the MATLAB command-line.
matlabCommand = lsf.pCalculateMatlabCommandForJob(job);


% Copy the wrapper script to somewhere we know it will be accessible on the
% cluster - i.e. put it within the job subdirectory. This relies on
% filestorage. To support some other sort of storage, we could simply pipe
% the contents of the wrapper script into the bsub command
if isa( storage, 'distcomp.filestorage' )
    % All the work here will be done by pJobSpecificFile
else
    error( 'distcomp:lsfscheduler:NotFileStorage', ...
           'The LSF scheduler may only use file storage for executing parallel jobs' );
end

if strcmp( lsf.ClusterOsType, 'mixed' )
    error( 'distcomp:lsfscheduler:BadClusterType', ... 
           ['The LSF scheduler cannot operate on ''mixed'' worker machines. Please configure ', ...
            'the scheduler to have ClusterOsType ''pc'' or ''unix'''] );
end

scriptOnCluster = LsfUtils.copyWrapperScript( ...
    lsf.ParallelSubmissionWrapperScript, lsf.ClusterOsType, ...
    job.pReturnStorage().getStorageLocationStruct(), ...
    job.pGetEntityLocation() );
jobLogTrail = sprintf( 'Job%d.log', job.ID );
[~, clusterLogLocation] = lsf.pJobSpecificFile( job, jobLogTrail, false );
logRelToStorage = [job.pGetEntityLocation, '/', jobLogTrail];
logLocation = ['-o ', FilenameUtils.quoteForClient( clusterLogLocation )];

% Here, we need to quote the scriptOnCluster in such a way that LSF will
% correctly execute that script. We need to quote the script name twice to
% protect against the system command AND LSF's execution of that command.
if ispc
    % Use a backslash-protected double-quote
    quotedScript = ['"\"', scriptOnCluster, '\""'];
else
    % Use a single-quote-protected double-quote
    quotedScript = ['''"', scriptOnCluster, '"'''];
end

submitString = sprintf( 'bsub -H -J %s -n %d,%d %s %s %s %s', ...
                        jobLocation, job.MinimumNumberOfWorkers, ...
                        job.MaximumNumberOfWorkers, ...
                        logLocation, ...
                        lsf.SubmitArguments, ...
                        quotedScript, matlabCommand );

lsfID = LsfUtils.callBsub( submitString );

schedulerData = struct('type', 'lsf', 'lsfID', lsfID, 'logRelToStorage', logRelToStorage);
job.pSetJobSchedulerData(schedulerData);
LsfUtils.callBresume( lsfID )
