function [proxies, constructors] = findProxies(~, ~) %#ok<STOUT>
; %#ok Undocumented
%findProxies 
%
%  PROXIES = FINDPROXIES(STORAGE, parentLocation)
%
% 

%  Copyright 2004-2012 The MathWorks, Inc.


error(message('parallel:internal:AbstractStorageMethodCall'));
