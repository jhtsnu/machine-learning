function val = pSetStorageLocation(~, ~) %#ok<STOUT>
; %#ok Undocumented

%  Copyright 2000-2012 The MathWorks, Inc.


error(message('parallel:internal:AbstractStorageMethodCall'));
