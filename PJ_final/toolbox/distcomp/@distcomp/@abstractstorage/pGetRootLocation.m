function location = pGetRootLocation(~) %#ok<STOUT>
; %#ok Undocumented
%pGetRootLocation 
%
%  LOCATION = pGetRootLocation(STORAGE)

%  Copyright 2000-2012 The MathWorks, Inc.

error(message('parallel:internal:AbstractStorageMethodCall'));
