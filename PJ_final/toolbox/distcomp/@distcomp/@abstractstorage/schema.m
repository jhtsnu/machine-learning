function schema
%SCHEMA defines the distcomp.abstractstorage class
%

%   Copyright 1984-2012 The MathWorks, Inc.


hThisPackage = findpackage('distcomp');
hParentClass = hThisPackage.findclass('lockedobject');
hThisClass   = schema.class(hThisPackage, 'abstractstorage', hParentClass);
mlock

p = schema.prop(hThisClass, 'Serializer', 'handle');
p.AccessFlags.PublicSet = 'off';


p = schema.prop(hThisClass, 'StorageLocation', 'MATLAB array');
p.AccessFlags.PublicSet = 'off';
p.SetFunction = @pSetStorageLocation;

p = schema.prop(hThisClass, 'IsReadOnly', 'bool');
p.AccessFlags.PublicSet = 'off';

