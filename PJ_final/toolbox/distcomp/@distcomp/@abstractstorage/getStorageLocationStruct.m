function value = getStorageLocationStruct(obj) %#ok<STOUT,INUSD>
; %#ok Undocumented
% 
% Abstract method that should be overloaded by 
% subclass. Returns information about the storage in a structure.  
% Currently used by genericscheduler.getDataLocation.
% 

%  Copyright 2010-2012 The MathWorks, Inc.


error(message('parallel:internal:AbstractStorageMethodCall'));
