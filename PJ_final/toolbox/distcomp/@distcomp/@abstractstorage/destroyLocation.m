function destroyLocation(~, ~)
; %#ok Undocumented
%destroyLocation remove a location from storage
%
%  DESTROYLOCATION(OBJ, entityLocation)
%
% The input parent is a string without an extension, which uniquely
% identifies the parent of the locations we are trying to create

%  Copyright 2005-2012 The MathWorks, Inc.


error(message('parallel:internal:AbstractStorageMethodCall'));
