function dataLoc = pGetDisplayItem() %#ok<STOUT>
; %#ok Undocumented
% 
% Abstract method that should be overloaded by 
% subclass. Gets the platform specific data location if there is one.
% used for object display as the DataLocation property only returns the
% currently used DataLocation.
% 

%  Copyright 2007-2012 The MathWorks, Inc.

error(message('parallel:internal:AbstractStorageMethodCall'));
