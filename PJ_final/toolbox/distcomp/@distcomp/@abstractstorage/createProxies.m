function proxies = createProxies(~, ~, ~, ~) %#ok<STOUT>
; %#ok Undocumented
%createProxies creates a new array of entities
%
%  PROXIES = CREATEPROXIES(STORAGE, parentLocation, numberToCreate, constructor)
%


%  Copyright 2004-2012 The MathWorks, Inc.


error(message('parallel:internal:AbstractStorageMethodCall'));
