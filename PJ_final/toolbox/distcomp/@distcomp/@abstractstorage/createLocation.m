function location = createLocation(~, ~, ~) %#ok<STOUT>
; %#ok Undocumented
%createLocation creates an empty location in the storage for an entity
%
%  LOCATION = CREATELOCATION(OBJ, PARENT, NUMBER)
%
% The input parent is a string without an extension, which uniquely
% identifies the parent of the locations we are trying to create

%  Copyright 2004-2012 The MathWorks, Inc.


error(message('parallel:internal:AbstractStorageMethodCall'));
