function pAddListeners(obj, L)
; %#ok Undocumented
%ADDLISTENERS Add listeners to component store
%
%  ADDLISTENERS(OBJ, L) adds the listener vector L to the list of listeners
%  being held in the component.

%  Copyright 2000-2012 The MathWorks, Inc.


obj.EventListeners = [obj.EventListeners; L(:)];
