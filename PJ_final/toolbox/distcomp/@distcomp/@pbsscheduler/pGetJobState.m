function state = pGetJobState( pbs, job, state )
; %#ok Undocumented

% pGetJobState - ask PBS for job state information

% Copyright 2007-2011 The MathWorks, Inc.

import parallel.internal.apishared.PbsHelper

% Only ask the scheduler what the job state is if it is queued or running
if strcmp(state, 'queued') || strcmp(state, 'running')

    % Get the information about the actual scheduler used
    data = job.pGetJobSchedulerData;
    if isempty(data) || ~strcmp( data.type, 'pbs' )
        return
    end

    % Finally let's get the actual jobIDs
    jobIDs = data.pbsJobIds;

    % Actually, for running/pending calculation, we don't care about the state
    % of the subjobs, so treat everything the same.
    [anyRunning, anyPending, FAILED] = PbsHelper.getRunningPending( ...
        pbs.StateIndicators, jobIDs );

    if FAILED
        % Already warned in iGetRunningPending
        return
    end
    
    % Now deal with the logic surrounding these
    % Any running indicates that the job is running
    if anyRunning
        state = 'running';
        return
    end

    % We know numRunning == 0 so if there are some still pending then the
    % job must be queued again, even if there are some finished
    if anyPending
        state = 'queued';
        return
    end

    % Ensure that all tasks have the right state
    if ~anyRunning && ~anyPending
        
        stateFromTasks = job.pGetStateFromTasks;
        
        if strcmp( stateFromTasks, 'finished' )
            % Ok, we're good, the job will be set 'finished'
	        jobState = 'finished';
        else
            % Bad - there are no running or pending tasks, so let's overwrite any tasks
            % that aren't finished to 'failed', and then set the job to be failed.
            jobState = 'failed';
        end

        job.pSetTerminalStateFromScheduler(jobState);
    end
end
