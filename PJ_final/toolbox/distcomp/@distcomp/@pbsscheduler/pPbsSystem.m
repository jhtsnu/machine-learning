function [FAILED, out] = pPbsSystem( ~, cmd )
; %#ok Undocumented

% Copyright 2007-2011 The MathWorks, Inc.
[FAILED, out] = parallel.internal.apishared.PbsHelper.system( cmd );
