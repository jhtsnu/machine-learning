function pSubmitParallelJob( pbs, job )
; %#ok Undocumented
%pSubmitParallelJob submit a parallel job to PBS
%
%  pSubmitParallelJob(SCHEDULER, JOB)

%  Copyright 2007-2011 The MathWorks, Inc.

import parallel.internal.apishared.PbsHelper

tasks = job.Tasks;

pbs.pPreSubmissionChecks( job, tasks );

% Check the consistency of min workers and cluster size
pbs.pMinWorkersClusterSizeConsistentCheck(job);

% Duplicate the tasks for parallel execution
job.pDuplicateTasks;

% Ensure that the job has been prepared
job.pPrepareJobForSubmission;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generic things to do with setting up environment variables

% Define the function that will be used to decode the environment variables
setenv('MDCE_DECODE_FUNCTION', 'decodePbsSimpleParallelTask');
storage = job.pReturnStorage;
% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end
[stringLocation, stringConstructor] = storage.getSubmissionStrings;
setenv('MDCE_STORAGE_LOCATION', stringLocation);
setenv('MDCE_STORAGE_CONSTRUCTOR', stringConstructor);

% Get the location of the storage
jobLocation = job.pGetEntityLocation;
setenv('MDCE_JOB_LOCATION', jobLocation);

% For parallel execution, we simply forward ClusterMatlabRoot to the cluster
% using an environment variable. This means we don't need to worry about
% quoting it here.
setenv( 'MDCE_CMR', pbs.ClusterMatlabRoot );

% For parallel execution, the wrapper script chooses which matlab executable
% to launch via mpiexec, so we only pass the options that need to be added
% to the MATLAB command-line.
[~, matlabExe, matlabArgs] = pbs.pCalculateMatlabCommandForJob( job );

setenv( 'MDCE_MATLAB_EXE', matlabExe );
if ispc && isempty( matlabArgs )
    % Work around problem whereby an empty setting for this causes PBS to think
    % that it cannot send the environment
    matlabArgs = ' ';
end
setenv( 'MDCE_MATLAB_ARGS', matlabArgs );
setenv( 'MDCE_TOTAL_TASKS', num2str( numel( job.Tasks ) ) );
setenv( 'MDCE_REMSH', pbs.RshCommand );
if pbs.UsePbsAttach
    setenv( 'MDCE_USE_ATTACH', 'on' );
else
    setenv( 'MDCE_USE_ATTACH', 'off' );
end

% Store the scheduler type so the runprop on the far end can be correctly
% constructed. Ensure it's lower case for distcomp.getSchedulerUDDConstructor.
setenv( 'MDCE_SCHED_TYPE', lower( pbs.Type ) );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Everything to do with setting up the qsub command-line arguments
actuallyUseJobArrays = false;
selectStr = PbsHelper.calculateResourceArg( pbs.ResourceTemplate, numel( job.Tasks ) );
helper    = PbsHelper( pbs.ClusterOsType, pbs.ResourceTemplate, actuallyUseJobArrays, ...
                       pbs.UsePbsAttach, pbs.RcpCommand, storage.getStorageLocationStruct(), ...
                       job.ID, {} ); % no task IDs required for communicating job

% Define the location for logs to be returned
[logLocation, relLog, absLog] = helper.chooseCommJobLog( job.pGetEntityLocation() );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Copy the submission script into the storage, and submit it directly from
% there. Under PBS, it doesn't matter if the submission script is not
% available on the cluster.

if isa( storage, 'distcomp.filestorage' )
    % All the work here will be done by pJobSpecificFile
else
    error( 'distcomp:pbsscheduler:NotFileStorage', ...
           'The PBS scheduler may only use file storage for executing parallel jobs' );
end

% Deal with the copying the wrapper script into the job subdirectory. 
[clientWrapper, doAppend] = helper.copyParallelWrapper( ...
    job.pGetEntityLocation(), pbs.ParallelSubmissionWrapperScript );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Actual submission

cmdLineArgs = sprintf( '%s %s %s ', selectStr, pbs.SubmitArguments, logLocation );

storedEnv = distcomp.pClearEnvironmentBeforeSubmission();

try
    % Make the shelled out call to bsub.
    [FAILED, out, pbsJobID] = helper.submitCommunicatingJob( clientWrapper, cmdLineArgs, doAppend );
catch err
    FAILED = true;
    out = err.message;
end

distcomp.pRestoreEnvironmentAfterSubmission( storedEnv );

if FAILED
    error('distcomp:pbsscheduler:UnableToFindService', ...
          'Error executing the PBS script command ''qsub''. The reason given is \n %s', out);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the scheduler data
schedulerData = struct('type', 'pbs', ...
                       'usingJobArray', false, ...
                       'skippedTaskIDs', [], ...
                       'pbsJobIds', { pbsJobID }, ...
                       'absLogLocation', absLog, ...
                       'relLogLocation', relLog );

job.pSetJobSchedulerData(schedulerData);
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Now set the job actually running 
helper.releaseHold( pbsJobID );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

