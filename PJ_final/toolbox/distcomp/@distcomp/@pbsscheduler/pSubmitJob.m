function pSubmitJob( pbs, job )
; %#ok Undocumented

%pSubmitJob - submit a job to PBS. Handles all variants of jobarray/multiple
%jobs and shared/nonshared filesystems

% Copyright 2007-2011 The MathWorks, Inc.

import parallel.internal.apishared.PbsHelper

tasks = job.Tasks;

% Check properties
pbs.pPreSubmissionChecks( job, tasks );

% Ensure that the job has been prepared
job.pPrepareJobForSubmission;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generic things to do with setting up environment variables

storage = job.pReturnStorage;
% Ask the storage object how it would like to serialize itself and be
% reconstructed at the far end
[stringLocation, stringConstructor] = storage.getSubmissionStrings;
setenv('MDCE_STORAGE_LOCATION', stringLocation);
setenv('MDCE_STORAGE_CONSTRUCTOR', stringConstructor);

% Get the location of the storage
jobLocation = job.pGetEntityLocation;
setenv('MDCE_JOB_LOCATION', jobLocation);

[~, matlabExe, matlabArgs] = pbs.pCalculateMatlabCommandForJob( job );
setenv( 'MDCE_MATLAB_EXE', matlabExe );
setenv( 'MDCE_MATLAB_ARGS', matlabArgs );

if pbs.HasSharedFilesystem
    setenv('MDCE_DECODE_FUNCTION', 'decodePbsSingleTask');
else
    setenv('MDCE_DECODE_FUNCTION', 'decodePbsSingleZippedTask');
end

% Store the scheduler type so the runprop on the far end can be correctly
% constructed. Ensure it's lower case for distcomp.getSchedulerUDDConstructor.
setenv( 'MDCE_SCHED_TYPE', lower( pbs.Type ) );

% Only use job arrays if there's more than one task. PBSPro cannot submit a
% single-element job array.
actuallyUseJobArrays = pbs.UseJobArrays && numel( tasks ) > 1;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Build the PbsHelper we're going to use henceforth
taskIDs = cell2mat( get( job.Tasks, {'ID'} ) );
helper = PbsHelper( pbs.ClusterOsType, pbs.ResourceTemplate, actuallyUseJobArrays, ...
                    pbs.UsePbsAttach, pbs.RcpCommand, storage.getStorageLocationStruct(), ...
                    job.ID, taskIDs );

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Everything to do with setting up complex replacements for the templates
% and the other command-line arguments.

headerEls = helper.calcHeaderElements();

% Define the location for logs to be returned
[logArgs, relLog, absLog] = helper.chooseIndTaskLog( job.pGetEntityLocation() );

[cmdLineDirective, directive] = helper.getDirective();
cmdLineArgs = [ pbs.SubmitArguments, ' ', logArgs, ' ', cmdLineDirective ];

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the submission script from template elements
submissionScript = helper.chooseSubmitScriptName( job.pGetEntityLocation() );

fhSubScript = fopen( submissionScript, 'wt' );
if fhSubScript == -1
    error( 'distcomp:pbsscheduler:cantwritescript', ...
           'Couldn''t write the submission script "%s"',submissionScript );
end

try
    helper.addHeaderToScript( fhSubScript, headerEls, directive );

    if ~pbs.HasSharedFilesystem
        helper.addCopyInToScript( fhSubScript );
    end
    
    helper.addExecutionToScript( fhSubScript );

    if ~pbs.HasSharedFilesystem
        helper.addCopyOutToScript( fhSubScript );
    end

    % Ensure script has a trailing newline
    fprintf( fhSubScript, '\n' );

    err = [];
catch exception
    err = exception;
end

fclose( fhSubScript );

if ~isempty( err )
    rethrow( err );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Actual submission

% Before submitting we need to ensure that certain environment variables
% are no longer set otherwise PBS will copy them across and break the
% remote MATLAB startup - these are used by MATLAB startup to pick the
% matlabroot and toolbox path and they explicitly override any local
% settings.
storedEnv = distcomp.pClearEnvironmentBeforeSubmission();

if ~pbs.HasSharedFilesystem
    storage = pbs.pReturnStorage;
    storage.serializeForSubmission( job );
end

try
    % Make the shelled out call to qsub
    [FAILED, out, jobIDs] = helper.submitIndependentJob( submissionScript, cmdLineArgs );
catch err
    FAILED = true;
    out = err.message;
end

distcomp.pRestoreEnvironmentAfterSubmission( storedEnv );

if FAILED
    error('distcomp:pbsscheduler:UnableToFindService', ...
          'Error executing the PBS script command ''qsub''. The reason given is \n %s', out);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the scheduler data
schedulerData = struct( 'type', 'pbs', ...
                        'usingJobArray', actuallyUseJobArrays, ...
                        'pbsJobIds', {jobIDs}, ...
                        'skippedTaskIDs', helper.SkippedTaskIDs, ...
                        'absLogLocation', absLog, ...
                        'relLogLocation', relLog );

job.pSetJobSchedulerData( schedulerData );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Release the hold on the jobs
helper.releaseHold( jobIDs );
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

