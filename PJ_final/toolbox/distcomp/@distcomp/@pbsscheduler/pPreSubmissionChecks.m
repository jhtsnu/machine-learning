function pPreSubmissionChecks( pbs, job, tasks )
; %#ok Undocumented
% pPreSubmissionChecks - check properties before we allow submission to
% proceed. Side-effect - calculate script extension on the cluster.

% Copyright 2007-2011 The MathWorks, Inc.

if numel( tasks ) < 1
    error('distcomp:pbsscheduler:InvalidState', ...
          'A job must have at least one task to submit to a PBS scheduler');
end

% Check for non-shared vs. parallel job
if isa( job, 'distcomp.simpleparalleljob' ) && ~pbs.HasSharedFilesystem
    error( 'distcomp:pbsscheduler:nonsharedParallel', ...
           'The PBS scheduler requires a shared filesystem for parallel jobs' );
end
