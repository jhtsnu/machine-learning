function value = pSetParallelSubmissionWrapperScript( pbs, value )
; %#ok Undocumented
%pSetParallelSubmissionWrapperScript - update the wrapper script
% and interpret special values - special values are passed directly to
% "pSetupForParallelExecution".

% Copyright 2007-2012 The MathWorks, Inc.

switch lower( value )
  case {'pc', 'pcnodelegate', 'unix'}
    % Don't set the machine type or wrapper script - just query the values that
    % would be set. We'll ignore the machineType here.
    [~, value] = pbs.pSetupForParallelExecution( value, false, false );
  otherwise
    % Do nothing - just set the value
end
