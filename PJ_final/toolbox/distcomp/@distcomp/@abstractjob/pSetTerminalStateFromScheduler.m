function state = pSetTerminalStateFromScheduler(job, state)
; %#ok Undocumented
%pSetTerminalStateFromScheduler
%
%  STATE = pSetTerminalStateFromScheduler(JOB, STATE)

%  Copyright 2010 The MathWorks, Inc.

% $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:49:33 $

% This method will only set finished or failed state on the job
if ~iIsTerminalState(state)
    return;
end

% Set job and all tasks to final state.  The job's 
% state is always set to state that we got from the scheduler.
% Each task's state is set to the state from the scheduler
% only if it wasn't already in a terminal state.  (i.e. if the 
% scheduler state is failed, but the task completed successfully 
% and is in state finished, then we won't change the task state
% to failed).
tasks = job.Tasks;
for ii = 1:numel(tasks)
    if ~iIsTerminalState(tasks(ii).State)
        tasks(ii).pSetState(state);
    end
end
job.pSetState(state);
job.pDeduceAndSetFinishTime(state);


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function isTerminal = iIsTerminalState(state)
isTerminal = any(strcmp(state, {'finished' 'failed'}));
