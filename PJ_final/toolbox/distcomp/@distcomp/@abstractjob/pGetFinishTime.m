function val = pGetFinishTime(job, val)
; %#ok Undocumented
%pGetFinishTime 
%
%  VAL = pGetFinishTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(job, 'finishtime'));
    catch %#ok<CTCH> swallow errors
    end
end
