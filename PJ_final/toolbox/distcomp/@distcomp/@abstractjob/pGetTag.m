function val = pGetTag(job, val)
; %#ok Undocumented
%pGetTag 
%
%  VAL = pGetTag(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'tag');
    catch %#ok<CTCH> swallow errors
    end
end
