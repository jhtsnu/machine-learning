function pDeduceAndSetFinishTime(job, state)
; %#ok Undocumented
%pDeduceAndSetFinishTime - deduce and set the job and task finish times if 
%                          they are not set correctly
%

%  Copyright 2010 The MathWorks, Inc.

%  $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:49:30 $

% Only set finish times if the job state is actually finished.
% Failed jobs will not have a finish time
if ~strcmp(state, 'finished')
    return;
end

% Ensure that the job finish time is set appropriately (it may not be set 
% if the task that was supposed to set it failed before it was able to set 
% the job finish time.  
%
% All the tasks are independent, so do nothing about task finish times.
if isempty(job.FinishTime)
    finishTimeFromTasks = job.pGetFinishTimeFromTasks();
    job.pSetFinishTime(finishTimeFromTasks);
end
