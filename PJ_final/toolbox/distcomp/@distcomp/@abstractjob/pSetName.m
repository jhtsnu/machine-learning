function val = pSetName(job, val)
; %#ok Undocumented
%pSetName 
%
%  VAL = pSetName(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'name', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
