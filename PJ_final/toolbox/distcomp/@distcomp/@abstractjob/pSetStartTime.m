function val = pSetStartTime(job, val)
; %#ok Undocumented
%pSetStartTime 
%
%  VAL = pSetStartTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'starttime', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
