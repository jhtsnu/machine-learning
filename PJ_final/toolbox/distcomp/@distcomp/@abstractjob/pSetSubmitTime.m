function val = pSetSubmitTime(job, val)
; %#ok Undocumented
%pSetSubmitTime 
%
%  VAL = pSetSubmitTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'submittime', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
