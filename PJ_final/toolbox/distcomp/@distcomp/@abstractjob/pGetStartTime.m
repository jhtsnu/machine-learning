function val = pGetStartTime(job, val)
; %#ok Undocumented
%pGetStartTime 
%
%  VAL = pGetStartTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(job, 'starttime'));
    catch %#ok<CTCH> swallow errors
    end
end
