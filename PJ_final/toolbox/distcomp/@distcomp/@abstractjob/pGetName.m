function val = pGetName(job, val)
; %#ok Undocumented
%pGetName 
%
%  val = pGetName(JOB, val)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(job, 'name'));
    catch %#ok<CTCH> swallow errors
    end
end
