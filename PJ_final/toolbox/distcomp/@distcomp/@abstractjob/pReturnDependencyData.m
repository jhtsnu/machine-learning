function [fileList, pathList, zipData] = pReturnDependencyData(job)
; %#ok Undocumented
%pReturnDependencyData 
%
%  [FILELIST, ZIPDATA] = pReturnDependencyData(JOB)

%  Copyright 2005-2012 The MathWorks, Inc.

%  $Revision: 1.1.10.6 $    $Date: 2012/10/24 04:12:18 $ 

serializer = job.Serializer;

if ~isempty(serializer)
    try
        values = serializer.getFields(job, {'filedependencies' 'filedata'});
        [fileList, zipData] = deal(values{:});
        % This is required for compatibility with API-2, which requires an
        % extra output argument - the list of paths to the attached files
        % relative to the zip archive root. This is irrelevant in API-1 so
        % we just duplicate the output arguments.
        pathList = fileList;
    catch err
        error('distcomp:job:CorruptData', ...
            'Unable to read the file dependency data from storage.\nNested error :%s', err.message);
    end
end
