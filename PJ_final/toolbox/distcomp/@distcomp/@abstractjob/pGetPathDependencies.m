function val = pGetPathDependencies(job, val)
; %#ok Undocumented
%pGetPathDependencies 
%
%  VAL = pGetPathDependencies(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'pathdependencies');
    catch %#ok<CTCH> swallow errors
    end
end
