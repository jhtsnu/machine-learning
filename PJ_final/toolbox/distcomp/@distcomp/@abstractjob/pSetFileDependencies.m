function val = pSetFileDependencies(job, val)
; %#ok Undocumented
%pSetFileDependencies 
%
%  VAL = pSetFileDependencies(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.

if job.IsBeingConstructed
    % Whilst the job is being constructed we need to ensure that we concatenate
    % the FileDependencies rather than overwrite the existing ones. We can safely
    % store the values in the actual field until after construction.
    
    % The post function is stored with the value to set 
    postFcn = @iPostConstructionSetFileDependencies;
    % Have we already set this post construction function once?
    [index, oldVal, oldConfig] = job.pFindPostConstructionFcn(postFcn);
    % Which configuration is being used to set us
    newConfig = job.ConfigurationCurrentlyBeingSet;
    if isempty(index)
        job.pAddPostConstructionFcn(postFcn, val, newConfig);
    else
        newConfig = distcomp.configurableobject.pGetConfigNameFromConfigPair(oldConfig, newConfig);
        job.pModifyPostConstructionFcn(index, postFcn, [oldVal ; val], newConfig);
    end
    val = {};
    return
end

serializer = job.Serializer;

import parallel.internal.apishared.AttachedFiles

if ~isempty(serializer)
    try
        % We need to resolve the file paths first as zipFiles() no longer has
        % this side effect after the implementation of auto-attached files.
        val = cellfun( @parallel.internal.apishared.FilenameUtils.safeWhich, val, ...
                'UniformOutput', false );
        [filedata, val] = AttachedFiles.zipFiles( val );
        if isempty(val)
            filedata = int8([]);
        end
        try
            serializer.putFields(job, {'filedependencies' 'filedata'}, {val filedata});
        catch err
            % Try and write sensible defaults back
            serializer.putFields(job, {'filedependencies' 'filedata'}, {{} int8([])});
            rethrow(err);
        end
    catch err
        rethrow(err);
    end
end
% Do not hold anything locally
val = {};

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iPostConstructionSetFileDependencies(job, val, config)
% Actually set the file dependencies
pSetPropertyAndConfiguration(job, 'FileDependencies', val, config);
