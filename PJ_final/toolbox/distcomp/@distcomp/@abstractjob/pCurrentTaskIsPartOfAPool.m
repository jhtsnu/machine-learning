function isPoolTask = pCurrentTaskIsPartOfAPool(~)
; %#ok Undocumented
%pCurrentTaskIsPartOfAPool 
%
%  isPoolTask = pCurrentTaskIsPartOfAPool(JOB)

%  Copyright 2008-2012 The MathWorks, Inc.

isPoolTask = false;
