function finishTime = pGetFinishTimeFromTasks(job)
; %#ok Undocumented
%pGetFinishTimeFromTasks - determine the most likely job finish time 
%                          based on the task finish times
%
%  FINISHTIME = PGETFINISHTIMEFROMTASKS(JOB)

%  Copyright 2010 The MathWorks, Inc.

%  $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:49:31 $

% To ensure format/locale consistency, first find the index
% of the task with the latest finish time and return that
% task's finish time string.

allFinishTimes = get(job.Tasks, {'FinishTime'});
% Convert the strings into times in milliseconds since 
% January 1, 1970, 00:00:00 GMT so that we can sort easily.
% Note that empty finish times are not removed from the
% cell array so that we can preserve the task index.
numericTimes = zeros(numel(allFinishTimes), 1);
simpleDateFormat = java.text.SimpleDateFormat('E MMM dd H:m:s z yyyy', java.util.Locale.US);
for ii = 1:numel(allFinishTimes)
    if ~isempty(allFinishTimes{ii})
        try
            numericTimes(ii) = simpleDateFormat.parse(allFinishTimes{ii}).getTime();
        catch  %#ok<CTCH>
            numericTimes(ii) = 0;
        end
    end
end

[~, lastTimeIndex] = max(numericTimes);

finishTime = allFinishTimes{lastTimeIndex};


