function val = pGetSubmitTime(job, val)
; %#ok Undocumented
%pGetSubmitTime 
%
%  VAL = pGetSubmitTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(job, 'submittime'));
    catch %#ok<CTCH> swallow errors
    end
end
