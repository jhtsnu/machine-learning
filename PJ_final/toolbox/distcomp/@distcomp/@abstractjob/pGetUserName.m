function val = pGetUserName(job, val)
; %#ok Undocumented
%pGetUserName 
%
%  VAL = pGetUserName(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'username');
    catch %#ok<CTCH> swallow errors
    end
end
