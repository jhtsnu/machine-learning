function val = pSetJobSchedulerData(job, val)
; %#ok Undocumented
%pSetJobSchedulerData 
%
%  VAL = pSetJobSchedulerData(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'jobschedulerdata', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = [];
