function tasks = createTask(job, varargin)
; %#ok Undocumented

%  Copyright 2005-2011 The MathWorks, Inc.

import parallel.internal.apishared.TaskCreation;

try
    [taskFcns, numArgsOut, argsIn, setArgs] = TaskCreation.createTaskArgCheck(numel(job), ...
                                                      varargin{:});
catch err
    throw(err);
end

if ~isequal( job.pGetState, 'pending' )
    error( 'parallel:job:BadStateForCreateTask', ...
           'Tasks can only be added to jobs which are in state ''pending''.' );
end

try
    tasks = job.pCreateTask(taskFcns, numArgsOut, argsIn);
catch err
    % TODO - what sort of error here?
    rethrow(err)
end

try
    % If we have any extra parameters then set them here
    if numel(setArgs) > 0
        % Catch the invalid input of a single char array as it would cause
        % set to return the possible values of the input
        if numel(setArgs) == 1 && ischar(setArgs{1})
            error('distcomp:job:InvalidArgument', '??? Invalid parameter/value pair arguments.');
        end
        set(tasks, setArgs{:});
    end
catch err
    % Invalid parameter or value
    destroy(tasks);
    rethrow(err);
end

