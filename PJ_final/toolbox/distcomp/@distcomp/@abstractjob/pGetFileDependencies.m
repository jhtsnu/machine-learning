function val = pGetFileDependencies(job, val)
; %#ok Undocumented
%pGetFileDependencies 
%
%  VAL = pGetFileDependencies(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'filedependencies');
    catch %#ok<CTCH> swallow errors
    end
end
