function val = pGetJobData(job, val)
; %#ok Undocumented
%pGetJobData 
%
%  VAL = pGetJobData(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'jobdata');
    catch %#ok<CTCH> swallow errors
    end
end
