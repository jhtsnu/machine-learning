function val = pSetTag(job, val)
; %#ok Undocumented
%pSetTag 
%
%  VAL = pSetTag(JOB, VAL)

%  Copyright 2004-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'tag', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
