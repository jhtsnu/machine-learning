function pPostJobEvaluate(job)
; %#ok Undocumented
%pPostJobEvaluate
%
%  pPostJobEvaluate(JOB)

%  Copyright 2005-2010 The MathWorks, Inc.

%  $Revision: 1.1.10.4 $    $Date: 2010/07/19 12:49:32 $

% We shall try and determine if this job should be treated as finished.

% Get the parent of this job and make sure it's an abstract scheduler
scheduler = job.Parent;
if ~isa(scheduler, 'distcomp.abstractscheduler')
    return
end
% Lets try and test to see if we are the last job to execute and set
% the job to the finished state
if scheduler.HasSharedFilesystem
    jobStateFromTasks = job.pGetStateFromTasks;
    % If all the tasks are finished then set the job to finished
    if strcmp(jobStateFromTasks, 'finished')
        dctSchedulerMessage(4, 'About to set job state to finished');
        job.Serializer.putFields(job, {'state' 'finishtime'}, ...
            {'finished' char(java.util.Date)});
    end
end
