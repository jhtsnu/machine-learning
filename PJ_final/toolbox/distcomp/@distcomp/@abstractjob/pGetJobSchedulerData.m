function val = pGetJobSchedulerData(job, val)
; %#ok Undocumented
%pGetJobSchedulerData 
%
%  VAL = pGetJobSchedulerData(JOB, VAL)

%  Copyright 2005-2010 The MathWorks, Inc.

%  $Revision: 1.1.10.4 $    $Date: 2010/08/02 23:07:25 $ 

serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(job, 'jobschedulerdata');
    catch err %#ok<NASGU>
        val = [];
    end
end