function val = pSetState(job, val)
; %#ok Undocumented
%pSetState 
%
%  VAL = pSetState(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'state', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = 'unavailable';
