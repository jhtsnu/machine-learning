function serializer = pReturnSerializer(job)
; %#ok Undocumented
%pReturnSerializer 
%
%  SERIALIZER = PRETURNSERIALIZER(JOB)

% Copyright 2005-2012 The MathWorks, Inc.

serializer = job.Serializer;
