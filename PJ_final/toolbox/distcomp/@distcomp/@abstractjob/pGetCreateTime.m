function val = pGetCreateTime(job, val)
; %#ok Undocumented
%pGetCreateTime 
%
%  VAL = pGetCreateTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(job, 'createtime'));
    catch %#ok<CTCH> swallow errors
    end
end
