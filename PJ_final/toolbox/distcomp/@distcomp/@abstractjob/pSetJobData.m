function val = pSetJobData(job, val)
; %#ok Undocumented
%pSetJobData 
%
%  VAL = pSetJobData(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'jobdata', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = [];
