function val = pSetFinishTime(job, val)
; %#ok Undocumented
%pSetFinishTime 
%
%  VAL = pSetFinishTime(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = job.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(job, 'finishtime', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
