function obj = runprop
; %#ok Undocumented
%RUNPROP concrete constructor for this class
%
%  OBJ = RUNPROP

%  Copyright 2000-2012 The MathWorks, Inc.



obj = distcomp.runprop;
obj.DependencyDirectory = [tempname, 'rp', num2str(system_dependent('getpid'))];
obj.HasSharedFilesystem = true;
obj.AppendPathDependencies = true;
obj.AppendFileDependencies = true;
obj.IsFirstTask = true;
obj.LocalSchedulerName = 'runner';
obj.FallbackSchedulerName = '';
obj.DecodeArguments = {};
obj.ExitOnTaskFinish = false;
obj.CleanUpDependencyDirOnTaskFinish = false;
obj.TaskEvaluator = parallel.internal.evaluator.EvalcEvaluator( false );
