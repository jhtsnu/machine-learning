function schema
%SCHEMA defines the distcomp.runnerdefaults class
%

%   Copyright 2005-2012 The MathWorks, Inc.


hThisPackage = findpackage('distcomp');
hThisClass   = schema.class(hThisPackage, 'runprop');
% Make this a pass-by-value object
%set(hThisClass, 'Handle', 'off')

% Define the fields that are needed to run a task - most of these fields
% need to be setup before we can instantiate a real task runner
schema.prop(hThisClass, 'StorageConstructor', 'string');

schema.prop(hThisClass, 'StorageLocation', 'string');

schema.prop(hThisClass, 'JobLocation', 'string');

schema.prop(hThisClass, 'TaskLocation', 'string');

schema.prop(hThisClass, 'DependencyDirectory', 'string');

schema.prop(hThisClass, 'HasSharedFilesystem', 'bool');

schema.prop(hThisClass, 'AppendPathDependencies', 'bool');

schema.prop(hThisClass, 'AppendFileDependencies', 'bool');

schema.prop(hThisClass, 'IsFirstTask', 'bool');

schema.prop(hThisClass, 'LocalSchedulerName', 'string');

schema.prop(hThisClass, 'FallbackSchedulerName', 'string');

schema.prop(hThisClass, 'DecodeArguments', 'MATLAB array');

schema.prop(hThisClass, 'ExitOnTaskFinish', 'bool');

schema.prop(hThisClass, 'CleanUpDependencyDirOnTaskFinish', 'bool');

schema.prop(hThisClass, 'TaskEvaluator', 'MATLAB array');
