function val = pGetErrorMessage(task, val)
; %#ok Undocumented
%pGetErrorMessage 
%
%  VAL = pGetErrorMessage(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(task, 'errormessage');
    catch %#ok<CTCH> swallow errors
    end
end
