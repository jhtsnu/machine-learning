function val = pSetFinishTime(task, val)
; %#ok Undocumented
%pSetFinishTime 
%
%  VAL = pSetFinishTime(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'finishtime', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
