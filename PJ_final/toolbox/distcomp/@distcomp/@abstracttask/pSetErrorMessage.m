function val = pSetErrorMessage(task, val)
; %#ok Undocumented
%pSetErrorMessage 
%
%  VAL = pSetErrorMessage(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'errormessage', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
