function val = pSetCommandWindowOutput(task, val)
; %#ok Undocumented
%pSetCommandWindowOutput 
%
%  VAL = pSetCommandWindowOutput(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'commandwindowoutput', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
