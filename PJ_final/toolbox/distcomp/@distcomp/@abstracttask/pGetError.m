function val = pGetError(task, val)
; %#ok Undocumented
%pGetErrorMessage 
%
%  VAL = pGetError(TASK, VAL)

%  Copyright 2006-2012 The MathWorks, Inc.

serializer = task.Serializer;

if ~isempty(serializer)
    try
        values = serializer.getFields(task, {'errorstruct', 'erroridentifier', 'errormessage'});
        val = values{1};
        errID = values{2};
        errMessage = values{3};
        % If the error itself is empty, but either of the ErrorMessage
        % or ErrorIdentifier are non-empty, then build the appropriate
        % MException from these
        if isempty(val) && ~(isempty(errID) && isempty(errMessage))
            val = MException(errID, '%s', errMessage);
        end
    catch err %#ok<NASGU>
    end
end
