function val = pSetCaptureCommandWindowOutput(task, val)
; %#ok Undocumented
%pSetCaptureCommandWindowOutput 
%
%  VAL = pSetCaptureCommandWindowOutput(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'capturecommandwindowoutput', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = false;
