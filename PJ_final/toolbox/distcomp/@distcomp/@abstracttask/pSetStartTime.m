function val = pSetStartTime(task, val)
; %#ok Undocumented
%pSetStartTime 
%
%  VAL = pSetStartTime(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'starttime', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
