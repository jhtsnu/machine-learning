function val = pGetCommandWindowOutput(task, val)
; %#ok Undocumented
%pGetCommandWindowOutput 
%
%  VAL = pGetCommandWindowOutput(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(task, 'commandwindowoutput');
    catch %#ok<CTCH> swallow errors
    end
end
