function val = pGetErrorIdentifier(task, val)
; %#ok Undocumented
%pGetErrorIdentifier 
%
%  VAL = pGetErrorIdentifier(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(task, 'erroridentifier');
    catch %#ok<CTCH> swallow errors
    end
end
