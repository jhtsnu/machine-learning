function val = pSetState(task, val)
; %#ok Undocumented
%pSetState 
%
%  VAL = pSetState(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'state', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = 'unavailable';
