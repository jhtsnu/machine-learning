function val = pSetOutputArguments(task, val)
; %#ok Undocumented
%pSetOutputArguments 
%
%  VAL = pSetOutputArguments(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    % Check we have been sent something sensible
    if ~(iscell(val) && (isvector(val) || isempty(val)))
        error('distcomp:simpletask:InvalidProperty','OutputArguments must be a vector cell array');
    end
    try
        serializer.putField(task, 'argsout', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = [];
