function val = pSetName(task, val)
; %#ok Undocumented
%pSetName 
%
%  VAL = pSetName(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'name', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
