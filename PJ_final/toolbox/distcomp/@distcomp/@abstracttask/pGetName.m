function val = pGetName(task, val)
; %#ok Undocumented
%pGetName 
%
%  val = pGetName(TASK, val)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(task, 'name'));
    catch %#ok<CTCH> swallow errors
    end
end
