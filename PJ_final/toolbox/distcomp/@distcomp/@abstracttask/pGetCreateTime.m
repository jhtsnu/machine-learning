function val = pGetCreateTime(task, val)
; %#ok Undocumented
%pGetCreateTime 
%
%  VAL = pGetCreateTime(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(task, 'createtime'));
    catch %#ok<CTCH> swallow errors
    end
end
