function val = pGetInputArguments(task, val)
; %#ok Undocumented
%pGetInputArguments 
%
%  VAL = pGetInputArguments(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(task, 'argsin');
    catch %#ok<CTCH> swallow errors
    end
end
