function val = pGetNumberOfOutputArguments(task, val)
; %#ok Undocumented
%pGetNumberOfOutputArguments 
%
%  VAL = pGetNumberOfOutputArguments(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(task, 'nargout');
    catch %#ok<CTCH> swallow errors
    end
end
