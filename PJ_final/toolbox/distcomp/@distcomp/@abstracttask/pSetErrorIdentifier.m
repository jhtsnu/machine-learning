function val = pSetErrorIdentifier(task, val)
; %#ok Undocumented
%pSetErrorIdentifier 
%
%  VAL = pSetErrorIdentifier(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'erroridentifier', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
