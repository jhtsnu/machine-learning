function val = pGetStartTime(task, val)
; %#ok Undocumented
%pGetStartTime 
%
%  VAL = pGetStartTime(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(task, 'starttime'));
    catch %#ok<CTCH> swallow errors
    end
end
