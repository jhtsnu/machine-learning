function val = pGetFunction(task, val)
; %#ok Undocumented
%pGetFunction 
%
%  VAL = pGetFunction(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(task, 'taskfunction');
    catch %#ok<CTCH> swallow errors
    end
end
