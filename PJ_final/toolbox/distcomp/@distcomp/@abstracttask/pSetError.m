function val = pSetError(task, val)
; %#ok Undocumented
%pSetErrorMessage 
%
%  VAL = pSetErrorMessage(TASK, VAL)

%  Copyright 2006-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'errorstruct', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = '';
