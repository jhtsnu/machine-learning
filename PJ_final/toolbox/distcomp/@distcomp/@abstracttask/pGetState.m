function val = pGetState(task, val)
; %#ok Undocumented
%pGetState 
%
%  VAL = pGetState(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(task, 'state'));
    catch %#ok<CTCH> swallow errors
    end
end
