function val = pSetNumberOfOutputArguments(task, val)
; %#ok Undocumented
%pSetNumberOfOutputArguments 
%
%  VAL = pSetNumberOfOutputArguments(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'nargout', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = 0;
