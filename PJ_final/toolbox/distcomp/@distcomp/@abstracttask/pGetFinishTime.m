function val = pGetFinishTime(task, val)
; %#ok Undocumented
%pGetFinishTime 
%
%  VAL = pGetFinishTime(TASK, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = char(serializer.getField(task, 'finishtime'));
    catch %#ok<CTCH> swallow errors
    end
end
