function pPreTaskEvaluate(task)
; %#ok Undocumented
%pPreTaskEvaluate 
%
%  pPreTaskEvaluate(TASK)

%  Copyright 2005-2012 The MathWorks, Inc.


try
    set(task, ...
        'StartTime', char(java.util.Date), ...
        'State', 'running');
catch %#ok<CTCH> swallow errors
end
