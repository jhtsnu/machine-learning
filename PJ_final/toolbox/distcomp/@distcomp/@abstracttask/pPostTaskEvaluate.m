function pPostTaskEvaluate(task, output, m_exception, cmd_window_output)        
; %#ok Undocumented
%pPostTaskEvaluate 
%
%  pPostTaskEvaluate(TASK, OUTPUT, ERR_MSG, ERR_ID, CMD_WINDOW_OUTPUT)

%  Copyright 2005-2011 The MathWorks, Inc.

if isempty(m_exception)
    errID = '';
    errMessage = '';
else
    errID = m_exception.identifier;
    errMessage = m_exception.message;
end
try
    set(task, ...
        'OutputArguments', output, ...
        'Error', m_exception, ...
        'ErrorMessage', errMessage, ...
        'ErrorIdentifier', errID, ...
        'CommandWindowOutput', cmd_window_output, ...
        'State', 'finished', ...
        'FinishTime', char(java.util.Date));
catch err
    % Try and write an appropriate error message if possible
    if ishandle(task)
        try
            set(task, ...
                'Error', err,...
                'ErrorMessage', err.message, ...
                'ErrorIdentifier', err.identifier);
        catch e  %#ok<NASGU>
        end
    end
end
