function val = pSetInputArguments(task, val)
; %#ok Undocumented
%pSetInputArguments 
%
%  VAL = pSetInputArguments(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    % Check we have been sent something sensible
    if ~(iscell(val) && (isvector(val) || isempty(val)))
        error('distcomp:simpletask:InvalidProperty','InputArguments must be a vector cell array');
    end
    try
        serializer.putField(task, 'argsin', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = [];
