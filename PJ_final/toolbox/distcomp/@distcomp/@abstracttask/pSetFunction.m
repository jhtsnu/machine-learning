function val = pSetFunction(task, val)
; %#ok Undocumented
%pSetFunction 
%
%  VAL = pSetFunction(JOB, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        serializer.putField(task, 'taskfunction', val);
    catch %#ok<CTCH> swallow errors
    end
end
% Store nothing
val = [];
