function val = pGetCaptureCommandWindowOutput(task, val)
; %#ok Undocumented
%pGetCaptureCommandWindowOutput 
%
%  VAL = pGetCaptureCommandWindowOutput(TASK, VAL)

%  Copyright 2005-2012 The MathWorks, Inc.


serializer = task.Serializer;

if ~isempty(serializer)
    try
        val = serializer.getField(task, 'capturecommandwindowoutput');
    catch %#ok<CTCH> swallow errors
    end
end
