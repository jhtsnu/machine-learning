function fileName = pGetFileForStreaming(storage, entityLocation, entityType, item)
; %#ok Undocumented
%pGetFileForStreaming - get a file to stream to for a property of a job or task.
%
%    pGetFileForStreaming(STORAGE, ENTITY, ITEM) 

%    Copyright 2012 The MathWorks, Inc.

% Get the entity location and the storage location to prepend.
storageLocation = storage.StorageLocation;

extensionForItem = storage.pGetExtensionsForFields( entityType, item );

fileName = [ fullfile( storageLocation, entityLocation) extensionForItem ];
