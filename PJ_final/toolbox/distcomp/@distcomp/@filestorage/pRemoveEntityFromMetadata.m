function pRemoveEntityFromMetadata(storage, parent, ~)
; %#ok Undocumented
%pRemoveEntityFromMetadata 
%
% pRemoveEntityFromMetadata(fileStorage, IDs)

%  Copyright 2005-2012 The MathWorks, Inc.



metadataFilename = storage.pGetMetadataFilename(parent);
try
    % Load the metadata
    load(metadataFilename);
catch err
    error(message('parallel:cluster:FileStorageNonExistentOrCorruptMetadata'));
end    
% TODO - add more here!