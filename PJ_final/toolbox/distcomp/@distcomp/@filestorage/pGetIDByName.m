function ID = pGetIDByName(~, entityLocation)
; %#ok Undocumented
%pGetIDByName 
%
%  ID = pGetIDByName(STORAGE, LOCATION)
%
% 

%  Copyright 2005-2012 The MathWorks, Inc.



% Extract the ID from the location - it's the digits at the end of the
% Location.
strID = regexp(entityLocation, '\d*$', 'match', 'once');
ID = sscanf(strID, '%d');
if isempty(ID)
    error(message('parallel:cluster:FileStorageEntityEndInID'));
end
