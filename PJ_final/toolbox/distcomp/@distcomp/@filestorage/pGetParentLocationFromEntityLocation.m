function parentLocation = pGetParentLocationFromEntityLocation(~, entityLocation)
; %#ok Undocumented
%pGetParentLocationFromEntityLocation 
%
% parentLocation = pGetParentLocationFromEntityLocation(storage, entityLocation)

%  Copyright 2005-2012 The MathWorks, Inc.


% Find everything up to the last '/' 
parentLocation = regexp(entityLocation, '^.*/', 'match', 'once');
% Then remove the '/'
if ~isempty(parentLocation)
    parentLocation(end) = '';
end
