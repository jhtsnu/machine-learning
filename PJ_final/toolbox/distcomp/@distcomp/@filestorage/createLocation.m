function [location, ID] = createLocation(obj, parent, numberToCreate)
; %#ok Undocumented
%createLocation creates an empty location in the storage for an entity
%
%  LOCATION = CREATELOCATION(OBJ, PARENT, numberToCreate)
%
% The input parent is a string without an extension, which uniquely
% identifies the parent of the locations we are trying to create

%  Copyright 2004-2012 The MathWorks, Inc.

% This is the mechanism by which a new location becomes allocated on the disk 

% Don't bother if the storage is read only
if obj.IsReadOnly
    error(message('parallel:cluster:FileStorageNotWritableChangeDir', obj.StorageLocation));
end

% Deal with vectorized creation and output a cell array of locations
CELL_OUTPUT = true;
if nargin < 3
    numberToCreate = 1;
    CELL_OUTPUT = false;
end

storageLocation = obj.StorageLocation;
if ~isempty(parent)
    % Check to see that the requested parent actually exists - if it does
    % ensure the child container exists, if not create it, and then return.
    % The function WILL THROW AN ERROR if the parent has ceased to exist
    % (for example if it has been destroyed by a different process)
    iEnsureChildContainerExists(obj, storageLocation, parent);
    parentLocation = [storageLocation filesep parent];
else
    parentLocation = storageLocation;
end
% Check if the storage metadata file exists
if ~obj.RootMetadataFileExists
    % Create the root metadata file
    obj.pCreateMetadataFile('');
    obj.RootMetadataFileExists = true;
end
% It is likely that an object with no parent is called a Job and an object
% with a parent is called a Task.
if isempty(parent)
    type = obj.JobLocationString;
    IS_JOB = true;
else
    type = obj.TaskLocationString;
    IS_JOB = false;
end
% Get the highest numbered entity of the correct type here
lastValue = iGetLastEntityValueFromLocation(parentLocation, type);
if IS_JOB
    lastValue = max([lastValue obj.myLastJobValue]);
end
ID = zeros(1, numberToCreate);
location = cell(size(ID));
ADD_PARENT_STRING = ~isempty(parent);
% Invert the creation order to minimize the likelihood of two processes
% attempting to allocate the same job number
for i = numberToCreate:-1:1
    thisID = lastValue + i;
    % What is the name of this location
    thisLocation = sprintf([type '%d'], thisID);
    % For each defined extension we need to create a new file
    for j = 1:numel(obj.Extensions)
        % Get the full name of this file
        fullLocation = [parentLocation filesep thisLocation obj.Extensions{j}];
        try
            % Create the file and immediate close the handle
            f = fopen(fullLocation, 'w');
            % Error if we failed to open the file
            if f < 0
                error(message('parallel:cluster:FileStorageCannotCreateFile', fullLocation)); 
            end
            fclose(f);
        catch err
            rethrow(err);
        end
    end
    % Return the location string - note that parent might be empty and
    % hence you don't want the filesep in the string - unix filesep to
    % allow this name to be used on all platforms
    if ADD_PARENT_STRING
        thisLocation = [parent '/' thisLocation]; %#ok<AGROW> Incorrect analysis
    end
    location{i} = thisLocation;
    ID(i) = thisID;
end
if IS_JOB && numel(ID) > 0
    obj.myLastJobValue = ID(end);
end

if ~CELL_OUTPUT
    location = location{1};
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function iEnsureChildContainerExists(obj, storageLocation, parent)
% Next ensure that the container directory exists
if ~exist([storageLocation filesep parent], 'dir')
    % Check that the relevant parent files already exist
    for i = 1:numel(obj.Extensions)
        fullLocation = [storageLocation filesep parent obj.Extensions{i}];
        if ~exist(fullLocation, 'file')
            error(message('parallel:cluster:FileStorageInvalidJob', fullLocation));
        end
    end
    [OK, errorMessage, errorID] = mkdir(storageLocation, parent);
    OK = OK && obj.pCreateMetadataFile(parent);
    if ~OK
        error(errorID, '%s', errorMessage);
    end
end

%--------------------------------------------------------------------------
%Return the number of the highest numbered existing entity, or zero if none
%exist. Note that this is not the same check as exists in
%pGetEntityNamesFromLocation - here we are more conservative and check anything
%that looks like it might be relevant to avoid problems like g791482.
%--------------------------------------------------------------------------
function lastValue = iGetLastEntityValueFromLocation(path, type)

% List all entities that might match - include directories as well as all files.
entries = dir( fullfile( path, [type '*'] ) );
% Convert 'Job##' and 'Job##.ext' into '##'. Might still end up with some junk
% in "names", str2double will convert these to NaN.
names = regexprep( {entries.name}, ['^' type '([0-9]*)(\..*)?'], '$1' );
% Find the maximum value, 0 if nothing matched.
lastValue = max( [0, str2double( names )] );
