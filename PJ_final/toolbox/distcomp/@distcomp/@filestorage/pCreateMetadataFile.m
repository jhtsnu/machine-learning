function OK = pCreateMetadataFile(storage, parent)
; %#ok Undocumented
%pCreateMetadataFile 
%
% pCreateMetadataFile(fileStorage, parent)

%  Copyright 2005-2012 The MathWorks, Inc.


metadataFilename = storage.pGetMetadataFilename(parent);
% These fields are intended for use as follows - most jobs will be a
% DefaultJobConstructor. Those that aren't should add their constructor to
% the list of alternatives if it isn't there already and their ID to the
% list of ID's and the index of their constructor in the same index of the
% AlternativeConstructorIndex array.
structToSave = struct( ...
    'DefaultConstructor', @distcomp.simplejob, ...
    'AlternativeConstructors', {{}}, ...
    'IDsUsingAlternative', [], ...
    'AlternativeConstructorIndex', [] ...
    ); %#ok<NASGU> Used in 'save'
try
    % Try and save the metadata file
    save(metadataFilename, '-struct', 'structToSave');
catch err
    rethrow(err);
end
OK = true;
