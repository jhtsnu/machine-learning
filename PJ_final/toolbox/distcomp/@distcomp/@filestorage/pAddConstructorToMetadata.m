function pAddConstructorToMetadata(storage, parent, constructor, IDs)
; %#ok Undocumented
%pAddConstructorToMetadata 
%
% pAddConstructorToMetadata(fileStorage, constructor, IDs)

%  Copyright 2005-2012 The MathWorks, Inc.

% Do nothing with an empty constructor
if ~isa(constructor, 'function_handle')
    error(message('parallel:cluster:FileStorageNotFunctionHandle'));
end
metadataFilename = storage.pGetMetadataFilename(parent);

try
    % Load the metadata
    data = load(metadataFilename);
catch err %#ok<NASGU>
    % Default to throwing an error
    OK = false;
    % Did this fail because the metadata file simply didn't exist - perhaps
    % the user went and deleted it whilst we were still running MATLAB?
    if ~exist(metadataFilename, 'file')
        try
            % Lets try and re-create the file
            OK = storage.pCreateMetadataFile(parent);
            % If we succeed then get the data from it
            if OK
                data = load(metadataFilename);
            end
        catch err2 %#ok<NASGU>
        end
    end
    if OK
        warning(message('parallel:cluster:FileStorageMissingMetadata'));
    else
        error(message('parallel:cluster:FileStorageCorruptMetadata'));
    end
end    

% If this one is equal to the default do nothing
if isequal(constructor, data.DefaultConstructor)
    % Need to ensure that my IDs are not in the list of IDsUsingAlternatives
    [data.IDsUsingAlternative, indexToKeep] = setdiff(data.IDsUsingAlternative, IDs);
    % Return early if none found
    if numel(indexToKeep) == numel(data.AlternativeConstructorIndex)
        return
    end
    data.AlternativeConstructorIndex = data.AlternativeConstructorIndex(indexToKeep);
else
    % Not the default - check if it is in the list of alternatives
    alternativeConstructors = data.AlternativeConstructors;
    FOUND = false;
    foundAtIndex = 0;
    while ~FOUND && foundAtIndex < numel(alternativeConstructors)
        foundAtIndex = foundAtIndex + 1;
        FOUND = isequal(alternativeConstructors{foundAtIndex}, constructor);
    end
    % If it wasn't found then add it to the list of possible constructors
    if ~FOUND
        data.AlternativeConstructors{end + 1} = constructor;
        foundAtIndex = numel(data.AlternativeConstructors);
    end
    % Add the new IDs and index to the end of the existing values
    IDsUsingAlternative = [data.IDsUsingAlternative ; IDs(:)];
    AlternativeConstructorIndex = [data.AlternativeConstructorIndex ; repmat(foundAtIndex, numel(IDs), 1)];
    % We don't know if we have duplicated the IDs by adding them above so we need
    % to ensure that we remove any duplicates
    [data.IDsUsingAlternative, indexToKeep] = unique(IDsUsingAlternative, 'last');
    data.AlternativeConstructorIndex = AlternativeConstructorIndex(indexToKeep);
end

try
    % Try and save the metadata file
    save(metadataFilename, '-struct', 'data');
catch err
    rethrow(err);
end
