function val = pGetTask(job, val)
%pGetTask Return a handle to the first task

% Copyright 2007-2012 The MathWorks, Inc.


if ~isempty(job.ProxyObject)
    try
        % Get the java task from the job
        proxyTasks = job.ProxyObject.getTasks(job.UUID);
        if isempty(proxyTasks(1))
            val = [];
        else
            val = distcomp.createObjectsFromProxies(proxyTasks(1, 1), ...
                                                    @distcomp.task, job);
        end
    catch %#ok<CTCH> swallow errors
        % TODO - error thrown in here?
    end
else
    % TODO - what if ProxyObject is empty?
end
