function schema
%SCHEMA defines the distcomp.lsfscheduler class
%

%   Copyright 2005-2012 The MathWorks, Inc.


hThisPackage = findpackage('distcomp');
hParentClass = hThisPackage.findclass('abstractscheduler');
hThisClass   = schema.class(hThisPackage, 'taskrunner', hParentClass);

schema.prop(hThisClass, 'DependencyDirectory', 'string');

