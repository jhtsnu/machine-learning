function pSubmitParallelJob(scheduler, job)
; %#ok Undocumented
%pSubmitJob A short description of the function
%
%  pSubmitJob(SCHEDULER, JOB)

%  Copyright 2005-2012 The MathWorks, Inc.


try
    pSubmitJob(scheduler, job);
catch e
    throw(e);
end

