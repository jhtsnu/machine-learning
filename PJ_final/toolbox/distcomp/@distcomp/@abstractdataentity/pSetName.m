function val = pSetName(~, ~) %#ok<STOUT> method errors unconditionally
; %#ok Undocumented
%pSetName 
%
%  val = pSetName(JOB, val)

%  Copyright 2005-2012 The MathWorks, Inc.


error('distcomp:abstractdataentity:AbstractMethodCall', 'Entity sub-classes MUST override this method');
