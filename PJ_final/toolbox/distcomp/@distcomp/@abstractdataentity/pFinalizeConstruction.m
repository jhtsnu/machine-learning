function pFinalizeConstruction(obj)
; %#ok Undocumented
%pFinalizeConstruction carry out any post construction tasks

%  Copyright 2008-2010 The MathWorks, Inc.

%  $Revision: 1.1.6.2 $    $Date: 2010/07/06 16:36:55 $ 

% Make this function idempotent
if obj.IsBeingConstructed
    % Indicate that the object has now been constructed
    obj.IsBeingConstructed = false;
    
    postConstructionFcns = obj.PostConstructionFcns;
    obj.PostConstructionFcns = cell(0, 2);
    % And remember to carry out any post-construction task like attaching
    % the callback eventing correctly
    if ~isempty(postConstructionFcns)
        for j = 1:size(postConstructionFcns, 1)
            thisFcn = postConstructionFcns(j, :);
            feval(thisFcn{1}, obj, thisFcn{2}{:});
        end
    end
end