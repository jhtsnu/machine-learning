function val = pGetParent(obj, ~)
; %#ok Undocumented
%pGetParent A short description of the function
%
%  VAL = pGetParent(JOB, VAL)

%  Copyright 2000-2012 The MathWorks, Inc.

val = obj.up;
