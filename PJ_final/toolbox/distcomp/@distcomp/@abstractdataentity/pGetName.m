function val = pGetName(~, ~) %#ok<STOUT> method errors unconditionally
; %#ok Undocumented
%pGetName 
%
%  val = pGetName(JOB, val)

%  Copyright 2005-2012 The MathWorks, Inc.


error('distcomp:abstractdataentity:AbstractMethodCall', 'Entity sub-classes MUST override this method');
