function D = linspace(varargin)
%LINSPACE Build codistributed arrays of linearly equally spaced points
%   using codistributor2dbc
%   V = LINSPACE(..., CODISTR) and V = LINSPACE(..., CODISTR, ...) where CODISTR
%   is a 2DBC codistributor, are equivalent to calling CODISTRIBUTED.LINSPACE with
%   the same input arguments.
%
%   Example:
%     Create a vector of linearly equally spaced points as a codistributed array:
%     spmd
%         v = linspace(1, 1000, codistributor('2dbc'))
%     end
% 
%   See also LINSPACE, CODISTRIBUTED/LINSPACE, CODISTRIBUTOR2DBC

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:47:43 $

try
    D = codistributed.linspace(varargin{:});
catch E
    throw(E);
end

end % End of linspace.
