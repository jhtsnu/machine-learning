
function dist = hBuildFromLocalPartImpl(dist, L, buildOption)
; %#ok<NOSEM> % Undocumented
  % Implementation of hBuildFromLocalPartImpl for codistributor2dbc.

%   Copyright 2009-2010 The MathWorks, Inc.
%   $Revision: 1.1.6.5 $  $Date: 2011/04/27 17:30:20 $

if ~distributedutil.Allocator.supportsCreation(L)
    error(message('parallel:codistributors:c2dbcBuildFromLPUnsupportedClass', class( L )));
end

% The 2dbc codistributor does not support matching the local parts.
if buildOption == distributedutil.BuildOption.MatchLocalParts
    error(message('parallel:codistributors:c2dbcBuildFromLPInvalidBuildOption'));
end

if buildOption ~= distributedutil.BuildOption.CalculateSize ...
        && ~dist.isComplete()
    error(message('parallel:codistributors:c2dbcBuildFromLPIncompleteCodistributor'));
end
allowCommunication = (buildOption ~= distributedutil.BuildOption.NoCommunication);

lbgrid = dist.LabGrid;
blksize = dist.BlockSize;
siz = dist.Cached.GlobalSize;

% L can be a MATLAB array of any class, sparsity, complexity and size.
% All except complexity must match across all labs. 
if allowCommunication
    if ~isreplicated({'hBuildFromLocalPartImpl', class(L), issparse(L), ...
                      class(dist), lbgrid, blksize, siz})
        error(message('parallel:codistributors:c2dbcBuildFromLPAttributeMismatch'));
    end
end 

if buildOption == distributedutil.BuildOption.CalculateSize
    gsize = gop(@plus, size(L))./[lbgrid(2) lbgrid(1)];
    dist = dist.hGetCompleteForSize(gsize);
end

locSize = dist.hLocalSize();
if ~isequal(locSize, size(L))
    error(message('parallel:codistributors:c2dbcBuildFromLPSizeMismatch2DimSize', num2str( locSize ), num2str( size( L ) )));
end

end % End of hBuildFromLocalPartImpl.
