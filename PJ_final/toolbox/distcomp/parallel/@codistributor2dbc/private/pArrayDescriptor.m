function d = pArrayDescriptor(codistr)
% Construct the array descriptor
% Note: The block size is always square

% Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $  $Date: 2010/10/07 15:42:59 $

DTYPE = 1;                             % Block cyclic 2D: 1 for dense matrix
CTXT  = NaN;                           % BLACS context to be determined
M     = codistr.Cached.GlobalSize(1);  % No. of rows of the distributed array
N     = codistr.Cached.GlobalSize(2);  % No. of columns of the distributed array
MB    = codistr.BlockSize;             % Row block size
NB    = codistr.BlockSize;             % Column Block size
RSRC  = 0;                             % Zero-base. First process row where the first 
                                       % row of the distributed array is located
CSRC  = 0;                             % Zero-base. First process column where the first 
                                       % column of the distributed array is located
szLocal = codistr.hLocalSize();        % Leading dimension of the
LLD   = max(1, szLocal(1));            % local array, LLD >= 1.

d = [DTYPE CTXT M N MB NB RSRC CSRC LLD];
end