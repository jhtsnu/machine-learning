function szs = hLocalSize(codistr, labidx)
%hLocalSize  Return the size of the local part of the codistributed array.
%   This method can only be called on a completely specified codistributor.
%   szs = hLocalSize(codistr) Returns the size of the local part.
%
%   See also codistributor2dbc/isComplete.

%   Copyright 2009 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:30:22 $

if ~codistr.isComplete()
    error(message('parallel:codistributors:c2dbcLocalSizeNotComplete'));
end

if nargin < 2
    labidx = labindex;
end

szs = zeros(1, 2);
for dim = 1:2
    [e, f] = codistr.globalIndices(dim, labidx);
    % Calculate the sum of the lengths of all of the extents, i.e.
    % sum of (f(i) - e(i) + 1)
    szs(dim) = sum(diff([e(:)'; f(:)']) + 1);
end

% We should not remove any trailing ones in the size vector because it is only
% of length 2.
end % End of hLocalSize.
