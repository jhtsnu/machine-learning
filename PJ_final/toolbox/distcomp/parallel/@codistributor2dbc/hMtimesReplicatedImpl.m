function [LPC, codistrC] = hMtimesReplicatedImpl(codistr, codistrA, LPA, codistrB, LPB)
% hMtimesReplicatedImpl:  Implementation of mtimes for codistributor2dbc
% when one matrix is replicated

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $ $Date: 2010/10/07 15:42:58 $

if isempty(codistrB)  % A distributed, B replicated
    [LPB, codistrB] = iDistributeReplicatedMultiplier(LPB, codistrA);
else   % A replicated, B distributed
    [LPA, codistrA] = iDistributeReplicatedMultiplier(LPA, codistrB);
end

[LPC, codistrC] = codistr.hMtimesImpl(codistrA, LPA, codistrB, LPB);

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [LP, codistr] = iDistributeReplicatedMultiplier(LPR, codistrD)
% Distribute a replicated array R like the distribution of distributed
% array D
    codistr = codistrD.hGetNewForSize(size(LPR));
    [LP, codistr] = codistr.hBuildFromReplicatedImpl(0, LPR);
end