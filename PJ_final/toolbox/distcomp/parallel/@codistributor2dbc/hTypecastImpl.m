function [LP, codistr] = hTypecastImpl(codistr, LP, datatype)
; %#ok<NOSEM> % Undocumented
    
%   hTypecastImpl Implementation of typecast for codistributor2dbc.
    
%   Copyright 2012 The MathWorks, Inc.


    finalClassSize = distributedutil.Allocator.findDataTypeSizeInBytes(datatype);
    origClassSize = distributedutil.Allocator.findDataTypeSizeInBytes(class(LP));
    
    % Guarantee that each worker will create an integer number of elements of the
    % new datatype.
    [LP, codistr] = iRedistributeIfNeeded(codistr, LP, origClassSize, ...
                                          finalClassSize);
    
    LP = typecast(LP, datatype);
    labGridOrient = codistr.Orientation;
    blockSize = codistr.BlockSize;
    labGrid = codistr.LabGrid;
    gsize = codistr.Cached.GlobalSize;
    ratio = origClassSize / finalClassSize;
    
    if gsize(1) <= gsize(2)
        % The codistributed array is either a scalar, or a row vector.
        nonTrivialDim = 2;
    else
        % The codistributed array is a column vector.
        nonTrivialDim = 1;
    end
    gsize(nonTrivialDim) = gsize(nonTrivialDim) * ratio;

    % If data is distributed on more than just one worker rescale the blockSize so
    % that the amount of data (in bytes per block) on each worker after the
    % typecast is kept constant.
    if ~iIsDataJustOnOneWorker(codistr)
        blockSize = blockSize * ratio;
    end

    codistr = codistributor2dbc(labGrid, blockSize, labGridOrient, gsize);
    
    if isempty(LP)
        % Reshape the empty local part matrix to correct size for construction
        % of a new codistributed array.
        newSize = hLocalSize(codistr, labindex);
        LP = reshape(LP, newSize);
    end
end


function isDataOnOneWorker = iIsDataJustOnOneWorker(codistr)
% Determine whether the vector is distributed in such a way that data is present
% only on one worker.
    
    % Data will be present only on one worker if the processor labGrid
    % vector and gsize vectors are perpendicular to each other, i.e., if
    % the dot product of gsize - 1 and labGrid - 1 is equal to 0.

    isDataOnOneWorker = dot(codistr.Cached.GlobalSize - 1, codistr.LabGrid - 1) == 0;
end



function [LP, codistr] = iRedistributeIfNeeded(codistr, LP, ...
    origClassSize, finalClassSize)
    % Redistribute the vector if needed to ensure that the block size is a multiple
    % of the elements needed to cast to larger datatype.

    if origClassSize >= finalClassSize
        return; % We can always perform this typecast
    end
    
    blockSize = codistr.BlockSize;
    gsize = codistr.Cached.GlobalSize;
    minBlockSize = finalClassSize / origClassSize; % number of elements of original
                                                   % datatype needed to build
                                                   % one element of new datatype
    nel = prod(gsize);
    if rem(nel, minBlockSize) ~= 0
        error(message('parallel:distributed:TypecastNotEnoughInputElements'));
    end
    
    if nel > blockSize && rem(blockSize, minBlockSize) ...
            && ~iIsDataJustOnOneWorker(codistr)
        blockSize = blockSize - ...
            rem(blockSize, minBlockSize);
        if blockSize < minBlockSize
            blockSize = minBlockSize;
        end
        destCodistr = codistributor2dbc(codistr.LabGrid , blockSize, ...
                                        codistr.Orientation, gsize);
        [LP, codistr] = distributedutil.Redistributor.redistribute(...
            codistr, LP, destCodistr);
    end
end
