function D = logspace(varargin)
%LOGSPACE Build codistributed arrays of logarithmically equally spaced
%   points using codistributor2dbc
%   V = LOGSPACE(..., CODISTR) and V = LOGSPACE(..., CODISTR, ...) where CODISTR
%   is a 2DBC codistributor, are equivalent to calling CODISTRIBUTED.LOGSPACE with
%   the same input arguments.
%
%   Example:
%     Create a vector of logarithmically equally spaced points as a codistributed array:
%     spmd
%         v = logspace(1, 5, codistributor('2dbc'))
%     end
% 
%   See also LOGSPACE, CODISTRIBUTED/LOGSPACE, CODISTRIBUTOR2DBC

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:47:44 $

try
    D = codistributed.logspace(varargin{:});
catch E
    throw(E);
end

end % End of logspace.
