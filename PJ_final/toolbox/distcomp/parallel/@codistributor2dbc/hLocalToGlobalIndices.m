function globalInd = hLocalToGlobalIndices(codistr, localInd, dim, lab)
; %#ok<NOSEM> % Undocumented

%   Implementation of hLocalToGlobalIndices for codistributor2dbc.

%   Copyright 2011-2012 The MathWorks, Inc.

    if nargin < 4
        lab = labindex;
    end
    
    if ~distributedutil.CodistParser.isValidLabindex(lab)
        error(message('parallel:codistributors:InvalidLabInput'));
    end
    
    if ~isPositiveIntegerValuedNumeric(dim) || ...
            dim > length(codistr.Cached.GlobalSize)
        error(message('parallel:codistributors:InvalidDimInput'));
    end
    
    localSize = codistr.hLocalSize();
    if  ~isPositiveIntegerValuedNumeric(localInd) || ...
            localInd < 1 || localInd > localSize(dim)
        error(message('parallel:codistributors:InvalidLocalIndex'));
    end
    
    if dim == 1
        proc = codistr.pLabindexToProcessorRow(lab);
    else
        proc = codistr.pLabindexToProcessorCol(lab);
    end
    
    blockSize = codistr.BlockSize;
    % blockInDim is the block in the local part that contains lInd
    blockInDim = floor((localInd - 1)/blockSize) + 1;
    offsetInBlock = mod(localInd, blockSize);
    procsInDim = codistr.LabGrid(dim);
    % The global index is the sum of three parts
    %  (1) The number of elements in complete blocks on this dimension
    %  (2) The number of elements in blocks in earlier labs on this dim
    %  (3) The index of this element in this block on this lab
    globalInd = (blockInDim - 1)*procsInDim*blockSize + (proc - 1)*blockSize +...
        offsetInBlock;
end

