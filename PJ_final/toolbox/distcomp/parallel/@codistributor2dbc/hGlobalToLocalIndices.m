function localInd = hGlobalToLocalIndices(codistr, globalInd, dim, lab)
; %#ok<NOSEM> % Undocumented

%   Implementation of hGlobalToLocalIndices for codistributor2dbc.

%   For example, if globalInd = 230, blockSize = 64, and LabGrid(dim) = 3;
%   blockInDim = 4, procInDim = 1, and localInd = 102, which will be returned
%   if proc == 1.  If the designated lab is the second or third processor 
%   in this dimension of the LabGrid, we return -1 because this global
%   index doesn't correspond to an element on that lab.

%   Copyright 2011-2012 The MathWorks, Inc.

    if nargin < 4
        lab = labindex;
    end
    
    if ~distributedutil.CodistParser.isValidLabindex(lab)
        error(message('parallel:codistributors:InvalidLabInput'));
    end
    
    if ~isPositiveIntegerValuedNumeric(dim) || ...
            dim > length(codistr.Cached.GlobalSize)
        error(message('parallel:codistributors:InvalidDimInput'));
    end
    
    if  ~isPositiveIntegerValuedNumeric(globalInd) || ...
            any(globalInd > codistr.Cached.GlobalSize(dim)) || ...
            any(globalInd < 1)
        error(message('parallel:codistributors:InvalidGlobalIndex'));
    end
    
    if dim == 1
        proc = codistr.pLabindexToProcessorRow(lab);
    else
        proc = codistr.pLabindexToProcessorCol(lab);
    end
    
    blockSize = codistr.BlockSize;
    % blockInDim is the block in the distribution that contains globalInd
    blockInDim = floor((globalInd - 1)/blockSize) + 1;
    % procInDim is the processor in the LabGrid that contains globalInd
    procInDim = mod(blockInDim - 1, codistr.LabGrid(dim)) + 1;
    % compute the local index by adding the offset of globalInd within its block
    % to the number of smaller global indices in other blocks that are on this lab 
    localInd = floor((blockInDim-1)/codistr.LabGrid(dim))*blockSize + ...
        mod(globalInd - 1, blockSize) + 1;
    localInd(proc ~= procInDim) = -1;
end

