function tf = hCatCheck(obj, catdim, codistrs, LPs)
; %#ok<NOSEM> % Undocumented
%   Implementation of hCatCheck for codistributor2dbc.  It only handles
%   concatenation that can be performed trivially and without any 
%   communication.

%   Copyright 2009-2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:50:14 $

if catdim > 2
    tf  = false;
    return
end

% All codistributors must have the same LabGrid, BlockSize and Orientation.
% Size in catdim must be a multiple of LabGrid(dim)*BlockSize.
isReplicated = cellfun(@isempty, codistrs); 
samePropVals = @(c) isequal(c.Orientation, obj.Orientation) ...
    && isequal(c.LabGrid, obj.LabGrid) ...
    && isequal(c.BlockSize, obj.BlockSize);
tf = all(cellfun(samePropVals, codistrs(~isReplicated)));

% Verify that all labs have the same number of blocks and that the blocks are
% all of full size.
szMultiple = obj.LabGrid(catdim)*obj.BlockSize;
sizesOk = @(c) mod(c.Cached.GlobalSize(catdim), szMultiple) == 0;
tf = tf && all(cellfun(sizesOk, codistrs(~isReplicated)));

% Verify that the replicated arrays also lead to the labs having the same number
% of blocks and that the blocks are of full size.
sizesOk = @(LP) mod(size(LP, catdim), szMultiple) == 0;
tf = tf && all(cellfun(sizesOk, LPs(isReplicated)));

% Note that the preconditions of this method state that the sizes of the input
% arrays must be consistent for concatenation.  Thus, we know that the
% replicated arrays are either vectors or matrices, because they are consistent
% with the 2DBC array that obj represents.

end % End of hCatCheck.
