function isOnEdge = pIsOnLabGridEdge(codistr, dim)
%pIsOnLabGridEdge Return true iff this lab is in processor row/column 1.

%   Copyright 2009 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2011/04/27 17:30:25 $
if dim == 1
    isOnEdge = (codistr.pLabindexToProcessorRow(labindex) == 1);
elseif dim == 2
    isOnEdge = (codistr.pLabindexToProcessorCol(labindex) == 1);
else
    error(message('parallel:codistributors:c2dbcIsOnLabGridEdgeInvalidDimension'));
end
