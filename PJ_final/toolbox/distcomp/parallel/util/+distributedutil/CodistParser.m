%CodistParser Collection of utility functions to help with argument parsing in
%codistributed and codistributors.

% Copyright 2009-2012 The MathWorks, Inc.

classdef CodistParser

    methods ( Access = public, Static )
        function [argList, allowCommunication] = extractCommFlag(argList)
        %[argList, allowCommunication] = extractCommFlag(argList) Finds 
        %and removes the optional flag 'noCommunication' from the end of argList.
        %allowCommunication defaults to true.
            allowCommunication = true;
            if isempty(argList)
                return;
            end
            if iIsCommFlag(argList{end})
                argList(end) = [];
                allowCommunication = false;
            end
        end    

        function [argList, codistr] = extractCodistributor(argList)
        %[argList, allowCommunication] = extractCodistributor(argList) Finds 
        %and removes an optional codistributor from the end of argList.  
        %The codistributor defaults to codistributor().
            if isempty(argList)
                codistr = codistributor();
                return;
            end
            if isa(argList{end}, 'AbstractCodistributor')
                codistr = argList{end};
                argList(end) = [];
            else
                codistr = codistributor();
            end
            % There should not be any codistributors remaining in the argument list.
            if any(cellfun(@(x) isa(x, 'AbstractCodistributor'), argList))
                ex = MException(message(...
                    'parallel:distributed:BuildArgsBadCodistributorPosition', ...
                    'noCommunication'));
                throwAsCaller(ex);
            end
        end
        
        function verifyNotCodistWithNoComm(fcnName, argList)
        %verifyNotCodistWithNoComm(fcnName, argList)
        % Call this function if the 'noCommunication' flag has been set to
        % verify that other input arguments are not codistributed and
        % need to be gathered.  Throws an error if any of the elements
        % in the argList cell array are of class codistributed.
            if any(cellfun(@(x) isa(x, 'codistributed'), argList))
                ex = MException(message(...
                    'parallel:distributed:CodistrNotAllowed', ...
                    distributedutil.CodistParser.fcnNameToUpper(fcnName), ...
                    'noCommunication'));
                throwAsCaller(ex);
            end
        end
        
        function val = gatherIfCodistributed(val)
            if isa(val, 'codistributed')
                val = gather(val);
            end
        end

        function val = gatherElements(cellArr)
        % Gather all codistributed elements in the input cell array
            val = cellfun(@distributedutil.CodistParser.gatherIfCodistributed, ...
                          cellArr, 'UniformOutput', false);
        end

        function tf = isValidLabindex(labidx)
        % tf = isValidLabindex(labidx) returns true if and only if labidx is a valid lab
        % index.
            allowZero = false;
            tf = isscalar(labidx) ...
                 && isPositiveIntegerValuedNumeric(labidx, allowZero) ...
                 && labidx <= numlabs;
        end

        function tf = isIntegerValuedNumeric(values)
        % tf = isIntegerValuedNumeric returns true if and only if all values are integer
        % valued numerics.
            if ~isreal( values )
                tf = false;
                return;
            end            
            tf = true;
            for i = 1 : numel(values)
                value = values(i);
                tf = isnumeric(value) && isfinite(value) && round(value) == value;
                if ~tf
                    return;
                end
            end
        end
        
        function valid = isValidTypecastDataType(datatype)
            if ~ischar(datatype)
                valid = false;
                return
            end
        
            valid = any(strcmp(datatype, {'int8', 'int16', 'int32', 'int64', ...
                    'uint8', 'uint16', 'uint32', 'uint64', ...
                    'single', 'double'}));
        end
        
        function sizesvec = parseArraySizes(sizesD)
        % sizesD is given as a cell array of sizes.  
        % Output is a non-empty vector of sizes.
    
        % Error check inputs and extract a cell array sizesD of the size inputs
            sizesD = cellfun(@distributedutil.CodistParser.gatherIfCodistributed, ...
                             sizesD, 'UniformOutput', false);
            if isempty(sizesD)
                % Like most of the build functions, we create a scalar if the size is
                % omitted.  
                sizesvec = [1 1];
                return;
            elseif length(sizesD) == 1
                % row vector of sizes, including a single scalar
                if ~isvector(sizesD{1}) || size(sizesD{1},1)~=1
                    ex = MException(message(...
                        'parallel:distributed:BuildArgsRowVectorSizes'));
                    throwAsCaller(ex);
                end
                if isscalar(sizesD{1})
                    sizesD = [sizesD sizesD];
                end
            else
                % (more than 1) individual scalar sizes
                if ~all(cellfun(@isscalar,sizesD)) || ~all(cellfun(@isnumeric,sizesD))
                   ex = MException(message(...
                       'parallel:distributed:BuildArgsScalarSizes'));
                   throwAsCaller(ex);
                end
            end
            sizesvec = [sizesD{:}];
            
            if ~isPositiveIntegerValuedNumeric(sizesvec, true)
                ex = MException(message(...
                    'parallel:distributed:BuildArgsBadSizesInput'));
                throwAsCaller(ex);
            end
            
        end % End of parseArraySizes.

        function [m, n, codistr, allowCommunication] = parseCodistributorSparse(argList)
        % Parse the arguments to AbstractCodistributor.sparse. 
            [argList, allowCommunication] = distributedutil.CodistParser.extractCommFlag(argList);
            [argList, codistr] = distributedutil.CodistParser.extractCodistributor(argList);
            if ~allowCommunication
                distributedutil.CodistParser.verifyNotCodistWithNoComm('sparse', argList);
            end

            if length(argList) ~= 2
                ex = MException(message('parallel:distributed:Sparse', ...
                                        'noCommunication'));
                throwAsCaller(ex);
            end

            sizeVec = distributedutil.CodistParser.parseArraySizes(argList);
            m = sizeVec(1);
            n = sizeVec(2);

        end % End of parseCodistributorSparse.

        function verifyNonCodistributedInputs(argList)
        %pCheckNonCodistributedInputs(argList) Check input types.
        %   The input argList must be a cell array, and if any of its elements 
        %   are of an invalid type, verifyNonCodistributedInputs throws an error
        %   as the caller.  All codistributed methods that take more than one 
        %   input argument should call this function to verify that they haven't
        %   received inputs of an invalid type.
            for ii = 1:length( argList )
                if isa( argList{ii}, 'Composite' )
                    ex = MException(message(...
                        'parallel:distributed:IllegalComposite'));
                    throwAsCaller(ex);
                elseif isa( argList{ii}, 'distributed' )
                    % Cannot combine codistributed and distributed in a single operation.
                    ex = MException(message(...
                        'parallel:distributed:IllegalDistributed'));
                    throwAsCaller(ex);
                end
            end
        end % End of verifyNonCodistributedInputs.

        function verifyReplicatedInputArgs(fcnName, argList)
        % Verify that all input arguments are replicated, and throws an error
        % if they are not.  fcnName is the function name for the
        % message identifier and error message.
            if ~isreplicated([{fcnName}, argList])
                ex = MException(message('parallel:distributed:NonReplicated', ...
                                        distributedutil.CodistParser.fcnNameToUpper(fcnName)));
                throwAsCaller(ex);
            end
        end % End of verifyReplicatedInputArgs. 

        function fcnName = fcnNameToUpper(fcnName)
        % Converts a lower case function name to upper case.  Leaves a mixed
        % case function name unmodified.
            if strcmp(lower(fcnName), fcnName)
                % All in lower case, so display in upper case.
                fcnName = upper(fcnName);
            end
        end % End of fcnNameToUpper.
        
        function verifyDiagIntegerScalar(fcnName, diag)
        %verifyDiagIntegerScalar(fcnName, diag)
        % Call this function to check that specified diagonal
        % input argument is valid (integer scalar).  Called in
        % diag, tril, triu.
            if ~( isscalar(diag) && ...
                  distributedutil.CodistParser.isIntegerValuedNumeric(diag) )
                % Error message is identical to the one in base MATLAB.
                ex = MException(message(...
                    'parallel:distributed:kthDiagInputNotInteger', ...
                    distributedutil.CodistParser.fcnNameToUpper(fcnName)));
                throwAsCaller(ex);
            end
        end % End of verifyDiagIntegerScalar
        
        function tf = isa( obj, className )
        % the distributed check is useful in the test directories
            if isa(obj, 'codistributed') || isa(obj, 'distributed')
                tf = isaUnderlying(obj, className);
            else
                tf = isa(obj, className);
            end
        end
        
        function objClass = class( obj )
        % the distributed check is useful in the test directories
            if isa(obj, 'codistributed') || isa(obj, 'distributed')
                objClass = classUnderlying(obj);
            else
                objClass = class(obj);
            end
        end
    end
end


function tf = iIsCommFlag(flag)
    tf = ischar(flag) && strcmp(flag, 'noCommunication');
end
