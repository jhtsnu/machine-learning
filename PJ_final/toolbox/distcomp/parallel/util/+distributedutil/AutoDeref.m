%AUTODEREF - what a "AutoTransfer" becomes outside an SPMD block.

% Copyright 2008-2009 The MathWorks, Inc.
% $Revision: 1.1.6.3 $   $Date: 2011/04/27 17:30:30 $

classdef AutoDeref < spmdlang.AbstractRemote
    
    properties ( GetAccess = public, Hidden )
        Value
    end
    
    methods ( Access = public )
        function obj = AutoDeref( value )
            obj.Value = value;
        end
        
        % This prevents an autoderef from making it back into SPMD.
        function [a,b] = getUserDataToSPMD( obj ) %#ok
            error(message('parallel:lang:spmd:AutoDerefTransferred'));
        end
    end
end
