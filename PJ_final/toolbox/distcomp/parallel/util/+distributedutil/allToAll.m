function cellArrayOfOutputs = allToAll(cellArrayOfInputs)
; %#ok<NOSEM> % Undocumented

%   Copyright 2011 The MathWorks, Inc.
%   This function takes a cell vector of data to be exchanged with other 
%   labs and returns a cell row vector of data received from other labs
%   in an all-to-all communication

    cellArrayOfOutputs = cell(1, numlabs);
    mwTag = 31979;
    
    % cellArrayOfOutputs(i) contains the data belonging to me that have
    % been received from lab i.
    for offset = 0:numlabs-1
        labTo = mod(labindex + offset - 1, numlabs) + 1;
        labFrom = mod(labindex - offset - 1, numlabs) + 1;
        cellArrayOfOutputs(labFrom) = labSendReceive(labTo, labFrom, ...
           cellArrayOfInputs(labTo), mwTag);
    end
end

