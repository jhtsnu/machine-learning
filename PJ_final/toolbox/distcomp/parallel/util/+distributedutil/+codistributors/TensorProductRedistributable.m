% TensorProductRedistributable The minimum interface that needs to be
% implemented in order for redistribution to work.

%   Copyright 2009-2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:50:20 $
classdef TensorProductRedistributable 
    
    % Abstract methods declared by this class.
    methods(Abstract, Hidden = true)
        % Implementation of global indices that is optimized for speed, perhaps at the
        % expense of input argument checking.  All input arguments are required
        % arguments.  The output global indices are in ascending order.
        % Note that this function prototype only supports one output argument.
        gInd = hGlobalIndicesImpl(codistr, dim, lab)

        % Return the dimensions that this codistributor works with.  A tensor product
        % distribution scheme is completely specified by the global indices in
        % these dimensions.  
        dims = hGetDimensions(codistr);

        % hIsGlobalIndexOnlab Returns a logical vector of same length as index vector
        % gIndexInDim.  The logical vector is true for the values of gIndexInDim
        % such that specified lab stores some of the values which have the
        % global index gIndInDim in the dimension dim.
        tf = hIsGlobalIndexOnLab(codistr, dim, gIndexInDim, lab)
    end
end % End classdef


