% ShiftedRedistributable A TensorProductRedistributable that represents an array
% whose indices do not necessarily at (1, 1, ..., 1).
%
% Example: 
%   If org is a TensorProductRedistributable for a regular, 20-by-20 matrix, 
%   then c
%       s = ShiftedRedistributable(org, [10, 10], 1:3)
%   is a TensorProductRedistributable for a 20-by-20 matrix whose global indices
%   are [11:30, 11:30].  Its hGetDimensions method returns the union of
%   codistr.hGetDimensions and 1:3.

%   Copyright 2009-2012 The MathWorks, Inc.

classdef ShiftedRedistributable < distributedutil.codistributors.TensorProductRedistributable 
    
    properties(Access = private)
        % The shift applied of the first element in the array.  For a matrix,
        % this would be the index of the upper left corner of the
        % matrix less (1, 1).
        ElementShift

        % The TensorProductRedistributable that we wrap.
        Redist
        
        % The additional "interesting" dimensions we add on top of the ones
        % that Redist deems interesting.
        Dimensions
    end


    % Methods declared by this class.
    methods(Hidden = true)
        function obj = ShiftedRedistributable(srcRedist, elementShift, dims)
        % Construct a shifted redistributable.  The redistributable works with
        % the dimensions srcRedist.hGetDimensions() and dims.

            narginchk(3, 3);
            if ~isa(srcRedist, 'distributedutil.codistributors.TensorProductRedistributable')
                error(message('parallel:distributed:ShiftedRedistNotTPRedist'));
            end
            if ~distributedutil.CodistParser.isIntegerValuedNumeric(elementShift)
                error(message('parallel:distributed:ShiftedRedistInvalidFirst'));
            end
            if ~(distributedutil.CodistParser.isIntegerValuedNumeric(dims) ...
                && all(dims > 0))
                error(message('parallel:distributed:ShiftedRedistInvalidDims'));
            end
            obj.Redist = srcRedist;
            obj.ElementShift = elementShift;
            obj.Dimensions = dims;
        end

        function gInd = hGlobalIndicesImpl(obj, dim, lab)
            % Our global indices are those of the original redistributable
            % + ElementShift.
            gInd = obj.Redist.hGlobalIndicesImpl(dim, lab);
            if dim <= length(obj.ElementShift)
                gInd = gInd + obj.ElementShift(dim);
            end
        end

        function dims = hGetDimensions(obj)
        % The dimensions of the shifted redistributable include those of the
        % original redistributable, as well as those specified in the
        % constructor.
            dims = unique([obj.Redist.hGetDimensions(), ...
                           obj.Dimensions]);
        end

        function tf = hIsGlobalIndexOnLab(obj, dim, gIndexInDim, lab)
            % Translate the global index back into the global indices of the 
            % original array before forwarding the call.
            if dim <= length(obj.ElementShift)
                gIndexInDim = gIndexInDim - obj.ElementShift(dim);
            end
            tf = obj.Redist.hIsGlobalIndexOnLab(dim, gIndexInDim, lab);
        end
    end
end % End classdef


