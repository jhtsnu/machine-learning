% LinearIndRedistributable A TensorProductRedistributable that represents a
% column vector view of an array.
%
% Example: 
%   If org is a TensorProductRedistributable for a regular, 20-by-20 matrix, 
%   then c
%       s = LinearIndRedistributable(org, [20, 20])
%   is a TensorProductRedistributable for a 20-by-20 matrix whose global indices
%   are 1:400.  Its hGetDimensions method returns 1.

%   Copyright 2010-2012 The MathWorks, Inc.

classdef LinearIndRedistributable < distributedutil.codistributors.TensorProductRedistributable 
    
    properties(Access = private)
        % The size of the underlying array that we represent.
        Size

        % The TensorProductRedistributable that we wrap.
        Redist
    end

    methods (Access = private)
        function szInDim = pSizeInDim(obj, dim)
            if dim <= length(obj.Size)
                szInDim = obj.Size(dim);
            else
                szInDim = 1;
            end
        end
    end

    methods (Hidden = true)
        function obj = LinearIndRedistributable(srcRedist, sz)
        % Construct a linear indices redistributable.  The redistributable
        % works only with dimension 1.
            narginchk(2, 2);
            expClass = 'distributedutil.codistributors.TensorProductRedistributable';
            if ~isa(srcRedist, expClass)
                error(message('parallel:distributed:LinearIndRedistNotTPRedist'));
            end
            zerosAllowed = true;
            if ~isPositiveIntegerValuedNumeric(sz, zerosAllowed)
                error(message('parallel:distributed:LinearIndRedistInvalidSize'));
            end

            obj.Redist = srcRedist;
            obj.Size = sz;
        end

        function gInd = hGlobalIndicesImpl(obj, dim, lab)
        % Our global indices are those of the original redistributable, we
        % just re-write them to be linear indices rather than N-D
        % indices.
            if dim ~= 1
                gInd = 1;
                return;
            end
            % Return all combinations of all the global indices of the original
            % redistributable, re-written as linear indices.  That is, if
            % the original redistributable has global indices g_1 in dim 1,
            % up to g_m in dimension m, then we effectively return the
            % values sub2ind(orgArraySize, g_1(i_1), ..., g_m(i_m)) by
            % using all combinations of the valid values of i_1, ..., i_m.
            %
            % We return the global linear indices in ascending order as
            % required by the TensorProductRedistributable interface.
            maxDim = max(length(obj.Size), obj.Redist.hGetDimensions());
            gInd = obj.Redist.hGlobalIndicesImpl(maxDim, lab);
            gInd = gInd(:);
            if isempty(gInd)
                return;
            end
            for d = maxDim-1:-1:1
                % At this point, gInd contains all combinations of the global 
                % indices in dimensions maxDim, .., d, and they are folded as
                % in dimensions (d+1, ..., maxDim)
                gIndInDim = obj.Redist.hGlobalIndicesImpl(d, lab);
                szInDim = obj.pSizeInDim(d);
                % Update global indices account for dimensions down to and
                % including dimension d.
                if isempty(gIndInDim) 
                    gInd = [];
                    return;
                else
                    % Use bsxfun to simultaneously create all combinations of 
                    % the indices in gInd with the indices in gIndInDim and 
                    % also fold them in dimensions (d, ..., maxDim).  
                    % The output of bsxfun is sorted in ascending order.
                    gInd = bsxfun(@plus, szInDim*(gInd' - 1), gIndInDim(:));
                end
                gInd = gInd(:);
            end
        end

        function dims = hGetDimensions(~)
        % We only work with the first dimension.
            dims = 1;
        end

        function tf = hIsGlobalIndexOnLab(obj, dim, index, lab)
        % Convert global linear indices into regular global indices and
        % defer to the underlying redistributable.
            if dim ~= 1
                tf = (index == 1);
                return;
            end
            tf = true(size(index));
            maxDim = max(length(obj.Size), obj.Redist.hGetDimensions());
            % We only return true for a linear index gl such that when we do
            % [g_1, ..., g_m] = ind2sub(orgArraySize, gl), then 
            % obj.Redist.hIsGlobalIndexOnLab returns true for all of 
            % g_1, ..., g_m.
            for d = 1:maxDim
                szInDim = obj.pSizeInDim(d);
                % Get the indices in the current dimension.
                currInd = mod(index - 1, szInDim) + 1;
                tf = tf & obj.Redist.hIsGlobalIndexOnLab(d, currInd, lab);
                % Reduce the indices to be in terms of dimensions d+1:maxDim.
                index = floor((index- 1)/szInDim) + 1;
            end
        end
    end
end 
