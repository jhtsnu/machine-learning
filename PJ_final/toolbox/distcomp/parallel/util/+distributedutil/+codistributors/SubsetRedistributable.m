% SubsetRedistributable A TensorProductRedistributable that represents a subset
% of an array.
%
% Example: 
%   If org is a TensorProductRedistributable for a regular, 20-by-20 matrix, 
%   then c
%       s = SubsetRedistributable(org, [10, 10], [5, 5], 1:3);
%   is a TensorProductRedistributable for a 5-by-5 matrix whose global indices
%   are [1:5, 1:5] + [10, 10].  Its hGetDimensions method returns the union of
%   codistr.hGetDimensions and 1:3.

%   Copyright 2009-2012 The MathWorks, Inc.

classdef SubsetRedistributable < ...
        distributedutil.codistributors.TensorProductRedistributable 
    
    properties(Access = private)
        % The index of the first element in our subset of the array.  For a
        % matrix, this would be the index of the upper left corner of
        % the matrix.
        First

        % The TensorProductRedistributable that we wrap.
        Redist
        
        % The additional "interesting" dimensions we add on top of the ones
        % that Redist deems interesting.
        Dimensions

        % The highest index of our subset of the array.  For a matrix, this
        % would be the index of the lower right corner of the matrix.
        Last
    end

    methods (Access = private)
        function [first, last] = pGetFirstLast(obj, dim)
            if dim <= length(obj.First)
                first = obj.First(dim);
                last = obj.Last(dim);
            else
                % Once we exceed ndims, all indices must be 1.
                first = 1;
                last = 1;
            end
        end 

        function tf = pIsIndexInSubset(obj, gIndexInDim, dim)
        % Return true iff global index is in our subset.  dim must be one of the
        % dimensions that we subset on.
            [first, last] = obj.pGetFirstLast(dim);
            tf = first <= gIndexInDim & gIndexInDim <= last;
        end
    end

    % Methods declared by this class.
    methods (Hidden = true)
        function obj = SubsetRedistributable(srcRedist, elemShift, sz, dims)
        % Construct a subset redistributable.  The redistributable works with
        % the dimensions srcRedist.hGetDimensions() and dims.

            narginchk(4, 4);
            clz = 'distributedutil.codistributors.TensorProductRedistributable';
            if ~isa(srcRedist, clz)
                error(message('parallel:distributed:SubsetRedistNotTPRedist'));
            end
            if ~distributedutil.CodistParser.isIntegerValuedNumeric(elemShift)
                error(message('parallel:distributed:SubsetRedistInvalidFirst'));
            end
            if ~(distributedutil.CodistParser.isIntegerValuedNumeric(sz) ...
                && all(sz >= 0))
                error(message('parallel:distributed:SubsetRedistInvalidSize'));
            end
            if ~(distributedutil.CodistParser.isIntegerValuedNumeric(dims) ...
                && all(dims > 0))
                error(message('parallel:distributed:SubsetRedistInvalidDims'));
            end
            if length(elemShift) < length(sz)
                error(message('parallel:distributed:SubsetRedistSizeMismatch'));
            end

            obj.Redist = srcRedist;
            obj.First = elemShift + 1;
            % Pad size with 1's as necessary to match the shift.
            sz(end+1:length(elemShift)) = 1;
            obj.Last = elemShift + sz;
            obj.Dimensions = dims;
        end

        function gInd = hGlobalIndicesImpl(obj, dim, lab)
        % Our global indices are those of the original redistributable, 
        % intersected with our region.
            gInd = obj.Redist.hGlobalIndicesImpl(dim, lab);
            % Now intersect with our region.
            gInd = gInd(obj.pIsIndexInSubset(gInd, dim));
        end

        function dims = hGetDimensions(obj)
        % The dimensions of the shifted redistributable include those of the
        % original redistributable, as well as those specified in the
        % constructor.
            dims = unique([obj.Redist.hGetDimensions(), ...
                           obj.Dimensions]);
        end

        function tf = hIsGlobalIndexOnLab(obj, dim, gIndexInDim, lab)
        % Intersect global indices with our region.
            tf = obj.Redist.hIsGlobalIndexOnLab(dim, gIndexInDim, lab);
            tf = tf & obj.pIsIndexInSubset(gIndexInDim, dim);
        end

        function LP = hBuildFromReplicated(obj, X)
        % Convert a replicated array to local part corresponding to this
        % TensorProductRedistributable.

            if ndims(X) > length(obj.First)
                error(message('parallel:distributed:SubsetRedistInvalidNDims'));
            end
            szX = size(X);
            expSz = obj.Last - (obj.First - 1);
            if ~( isequal(szX, expSz(1:length(szX))) ...
                 && all(expSz(length(szX)+1:end) == 1) )
                error(message('parallel:distributed:SubsetRedistIncorrectSize'));
                
            end
            % Use this object to get the portion of the replicated array that
            % corresponds to the local part for this lab.  The global
            % indices of obj are in terms of the global indices of the
            % target location in the larger array that obj.Redist
            % represents, and we need to shift those global indices to get
            % back into indices into the replicated array X.
            % 
            % For example, assume X is of size [100, 200], and that obj
            % represents the global indices (201:300, :) of a 300-by-200 array.
            % In that case, shift is [200, 0], and the global indices returned
            % by obj in dimension 1 is in the range 201:300.  We therefore need
            % to subtract shift(dim) from the global indices of redist in
            % dimension dim to have the correct indices into X.
            dims = obj.hGetDimensions();
            % Ensure substruct includes all dimensions of X so we don't 
            % accidentally use linear indexing or fold the trailing 
            % dimensions.
            idx = substruct('()', repmat({':'}, 1, max([dims, length(szX)])));
            for i = 1:length(dims)
                first = obj.pGetFirstLast(dims(i));
                shift = first - 1;
                idx.subs{dims(i)} = obj.hGlobalIndicesImpl(dims(i), labindex) - shift;
            end
            LP = subsref(X, idx);
        end % End of iConvertReplicated.

    end
end % End classdef
