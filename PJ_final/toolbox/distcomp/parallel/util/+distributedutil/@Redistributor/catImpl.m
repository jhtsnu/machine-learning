function [LPC, cDist] = catImpl(catdim, codistrs, LPs, targetSize, sizesInCatDim, templ)
% Concatenate the specified codistributors and local parts into an array of the
% specified target size.  The vector sizesInCatDim is the global size of each of
% the input arrays in the dimension catdim.

% Copyright 2009-2010 The MathWorks, Inc.
% $Revision: 1.1.8.1 $   $Date: 2010/07/19 12:50:22 $

isReplicated = cellfun(@isempty, codistrs);
% Use the codistributed inputs to determine the target codistributor.
cDist = iGetTargetCodistributor(catdim, codistrs(~isReplicated), targetSize);

LPC = distributedutil.Allocator.create(cDist.hLocalSize(), templ);

% Redistribute the local parts in LPs into LPC.
shift = zeros(size(cDist.Cached.GlobalSize));
for i = 1:length(LPs)
    % Construct a TensorProductRedistributable that represents the area into which
    % we want to redistribute the i-th input array.  We must add catdim as one
    % of the dimensions in which global indices must be checked.
    %
    % For example, when concatenating 2D block-cyclic arrays along the 3rd
    % dimension, the result is a 3D array, but the inputs are matrices.
    % Therefore, redistributeInto needs to inspect the global indices of both
    % source and target in dimensions 1:3.
    if isempty(codistrs{i})
        sRedist = distributedutil.codistributors.SubsetRedistributable(cDist, shift, ...
                                                          size(LPs{i}), catdim);
        LPs{i} = sRedist.hBuildFromReplicated(LPs{i});
    else
        sRedist = distributedutil.codistributors.ShiftedRedistributable(codistrs{i}, ...
                                                          shift, catdim);
    end
    LPC = distributedutil.Redistributor.redistributeInto(sRedist, LPs{i}, ...
                                                      cDist, LPC);
    shift(catdim) = shift(catdim) + sizesInCatDim(i);
end

end % End of cat.

function codistr = iGetTargetCodistributor(catdim, codistrs, targetSize)
% We use the very simple logic of distributing the result in the manner
% prescribed by the first codistributor.
classes = cellfun(@class, codistrs, 'UniformOutput', false);
if strcmp(classes{1}, 'codistributor1d')
    codistr = codistributor1d(codistrs{1}.Dimension, ...
                              codistributor1d.unsetPartition, ...
                              targetSize);
elseif catdim <= 2 && all(strcmp(classes, 'codistributor2dbc'))
    codistr = codistributor2dbc(codistributor2dbc.defaultLabGrid, ...
                                    codistributor2dbc.defaultBlockSize, ...
                                    codistributor2dbc.defaultOrientation, ...
                                    targetSize);
else
    codistr = codistributor1d(catdim, codistributor1d.unsetPartition, ...
                              targetSize);
end
        
end % End of iGetTargetCodistributor.

