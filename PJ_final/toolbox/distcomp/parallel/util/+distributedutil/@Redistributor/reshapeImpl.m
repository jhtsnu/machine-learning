function [LPB, codistrB] = reshapeImpl(codistrA, LPA, sz)
% Reshape an arbitrary codistributed array to the specified size.  The target
% size must be a valid size vector.

% Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2010/09/02 13:30:38 $

% Create the components of the reshaped array.
codistrB = iGetTargetCodistributor(codistrA, sz);
LPB = distributedutil.Allocator.create(codistrB.hLocalSize(), LPA);

% We implement the reshape operation by using a redistribution from A to B,
% using linear indices to access the arrays.  
linA = distributedutil.codistributors.LinearIndRedistributable(codistrA, ...
                                                  codistrA.Cached.GlobalSize);
linB = distributedutil.codistributors.LinearIndRedistributable(codistrB,  ...
                                                  codistrB.Cached.GlobalSize);

useLinearIndices = true;
LPB = distributedutil.Redistributor.redistributeInto(linA, LPA, linB, LPB, ...
                                                  useLinearIndices);


end % End of reshape.
    
function codistr = iGetTargetCodistributor(codistr, sz)
% Use the simplest rules possible for the output of reshape: Remain in 2DBC
% whenever possible, otherwise go to 1D.
    if isa(codistr, 'codistributor2dbc') && length(sz) <= 2
        codistr = codistributor2dbc(codistr.LabGrid, codistr.BlockSize, ...
                                    codistr.Orientation, sz);
        return;
    end
    codistr = codistributor1d(codistributor1d.unsetDimension, ...
                              codistributor1d.unsetPartition, ...
                              sz);
end % End of iGetTargetCodistributor.
