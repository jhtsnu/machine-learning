function BLP = redistributeInto(Acodistr, ALP, Bcodistr, BLP, useLinInd)
% Redistribution between two arbitrary tensor product redistributables.
% Acodistr and Bcodistr must support global indices.

% Copyright 2009-2011 The MathWorks, Inc.
% $Revision: 1.1.6.3 $   $Date: 2011/05/13 17:11:24 $

if nargin < 5
    useLinInd = false;
end
% Since the inputs are tensor product redistributables, we get a complete
% understanding of them by the global indices in the dimensions dims(1), ...,
% dims(end).
if useLinInd
    % Only consider the first dimension as we are working with linear indices.
    dims = 1;
else
    % Look at the union of the dimensions of A and B.
    dims = unique([Acodistr.hGetDimensions(), Bcodistr.hGetDimensions()]);
end
ourSrcInd = cell(1, length(dims));
ourDestInd = cell(1, length(dims));
for i = 1:length(dims)
    ourSrcInd{i} = Acodistr.hGlobalIndicesImpl(dims(i), labindex);
    ourDestInd{i} = Bcodistr.hGlobalIndicesImpl(dims(i), labindex);
end

% Prepare the substructs for sending and receiving bits of the local part.
if useLinInd
    % Linear indexing must only use one index.  
    indLen = 1;
else
    % Use at least as many indices as the dimensions in A and B so that we
    % don't fold the trailing dimensions.
    indLen = max([ndims(ALP), ndims(BLP), dims]);
end
aidx = substruct('()', repmat({':'}, 1, indLen));
bidx = substruct('()', repmat({':'}, 1, indLen));

% Use the standard all-to-all communication pattern where we send to our right
% and receive from our left.
for offset = 0:(numlabs-1)
    sendToLab = mod( labindex + offset - 1, numlabs ) + 1;
    recvFromLab  = mod( labindex - offset - 1, numlabs ) + 1;
    
    % Identify the portion of ALP that sendToLab needs to get.  
    % We do this by getting intersection of the global indices that we have
    % from A with the global indices that sendToLab will store in B.  Since
    % the intersection is returned as a logical vector of same length as
    % our global indices into A, we can use the intersection to index
    % directly into ALP.
    needToSend = true;
    for i = 1:length(dims)
        ind = Bcodistr.hIsGlobalIndexOnLab(dims(i), ourSrcInd{i}, sendToLab);
        needToSend = needToSend && any(ind);
        aidx.subs{dims(i)} = ind;
    end

    if needToSend
        toSend = subsref(ALP, aidx);
    else
        toSend = [];
        sendToLab = [];
    end

    % Identify the portion of BLP we can get from recvFromLab.
    needToRecv = true;
    for i = 1:length(dims)
        ind = Acodistr.hIsGlobalIndexOnLab(dims(i), ourDestInd{i}, recvFromLab);
        needToRecv = needToRecv && any(ind);
        bidx.subs{dims(i)} = ind;
    end

    if ~needToRecv
        recvFromLab = [];
    end

    % Data exchange.
    mwTag = 31056;
    data = labSendReceive(sendToLab, recvFromLab, toSend, mwTag);

    % If data is empty, there is nothing for us to do.  Guard against it
    % because subsasgn doesn't allow a null assignment with more than one
    % non-colon index.
    if needToRecv && ~isempty(data)
        BLP = subsasgn(BLP, bidx, data);
    end
end 

end % End of redistributeInto.
