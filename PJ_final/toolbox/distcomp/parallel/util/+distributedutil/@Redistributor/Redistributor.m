%Redistributor Collection of undocumented utility functions that redistribute codistributed arrays.

% Copyright 2009-2010 The MathWorks, Inc.
% $Revision: 1.1.6.5 $   $Date: 2010/09/02 13:30:37 $
classdef Redistributor

    methods (Access = private, Static)

        function tf = pIs1DOnOneLab(codistr)
        %pIs1DHighDim Returns true iff input codistributor is codistributor1d with the
        %entire array on a single lab.
            tf = isa(codistr, 'codistributor1d') && nnz(codistr.Partition) == 1;
        end % End of pIs1DHighDim.
    end

    methods (Access = public, Static)
        % The entry point that handles all possible to- and from- distribution 
        % schemes.
        [LPB, codistrB] = redistribute(codistr, LP, destCodistr)

        % Redistribution into a pre-existing array.
        BLP = redistributeInto(Acodistr, ALP, Bcodistr, BLP, useLinearInd)

        % Cat using redistribution.
        [LP, codistr] = catImpl(catdim, codistrs, LPs, targetSize, sizesInDim, templ);

        % Reshape using redistribution.
        [LP, codistr] = reshapeImpl(codistr, LP, targetSize);
    end
        
end
