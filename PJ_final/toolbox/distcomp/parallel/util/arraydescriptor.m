function d = arraydescriptor(A)
%ARRAYDESCRIPTOR                 Private utility function for parallel

% Copyright 2006-2012 The MathWorks, Inc.

narginchk(1, 1);

if ~isa(A, 'codistributed')
    error(message('parallel:distributed:ArrayDescriptorInvalidInput'));
end
   
distA = getCodistributor(A);

if ~isa(distA, 'codistributor2dbc')
    error(message('parallel:distributed:ArrayDescriptorInvalidInputCodist'));
end

% Construct the array descriptor
% Note: The block size is always square
DTYPE = 1;               % Block cyclic 2D: 1 for dense matrix
CTXT  = NaN;             % BLACS context to be determined
M     = size(A, 1);      % No. of rows of the distributed array
N     = size(A, 2);      % No. of columns of the distributed array
MB    = distA.BlockSize; % Row block size
NB    = distA.BlockSize; % Column Block size
RSRC  = 0;               % Zero-base. First process row where the first 
                         % row of the distributed array is located
CSRC  = 0;               % Zero-base. First process column where the first 
                         % column of the distributed array is located
LLD   = max( 1, size(getLocalPart(A), 1) );  % Leading dimension of the 
                                             % local array, LLD >= 1.

d = [DTYPE CTXT M N MB NB RSRC CSRC LLD];
