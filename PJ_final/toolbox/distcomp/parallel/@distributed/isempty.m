function tf = isempty( obj )
%ISEMPTY True for empty distributed array
%   TF = ISEMPTY(D)
%   
%   Example:
%       N = 1000;
%       D = distributed.zeros(N,0,N);
%       t = isempty(D)
%   
%   returns t = true.
%   
%   See also ISEMPTY, DISTRIBUTED, DISTRIBUTED/ZEROS.


%   Copyright 2006-2011 The MathWorks, Inc.

tf = prod( obj.Size ) == 0;
end
