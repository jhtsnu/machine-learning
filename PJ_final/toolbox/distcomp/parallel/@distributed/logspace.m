function D = logspace( varargin )
%DISTRIBUTED.LOGSPACE Logspace distributed array
%   DISTRIBUTED.LOGSPACE(X1, X2) generates a row vector of 50 logarithmically
%   equally spaced points between decades 10^X1 and 10^X2.  If X2
%   is pi, then the points are between 10^X1 and pi.
%   
%   DISTRIBUTED.LOGSPACE(X1, X2, N) generates N points.
%   For N < 2, LOGSPACE returns 10^X2.
%   
%   X1 and X2 must be a single or double scalar. Both real and complex types are supported.
%   
%   Examples:
%       N  = 1000;
%       a = 0;
%       b = 10;
%       D = distributed.logspace(a, b, N); 
%   
%   See also LOGSPACE, DISTRIBUTED, DISTRIBUTED/LINSPACE.
%   


%   Copyright 2010-2012 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/03/14 02:49:22 $

% static method of distributed

D = distributed.sBuild( @codistributed.logspace, 'logspace', varargin{:} );
end
