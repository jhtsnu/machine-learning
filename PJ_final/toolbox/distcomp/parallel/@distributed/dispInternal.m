function dh = dispInternal( obj, className, objName )
;%#ok undocumented

% Return a distributedutil.DisplayHelper object

% Copyright 2008-2012 The MathWorks, Inc.

% Deal with closed pool / invalid object up front, with early return.
if ~obj.isValid()
    
    if ~obj.isResourceSetOpen()
        % The pool that this referred to has been closed
        msg = getString(message('parallel:array:DisplayInvalidDueToClosedPool', className));
    else
        % No clear reason - could have been load/save
        msg = getString(message('parallel:array:DisplayInvalid', className));
    end
    
    dh = parallel.internal.shared.DisplayHelperInvalid( objName, className, msg );
    return
end

if numel( obj ) == 0
    % Send the gathered empty data through to the helper so that it has the
    % correct underlying class (i.e. int8/single/whatever)
    if issparse( obj )
        dh = parallel.internal.shared.DisplayHelperSparse( objName, className, gather( obj ), nnz( obj ) );
    else
        dh = parallel.internal.shared.DisplayHelperDense( objName, className, gather( obj ), size( obj ) );
    end
else
    N = 1000;
    dh = iBuildDisplayHelper( obj, className, objName, N );
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function dh = iBuildDisplayHelper( x, className, name, N )

% One day, we'll use isnumeric here.
if iIsNumericIsh( x )
    dh = iFirstNNumericDisplayHelper( x, className, name, N );
else
    dh = parallel.internal.shared.DisplayHelperOther( name, className, ...
                                             classUnderlying( x ), size( x ) );
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tf = iIsNumericIsh( x )
numericIshClasses = { 'double', 'single', ...
                    'int8', 'uint8', 'int16', 'uint16', 'int32', 'uint32', ...
                    'int64', 'uint64', ...
                    'logical' };
tf = ismember( classUnderlying( x ), numericIshClasses );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For a numeric style distributed array, display the first N entries.
function dh = iFirstNNumericDisplayHelper( x, className, name, N )

if issparse( x )
    totalEls = nnz( x );
    if totalEls > N
        maybeTruncatedValue = iSparseNTruncate( x, N );
    else
        maybeTruncatedValue = gather( x );
    end
    dh = parallel.internal.shared.DisplayHelperSparse( name, className, ...
                                              maybeTruncatedValue, totalEls );
else
    totalEls = numel( x ); 
    if totalEls > N
        rangeStruct = parallel.internal.shared.denseDisplayRangeHelper( x, N );
        maybeTruncatedValue = transferPortion( x, rangeStruct );
    else
        maybeTruncatedValue = gather( x );
    end
    % Using the 4-arg ctor since we always truncate from 1:N in each dim
    dh = parallel.internal.shared.DisplayHelperDense( name, className, maybeTruncatedValue, ...
                                             size( x ) );
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% display the first N entries of a sparse distributed
function truncated = iSparseNTruncate( x, N )
% Sparse truncation - simply pick the first N elements
truncated = spmd_feval_fcn( @iSparseNTruncateI, {x, N} );
truncated = truncated.Value;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Run in parallel on a sparse codistributed - pick the first N using "find 'first'",
% let's hope that's a good choice for codistributed.
function truncated = iSparseNTruncateI( x, N )

% Find the linear indices of the first N
linInd = gather( find( x, N, 'first' ) );

% Gather to lab 1 that portion of x
tmp = gather( x( linInd ), 1 ); 

% Collectively call "size"
[m, n] = size( x );
if labindex == 1
    % Construct "to_disp" only on lab 1, that's all the AutoTransfer needs
    truncated = spalloc( m, n, N );
    truncated( linInd ) = tmp;
else
    truncated = [];
end

% truncated has the correct value only on lab 1
truncated = distributedutil.AutoTransfer( truncated, 1 );
end
