function D = linspace( varargin )
%DISTRIBUTED.LINSPACE Linspace distributed array
%   DISTRIBUTED.LINSPACE(X1, X2) generates a row vector of 100 linearly
%   equally spaced points between X1 and X2.
%   
%   DISTRIBUTED.LINSPACE(X1, X2, N) generates N points between X1 and X2.
%   For N < 2, LINSPACE returns X2.
%    
%   X1 and X2 must be a single or double scalar. Both real and complex types are supported.
%   
%   Examples:
%       N  = 1000;
%       a = 0;
%       b = 10;
%       D = distributed.linspace(a, b, N); 
%   
%   See also LINSPACE, DISTRIBUTED, DISTRIBUTED/LOGSPACE, DISTRIBUTED/COLON.
%   


%   Copyright 2010-2012 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/03/14 02:49:21 $

% static method of distributed

D = distributed.sBuild( @codistributed.linspace, 'linspace', varargin{:} );
end
