function [LP, codistr] = hLogspaceImpl(codistr, a, b, n)
%hLogspaceImpl  Implementation for TensorProductCodistributor.

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:47:37 $

n = floor(n);  % in case n was nonintegral
n1 = n - 1;
codistr = codistr.hGetCompleteForSize([1, n]);
[LP, codistr] = codistr.hColonImpl(0, 1, n1);

if b == pi || b == single(pi) % Special case for pi
    b = log10(b);
end

LP = (10).^(a + ((b - a)/n1).*LP);

if ~isempty(LP)
    isGlobalRowEndpoint = codistr.hGlobalIndicesImpl(1, labindex) == 1;
    isGlobalColEndpoint = codistr.hGlobalIndicesImpl(2, labindex) == n;

    LP(isGlobalRowEndpoint, isGlobalColEndpoint) = 10^b;
end