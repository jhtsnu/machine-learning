% TensorProductCodistributor Base class for both codistributor1d and
% codistributor2dbc (as well as any future codistributors that store 
% data on the labs in the same fashion).  It should contain all the 
% tensor product distribution-specific methods that codistributed 
% needs access to in order for all of its methods to work with a 
% distribution scheme of this type.

%   Copyright 2009-2012 The MathWorks, Inc.

classdef TensorProductCodistributor < AbstractCodistributor & distributedutil.codistributors.TensorProductRedistributable
    
    methods(Access = private)
        [LP, codistr] = pGenericTriLowerUpperImpl(codistr, LP, k, compareFcn)
        outCellLPs = pCellfunArrayfunImpl(codistr, arrayfunOrCellfun, ...
                                          fcn, inCellLPs, ...
                                          trailingArgs, ...
                                          funNargout) 
    end
    
    methods(Hidden = true)
        function outCellLPs = hArrayfunImpl(codistr, fcn, inCellLPs, ...
                                            trailingArgs, arrayfunNargout) 
            outCellLPs = pCellfunArrayfunImpl(codistr, @arrayfun, fcn, ...
                                              inCellLPs, trailingArgs, ...
                                              arrayfunNargout);
        end % End hArrayfunImpl
        [LP, codistr] = hBuildFromFcnImpl(codistr, fun, sizesDvec, className)
        function outCellLPs = hCellfunImpl(codistr, fcn, inCellLPs, ...
                                           trailingArgs, cellfunNargout)
            outCellLPs = pCellfunArrayfunImpl(codistr, @cellfun, fcn, ...
                                              inCellLPs, trailingArgs, ...
                                              cellfunNargout);
        end % End hCellfunImpl
        
        function LP = hCell2StructBuildLPImpl(codistr, LP, fields, dim)
        % Helper function used to create appropriate LP for cell2struct for
        % the TensorProductCodistributor.
            if ~isempty(LP)
                LP = cell2struct(LP, fields, dim);
            else
                % This code is taken from Allocator.
                % Build an empty structure using fields as template.
                structTemplate = [fields; cell( size( fields ) )];
                LP = struct(structTemplate{:});
                LP = repmat(LP, codistr.hLocalSize(labindex));
            end
        end

        
        function clz = hClassUnderlyingImpl(codistr, LP) %#ok<INUSL>
            clz = class(LP);
        end % End hClassUnderlyingImpl
        [LP, codistr] = hElementwiseBinaryOpImpl(codistr, fcn, codistrA, LPA, codistrB, LPB)
        function [LP, codistr] = hElementwiseUnaryOpImpl(codistr, fcn,  LP)
            LP = fcn(LP);
        end % End hElementwiseUnaryOpImpl
        [LP, codistr] = hEyeImpl(codistr, m, n, className)
        function names = hFieldnamesImpl(codistr, LP, optArg) %#ok<INUSL>
            if ~isempty(optArg)
                names = fieldnames(LP, optArg);
            else 
                names = fieldnames(LP);
            end
        end % End hFieldnamesImpl

        function froNorm = hFrobeniusNormImpl(codistr, LP)  %#ok<INUSL>
        % The temporary variable froNormLP is required because 
        % codistributor1d with distribution dimension > 2 create 3D
        % arrays that norm balks at.
            if isempty(LP)
                froNormLP = zeros(1, 1, class(LP));	 
            else	       
                froNormLP = norm(LP, 'fro');
            end	   
            froNorm = gop( @hypot, froNormLP );
        end % End hFrobeniusNormImpl
        
        function tf = hIsaUnderlyingImpl(codistr, LP, clz) %#ok<INUSL>
            tf = isa(LP, clz);
        end % End hIsaUnderlyingImpl
        
        function tf = hIscellstrImpl(codistr, LP) %#ok<INUSL>
            tf = gop(@and, iscellstr(LP));
        end % End hIscellstrImpl
        
        function tf = hIsequaltemplateImpl(codistr, F, inCellLPs) %#ok<INUSL>
            % inCellLPs contains the LP of all arrays we are testing
            % equality of.  
            tf = gop(@and, F(inCellLPs{:}));
        end % End hIsequaltemplateImpl
        
        function tf = hIsrealImpl(codistr, LP) %#ok<INUSL>
            tf = gop(@and, isreal(LP));
        end % End hIsrealImpl     
        
        function tf = hIssparseImpl(codistr, LP) %#ok<INUSL>
            tf = issparse(LP);
        end % End hIssparseImpl
        
        matrixType = hIsTriangularImpl(codistr, LP)
         
        [LP, codistr] = hLinspaceImpl(codistr, a, b, n)
        
        function exponentLP = hLog2Impl(codistr, LP, log2Nargout) %#ok<INUSL>
            exponentLP = cell(1, log2Nargout);
            [exponentLP{:}] = log2(LP);
        end % End hLog2Impl
        
        [LP, codistr] = hLogspaceImpl(codistr, a, b, n)
        
        [codistr, cellOfLPs] = hNDGridImpl(codistr, cellOfReplicated, numOutputArrays)
        
        function [LP, codistr] = hNum2CellNoDimImpl(codistr, LP)
            LP = num2cell(LP);
        end % End of hNum2CellNoDimImpl.

        function num = hNnzImpl(codistr, LP) %#ok<INUSL>
            num = gplus(nnz(LP));
        end % End hNnzImpl     
        
        function amt = hNzmaxImpl(codistr, LP) %#ok<INUSL>
            amt = gplus(nzmax(LP));
        end % End hNzmaxImpl
        
        [LP, codistr] = hSparseImpl(codistr, i, j, vals, m, n, nzmax)
        
        [LP, codistr] = hSpeyeImpl(codistr, m, n)
        
        tf = hSupportsDimensionality(codistr, dims)
                
        % hTransposeTemplateImpl Return the (c)transpose of the local part 
        % of the codistributed array based on the value of transposeFcn
        function [LP, codistr] = hTransposeTemplateImpl(codistr, LP, transposeFcn)
            codistr = codistr.pTransposeCodistributor();
            if ~isempty(LP)
                LP = transposeFcn(LP);
            else
                LP = distributedutil.Allocator.create(codistr.hLocalSize(), LP);
            end
        end % End hTransposeTemplateImpl
       
        [LP, codistr] = hTrilImpl(codistr, LP, k)
        [LP, codistr] = hTriuImpl(codistr, LP, k)
    end % End of hidden methods  
    
    % Abstract methods declared by this class.
    methods(Abstract, Hidden = true)
        % Implementation of global indices that is optimized for speed, perhaps at the
        % expense of input argument checking.  All input arguments are required
        % arguments.  This method can only be called on a completely specified
        % codistributor.
        varargout = hGlobalIndicesImpl(codistr, dim, lab)

        % Return the dimensions that this codistributor works with.  A tensor product
        % distribution scheme is completely specified by the global indices in
        % these dimensions.  The codistributor must be complete for calling this
        % method.
        dims = hGetDimensions(codistr);

        % hIsGlobalIndexOnlab Returns a logical vector of same length as index vector
        % gIndexInDim.  The logical vector is true for the values of gIndexInDim
        % such that specified lab stores some of the values which have the
        % global index gIndInDim in the dimension dim.
        tf = hIsGlobalIndexOnLab(codistr, dim, gIndexInDim, lab)

        % hLocalSize Return the size of the local part of the codistributed 
        % array.
        % This method can only be called on a completely specified 
        % codistributor.  The labindex is an optional input argument.
        szs = hLocalSize(codistr, labidx)
    end
    
    % Abstract, protected methods
    methods(Abstract, Access = protected)
        % pTransposeCodistributor() given the codistributor for matrix A, this 
        % function returns the codistributor for either A' or A.'. The 
        % implementation is left to the specific codistributors.
        codistr = pTransposeCodistributor(codistr);
    end
    
    methods 
        function D = Inf(varargin)
            try
                D = codistributed.Inf(varargin{:});
            catch E
                throw(E);
            end
        end % End of Inf.
        function D = NaN(varargin)
            try
                D = codistributed.NaN(varargin{:});
            catch E
                throw(E);
            end
        end % End of NaN.  
    end
    
end % End classdef


