function [header, matrix] = hDispImpl(codistr, LP, varName, ~)
% Undocumented
% hDispImpl Implementation for TensorProductCodistributor.

%   Copyright 2009-2012 The MathWorks, Inc.

if isempty(LP)
    header = sprintf('%s', getString(...
        message('parallel:codistributors:DispNoElements', varName)));
    matrix = '';
else 
    % Get the indexing expression that represents this worker's local part.  
    indexExpr = iGetIndexExprForDisplay(codistr);
    if isempty(indexExpr)
        header = sprintf('%s', getString(...
            message('parallel:codistributors:DispAllElements', varName)));
    else
        header = sprintf('%s', getString(...
            message('parallel:codistributors:DispSomeElements', ...
                    varName, indexExpr)));
    end
    dispFcn = @() disp(struct('LocalPart', {LP}, 'Codistributor', {codistr})); %#ok<NASGU>
    matrix = evalc('dispFcn()');
end
end

%-------------------------

function indxExprStr = iGetIndexExprForDisplay(codistr)
    if numlabs == 1
        indxExprStr = ''; % Entire array resides on this worker, so 
                          % index not needed.
        return;
    end

    ndims = length(codistr.Cached.GlobalSize);
    idx = cell(1, ndims);
    
    for dim = 1:ndims
        idx{dim} = iGetIndexExprInDim(codistr, dim);
    end
    
    if all(strcmp(idx, ':'))
        % The entire local part resides on this worker, so index not needed;
        indxExprStr = '';
        return;
    end
    
    % Append commas to all indices except the last one.
    idx(1:end-1) = strcat(idx(1:end-1), {','});
    
    % Concatenate all the indices to get a single index expression.
    indxExprStr = sprintf('(%s)', [idx{:}]);
end

function str = iGetIndexExprInDim(codistr, dim)
%    Returns an indexing expression that represents the global 
%    indices into the local part in a particular dimension.
%    Returned as a string in one of the following formats:
%    'a'
%    ':'
%    'a:b'
%    '[a1:b1 a2:b2 ... an:bn]'

globalLen = codistr.Cached.GlobalSize(dim);
localLen = codistr.hLocalSize;
localLen = localLen(dim);
if globalLen == localLen
    % We have the entire length in this dimension.
    str = ':';
    return;
end

[first, last] = codistr.globalIndices(dim, labindex);
if isscalar(first) && first == last
    % We only store one index in this dimension.
    str = sprintf('%d', first);
else
    % Create list of segments of the form first(i):last(i) and separate them by
    % space, as in 1:64 129:193.
    idx = arrayfun(@(a,b) sprintf('%d:%d', a, b), first, last, ...
                  'UniformOutput', false);
    if length(idx) > 1
        % Put the space between the segments:
        idx(1:end-1) = strcat(idx(1:end-1), {' '});
        % Surround the segments with brackets.
        idx = [{'['}, idx, {']'}];
    end
    % Concatenate all the segments into one long string representation of 
    % the index.
    str = [idx{:}];
end
end

