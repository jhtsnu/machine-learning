function outCellLPs = pCellfunArrayfunImpl(codistr, arrayfunOrCellfun, ...
                                           fcn, inCellLPs, ...
                                           trailingArgs, funNargout) 
%pCellfunArrayfunImpl Implementation for TensorProductCodistributor.

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.10.2 $  $Date: 2011/04/27 17:30:04 $

% Since the local parts all correspond to the same codistributor, they are all
% of the same size.  Therefore, the arrayfun/cellfun implementation simply calls
% the built-in arrayfun/cellfun on the local parts.
outCellLPs = cell(1, funNargout);
[outCellLPs{:}] = distributedutil.syncOnError(arrayfunOrCellfun, ...
                                              fcn, inCellLPs{:}, ...
                                              trailingArgs{:});
% We need to ensure that the local parts have the same class across all the
% labs.  Also, if some (but not all) of the local parts are empty, we need to
% fill in the empty local parts with empties of the correct class.
isLPEmptyOnLabFcn = @(labidx) prod(codistr.hLocalSize(labidx)) == 0;
isLPEmpty = arrayfun(isLPEmptyOnLabFcn, 1:numlabs);
firstNonEmptyLab = find(~isLPEmpty, 1, 'first');
if isempty(firstNonEmptyLab) 
    % Nothing to do since all labs had empty local parts.  Arrayfun and cellfun
    % always return these as double arrays.
    return;
end

% Collect template elements from all the labs into a matrix whose (i,j)-th
% element corresponds to the j-th output argument on lab i.  All labs verify
% consistency in class across the labs.
templCell = cellfun(@distributedutil.Allocator.extractTemplate, ...
                    outCellLPs, 'UniformOutput', false);
templCell = gcat(templCell, 1);
templCell = templCell(~isLPEmpty, :);
for outIdx = 1:size(templCell, 2)
    clza = class(templCell{1, outIdx});
    for i = 2:size(templCell, 1)
        clzb = class(templCell{i, outIdx});
        if ~isequal(clza, clzb);
            % Error message mimics the error message from arrayfun and cellfun.
            error(message('parallel:codistributors:TensorArrayCellfunClassMismatch', outIdx, clza, clzb));
        end
    end
end
if isLPEmpty(labindex)
    % Our local part is empty, but not all local parts are empty.  Use the
    % template elements from the first lab with non-empty local parts to
    % create our local part of the correct class.
    templCell = templCell(1, :)';
    createLPFcn = @(templ) distributedutil.Allocator.create(...
        codistr.hLocalSize(), templ);
    outCellLPs = cellfun(createLPFcn, templCell, ...
                         'UniformOutput', false);
end

end % End pCellfunArrayfunImpl
