function [LP, codistr] = hSparseImpl(codistr, globalILP, globalJLP,...
    valsLP, m, n, ~)
; %#ok<NOSEM> % Undocumented

%   Copyright 2011 The MathWorks, Inc.

    % This function returns a codistributor for a sparse matrix and its
    % local parts, given global input.
    
    codistr = codistr.hGetNewForSize([m n]);
    
    % Make sure inputs are row vectors
    globalILP = globalILP(:)';
    globalJLP = globalJLP(:)';
    valsLP = valsLP(:)';
    

    % Create a cell array of data to be transferred to other labs.
    % Each cell contains i, j, s values that are on this lab that belong on
    % lab dest.  Since the local parts are row vectors, each cell has three
    % rows: (1) i, (2) j, (3) vals.
    data = cell(1, numlabs);
    if ~isempty(globalILP)
        for dest = 1:numlabs
            tf = codistr.hIsGlobalIndexOnLab(1, globalILP, dest) & ...
                codistr.hIsGlobalIndexOnLab(2, globalJLP, dest);
            data{dest} = [globalILP(tf); globalJLP(tf); valsLP(tf)];
        end
    end

    % mydata will be a cell array containing the data cells from each lab 
    % that were identified by them as belonging on this lab.
    mydata = distributedutil.allToAll(data);
    
    % All of the nonempty cells have three rows; cell2mat will
    % concatenate them but the empty cells don't have three rows so they
    % must be removed
    mydata = mydata(~cellfun('isempty', mydata));   % remove empty cells
    mydata = cell2mat(mydata);

    % Now mydata (if it isn't empty) has three rows containing all of the
    % i, j, s values that belong on this lab.  For constructing the local
    % part of the sparse matrix, the global i and j indices need to be
    % converted to local indices.
    
    if ~isempty(mydata)
        globalILP = mydata(1, :);
        globalJLP = mydata(2, :);
        valsLP = mydata(3, :);

        localIInd = codistr.hGlobalToLocalIndices(globalILP, 1);
        localJInd = codistr.hGlobalToLocalIndices(globalJLP, 2);
    else
        localIInd = [];
        localJInd = [];
        valsLP = [];
    end
    
    localSize = codistr.hLocalSize();
    LP = sparse(localIInd, localJInd, valsLP, localSize(1), localSize(2));   
end