function [LP, codistr] = hElementwiseBinaryOpImpl(codistr, fcn, codistrA, LPA, codistrB, LPB)
; %#ok<NOSEM> %Undocumented
%hElementwiseUnaryOpImpl Implementation for TensorProductCodistributor.

%   Copyright 2009 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2011/04/27 17:30:03 $

if (isempty(codistrA) && ~isscalar(LPA)) || ...
        (isempty(codistrB) && ~isscalar(LPB))
    error(message('parallel:codistributors:TensorBinaryOpANotScalar'));
end
if isempty(codistrA) && isempty(codistrB)
    error(message('parallel:codistributors:TensorBinaryOpAtLeastOneCodistributor'));
end

% In all the cases that this method can be invoked, LPA stores data (replicated
% scalar or a local part), so does LPB, and we can always perform the binary
% operation in the same way:
LP = fcn(LPA, LPB);

% In all the cases that this method can be invoked (codistributed inputs or
% replicated inputs), the input and the output have the same distribution
% schemes, so the input value of codistr is the same as the output value.

end % End of hElementwiseBinaryOpImpl.
