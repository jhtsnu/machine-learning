function [codistr, cellOfLPs] = hNDGridImpl(codistr, cellOfReplicated, numOutputArrays)

%hNDGridImpl produces a cell array of local parts of an ndgrid based on the
%   input cell array of replicated matrices

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:47:38 $

sizes = cellfun(@numel, cellOfReplicated);
codistr = codistr.hGetNewForSize(sizes);

% Get global indices
for i = 1:length(cellOfReplicated)
    cellOfReplicated{i} = cellOfReplicated{i}(codistr.hGlobalIndicesImpl(i, labindex));
end

% Get local parts
[cellOfLPs{1:numOutputArrays}] = ndgrid(cellOfReplicated{:});

end   %end of hNDGridImpl