function [LP, codistr] = hLinspaceImpl(codistr, a, b, n)
%hLinspaceImpl  Implementation for TensorProductCodistributor.

%   Copyright 2010-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:47:36 $

n = floor(n);
n1 = n - 1;
codistr = codistr.hGetCompleteForSize([1, n]);
[LP, codistr] = codistr.hColonImpl(0, 1, n1);
differenceInEndPoints = b - a;

if isinf(differenceInEndPoints)
    LP = a + (b/n1).*LP - (a/n1).*LP; % overflow for a < 0 and b > 0
else
    LP = a + (differenceInEndPoints/n1).*LP;
end
    
if ~isempty(LP)
    isGlobalRowEndpoint = codistr.hGlobalIndicesImpl(1, labindex) == 1;
    isGlobalColEndpoint = codistr.hGlobalIndicesImpl(2, labindex) == n;
    LP(isGlobalRowEndpoint, isGlobalColEndpoint) = b;
end