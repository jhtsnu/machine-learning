function D = logspace(varargin)
%LOGSPACE Build codistributed arrays of logarithmically equally spaced
%   points using codistributor1d
%   V = LOGSPACE(..., CODISTR) and V = LOGSPACE(..., CODISTR, ...) where CODISTR
%   is a 1D codistributor, are equivalent to calling CODISTRIBUTED.LOGSPACE with
%   the same input arguments.
%
%   Example:
%     Create a vector of logarithmically equally spaced points as a codistributed array:
%     spmd
%         v = logspace(1, 5, codistributor('1d'))
%     end
% 
%   See also LOGSPACE, CODISTRIBUTED/LOGSPACE, CODISTRIBUTOR1D

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:47:41 $

try
    D = codistributed.logspace(varargin{:});
catch E
    throw(E);
end

end % End of logspace.
