function D = linspace(varargin)
%LINSPACE Build codistributed arrays of linearly equally spaced points
%   using codistributor1d
%   V = LINSPACE(..., CODISTR) and V = LINSPACE(..., CODISTR, ...) where CODISTR
%   is a 1D codistributor, are equivalent to calling CODISTRIBUTED.LINSPACE with
%   the same input arguments.
%
%   Example:
%     Create a vector of linearly equally spaced points as a codistributed array:
%     spmd
%         v = linspace(1, 1000, codistributor('1d'))
%     end
% 
%   See also LINSPACE, CODISTRIBUTED/LINSPACE, CODISTRIBUTOR1D

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:47:40 $

try
    D = codistributed.linspace(varargin{:});
catch E
    throw(E);
end

end % End of linspace.
