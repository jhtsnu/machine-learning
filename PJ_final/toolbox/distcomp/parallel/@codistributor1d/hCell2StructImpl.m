function [LP, codistr] = hCell2StructImpl(codistr, LP, fields, dim)
    ; %#ok<NOSEM> % Undocumented

    %   hCell2StructImpl Implementation of cell2struct for codistributor1d.

    %   Copyright 2012 The MathWorks, Inc.

    distrDim = codistr.Dimension;
    part = codistr.Partition;
    gsize = codistr.Cached.GlobalSize;
    
    if distrDim == dim
        % We are always guaranteed that gsize has at least two elements
        if distrDim == 1
            distrDim = 2;
        else 
            distrDim = distrDim - 1;
        end
                
        destCodistr = codistributor1d(distrDim, codistributor1d.unsetPartition, gsize);
        [LP, codistr] = distributedutil.Redistributor.redistribute(codistr, LP, destCodistr);
        part = codistr.Partition;
    end

    gsize(dim) = [];
    if numel(gsize)==1
        % Need to reorient because if input was a 2D codistributed MATRIX, cell2struct
        % always returns a column vector.
       gsize = [gsize, 1];
    end 

    %If the dim is less than distDim, then one matrix dimension has been
    %"folded" away, thus we need to decrement the distrDim.
    if dim < distrDim
        distrDim = distrDim - 1;
    end
    
    codistr = codistributor1d(distrDim, part, gsize);
    
    LP = codistr.hCell2StructBuildLPImpl(LP, fields, dim);
    
end
