function globalInd = hLocalToGlobalIndices(codistr, localInd, dim, lab)
; %#ok<NOSEM> % Undocumented

%   Implementation of hLocalToGlobalIndices for codistributor1d.

%   Copyright 2011-2012 The MathWorks, Inc.

    if nargin < 4
        lab = labindex;
    end
    
    if ~distributedutil.CodistParser.isValidLabindex(lab)
        error(message('parallel:codistributors:InvalidLabInput'));
    end
    
    if ~isPositiveIntegerValuedNumeric(dim) || ...
            dim > length(codistr.Cached.GlobalSize)
        error(message('parallel:codistributors:InvalidDimInput'));
    end
    
    localSize = codistr.hLocalSize();
    if  ~isPositiveIntegerValuedNumeric(localInd) || ...
            localInd < 1 || localInd > localSize(dim)
        error(message('parallel:codistributors:InvalidLocalIndex'));
    end

    
    minInd = codistr.hGlobalIndicesImpl(dim, lab);
    globalInd = localInd + minInd - 1;
end

