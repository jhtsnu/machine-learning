function [LPC, codistrC] = hMtimesImpl(~, codistrA, LPA, codistrB, LPB)
% hMtimesImpl:  Implementation of mtimes for codistributor1d when both 
% matrices are codistributed1d

%   Copyright 2010-2012 The MathWorks, Inc.

[codistrA, LPA] = pRedistribute2DMatrixToDimOneOrTwo(codistrA, LPA);
[codistrB, LPB] = pRedistribute2DMatrixToDimOneOrTwo(codistrB, LPB);
Apart = codistrA.Partition;
Bpart = codistrB.Partition;
numRowsC = codistrA.Cached.GlobalSize(1);
numColsC = codistrB.Cached.GlobalSize(2);
szC = [numRowsC numColsC];
switch 10*codistrA.Dimension + codistrB.Dimension
    
    case 11
        % Rows * rows -> rows.
        % C(i, :) = sum over k A(i, k)*B(k, :) where A(i, k) is a submatrix
        % of LPA on the k-th lab and B(k, :) is the entire LPB on the k-th
        % lab.  Send B(k, ;)'s around a ring of labs.
        
        codistrC = codistributor1d(1, Apart, szC);
        LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
        k = codistrB.globalIndices(1);
        LPC(:, :) = LPA(:, k)*LPB;
        to = mod(labindex-2, numlabs)+1;
        from = mod(labindex, numlabs)+1;
        mwTag1 = 31639;
        for p = [labindex+1:numlabs 1:labindex-1]
            LPB = labSendReceive(to, from, LPB, mwTag1);
            k = codistrB.globalIndices(1, p);
            LPC = LPC + LPA(:, k)*LPB;
        end
        
    case 12
        % Rows * columns -> columns.
        % No partial sums required.
        % C(i, j) = A(i, :)*B(:, j) where A(i, ;) and B(:, j)
        % are the entire local portions on the i-th and j-th labs
        % Send A(i, :)'s around a ring of labs.
        
        codistrC = codistributor1d(2, Bpart, szC);
        LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
        i = codistrA.globalIndices(1);
        LPC(i, :) = LPA*LPB;
        to = mod(labindex-2, numlabs)+1;
        from = mod(labindex, numlabs)+1;
        mwTag2 = 31640;
        for p = [labindex+1:numlabs 1:labindex-1]
            LPA = labSendReceive(to, from, LPA, mwTag2);
            i = codistrA.globalIndices(1, p);
            LPC(i, :) = LPA*LPB;
        end
        
    case 21
        % Columns * rows
        % Two sub-cases:
        % numCols < numRows -> distribute result by rows.
        % numCols >= numRows -> distribute result by columns.
        % Reduction over distributed dimensions.
        % Make sure size(LPA, 2) == size(LPB,  1), then
        % compute T = LPA*LPB, LP = codistributed(gplus(T))
        % without actually using gplus and codistributed(...).
        % Message pattern is all-to-all to distribute and sum T's.
        
        if ~isequal(Apart, Bpart)
            origCodistrB = codistrB;
            codistrB = codistributor1d(1, Apart, codistrB.Cached.GlobalSize);
            LPB = distributedutil.Redistributor.redistribute(origCodistrB, LPB, codistrB);
        end
        mwTag3 = 31641;
        if numColsC<numRowsC
            % Matrix * matrix, result distributed by rows
            Cpart = codistributor1d.defaultPartition(numRowsC);
            codistrC = codistributor1d(1, Cpart, szC);            
            LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
            i = codistrC.globalIndices(1);
            LPC(:, :) = LPA(i, :)*LPB;
            for p = 1:numlabs-1
                to = mod(labindex+p-1, numlabs)+1;
                from = mod(labindex-p-1, numlabs)+1;
                i = codistrC.globalIndices(1, to);
                myLPC = LPA(i, :)*LPB;
                LPC = LPC + labSendReceive(to, from, myLPC, mwTag3);
            end
        else
            % Matrix * matrix, result distributed by columns
            Cpart = codistributor1d.defaultPartition(numColsC);
            codistrC = codistributor1d(2, Cpart, szC);
            LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
            j = codistrC.globalIndices(2);
            LPC(:, :) = LPA*LPB(:, j);
            for p = 1:numlabs-1
                to = mod(labindex+p-1, numlabs)+1;
                from = mod(labindex-p-1, numlabs)+1;
                j = codistrC.globalIndices(2, to);
                myLPC = LPA*LPB(:, j);
                LPC = LPC + labSendReceive(to, from, myLPC, mwTag3);
            end
        end
        
    case 22
        % Columns * columns -> columns
        % (Includes default matrix distributions.)
        % C(:, j) = sum over k A(:, k)*B(k, j) where A(:, k) is the
        % entire local portion of A on the k-th lab and B(k, j) is
        % a submatrix of the local portion of B on the k-th lab.
        % Send A(:, k)'s around a ring of labs.
        
        codistrC = codistributor1d(2, Bpart, szC);
        LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
        k = codistrA.globalIndices(2);
        LPC(:, :) = LPA*LPB(k, :);
        to = mod(labindex-2, numlabs)+1;
        from = mod(labindex, numlabs)+1;
        mwTag4 = 31642;
        for p = [labindex+1:numlabs 1:labindex-1]
            LPA = labSendReceive(to, from, LPA, mwTag4);
            k = codistrA.globalIndices(2, p);
            LPC = LPC + LPA*LPB(k, :);
        end
        
    otherwise
        ME = MException(message(...
            'parallel:codistributors:c1dMtimesHighDistDimNotSupported'));
        throwAsCaller(ME);   
end

end  % End of hMtimesImpl
