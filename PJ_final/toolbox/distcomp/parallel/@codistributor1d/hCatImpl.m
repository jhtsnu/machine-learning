function [LP, codistr] = hCatImpl(obj, catdim, codistrs, LPs, templ, targetSize)
; %#ok<NOSEM> % Undocumented
%   Implementation of hCatImpl for codistributor1d.  

%   Copyright 2009-2010 The MathWorks, Inc.
%   $Revision: 1.1.8.1 $  $Date: 2010/07/19 12:50:12 $
    
% Since hCatCheck has returned true, we can trivially concatenate the local
% parts to form the resulting array.  
% First, convert the replicated arrays into the corresponding local parts.
isReplicated = cellfun(@isempty, codistrs); 
LPs(isReplicated) = cellfun(@(lp) iConvertToLocalPart(obj, lp), ...
                            LPs(isReplicated), ...
                            'UniformOutput', false);

codistr = codistributor1d(obj.Dimension, obj.Partition, targetSize);
LP = distributedutil.Allocator.create(codistr.hLocalSize(), templ);
LP(:) = cat(catdim, LPs{:});
if issparse(templ) && ~issparse(LP)
    LP = sparse(LP);
end
end % End of hCatImpl.

function LP = iConvertToLocalPart(obj, X)
    codistr =  codistributor1d(obj.Dimension, obj.Partition, size(X));
    srcLab = 0;
    LP = codistr.hBuildFromReplicatedImpl(srcLab, X);
end % End of iConvertToLocalPart.
