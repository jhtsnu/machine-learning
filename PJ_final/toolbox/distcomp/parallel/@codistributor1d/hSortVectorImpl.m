function [LPA, LPI, codistr] = hSortVectorImpl(codistr, LPA, trailingArgs,...
    wantI)
;%#ok<NOSEM> % Undocumented
%   Implementation of hSortVectorImpl for codistributor1d.
%   This function should be called only if the matrix has one nontrivial 
%   dimension.

%   The algorithm used here, column sort, was originally described by F.
%   Thomson Leighton, IEEE Transactions on Computers, C-34(4):344-354
%   (1985).  We begin by ensuring that the distributed array is in the form
%   of a column vector distributed by rows.  Then we choose r and s so that
%   the vector can be reshaped (after suitable padding) to [r s].  Further 
%   details on choosing appropriate values for r and s are in the comments
%   for subfunction iPickRS.  Every lab has the same number of elements of
%   the array, even if it hadn't had any elements in the original
%   distribution.

%   The sorting proceeds in 8 steps.
%   1. Sort local columns.  Each column is of length r.  This is done in
%      iCallSort.
%   2. Reshape/transpose.  The entire array is reshaped to [s r] and then
%      transposed back to [r s].  The local parts of the array are reshaped
%      and columns of the reshaped array are exchanged between labs in an
%      all-to-all communication so that each lab will have its part of the
%      array after transposition.  This is done in iTransposeReshape(...,
%      'forward', ...).
%   3. Sort local columns again using iCallSort.
%   4. Transpose/reshape.  This is the inverse of step 2, but sorting
%      progress is made because the contents of the columns have been 
%      reordered in step 3.  This time, the matrix is transposed to [s r], 
%      appropriate columns of the transposed matrix are exchanged between
%      labs, and the local parts are reshaped.  This is done in 
%      iTransposeReshape(..., 'reverse',...).
%   5. Sort local columns again using iCallSort.
%   6. Shift the entire matrix down by r/2, moving the lower half of each
%      column to the top half of the column to its right.  If it is the
%      lower half of the last column on a lab, it is sent to the adjacent
%      lab to be inserted at the top left of that lab's matrix.  The last 
%      half-column from the last lab gets sent to the first lab.  This is 
%      done in iShift(..., 'down', ...).
%   7. The local columns are sorted again, but the top and bottom halves of
%      the first column on the first lab are sorted separately.  This is
%      done in iFinalSort.
%   8. Step 6 is undone by shifting the entire matrix up by r/2, restoring
%      the half-columns that had been moved to other labs to the labs from
%      which they had come.  This is done in iShift(..., 'up', ...).
%
%   At this point, the local parts contain the sorted array, but the
%   padding must be removed to get back to the original number of elements
%   and the array must be converted back to a column vector.  This is done
%   in iRemovePadding.  iRestoreShape restores the vector to being 
%   distributed on the same dimension as when it arrived, but a new
%   codistributor is created because its partition is likely to be
%   different.  This is done in iRestoreShape.

%   When an index of the sorted vector is requested, a step to sort the
%   indices of equivalent elements into ascending order is taken after each
%   sort step (steps 1, 3, 5, 7).  This is necessary to preserve the stability
%   of (1) the index vector when sorting numeric vectors and (2) the data and
%   index vector if it's a cell array of strings which contains empty strings.
% This is done in iSortIndicesOfDuplicates.

%   Copyright 2011-2012 The MathWorks, Inc.

if nnz(codistr.Partition) == 1  % All of the vector is on one lab
    [LPA, LPI] = iDoLocalSort(codistr, LPA, trailingArgs);
    return
end

globalSize = codistr.Cached.GlobalSize;
origDim = codistr.Dimension;
N = globalSize(origDim);
isCellA = iscell(LPA);

% Insure A is column vector codistributed by rows
if origDim ~= 1
    codistr = codistributor1d(1, codistr.Partition, [N, 1]);
    if ~isempty(trailingArgs)
        trailingArgs{1} = 1;
    end
    if isempty(LPA)
        LPA = distributedutil.Allocator.create(codistr.hLocalSize(), LPA);
    else
        LPA = LPA(:);
    end
end

[r, s, numColsPerLab, myAugment] = iPickRS(N, codistr.Partition, isCellA);
LPA = iPadLocalPart(LPA, myAugment);

LPA = reshape(LPA, r, numColsPerLab);

if wantI
    LPI = codistr.globalIndices(1)';
    LPI = [LPI; NaN(myAugment, 1)];
    LPI = reshape(LPI, r, numColsPerLab);
else
    LPI = [];
end

[LPA, LPI] = iCallSort(LPA, LPI, trailingArgs, wantI);
    
[LPA, LPI] = iTransposeReshape(LPA, LPI, r, 'forward', wantI);

[LPA, LPI] = iCallSort(LPA, LPI, trailingArgs, wantI);    

[LPA, LPI] = iTransposeReshape(LPA, LPI, r, 'reverse', wantI);

[LPA, LPI] = iCallSort(LPA, LPI, trailingArgs, wantI);
    
[LPA, LPI] = iShift(LPA, LPI, r, 'down', wantI);

[LPA, LPI] = iFinalSort(LPA, LPI, r, trailingArgs, wantI);

[LPA, LPI] = iShift(LPA, LPI, r, 'up', wantI);

[LPA, LPI, newPart] = iRemovePadding(LPA, LPI, r, s, N, trailingArgs, wantI);

[LPA, LPI, codistr] = iRestoreShape(LPA, LPI, origDim, N, newPart, wantI);

end % End of hSortVectorImpl.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [LPA, LPI] = iCallSort(LPA, LPI, trailingArgs, wantI)
if wantI
    [LPA, I] = sort(LPA, trailingArgs{:});
    for i=1:size(I, 2); 
        LPI(:, i) = LPI(I(:, i), i);
    end
    [LPA, LPI] = iSortIndicesOfDuplicates(LPA, LPI);
else
    LPA = sort(LPA, trailingArgs{:});
end
end

function [LPA, LPI] = iDoLocalSort(codistr, LPA, trailingArgs)
if isempty(LPA)  % all of A is on another lab
    LPI = [];
    LPI = distributedutil.Allocator.create(codistr.hLocalSize(), LPI);
else
    [LPA, LPI] = sort(LPA, trailingArgs{:});
end
end

function [LPA, LPI] = iFinalSort(LPA, LPI, r, trailingArgs, wantI)
if labindex == 1
    halfR = r/2;
    if wantI
        [LPA(1:halfR, 1), LPI(1:halfR, 1)] = iCallSort(LPA(1:halfR, 1), ...
            LPI(1:halfR, 1), trailingArgs, wantI);
        [LPA(halfR+1:end, 1), LPI(halfR+1:end, 1)] = ...
            iCallSort(LPA(halfR+1:end, 1), LPI(halfR+1:end, 1), trailingArgs, ...
            wantI);
        if size(LPA, 2) > 1
            [LPA(:, 2:end), LPI(:, 2:end)] = iCallSort(LPA(:, 2:end), ...
             LPI(:, 2:end), trailingArgs, wantI);
        end
    else
        [LPA(1:halfR, 1), LPI] = iCallSort(LPA(1:halfR, 1), LPI, ...
            trailingArgs, wantI);
        [LPA(halfR+1:end, 1), LPI] = iCallSort(LPA(halfR+1:end, 1), LPI, ...
            trailingArgs, wantI);
        if size(LPA, 2) > 1
            [LPA(:, 2:end), LPI] = iCallSort(LPA(:, 2:end), LPI, trailingArgs, ...
            wantI);
        end
    end
    return
end

[LPA, LPI] = iCallSort(LPA, LPI, trailingArgs, wantI);

end

function LPA = iPadLocalPart(LPA, myAugment)
if myAugment > 0
    if isnumeric(LPA)
        myPadding = NaN(myAugment, 1);
    elseif iscell(LPA)
        myPadding = repmat({''}, myAugment, 1);
    elseif ischar(LPA)
        myPadding = zeros(myAugment, 1);
    elseif islogical(LPA)
        myPadding = false(myAugment, 1);
    else
        error(message('parallel:codistributors:c1dSortUnknownType', class( LPA )));
    end
    LPA = [LPA; myPadding];
end
end

function [r, s, numColsPerLab, myAugment] = iPickRS(N, part, isCellA)
% This subfunction chooses r, the number of rows, and s, the number of
% columns, in the working matrix for column sort.  The requirements are that
% s be a multiple of numlabs and that r be even, a multiple of s, large
% enough that the number of elements on each lab is >= the greatest number
% on a lab upon entering this routine, and >= 2*(s-1)^2.

% Because sorting a cell array of strings converts multi-column arrays into
% single column ones, s is always chosen to be equal to numlabs for
% cellstrs.

% Any values of r and s that are consistent with these requirements will
% work, but choosing larger s so that more (and shorter) columns can be
% sorted in parallel should lead to higher efficiency.  Since N <= r*s and
% r >= 2*(s-1)^2 are two of the requirements, a maximum value for s that
% minimizes padding is given by the positive real root of s^3-2*s^2-s-N/2.
% Since this is rounded up to make it a multiple of numlabs, decreasing it
% in steps of numlabs may reduce the padding, so that is done in the while
% loop

if isCellA  
    s = numlabs;
    r = max([max(part), ceil(N/s), 2*(s-1).^2]);
    numColsPerLab = 1;
else
    np = roots([1 -2 1 -N/2]);
    np = np(real(np) == np);  % Keep only the real one
    if (isempty(np) || np(1) <= 0)
        % No solution to the polynomial, so we won't optimize the algorithm
        numColsPerLab = 1;
    else
        numColsPerLab = ceil(np(1)/numlabs);
    end
    s = numColsPerLab*numlabs;
    r = max([max(part)/numColsPerLab, ceil(N/s), 2*(s-1).^2]);
    while s > numlabs
        s = s - numlabs;
        numColsPerLab = numColsPerLab - 1;
        r = max([max(part)/numColsPerLab, ceil(N/s), 2*(s-1).^2]);
        if r > 2*(s-1).^2
            break
        end
    end
end
r = ceil(r/s)*s;

% Ensure r is even
if mod(r, 2) ~= 0
    r = r+s;
end
myAugment = r*numColsPerLab - part(labindex);
end

function [LPA, LPI, newPart] = iRemovePadding(LPA, LPI, r, s, N, ...
    trailingArgs, wantI)
newPart = (r*s/numlabs)*ones(1, numlabs);
LPA = LPA(:);
LPI = LPI(:);
if r*s > N  % Extra values were added
    if wantI
        % Trim back to original size
        indicesToKeep = ~isnan(LPI); % NaN's in index tell us what was added
        LPA = LPA(indicesToKeep);
        LPI = LPI(indicesToKeep);
        myPart = nnz(indicesToKeep);
        newPart = gcat(myPart);
    else
        numColsPerLab = s/numlabs;
        cumsumOfPart = (r*numColsPerLab)*(1:numlabs);
        if isempty(trailingArgs)
            mode = 'ascend';
        else
            mode = trailingArgs{2};
        end
        % Trim back to original size
        if (isnumeric(LPA) && strcmpi(mode, 'ascend')) || ...
            (~isnumeric(LPA) && strcmpi(mode, 'descend'))
            % remove extras from end
            firstExceed = find(cumsumOfPart > N, 1, 'first');
            if firstExceed == 1
                preceding = 0;
            else
                preceding = cumsumOfPart(firstExceed-1);
            end
            newPart(firstExceed) = N - preceding;
            newPart(firstExceed+1:end) = 0;
            LPA = LPA(1:newPart(labindex));
        else  % remove extras from beginning
            num2Remove = r*s - N;
            firstExceed = find(cumsumOfPart >= num2Remove, 1, 'first');
            newPart(firstExceed) = cumsumOfPart(firstExceed) - num2Remove;
            if firstExceed > 1
                newPart(1:firstExceed-1) = 0;
            end
            LPA = LPA((1+r*numColsPerLab-newPart(labindex)):end);
        end
    end
end
end

function [LPA, LPI, codistr] = iRestoreShape(LPA, LPI, origDim, N, newPart, ...
    wantI)
% This creates a codistributor that describes the sorted array as a vector
% on the original dimension and reshapes the local parts to match
szA = ones(1, ndims(LPA));
szA(origDim) = N;
szLP = ones(1, ndims(LPA));
szLP(origDim) = numel(LPA);
LPA = reshape(LPA, szLP);
if wantI
    LPI = reshape(LPI, szLP);
end
codistr = codistributor1d(origDim, newPart, szA);
end

function [LPA, LPI] = iShift(LPA, LPI, r, direction, wantI)

% A list of interesting points - store theme so we don't keep too many
% vectors of size LP in memory.
topStartIdx = 1;
topEndIdx = r/2;
bottomEndIdx = r*size(LPA, 2);
bottomStartIdx = bottomEndIdx-(r/2)+1;

switch direction
    case 'down' % and right
        offset                           = 1;
        toSend.values                    = LPA(bottomStartIdx:bottomEndIdx);
        recvEls                          = [topStartIdx, topEndIdx];
        LPA(topEndIdx+1:bottomEndIdx)    = LPA(1:bottomStartIdx-1);
        if wantI
            toSend.index                  = LPI(bottomStartIdx:bottomEndIdx);
            LPI(topEndIdx+1:bottomEndIdx) = LPI(1:bottomStartIdx-1);
        end
    case 'up' % and left
        offset                           = -1;
        toSend.values                    = LPA(topStartIdx:topEndIdx);
        recvEls                          = [bottomStartIdx, bottomEndIdx];
        LPA(1:bottomStartIdx-1)          = LPA(topEndIdx+1:bottomEndIdx);
        if wantI
            toSend.index                 = LPI(topStartIdx:topEndIdx);
            LPI(1:bottomStartIdx-1)      = LPI(topEndIdx+1:bottomEndIdx);
        end
end
labTo = mod(labindex + offset - 1, numlabs) + 1;
labFrom = mod(labindex - offset -1, numlabs) + 1;

mwTag2 = 31212;
toSend = labSendReceive(labTo, labFrom, toSend, mwTag2);
LPA(recvEls(1):recvEls(2)) = toSend.values;
if wantI
    LPI(recvEls(1):recvEls(2)) = toSend.index;
end    
end

function [LP, LPI] = iSortIndicesOfDuplicates(LP, LPI)
if size(LP, 1) < 2; return; end  % no sorting to do

isCellArray = iscellstr(LP);

if isCellArray
    diffBetweenElements = ~strcmp(LP(2:end, :), LP(1:end-1, :));
else
    finiteLP = isfinite(LP);
    % At this step, diffBetweenElements contains true at the beginning
    % and end of blocks of Infs and Nans
    diffBetweenElements = or(logical(diff(isnan(LP))), ...
                             logical(diff(isinf(LP))));
    remainingFinite = ~(~finiteLP(1:end-1, :) + diffBetweenElements);
    % In this loop, we add true for cases where finite elements are
    % different
    for i = 1:size(LP, 2)
        if any(remainingFinite(:, i))
            diffBetweenElements(remainingFinite(:, i), i) =...
                   logical(diff(double(LP(finiteLP(:, i), i))));
        end
    end
end

if all(diffBetweenElements) % no duplicates
    return
end

if ~any(diffBetweenElements) % all duplicates
    if ~isCellArray
        LPI = sort(LPI);
    else
        % All the duplicates could be empty strings of different sizes, so
        % we need to ensure that the data and indices stay in sync. 
        [LPI, I] = sort(LPI);
        LP = LP(I);
    end
    return
end

localIndicesOfDiff = 1:size(LP, 1)-1;
for j = 1:size(LP, 2) % Sort blocks of duplicates in each column
    uniqueElemIndices = localIndicesOfDiff(diffBetweenElements(:, j));
    first = [0 uniqueElemIndices] + 1;
    last = [uniqueElemIndices size(LP, 1)];

    duplicateElemIndices = find(first ~= last);
    for i = duplicateElemIndices
        if ~isCellArray
            LPI(first(i):last(i), j) = sort(LPI(first(i):last(i), j));
        else
            % Duplicates could be empty strings of different sizes, so
            % we need to ensure that the data and indices stay in sync. 
            [LPI(first(i):last(i), j), I] = sort(LPI(first(i):last(i), j));
            tmp = LP(first(i):last(i), j);
            LP(first(i):last(i), j) = tmp(I,:);    
        end
    end
end
end

function [LPA2, LPI2] = iTransposeReshape(LPA, LPI, r, direction, wantI)

numColsPerLab = numel(LPA)/r;  % number of columns on each lab
nElsToSend = r/numlabs;
switch direction
    case 'forward'
        LPA = reshape(LPA, numColsPerLab, r);
        if wantI
            LPI = reshape(LPI, numColsPerLab, r);
        end
    case 'reverse'
        LPA = LPA.';
        LPI = LPI.';
end
LPA2 = LPA;
LPI2 = LPI;
% Do communication while matrix is in s x r shape (numColsPerLab x r on each lab)
% All-to-all could be used here if we had it
for offset = 0:numlabs-1
    labTo = mod(labindex + offset - 1, numlabs) + 1;
    labFrom = mod(labindex - offset - 1, numlabs) + 1;
    
    switch direction
        case 'forward'
            % Send with stride
            sendEls      = labTo:numlabs:r;
            % Receive into contiguous block
            recvStartIdx = 1 + (labFrom-1)*nElsToSend;
            recvEndIdx   = recvStartIdx + nElsToSend - 1;
            recvEls      = recvStartIdx:recvEndIdx;
       case 'reverse'
            % Send from contiguous block
            sendStartIdx = 1 + (labTo-1)*nElsToSend;
            sendEndIdx   = sendStartIdx + nElsToSend - 1;
            sendEls      = sendStartIdx:sendEndIdx;
            % Receive with stride
            recvEls      = labFrom:numlabs:r;
    end
    % exchange columns
    data2Send.values = LPA(:, sendEls);
    if wantI
        data2Send.index = LPI(:, sendEls);
    end
    mwTag1 = 31211;
    data2Send = labSendReceive(labTo, labFrom, data2Send, mwTag1);
    LPA2(:, recvEls) = data2Send.values;
    if wantI
        LPI2(:, recvEls) = data2Send.index;
    end
end
switch direction  % put it back to r x s (r x numColsPerLab on each lab)
    case 'forward'
        LPA2 = LPA2.';
        LPI2 = LPI2.';
    case 'reverse'
        LPA2 = reshape(LPA2, r, numColsPerLab);
        if wantI
            LPI2 = reshape(LPI2, r, numColsPerLab);
        end
end
end
