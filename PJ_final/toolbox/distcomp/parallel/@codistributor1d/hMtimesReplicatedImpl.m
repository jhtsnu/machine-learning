function [LPC, codistrC] = hMtimesReplicatedImpl(~, codistrA, LPA, codistrB, LPB)
% hMtimesReplicatedImpl:  Implementation of mtimes for codistributor1d
% when one matrix is replicated

%   Copyright 2010-2012 The MathWorks, Inc.
     
if isempty(codistrB)  % A distributed, B replicated
    [codistrA, LPA] = pRedistribute2DMatrixToDimOneOrTwo(codistrA, LPA);
    numRowsC = codistrA.Cached.GlobalSize(1);
    numColsC = size(LPB, 2);
    szC = [numRowsC numColsC];
 
    switch codistrA.Dimension
        case 1        % A distributed by rows
            codistrC = codistributor1d(1, codistrA.Partition, szC);
            LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
            LPC(:, :) = LPA*LPB;

        case 2        % A distributed by columns
            codistrC = codistributor1d(2, codistributor1d.unsetPartition, szC);
            LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
            i = codistrA.globalIndices(2);
            j = codistrC.globalIndices(2);
            LPC(:, :) = LPA*LPB(i, j);
            to = mod(labindex-2, numlabs)+1;
            from = mod(labindex, numlabs)+1;
            mwTag1 = 30618;
            for p = [labindex+1:numlabs 1:labindex-1]
                LPA = labSendReceive(to, from, LPA, mwTag1);
                k = codistrA.globalIndices(2, p);
                LPC = LPC + LPA*LPB(k, j);
            end
        otherwise     % A distributed on a higher dimension
            ME = MException(message(...
                'parallel:codistributors:c1dMtimesHighDistDimNotSupported'));
            throwAsCaller(ME);
    end
else   % A replicated, B distributed
    [codistrB, LPB] = pRedistribute2DMatrixToDimOneOrTwo(codistrB, LPB);
    numRowsC = size(LPA, 1);
    numColsC = codistrB.Cached.GlobalSize(2);
    szC = [numRowsC numColsC];
    
    switch codistrB.Dimension
        case 1       % B distributed by rows
            codistrC = codistributor1d(1, codistributor1d.unsetPartition, szC);
            LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
            k = codistrB.globalIndices(1);
            j = codistrC.globalIndices(1);
            LPC(:, :) = LPA(j, k)*LPB;
            to = mod(labindex-2, numlabs)+1;
            from = mod(labindex, numlabs)+1;
            mwTag2 = 30619;
            for p = [labindex+1:numlabs 1:labindex-1]
                LPB = labSendReceive(to, from, LPB, mwTag2);
                k = codistrB.globalIndices(1, p);
                LPC = LPC + LPA(j, k)*LPB;
            end
        case 2       % B distributed by columns
            codistrC = codistributor1d(2, codistrB.Partition, szC);
            LPC = distributedutil.Allocator.create(codistrC.hLocalSize(), LPA([])*LPB([]));
            LPC(:, :) = LPA*LPB;
        otherwise    % B distributed on a higher dimension
            ME = MException(message(...
                'parallel:codistributors:c1dMtimesHighDistDimNotSupported'));
            throwAsCaller(ME);
    end
end

end  % End of hMtimesReplicatedImpl
