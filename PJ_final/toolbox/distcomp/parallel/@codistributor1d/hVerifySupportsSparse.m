function hVerifySupportsSparse(codistr)
% Throw an error if the codistributor is not suitable for sparse codistributed
% arrays.

%   Copyright 2009-2012 The MathWorks, Inc.

numDims = length(codistr.Cached.GlobalSize);
if numDims > 2
    ex = MException('parallel:codistributors:c1dSparseSupportNDNotSupported', ...
                    'N-D sparse is not supported.');
    throwAsCaller(ex);
end

if codistr.Dimension > 2
    ex = MException(message(...
        'parallel:codistributors:c1dSparseSupportTooHighDistributionDim', ...
        codistr.Dimension));
    throwAsCaller(ex);
end

end % End of hVerifySupportsSparse.
         
    
