function [LPA, LPI, codistr] = hSortArrayImpl(codistr, LPA, trailingArgs, ...
    wantI)
;%#ok<NOSEM> % Undocumented
%   Implementation of hSortArrayImpl for codistributor1d.
%   This function should be called only if the matrix has more than one
%   nontrivial dimension.

%   Copyright 2011 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/04/01 23:55:35 $

dimToSort = trailingArgs{1};
sortCodistr = codistr;

if codistr.Dimension == dimToSort
    %redistribute onto another nontrivial dimension and sort if possible
    szA = codistr.Cached.GlobalSize;
    szA(dimToSort) = 1;
    [lenA, newSortDim]= max(szA);
    if lenA > 1
        sortCodistr = codistributor1d(newSortDim, ...
                     codistributor1d.unsetPartition, codistr.Cached.GlobalSize);
        [LPA, sortCodistr] = distributedutil.Redistributor.redistribute(codistr, ...
                            LPA, sortCodistr);
    end
end

if wantI
    [LPA, LPI] = sort(LPA, trailingArgs{:});
else
    LPA = sort(LPA, trailingArgs{:});
    LPI = [];
end
[LPA, codistr] = distributedutil.Redistributor.redistribute( ...
                 sortCodistr, LPA, codistr);
if wantI
    [LPI, codistr] = distributedutil.Redistributor.redistribute( ...
                     sortCodistr, LPI, codistr);
end                     

end % End of hSortArrayImpl.