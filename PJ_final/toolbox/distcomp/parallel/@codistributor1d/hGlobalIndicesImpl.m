function varargout = hGlobalIndicesImpl(dist, dim, lab)
%hGlobalIndicesImpl The implementation of global indices without the error checking.
%
%   See also codistributor1d/globalIndices.

%   Copyright 2007-2009 The MathWorks, Inc.
%   $Revision: 1.1.6.4 $  $Date: 2011/04/27 17:30:10 $

part = dist.Partition;

if dim == dist.Dimension
    [varargout{1:nargout}] = partitionIndices(part, lab);
    return;
end

% We only arrive here if the dimension is not equal to the distribution
% dimension.  
gSize = dist.Cached.GlobalSize;
if dim > length(gSize)
    dimSize = 1;
else
    dimSize = gSize(dim);
end

if nargout <= 1
    varargout{1} = 1:dimSize;
elseif nargout == 2
    varargout{1} = 1;
    varargout{2} = dimSize;
else
    error(message('parallel:codistributors:c1dGlobalIndicesImplTooManyOutputArguments'));
end

end % End of hGlobalIndicesImpl.
