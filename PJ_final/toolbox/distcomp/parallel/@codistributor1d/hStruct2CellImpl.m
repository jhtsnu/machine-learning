function [LP, codistr] = hStruct2CellImpl(codistr, LP)
    ;%#ok<NOSEM> % Undocumented

    %   hStruct2Cell Implementation of struct2cell for codistributor1d.
    
    %   Copyright 2012 The MathWorks, Inc.
    
    distDim = codistr.Dimension;
    distPart = codistr.Partition;
    gsize = codistr.Cached.GlobalSize;
    structFieldNames = fieldnames(LP);
    LP = struct2cell(LP);
    
    % struct2cell always pre-pends an extra dimension to the vector matrix that is
    % equivalent to the number of fields in the struct.  No redistribution is
    % necessary. Only the distribution dimension shifts by one. This also works
    % for when a column vector is distributed by a trivial or non-trivial
    % direction. This works because for a vector n x 1, newGlobalSize becomes f
    % x n x 1, and the last trivial dimension is folded away when the new
    % codistributor1d is built. Also since arrays in MATLAB are infinitely
    % dimensional it is legal to distribute across any dimension great than 0.
    
    newGlobalSize = [numel(structFieldNames), gsize];
    
    distDim = distDim + 1;
    
    codistr = codistributor1d(distDim, distPart, newGlobalSize);
    
    if isempty(LP)
        LP = cell(codistr.hLocalSize(labindex));
    end
end
