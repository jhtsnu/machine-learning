function [LPA, LPI, codistr] = hSortCellStrImpl(codistr, LPA, trailingArgs, ...
    wantI)
; %#ok<NOSEM> % Undocumented
%   Implementation of hSortCellStrImpl for codistributor1d.  

%   Copyright 2011-2012 The MathWorks, Inc.
    
origDim = codistr.Dimension;
szA = codistr.Cached.GlobalSize;
if length(szA) > 2 || min(szA) > 1 
    lastNonSingletonDim = distributedutil.Sizes.lastNonSingletonDimension(szA);
    if lastNonSingletonDim > origDim  && wantI
        [LPA, codistr] = distributedutil.Redistributor.redistribute(codistr, ...
         LPA, codistributor1d(lastNonSingletonDim));
    end
    % Adjust partition sizes because elements have been moved into distribution
    % dimension
    N = prod(szA);
    codistr.Partition = N/sum(codistr.Partition)*codistr.Partition;
    codistr.Dimension = 1;
    codistr.Cached.GlobalSize = [N 1];
    if ~isempty(LPA)
        LPA = LPA(:);
    else
        LPA = distributedutil.Allocator.create(codistr.hLocalSize(), LPA);
    end
end

% We always use indices to ensure empties are sorted properly
wantIndices = true;
[LPA, LPI, codistr] = codistr.hSortVectorImpl(LPA, trailingArgs, ...
                                              wantIndices);

end % End of hSortCellStrImpl.
