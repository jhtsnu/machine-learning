function [LP, codistr] = hTypecastImpl(codistr, LP, datatype)
    ; %#ok<NOSEM> % Undocumented

    %   hTypecastImpl Implementation of typecast for codistributor1d.

    %   Copyright 2012 The MathWorks, Inc.

    finalClassSize = distributedutil.Allocator.findDataTypeSizeInBytes(datatype);
    origClassSize = distributedutil.Allocator.findDataTypeSizeInBytes(class(LP));
    
    % Guarantee that each worker will create an integer number of elements
    % of the new datatype.
    [LP, codistr] = iRedistributeIfNeeded(codistr, LP, origClassSize, ...
                                          finalClassSize);

    LP = typecast(LP, datatype);
    distrDim = codistr.Dimension;
    part = codistr.Partition;
    gsize = codistr.Cached.GlobalSize;
    ratio = origClassSize / finalClassSize;

    if gsize(1) <= gsize(2) 
        % The codistributed array is either a scalar, or a row vector.
        nonTrivialDim = 2;
    else
        % The codistributed array is a column vector.
        nonTrivialDim = 1;
    end
    gsize(nonTrivialDim) = gsize(nonTrivialDim) * ratio;


    % Rescale the partitions if the vector is distributed along the non
    % trivial dimension, or if the result of the typecast is a 1 by 1
    % scalar.
    if distrDim == nonTrivialDim
        part = part .* ratio;
    end

    codistr = codistributor1d(distrDim, part, gsize);

    if isempty(LP) 
        % Reshape the empty local part matrix to correct size for construction
        % of the new codistributed array.
        newSize = hLocalSize(codistr, labindex);
        LP = reshape(LP, newSize);
    end
end

function [LP, codistr] = iRedistributeIfNeeded(codistr, LP, ...
    origClassSize, finalClassSize) 
    % Redistribute the vector is needed to ensure that each workers partition is
    % a multiple of the elements needed to cast to larger datatype.
    
    if origClassSize >= finalClassSize
        return; % We can always perform this typecast operation.
    end
    
    distrDim = codistr.Dimension;
    part = codistr.Partition;
    gsize = codistr.Cached.GlobalSize;
    nel = prod(gsize);
    isNonTrivialDistribution = gsize(distrDim) == nel && nel > 1;
    minBlockSize = finalClassSize / origClassSize; % number of elements of original
                                                   % datatype needed to build
                                                   % one element of new datatype

    if rem(nel, minBlockSize) ~= 0
        error(message('parallel:distributed:TypecastNotEnoughInputElements'));
    end

    if any(rem(part, minBlockSize)) ...
        && isNonTrivialDistribution
        for index = 1:numlabs - 1
            part(index) = part(index) - rem(part(index), minBlockSize);
            nel = nel - part(index);
        end
        part(numlabs) = nel;
        destCodistr = codistributor1d(distrDim, part, gsize); 
        [LP, codistr] = distributedutil.Redistributor.redistribute(...
            codistr, LP, destCodistr);
    end
end
