function localInd = hGlobalToLocalIndices(codistr, globalInd, dim, lab)
; %#ok<NOSEM> % Undocumented

%   Implementation of hGlobalToLocalIndices for codistributor1d.

%   Copyright 2011-2012 The MathWorks, Inc.

    if nargin < 4
        lab = labindex;
    end
    
    if ~distributedutil.CodistParser.isValidLabindex(lab)
        error(message('parallel:codistributors:InvalidLabInput'));
    end
    
    if ~isPositiveIntegerValuedNumeric(dim) || ...
            dim > length(codistr.Cached.GlobalSize)
        error(message('parallel:codistributors:InvalidDimInput'));
    end
    
    if  ~isPositiveIntegerValuedNumeric(globalInd) || ...
            any(globalInd > codistr.Cached.GlobalSize(dim)) || ...
            any(globalInd < 1)
        error(message('parallel:codistributors:InvalidGlobalIndex'));
    end
    
    [minInd, maxInd] = codistr.hGlobalIndicesImpl(dim, lab);
    localInd = globalInd - minInd + 1;
    localInd(globalInd < minInd) = -1;
    localInd(globalInd > maxInd) = -1;
end

