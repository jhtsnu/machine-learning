function [ newDimension, newPartition ] = pGetReshapeTarget( codistr, newSize )
% pGetReshapeTarget Determines a distribution dimension and partition for
% reshaping a codistributed1d matrix.

%   If newDimension is > 0 and newPartition is integer-valued, a matrix
%   distributed by codistr can be reshaped to newSize without communication

%   Copyright 2011 The MathWorks, Inc.

    newDimension = -1;
    newPartition = [];
    oldSize = codistr.Cached.GlobalSize;
    
    % Is entire array on one lab?
    if nnz(codistr.Partition) == 1
        newPartition = codistr.Partition;
        % Since the new distribution dimension is > size of the new array, 
        % its size is 1, so the partition has to have a 1 in it.
        newPartition(codistr.Partition>0) = 1;
        newDimension = length(newSize) + 1;
        return
    end
    
    % Find newDimension if possible
    prodOfUpperDimsIn = prod(oldSize(codistr.Dimension+1:end));
    revCumprodOfNewSize = fliplr(cumprod(fliplr([newSize 1])));
    loc = find(revCumprodOfNewSize == prodOfUpperDimsIn, 1, 'first');
    if isempty(loc)
        return
    else
        newDimension = max(1, loc - 1);
    end

    % Calculate newPartition
    % The first element of revCumprodOfNewSize is numel(NewSize)
    numelInOtherDims = revCumprodOfNewSize(1)/newSize(newDimension);
    newPartition = zeros(1, numlabs);
    for i = 1:numlabs
        newPartition(i) = prod(codistr.hLocalSize(i))/numelInOtherDims;
    end
end