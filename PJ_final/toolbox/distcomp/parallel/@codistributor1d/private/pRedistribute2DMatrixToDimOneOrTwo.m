function [ codistr, LP ] = pRedistribute2DMatrixToDimOneOrTwo( codistr, LP )
% pRedistribute2DMatrixToDimOneOrTwo Redistributes a codistributed1d matrix if it is
% distributed on the third or higher dimension

%   Matrices that are distributed on the third or higher dimension will 
%   always be redistributed.

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/09/20 14:35:47 $

    if codistr.Dimension >= 3
        [~, newDim] = max(codistr.Cached.GlobalSize);
        origCodistr = codistr;
        newPart = codistributor1d.defaultPartition(codistr.Cached.GlobalSize(newDim));
        codistr = codistributor1d(newDim, newPart, codistr.Cached.GlobalSize);
        LP = distributedutil.Redistributor.redistribute(origCodistr, LP, codistr);
    end
end
