function tf = hCatCheck(obj, catdim, codistrs, ~)
; %#ok<NOSEM> % Undocumented
%   Implementation of hCatCheck for codistributor1d.  It only handles
%   concatenation that can be performed trivially and without any 
%   communication.

%   Copyright 2009-2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:50:11 $

% All codistributors must have the same Partition and Dimension, and Dimension
% must not equal catdim.

if catdim == obj.Dimension
    tf  = false;
    return
end

isReplicated = cellfun(@isempty, codistrs);
samePropVals = @(c) isequal(c.Partition, obj.Partition) ...
    && isequal(c.Dimension, obj.Dimension);
tf = all(cellfun(samePropVals, codistrs(~isReplicated)));

end % End of hCatCheck.
