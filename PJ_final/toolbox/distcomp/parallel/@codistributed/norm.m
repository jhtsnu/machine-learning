function s = norm(A,p)
%NORM Matrix or vector norm for codistributed array
%   All norms supported by the built-in function have been overloaded for codistributed arrays.
%   
%   For matrices...
%         N = NORM(D) is the 2-norm of D.
%         N = NORM(D, 2) is the same as NORM(D).
%         N = NORM(D, 1) is the 1-norm of D.
%         N = NORM(D, inf) is the infinity norm of D.
%         N = NORM(D, 'fro') is the Frobenius norm of D.
%         N = NORM(D, P) is available for matrix D only if P is 1, 2, inf, or 'fro'.
%   
%   For vectors...
%         N = NORM(D, P) is the same as sum(abs(D).^P)^(1/P) for 1 <= P < inf.
%         N = NORM(D) is the same as norm(D, 2).
%         N = NORM(D, inf) is the same as max(abs(D)).
%         N = NORM(D, -inf) is the same as min(abs(D)).
%   
%   Example:
%   spmd
%       N = 1000;
%       D = diag(codistributed.colon(1,N));
%       n = norm(D,1)
%   end
%   
%   returns n = 1000.
%   
%   See also NORM, CODISTRIBUTED, CODISTRIBUTED/COLON, CODISTRIBUTED/ZEROS.
%   


%   Copyright 2006-2012 The MathWorks, Inc.

    if nargin == 2
        distributedutil.CodistParser.verifyNonCodistributedInputs({A, p});
    end

    if ~ismatrix(A) 
        error(message('parallel:distributed:NormNotVectorOrMatrix'))
    end 
    
    if nargin < 2
        p = 2;
    else 
        p = distributedutil.CodistParser.gatherIfCodistributed(p);
        if ~isa(A, 'codistributed')
            % Only the p was codistributed.
            s = norm(A, p);
            return;
        end 
    end 
    
    if ~isaUnderlying(A,'float')
        error(message('parallel:distributed:NormNotSupported'));
    end  
    
    if isempty(A)
        s = zeros(1, 1, classUnderlying(A));
        return;
    end 

    if ~iIsValidNorm( p )
        error(message('parallel:distributed:NormInvalidP'));
    end
    
    if isvector(A)
        s = iVectorNorm(A, p);
    else
        s = iMatrixNorm(A, p);
    end
end % End norm

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function s = iVectorNorm(A, p)
    switch p
      case 'fro'
        s = gather(sqrt(sum(abs(A).^2)));
      case {inf, 'inf'} 
        s = gather(max(abs(A)));
      case {-inf, '-inf'}
        s = gather(min(abs(A)));            
      otherwise 
        if isfloat(p) 
            s = gather(nthroot(sum(abs(A).^p), p));
        else
            ME=MException(message(...
                'parallel:distributed:NormVectorNormNotSupport'));
            throwAsCaller(ME)
        end    
    end
    s = full(s);
end 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function s = iMatrixNorm(A, p)
    switch p
      case 1 
        s = gather(max(sum(abs(A),1)));
      case 2
        if issparse(A)
            ME = MException(message( ...
            'parallel:distributed:NormSparseMat2NormUnsupported'));
            throwAsCaller(ME)
        else
            s = gather( max( svd(A) ) );
        end 
      case 'fro'      
        LP = getLocalPart(A);
        codistr = getCodistributor(A);
        s = codistr.hFrobeniusNormImpl(LP);  
      case {inf, 'inf'}
        s = gather(max(sum(abs(A),2)));
      otherwise
        ME = MException(message(...
            'parallel:distributed:NormMatrixNormNotSupport','fro'));
        throwAsCaller(ME)
    end
    s = full(s);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tf = iIsValidNorm( p )
    tf = ( isscalar(p) && isnumeric(p) ) || ischar(p);
end
