function Y = cscd(X)
%CSCD Cosecant of codistributed array in degrees
%   Y = CSCD(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = codistributed.ones(N);
%       E = cscd(D)
%   end
%   
%   See also CSCD, CODISTRIBUTED, CODISTRIBUTED/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:32:26 $

if ~isreal(X)
    error(message('parallel:distributed:CscdComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@cscd, X);
