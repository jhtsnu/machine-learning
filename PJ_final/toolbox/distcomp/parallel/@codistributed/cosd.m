function Y = cosd(X)
%COSD Cosine of codistributed array in degrees
%   Y = COSD(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = codistributed.zeros(N);
%       E = cosd(D)
%   end
%   
%   See also COSD, CODISTRIBUTED, CODISTRIBUTED/ZEROS.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:32:23 $

if ~isreal(X)
    error(message('parallel:distributed:CosdComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@cosd, X);
