function Y = asind(X)
%ASIND Inverse sine of codistributed array, result in degrees
%   Y = ASIND(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = codistributed.ones(N);
%       E = asind(D)
%   end
%   
%   See also ASIND, CODISTRIBUTED, CODISTRIBUTED/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:32:13 $

if ~isreal(X)
    error(message('parallel:distributed:AsindComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@asind, X);

