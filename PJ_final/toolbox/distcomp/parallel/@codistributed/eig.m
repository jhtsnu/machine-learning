function varargout = eig(varargin)
%EIG Eigenvalues and eigenvectors of codistributed array
%   D = EIG(A)
%   [V,D] = EIG(A)
%   [...] = EIG(A,'balance')
%   [...] = EIG(A,B)
%   [...] = EIG(A,B,'chol')
%   
%   A (and B, if present) must be real symmetric or complex Hermitian.
%   
%   The following syntaxes are not supported for full codistributed array:
%   [...] = EIG(A,'nobalance')
%   [...] = EIG(A,B,'qz')
%   
%   Example:
%   spmd
%       N = 1000;
%       A = codistributed.rand(N);
%       A = A+A';  % create a real, symmetric matrix A
%       [V,D] = eig(A);
%       norm(A*V-V*D, 1)  % A*V is within round-off error of V*D 
%   end
%   
%   See also EIG, CODISTRIBUTED, CODISTRIBUTED/RAND.


%   Copyright 2006-2012 The MathWorks, Inc.

    narginchk(1, 3);
    nargoutchk(0, 2);

    distributedutil.CodistParser.verifyNonCodistributedInputs(varargin);

    [numericArgs, flag] = iParseArgs(varargin);
    if any(cellfun(@iIsUnsupported, numericArgs))
        error(message('parallel:distributed:EigNotReal'));
    end

    % are no numeric arguments codistributed?
    if ~any(cellfun(@(x) isa(x, 'codistributed'), numericArgs))
        % flag was codistributed but matrices were not - pass to MATLAB
        [varargout{1:nargout}] = eig(numericArgs{:}, flag);
        return
    end

    A = numericArgs{1};
    if ~isequaln(A, A')
        error(message('parallel:distributed:EigNoNonSymEig'))
    end

    if length(numericArgs) == 1   % standard eig case
        iCheckFlag(flag);
        if isempty(A)
            [varargout{1:nargout}] = iEigEmpty(numericArgs);
        else
            [varargout{1:nargout}] = scalaEig(A);            
        end    
        return
    end

    % generalized eig case
    B = numericArgs{2};
    iCheckGeneralizedFlag(flag);
    usechol = strcmpi(flag, 'chol');

    % check that A & B are the same size
    if ~isequal(size(A), size(B))
        error(message('MATLAB:dimagree'));
    end

    % deal with the case of empty inputs because ScaLAPACK doesn't
    if isempty(A)
        [varargout{1:nargout}] = iEigEmpty(numericArgs);
        return
    end    
    
    % check that B is symmetric and if so do generalized problem
    if usechol && isequaln(B, B')
        [varargout{1:nargout}, isDecompSuccessful] = scalaGeneralizedEig(A, B, usechol);
        if ~isDecompSuccessful
            error(message('parallel:distributed:EigBMustBePosDef'));
        end
    else  % 'qz' code would be called in this case
        error(message('parallel:distributed:EigUnsupportedSyntax'));
    end
end

function [numericArgs, flag] = iParseArgs(argList)
    switch length(argList)
        case 1
            numericArgs = argList;
            flag = 'balance';
        case 2
            if distributedutil.CodistParser.isa(argList{2}, 'char')
                flag = distributedutil.CodistParser.gatherIfCodistributed(argList{2});
                numericArgs = argList(1);
            else
                numericArgs = argList;
                flag = 'chol';
            end
        case 3
            numericArgs = argList(1:2);
            if distributedutil.CodistParser.isa(argList{3}, 'char')
                flag = distributedutil.CodistParser.gatherIfCodistributed(argList{3});
            else
                ME = MException(message('MATLAB:eig:unknownThirdArgument'));
                throwAsCaller(ME);
            end
    end
end

function iCheckFlag(flag)
    if ~any(strcmpi(flag, {'balance', 'nobalance'}))
        ME = MException(message('MATLAB:eig:unknownSecondArgument'));
        throwAsCaller(ME);
    end
    if strcmpi(flag, 'nobalance')
        ME = MException(message('parallel:distributed:EigUnsupportedSyntax'));
        throwAsCaller(ME);
    end
end

function iCheckGeneralizedFlag(flag)
    if ~any(strcmpi(flag, {'chol', 'qz'}))
        ME = MException(message('MATLAB:eig:unknownThirdArgument'));
        throwAsCaller(ME);
    end
    if strcmpi(flag, 'qz')
        ME = MException(message('parallel:distributed:EigUnsupportedSyntax'));
        throwAsCaller(ME);
    end

end

function tf = iIsUnsupported(A)
    tf = ~distributedutil.CodistParser.isa(A, 'float') || ~ismatrix(A) || issparse(A);
end

function varargout = iEigEmpty(numericArgs)
% we got here because all of numericArgs are empty

    isArgCodistributed = cellfun(@(x) isa(x, 'codistributed'), numericArgs);
    firstCodistributedArg = find(isArgCodistributed, 1, 'first');
    firstCodistributedMatrix = numericArgs{firstCodistributedArg};
    codistr = getCodistributor(firstCodistributedMatrix);
    if any(cellfun(@(x) distributedutil.CodistParser.isa(x, 'single'), numericArgs))
        typeArg = 'single';
    else
        typeArg = 'double';
    end
    switch nargout
      case 0
        return
      case 1
        codistr = codistr.hGetNewForSize([0 1]);
        varargout{1} = zeros(0, 1, typeArg, codistr);
      case 2
        codistr = codistr.hGetNewForSize([0 0]);
        varargout{1} = zeros(0, 0, typeArg, codistr);
        varargout{2} = zeros(0, 0, typeArg, codistr);
    end
end
