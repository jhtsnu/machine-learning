function A = mldivide(A, B)
%\ Backslash or left matrix divide for codistributed arrays
%   X = A \ B
%   X = MLDIVIDE(A,B)
%   
%   A and B must be full codistributed arrays of floating point numbers (single or double). 
%   
%   The MATLAB MLDIVIDE function prints a warning message if A is
%   rectangular and rank deficient.  The codistributed MLDIVIDE  
%   method cannot check for this condition; therefore, it is unable 
%   to warn if there is a violation. You should be aware of this 
%   possibility and take action to avoid it.
%   
%   Example:
%   spmd
%       N = 1000;
%       A = codistributed.rand(N);
%       B = codistributed.rand(N,1);
%       X = A \ B;
%       norm(B-A*X, 1)
%   end
%   
%   See also MLDIVIDE, CODISTRIBUTED, CODISTRIBUTED/RAND.


%   Copyright 2006-2012 The MathWorks, Inc.

    narginchk(2, 2);

    distributedutil.CodistParser.verifyNonCodistributedInputs({A, B});

    if isscalar(A) || (isscalar(B) && ~ismatrix(A))
        A = codistributed.pElementwiseBinaryOp(@ldivide, A, B); %#ok<DCUNK>
        return;
    end

    if iIsUnsupported(A) || iIsUnsupported(B)
        error(message('parallel:distributed:MldivideNotSupported'));
    end

    if isa(A, 'codistributed') && (issparse(A) || issparse(B))
        error(message('parallel:distributed:MldivideSparseInput'));
    end

    transposeFirstMatrix = false;
    [A, isNearlySingular] = codistributed.pMldivideAndMrdivide(transposeFirstMatrix,...
                                                      A, B); %#ok<DCUNK>
    if isNearlySingular
        warning(message('parallel:distributed:MldivideNearlySingularMatrix'));
    end
end
%------------------------------------
function tf = iIsUnsupported(D)
    tf = ~distributedutil.CodistParser.isa(D, 'float') || ~ismatrix(D);
end
