function A = mrdivide(A, B)
%/ Slash or right matrix divide for codistributed array
%   C = A / B
%   C = MRDIVIDE(A,B)
%   
%   A and B must be full codistributed arrays of floating point numbers (single or double). 
%   
%   Example:
%   spmd
%       N = 1000;
%       D1 = codistributed.colon(1, N)';
%       D2 = D1 / 2;
%   end
%   
%   See also MRDIVIDE, CODISTRIBUTED, CODISTRIBUTED/COLON, CODISTRIBUTED/ZEROS.


%   Copyright 2006-2012 The MathWorks, Inc.

    narginchk(2, 2);

    distributedutil.CodistParser.verifyNonCodistributedInputs({A, B});

    if isscalar(B) || (isscalar(A) && ~ismatrix(B))
        A = codistributed.pElementwiseBinaryOp(@rdivide, A, B); %#ok<DCUNK>
        return;
    end

    if iIsUnsupported(A) || iIsUnsupported(B)
        error(message('parallel:distributed:MrdivideNotSupported'));
    end

    if isa(A, 'codistributed') && (issparse(A) || issparse(B))
        error(message('parallel:distributed:MrdivideSparseInput'));
    end

    transposeFirstMatrix = true;
    AT = ctranspose(A);
    [AT, isNearlySingular] = codistributed.pMldivideAndMrdivide(transposeFirstMatrix, ...
                                                      B, AT); %#ok<DCUNK>
    A = ctranspose(AT);

    if isNearlySingular
        warning(message('parallel:distributed:MrdivideNearlySingularMatrix'));
    end
end
%------------------------------------
function tf = iIsUnsupported(D)
    tf = ~distributedutil.CodistParser.isa(D, 'float') || ~ismatrix(D);
end
