function Y = acscd(X)
%ACSCD Inverse cosecant of codistributed array, result in degrees
%   Y = ACSCD(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = codistributed.ones(N);
%       E = acscd(D)
%   end
%   
%   See also ACSCD, CODISTRIBUTED, CODISTRIBUTED/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:32:11 $

if ~isreal(X)
    error(message('parallel:distributed:AcscdComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@acscd, X);
