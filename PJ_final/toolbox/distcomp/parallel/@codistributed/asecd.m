function Y = asecd(X)
%ASECD Inverse secant of codistributed array, result in degrees
%   Y = ASECD(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = codistributed.ones(N);
%       E = asecd(D)
%   end
%   
%   See also ASECD, CODISTRIBUTED, CODISTRIBUTED/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:32:12 $

if ~isreal(X)
    error(message('parallel:distributed:AsecdComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@asecd, X);

