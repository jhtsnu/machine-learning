function D = pElementwiseUnaryOp(F, A)
%pElementwiseUnaryOp Perform elementwise unary operations
%   D2 = codistributed.pElementwiseUnaryOp(F, D1) performs the elementwise 
%   unary operation F on all elements of D1.


%   Copyright 2007-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.2 $  $Date: 2010/11/08 02:21:23 $

codistr = getCodistributor(A);
LP = getLocalPart(A);
try
    [LP, codistr] = codistr.hElementwiseUnaryOpImpl(F,  LP);
catch E
    throwAsCaller(E); % Hide the implementation stack.
end

D = codistributed.pDoBuildFromLocalPart(LP, codistr); %#ok<DCUNK>
