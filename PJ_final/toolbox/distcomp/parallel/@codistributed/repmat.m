function A = repmat(A, M, N)
%REPMAT Replicate and tile a codistributed array
%   B = repmat(A,M,N) creates a large codistributed array B consisting of an M-by-N
%   tiling of copies of A. The size of B is [size(A,1)*M, size(A,2)*N].  
%   
%   B = repmat(A,N) creates an N-by-N tiling.
%   B = repmat(A,[M N]) accomplishes the same result as repmat(A,M,N).
%    
%   B = repmat(A,[M N P ...]) tiles the codistributed array A to produce a 
%   multidimensional codistributed array B composed of copies of A. The size of B is 
%   [size(A,1)*M, size(A,2)*N, size(A,3)*P, ...].
%    
%   Class support for input A:
%   float: double, single
%   
%   Example:
%   spmd
%       A = codistributed.rand(10);
%       B = repmat( A, 200, 300 );
%       size(B) % B is a 2000 x 3000 array of tiled copies of A
%   end 
%   
%   See also REPMAT, CODISTRIBUTED, CODISTRIBUTED/RAND.


%   Copyright 2011-2012 The MathWorks, Inc.

    narginchk(2, 3);
    if nargin == 2
        N =[];
    end
    distributedutil.CodistParser.verifyNonCodistributedInputs([{A}, {M}, {N}]);
    className = 'codistributed';
    if isa(A, className)
        codistr = getCodistributor(A);
    elseif isa(M, className)
        codistr = getCodistributor(M);
    elseif isa(N, className)
        codistr = getCodistributor(N);
    end
    zerosFcn = @iGetEmpty;
    catDimOrderingFcn = @codistr.hGetRepmatCatDimensionOrdering;
    A = parallel.internal.array.repmatImpl(A, M, N, className, zerosFcn, ...
        catDimOrderingFcn);
end

function A = iGetEmpty(A, outSz)
    codistrA = getCodistributor(A);
    if ~codistrA.hSupportsDimensionality(length(outSz))
        [maxSz, maxDim] = max(outSz);
        codistrA = codistributor1d(maxDim, ...
            codistributor1d.defaultPartition(maxSz), outSz);
    else
        codistrA = codistrA.hGetNewForSize(outSz);
    end
    A = codistributed.zeros(outSz, classUnderlying(A), codistrA);
end
