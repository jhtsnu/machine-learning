function C = mtimes(A,B)
%* Matrix multiply for codistributed array
%   C = A * B
%   C = MTIMES(A,B)
%   
%   Example:
%   spmd
%       N = 1000;
%       A = codistributed.rand(N)
%       B = codistributed.rand(N)
%       C = A * B
%   end
%   
%   See also MTIMES, CODISTRIBUTED.


%   Copyright 2006-2012 The MathWorks, Inc.

narginchk(2, 2);

distributedutil.CodistParser.verifyNonCodistributedInputs({A, B});

if isscalar(A) || isscalar(B)
    C = times(A, B); 
    return
end

iValidateInputArgs(A, B);

areBothMatricesDense = ~(issparse(A) || issparse(B));

if isa(A, 'codistributed') && isa(B, 'codistributed')
    % Both A and B are codistributed arrays.
    codistrA = getCodistributor(A);
    codistrB = getCodistributor(B);
    if ~strcmpi(class(codistrA), class(codistrB)) 
        % A & B are of different distribution classes
        % Redistribute to the same class before proceeding
        [A, B] = iUnifyCodistributors(A, B, areBothMatricesDense);
    end
    if ~codistrA.hMtimesCheck(areBothMatricesDense)   % Both are distributed in same class
        A = iConvertTo1d(A);
        B = iConvertTo1d(B);
    end
    
    [localA, codistrA]  = iExtractCodistrAndLP(A);
    [localB, codistrB]  = iExtractCodistrAndLP(B);
    [localC, codistrC] = codistrA.hMtimesImpl(codistrA, localA, ...
                                              codistrB, localB);
else % One array is codistributed; the other is replicated
    if ~iIsValidCodistributor(A, areBothMatricesDense)
        A = iConvertTo1d(A);
    end
    
    if ~iIsValidCodistributor(B, areBothMatricesDense)
        B = iConvertTo1d(B);
    end
    
    [localA, codistrA]  = iExtractCodistrAndLP(A);
    [localB, codistrB]  = iExtractCodistrAndLP(B);    
    if ~isempty(codistrA)
        codistr = codistrA;
    else
        codistr = codistrB;
    end
    [localC, codistrC] = codistr.hMtimesReplicatedImpl(codistrA, localA, ...
                                                       codistrB, localB);
end
C = codistributed.pDoBuildFromLocalPart(localC, codistrC); %#ok<DCUNK>

end % End of mtimes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function iValidateInputArgs(A, B)
    if ~distributedutil.CodistParser.isa(A, 'float') || ...
            ~distributedutil.CodistParser.isa(B, 'float')
        ME = MException(message('parallel:distributed:MtimesInputsMustBeFloat'));
        throwAsCaller(ME);
    end
    
    if ~ismatrix(A) || ~ismatrix(B)
        ME = MException(message('parallel:distributed:MtimesInputsMustBe2D'));
        throwAsCaller(ME);
    end
    
    if size(A, 2) ~= size(B, 1)
        ME = MException(message('parallel:distributed:MtimesInnerdim'));
        throwAsCaller(ME);
    end
end

function [A, B] = iUnifyCodistributors(A, B, areBothMatricesDense)
    codistrA = getCodistributor(A);
    codistrB = getCodistributor(B);
    if numel(A) > numel(B)
        if codistrA.hMtimesCheck(areBothMatricesDense)
            codistrB = codistrA.hGetNewForSize( size(B) );
            B = redistribute(B, codistrB);
        else
            if codistrB.hMtimesCheck(areBothMatricesDense)
                codistrA = codistrB.hGetNewForSize( size(A) );
                A = redistribute(A, codistrA);
            else
                A = iConvertTo1d(A);
                B = iConvertTo1d(B);
            end
        end
    else
        if codistrB.hMtimesCheck(areBothMatricesDense)
            codistrA = codistrB.hGetNewForSize( size(A) );
            A = redistribute(A, codistrA);
        else
            if codistrA.hMtimesCheck(areBothMatricesDense)
                codistrB = codistrA.hGetNewForSize( size(B) );
                B = redistribute(B, codistrB);
            else
                A = iConvertTo1d(A);
                B = iConvertTo1d(B);
            end  
        end
    end
end

function A = iConvertTo1d(A)
% To be called if hMtimesCheck fails for the distributions of an input
    [~, dim] = max(size(A));
    A = redistribute(A, codistributor1d(dim));
end

function [LP, codistr] = iExtractCodistrAndLP(D)
    if isa(D, 'codistributed')
        % Get codistributor and local part from codistributed array.
        LP = getLocalPart(D);
        codistr = getCodistributor(D);
    else
        % Let empty codistributor indicate that local part represents a
        % replicated array.
        LP = D;
        codistr = [];
    end
end

function tf = iIsValidCodistributor(X, areBothMatricesDense)
    if isa(X, 'codistributed')
        tf = hMtimesReplicatedCheck( getCodistributor(X), areBothMatricesDense );
    else
        % replicated array has a "valid" codistributor
        tf = true;
    end
end
