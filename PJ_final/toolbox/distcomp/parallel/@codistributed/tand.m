function Y = tand(X)
%TAND Tangent of codistributed array in degrees
%   Y = TAND(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = 45*codistributed.ones(N);
%       E = tand(D)
%   end
%   
%   See also TAND, CODISTRIBUTED, CODISTRIBUTED/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:33:13 $

if ~isreal(X)
    error(message('parallel:distributed:TandComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@tand, X);
