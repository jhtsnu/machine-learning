function [A, isNearlySingular] = pMldivideAndMrdivide(transposeFirstMatrix, A, B)
%pMldivideAndMrdivide is a common implementation of mldivide and mrdivide
%   for (co)distributed matrices
%   


%   Copyright 2012 The MathWorks, Inc.

    isNearlySingular = false;
    if isa(A, 'codistributed')
        if size(A, 1) == size(A, 2) % square
                                    % Check whether A is triangular and use the most efficient solver
            aDist = getCodistributor(A);
            matrixType = aDist.hIsTriangularImpl(getLocalPart(A));
            if strcmp(matrixType, 'NotTriangular')
                [A, rCond] = scalaLUsolve(transposeFirstMatrix, A, B);
            elseif any(strncmpi(matrixType, {'LowerTriangular', 'UpperTriangular'}, 1))
                [A, rCond] = scalaTrisolve(transposeFirstMatrix, A, B, matrixType);
            else  % Diagonal or zero
                [A, rCond] = scalaTrisolve(transposeFirstMatrix, A, B, 'LowerTriangular');
            end
            isNearlySingular = isnan(rCond) || (rCond < eps(class(rCond)));
        else  % not square
            if transposeFirstMatrix
                A = scalaQRsolve(ctranspose(A), B);
            else
                A = scalaQRsolve(A, B);
            end
        end
    else
        bDist = getCodistributor(B);
        if ~( isa(bDist, 'codistributor1d') && (bDist.Dimension == 2) )
            B = redistribute(B, codistributor('1d', 2, ...
                                              codistributor1d.defaultPartition(size(B, 2))));
            bDist = getCodistributor(B);
        end
        if transposeFirstMatrix
            A = ctranspose(A);
        end
        % Since A is not codistributed and B is distributed by columns, the
        % labs all have different linear systems to solve.  A new codistributor
        % will be needed if A is not square.  In any case, the results of the
        % divide operation will have the same column distribution as B, and
        % the number of rows will be the number of columns in the input A
        aDist = codistributor('1d', 2, bDist.Partition, [size(A, 2) size(B, 2)]);
        A = codistributed.pDoBuildFromLocalPart(A\getLocalPart(B), aDist); %#ok<DCUNK>
    end
end
