function Y = atand(X)
%ATAND Inverse tangent of codistributed array, result in degrees
%   Y = ATAND(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = codistributed.ones(N);
%       E = atand(D)
%   end
%   
%   See also ATAND, CODISTRIBUTED, CODISTRIBUTED/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:32:14 $

if ~isreal(X)
    error(message('parallel:distributed:AtandComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@atand, X);
