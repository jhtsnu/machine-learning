function Y = sind(X)
%SIND Sine of codistributed array in degrees
%   Y = SIND(X)
%   
%   Example:
%   spmd
%       N = 1000;
%       D = pi/2*codistributed.ones(N);
%       E = sind(D)
%   end
%   
%   See also SIND, CODISTRIBUTED, CODISTRIBUTED/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2011/04/27 17:33:02 $

if ~isreal(X)
    error(message('parallel:distributed:SindComplexInput'));
end

Y = codistributed.pElementwiseUnaryOp(@sind, X);
