function [Y, I] = minmaxop(fcnMinMax,varargin)
%minmaxop    template for max and min

%   Copyright 2006-2012 The MathWorks, Inc.

distributedutil.CodistParser.verifyNonCodistributedInputs(varargin);

try
    if nargin == 2 || nargin == 4
        % Handle fMinMax(X) and fMinMax(X, [], dim).
        X = varargin{1};
        if nargin == 2
            dim = distributedutil.Sizes.firstNonSingletonDimension(size(X));
        else
            if ~isempty(varargin{2})
                % This is the same error message as with builtin min and max.
                % For example, min(3, 4, 2).
                error(message('parallel:distributed:MinMaxCaseNotSupported', upper( func2str( fcnMinMax ) )));
            end
            dim =  distributedutil.CodistParser.gatherIfCodistributed(varargin{3});
            if ~isa(X, 'codistributed')
                if nargout > 1
                    [Y, I] = fcnMinMax(X, [], dim);
                else
                    Y = fcnMinMax(X, [], dim); 
                end
                return;
            end
        end
        wantI = nargout > 1;
        [Y, I] = iMinMaxAlongDim(fcnMinMax, X, dim, wantI);
    elseif nargin == 3
        % Handle fMinMax(X, Y), i.e. elementwise comparison between X and Y.
        if nargout > 1
            error(message('parallel:distributed:MinMaxMaxlhs'));
        end
        X = varargin{1};
        Z = varargin{2};
        Y = codistributed.pElementwiseBinaryOp(fcnMinMax, X, Z); %#ok<DCUNK> private method.
    else
        error(message('parallel:distributed:MinMaxMaxrhs'));
    end
catch E
    % Error stack should only show min or max, not minmaxop.
    throwAsCaller(E);
end

end % End of minmaxop.

function [Y, I] = iMinMaxAlongDim(fcnMinMax, X, dim, wantI)
I = [];
if any(size(X, dim) == [0, 1])
    % min/max on a singleton dimension or an empty dimension is a no-op.
    % This is completely different from any/all/sum/prod.
    Y = X;
    if wantI
        I = codistributed.ones(size(X), getCodistributor(X), 'noCommunication');
    end
    return;
end
% Defer to implementation of non-trivial min-max.
codistr = getCodistributor(X);
LP = getLocalPart(X);
[LPY, LPI, codistr] = codistr.hMinMaxImpl(fcnMinMax, LP, dim, wantI);
Y = codistributed.pDoBuildFromLocalPart(LPY, codistr);  %#ok<DCUNK> private static.
if wantI
    I = codistributed.pDoBuildFromLocalPart(LPI, codistr);  %#ok<DCUNK> private static.
end

end % End of iMinMaxAlongDim.
