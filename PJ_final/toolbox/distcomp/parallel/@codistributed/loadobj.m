function A = loadobj(A)
;%#ok

%LOADOBJ Overloaded for codistributed arrays
%   
%   See also LOADOBJ, CODISTRIBUTED.


%   Copyright 2008-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.4 $  $Date: 2011/04/27 17:32:37 $

aDist = getCodistributor(A);
storedNumLabs = aDist.hNumLabs();
if storedNumLabs ~= numlabs
    warning(message('parallel:distributed:InvalidNumberOfLabs', storedNumLabs, numlabs));
end
