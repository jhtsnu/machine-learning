function B = permute(A, order) %#ok<STOUT,INUSD>
%PERMUTE Permute codistributed array dimensions
%    Not yet implemented.


%   Copyright 2008-2011 The MathWorks, Inc.
% $Revision: 1.1.6.3 $   $Date: 2011/04/27 17:32:57 $

error(message('parallel:distributed:PermuteNotImplemented'));
