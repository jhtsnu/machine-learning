function C = mat2cell(A,varargin) %#ok<STOUT,INUSD>
%MAT2CELL Break codistributed matrix up into a codistributed cell array of underlying data.
%   Not yet implemented.
%   


%   Copyright 2009-2011 The MathWorks, Inc.
% $Revision: 1.1.6.2 $   $Date: 2011/04/27 17:32:40 $

error(message('parallel:distributed:Mat2cellNotImplemented'));
