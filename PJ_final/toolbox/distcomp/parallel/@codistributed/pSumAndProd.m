function R = pSumAndProd(fcn, A, argList)
%pSumAndProd is a common implementation of sum and prod for (co)distributed 
%   matrices
%   


%   Copyright 2012 The MathWorks, Inc.
    distributedutil.CodistParser.verifyNonCodistributedInputs({A, argList});
    argList = distributedutil.CodistParser.gatherElements(argList);
    if ~isa(A, 'codistributed')
        % Dimension or accumType are codistributed, but not the array itself.
        try
            R = fcn(A, argList{:});
            return;
        catch E
            throwAsCaller(E);
        end
    end

    % Unlike the other reduction functions (any, all), sum and prod take a 3rd
    % optional argument, the accumulative type.  We peel that off and hide it
    % from our generic reduction processing.
    if length(argList) == 2 || (length(argList) == 1 && ischar(argList{1}))
        accumType = argList{end};
        argList(end) = [];
    else
        % Use default accumulation type.
        if strcmp(classUnderlying(A),'single')
            accumType = 'native';
        else
            accumType = 'double';
        end
    end
    % Forward accumType to sum or prod, then reduce.
    accumFcn = @(varargin) fcn(varargin{:}, accumType);

    try
        R = codistributed.pReductionOpAlongDim(accumFcn, A, argList{:});
    catch E
        throwAsCaller(E); % Strip ourselves from the stack.
    end
end 
