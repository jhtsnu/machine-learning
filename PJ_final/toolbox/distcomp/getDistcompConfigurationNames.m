function names = getDistcompConfigurationNames
%getDistcompConfigurationNames get the names of all configurations
%
% names = getDistcompConfigurationNames returns a cell array of string that are
% all the currently defined configurations.
%
% Example:
%     % Get all current configurations.
%     names = getDistcompConfigurationNames;
%

%  Copyright 2006-2012 The MathWorks, Inc.


import parallel.internal.settings.ConfigurationsHelper;
ser = ConfigurationsHelper.getCurrentSerializer();
names = ser.getAllNames();

