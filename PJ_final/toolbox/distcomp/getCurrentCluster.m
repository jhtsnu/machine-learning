function currentCluster = getCurrentCluster
%getCurrentCluster Get cluster object that submitted current task
%    myCluster = getCurrentCluster returns the cluster object that has
%    sent the task currently being evaluated by the worker session.
%
%    If the function is executed in a MATLAB session that is not a worker,
%    you get an empty result.
%
%    Example:
%    % Find the current cluster 
%    myCluster = getCurrentCluster;
%    % Get the host on which the cluster is running
%    host = myCluster.Host;
%
% See also parcluster, getCurrentWorker, getCurrentJob, getCurrentTask.

%  Copyright 2000-2012 The MathWorks, Inc.

try
    root = distcomp.getdistcompobjectroot;
    currentCluster = root.CurrentJobmanager;
catch E %#ok<NASGU>
    warning(message('parallel:cluster:UnexpectedFailureToGetCluster'));
    currentCluster = [];
end
