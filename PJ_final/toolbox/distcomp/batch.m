function job = batch(scriptName, varargin)
%BATCH Run MATLAB function or script on worker
%   j = BATCH('aScript') runs the script aScript.m on a worker according to
%   the cluster defined in the default parallel profile.  The function
%   returns j, a handle to the job object that runs the script. The script
%   file aScript.m is added to the AttachedFiles and copied to the worker.
%
%   j = BATCH(myCluster, 'aScript') is identical to BATCH('aScript') except
%   that the script runs on a worker using the cluster identified by the
%   object myCluster.
%
%   j = BATCH(fcn, N, {x1,..., xn}) runs the function specified by a
%   function handle or function name, fcn, on a worker according to the
%   cluster defined in the default parallel profile.  The function returns
%   j, a handle to the job object that runs the function. The function is
%   evaluated with the given arguments, x1,...,xn, returning N output
%   arguments.  The function file for fcn is added to the AttachedFiles and
%   copied to the worker.
%
%   j = BATCH(myCluster, fcn, N, {x1,..., xn}) is identical to BATCH(fcn,
%   N, {x1,..., xn}) except that the function runs on a worker using the
%   cluster identified by the object myCluster.
%
%   j = BATCH( ..., P1, V1, ..., Pn, Vn) allows additional parameter-value
%   pairs that modify the behavior of the job.  These parameters support
%   batch for functions and scripts, unless otherwise indicated.  The
%   accepted parameters are:
%
%   - 'Workspace' - A 1-by-1 struct to define the workspace on the worker
%     just before the script is called. The field names of the struct
%     define the names of the variables, and the field values are assigned
%     to the workspace variables. By default this parameter has a field for
%     every variable in the current workspace where batch is executed. This
%     parameter can only be used with scripts.
%
%   - 'Profile' - A single string that is the name of a parallel profile to
%     use to identify the cluster.  If this option is omitted, the default
%     profile is used to identify the cluster and is applied to the job and
%     task properties.
%
%   - 'AdditionalPaths' - A string or cell array of strings that defines
%     paths to be added to the workers' MATLAB path before the script or
%     function is executed.
%
%   - 'AttachedFiles' - A string or cell array of strings.  Each string
%     in the list identifies either a file or a folder, which is
%     transferred to the worker. If specified as a string then the list is
%     space delimited. In addition, the script being run is always added to
%     the list of files sent to the worker.
%
%   - 'CurrentFolder' - A string to indicate in what folder the script
%     executes. There is no guarantee that this folder exists on the
%     worker. The default value for this property is the current folder of
%     MATLAB when the batch command is executed. If the string for this
%     argument is '.', there is no change in folder before batch execution.
%
%   - 'CaptureDiary' - A boolean flag to indicate that diary output should
%     be retrieved from the script execution or function call.  See the
%     DIARY function for how to return this information to the client.  The
%     default is true.
%
%   - 'Matlabpool' - A nonnegative scalar integer that defines the number 
%     of additional workers to make into a MATLAB pool for the job to run
%     on. This value will override the NumWorkersRange specified in the
%     profile.  A value of N for the property Matlabpool is equivalent to
%     adding a call to matlabpool N into the script or function. The
%     default is 0, which causes the script or function to run on only the
%     single worker without a MATLAB pool.
%
%   Examples:
%   % Run a batch script on a worker:
%   j = batch('script1');
%
%   % Run a batch script, capturing the diary, adding a path to the workers
%   % and transferring some required files
%   j = batch('script1', ...
%             'AdditionalPaths', '\\Shared\Project1\HelperFiles',...
%             'AttachedFiles', 'script1helper1 script1helper2');
%   % Wait for the job to finish
%   wait(j)
%   % Display the diary
%   diary(j)
%   % Get the results of running the script in this workspace
%   load(j)
%
%   % Run a batch job on a remote cluster using a pool of 8 workers:
%   j = batch('script1', 'matlabpool', 8);
%
%   % Run a batch job on a local worker, which employs two other
%   % local workers:
%   j = batch('script1', 'Profile', 'local', 'matlabpool', 2);
%
%   % Run a batch function on a remote cluster defined by the profile
%   % myProfile that generates a 10-by-10 random matrix
%   myCluster = parcluster('myProfile');
%   j = batch(myCluster, @rand, 1, {10, 10});
%   % Wait for the job to finish
%   wait(j)
%   % Display the diary
%   diary(j)
%   % Get the results of running the job into a cell array
%   r = fetchOutputs(j)
%   % Get the generated random number from r
%   r{1}
%
%   % Run some batch scripts and then use findJob to retrieve the jobs.
%   batch(myCluster, 'script1');
%   batch(myCluster, 'script2');
%   batch(myCluster, 'script3');
%   myBatchJobs = findJob(myCluster, 'Username', 'myUsername')
%
%   See also parallel.Job/wait, parallel.Job/load, parallel.Job/diary,
%            parallel.Cluster/batch, parallel.Cluster/Jobs, 
%            parallel.Cluster/findJob, parcluster.

%   Copyright 2007-2012 The MathWorks, Inc.

import parallel.internal.apishared.ProfileConfigHelper

% Ensure that we have between 1 and Inf input arguments
% (must have a script name), but don't use standard nargchk so users don't
% get confused between input arguments to batch and nargin/nargout
% required by their function handle.
parallel.internal.cluster.checkNumberOfArguments('input', 1, inf, nargin, mfilename);

[~, undoc] = pctconfig();
if undoc.convenienceapi == 1
    constructor  = @parallel.internal.cluster.BatchHelper;
    pch          = ProfileConfigHelper.buildApi1();
else
    constructor  = @parallel.internal.cluster.BatchHelper2;
    pch          = ProfileConfigHelper.buildApi2();
end

% Parse the inputs
try
    batchHelper = constructor(pch, scriptName, varargin);
catch err
    % Make all errors appear from batch
    throw(err);
end

% Deal with the Workspace and configuration.  Set the WorkspaceIn to the
% caller if one wasn't supplied.  Note that this code has to exist here
% because multiple evalin calls cannot be nested.  This code can't even be
% put into a script, because evalin('caller') from the script just gets
% this function's workspace and not the caller of this function.  If you
% change this code, make sure you change the versions in @jobmanager/batch.m
% and parallel.Cluster.batch as well.
if batchHelper.needsCallerWorkspace
    where = 'caller';
    % No workspace supplied - we need to make our own from the calling workspace
    vars = evalin(where, 'whos');
    % Loop over each variable in the calling workspace to get its value
    numVars = numel(vars);
    varNames = cell(numVars, 1);
    varValues = cell(numVars, 1);
    for ii = 1:numVars
        varNames{ii} = vars(ii).name;
        varValues{ii} = evalin(where, vars(ii).name);
    end
    batchHelper.filterAndSetCallerWorkspace(varNames, varValues);
end

% Configuration
if isempty(batchHelper.Configuration)
    batchHelper.Configuration = pch.getDefaultName();
end

try
    % Get the scheduler to use on this job
    scheduler = pch.buildScheduler(batchHelper.Configuration);
    % Actually run batch on the scheduler
    job = batchHelper.doBatch(scheduler);
catch err
    % Make all errors appear from batch
    throw(err);
end
