function currentTask = getCurrentTask
%getCurrentTask Get task object currently being evaluated in this worker session
%
%    task = getCurrentTask returns the task object that is currently being
%    evaluated by the worker session.
%
%    If the function is executed in a MATLAB session that is not a worker,
%    you get an empty result.
%
%    Example:
%    % Find the current task
%    task = getCurrentTask;
%    % Get task ID to find its rank 
%    id = get(task, 'ID');
%
%    See also findResource, getCurrentJobmanager, getCurrentWorker, getCurrentJob

%  Copyright 2000-2012 The MathWorks, Inc.

try
    root = distcomp.getdistcompobjectroot;
    currentTask = root.CurrentTask;
catch err %#ok<NASGU>
    warning(message('parallel:cluster:GetCurrentTaskFailed'));
    currentTask = [];
end
