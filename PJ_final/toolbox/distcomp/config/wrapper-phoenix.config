#********************************************************************
#
# Wrapper config file for starting the phoenix RMI daemon
#
#   Service Name : phoenixd
#
# Prerequisites for this wrapper config to work are that the following
# environment variables have been set or will be set in the environment
# specific include files listed below :
#
#   JRECMD		 	The location of the java executable to call
#
#   JREBASE      	The location of the jre that will be used
#   MATBASE      	The MATLABROOT directory
#   MDCEBASE     	The DISTCOMP toolbox directory
#   LOGBASE      	The directory to log the service stdout
#   CHECKPOINTBASE	The directory to CHECKPOINT the service 
#
#   ARCH		The architecture we are running on - defines the location of wrapper library
#   HOSTNAME		The name of the host this service is running on
#
#   WORKER_START_TIMEOUT
#
#   JOB_MANAGER_HOST
#
#   BASE_PORT
#
#   The following are used when phoenix creates the service descriptor for 
#   a job manager.
#   DEFAULT_JOB_MANAGER_NAME 
#   MDCEQE_JOBMANAGER_DEBUG_PORT
#
#   The following are used when phoenix creates the service descriptor for 
#   a worker.
#   MATLAB_EXECUTABLE 
#   DEFAULT_WORKER_NAME
#
#   The following are used for security purposes.
#   WORKER_DOMAIN (Windows only)
#   SECURITY_LEVEL
#   SECURITY_DIR
#   USE_SECURE_COMMUNICATION
#   SHARED_SECRET_FILE 
#   DEFAULT_KEYSTORE_PATH
#   KEYSTORE_PASSWORD
#   MDCE_ALLOW_GLOBAL_PASSWORDLESS_LOGON
#   ALLOW_CLIENT_PASSWORD_CACHE
#   ADMIN_USER
#   ALLOWED_USERS
#
#   The following are used for on-demand operation
#   RELEASE_LICENSE_WHEN_IDLE
#
# Copyright 2004-2012 The MathWorks, Inc.
#********************************************************************

# Including the following file as defined by an environment variable
# provides a way for us to set environment variables in NT. The
# problem is that we wish a call to service start to pick up any
# changes to mdce_def.bat. To do this we actually write the
# requested environment variables to %MDCE_CONFIG_FILE%
# and simply set the variable MDCE_CONFIG_FILE in the call to wrapper.
# This allows all the required variables to be set by the scripts
# and picked up in this config. To write this include we use
# MATLABROOT/toolbox/distcomp/bin/win32/writeconfig.bat
# Currently this facility is NOT used on unix but could be. Also
# note that on windows it is expected that this file will set
# the variable %MDCE_PLATFORM_WRAPPER_CONF% which will be used
# in the next line to source platform specific behaviour
#include %MDCE_CONFIG_FILE%

# Include the platform specific wrapper configuration file from the
# configuration directory. This environment variable should be set to 
# something like %MDCEBASE%/config/wrapper-phoenix-$ARCH.config by the 
# setup scripts or the config file
#include %MDCE_PLATFORM_WRAPPER_CONF%

# Java Application
wrapper.java.command=%JRECMD_FOR_MDCS%

# All parameters that might have spaces in them must be in double quotes,
# and wrapper.java.additional.X.stripquotes must then also be set to TRUE.
wrapper.java.additional.1=-Dcom.mathworks.toolbox.distcomp.base_port=%BASE_PORT%

wrapper.java.additional.2="-Dcom.mathworks.toolbox.distcomp.matlabroot=%MATBASE%"
wrapper.java.additional.2.stripquotes=TRUE

wrapper.java.additional.3="-Dcom.mathworks.toolbox.distcomp.toolboxroot=%MDCEBASE%"
wrapper.java.additional.3.stripquotes=TRUE

wrapper.java.additional.4="-Dcom.mathworks.toolbox.distcomp.checkpointdir=%CHECKPOINTBASE%"
wrapper.java.additional.4.stripquotes=TRUE

wrapper.java.additional.5="-Dcom.mathworks.toolbox.distcomp.configbase=%CONFIGBASE%"
wrapper.java.additional.5.stripquotes=TRUE

wrapper.java.additional.6="-Dcom.mathworks.toolbox.distcomp.mdceDefFile=%MDCE_DEFFILE%"
wrapper.java.additional.6.stripquotes=TRUE

# Logging
wrapper.java.additional.7="-Dcom.mathworks.toolbox.distcomp.logdir=%LOGBASE%"
wrapper.java.additional.7.stripquotes=TRUE
wrapper.java.additional.8=-Dcom.mathworks.toolbox.distcomp.loglevel=%LOG_LEVEL%

# Security policy for phoenix
wrapper.java.additional.9="-Djava.security.policy=%MDCEBASE%/config/jsk-all.policy"
wrapper.java.additional.9.stripquotes=TRUE

# Use urandom as source of entropy
wrapper.java.additional.10=-Djava.security.egd=file:/dev/urandom

# Hostname
wrapper.java.additional.11="-Dcom.mathworks.toolbox.distcomp.hostname=%HOSTNAME%"
wrapper.java.additional.11.stripquotes=TRUE

wrapper.java.additional.12="-Djava.rmi.server.hostname=%HOSTNAME%"
wrapper.java.additional.12.stripquotes=TRUE

# Job manager
wrapper.java.additional.13="-Dcom.mathworks.toolbox.distcomp.default_jobmanager_name=%DEFAULT_JOB_MANAGER_NAME%"
wrapper.java.additional.13.stripquotes=TRUE
wrapper.java.additional.14=-Dcom.mathworks.toolbox.distcomp.job_manager_maximum_memory=%JOB_MANAGER_MAXIMUM_MEMORY%
wrapper.java.additional.15="-Dcom.mathworks.toolbox.distcomp.lookup_hosts=%JOB_MANAGER_HOST%"
wrapper.java.additional.15.stripquotes=TRUE
wrapper.java.additional.16=-Dcom.mathworks.toolbox.distcomp.debug_jobmanager_port=%MDCEQE_JOBMANAGER_DEBUG_PORT%
wrapper.java.additional.17=-Dcom.mathworks.toolbox.distcomp.jobmanager_gcInterval=10000

# Workers
wrapper.java.additional.18="-Dcom.mathworks.toolbox.distcomp.matlabexecutable=%MATLAB_EXECUTABLE%"
wrapper.java.additional.18.stripquotes=TRUE
wrapper.java.additional.19=-Dcom.mathworks.toolbox.distcomp.workerstarttimeout=%WORKER_START_TIMEOUT%
wrapper.java.additional.20="-Dcom.mathworks.toolbox.distcomp.default_worker_name=%DEFAULT_WORKER_NAME%"
wrapper.java.additional.20.stripquotes=TRUE

# Jini/RMI settings used by the services
wrapper.java.additional.21=-Dcom.mathworks.toolbox.distcomp.membergroups=default_group
wrapper.java.additional.22=-Dcom.mathworks.toolbox.distcomp.RMI_readTimeout=300000
wrapper.java.additional.23=-Dcom.mathworks.toolbox.distcomp.DNS_lookupInterval=300
wrapper.java.additional.24=-Dcom.mathworks.toolbox.distcomp.RMI_connectionTimeout=10000

# This is the java.library.path used by the services
wrapper.java.additional.25="-Dcom.mathworks.toolbox.distcomp.library_path=%MATBASE%/bin/%ARCH%"
wrapper.java.additional.25.stripquotes=TRUE

# The JRE flags passed to services 
wrapper.java.additional.26="-Dcom.mathworks.toolbox.distcomp.jreflags=%JREFLAGS%"
wrapper.java.additional.26.stripquotes=TRUE

# Security Level settings
wrapper.java.additional.27=-Dcom.mathworks.toolbox.distcomp.securityLevel=%SECURITY_LEVEL%
wrapper.java.additional.28="-Dcom.mathworks.toolbox.distcomp.securityDir=%SECURITY_DIR%"
wrapper.java.additional.28.stripquotes=TRUE
wrapper.java.additional.29=-Dcom.mathworks.toolbox.distcomp.rmi.useSecureCommunication=%USE_SECURE_COMMUNICATION%
wrapper.java.additional.30=-Dcom.mathworks.toolbox.distcomp.rmi.clientsAreTrusted=%TRUSTED_CLIENTS%
wrapper.java.additional.31="-Dcom.mathworks.toolbox.distcomp.mjs.security.keystorePath=%SHARED_SECRET_FILE%"
wrapper.java.additional.31.stripquotes=TRUE
wrapper.java.additional.32="-Dcom.mathworks.toolbox.distcomp.mjs.security.defaultKeystorePath=%DEFAULT_KEYSTORE_PATH%"
wrapper.java.additional.32.stripquotes=TRUE
wrapper.java.additional.33=-Dcom.mathworks.toolbox.distcomp.mjs.security.keystorePassword=%KEYSTORE_PASSWORD%
wrapper.java.additional.34=-Dcom.mathworks.toolbox.distcomp.auth.mdceAllowGlobalPasswordlessLogon=%MDCE_ALLOW_GLOBAL_PASSWORDLESS_LOGON%
wrapper.java.additional.35=-Dcom.mathworks.toolbox.distcomp.auth.allowClientPasswordCache=%ALLOW_CLIENT_PASSWORD_CACHE%
wrapper.java.additional.36=-Dcom.mathworks.toolbox.distcomp.auth.adminUser=%ADMIN_USER%
wrapper.java.additional.37="-Dcom.mathworks.toolbox.distcomp.auth.allowedUsers=%ALLOWED_USERS%"
wrapper.java.additional.37.stripquotes=TRUE
wrapper.java.additional.38=-Dcom.mathworks.toolbox.distcomp.worker.windowsDomain=%WORKER_DOMAIN%

# Configure the jobmanager ports
wrapper.java.additional.39=-Dcom.mathworks.toolbox.distcomp.allServerSocketsInCluster=%MDCS_ALL_SERVER_SOCKETS_IN_CLUSTER%
wrapper.java.additional.40=-Dcom.mathworks.toolbox.distcomp.peerSessionPort=%MDCS_JOBMANAGER_PEERSESSION_PORT%
wrapper.java.additional.41=-Dcom.mathworks.toolbox.distcomp.workerMatlabPoolMinPort=%MDCS_WORKER_MATLABPOOL_MIN_PORT%
wrapper.java.additional.42=-Dcom.mathworks.toolbox.distcomp.workerMatlabPoolMaxPort=%MDCS_WORKER_MATLABPOOL_MAX_PORT%

# Configure the lifecycle reporter and heartbeat intervals
wrapper.java.additional.43=-Dcom.mathworks.toolbox.distcomp.worker.lifecycleReporter=%MDCS_LIFECYCLE_REPORTER%
wrapper.java.additional.44=-Dcom.mathworks.toolbox.distcomp.worker.workerLifecycleHeartBeat=%MDCS_LIFECYCLE_WORKER_HEARTBEAT%
wrapper.java.additional.45=-Dcom.mathworks.toolbox.distcomp.worker.taskLifecycleHeartBeat=%MDCS_LIFECYCLE_TASK_HEARTBEAT%

# Additional jar files to add to classpath
wrapper.java.additional.46="-Dcom.mathworks.toolbox.distcomp.additionalClasspath=%MDCS_ADDITIONAL_CLASSPATH%"
wrapper.java.additional.46.stripquotes=TRUE

# Peer Lookup Service configuration
wrapper.java.additional.47=-Dcom.mathworks.toolbox.distcomp.mjs.peerlookupservice.enabled=%MDCS_PEER_LOOKUP_SERVICE_ENABLED%
wrapper.java.additional.48=-Dcom.mathworks.toolbox.distcomp.mjs.peerlookupservice.port=%MDCS_PEER_LOOKUP_SERVICE_PORT%

# On demand flags
wrapper.java.additional.49=-Dcom.mathworks.toolbox.distcomp.worker.onDemand=%RELEASE_LICENSE_WHEN_IDLE%

# Web licensing
wrapper.java.additional.50=-Dcom.mathworks.toolbox.distcomp.requireWebLicensing=%MDCS_REQUIRE_WEB_LICENSING%

wrapper.java.additional.51="-Dcom.mathworks.toolbox.distcomp.jrecmd=%JRECMD_FOR_MDCS%"
wrapper.java.additional.51.stripquotes=TRUE

# Limit the GC threads
wrapper.java.additional.52=-XX:ParallelGCThreads=6

# If you add more properties to the list above, you must also update 
# the numbers on the properties used in wrapper-phoenix-ARCH.config.


# Java Main class.  This class must implement the WrapperListener interface
#  or guarantee that the WrapperManager class is initialized.  Helper
#  classes are provided to do this for you.  See the Integration section
#  of the documentation for details.
wrapper.java.mainclass=org.tanukisoftware.wrapper.WrapperSimpleApp

# Application parameters.  Add parameters as needed starting from 1
wrapper.app.parameter.1=com.mathworks.toolbox.distcomp.control.Starter
wrapper.app.parameter.2="%MDCEBASE%/config/start-phoenix.config"
wrapper.app.parameter.2.stripquotes=TRUE

# Java Classpath (include wrapper.jar)  Add class path elements as
# needed starting from 1.
# Remember that phoenix creates the service descriptors for the worker and 
# the job manager, which is the reason for a few of these libraries.
wrapper.java.classpath.1=%JAREXTBASE%/wrapper.jar
wrapper.java.classpath.2=%JINILIB%/start.jar
wrapper.java.classpath.3=%JINILIB%/destroy.jar
wrapper.java.classpath.4=%JINILIB%/phoenix.jar
wrapper.java.classpath.5=%JARBASE%/parallel/pctutil.jar
wrapper.java.classpath.6=%JARBASE%/distcomp.jar
wrapper.java.classpath.7=%JINILIB%/reggie.jar
wrapper.java.classpath.8=%JINILIB%/jini-ext.jar
wrapper.java.classpath.9=%JINILIB%/group.jar
wrapper.java.classpath.10=%JINILIB%/phoenix-init.jar
wrapper.java.classpath.11=%MATBASE%/java/jar/util.jar
wrapper.java.classpath.12=%JARBASE%/parallel/admincenter.jar

# Java Library Path (location of Wrapper.DLL or libwrapper.so)
wrapper.java.library.path.1=%MDCEBASE%/bin/%ARCH%
wrapper.java.library.path.2=%MATBASE%/bin/%ARCH%

# Initial Java Heap Size (in MB)
wrapper.java.initmemory=3

# Maximum Java Heap Size (in MB)
wrapper.java.maxmemory=64

#********************************************************************
# Wrapper Logging Properties
#********************************************************************
# Format of output for the console.  (See docs for formats)
wrapper.console.format=PM

# Log Level for console output.  (See docs for log levels)
wrapper.console.loglevel=INFO

# Log file to use for wrapper output logging.
wrapper.logfile=%LOGBASE%/mdce-service.log

# File to hold the pid of the wrapper process. This (in reality)
# simply proves that the wrapper process can write a file in the
# checkpoint directory, getting round filesystem issues where we
# don't have write access to this directory.
wrapper.pidfile=%CHECKPOINTBASE%/mdce_writetest.pid

# Format of output for the log file.  (See docs for formats)
wrapper.logfile.format=LPTM

# Log Level for log file output.  (See docs for log levels)
wrapper.logfile.loglevel=INFO

# Maximum size that the log file will be allowed to grow to before
#  the log is rolled. Size is specified in bytes.  The default value
#  of 0, disables log rolling.  May abbreviate with the 'k' (kb) or
#  'm' (mb) suffix.  For example: 10m = 10 megabytes.
wrapper.logfile.maxsize=2500k

# Maximum number of rolled log files which will be allowed before old
#  files are deleted.  The default value of 0 implies no limit.
wrapper.logfile.maxfiles=4

# Log Level for sys/event log output.  (See docs for log levels)
wrapper.syslog.loglevel=NONE

# If false, the wrapper creates a background thread which enters a light weight
# loop and increments an internal "tick" counter. This is expected to make 
# spurious timeouts due to high CPU loads very unlikely.
wrapper.use_system_time=false

# Disable the ping between the wrapper and the JVM so that the wrapper will 
# never try to kill and restart the JVM.  It also has the effect of disabling
# the JVM monitoring of the wrapper.
wrapper.ping.timeout=0
