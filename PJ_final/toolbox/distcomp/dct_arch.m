function out = dct_arch
%DCT_ARCH - return the architecture directory component
%   arch = DCT_ARCH returns the current computer architecture.

% Copyright 2006-2012 The MathWorks, Inc.


persistent cachedValue

if isempty( cachedValue )
    if ispc
        switch computer
          case 'PCWIN'
            cachedValue = 'win32';
          case 'PCWIN64'
            cachedValue = 'win64';
          otherwise
            error(message('parallel:internal:cluster:UnknownArch', computer));
        end
    else
        cachedValue = lower( computer );
    end
end
out = cachedValue;