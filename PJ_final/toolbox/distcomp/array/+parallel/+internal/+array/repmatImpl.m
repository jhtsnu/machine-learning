function A = repmatImpl(A, M, N, className, zerosFcn, catDimOrderingFcn)
% Implementation of repmat for arrays.  This is intended to be used with
% either codistributed arrays or gpuArrays.  

%   Copyright 2012 The MathWorks, Inc.

    if isa(M, className)
        M = gather(M);
    end
    if isempty(N)
        if isscalar(M)
            sz = [M M];
        else
            sz = M;
        end
    else
        if isa(N, className)
            N = gather(N);
        end
        sz = [M N];
    end

    if ~isreal(sz) || ~all(isfinite(sz)) || any(isnan(sz)) 
        error(message('parallel:array:RepmatInvalidSizeParameter'));
    end
    
    if any(floor(sz) ~= sz)
        warning(message('parallel:array:RepmatInvalidSizeParameter'));
        sz = floor(sz);
    end

    if ~isa(A, className) % A is replicated
        A = repmat(A, sz);
        return;
    end
    
    if isempty(A) || any(sz < 1)
        outSz = size(A);
        if length(outSz) < length(sz)
            % expand outSz to have the same length as sz
            outSz(end+1:length(sz)) = 1;
        elseif length(outSz) > length(sz)
            % expand sz to have the same length as outSz
            sz(end+1:length(outSz)) = 1;    
        end
        sz = max(0, sz);  %in case an element of sz is negative
        outSz = outSz.*sz;
        A = zerosFcn(A, outSz);
        return;
    end
    
    A = catDimOrderingFcn(A, sz, @iReplicateInDim);
end

function A = iReplicateInDim(A, dim, numReps)
% Perform repeated concatenations to replicate the array A numRep times in the
% dimension dim.
    split = 8;
    % Write numReps = p * split + r;
    p = floor(numReps/split);
    r = numReps - p*split;
    if p ~= 0
        cats = iLogBasedReplicateInDim(A, dim, split*p);
    else
        cats = {};
    end
    if r ~= 0
        % Relatively small number of repetitions can easily be handled by cat.
        cats(end + 1:end + r) = {A};
    end
    A = cat(dim, cats{:});
end

function cats = iLogBasedReplicateInDim(A, dim, numReps)
% Use repeated concatenation to replicate the array A numRep times in the
% dimension dim.  

    % Benchmarking shows that a base of 8 performs quite well.  The difference
    % between a base of 2 and 4 vs. 8 or 16 shows very clearly when size(A,
    % dim) is 1.
    base = 8;
    maxPow = floor(log10(numReps)/log10(base));
    % The expansion of numReps into powers of base tells us how to repeatedly
    % concatenate A.
    %
    % As an example, assume numRep is 560 and base is 4.
    % We then write numReps as 2*4^4 + 3*4^2.
    % 
    % Let's denote the concatenation of A with itself 4^2 times by A2 and the
    % concatenation of A with itself 4^4 times as A4.  The concatenation of A
    % 560 times is then the concatenation of A4, A4, A2, A2 and A2.  
    % 
    % Notice that the number of times we use A4 and A2 match the coefficients to
    % 4^4 and 4^2 when writing numReps as 2*4^4 + 3*4^2.

    % Calculate the expansion of numReps into powers of base.  coeffs(p+1) 
    % stores the coefficient to base^p.
    coeffs = zeros(1, maxPow + 1);
    remainder = numReps;
    for p = maxPow:-1:0
        coeffs(p+1) = floor(remainder/base^p);
        remainder = remainder - coeffs(p+1)*base^p;
    end

    elemsInResult = numel(A)*numReps;
    isResultQuiteLarge = elemsInResult > 1e7;
    if (isResultQuiteLarge)
        % In order to reduce memory usage, we don't store the largest 
        % concatenation of A with itself.  Instead, we fold the largest term 
        % into the second largest term.  Thus, we would write 560 as 
        % 8*4^3 + 3*4^2.
        if length(coeffs) > 1
            coeffs(end - 1) = coeffs(end - 1) + coeffs(end)*base;
            coeffs(end) = [];
        end
    end
    cats = cell(1, length(coeffs));
    for c = 1:length(coeffs)
        if coeffs(c) > 0
            reps = cell(1, coeffs(c));
            reps(:) = {A};
            cats{c} = reps;
        end
        % Expand the size of A in the dimension dim.
        if c < length(coeffs)
            args = cell(1, base);
            args(:) = {A};
            A = cat(dim, args{:});
        end
    end
    % To continue with our example, cats would now contain
    % {[], [], {A2, A2, A2}, {A3, A3, A3, A3, A3, A3, A3, A3}}.  
    % We convert that into a single cell array containing all the arrays that 
    % need to be concatenated.
    % {A2, A2, A2, A3, A3, A3, A3, A3, A3, A3, A3}.  
    cats = cats(~cellfun(@isempty, cats));
    cats = [cats{:}];
end
