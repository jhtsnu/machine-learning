% MATLAB Distributed Computing Server
% Version 6.2 (R2013a) 13-Feb-2013
%
% Functions Used in MATLAB Workers
%    getAttachedFilesFolder - Get the name of the folder into which AttachedFiles are written
%    getCurrentCluster      - Get cluster object that submitted current task
%    getCurrentJob          - Get the job to which the current task belongs
%    getCurrentTask         - Get task object currently being evaluated in this worker session
%    getCurrentWorker       - Get worker object currently running this session
%
% See also distcomp, distcomp/cluster.

% Copyright 2004-2013 The MathWorks, Inc. 
