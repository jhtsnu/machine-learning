function pctSetmcrappkeys( k1, k2 )
; %#ok<NOSEM> Undocumented

% Wrapper around setmcrappkeys to deal with setmcrappkeys not being 
% available (for example, on local workers).
% Throws error if setmcrappkeys does not exist and the keys are being
% set to non-empty values.

% Copyright 2008-2012 The MathWorks, Inc.

if exist( 'setmcrappkeys', 'builtin' ) && feature('isdmlworker')
    setmcrappkeys( k1, k2 );
else
    if isempty(k1) && isempty(k2)       
        % Empty keys do nothing, so doesn't matter if we can't set them.
    else
        % This is bad though, someone expected to be able to set the keys
        % to real values and they can't 
        error(message('parallel:internal:cluster:SetmcrappkeysFailed'))
    end
end
