function exitOnException( exitStatus )

%   Copyright 2012 The MathWorks, Inc.

% PLEASE NOTE that there is currently an exact copy of this function in
% distcomp_evaluate_filetask_core to try and alleviate g856027. If you make
% changes to this function you must ALSO make the same changes to that
% function as well.

% If no exit status is defined then assume 1
if nargin < 1
    exitStatus = 1;
end

dctSchedulerMessage(0, 'About to exit with code: %d', exitStatus);

[isWorker, isDebugWorker] = system_dependent('isdmlworker');
if isWorker && ~isDebugWorker
    % If we are exiting normally then try and give everyone a chance to clean up
    % before we exit.
    if exitStatus == 0
        try
            dct_psfcns('fastexit', exitStatus)
        catch e  %#ok<NASGU>
        end
    end
    % Kill MATLAB hard, without giving anyone a chance to run their static
    % finalizers.
    pm = com.mathworks.toolbox.distcomp.nativedmatlab.ProcessManipulation();
    pm.sendSIGKILL;
end
