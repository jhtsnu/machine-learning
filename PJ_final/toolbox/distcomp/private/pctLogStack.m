function pctLogStack( logLevel, what )
%pctLogExitStack Log a stack trace for an exit
%   pctLogExitStack(level,msg) logs a stack trace at specified level with
%   specified prefix. Both arguments are optional.

% Copyright 2012 The MathWorks, Inc.

if nargin < 1
    logLevel = 0;
end
if nargin < 2
    what = '';
end

try
    % Get the stack trace:
    try
        assert(false);
    catch E
    end

    % Extract the relevant frames and corresponding indices.
    relevantStack = E.stack(2:end);
    idxs          = transpose(1:numel(relevantStack));

    % Log the stack trace.
    arrayfun( @nLog, relevantStack, idxs );
catch E %#ok<NASGU> Let no exceptions escape, we're exiting.
end

    function nLog( frame, idx )
    % inner function called through arrayfun for each stack frame along with the
    % index of that frame.
    dctSchedulerMessage( logLevel, '%s[%d]: %s>%s:%d', what, idx, ...
                         frame.file, frame.name, frame.line );
    end

end
