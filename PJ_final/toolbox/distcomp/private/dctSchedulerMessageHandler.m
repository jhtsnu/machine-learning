function oldHandler = dctSchedulerMessageHandler(input, levelNum)
%DCTSCHEDULERMESSAGEHANDLER 
%
%  dctSchedulerMessageHandler(input, levelNum)
%

%  Copyright 2005-2012 The MathWorks, Inc.


mlock;
persistent theHandler;
persistent handlerNargIn;

oldHandler = [];
if isempty(input)
    % Return the old handler if we are going to change the 
    % current handler
    oldHandler = theHandler;
    theHandler = [];
elseif ischar(input)
    % Do nothing if no handler has been registered
    if isempty(theHandler)
        return
    end
    % This function should NEVER error
    try
        if handlerNargIn == 1
            theHandler(input); %#ok<NOEFF>
        elseif handlerNargIn > 1
            theHandler(input, levelNum); %#ok<NOEFF>
        end
    catch err
        warning(err.identifier, '%s', err.message);
    end
elseif isa(input, 'function_handle')
    % Return the old handler if we are going to change the 
    % current handler
    oldHandler = theHandler;
    theHandler = input;
    handlerNargIn = nargin(theHandler);
end
