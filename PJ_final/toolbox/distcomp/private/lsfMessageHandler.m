function aHandler = lsfMessageHandler(jobID, taskID, varargin)
%lsfMessageHandler 
%
%  lsfMessageHandler(jobID, taskID, initialMessageNumber)
%

%  Copyright 2005-2011 The MathWorks, Inc.

aHandler = parallel.internal.apishared.LsfUtils.messageHandler( jobID, taskID, varargin{:} );
