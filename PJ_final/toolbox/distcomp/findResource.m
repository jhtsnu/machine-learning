function out = findResource(resourceType, varargin)
%findResource Find available distributed computing resources
%
%   findResource will be removed in a future release. Use parcluster instead.
%
%   OUT = findResource
%   returns a scheduler object representing the scheduler identified by the
%   default parallel configuration, with the scheduler object properties set to
%   the values defined in that configuration.
%
%   OUT = findResource('scheduler', 'configuration', CONFIG)
%   returns a scheduler object representing the scheduler identified by the
%   specified parallel configuration, with the scheduler object properties set
%   to the values defined in that configuration.
%
%   OUT = findResource('scheduler','type', SCHEDTYPE)
%   OUT = findResource('worker')
%   return an array, OUT, containing objects representing all available
%   distributed computing schedulers of the given type, or workers. SCHEDTYPE
%   can be one of :
%
%   'local', 'jobmanager', 'hpcserver', 'lsf', 'mpiexec', 'pbspro', 'torque', or any
%   string starting with 'generic'
%
%   The 'local' option allows your jobs to run on MATLAB workers on your
%   local client machine, without a jobmanager or separate scheduler.
%   You can use different scheduler types starting with 'generic' to
%   identify one generic scheduler or configuration from another. For
%   third-party schedulers, job data is stored in the location specified by the
%   scheduler object's DataLocation property.
%
%   OUT = findResource('scheduler','type','jobmanager','LookupURL','HOST:PORT')
%   OUT = findResource('worker','LookupURL','HOST:PORT')
%   use the lookup process of the job manager running at a specific
%   location. The lookup process is part of a job manager. By default,
%   findResource uses all the lookup processes that are available to the local
%   machine via multicast. If you specify 'LookupURL' with a host and port,
%   findResource uses the job manager lookup process running at that
%   location. This URL is where the lookup is performed from, it is not
%   necessarily the host running the job manager or worker. This unicast call is
%   useful when you want to find resources that might not be available via
%   multicast or in a network that does not support multicast. Note that the
%   port value of a lookup defaults to 27350 if it is not specified. This
%   is the default base port for a cluster as specified in the mdce_def file.
%
%   Note:  LookupURL is ignored when finding third-party schedulers.
%
%   OUT = findResource('scheduler','type','hpcserver','SchedulerHostname','headNode')
%   use the HPC Server scheduler with the specified head node.  
%
%   Note:  SchedulerHostname is ignored for all schedulers except HPC Server.
%
%   OUT = findResource(... ,'P1', V1, 'P2', V2,...)
%   returns an array, OUT, of resources whose property names and property values
%   match those passed as parameter-value pairs, P1, V1, P2, V2.
%   Note that the property value pairs can be in any format supported by the SET
%   function.
%   When a property value is specified, it must use the same exact value that
%   the GET function returns, including letter case. For example, if GET returns
%   the Name property value as 'MyJobManager', then findResource will not find
%   that object if searching for a Name property value of 'myjobmanager'.
%
%   Remarks
%   Note that it is permissible to use parameter-value string pairs, structures,
%   and parameter-value cell array pairs in the same call to findResource.  The
%   parameter-value pairs can also be specified as a configuration, described in
%   the "Programming with User Configurations" section in the documentation. If 
%   a configuration is specified then the Configuration property of the returned
%   scheduler object will also be set to the specified value.
%
%   Examples: 
%   Find the scheduler identified by the default parallel configuration, with
%   the scheduler object properties set to the values defined in that
%   configuration.
%     sched = findResource();
%   
%   Find a particular job manager by its name.
%     jm1 = findResource('scheduler','type','jobmanager', ...
%                        'Name', 'ClusterQueue1');
%
%   Find all job managers. In this example, there are four.
%     all_job_managers = findResource('scheduler','type','jobmanager')
%     all_job_managers =
%         distcomp.jobmanager: 1-by-4
%
%   Find all job managers accessible from the lookup service on a particular
%   host.
%     jms = findResource('scheduler','type','jobmanager', ...
%                        'LookupURL','MyJobManagerHost');
%
%   Find a particular job manager accessible from the lookup service on a
%   particular host. In this example, subnet2.host_alpha port 6789 is where the
%   lookup is performed, but the job manager named SN2Jmgr might be running on
%   another machine.
%
%     jm = findResource('scheduler','type','jobmanager', ...
%                        'LookupURL', 'subnet2.host_alpha:6789', ...
%                        'Name', 'SN2JMgr');
%
%   Find the LSF scheduler on the network.
%     lsf_sched = findResource('scheduler','type','LSF')
%
%   Find the HPC Server scheduler with head node hpcsHeadNode
%     hpcserver_sched = findResource('scheduler', 'type', 'hpcserver', ...
%                                    'SchedulerHostname', 'hpcsHeadNode');
%
%   See also defaultParallelConfig, batch, createJob, createParallelJob,
%   createMatlabpoolJob, parcluster.

% Copyright 2004-2012 The MathWorks, Inc.

cleanup = parallel.internal.apishared.apiDeprecationMessage( 'findResource', 'parcluster' ); %#ok<NASGU>

% This will error in a deployed application if
% this is the second MCR component in the application.
parallel.internal.apishared.mcrShutdownHandler( 'initialize' );

% Need a variable to carry out a one off check to ensure that the java
% security manager has been set correctly - this set occurs in the schema
% of the distcomp package so all we need to do is get the singleton instance of
% distcomp.objectroot
persistent HAS_BEEN_INITIALISED
if isempty(HAS_BEEN_INITIALISED)
    if ~usejava('jvm')
        error('distcomp:findresource:jvmNotPresent','Java must be initialized in order to use the Parallel Computing Toolbox. \nPlease launch MATLAB without the ''-nojvm'' flag.');
    end
    % This might throw an error if java isn't available
    try
        distcomp.getdistcompobjectroot;
        HAS_BEEN_INITIALISED = true;
    catch err
        % Catch the port unavailable exception and display nicely
        throw(distcomp.handleJavaException([], err));
    end
end

% Has the user specified a resource type - if not this is the default
% call that uses the configuration specified in defaultParallelConfig
if nargin < 1
    resourceType = 'scheduler';
    pvals = {'configuration', defaultParallelConfig}; %#ok<REMFF1> API1 implementation
else
    pvals = varargin;
end
% Verify that the resource type is valid before we go any further.
% It should be a 1xN char array.
idInvalidResType = 'distcomp:findresource:InvalidResourceType';
if ~iIsString(resourceType)
    error(idInvalidResType, 'Resource type must be a string.');
end
validResourceTypes = {'jobmanager', 'worker', 'scheduler'};
if ~any(strcmp(validResourceTypes, resourceType))
    % Handle the case when users call
    % findResource('configuration', 'myconfig') instead of
    % findResource('scheduler', 'configuration', 'myconfig')
    if strcmpi(resourceType, 'configuration')
        error(idInvalidResType, ...
              iConfigurationNeedsResourceTypeMsg(pvals{:}));
    else
        error(idInvalidResType, iInvalidResourceTypeMsg(resourceType));
    end
end

try
    [props, pvals, configurationNameSpecified] = pConvertToPVArraysWithConfig(pvals, 'findResource');
catch err
    throw(err);
end
distcompserialize([]);

% Make errors originate from findResource
try
    % Ensure that the user has asked us to find a valid type of resource
    switch resourceType
        case 'jobmanager'
            [out, props, pvals] = iFindScheduler([{'type'} props], [{'jobmanager'} pvals], configurationNameSpecified);
        case 'worker'
            [out, props, pvals] = iFindWorkers(props, pvals);
        case 'scheduler'
            [out, props, pvals] = iFindScheduler(props, pvals, configurationNameSpecified);
        otherwise
            error(idInvalidResType, iInvalidResourceTypeMsg(resourceType));
    end
catch err
    throw(distcomp.handleJavaException([], err));
end

% Has the user asked to sub-select the list
if numel(out) > 0 && ~isempty(props)
    out = out.find(props, pvals, '-depth', 0);
end
% Conduct a two-way communication check for the MathWorks jobmanager
if isa(out, 'distcomp.jobmanager')
    out.pCheckTwoWayCommunications;
end
% Have we been asked to set the configuration on the scheduler as
% well
if ~isempty(configurationNameSpecified)
    set(out, 'Configuration', configurationNameSpecified);
end
% Set the username in the jobmanager.
% If the configuration has already set the username we do not do anything
% here. But therefore this needs to run after setting the configuration.
if isa(out, 'distcomp.jobmanager')
    for i = 1:numel(out)
        jm = out(i);
        if isempty(jm.pGetUserName())
            try
                userIdentity = jm.pReturnProxyObject().promptForIdentity([]);
                jm.pSetUserName(userIdentity);
            catch err
                throw(distcomp.handleJavaException(jm, err));
            end
        end
    end
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [out, props, pvals] = iFindJobManagers(props, pvals)
[url, props, pvals] = iGetURL(props, pvals);
[name, props, pvals] = iGetName(props, pvals);
% Get all the jobmanager serviceItems
proxyManagers = [];
import com.mathworks.toolbox.distcomp.service.ServiceAccessor
try
    proxyManagers = ServiceAccessor.getJobManagers(url, name);
catch err
    iHandleServiceAccessorException(err);
end

if isempty(proxyManagers)
    out = [];
    return;
end

[certificate, props, pvals] = iGetCertificate(props, pvals);

try
    % Check each to ensure that version info is the same
    OK = true(size(proxyManagers));
    for i = 1:numel(OK)
        OK(i) = proxyManagers(i).checkVersion;
        if ~OK(i)
            warning('distcomp:findresource:IncorrectVersion', ...
                iIncorrectVersionString(proxyManagers(i)));
        end
    end
    % Remove those that fail the version check
    proxyManagers = proxyManagers(OK);
    % Jobmanager construction could throw an error (not able to access the
    % JobAccessProxy for example - so loop over the createObject to pick up
    % those errors
    OK = true(numel(proxyManagers), 1);
    out = handle(-ones(numel(proxyManagers), 1));
    uddConstructor = distcomp.getSchedulerUDDConstructor('jobmanager');
    for i = 1:numel(OK)
        thisProxyManager = proxyManagers(i);
        try
            % If we were given a certificate, tell the proxy about it
            if ~isempty( certificate );
                thisProxyManager.setSSLCertificate( certificate );
            end
            
            out(i) = distcomp.createObjectsFromProxies(thisProxyManager, ...
                uddConstructor, distcomp.getdistcompobjectroot);
            
        catch err %#ok<NASGU>
            thisProxyManager.dispose();
            OK(i) = false;
        end
    end
    % Sub-select only the successfully created objects
    out = out(OK);
catch err
    rethrow(err);
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [out, props, pvals] = iFindWorkers(props, pvals)
[url, props, pvals] = iGetURL(props, pvals);
[name, props, pvals] = iGetName(props, pvals);
proxyWorkers = [];
import com.mathworks.toolbox.distcomp.service.ServiceAccessor
try
    proxyWorkers = ServiceAccessor.getMLWorkers(url, name);
catch err
    iHandleServiceAccessorException(err);
end

if isempty(proxyWorkers)
    out = [];
    return;
end

try
    % Check each to ensure that version info is the same
    OK = true(size(proxyWorkers));
    for i = 1:numel(OK)
        OK(i) = proxyWorkers(i).checkVersion;
        if ~OK(i)
            warning('distcomp:findresource:IncorrectVersion', ...
                iIncorrectVersionString(proxyWorkers(i)));
        end
    end
    % Remove those that fail the version check
    proxyWorkers = proxyWorkers(OK);
    uddConstructor = distcomp.getSchedulerUDDConstructor('worker');
    out = distcomp.createObjectsFromProxies(proxyWorkers, uddConstructor, distcomp.getdistcompobjectroot);
catch err
    rethrow(err);
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function str = iIncorrectVersionString(proxy)
localVersion = com.mathworks.toolbox.distcomp.util.Version.VERSION_STRING;
if isempty(proxy.getName)
    resStr = 'a remote resource';
else
    resStr = sprintf('the resource %s', char(proxy.getName));
end
if isempty(proxy.getHostName)
    hostStr = '';
else
    hostStr = sprintf(' on computer %s', char(proxy.getHostName));
end
str = sprintf([ ...
    '\nThe local product version of the Parallel Computing Toolbox is \n'...
    'incompatible with that of %s%s.\n' ...
    'The local product version is %s while the version on the remote computer is %s.\n' ...
    'Specify the ''Name'' and ''LookupURL'' inputs to findResource to find only\n' ...
    'the resources you want to use.\n' ...
    ], resStr, hostStr, char(localVersion), char(proxy.getVersionString));

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [out, props, pvals] = iFindScheduler(props, pvals, configurationNameSpecified)
[type, props, pvals] =  iGetType(props, pvals);
if isempty(type)
    error('distcomp:findresource:InvalidArgument', ...
          'You must supply a type argument to find a scheduler');
end

% CCS will be deprecated in the future
if strcmpi(type, 'ccs')
    warning('distcomp:ccs:DeprecatedSchedulerType', ...
        ['The CCS scheduler type will be removed in a future version \n' ...
        'of the Parallel Computing Toolbox. Please use the HPCSERVER \n' ...
        'scheduler type instead.']);
    warning('off', 'distcomp:ccs:DeprecatedSchedulerType');
    
    % Change the name to hpcserver instead
    type = 'hpcserver';
end

% Create the correct scheduler type. REMEMBER to add these to the error just below
switch lower(type)
    case {'lsf' 'pbspro' 'torque' 'mpiexec' 'hpcserver' 'local' 'runner'}
        out = iCreateFileBasedScheduler(type);
        if strcmpi(type, 'hpcserver')
            % Note that iFindAndSetDefaultSchedulerHostname may perform
            % an AD search if we can't determine the default scheduler 
            % hostname any other way.
            [out, props, pvals] = iFindAndSetDefaultSchedulerHostname(out, props, pvals, configurationNameSpecified);
        end
    case 'jobmanager'
        [out, props, pvals] = iFindJobManagers(props, pvals);
    otherwise
        % Generic schedulers can be specified as 'generic<arbitrary text>',
        uddType = 'generic';
        found = strncmpi(type, uddType, numel(uddType));
        if found
            out = iCreateFileBasedScheduler(type, uddType);
        else
            error('distcomp:findresource:InvalidArgument', ...
                  ['Currently supported schedulers are:\n''jobmanager'', ' ...
                   '''lsf'', ''mpiexec'', ''hpcserver'', ''local'', ''pbspro'', ' ...
                   '''torque'', and ''generic''.']);
        end
end


%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function out = iCreateFileBasedScheduler(proxyType, uddType)
% NB proxyType refers to the actual string that is stored in the scheduler's 
% Type property and is also the string that we use for the proxies.
% uddType refers to the name of the scheduler for determining the correct 
% constructor.
% For most schedulers, the uddType and proxyType will be identical.  For generic
% schedulers, the uddType will always be 'generic', but the type may be 
% 'generic<someOtherText>'.
if nargin < 2
    uddType = proxyType;
end
% The udd type MUST be lower case
uddType = lower(uddType);

import com.mathworks.toolbox.distcomp.distcompobjects.SchedulerProxy
% Remember to not warn on a permission error here
WARN_ON_PERMISSION_ERROR = false;
% Create the storage object to use with the scheduler
storage = distcomp.filestorage(pwd, WARN_ON_PERMISSION_ERROR);
proxy = SchedulerProxy.createInstance(proxyType, storage);
uddConstructor = distcomp.getSchedulerUDDConstructor(uddType);
out = distcomp.createObjectsFromProxies(proxy, uddConstructor, distcomp.getdistcompobjectroot);

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [value, props, pvals] = iGetProperty(property, props, pvals)
value = [];
% Can we find the property anywhere (it might be defined multiple times).
found = strcmpi(property, props);
% If we found it, get the last value for it and remove all occurrences from
% the lists.
if any(found)
   value = pvals{find(found, 1, 'last')};
   props(found) = [];
   pvals(found) = [];
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [name, props, pvals] = iGetName(props, pvals)
[name, props, pvals] = iGetProperty('name', props, pvals);
if ~isempty( name ) && ~ischar( name )
    error('distcomp:findresource:InvalidPropertyValueClass','Name property must be a string value');
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [url, props, pvals] = iGetURL(props, pvals)
[url, props, pvals] = iGetProperty('lookupURL', props, pvals);
if ~isempty( url ) && ~ischar( url )
    error('distcomp:findresource:InvalidPropertyValueClass','lookupURL property must be a string value');
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [type, props, pvals] = iGetType(props, pvals)
[type, props, pvals] = iGetProperty('type', props, pvals);
if ~isempty( type ) && ~ischar( type )
    error('distcomp:findresource:InvalidPropertyValueClass','Type property must be a string value');
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [schedulerHostname, props, pvals] = iGetSchedulerHostname(props, pvals)
[schedulerHostname, props, pvals] = iGetProperty('SchedulerHostname', props, pvals);
if ~isempty( schedulerHostname ) && ~ischar( schedulerHostname )
    error('distcomp:findresource:InvalidPropertyValueClass','SchedulerHostname property must be a string value');
end

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [certificate, props, pvals] = iGetCertificate(props, pvals)
[certificate, props, pvals] = iGetProperty('certificate', props, pvals);
if ~isempty( certificate ) && ~ischar( certificate )
    error('distcomp:findresource:InvalidPropertyValueClass', ...
        'Certificate property must be a string value');
end

% If the certificate is a file name, convert to
% a java.io.file so the correct setSSLCertificate is called.
if ~isempty( certificate ) && exist( certificate, 'file' )
    certificate = java.io.File( certificate );
end


%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function msg = iInvalidResourceTypeMsg(resourceType)
msg = sprintf('Invalid resource type: ''%s''. You can only find resources\nof type ''scheduler'', ''jobmanager'', or ''worker''.', resourceType);


%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function msg = iConfigurationNeedsResourceTypeMsg(varargin)
if nargin == 1 && iIsString(varargin{1})
    % The user executed findResource('configuration', 'some_string') Let's
    % assume that the string argument they passed is the name of a
    % configuration.
    config = varargin{1};
else
    % Let's be cautious and use a made-up name of a configuration in our
    % message.
    config = 'MyConfig';
end
msg = sprintf(['The first argument to findResource must be the ', ...
               'resource type to be found,\n', ...
               'such as ''scheduler''.  If you want to find a scheduler ', ...
               'using a configuration\n', ...
               'named ''%s'', use:\n\n', ...
               '  findResource(''scheduler'',',  ...
               ' ''configuration'', ''%s'')', ...
               ], config, config);

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function valid = iIsString(value)
valid = ischar(value) && (size(value, 2) == numel(value));

%--------------------------------------------------------------------------
%
%--------------------------------------------------------------------------
function [out, props, pvals] = iFindAndSetDefaultSchedulerHostname(out, props, pvals, configurationNameSpecified)
% Is there a SchedulerHostname P-V pair?
[schedulerHostname, props, pvals] = iGetSchedulerHostname(props, pvals);

% NB Even though the cached version of the ccsscheduler object may
% have a valid SchedulerHostname, we want
% findResource('scheduler', 'type', 'hpcserver') to behave the same
% way each time, so if we can't deduce a SchedulerHostname from the 
% P-V pairs, then we will always ask the scheduler what its default
% hostname should be.
if isempty(schedulerHostname)
    schedulerHostname = distcomp.ccsscheduler.pGetDefaultSchedulerHostname(configurationNameSpecified);
    if isempty(schedulerHostname)
        error('distcomp:findresource:CannotFindScheduler', ...
            'Could not locate a scheduler of type ''hpcserver''');
    end
    if iscell(schedulerHostname)
        % Can't deal with multiple host names
        error('distcomp:findresource:TooManySchedulers', ...
            ['Found more than one scheduler of type ''hpcserver'' on your network, \n', ...
            'with the following head nodes:\n%s\n', ...
            'You can narrow the search by specifying one of these hosts as the \n', ...
            'SchedulerHostname for findResource, either by creating a configuration \n', ...
            'or by using the command:\n', ...
            '\tfindResource(''scheduler'', ''type'', ''hpcserver'', ...\n', ...
            '\t\t''SchedulerHostname'', <desired hostname>)'], ...
            sprintf('\t%s\n', schedulerHostname{:}));
    end
end

% Set the scheduler hostname, but ensure that we use the correct ClusterVersion 
% when doing so.
out.pDeduceVersionAndSetHostname(schedulerHostname, configurationNameSpecified);

function iHandleServiceAccessorException(err)
import com.mathworks.toolbox.distcomp.service.ServiceAccessor
% Get the exception stashed by the ServiceAccessor. This should be able to provide a MATLAB
% identifier and localized message we can use to construct a warning.
exception = ServiceAccessor.getStashedException();
if ~isempty(exception) && isa(exception, 'com.mathworks.toolbox.distcomp.util.HasMatlabErrorId')
    warning(char(exception.getMatlabErrorId()), '%s', char(exception.getLocalizedMessage()));
else    
    throw(err);
end

