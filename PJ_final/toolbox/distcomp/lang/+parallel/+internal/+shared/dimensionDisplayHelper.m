function szStr = dimensionDisplayHelper( sz )
%iDimensionDisplayHelper  build a string representing array dimensions
%
%   szStr = dimensionDisplayHelper(sz) creates a string displaying the
%   array dimensions in sz in an N-by-M-by-... form
%
%   Examples:
%   >> szStr = iDimensionDisplayHelper([1,2,3,0])
%   szStr = 
%   1-by-2-by-3-by-0

%   Copyright 2012 The MathWorks, Inc.


% Get the separator string
sep = getString(message('parallel:array:DisplayDimensionSeparator'));
% Format as N-by-M-by-P-by- etc.
szStr = sprintf( ['%d',sep], sz );
% trim the final -by- from sprintf
szStr = szStr(1:end-length(sep));
end