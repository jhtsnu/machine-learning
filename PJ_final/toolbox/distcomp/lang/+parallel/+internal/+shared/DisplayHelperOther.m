% DisplayHelperOther - helper class used for display non-numeric distributed
% objects.

% Copyright 2009-2012 The MathWorks, Inc.
classdef DisplayHelperOther < parallel.internal.shared.DisplayHelper

    properties ( Access = private )
        UnderlyingClassName
        FullSize
    end
    
    methods
        function obj = DisplayHelperOther( name, classname, underlyingclass, fullsize )
            obj.Name                = name;
            obj.ClassName           = classname;
            obj.Value               = []; % unused
            obj.UnderlyingClassName = underlyingclass;
            obj.FullSize            = fullsize;
            obj.IsTruncated         = false;
        end
        
        function doDisp( obj )
            doDisplayOther( obj );
        end
        
        function doDisplay( obj )
            noPageRange    = '';
            noTruncMessage = '';
            obj.nameLine( 'display', noPageRange, noTruncMessage );
            doDisplayOther( obj );
        end
    end

    methods ( Access = private )
        function doDisplayOther( obj )
            % szStr := 4-by-7-by-477-by-
            szStr = parallel.internal.shared.dimensionDisplayHelper( obj.FullSize );
            
            msgStr = getString(message('parallel:array:DisplayOther', ...
                obj.ClassName, szStr, obj.UnderlyingClassName));
            fprintf( 1, '    %s\n%s', msgStr, obj.separator() );
        end
    end
    
    methods ( Access = protected )
        function res = formatEndTruncationMsg( obj ) %#ok<MANU> - match interface
        % "other" arrays are never truncated
            res = '';
        end
    end
    
end
