function range = denseDisplayRangeHelper( x, N )
%denseDisplayRangeHelper  calculate the range of data to be displayed
%
%   range = denseDisplayRangeHelper(x,N) calculates the range of the data "x"
%   that should be shown given a desired number of visible elements "N". The
%   range is returned as a structure with fields "start" and "end" that give
%   the minimum and maximum indices in each of the input array dimensions.
%
%   Examples:
%   >> x = zeros(100,100,10,3);
%   >> denseDisplayRangeHelper( x, 1000 )
%   >> denseDisplayRangeHelper( gpuArray(x), 500 )
%   >> denseDisplayRangeHelper( distributed(x), 500 )

%   Copyright 2010 The MathWorks, Inc.
%   $Date: 2010/12/27 01:10:36 $
%   $Revision: 1.1.6.1 $

% Dense truncation. If we can display whole "pages", do as many of those as
% we can in as many dimensions as possible. Otherwise, truncate one page and
% show that. When truncating a page, do it in the larger dimension.
sz         = size( x );
szprod     = cumprod( sz );

if szprod( 2 ) <= N
    % The first page is smaller than N, so pick some pages
    range = iChooseRangeForPages( sz, szprod, N );
else
    % The first page is too big, truncate that page, and pick only 1 in all
    % subsequent dimensions
    range = iChooseRangeTruncatingFirstPage( sz, szprod, N );
end

end



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iChooseArgsForPages - build up subsargs to subsref an array, and a
% trunc_message for the case where we're selecting whole pages
function range = iChooseRangeForPages( fullSz, szProd, N )

% cutoverDim is the first dimension which overflows N, we wont take all of
% that dimension back to the client for display.
cutoverDim = find( szProd > N, 1, 'first' );

% Pick how many elements in the cutoverDim to bring back. Note that by
% calling ceil, we might bring back at most 2*N elements.
numInCutoverDim = ceil( N / szProd(cutoverDim-1) );

% Build range to subselect x
range.start                 = ones( length( fullSz ), 1 );
range.end                   = ones( length( fullSz ), 1 );
range.end( 1:cutoverDim-1 ) = fullSz( 1:cutoverDim - 1 );
range.end( cutoverDim )     = numInCutoverDim;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function range = iChooseRangeTruncatingFirstPage( sz, fullSz, N )
% We can display only part of the first page, pre-allocate the subsargs with
% ones.

% Use a heuristic method to truncate. My heuristic is: do not truncate any
% dimension smaller than TRUNCATE_THRESH; if truncating both
% dimensions, attempt to keep the aspect ratio approximately correct.
TRUNCATE_THRESH = min( 20, ceil( sqrt( N ) ) );

range.start      = ones( length( fullSz ), 1 );
range.end        = ones( length( fullSz ), 1 );
range.end( 1:2 ) = sz( 1:2 );
% NB - in higher dimensions, default range of [1 1] is Ok.

firstTwoSizeElements = sz(1:2);

canTruncate = firstTwoSizeElements > TRUNCATE_THRESH;

if all( canTruncate )
    aspectRatio      = firstTwoSizeElements(1) / firstTwoSizeElements(2);
    newRows          = ceil( sqrt( N * aspectRatio ) );
    newCols          = ceil( sqrt( N / aspectRatio ) );
    % Truncate in both dimensions
    range.end( 1:2 ) = [newRows, newCols];
elseif any( canTruncate )
    % Truncate in one dimension
    truncated                = canTruncate;
    truncatedTo              = firstTwoSizeElements;
    truncatedTo( truncated ) = ceil( N / firstTwoSizeElements( ~truncated ) );
    range.end( truncated )   = truncatedTo( truncated );
end    

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
