%DisplayHelperInvalid - helper class for displaying invalid distributed
%arrays

% Copyright 2009-2012 The MathWorks, Inc.
classdef DisplayHelperInvalid < parallel.internal.shared.DisplayHelper
    
    properties (Access = private)
        WhyInvalidMessage = '';
    end
    
    methods 
        function obj = DisplayHelperInvalid( name, classname, msg )
            obj.Name                = name;
            obj.ClassName           = classname;
            obj.Value               = []; % unused
            obj.IsTruncated         = false;
            obj.WhyInvalidMessage   = msg;
        end
        
        function doDisp( obj )
            doDisplayOther( obj );
        end
        
        function doDisplay( obj )
            noPageRange    = '';
            noTruncMessage = '';
            obj.nameLine( 'display', noPageRange, noTruncMessage );
            doDisplayOther( obj );
        end
    end
    
    methods ( Access = private )
        function doDisplayOther( obj )
            fprintf( 1, '    %s\n%s', obj.WhyInvalidMessage, obj.separator() );
        end
    end

    methods ( Access = protected )
        function res = formatEndTruncationMsg( ~ )
        % "other" arrays are never truncated
            res = '';
        end
    end
end
