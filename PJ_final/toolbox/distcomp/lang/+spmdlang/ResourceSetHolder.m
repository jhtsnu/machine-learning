% This class exists as an intermediary between any object and a resource
% set, and informs the resource set on destruction so that the resource set
% itself can maintain a reference count. Objects should not hold "raw"
% references to resource sets.

% Copyright 2008 The MathWorks, Inc.
% $Revision: 1.1.6.2 $   $Date: 2011/04/12 20:23:03 $
classdef (Hidden, Sealed) ResourceSetHolder < handle
   
    properties ( Access = private, Hidden, Transient )
        % The actual resource set
        ResourceSetObj = [];
    end

    methods ( Access = private, Hidden )
        function tf = isValid( obj )
            tf = ~isempty( obj.ResourceSetObj );
        end
    end

    
    methods ( Access = public, Hidden )
        function obj = ResourceSetHolder( resSet )
            obj.ResourceSetObj = resSet;
            if ~isempty( resSet )
                resSet.incrementRefCount();
            end
        end
        
        function rs = getResourceSet( obj )
            if isValid( obj )
                rs = obj.ResourceSetObj;
            else
                error(message('parallel:lang:spmd:ResourceSet'));
            end
        end

        function delete( obj )
            if isValid( obj )
                obj.ResourceSetObj.decrementRefCount();
            end
        end
    end
    
end
