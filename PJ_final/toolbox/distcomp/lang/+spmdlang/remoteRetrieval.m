function [OK, serVal] = remoteRetrieval( key, arrayHolderToClear )
% Copyright 2008-2012 The MathWorks, Inc.

    pClearIfNecessary( arrayHolderToClear );

    try
        serVal = parallel.internal.pool.serialize( ...
            spmdlang.ValueStore.retrieve( distcompdeserialize( key ) ) );
        OK     = true;
    catch E
        serVal = parallel.internal.pool.serialize( E );
        OK     = false;
    end
end
