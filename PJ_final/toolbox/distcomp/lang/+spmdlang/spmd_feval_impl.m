function spmd_feval_impl( bodyFcn, assignOutFcn, getOutFcn, unpackInFcn, initialOutCell, inputCell, varargin )
%SPMD_FEVAL_IMPL - support for SPMD block
    
% Copyright 2008-2012 The MathWorks, Inc.


    resSet = spmdlang.ResourceSetMgr.chooseResourceSet( inputCell, varargin{:} );
    
    attachFilesAndRetryOnError = resSet.shouldRetryIfFileNotFound();

    while true
        try
            % the resource set knows how to execute a block.
            blockExecutor = resSet.buildBlockExecutor( bodyFcn, assignOutFcn, getOutFcn, ...
                                                       unpackInFcn, initialOutCell );
            
            blockExecutor.initiateComputation();
            
            while ~blockExecutor.isComputationComplete()
                % isComputationComplete will block for a while waiting for completion
            end

            % Give the block executor the opportunity to throw any errors detected on
            % the labs. The block executor is free to throw such errors at any other
            % time, but this is the last opportunity.
            blockExecutor.throwBlockExceptions();
            break;
        catch E
            % Dispose of the blockExecutor so we can make a new one when
            % we rerun.  Do this as early as possible so workers don't
            % think that they are being interrupted.  Note that it is OK to
            % dispose and then rethrow errors.
            dispose( blockExecutor );

            [isFileNotFound, possibleSources] = iIsFileNotFoundError(E);
            if ~attachFilesAndRetryOnError || ~isFileNotFound
                rethrow(E)
            end
            
            % Get the body function and add it to the possible sources
            possibleSources = [possibleSources; blockExecutor.getBodyFunctionFile()]; %#ok<AGROW>
            possibleSources = unique(possibleSources);
            % Only try to do the dependency analysis once
            attachFilesAndRetryOnError = false;
            try
                filesAttached = parallel.internal.pool.attachDependentFilesToPool(...
                    possibleSources);
                if ~filesAttached
                    rethrow(E);
                end
                dctSchedulerMessage(6, 'Rerunning spmd_feval_impl after attaching files.');
            catch attachErr
                % We'll just throw the exception that we caught earlier if the
                % dependency analysis goes wrong.
                rethrow(E)
            end
        end
    end
        
    dispose( blockExecutor );
    
    % NB outputs assigned for caller in "delete" of blockExecutor.
end

function [tf, possibleSourceFiles] = iIsFileNotFoundError(err)
    % The actual interesting error will be in the cause of the ParallelException.  
    % Note that 
    sourceCodeUnavailableErr = 'parallel:lang:spmd:SourceCodeNotAvailable';
    sourceCodeUnavailableErrOnLab = 'parallel:lang:spmd:SourceCodeNotAvailableOnLab';
    undefinedErr = {'parallel:lang:spmd:UndefinedFunctionOnWorker', ...
                    'MATLAB:UndefinedFunction'};
    
    causes = err.cause;
    isSourceUnavailable = cellfun(@(x) any(strcmp(x.identifier, sourceCodeUnavailableErr)), causes);
    isSourceUnavailableOnLab = cellfun(@(x) any(strcmp(x.identifier, sourceCodeUnavailableErrOnLab)), causes);
    isUndefined = cellfun(@(x) any(strcmp(x.identifier, undefinedErr)), causes);
    
    tf = any(isSourceUnavailable) || any(isSourceUnavailableOnLab) || any(isUndefined);
    possibleSourceFiles = {};
    if tf
        % Fish out the argument from the errors as these may tell us what the 
        % files were.  Note that the SourceCodeNotAvailableOnLab won't be able to 
        % give us anything interesting.  
        interestingErrs = causes(isSourceUnavailable | isUndefined);
        possibleSourceFiles = cellfun(@(x) iGetFirstArgumentFromError(x), ...
            interestingErrs, 'UniformOutput', false);
        empties = cellfun(@isempty, possibleSourceFiles);
        possibleSourceFiles = possibleSourceFiles(~empties);
    end
    
end

function firstArg = iGetFirstArgumentFromError(err)
firstArg = '';
if ~isempty(err.arguments)
    firstArg = err.arguments{1};
end
end