% Trivial resource set - the resource set used for SPMD(0) calls


% Copyright 2008-2012 The MathWorks, Inc.
classdef TrivialResourceSet < spmdlang.AbstractResourceSet
    methods ( Access = public, Hidden )
        
        function tf  = isValid( obj ) %#ok<MANU>
            tf = true;
        end
        
        function tf  = canAccessLabs( obj ) %#ok<MANU>
            tf = internal.matlab.getParallelFunctionDepth == 0;
        end

        function obj = TrivialResourceSet()
            obj@spmdlang.AbstractResourceSet( 1 );
        end
        
        function tf = satisfiesConstraints( ~, minN, ~ )
        % Trivial resource set only satisfies constraints if minN is zero
            tf = ( minN == 0 );
        end
        
        function blockEx = buildBlockExecutor( obj, bodyF, assignOutF, getOutF, unpackInF, initialOuts )
            blockEx = spmdlang.LocalSpmdExecutor( spmdlang.ResourceSetHolder( obj ), ...
                                                 bodyF, assignOutF, getOutF, unpackInF, initialOuts );
        end
        
        function val = getFromLab( ~, labidx, key )
            if labidx ~= 1
                error(message('parallel:lang:spmd:TrivialResourceUnexpectedGetIdx', labidx));
            end
            val = spmdlang.ValueStore.retrieve( key );
            % Ensure that hidden Composites are turned into broken
            % Composites. Serializing and de-serializing in this way may be
            % somewhat inefficient, but it's the only way to ensure
            % consistency between local and remote operation.
            val = parallel.internal.pool.deserialize( ...
                parallel.internal.pool.serialize( val ) );
        end
        
        function newKey = setOnLab( ~, labidx, newValue )
            if labidx ~= 1
                error(message('parallel:lang:spmd:TrivialResourceUnexpectedSetIdx', labidx));
            end
            newKey = spmdlang.ValueStore.store( newValue );
        end
        
        function keyUnreferenced( ~, labidx, key )
            if labidx ~= 1
                error(message('parallel:lang:spmd:TrivialResourceUnexpectedKeyIdx', labidx));
            end
            spmdlang.ValueStore.remove( key );
        end
        
        function tf = shouldRetryIfFileNotFound( ~ )
        % Never retry the computation a file not found error is thrown by the resource set.
            tf = false;
        end
    end
end
