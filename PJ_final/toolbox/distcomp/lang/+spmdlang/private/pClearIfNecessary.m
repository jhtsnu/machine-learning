function pClearIfNecessary( arrayHolderToClear )
%pClearIfNecessary - if we've been given some keys to clear, deal with them

% Copyright 2008 The MathWorks, Inc.
% $Revision: 1.1.6.2 $   $Date: 2011/04/12 20:23:10 $
    
    if ~isempty( arrayHolderToClear )
        % Clear any remote keys first before we attempt anything else.
        try
            spmdlang.ValueStore.remove( arrayHolderToClear.array() );
        catch E
            % Simply warn.
            warning(message('parallel:lang:spmd:RemoteClear', getReport( E )));
        end
    end
end

