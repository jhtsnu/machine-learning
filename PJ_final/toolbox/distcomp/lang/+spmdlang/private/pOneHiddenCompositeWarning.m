function pOneHiddenCompositeWarning
%pOneHiddenCompositeWarning - warn once about "hidden" Composites

% Copyright 2008-2009 The MathWorks, Inc.
% $Revision: 1.1.6.4 $   $Date: 2011/04/12 20:23:11 $

persistent DONE

mlock;

if isempty( DONE )
    DONE = true;
    turnOffWarning = true;
    warnAppend = sprintf( ['\nThis warning will now be disabled, but can be re-enabled by executing \n', ...
                        'warning on parallel:lang:spmd:RemoteTransfer'] );
else
    turnOffWarning = false;
    warnAppend = '';
end

warning(message('parallel:lang:spmd:RemoteTransfer', warnAppend));

if turnOffWarning
    warning( 'off', 'parallel:lang:spmd:RemoteTransfer' );
end

end
