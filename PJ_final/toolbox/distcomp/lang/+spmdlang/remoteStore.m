function [OK, serKey] = remoteStore( serVal, arrayHolderToClear )

% Copyright 2008-2012 The MathWorks, Inc.

    pClearIfNecessary( arrayHolderToClear );

    try
        deserVal = parallel.internal.pool.deserialize( serVal );
        serKey   = parallel.internal.pool.serialize( spmdlang.ValueStore.store( deserVal ) );
        OK       = true;
    catch E
        OK       = false;
        serKey   = parallel.internal.pool.serialize( E );
    end
end
