% Parallel computing programming language constructs.
%
% Single program multiple data support.
%   spmd          - Begin single program multiple data block.
%   Composite     - Composite object used with SPMD block.
%
% See also distcomp, distcomp/parallel, PARFOR, MATLABPOOL.

% Copyright 2008-2011 The MathWorks, Inc.
