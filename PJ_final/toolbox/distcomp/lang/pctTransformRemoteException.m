function newEx = pctTransformRemoteException( cause )
%pctTransformRemoteException - transform a remote exception for local display

% Used by pctAddRemoteCause

% Copyright 2009-2012 The MathWorks, Inc.

oldMsg   = cause.message;
oldId    = cause.identifier;
oldStack = cause.stack;

stackToPrint = '';

for ii=1:length( oldStack )
    frame = oldStack(ii);
    [~, displayFile] = fileparts( frame.file );
    
    % Only include stack frames up to the remote transition point
    if iFrameIsTransitionToRemote( displayFile, frame.name )
        break;
    end
    
    % Choose how to display the function
    if ~strcmp( displayFile, frame.name )
        displayItem = sprintf( '%s>%s', displayFile, frame.name );
    else
        displayItem = sprintf( '%s.m', displayFile );
    end
    msg = message('parallel:lang:spmd:ConcatFileWithLineNum', ...
                  displayItem, num2str( frame.line )); 
    stackToPrint = sprintf( '%s\n%s', stackToPrint, getString(msg) );
end

if isempty( stackToPrint )
    msg = message('parallel:lang:spmd:NoRemoteErrorStack');
    stackToPrint = sprintf( '\n%s', getString(msg) );
end

msg = message('parallel:lang:spmd:CombineErrorMsgAndStack', ...
              oldMsg, stackToPrint);
newEx  = MException( oldId, '%s', getString(msg) );

for ii = 1:numel(cause.cause)
    newEx = newEx.addCause(cause.cause{ii});
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iFrameIsTransitionToRemote - check a given stack frame to see if this is
% the point at which "remote" execution starts
function tf = iFrameIsTransitionToRemote( file, fcn )

% Incorporate the known elements on the stack which are the transition to
% remote execution
tf = ( strcmp( file, 'parallel_function' ) ) || ...
     ( strcmp( file, 'LocalSpmdExecutor' ) && ...
       strcmp( fcn, 'LocalSpmdExecutor.initiateComputation' ) ) || ...
     ( strcmp( file, 'remoteBlockExecution' ) );
end
