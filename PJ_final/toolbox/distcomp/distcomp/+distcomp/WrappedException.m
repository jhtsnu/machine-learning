classdef WrappedException < MException

%  Copyright 2008-2012 The MathWorks, Inc.

    properties ( SetAccess = protected )
        CauseException;
    end

    methods ( Access = public )
        function obj = WrappedException(e, errId, errMsg)
            obj = obj@MException(errId, '%s', errMsg);
            obj.CauseException = e;
        end
    end
end
