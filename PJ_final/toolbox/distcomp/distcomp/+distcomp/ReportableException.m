classdef ( Sealed = true ) ReportableException < distcomp.WrappedException
    
%  Copyright 2008-2012 The MathWorks, Inc.

    methods ( Access = public )
        function obj = ReportableException(e)
            obj = obj@distcomp.WrappedException(e, ...
                    'parallel:cluster:ReportableException', ...
                    'A reportable error occurred during job execution.');
        end
    end
end