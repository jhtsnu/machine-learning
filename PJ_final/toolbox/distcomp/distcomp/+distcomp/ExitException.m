classdef ( Sealed = true ) ExitException < distcomp.WrappedException
    
%  Copyright 2008-2012 The MathWorks, Inc.

    methods ( Access = public )
        function obj = ExitException(e, location)
            m = message( 'parallel:cluster:ExitException', location );
            obj = obj@distcomp.WrappedException(e, ...
                'parallel:cluster:ExitException', m.getString() );
        end
    end
end
