function initworker(varargin)
; %#ok Undocumented
% initialization function for MATLAB Distributed Computing Server

% Copyright 2003-2012 The MathWorks, Inc.

try   
    % Check to see if a jvm is present - error if not
    if ~usejava('jvm')
        error(message('parallel:internal:cluster:JVMNotPresent'));
    end    
    % Set up the log handler that will stream back to the Worker
    iInitializeLogging();
    iLog('Start initworker');

    % MATLAB has been launched with -nodesktop, yet the diary output is
    % likely to be viewed on a desktop client. We set the hotlinks feature on
    % here for backwards compatibility as tasks used to be evaluated with
    % evalc, which always turns on hotlinks.
    feature hotlinks on
    
    % Make sure we die when our parent does
    iLog('Starting ParentWatchdog');
    com.mathworks.toolbox.distcomp.control.ParentWatchdog;
    
    % Make sure this MATLAB cannot core dump, and will exit if anything goes
    % wrong. The (unused) return argument suppresses command window output.
    OK = system_dependent(100, 2); %#ok<NASGU>
    dct_psfcns('ensureProcessExitsOnFault');
    
    % Call the common initialization
    iLog('Calling initcommon');
    initcommon();
    
    % Initialize NativeMethods - do this now to get early warning if
    % nativedmatlab shared library can not be loaded.
    com.mathworks.toolbox.distcomp.nativedmatlab.NativeMethods;
    
    iLog('Setting system properties from environment');
    import com.mathworks.toolbox.distcomp.util.SystemPropertyNames;
    % Set java system properties from the environment
    iSetSystemPropertyFromEnv(...
        'java.rmi.server.hostname', ...
        'HOSTNAME');
    iSetSystemPropertyFromEnv(...
        SystemPropertyNames.HOST_NAME, ...
        'HOSTNAME');
    iSetSystemPropertyFromEnv(...
        SystemPropertyNames.BASE_PORT, ...
        'BASE_PORT');
    iSetSystemPropertyFromEnv(...
        SystemPropertyNames.RMI_USE_SERVER_SPECIFIED_HOSTNAME, ...
        'USE_SERVER_SPECIFIED_HOSTNAME');
    
    iLog('Setting port range');
    % Set the port range MATLAB will use to talk to the job manager.
    iSetPortRange();
    
    % If we have been passed a certificate, add it to the 
    % ClientCertificateStore so it can be used.
    iSetCertificate();
           
    iLog('Testing DataStore export');
    iCheckDataStoreExporter();
        
    address = getenv('WORKER_IPC_ADDRESS');
    if isempty(address)
        error(message('parallel:internal:cluster:InitWorkerInvalidEnvironment', 'WORKER_IPC_ADDRESS'));
    end
    requestServerArguments = {address};
    
    if ispc 
        % On Windows look for the WORKER_PROCESS_OWNER_SID variable
        % If set, pass it on to pctRequestServer.
        sid = getenv('WORKER_PROCESS_OWNER_SID');
        if ~isempty(sid)
            iLog('pctRequestServer will give permissions to SID %s', sid)
            requestServerArguments = {address, sid};
        end
    end
    
    % Start listening for commands
    iLog('Starting pctRequestServer on %s', address);
    pctRequestServer('start', requestServerArguments{:});
    
    iLog('End initworker');
catch err
    % Don't use iLog here just in case it is causing the error.
    fprintf(err.getReport());
    fprintf('Exiting MATLAB due to error in initworker\n');
    % Exit MATLAB (should send SIGKILL to be sure.)
    dct_psfcns('fastexit', 39)
end
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iSetCertificate()
encodedCertificate = getenv('WORKER_CERTIFICATE');
if ~isempty( encodedCertificate )
    iLog('Adding certificate: %s', encodedCertificate);
    certStore = com.mathworks.toolbox.distcomp.mjs.security.ClientCertificateStore.getInstance();
    certStore.setCertificateEntry('SharedSecret', encodedCertificate);
end
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iCheckDataStoreExporter()
import com.mathworks.toolbox.distcomp.service.ExportConfigInfo;
import com.mathworks.toolbox.distcomp.service.ServiceExporterFactory;
import com.mathworks.toolbox.distcomp.mjs.datastore.LargeDataInvoker;

% This should have been set by initcommon
assert( LargeDataInvoker.getDesiredDataStoreSize > 0, ...
        'LargeDataInvoker.setDesiredDataStoreSize has not been set');

% Check we can create/export a DataStore - this detects problems with
% settings that otherwise won't be found until later.
minTransferSize = 1; % Any size will do
portRange = ExportConfigInfo.getPortRange();
exporterFactory = ServiceExporterFactory(portRange(1), portRange(2));
invoker = LargeDataInvoker.createLargeDataInvoker(minTransferSize, exporterFactory.createExporter());
assert( ~isempty( invoker ), ...
        'Failed to create LargeDataInvoker');

% This next line will error if the DataStore can not be exported.
invoker.getDataStoreProxy();
invoker.dispose();
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iSetPortRange()
import com.mathworks.toolbox.distcomp.control.PortConfig;
import com.mathworks.toolbox.distcomp.service.ExportConfigInfo;
basePortString = getenv('BASE_PORT');
% The minimum port to use - this skips the reserved ports
minPort = PortConfig.getMinDistcompServiceExportPort(basePortString);
% Each worker needs 2 ports, so need a large port range to accommodate
% machines running many workers. This range will allow approx 2000 
% workers which should cover us for the foreseeable future.
ExportConfigInfo.setPortRange(minPort, minPort + 4000);
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iSetSystemPropertyFromEnv(property, variable)
% Set a java system property from environment variable
value = getenv(variable);
% Will need to change this if empty string is a valid value.
if isempty(value)
    error(message('parallel:internal:cluster:InitWorkerInvalidEnvironment', variable));
end
iLog('Setting "%s" to "%s"', char(property), value);
java.lang.System.setProperty(property, value);
end


% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iInitializeLogging()

logPort = str2double( getenv('WORKER_LOG_PORT') );
if isnan(logPort)
    error(message('parallel:internal:cluster:InitWorkerInvalidEnvironment', 'WORKER_LOG_PORT'));
end

logLevel = str2double( getenv('WORKER_LOG_LEVEL') );
if isnan(logLevel)
    error(message('parallel:internal:cluster:InitWorkerInvalidEnvironment', 'WORKER_LOG_LEVEL'));
end

import com.mathworks.toolbox.distcomp.logging.*;
% Set up the handler to stream stuff back to the Worker
handler = SocketLogRecordHandler(logPort);
handler.setLevel(DistcompLevel.getLevelFromValue(logLevel));
formatter = DistcompSimpleFormatter();
handler.setFormatter(formatter);
com.mathworks.toolbox.distcomp.PackageInfo.LOGGER.addHandler(handler);

end
% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iLog(varargin)
% initworker will log at "INFO" 
logger = com.mathworks.toolbox.distcomp.worker.PackageInfo.LOGGER;
msg = sprintf(varargin{:});
logger.info(msg);
end
