function pctRunDeployedCleanup
% PCTRUNDEPLOYEDCLEANUP
%
% Force the cleanup routines for deployed applications to run.
%
% After this has been called PCT functionality should not
% be used in the current MATLAB session.
%

% Copyright 2008-2011 The MathWorks, Inc.

parallel.internal.apishared.mcrShutdownHandler( 'run' );

