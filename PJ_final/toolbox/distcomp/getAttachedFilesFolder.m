function dependencyDirectory = getAttachedFilesFolder
%getAttachedFilesFolder Get the name of the folder into which AttachedFiles are written
%   folder = getAttachedFilesFolder returns a string which is the local
%   folder into which AttachedFiles are written. This function will return
%   an empty array if not called on a worker session.
%
%   Example:
%   % Find the current AttachedFiles folder
%   folder = getAttachedFilesFolder;
%   % Change to that folder to invoke an executable 
%   oldFolder = cd(folder);
%   % Invoke the executable
%   [OK, output] = system('myExecutable');
%   % Change back to the original folder
%   cd(oldFolder);
%
% See also getCurrentCluster, getCurrentWorker, 
%          getCurrentJob, getCurrentTask, AttachedFiles.

%  Copyright 2006-2012 The MathWorks, Inc.

try
    root = distcomp.getdistcompobjectroot;
    dependencyDirectory = root.DependencyDirectory;
catch E %#ok<NASGU>
    warning(message('parallel:cluster:GetAttachedFilesFolderFailed'));
    dependencyDirectory = '';
end
