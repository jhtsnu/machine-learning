function clusters = fetchCloudClusters()
%fetchCloudClusters Returns an array of your cloud clusters
% 
%   clusters = fetchCloudClusters() returns an array of clusters that you
%   have created in Cloud Console. If you are not already logged into your
%   MathWorks account, you will be prompted to log in so that your clusters
%   can be retrieved. The clusters returned will only be those associated
%   with the MathWorks account you have logged in with.
%
%   You can also discover cloud clusters by clicking the Discover Clusters
%   button in the Cluster Profile Manager (Parallel -> Manage Cluster Profiles).
%
%   Example:
%   clusters = fetchCloudClusters();
%   % Save a cluster as a profile for later use with parcluster.
%   clusters(1).saveAsProfile('MyCloudCluster');
%
%   See also parallel.Cluster/saveAsProfile, parcluster.

% Copyright 2011-2012 The MathWorks, Inc.

clusters = parallel.cluster.MJSComputeCloud.hDiscoverClusters();

end
