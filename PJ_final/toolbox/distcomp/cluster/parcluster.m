function c = parcluster( profile )
%PARCLUSTER Build a Cluster from a cluster profile
%   OUT = parcluster returns a cluster object representing the cluster
%   identified by the default cluster profile, with the cluster object
%   properties set to the values defined in that profile.
%
%   OUT = parcluster(PROFILE) returns a cluster object representing the
%   cluster identified by the cluster profile with the specified name, with
%   the cluster object properties set to the values defined in that
%   profile.
%
%   Profiles can be created and changed using the saveProfile or
%   saveAsProfile methods on a cluster object.  Profiles can be created,
%   deleted, and changed using  the Cluster Profiles Manager.  On the Home
%   tab, in the Environment section, click Parallel > Manage Cluster
%   Profiles... to open the Cluster Profiles Manager.
%
%   Examples: 
%   % Find the cluster identified by the default parallel computing cluster 
%   % profile, with the cluster object properties set to the values defined 
%   % in that profile.
%   myCluster = parcluster();
%
%   % View the name of the default profile and find the cluster identified 
%   % by it.
%   defaultProfile = parallel.defaultClusterProfile()
%   myCluster = parcluster(defaultProfile);
%   
%   % Find a particular cluster using the profile named 'MyProfile'.
%   myCluster = parcluster('MyProfile');
%
%   See also MATLABPOOL, BATCH, parallel.defaultClusterProfile, 
%            parallel.clusterProfiles, parallel.Cluster.saveProfile, 
%            parallel.Cluster.saveAsProfile.

% Copyright 2011-2012 The MathWorks, Inc.

if nargin < 1
    s = parallel.Settings;
    profile = s.DefaultProfile;
end

try
    type = parallel.internal.settings.ProfileExpander.getClusterType( profile );
catch E
    throw( E );
end

try
    ctor = iGetConstructorFromClassName( type );
    c = ctor( 'Profile', profile );
catch E
    throw( E );
end

end


function ctor = iGetConstructorFromClassName(type)
% NB Don't use str2func for the constructor so that we don't
% confuse depfun (for compiled application support)
    import parallel.internal.types.SchedulerType
    switch type
      case SchedulerType.Generic
        ctor = @parallel.cluster.Generic;
      case SchedulerType.HPCServer
        ctor = @parallel.cluster.HPCServer;
      case SchedulerType.Local
        ctor = @parallel.cluster.Local;
      case SchedulerType.LSF
        ctor = @parallel.cluster.LSF;
      case SchedulerType.MJS
        ctor = @parallel.cluster.MJS;
      case SchedulerType.MJSComputeCloud
        ctor = @parallel.cluster.MJSComputeCloud;
      case SchedulerType.Mpiexec
        ctor = @parallel.cluster.Mpiexec;
      case SchedulerType.PBSPro
        ctor = @parallel.cluster.PBSPro;
      case SchedulerType.Torque
        ctor = @parallel.cluster.Torque;
      otherwise
        error(message('parallel:cluster:UnsupportedType', type.Name));
    end
end
