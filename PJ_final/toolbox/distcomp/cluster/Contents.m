% Functions and Classes for Parallel Computing with Clusters
%
% Profiles Functions
%    parallel.importProfile         - Import cluster profiles from file
%    parallel.exportProfile         - Export one or more profiles to file
%    parallel.defaultClusterProfile - Get or set the default parallel computing cluster profile
%    parallel.clusterProfiles       - Return the name of all valid parallel computing cluster profiles
%
% Cluster Functions
%    batch                  - Run MATLAB script or function as batch job
%    createCommunicatingJob - Create a new communicating job
%    createJob              - Create a new independent job
%    findJob                - Find job objects belonging to a cluster object
%    isequal                - True if clusters have same property values
%    matlabpool             - Open a pool of MATLAB workers on cluster
%    saveAsProfile          - Save modified properties to a profile
%    saveProfile            - Save modified properties to the current profile
%
% Job Functions
%    cancel                - Cancel a pending, queued, or running job
%    createTask            - Create a new task in a job
%    delete                - Remove a job object from its cluster and memory
%    diary                 - Display or save text of batch job
%    fetchOutputs          - Retrieve output arguments from all tasks in a job
%    findTask              - Find task objects belonging to a job
%    listAutoAttachedFiles - List the files that are automatically attached to the job
%    load                  - Load workspace variables from batch job
%    submit                - Submit job for execution in the cluster
%    wait                  - Wait for job execution to change state
%
% Task Functions
%    cancel                - Cancel a pending or running task
%    delete                - Remove a task object from its job and memory
%    listAutoAttachedFiles - List the files that are automatically attached for this task
%    wait                  - Wait for task object to change state
%
% Types of Cluster
%    parallel.Cluster                 - Base class for all Cluster objects
%    parallel.cluster.Generic         - Interact with a Generic cluster type
%    parallel.cluster.HPCServer       - Interact with a Microsoft HPC Server Cluster
%    parallel.cluster.Local           - Interact with a Local cluster
%    parallel.cluster.LSF             - Interact with a Platform LSF cluster
%    parallel.cluster.MJS             - Interact with an MJS cluster
%    parallel.cluster.MJSComputeCloud - Interact with an MJS cluster that was started on the cloud
%    parallel.cluster.Mpiexec         - Interact with mpiexec on the local host
%    parallel.cluster.PBSPro          - Interact with a PBS Pro cluster
%    parallel.cluster.Torque          - Interact with a Torque cluster
%
%    MJS clusters use the MATLAB Job Scheduler (MJS) to control the job 
%    queue and execution.  All other cluster types are forms of Common Job 
%    Scheduler (CJS).
%
% Types of Job
%    parallel.Job                     - Base class for all Job objects
%    parallel.job.CJSCommunicatingJob - Communicating job for CJS clusters
%    parallel.job.CJSIndependentJob   - Independent job for CJS clusters
%    parallel.job.MJSCommunicatingJob - Communicating job for MJS clusters
%    parallel.job.MJSIndependentJob   - Independent job for MJS clusters
%
% Types of Task
%    parallel.Task         - Base class for all Task objects
%    parallel.task.CJSTask - Task for CJS clusters
%    parallel.task.MJSTask - Task for MJS clusters
%
% Types of Worker
%    parallel.Worker            - Base class for all Worker objects
%    parallel.cluster.CJSWorker - Worker in a CJS cluster
%    parallel.cluster.MJSWorker - Worker in a MJS cluster
%
% Classes Used with Generic Clusters
%    parallel.cluster.RemoteClusterAccess - Connect to clusters when the cluster's client utilities are not available locally
%
% Functions Used by Workers when Starting and Finishing Jobs and Tasks
%    jobStartup  - Perform user-specific job startup actions on a worker
%    poolStartup - Perform user-specific pool startup actions on a worker
%    taskFinish  - Perform user-specific task cleanup actions on a worker
%    taskFinish  - Perform user-specific task cleanup actions on a worker
%
% Functions Used in MATLAB Workers
%    getAttachedFilesFolder - Get the name of the folder into which AttachedFiles are written
%    getCurrentCluster      - Get cluster object that submitted current task
%    getCurrentJob          - Get the job to which the current task belongs
%    getCurrentTask         - Get task object currently being evaluated in this worker session
%    getCurrentWorker       - Get worker object currently running this session
%
% General Functions
%    pctconfig - Configure a Parallel Computing Toolbox session
%
% See also distcomp, distcomp/parallel, distcomp/lang.

% Copyright 2004-2013 The MathWorks, Inc.
