function matlabpool( obj, varargin )
%MATLABPOOL  Open a pool of MATLAB workers on cluster
%   MATLABPOOL enables the parallel language features within the MATLAB
%   language (e.g., parfor) by starting a communicating job that connects this
%   MATLAB client session with a number of workers.
%
%   MATLABPOOL(cluster) or MATLABPOOL(cluster, 'open') starts a worker pool
%   using the identified cluster.  The pool size is determined automatically and 
%   will fall within the NumWorkersRange defined in the cluster's profile.
%   You can also specify the pool size using MATLABPOOL(cluster, 'open', 
%   poolSize). The pool size cannot exceed the NumWorkers property of the 
%   cluster and must fall within the NumWorkersRange defined in the cluster's 
%   profile.  
%
%   MATLABPOOL(cluster, poolSize) is the same as MATLABPOOL(cluster,
%   'open', poolSize and is provided for convenience.
%
%   MATLABPOOL(..., 'AttachedFiles', attachedFiles) starts a worker pool
%   and concatenates the cell array attachedFiles with the AttachedFiles
%   specified in the cluster's Profile, should one exist. Note that
%   'AttachedFiles' is case-sensitive. These files are transferred to each
%   worker and placed in the attached files directory.
%
%   Examples:
%
%   % Start a MATLAB pool on a cluster, using a pool size specified
%   % by that cluster's profile
%   myCluster = parcluster('myProfile');
%   matlabpool(myCluster);
%
%   % Start a pool of 16 MATLAB workers
%   matlabpool(myCluster, 16);
%
%   See also matlabpool, parcluster, batch, parallel.Cluster/batch.


% Copyright 2011-2012 The MathWorks, Inc.


import parallel.internal.cluster.MatlabpoolHelper
import parallel.internal.apishared.ProfileConfigHelper

validateattributes( obj, {'parallel.Cluster'}, {'scalar'}, 'matlabpool', 'cluster', 1 );

try
    pch = ProfileConfigHelper.buildApi2();
    parsedArgs = MatlabpoolHelper.parseInputsAndCheckOutputsForMethod( ...
        pch, nargout, varargin{:});
catch err
    if strcmpi(err.identifier, ...
               MatlabpoolHelper.FoundNoParseActionErrorIdentifier)
        error(message('parallel:lang:matlabpool:InvalidMatlabpoolAction'));
    end
    throw(err);
end
% Do the actual matlabpool bit
try
    % we never expect any output args from doMatlabpool on a
    % cluster
    MatlabpoolHelper.doMatlabpool(parsedArgs, obj);
catch err
    if strcmp(parsedArgs.Action, MatlabpoolHelper.OpenAction)
        ex = iGetRunValidationException(err, obj.Profile);
        throw(ex);
    else
        % Make all errors appear from matlabpool
        throw(err);
    end
end
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function ex = iGetRunValidationException(err, profName)
if isempty(profName)
    ex = MException(message('parallel:cluster:MatlabpoolCreateProfileRunValidation'));
else
    ex = MException(message('parallel:cluster:MatlabpoolRunValidation', profName));
end
ex = ex.addCause(err);
end
