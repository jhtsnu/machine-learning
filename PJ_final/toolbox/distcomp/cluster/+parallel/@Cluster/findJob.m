function varargout = findJob( obj, varargin )
%findJob  Find job objects belonging to a cluster object
%   jobs = findJob(cluster) finds all jobs in a cluster, and is identical to
%   accessing the Jobs property on the cluster.
%
%   jobs = findJob(cluster, 'p1', v1, 'p2', v2, ...) gets a 1 x N array of job
%   objects belonging to the cluster. The returned job objects will be only
%   those having the specified property-value pairs.
%
%   When a property value is specified, it must use the same format that the get
%   function returns. For example, if get returns the Name property value as
%   'MyJob', then findJob will not find that object while searching for a Name
%   property value of 'myjob' as the match is case sensitive.
%
%   [pending, queued, running, finished] = findJob(cluster) returns the jobs
%   from the cluster in four output variables, grouped by their current
%   state. (Note that jobs with state 'failed' are also included in the 4th
%   output argument).
%
%   If the cluster is contained in a remote service, findJob will result in a
%   call to the remote service. This could result in findJob taking a long time
%   to complete, depending on the number of jobs retrieved and the network
%   speed. Also, if the remote service is no longer available, an error will be
%   thrown.
%
%   Examples:
%   % Find all jobs on the default cluster that were submitted by user 'myUserName'
%   myCluster = parcluster; 
%   myJobs = findJob(myCluster, 'Username', 'myUserName');
%
%   % Find all jobs grouped by state
%   [pendingJobs, queuedJobs, runningJobs, finishedJobs] = findJob(myCluster);
%
%   % Find all running jobs that were submitted by user 'myUserName'
%   myRunningJobs = findJob(myCluster, 'Username', 'myUserName', 'State', 'running');
%
%   See also parallel.Cluster/createJob, parallel.Cluster/Jobs, parcluster, 
%            parallel.Job/findTask, parallel.Job/submit.

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.internal.customattr.WorkUnitFinder;
if ~any( nargout == [ 0 1 4 ] )
    error(message('parallel:cluster:FindJobNumOutputArgs'));
end
try
    if nargout == 4
        [jp, jq, jr, jf] = arrayfun( @(c) c.getJobsByState(), ...
                                     obj, 'UniformOutput', false );
        % each output is a cell of cells, need to unpick first to
        % straight cell array
        jp = [jp{:}]; jq = [jq{:}]; jr = [jr{:}]; jf = [jf{:}];
        % And then reconstitute into a 4-cell containing columns
        jobsCell = { [jp{:}].', [jq{:}].', [jr{:}].', [jf{:}].' };
    else
        jobsCell = { vertcat( obj.Jobs ) };
    end
catch E
    throw( E );
end

try
    varargout = cellfun( @(x) WorkUnitFinder.find(x, varargin), ...
                         jobsCell, 'UniformOutput', false );
catch E
    if strcmpi(E.identifier, 'parallel:cluster:PropertyValueInPairs')
        rethrow(E);
    end
    
    % See if we have any better luck getting each prop individually.
    matchingJobs = cellfun( @(x) iFindSingleJobFromArraryNoError(x, varargin{:}), ...
                            jobsCell, 'UniformOutput', false );
    varargout = matchingJobs;
end

end

function matchingJobs = iFindSingleJobFromArraryNoError(jobArray, varargin)
isMatchingJob = arrayfun( @(x) iIsMatchingJobNoError(x, varargin{:}), ...
                         jobArray );
% Remove empties
matchingJobs = jobArray(isMatchingJob);
end

function tf = iIsMatchingJobNoError(job, varargin)
import parallel.internal.customattr.WorkUnitFinder;

tf = false;
try
    tf = ~isempty(WorkUnitFinder.find(job, varargin));
catch err %#ok<NASGU>
    % Swallow the error and return false
end

end


