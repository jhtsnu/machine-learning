function saveProfile( obj )
%saveProfile Save modified properties to the current profile
%   saveProfile(cluster) saves the modified properties on the cluster
%   object to the profile specified by the cluster's Profile property, and
%   sets the Modified property to false.  If the cluster's Profile property
%   is empty, then an error is thrown.
%
%   Example:
%   % Create a cluster using the 'local' profile.
%   myCluster = parcluster('local');
%
%   % Change the NumWorkers property on myCluster.  
%   % The Modified property will then be true
%   myCluster.NumWorkers = 3;
%
%   % Save myCluster to back to the 'local' profile.  
%   saveProfile(myCluster);
%
%   See also parallel.cluster.Modified, parallel.cluster.saveAsProfile.

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.Cluster

validateattributes( obj, {'parallel.Cluster'}, {'scalar'}, 'saveProfile', 'cluster', 1 );

if isempty( obj.Profile )
    error(message('parallel:cluster:SaveProfileNoProfileSet'));
end

% NB Always save the profile, even if Modified is false because
% the user may have changed or deleted the profile.
cleanupOnErrorFcn = [];
try
    p  = parallel.Settings;
    if p.hProfileExists( obj.Profile )
        [schedComp, cleanupOnErrorFcn] = iGetOrCreateSchedCompForProfile( obj.Type, obj.Profile );
    else
        warning(message('parallel:cluster:SaveProfileCreatingProfile', obj.Profile));
        [~, schedComp, cleanupOnErrorFcn] = Cluster.createProfile( obj.Type, obj.Profile );
    end
    obj.savePropsToSchedulerComponent( schedComp );
catch err
    if ~isempty( cleanupOnErrorFcn )
        cleanupOnErrorFcn(); %#ok<NOEFF> - Function handle
    end
    ex = MException(message('parallel:cluster:FailedToSaveProfile', obj.Profile));
    ex = ex.addCause( err );
    throw( ex );
end

obj.setClusterToUnmodified();
end

%---------------------------------------------------------
function [schedComp, cleanupOnErrorFcn] = iGetOrCreateSchedCompForProfile( clusterType, profileName )
% If the profile's scheduler component is undefined or does not exist,
% then create one of the correct type.
% If the profile has a scheduler component of the wrong type, then error.
import parallel.Cluster

cleanupOnErrorFcn  = [];

p = parallel.Settings;
prof = p.findProfile( 'Name', profileName );

needToCreate = false;
try
    schedComp = prof.getSchedulerComponent();
    if ~strcmp( schedComp.Type, clusterType )
        error(message('parallel:cluster:SaveProfileIncorrectSchedulerComponent', clusterType, profileName, schedComp.Type, profileName));
    end
catch err
    if any( strcmp( err.identifier, ...
            {'parallel:settings:ProfileSchedulerComponentNotDefined', ...
            'parallel:settings:ProfileCannotFindSchedulerComponent'} ) )
        warning(message('parallel:cluster:SaveProfileNoSchedulerComponent', profileName, clusterType, profileName));
        needToCreate = true;
    else
        rethrow( err );
    end
end

if ~needToCreate
    return;
end

schedComp = Cluster.createSchedulerComponent( clusterType, prof.Name );
oldSchedCompName = prof.SchedulerComponent;
try
    prof.SchedulerComponent = schedComp.Name;
    cleanupOnErrorFcn = @nDeleteSchedCompAndRevertProfile;
catch err
    schedComp.delete();
    rethrow( err );
end

    function nDeleteSchedCompAndRevertProfile()
        prof.SchedulerComponent = oldSchedCompName;
        schedComp.delete();
    end
end
