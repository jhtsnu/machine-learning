function saveAsProfile( obj, profileName )
%saveAsProfile Save modified properties to a profile
%   saveAsProfile(cluster, profileName) saves the modified properties on
%   the cluster object to the profile specified by profileName, and sets
%   this cluster's Profile property to profileName and the Modified
%   property to false.  If the cluster's Profile property is not empty,
%   then the values in the original profile are also copied to the new
%   profile.
%
%   If profileName is the same as the cluster's existing Profile property,
%   then this is the same as calling saveProfile(cluster).  If profileName
%   is different from the cluster's existing Profile property, then there
%   must not already be a profile with the same name as profileName.
%
%   Examples:
%   % Create a cluster using the 'local' profile.
%   myCluster = parcluster('local');
%   % Save myCluster to a new profile with name 'localCopy'.
%   % This will change the Profile property on myCluster to
%   % 'localCopy'.
%   saveAsProfile(myCluster, 'localCopy');
%   
%   % Change the NumWorkers property on myCluster.  
%   % The Modified property will then be true
%   myCluster.NumWorkers = 3;
%   % Save myCluster to another new profile with name 'local3Workers'.
%   saveAsProfile(myCluster, 'local3Workers');
%
%   See also parallel.cluster.Modified, parallel.cluster.saveProfile.

% Copyright 2011-2012 The MathWorks, Inc.

validateattributes( obj, {'parallel.Cluster'}, {'scalar'}, 'saveAsProfile', 'cluster', 1 );
validateattributes( profileName, {'char'}, {'row'}, mfilename, 'profileName' );

% You are allowed to do c.saveAsProfile( obj.Profile ) (this is just saveProfile) 
% but not c.saveAsProfile('anotherExistingName')
if strcmp( profileName, obj.Profile );
    try
        obj.saveProfile();
        return;
    catch err
        throw( err );
    end
end

p = parallel.Settings;
if p.hProfileExists( profileName )
    allNames = p.hGetAllProfileNames();
    error( message( 'parallel:cluster:SaveAsProfileAlreadyExists', ...
        profileName, profileName, profileName, ...
        strtrim( sprintf( '%s ', allNames{:} ) ) ) );
end

cleanupOnErrorFcn = [];
try
    [schedComp, cleanupOnErrorFcn] = iCreateProfileAndCopyProject( obj.Type, obj.Profile, profileName );
    obj.savePropsToSchedulerComponent( schedComp );
catch err
    if ~isempty( cleanupOnErrorFcn )
        cleanupOnErrorFcn(); %#ok<NOEFF> - function handle
    end

    ex = MException(message('parallel:cluster:FailedToSaveProfile', profileName));
    ex = ex.addCause( err );
    throw( ex );
end

% Set the current profile to be this new name
obj.Profile = profileName;
obj.setClusterToUnmodified();

end
%----------------------------------------------------------
function [schedComp, cleanupOnErrorFcn] = iCreateProfileAndCopyProject( clusterType, origProName, newProfName )
import parallel.Cluster
[newProf, schedComp, createProfCleanup] = Cluster.createProfile( clusterType, newProfName );

% Copy the project component from the original profile, if there is one.
newPcName = '';
cleanupOnErrorFcn = @nCleanupOnError;

p = parallel.Settings;
origProf = p.findProfile( 'Name', origProName );
if isempty( origProf )
    return;
end

try
    origPC = origProf.getProjectComponent();
catch err
    if strcmp( err.identifier, 'parallel:settings:ProfileCannotFindProjectComponent' )
        warning(message('parallel:cluster:SaveNonExistentProjectComponent', origProf.Name, newProfName));
        return;
    else
        nCleanupOnError();
        rethrow( err );
    end
end

if isempty( origPC )
    return;
end

% Really copy the project component
proposedName = sprintf( '%sProjectComponent', newProfName );
newPcName = p.hGetUnusedProjectComponentName( proposedName );
try
    origPC.saveAs( newPcName );
    newProf.ProjectComponent = newPcName;
catch err
    nCleanupOnError();
    rethrow( err );
end

    function nCleanupOnError()
        createProfCleanup();
        if ~isempty( newPcName )
            pc = p.findProjectComponent( 'Name', newPcName );
            if ~isempty( pc )
                pc.delete();
            end
        end
    end
end
