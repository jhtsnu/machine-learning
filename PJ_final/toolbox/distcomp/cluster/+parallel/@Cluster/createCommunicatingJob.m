function job = createCommunicatingJob( obj, varargin )
%createCommunicatingJob Create a new communicating job
%    job = createCommunicatingJob(cluster) creates a communicating job object
%    for the identified cluster.
%
%    job = createCommunicatingJob(..., 'p1', v1, 'p2', v2, ...) creates a
%    communicating job object with the specified property values.
%
%    job = createCommunicatingJob(..., 'Type', 'Pool', ...) creates a
%    'Pool'-type communicating job. This is the default if 'Type' is not
%    specified. A 'Pool'-type job runs the specified task function with a MATLAB
%    Pool available to run the body of PARFOR loops or SPMD blocks.
%
%    job = createCommunicatingJob(..., 'Type', 'SPMD', ...) creates an
%    'SPMD'-type communicating job where the specified task function is run
%    simultaneously on each worker, and lab* functions can be used for
%    communication.
%
%    job = createCommunicatingJob(..., 'Profile', 'profileName',...)  creates a
%    communicating job object with the property values specified in the profile
%    'profileName'.  If not specified and the cluster has a 'Profile' specified,
%    then the cluster's profile is automatically applied.
%
%    Example:
%    % Consider the function 'myFunction' which uses PARFOR:
%    function result = myFunction(N)
%        result = 0;
%        parfor ii=1:N
%           result = result + max(eig(rand(ii)));
%        end
%    end
%
%    % Create a communicating job object to evaluate myFunction on the cluster.
%    myCluster = parcluster; % create the default cluster object
%    j = createCommunicatingJob(myCluster); % use the default 'Pool' type
%
%    % Add the task to the job, supplying an input argument
%    createTask(j, @myFunction, 1, {100});
%
%    % Set the number of workers required for parallel execution.
%    j.NumWorkersRange = [5 10];
%
%    % Run the job.
%    submit(j);
%
%    % Wait until the job is finished.
%    wait(j);
%
%    % Retrieve and display task results.
%    out = fetchOutputs(j)
%
%    % Delete the job.
%    delete(j);
%
%    See also parcluster, parallel.Job/createTask, parallel.Job/submit,
%             parallel.Cluster/createJob.

% Copyright 2011 The MathWorks, Inc.

import parallel.internal.types.Variant
import parallel.internal.customattr.PropSet

validateattributes( obj, {'parallel.Cluster'}, {'scalar'}, 'createCommunicatingJob', 'cluster', 1 );

try
    % 'Type' is "special", we need to tease that out before we can even
    % build the job.
    [gotVariant, userVariant, otherArgs] = PropSet.extractOneProperty( ...
        'Type', varargin{:} );
    if gotVariant
        variant = Variant.fromNameI( userVariant );
        if ~ ( variant == Variant.CommunicatingPoolJob || ...
               variant == Variant.CommunicatingSPMDJob )
            error(message('parallel:cluster:InvalidType', userVariant));
        end
    else
        variant = Variant.CommunicatingPoolJob;
    end
    job = obj.buildCommunicatingJob( variant );
    obj.applyJobProperties( job, otherArgs{:} );
catch E
    throw( E );
end
end
