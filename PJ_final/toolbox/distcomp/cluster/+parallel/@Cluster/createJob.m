function job = createJob( obj, varargin )
%createJob  Create a new independent job
%    job = createJob(cluster) creates an independent job object for
%    the identified cluster.
%
%    job = createJob(..., 'p1', v1, 'p2', v2, ...) creates a job
%    object with the specified property values.
%
%    job = createJob(..., 'Profile', 'profileName', ...)  creates an
%    independent job object with the property values specified in the profile
%    'profileName'. If not specified and the cluster has a 'Profile' specified,
%    then the cluster's profile is automatically applied.
%
%    Example:
%    % Create an independent job object.
%    myCluster = parcluster; % create the default Cluster object
%    j = createJob(myCluster, 'Name', 'testjob');
%
%    % Add tasks to the job.
%    for i = 1:10
%        createTask(j, @rand, 1, {10});
%    end
%
%    % Run the job.
%    submit(j);
%
%    % Wait until the job is finished.
%    wait(j);
%
%    % Retrieve job results.
%    out = fetchOutputs(j);
%
%    % Display the random matrix.
%    disp(out{1, 1});
%
%    % Destroy the job.
%    delete(j);
%
%    See also parcluster, parallel.Job/createTask, parallel.Job/submit,
%             parallel.Cluster/createCommunicatingJob.

% Copyright 2011 The MathWorks, Inc.

validateattributes( obj, {'parallel.Cluster'}, {'scalar'}, 'createJob', 'cluster', 1 );

try
    % Simply ask the cluster to build a new job, and then we apply the
    % properties. Note that applyJobProperties destroys the job if
    % property application fails.
    job = obj.buildIndependentJob();
    obj.applyJobProperties( job, varargin{:} );
catch E
    throw( E );
end
end
