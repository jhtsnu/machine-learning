function job = createIndependentJob( obj, varargin )
%createIndependentJob Create Independent job

% Copyright 2011 The MathWorks, Inc.

validateattributes( obj, {'parallel.Cluster'}, {'scalar'}, 'createIndependentJob', 'cluster', 1 );

try
    % Simply ask the cluster to build a new job, and then we apply the
    % properties. Note that applyJobProperties destroys the job if
    % property application fails.
    job = obj.buildIndependentJob();
    obj.applyJobProperties( job, varargin{:} );
catch E
    throw( E );
end
end
