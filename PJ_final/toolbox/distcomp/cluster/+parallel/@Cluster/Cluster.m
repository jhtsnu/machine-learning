%Cluster Base class for all Cluster objects
%   parallel.Cluster is the base class for all cluster objects. Specific types
%   of cluster can be constructed either by creating a profile and using the
%   PARCLUSTER function, or by calling the subclass constructor directly.
%
%   All parallel.Cluster subclass constructors accept property-value pairs as
%   arguments. Supported properties depend on the specific subclass.
%
%   parallel.Cluster methods:
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.Cluster properties:
%      ClusterMatlabRoot                - Specifies the path to MATLAB for workers to use
%      Host                             - Host of the cluster's headnode
%      JobStorageLocation               - Location where cluster stores job and task information
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use with MathWorks hosted licensing
%      Modified                         - True if any properties in this cluster have been modified
%      NumWorkers                       - Number of workers available for this cluster
%      OperatingSystem                  - Operating system of the nodes used by the cluster
%      Profile                          - Profile used to build this cluster
%      RequiresMathWorksHostedLicensing - Cluster uses MathWorks hosted licensing
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%
%   parallel.Cluster types:
%      parallel.cluster.Generic         - Interact with a Generic cluster type
%      parallel.cluster.HPCServer       - Interact with a Microsoft HPC Server Cluster
%      parallel.cluster.Local           - Interact with a Local cluster
%      parallel.cluster.LSF             - Interact with a Platform LSF cluster
%      parallel.cluster.MJS             - Interact with an MJS cluster
%      parallel.cluster.MJSComputeCloud - Interact with an MJS cluster that was started using on the cloud
%      parallel.cluster.Mpiexec         - Interact with mpiexec on the local host
%      parallel.cluster.PBSPro          - Interact with a PBS Pro cluster
%      parallel.cluster.Torque          - Interact with a Torque cluster
%
%   See also PARCLUSTER, BATCH, MATLABPOOL.

% Copyright 2011-2012 The MathWorks, Inc.

classdef Cluster < handle ...
        & matlab.mixin.Heterogeneous ...               % We can concatenate disparate Clusters
        & parallel.internal.cluster.CustomPropDisp ... % For display
        & parallel.internal.customattr.CustomGetSet    % For PCT* property attributes

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Implementing properties for Cluster subclasses:
    % * All Profile properties must be PCTGetSet and Transient
    % * All such properties specified are liable to be saved into a resulting Profile
    % * All other implementation properties must not be PCTGetSet
    % * All Profile properties must be permissible P-V pairs during construction
    % * The constructed object must faithfully reflect the values specified
    %   during construction
    % * The constructor should use ConstructorArgsHelper.interpretClusterCtorArgs as
    %   this will perform constraint checks as well as checking for bad properties
    % * All constructors should invoke the Cluster constructor and then, for each
    %   PCTGetSet property defined in that class, call obj.PropStorage.addFactoryValue()
    % * Any P-V pairs requiring special handling should then handle those, updating
    %   the PropStorage if necessary to indicate the source of the value
    % * The constructor should then invoke obj.applyConstructorArgs( propsFromArgs )
    %   which will call hSetProperty for each property remaining, as well as
    %   indicating correctly to the PropStorage where the arg value came from.
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Common parallel.Cluster properties

    % Several properties of all Clusters are selectable only at construction
    % time. Therefore, they also must be specified in the Cluster constructor.
    properties ( SetAccess = immutable )
        %Type Type of this cluster
        %   The type of a cluster is the last part of the cluster class name,
        %   without the 'parallel.cluster' prefix.
        Type
    end

    properties ( Transient, Dependent, SetAccess = private )
        %Profile Profile used to build this cluster
        %   This property contains the name used to build this cluster, or it
        %   is empty if none was used. This is the profile to which values
        %   will be saved when calling 'saveProfile'. Properties defined by
        %   this profile will be applied to all job and task objects
        %   constructed by this cluster.
        %
        %   See also parallel.Cluster.saveProfile, 
        %            parallel.Cluster.saveAsProfile, 
        %            PARCLUSTER.
        Profile
    end

    % Common public properties, declared Abstract as these might be PCTGetSet in
    % subclasses
    properties ( Abstract )
        %NumWorkers Number of workers available for this cluster
        %   NumWorkers indicates the number of workers available to the
        %   cluster for running your jobs. NumWorkers must fall within the
        %   range defined by a job's NumWorkersRange.
        %   (read-only)
        %
        %   See also parallel.CommunicatingJob.NumWorkersRange
        NumWorkers

        %ClusterMatlabRoot Specifies the path to MATLAB for workers to use
        %   ClusterMatlabRoot specifies the path name to MATLAB for the
        %   cluster to use for starting MATLAB worker processes. 
        %
        %   See also parallel.Cluster.JobStorageLocation
        ClusterMatlabRoot

        %OperatingSystem Operating system of the nodes used by the cluster
        %   OperatingSystem specifies the operating system of the nodes in
        %   the cluster. Valid values are 'windows', 'unix', and 'mixed'.
        %
        %   See also parallel.Cluster.Host
        OperatingSystem

        %JobStorageLocation Location where cluster stores job and task information
        %
        %   See also parallel.Cluster.ClusterMatlabRoot
        JobStorageLocation
        
        %LicenseNumber License number to use with MathWorks hosted licensing
        %   License number specifies the license number to use on the workers
        %   when the cluster is using MathWorks hosted licensing.  This string
        %   must be the same as one of the licenses listed in your MathWorks
        %   account.
        %
        %   LicenseNumber cannot be defined if RequiresMathWorksHostedLicensing is
        %   false.
        %    
        %   See also parallel.Cluster.RequiresMathWorksHostedLicensing
        LicenseNumber
        
        %RequiresMathWorksHostedLicensing Cluster uses MathWorks hosted licensing
        %   RequiresMathWorksHostedLicensing indicates whether the cluster
        %   uses MathWorks hosted licensing.  You must provide your MathWorks
        %   account login information if this value is true.  If you have more
        %   than one MDCS license associated with your MathWorks account
        %   you will also need to supply the LicenseNumber to use.
        %
        %   See also parallel.Cluster.LicenseNumber
        RequiresMathWorksHostedLicensing
    end
    properties ( Abstract, SetAccess = immutable )
        %Host Host of the cluster's headnode
        %   You can use the host of the cluster in a profile to find a
        %   desired cluster when creating the cluster object with
        %   parcluster or the cluster constructor.
        %
        %   See also PARCLUSTER
        Host
    end

    properties ( Transient )
        %UserData Data associated with a cluster object in this client session
        %   UserData can be used to store any data associated with a cluster
        %   object. This data is stored in the client MATLAB session, and is
        %   not available on the workers. UserData is not stored in a profile.
        %
        %   Commands such as 
        %       clear all 
        %       clear functions 
        %   will clear an object in the local session, permanently removing
        %   the data in the UserData property.
        %   
        %   The default value is an empty vector.
        %   
        %   See also CLEAR
        UserData = [];
    end

    properties ( Dependent, SetAccess = private )
        %Jobs List of jobs contained in this cluster
        %   An array of all the job objects in the cluster. Job objects
        %   will be in the order indicated by their ID property, consistent
        %   with the sequence in which they were created, regardless of
        %   their State.
        %
        %   See also parallel.Cluster.createJob,
        %            parallel.Cluster.createCommunicatingJob,
        %            parallel.Cluster.findJob,
        %            parallel.Job.submit, parallel.Job.delete.
        Jobs

        %Modified True if any properties in this cluster have been modified
        %   A cluster is 'Modified' if properties have been changed since it was
        %   created using a profile. If no profile was used, then the cluster
        %   will be modified if any properties are not in their default
        %   state. Use 'saveProfile' to modify the underlying profile.
        %
        %   See also parallel.Cluster.saveProfile,
        %            parallel.Cluster.Profile.
        Modified
    end
    
    properties ( Transient, Access = private )
        ProfileName
    end

    methods % Get/Set for Dependent
        function jobs = get.Jobs( obj )
        % Use the abstract method to retrieve the Jobs list.
            try
                jobs = obj.getJobs();
                jobs = reshape( jobs, numel(jobs), 1 );
            catch E
                throw( E );
            end
        end
        function tf = get.Modified( obj )
            tf = obj.PropStorage.UserModified;
        end
        
        function name = get.Profile( obj )
            validName = iGetValidProfileName(obj.ProfileName);
            if isempty(validName) && ~strcmp(validName, obj.ProfileName)
                warning(message('parallel:cluster:ProfileDeleted', obj.ProfileName));
                obj.ProfileName = validName;
            end
            name = obj.ProfileName;
        end
        
        function set.Profile( obj, profileName )
            if ~isempty( profileName )
                profileName = iGetValidProfileName(profileName);
            end
            
            obj.ProfileName = profileName;
        end
    end

    properties ( Transient, GetAccess = protected, SetAccess = immutable )
        PropStorage % Used for PCTGetSet properties, and tracking properties
                    % set from Profiles or during construction.
    end
    
    properties ( Transient, Access = protected )
        LicenseEntitlement = parallel.internal.webclients.Entitlement.empty();
    end

    properties ( Constant, GetAccess = private )
        EnforceInitClient = distcomp.pGetDefaultUsername();
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Cluster abstract methods

    % These methods must be provided by cluster implementations:
    methods ( Abstract, Access = protected )
        % Return a list of parallel.Job objects:
        jobs  = getJobs( obj )

        % parallel.Cluster will use this method when a new
        % parallel.IndependentJob is to be built.
        job  = buildIndependentJob( obj )

        % parallel.Cluster will use this method when a new
        % parallel.CommunicatingJob is to be built. variant will be either Pool
        % or Spmd.
        job  = buildCommunicatingJob( obj, variant )

        % build a ...pmode.poolmessaging.ConnectionManager
        connMgr = buildConnectionManager( obj )
    end
    
    methods ( Abstract, Hidden )
      % The memento data must be a struct whose fieldnames are the
      % properties necessary to make a cluster object of the required type.
      % The values of the struct must be the values for the properties in
      % the fieldnames of the struct. The memento data is a struct of 
      % fieldnames and values that correspond to PV pairs which will go 
      % into the cluster constructor. 
        clusterMemento = hGetMementoData( obj )
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % helper methods
    methods ( Access = private )
        function applyJobProperties( obj, job, varargin )
        % This method is used when constructing a new job. This method is used
        % to set all the additional properties specified during a call to
        % create*Job. If anything goes wrong setting the properties, it will
        % throwAsCaller the error.

        % If the Cluster has a 'Profile', we prepend that to the job "set" args.
            if ~isempty( obj.Profile )
                jobArgs = [ 'Profile', obj.Profile, varargin ];
            else
                jobArgs = varargin;
            end

            if isempty( jobArgs )
                % Short-circuit if nothing to do.
                return
            end

            try
                % We use the Hidden 'hSetWithAppend' method of parallel.Job to
                % set the properties.
                doAppend = true;
                job.hSetWithAppend( doAppend, jobArgs{:} );
            catch E
                try
                    job.delete();
                catch E2 %#ok<NASGU>
                    % We're about to throw the original error.
                end
                % Caller is top-level entry-point
                throwAsCaller( E );
            end
        end

        function [n, v] = getModifiedProps( obj )
        % Return a names and values of all the modified properties on this
        % Cluster.
            obj.updateDerivedProfileProperties();

            [n, v] = obj.PropStorage.getProfileProperties();
        end

        function savePropsToSchedulerComponent( obj, schedComp )
        % Save the cluster's properties to the supplied scheduler component.
            import parallel.internal.types.SettingsLevel;
            obj.updateDerivedProfileProperties();

            % Start by unsetting all user values - this ensures all props
            % will be in the "factory" state.  (This gives the same end 
            % result as only unsetting the PropStorage Factory values.)
            % Then set all user values and only set profile values if
            % they are not already set.
            [uN, uV] = obj.PropStorage.getUserProperties();
            [pN, pV] = obj.PropStorage.getProfileProperties();
            
            userLevel = SettingsLevel.User;
            restorePoint = schedComp.hGetRestorePoint(userLevel);
            try
                schedComp.unsetAll( userLevel.Name );
                schedComp.hSetFromCluster( uN, uV );
                schedComp.hSetFromClusterIfNotAlreadySet( pN, pV );
            catch err
                schedComp.hRestore(restorePoint);
                rethrow(err);
            end
        end
        
        function setClusterToUnmodified( obj )
            obj.PropStorage.setNonFactoryAsProfile();
        end
    end
    
    methods ( Static, Access = private )
        function [prof, schedComp, cleanupOnErrorFcn] = createProfile( type, profileName )
            import parallel.Cluster
            schedComp = Cluster.createSchedulerComponent( type, profileName );
            p = parallel.Settings;
            try
                prof = p.createProfile( profileName, schedComp.Name );
            catch err
                schedComp.delete();
                rethrow( err );
            end

            cleanupOnErrorFcn = @nDeleteProfileAndSchedComp;

            function nDeleteProfileAndSchedComp()
                prof.delete();
                schedComp.delete();
            end
        end

        function schedComp = createSchedulerComponent( type, profileName )
            p = parallel.Settings; 
            proposedName = sprintf( '%sSchedulerComponent', profileName );
            scName = p.hGetUnusedSchedulerComponentName( proposedName );
            schedComp = p.createSchedulerComponent( type, scName );
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Protected access methods.
    methods ( Access = protected )
        function varargout = getJobsByState( obj )
        % Default implementation of getting jobs partitioned by their
        % State. This implementation is the obvious, simple
        % implementation. Cluster subclasses may be able to override this
        % implementation with something more efficient.

        % Note that we bundle 'failed' jobs in with 'finished'. 'unavailable'
        % jobs are dropped.
            import parallel.internal.types.States;

            allJobs   = obj.getJobs();
            % NB arrayfun doesn't support 'States' as an output type
            jobStates = arrayfun( @(x) x.StateEnum, allJobs, 'UniformOutput', false );
            jobStates = [ jobStates{:} ];
            states    = [ States.Pending, ...
                          States.Queued, ...
                          States.Running, ...
                          States.Finished, ...
                          States.Failed ];
            pqrff     = arrayfun( @(state) allJobs( jobStates == state ), states, ...
                                  'UniformOutput', false );
            % Need to concatenate Finished/Failed
            if nargout == 4
                varargout = { pqrff(1), pqrff(2), pqrff(3), {[pqrff{4:end}]} };
            else
                varargout = { [ pqrff(1:3), {[pqrff{4:end}]} ] };
            end
        end

        function E = buildCannotModify( obj, propertyName )
        % buildCannotModify use this if your Cluster subclass does not support
        % modification of a property.
            validateattributes( propertyName, {'char'}, {'row'} );
            m = message( 'MATLAB:class:SetProhibited', propertyName, ...
                         class( obj ) );
            E = MException( m.Identifier, '%s', m.getString() );
        end
        
        function tf = isNotCurrentValue( obj, name, value )
            validateattributes( name, {'char'}, {'row'} );
            tf = ~isequal( obj.(name), value );
        end

        function E = buildInvalidValue( ~, propertyName )
        % buildInvalidValue use this if your Cluster subclass does not support
        % modification of a property and it was set to a value other than the
        % current one
            validateattributes( propertyName, {'char'}, {'row'} );
            m = message( 'parallel:cluster:SetInvalidValueForProperty', propertyName );
            E = MException( m.Identifier, '%s', m.getString() );
        end

        function applyConstructorArgs( obj, propsFromArgs )
        % NB that even though very nearly all propsFromArgs are properties to be
        % stored in Profiles, UserData may appear here. So we need to be a
        % little more careful.
            import parallel.internal.customattr.Reflection

            names          = propsFromArgs.keys();
            valuesAndTypes = propsFromArgs.values();
            [props, useGetSet] = Reflection.getPublicProperties( obj );
            % Reorder the properties if necessary.
            [names, valuesAndTypes] = obj.reorderForSet( names, valuesAndTypes );

            try
                for ii = 1:length( names )
                    name         = names{ii};
                    idx          = find( strcmp( name, props ) );
                    if length( idx ) ~= 1
                        % Never get here, invalid props should already be stripped.
                        error(message('parallel:cluster:ConstructorArgInvalid', name));
                    end
                    thisGetSet   = useGetSet(idx);
                    valueAndType = valuesAndTypes{ii};
                    if thisGetSet
                        % use 'hSetProperty' here to give hSetProperty a chance to
                        % object. e.g. Local and NumWorkers.
                        obj.hSetProperty( name, valueAndType{1} );
                        obj.PropStorage.setValueType( name, valueAndType{2} );
                    else
                        obj.(name) = valueAndType{1};
                    end
                end
            catch E
                throwAsCaller( E );
            end
        end

        function updateDerivedProfileProperties( ~ )
        % updateDerivedProfileProperties this is to allow Clusters to have
        % properties that can be specified in a Profile, but also might be
        % 'derived' in the sense that they may be modified behind the scenes.
        % The motivating example here is the Local cluster which has
        % 'NumWorkers' which may be both specified by a Profile, but the true
        % value must be retrieved from the underlying scheduler. Subclasses
        % overriding this method should use the
        % ClusterProps.setValueAtSameLevel() method so that the value-type is
        % not modified, but the value is.
        end

        function tf = scalarIsequal( obj1, obj2 )
        % Compare two scalar Cluster objects for 'isequal' behavior.

            import parallel.internal.customattr.Reflection

            if isequal( class( obj1 ), class( obj2 ) )
                % Same class, compare properties *except* 'Jobs'.
                allProps = Reflection.getAllPublicProperties( obj1 );
                allPropsExceptJobs = setdiff( allProps, 'Jobs' );

                vals1  = get( obj1, allPropsExceptJobs );
                vals2  = get( obj2, allPropsExceptJobs );

                tf = isequal( vals1, vals2 );
            else
                tf = false;
            end
        end

        function tf = isEquivalentWorkers( obj1, obj2 )
        % isEquivalentWorkers returns TRUE if two clusters are equivalent
        % - in the sense that they submit to the same pool of workers.  obj1 and
        % obj2 are guaranteed to be of the same class. Clusters should override
        % this if they know a better way than to use the ISEQUAL test.
            tf = isequal( obj1, obj2 );
        end

        function tf = isEquivalentStorage( obj1, obj2 )
        % isEquivalentStorage takes two Clusters of the same class
        % and returns TRUE iff they store jobs in the same place.
            tf = isequal( obj1, obj2 );
        end

        function obj = Cluster( type, cProf )
        % Cluster superclass constructor for all Cluster types. This is Hidden
        % because there is no customer-visible behavior here.
            
            try
                parallel.internal.cluster.initializeClient();
            catch err
                throw(err);
            end
        
            % This will error in a deployed application if
            % this is the second MCR component in the application.
            parallel.internal.apishared.mcrShutdownHandler( 'initialize' );
            
            validateattributes( type, {'char'}, {'row'} );
            validateattributes( cProf, {'char'}, {} ); % Could be empty

            % simply set immutable properties.
            obj.Type        = type;
            obj.Profile     = cProf;
            obj.PropStorage = parallel.internal.settings.ClusterProps();
        end

        function entitlement = checkLicenseNumber( obj, licenseNumber )
            if ~obj.RequiresMathWorksHostedLicensing
                error( message( 'parallel:cluster:MHLMLicenseNotRequired' ) );
            end
            % Empty license numbers are allowed, no need to check.
            if isempty( licenseNumber )
                entitlement = parallel.internal.webclients.Entitlement.empty();
                return;
            end

            desktopClient = parallel.internal.webclients.currentDesktopClient();
            % Use the desktop client to make the web services calls and return a list of the valid values
            % so we can add these to any error messages.
            [ok, entitlement, validValues] = desktopClient.checkLicenseNumber( licenseNumber );
            if ok
                return;
            end
            
            % If there were no valid values then we have no MDCS licenses, and we should error
            if isempty( validValues )
                error( message( 'parallel:cluster:NoMDCSLicense' ) );
            else
                % If we had valid licenses put didn't pick one, we should error, letting the user know which
                % license numbers they can choose.
                if length( validValues ) > 1
                    validValues = [ sprintf( '  %s\n', validValues{1:end-1} ), sprintf( '  %s', validValues{end} ) ];
                else
                    validValues = sprintf( '  %s', validValues{1} );
                end
                error( message( 'parallel:cluster:InvalidLicenseNumber', licenseNumber, validValues ) );
            end
        end
        
        function ensureLicenseNumberIsSetOrError( obj )
            if ~obj.RequiresMathWorksHostedLicensing
                return
            end
            
            desktopClient = parallel.internal.webclients.currentDesktopClient();
            if ~desktopClient.isLoggedIn();
                error ( message( 'parallel:cluster:ClusterRequiresWebLicensing' ) );
            end
            
            if ~isempty( obj.LicenseNumber )
                return
            end

            % Get the license number to use.
            % NB We'll always get a non-empty entitlement back
            obj.LicenseEntitlement = desktopClient.Entitlement;
            obj.hSetPropertyNoCheck( 'LicenseNumber', obj.LicenseEntitlement.LicenseNumber );
        end
        
        function [names, valuesAndTypes] = reorderForSet( ~, names, valuesAndTypes )
        % [needsReorder, reorderIdx] = getNewPropOrderForSet( obj, names )
        % reorderPropsForSetIfRequired will determine if the supplied property names 
        % need re-ordering for the purposes of the set (e.g. because there are props that
        % are dependent on one another).  There must be only 1 instance of each
        % property in the names (PropSet.amalgamate must already have been called).
        %
        % Returns an array of indices such that
        %     reorderedNames = names(reorderIdx)
        %
        % Intended to be called from applyConstructorArgs.  
        % Subclasses can override this if required.
        
            if numel( unique( names ) ) ~= numel( names )
                error( message( 'parallel:internal:cluster:ReorderPropertiesForSetNotUnique' ) );
            end
            newOrderIdx  = 1:numel( names );
            % Must set LicenseNumber after RequiresMathWorksHostedLicensing because
            % the LicenseNumber can only be validated if RequiresMathWorksHostedLicensing
            % is true.
            propsInOrder = {'RequiresMathWorksHostedLicensing', 'LicenseNumber'};

            % If the names don't contain all the props that require re-ordering,
            % then just return early.
            [~, ~, namesIdx] = intersect( propsInOrder, names , 'stable' );
            if numel( namesIdx ) < numel( propsInOrder )
                return;
            end
            
            % Pull out the propsInOrder and move them to the end of the list
            % in the correct order.
            newOrderIdx(namesIdx) = [];
            newOrderIdx           = [newOrderIdx(:); namesIdx(:)];
            names = names(newOrderIdx);
            valuesAndTypes = valuesAndTypes(newOrderIdx);
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Static, sealed protected methods
    methods (Access = protected, Static, Sealed)
        function obj = getDefaultScalarElement() %#ok<STOUT>
            % getDefaultScalarElement is required for matlab.mixin.Heterogeneous
            % to ensure that gaps in arrays are filled in with valid objects.
            error(message('parallel:cluster:ArrayWithGapsNotAllowed', 'parallel.Cluster'));
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Undocumented/Hidden public parallel.Cluster API
    methods ( Hidden )
        function v = hGetProperty( obj, name )
        % Return one or more properties of the CJSCluster.

            obj.updateDerivedProfileProperties();   
            if ~iscell( name )
                v = obj.PropStorage.getValue( name );
            else
                v = cellfun( @(x) obj.PropStorage.getValue( x ), ...
                             name, 'UniformOutput', false );
            end
        end
        function hSetPropertyNoCheck( obj, names, values )
        % Simply apply the property at the "User" level.
            if ~iscell( names )
                names  = { names };
                values = { values };
            end
            cellfun( @(n, v) obj.PropStorage.setUserValue( n, v ), ...
                     names, values );
        end
        function tf = hIsVariantSupported(~, variant)
        % TODO:later remove when Mpiexec is removed.  (Required for
        % validation)
            validateattributes(variant, {'parallel.internal.types.Variant'}, {'scalar'});
            % Default to true most clusters
            tf = true;
        end
        function tf = hEquivalentWorkers( obj1, obj2 )
        %hEquivalentWorkers TRUE if clusters are equivalent
        % in the sense that they submit to the same pool of workers.
        % tf = hEquivalentWorkers(obj1, obj2) requires two scalar Cluster instances.
        % Uses isEquivalentWorkers if obj1 and obj2 are of the same class.
            tf = isequal( class( obj1 ), class( obj2 ) ) && ...
                 isEquivalentWorkers( obj1, obj2 );
        end
        function tf = hEquivalentStorage( obj1, obj2 )
        %hEquivalentStorage TRUE if clusters store jobs/tasks in the same place
        % tf = hEquivalentStorage(obj1, obj2) requires two scalar Cluster instances.
        % Uses isEquivalentStorage if obj1 and obj2 are of the same class.
            tf = isequal( class( obj1 ), class( obj2 ) ) && ...
                 isEquivalentStorage( obj1, obj2 );
        end
        
        function hSetLicenseEntitlementNoCheck( obj, entitlement )
        %hSetLicenseEntitlement set the license number based on the entitlement.
        % Called by workers in distcomp_evaluate_*task to set the license entitlement
        % and the license number.  Does NOT do any checking that the supplied entitlement 
        % is valid (i.e. no web service calls).
            obj.LicenseEntitlement = entitlement;
            obj.hSetPropertyNoCheck( 'LicenseNumber', entitlement.LicenseNumber );
        end
        
        % Add Cluster Properties to Display
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            % Create an empty map to hold displayable cluster properties
            clusterPropertyMap = containers.Map('KeyType', 'char', 'ValueType', 'any');
           
            % The cluster base class is never displayed, so it does not
            % have a specified property display order to put into propNames
            propNames = {};
            
            % Add default displayable items to map.
            names = {...
                'Profile', ...
                'Modified', ...
                'Host', ...
                'NumWorkers', ...
                'RequiresMathWorksHostedLicensing', ...
                'LicenseNumber', ...
                'ClusterMatlabRoot', ...
                'OperatingSystem'};
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            clusterPropertyMap = [clusterPropertyMap; containers.Map(names, values)];
            
            % Add non-default display items to map 
            clusterPropertyMap('JobStorageLocation') = diFactory.createJobStorageStructItem(obj.hGetDisplayPropertiesNoError('JobStorageLocation')); 
            % 'Separator' is not a typical name-value object. However, it
            % needs to be added to the propNames list in between property 
            % groups, so it is put into the map with the other 
            % displayable items.  
            clusterPropertyMap('Separator') = diFactory.createSeparator();  
        end
    end
    
    methods ( Hidden, Sealed )
         function link = hGetLink(obj, displayString)        
            serializedMemento = serialize(parallel.internal.display.ClusterMemento(obj));
            matlabFunction = sprintf('%s(''%s'')', ...
                parallel.internal.display.Displayer.DisplayObjectFunction, serializedMemento);
            link = parallel.internal.display.HTMLDisplayType(displayString, matlabFunction);
         end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Documented, public parallel.Cluster API. Sealed to prevent unfortunate messages.
    methods ( Sealed )
        job = batch( obj, scriptName, varargin )
        matlabpool( obj, varargin )
        varargout = findJob( obj, varargin )
        job = createJob( obj, varargin )
        job = createCommunicatingJob( obj, varargin )
        saveProfile( obj )
        saveAsProfile( obj, profileName )
    end
    methods ( Hidden, Sealed )
        job = createIndependentJob( obj, varargin )
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Simple public method implementations
    methods ( Sealed )
        function tf = isequal( varargin )
        %ISEQUAL True if clusters have same property values
        %    ISEQUAL(C1,C2) returns logical 1 (TRUE) if clusters C1 and C2
        %    have the same property values, and logical 0 (FALSE) otherwise.
        %
        %    ISEQUAL(C1,C2,C3,...) returns TRUE if all clusters are equal.
        %
        %    ISEQUAL can operate on arrays of clusters. In that case, each
        %    element of the arrays are compared.
        %
        %    When comparing clusters, ISEQUAL does not compare the contents of
        %    the 'Jobs' property.
        %
        %    See also ISEQUAL, PARCLUSTER.

            narginchk(1, Inf);
            szsCell  = cellfun( @size, varargin, 'UniformOutput', false );
            szsMatch = all( cellfun( @(x) isequal(szsCell{1}, x), szsCell(2:end) ) );
            if ~szsMatch
                tf = false;
                return
            end
            allClusters = all( cellfun( @(x) isa(x, 'parallel.Cluster'), varargin ) );
            if ~allClusters
                tf = false;
                return
            end

            % Actually need to compare
            tf = true;
            obj1 = varargin{1};
            argToCompare = 2;
            try
                % Outer loop over argument list
                while tf && argToCompare <= nargin
                    obj2 = varargin{argToCompare};
                    elementToCompare = 1;
                    % Inner loop over elements of argument
                    while tf && elementToCompare <= numel( obj1 )
                        tf = tf && ...
                             scalarIsequal( obj1(elementToCompare), obj2(elementToCompare) );
                        elementToCompare = elementToCompare + 1;
                    end
                    argToCompare = argToCompare + 1;
                end
            catch E
                throw( E );
            end
        end
    end
    
    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
    
    % --------------------------------------------------------------------------
    % Public/Hidden API-1 compatibility
    methods ( Hidden, Sealed )
        function cm = pCreateConnectionManager( obj )
            cm = obj.buildConnectionManager();
        end
    end
end
%#ok<*ATUNK> custom attributes



function profileName = iGetValidProfileName(profileName)
% Given a profile name, see if it really exists, and if it
% doesn't, then return empty.
p = parallel.Settings;
prof = p.findProfile('Name', profileName);
if isempty(prof)
    % If it turns out that the profile doesn't actually exist, we 
    % should fall back to an empty profile name
    profileName = '';
end
end
