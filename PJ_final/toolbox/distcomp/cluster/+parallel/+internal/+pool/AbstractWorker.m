%AbstractWorker Base class for matlabpool worker objects

% Copyright 2012 The MathWorks, Inc.
classdef AbstractWorker < parallel.internal.pool.SessionObject

    properties ( GetAccess = protected, SetAccess = immutable )
        WorkerIndex % Which worker number am I in the pool.
        NumWorkers  % How many workers in the pool.
    end

    methods ( Abstract )
        stopLabAndDisconnect(obj);
    end
    methods ( Abstract, Access = protected )
        % Subclasses define how to perform their connection.
        connection = connectToClientImpl( obj, connMgr, clientProc, connInfo );
    end

    methods
        function obj = AbstractWorker( idx, num )
            obj.WorkerIndex = idx;
            obj.NumWorkers  = num;
        end
    end

    methods ( Sealed )
        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoollab/addFileDependenciesToPool.m
        function addFileDependenciesToPool(~, dependencyDir, dependencyMap)
        % Get the current Session's FileDependencyAssistant
            fda = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession.getFileDependenciesAssistant;
            % Set the dependency directory
            fda.setDependencyDir(dependencyDir);
            % Add the elements from the map to the assistant
            for i = 1:size(dependencyMap, 1)
                fda.addDependency(dependencyMap{i, 1}, dependencyMap{i, 2});
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoollab/connectToClient.m
        function connectToClient( obj, connInfo )
        %connectToClient Start pmode and connect to the client.
        %   Lab opens a socket and connects back to the MATLAB client.  Errors if this
        %   is not possible.

            import com.mathworks.toolbox.distcomp.pmode.poolmessaging.ConnectionManager;
            import com.mathworks.toolbox.distcomp.pmode.poolmessaging.ProcessInstance;

            try
                cfg        = pctconfig();
                hostname   = cfg.hostname;
                connMgr    = ConnectionManager.buildLabConnManager( obj.WorkerIndex, ...
                                                                  obj.NumWorkers, ...
                                                                  hostname, ...
                                                                  connInfo );
                clientProc = ProcessInstance.getClientInstance();

                % Defer to subclasses to work out how to batch up connections
                connection = obj.connectToClientImpl( connMgr, clientProc, connInfo );
            catch err
                dctSchedulerMessage(1, 'Connection failed with the message:\n''%s''', ...
                                    err.message);
                rethrow(err);
            end

            % Create the Java objects for this pmode session.
            try
                % ConnectionManager is not needed from this point onwards
                connMgr.close();
                % Set up the streams for asynchronous output redirection
                com.mathworks.toolbox.distcomp.pmode.MatlabOutputWriters.getInstance.setup();
                % SessionFactory takes ownership of the Sockets as well as the Session.
                com.mathworks.toolbox.distcomp.pmode.SessionFactory.createLabSession(....
                    connection, obj.WorkerIndex, connMgr.getLocalInstance);
            catch err
                dctSchedulerMessage(1, ['Failed to create session object with ', ...
                                    'the message:\n''%s'''], err.message);
                rethrow(err);
            end
        end
    end

end
