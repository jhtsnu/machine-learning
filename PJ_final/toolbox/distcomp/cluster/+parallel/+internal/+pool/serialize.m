function bbhArray = serialize( data )
%parallel.internal.pool.serialize - Serialize data to an array of ByteBufferHandles
%   for transmission

%   Copyright 2012 The MathWorks, Inc.

persistent hostIs64Bit

if isempty( hostIs64Bit )
    hostIs64Bit = com.mathworks.util.PlatformInfo.is64Bit();
end

session    = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession();
gotSession = ~isempty( session );

host64AndNoSession = ~gotSession && hostIs64Bit;
active64Session    =  gotSession && session.supports64BitSerialization();

if host64AndNoSession || active64Session
    ser = distcompserialize64( data );
else
    ser = distcompserialize( data );
end
bbhArray = distcompMakeByteBufferHandles( ser );
end
