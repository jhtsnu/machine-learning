%PoolClient Client object for matlabpool jobs

% Copyright 2012 The MathWorks, Inc.
classdef PoolClient < parallel.internal.pool.AbstractClient
    properties ( Access = private )
        % Is this communicating job size 1 - i.e. no PoolWorkers.
        IsClientOnlySession = false
    end

    methods
        function obj = PoolClient
            obj.IsClientOnlySession = (numlabs < 2);
            obj.JobStartupTimeout   = Inf;
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/addFileDependenciesToPool.m
        function addFileDependenciesToPool(obj, dependencyDir, dependencyMap)
            % If we are running just on the client none of the following is needed.
            if obj.IsClientOnlySession
                return
            end
            % Get the current Session's FileDependencyAssistant
            fda = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession.getFileDependenciesAssistant;
            % Set the dependency directory
            fda.setDependencyDir(dependencyDir);
            % Add the elements from the map to the assistant - since we are a client we
            % are the source of the dependencies and need to add them in a similar way
            % to the client.
            for i = 1:size(dependencyMap, 1)
                fda.addDependency(dependencyMap{i, 1}, dependencyMap{i, 2});
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/start.m
        function start(obj, leadingTaskNum)
        %START Start matlabpool job client.

            % If we are running just on the client none of the following is needed.
            if obj.IsClientOnlySession
                return
            end

            try
                connMgr = obj.pCreateConnectionManager();
            catch err
                obj.pStopLabsAndDisconnect();
                rethrow(err);
            end

            % We need the name of this machine to connect back to from the parallel
            % job.  Get from pctconfig as it allows the user to override the default
            % hostname.
            [~, undoc] = pctconfig();

            % Build the connection info object that we are going to send to the remote
            % machines. This contains an InetSocketAddress object and a UUID that
            % identifies this connection attempt. The undocumented method below allows us to
            % simulate the situation where we get the wrong IP for the local machine
            % and still expect the remote machine to undertake a lookup to see if they
            % can resolve the correct address. This simulation only makes sense if
            % we are sending a ServerSocketConnectInfo to the workers
            connInfo = connMgr.getInfoToConnect();
            if ~isempty( undoc.hostinetaddress ) && connInfo.getClass().getSimpleName().equals('ServerSocketConnectInfo')
                connInfo = connMgr.getInfoToConnect().createMangledConnectionInfo( undoc.hostinetaddress );
            end

            % Share the hostname and port among all labs. The corresponding MPI call
            % resides in @matlabpoollab/start.m
            connInfo = labBroadcast(leadingTaskNum, connInfo); %#ok<NASGU>

            % Set up for SPMD execution - build the new communicator, and stash
            % that for later.
            try
                % Corresponding mpiCommManip is in the labs' "start" method
                newWorld = mpiCommManip( 'split', -1, labindex );
                worldVec = gcat( {newWorld} );
                spmdlang.commForWorld( 'set', worldVec(2:end), 2 );
                % Deadlock detection on for now while we don't have a good way of
                % interrupting non-dd communications
                mpiSettings( 'DeadlockDetection', 'on' );
            catch err
                obj.pStopLabsAndDisconnect();
                rethrow( err );
            end

            % Wait for the labs to connect back to the client.
            try
                connections = obj.pGetSockets();
            catch err
                % pGetSockets rewrites the error.
                obj.pStopLabsAndDisconnect();
                rethrow(err);
            end

            % Create the Java objects for this interactive session.
            try
                % SessionFactory takes ownership of the Connections as well as the Session.
                com.mathworks.toolbox.distcomp.pmode.SessionFactory.createClientSession( ...
                    connections, connMgr.getLocalInstance);
                session = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession();
                % Once created we should loop waiting for all labs to finish starting
                % up
                while ~session.waitForSessionToStart(100, java.util.concurrent.TimeUnit.MILLISECONDS);
                end
            catch err
                obj.pStopLabsAndDisconnect();
                ex = MException(message('parallel:lang:matlabpool:FailedToInitializeInteractiveSession'));
                ex = ex.addCause(err);
                throw(ex);
            end

            % For a matlabpool session tell the session to listen for Path and Clear
            % notification and send them onto the labs
            try
                session.startSendPathAndClearNotificationToLabs;
                dctPathAndClearNotificationGateway('on');
                mpiCommManip( 'select', 'self' );
            catch err
                obj.pStopLabsAndDisconnect();
                ex = MException(message('parallel:lang:matlabpool:FailedToInitializeInteractiveSession'));
                ex = ex.addCause(err);
                throw(ex);
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/stopLabsAndDisconnect.m
        function OK = stopLabsAndDisconnect(obj)
        %stopLabsAndDisconnect Stop all the labs and perform client cleanup
        %   Send the stop signal to all the labs so they exit.  Clean up sockets
        %   and streams.
            OK = true;
            if ~obj.IsClientOnlySession
                OK = obj.pStopLabsAndDisconnect();
            end

            % Clean up memory associated with Composites
            spmdlang.ValueStore.clear();
        end
    end

    methods ( Access = private )
        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/pStopLabsAndDisconnect.m
        function OK = pStopLabsAndDisconnect(obj)
        %stopLabsAndDisconnect Stop all the labs and perform client cleanup
        %   Send the stop signal to all the labs so they exit.  Clean up sockets
        %   and streams.

            % Must ensure we are all back in the world communicator to clean up.
            mpiCommManip( 'select', 'world' );

            OK = true;
            if ~isempty(com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession)
                disp('Sending a stop signal to all the labs...');
                dctPathAndClearNotificationGateway('off');
                OK = com.mathworks.toolbox.distcomp.pmode.SessionFactory.destroyClientSession;
            end

            if ~isempty(obj.ConnectionManager)
                try
                    obj.ConnectionManager.close;
                catch err
                    dctSchedulerMessage(2, ['Failed to close the server socket due to the ', ...
                                        'following error:\n%s'], err.message);
                end
            end
            % Finished with the ConnectionManager - forget about it
            obj.ConnectionManager = [];
        end


        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/pGetSockets.m
        function connections = pGetSockets(obj)
            % Loop whilst waiting for the job to connect back - this means
            % we aren't blocking on the accept, as this ServerSocket has had its
            % setSoTimeout set to a finite value.
            startTime = clock();

            % The following code mirrors the behaviour in @matlabpoollab/connectToClient
            % for the sake of the labBarriers. If one day labBarrier(2:numlabs) is supported,
            % replace the try clause with: socks = obj.pGetSockets(numlabs - 1);

            % The connections occur in batches which are separated by labBarriers (see
            % @matlabpoollab/connectToClient.m for details). The following loop
            % imitates that behaviour by calling labBarrier every batchSize step.
            constants = parallel.internal.pool.Constants;
            batchSize = max(1,constants.connectionBacklog - 1);

            connections = [];
            % BKN -- is this use of numlabs problematic? If yes, replace by
            %        count = 0; numlabs = ?; while count < nlabs, ... count++ ...
            for ii = 1:numlabs-1
                % numlabs -1 to skip this client lab.
                dctSchedulerMessage(6, 'Start attempt to get a single connection.');

                connection = iGetSingleConnection(obj, startTime);
                dctSchedulerMessage(6,'Got connection');

                labidx   = connection.getRemoteInstance().getLabIndex();
                nlabs    = connection.getRemoteInstance().getNumberOfLabs();

                % EME -- should we assert nlabs==numlabs-1?
                if isempty(connections)
                    connections = javaArray('com.mathworks.toolbox.distcomp.pmode.shared.Connection', nlabs);
                end

                connections(labidx) = connection; %#ok<AGROW> Deferred instantiation above

                if mod(ii, batchSize) == 0 || ii == numlabs - 1
                    % Ensure that all labs reach this point simultaneously so that their 
                    % "self-destruct" timers are approx. in sync.  See LabShutdownHandlerImpl.
                    dctSchedulerMessage(6, 'Client at interior labBarrier');
                    labBarrier;
                    dctSchedulerMessage(6, 'Client past interior labBarrier');
                end
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/pCreateConnectionManager.m
        function connMgr = pCreateConnectionManager( obj )
        % pCreateConnectionManager returns a newly created ConnectionManager object
        % Currently uses an ephemeral port

        % Ideally we'd provide a way to specify the portrange to use here, but
        % for now we'll use port = 0 to specify an ephemeral port.
            port      = 0;
            cfg       = pctconfig();
            hostname  = cfg.hostname;
            constants = parallel.internal.pool.Constants();

            useSecureMatlabPool = false;

            %ConnectionManager
            clientPeerInstance = com.mathworks.toolbox.distcomp.pmode.poolmessaging.ConnectionManager.createClientPeerInstance();
            acceptInfo = com.mathworks.toolbox.distcomp.pmode.poolmessaging.ConnectionManager.createAcceptInfo( ...
                clientPeerInstance, ...
                port, port, constants.connectionBacklog, ...
                useSecureMatlabPool, constants.handshakeTimeout, constants.handshakeConnectAttempts);

            connMgr = com.mathworks.toolbox.distcomp.pmode.poolmessaging.ConnectionManager.buildClientConnManager(...
                hostname, clientPeerInstance, acceptInfo);

            obj.ConnectionManager = connMgr;
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/pGetSockets.m iGetSingleConnection
function connectionAndNumberOfLabs = iGetSingleConnection(obj, startTime)
    WAITING_FOR_SOCKET = true;

    accepted = [];

    while WAITING_FOR_SOCKET

        accepted           = obj.ConnectionManager.activelyAccept();
        WAITING_FOR_SOCKET = isempty( accepted );

        if WAITING_FOR_SOCKET && etime(clock, startTime) > obj.JobStartupTimeout
            iThrowTimeoutError(obj);
        end

        if WAITING_FOR_SOCKET
            pause( 0.01 );
        else
            dctSchedulerMessage(2, 'Received a socket connection.');
        end

    end % while WAITING_FOR_SOCKET

    connectionAndNumberOfLabs = accepted;
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@matlabpoolclient/pGetSockets.m iThrowTimeoutError
function iThrowTimeoutError(obj)
    error(message('parallel:convenience:ConnectionTimeoutExceeded', obj.JobStartupTimeout));
end
