function data = deserialize( bbhArray, doFree )
%parallel.internal.pool.deserialize - Deserialize data

%   Copyright 2012 The MathWorks, Inc.

if nargin < 2
    doFree = true;
end
data = distcompdeserialize( distcompByteBufferHandles2MxArray( bbhArray ) );
if doFree
    arrayfun( @free, bbhArray );
end

end
