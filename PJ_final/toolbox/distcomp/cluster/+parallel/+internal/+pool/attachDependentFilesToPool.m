function haveAttachedFiles = attachDependentFilesToPool(filesToAnalyse)

%   Copyright 2012 The MathWorks, Inc.

haveAttachedFiles = false;

% See if we should even attempt to attach files to the pool
client = parallel.internal.pool.SessionManager.getSessionObject();
if ~isa(client, 'parallel.internal.pool.InteractiveClient')
    % Not an interactive matlabpool client, so do nothing.
    return
end
if ~client.ParallelJob.AutoAttachFiles
    return
end

if isempty(filesToAnalyse)
    return;
end

if ~iscell(filesToAnalyse)
    filesToAnalyse = {filesToAnalyse};
end

msg = getString(message('parallel:lang:matlabpool:PerformingDependencyAnalysis'));
fprintf('%s', msg);
printNewLineCleanup = onCleanup(@() fprintf('\n'));

% Suppress the parallel:cluster:DepfunError warning before analyzing the files
warnState = warning('off', 'parallel:cluster:DepfunError');
warnCleanupObj = onCleanup(@() warning(warnState));

filesToAttach = cellfun(@parallel.internal.apishared.AttachedFiles.calculateAttachedFiles, filesToAnalyse, 'UniformOutput', false);
filesToAttach = [filesToAttach{:}];

if isempty(filesToAttach)
    return;
end

dctSchedulerMessage(6, 'attachDependentFilesToPool - will attach:\n%s', sprintf('\t%s\n', filesToAttach{:}));
parallel.internal.cluster.MatlabpoolHelper.addOrUpdateAttachedFiles(filesToAttach);

doneMsg = getString(message('parallel:lang:matlabpool:DoneDependencyAnalysis'));
fprintf('%s', doneMsg);
haveAttachedFiles = true;
