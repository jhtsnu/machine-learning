%AbstractClient Base class for matlabpool client objects.

% Copyright 2012 The MathWorks, Inc.
classdef AbstractClient < parallel.internal.pool.SessionObject
    properties ( SetAccess = protected )
        ConnectionManager
    end
    properties
        % JobStartupTimeout Timeout in seconds to wait for job startup.
        JobStartupTimeout
    end
    methods ( Abstract )
        stopLabsAndDisconnect(obj, varargin);
    end
end
