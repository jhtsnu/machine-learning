%PoolWorker Worker object for matlabpool jobs

% Copyright 2012 The MathWorks, Inc.
classdef PoolWorker < parallel.internal.pool.AbstractWorker
    properties ( Access = private )
        NewWorldComms
    end

    methods
        function obj = PoolWorker()
        % Important note: there is a convenient fiction here - we pretend that our
        % labindex is "labindex-1", and that numlabs is "numlabs-1" so that the
        % matlabpool can still work with client and labs 1:N, rather than having to
        % deal with the fact that the client also happens to have a labindex.
            obj@parallel.internal.pool.AbstractWorker( labindex - 1, ...
                                                       numlabs  - 1 );
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoollab/start.m
        function start(obj, leadingTaskNum)
        %start This will start a matlabpool job on the non-leading labs
        %   The labs connect to the specified host and port and create their
        %   session object.
            try
                connInfo = labBroadcast(leadingTaskNum);

                % Set up for SPMD execution - make a new MPI communicator that involves only
                % labs 2:numlabs.
                obj.NewWorldComms = mpiCommManip( 'split', 1, labindex );
                gcat( {obj.NewWorldComms} );

                % Deadlock detection on for now while we don't have a good way of
                % interrupting non-dd communications
                mpiSettings( 'DeadlockDetection', 'on' );

                obj.connectToClient( connInfo );

                mpiCommManip( 'select', obj.NewWorldComms );
            catch err
                dctSchedulerMessage(1, 'Error message from PoolWorker/start: %s', ...
                                    err.message);
                rethrow(err);
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoollab/stopLabAndDisconnect.m
        function stopLabAndDisconnect(obj)
        %stopLabAndDisconnect Stop pmode, destroy Java objects and close sockets.

        % This is not quite a 1:1 copy of @interactivelab/stopLabAndDisconnect.m -
        % as we need to free the "NewWorldComms"

        % Destroy the Java objects for this pmode session, including the sockets.
            try
                mpiCommManip( 'select', 'world' );
                com.mathworks.toolbox.distcomp.pmode.SessionFactory.destroyLabSession;
                com.mathworks.toolbox.distcomp.pmode.MatlabOutputWriters.getInstance.teardown();
                mpiCommManip( 'free',   obj.NewWorldComms );
                % Clean up memory associated with Composites
                spmdlang.ValueStore.clear();
            catch err
                dctSchedulerMessage(1, ['Failed to destroy session object ', ...
                                    'with message:\n''%s'''], err.message);
                rethrow(err);
            end
        end
    end

    methods ( Access = protected )
        %#cf matlab/toolbox/distcomp/@distcomp/@matlabpoollab/connectToClient.m
        function connection = connectToClientImpl( obj, connMgr, clientProc, ~ )
        % Force connections to occur in batches, so that we don't overflow the
        % ServerSocket's connection backlog setting.
            constants = parallel.internal.pool.Constants;
            batchSize = max(1, constants.connectionBacklog - 1);

            myIdx      = obj.WorkerIndex;
            numWorkers = obj.NumWorkers;

            for ii = 1:batchSize:numWorkers
                if myIdx >= ii && myIdx < ii + batchSize
                    % Perturb the connections
                    pause(0.01 * (myIdx-ii));
                    connection = connMgr.activelyConnectTo( clientProc );
                end
                % Ensure that all labs reach this point simultaneously so that their 
                % "self-destruct" timers are approx. in sync.  See LabShutdownHandlerImpl.
                dctSchedulerMessage(6, 'Worker at interior labBarrier');
                labBarrier;
                dctSchedulerMessage(6, 'Worker past interior labBarrier');
            end
        end
    end
end
