%#cf matlab/toolbox/distcomp/@distcomp/pInterPPromptFcn.m
function interPPromptFcn

% Copyright 2007-2012 The MathWorks, Inc.

% Call the necessary functions for checking communication mismatches
mpigateway('setidle');
mpigateway('setrunning');
% Ensure that for:drange state is reset
parfor_depth(0);
