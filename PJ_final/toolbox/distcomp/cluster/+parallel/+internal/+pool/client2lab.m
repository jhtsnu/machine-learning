%#cf matlab/toolbox/distcomp/@distcomp/client2lab.m
function client2lab(transferSeqNumber, labidx, labvarname)
%client2lab Static wrapper around InteractiveWorker.client2lab.
%   This method can only be called on the labs, and it forwards the call to
%   the InteractiveWorker object.

%   Copyright 2006-2012 The MathWorks, Inc.

labobj = parallel.internal.pool.SessionManager.getSessionObject();
labobj.client2lab(transferSeqNumber, labidx, labvarname);


