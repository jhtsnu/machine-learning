%SessionObject Base class for client and worker objects

% Copyright 2012 The MathWorks, Inc.
classdef SessionObject < handle
    methods ( Abstract )
        start(obj, varargin);
    end
end
