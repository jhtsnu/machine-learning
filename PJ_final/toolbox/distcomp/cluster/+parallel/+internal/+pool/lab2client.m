%#cf matlab/toolbox/distcomp/@distcomp/lab2client.m
function lab2client(transferSeqNumber, labvarname, labidx)
%lab2client Static wrapper around InteractiveWorker.lab2client.
%   This method can only be called on the labs, and it forwards the call to
%   the InteractiveWorker object.

%   Copyright 2006-2012 The MathWorks, Inc.

try
    labobj = parallel.internal.pool.SessionManager.getSessionObject();
    labobj.lab2client(transferSeqNumber, labvarname, labidx);
catch err
    rethrow(err);
end
