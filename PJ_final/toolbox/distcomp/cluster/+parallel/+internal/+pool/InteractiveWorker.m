%InteractiveWorker Worker object for interactive matlabpool

% Copyright 2012 The MathWorks, Inc.
classdef InteractiveWorker < parallel.internal.pool.AbstractWorker

    methods
        function obj = InteractiveWorker()
        % All workers in the job are pool-workers, so use labindex and numlabs for
        % superclass constructor.
            obj@parallel.internal.pool.AbstractWorker( labindex, ...
                                                       numlabs );
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactivelab/client2lab.m
        function client2lab(~, transferSeqNumber, labidxes, labvarname)
        %   Lab code to transfer a serializable variable onto a lab.
        %   Caller should have made sure that labidxes is a vector of integers and
        %   isvarname(labvarname) returns true.

            import com.mathworks.toolbox.distcomp.pmode.SessionFactory;

            err = [];
            value = [];
            firstLab = min(labidxes);
            %%
            % firstLab reads the data from the client.  
            if labindex == firstLab
                try
                    try
                        session = SessionFactory.getCurrentSession();
                        transfer= session.getTransfer();
                        receiver = transfer.getDataReceiver();
                        manager = transfer.getTransferManager();
                        monitor = manager.respondToTransferInitiation(receiver, ...
                                                                      transferSeqNumber);
                    catch E
                        error( message( 'parallel:convenience:PmodeNotRunning' ) );
                    end
                    value = parallel.internal.pool.doInteractiveReceive(...
                        receiver, monitor);
                catch exception
                    % doInteractiveReceive reports all of its M errors to the monitor, so 
                    % we can simply rethrow the error.
                    err = exception;
                end
            end

            %%
            % All the labs need to know whether firstLab has value or not in order
            % to determine whether to proceed.
            err = labBroadcast(firstLab, err);
            if ~isempty(err)
                if labindex == firstLab
                    % doInteractiveReceive reported the error to the monitor, so we 
                    % can simply bail out.
                    rethrow(err);
                else
                    return;
                end
            end

            %%
            % Use MPI to send value from firstLab to the other labs and store it in 'base'.
            try
                iSendVarAndDeserialize(labidxes, firstLab, value, labvarname);
            catch err
                if labindex == firstLab
                    monitor.setLocalMError(err.identifier, err.message);
                end
                rethrow(err);
            end

            %%
            % We are now done, and have to wait till the remote side is done.
            if labindex == firstLab
                monitor.setLocalFinished();
                iLoopUntilErrorOrTrue(monitor, @() monitor.isRemoteFinished()); 
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactivelab/lab2client.m
        function lab2client(~, transferSeqNumber, labvarname, labidx)
        %lab2client Lab code to transfer a variable from lab to client.
        %   Caller should have made sure that lab is an integer, and that
        %   isvarname(labvarname) returns true.

            import com.mathworks.toolbox.distcomp.pmode.SessionFactory;

            if labindex ~= labidx
                return;
            end

            % Initialize the transfer objects.
            try
                session = SessionFactory.getCurrentSession();
                transfer = session.getTransfer();
                manager = transfer.getTransferManager();
                sender = transfer.getDataSender();
                % Prepare the manager and sender objects for the transfer.
                monitor = manager.respondToTransferInitiation(sender, transferSeqNumber);
            catch E
                error( message( 'parallel:convenience:PmodeNotRunning' ) );
            end

            % Create error messages that are specific to a transfer from a lab to a client.
            idx = labindex; % evaluate labindex here
            errorMsgStruct = struct('undefinedVariable', ...
                                    @() message( ...
                                        'parallel:convenience:VariableUndefinedInLab', labvarname, idx ), ...
                                    'failedToSerialize', ...
                                    @() message( ...
                                        'parallel:convenience:FailedToSerializeVariable', labvarname ) );


            % From this point onwards, the data send operation is identical to that when
            % the client is sending data to a lab, so let a package method do the sending.
            parallel.internal.pool.doInteractiveSend(...
                labvarname, sender, monitor, errorMsgStruct);
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactivelab/connectToClient.m connectToClient
        function start(obj, connInfo)
        %START call connectToClient

            try
                mpiSettings( 'DeadlockDetection', 'on' );
                obj.connectToClient(connInfo);
            catch err
                dctSchedulerMessage(1, 'Error message from connectToClient: %s', err.message);
                throw(distcomp.ReportableException(err));
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactivelab/stopLabAndDisconnect.m
        function stopLabAndDisconnect(obj) %#ok obj never used.
            ; %#ok Undocumented
              %stopLabAndDisconnect Stop pmode, destroy Java objects and close sockets.

            % Destroy the Java objects for this pmode session, including the sockets.
            try
                com.mathworks.toolbox.distcomp.pmode.SessionFactory.destroyLabSession;
                com.mathworks.toolbox.distcomp.pmode.MatlabOutputWriters.getInstance.teardown();
                % Clean up memory associated with Composites
                spmdlang.ValueStore.clear();
            catch err
                dctSchedulerMessage(1, ['Failed to destroy session object ', ...
                                    'with message:\n''%s'''], err.message);
                rethrow(err);
            end
        end
    end

    methods ( Access = protected )
        %#cf matlab/toolbox/distcomp/@distcomp/@interactivelab/connectToClient.m connectToClient
        function connection = connectToClientImpl( obj, connMgr, clientProc, connInfo )
        % Force connections to occur in batches, so that we don't overflow the
        % ServerSocket's connection backlog setting.
            constants = parallel.internal.pool.Constants;
            batchSize = max(1, constants.connectionBacklog - 1);

            myIdx      = obj.WorkerIndex;
            numWorkers = obj.NumWorkers;

            % Make sure all labs have created a connection manager and sync up
            % before trying to connect
            labBarrier;

            % Give each lab a slightly different delay before connecting
            for ii = 1:batchSize:numWorkers
                if myIdx >= ii && myIdx < ii + batchSize
                    % Perturb the connections
                    pause(0.01 * (myIdx-ii));

                    connection = connMgr.activelyConnectTo( clientProc );
                end
                %Don't use this labBarrier when using a brokered matlabpool
                if ~connInfo.peersConnectingAsGroupShareBarrier()
                    labBarrier;
                end
            end

            % Ensure that all labs reach this point simultaneously so that their
            % "self-destruct" timers are approx. in sync.  See LabShutdownHandlerImpl.
            labBarrier;
        end
    end
end

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%#cf matlab/toolbox/distcomp/@distcomp/@interactivelab/client2lab.m iSendVarAndDeserialize
function iSendVarAndDeserialize(labidxes, firstLab, value, labvarname)
%iSendVarAndDeserialize Send value from firstLab to labidxes and assign to base.
%   Note: Does not report any M-errors to any error listeners.

% Send the serialized value from firstLab to all the other destination labs.
    receiveLabs = setdiff(labidxes, firstLab);
    mwTag = 31777;
    if labindex == firstLab
        % Note that labSend with an empty set of destination labs is a noop,
        % so the labSend also works if firstLab is the only destination lab.
        labSend(value, receiveLabs, mwTag);
    elseif any(receiveLabs == labindex)
        value = labReceive(firstLab, mwTag);
    end

    % Deserialize the value and assign in the base workspace.
    if any(labidxes == labindex)
        try
            value = distcompdeserialize(value);
            assignin('base', labvarname, value);
        catch E
            error(message('parallel:convenience:TransferFailedOnLab', ...
                          labvarname, labindex));
        end
    end
end

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactivelab/client2lab.m iLoopUntilErrorOrTrue
function iLoopUntilErrorOrTrue(monitor, func)
% This function only returns normally when func returns true is true and there
% are no errors.
    while true
        err = monitor.getOutsideLocalMError();
        if ~isempty(err)
            error(char(err.getErrorIdentifier()), '%s', char(err.getErrorMessage()));
        end
        if func()
            break;
        end
    end
end
