%SessionManager manage pool worker and client objects.

% Copyright 2012 The MathWorks, Inc.
classdef SessionManager

    methods ( Static )
        %#cf matlab/toolbox/distcomp/@distcomp/getInteractiveObject.m
        function s = getSessionObject()
        %getSessionObject Return a singleton lab or client object.
        %   The return type depends on whether this is invoked on a lab or on the 
        %   client and whether it is a MatlabPool job or not.

            s = iGetOrSetInteractiveObject( 'create' );
        end

        %#no previous equivalent
        function s = setSessionObject( obj )
            validateattributes( obj, {'parallel.internal.pool.SessionObject'}, {'scalar'} );
            s = iGetOrSetInteractiveObject( 'set', obj );
        end

        %#cf matlab/toolbox/distcomp/@distcomp/clearInteractiveObject.m
        function s = clearSessionObject()
        %clearSessionObject Clear the singleton lab or client object.
        %   We need to clear this after every parallel or MatlabPool job.
        %   Otherwise, a later job will not recreate it as it still exists.

            s = iGetOrSetInteractiveObject( 'clear' );
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%#cf matlab/toolbox/distcomp/@distcomp/pGetInteractiveObject.m
function c = iGetOrSetInteractiveObject( action, varargin )
%iGetOrSetInteractiveObject Return a singleton lab or client object.
%   The return type depends on whether this is invoked on a lab or on the
%   client and whether it is a MatlabPool job or not.

    mlock;
    persistent theConnection;

    switch action
      case 'clear'
        delete(theConnection);
        theConnection = [];
      case 'create'
        if isempty(theConnection) || ~isvalid(theConnection)
            if system_dependent('isdmlworker')
                job = getCurrentJob();
                % A MatlabPool job is never on a client.
                if job.pIsMatlabPoolJob
                    error(message('parallel:internal:cluster:InteractiveObjectNotSet'));
                else
                    % This has to be an interactive lab, if it is on a
                    % worker and not a MatlabPool job.
                    conn = parallel.internal.pool.InteractiveLab();
                end
            else
                % There is only one type to be used on the client.
                conn = parallel.internal.pool.InteractiveClient();
            end
            theConnection = conn;
        end
      case 'set'
        conn = varargin{1};
        theConnection = conn;
      otherwise
        error(message('parallel:internal:cluster:InteractiveObjectInvalidArgument', action));
    end

    c = theConnection;
end
