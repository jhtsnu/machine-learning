%InteractiveClient Client object for interactive matlabpool

% Copyright 2012-2013 The MathWorks, Inc.
classdef InteractiveClient < parallel.internal.pool.AbstractClient
    properties ( SetAccess = private )
        UserName
        Tag
        ParallelJob
        IsGUIOpen
        CurrentInteractiveType = 'none'
        IsStartupComplete
    end

    methods
        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/interactiveclient.m
        function obj = InteractiveClient()
            obj.UserName = distcomp.pGetDefaultUsername();
            obj.JobStartupTimeout = Inf;
            obj.Tag = 'Created_by_pmode';
            obj.IsStartupComplete = false;
            obj.IsGUIOpen = false;
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/cleanup.m
        function cleanup( obj, type, sched )
        %Remove all interactive jobs owned by the current user.

        % Error if we are currently the wrong interactive type
            obj.pCheckAndSetInteractiveType(type)

            % Get the job tag we should look for as the stopLabsAndDisconnect might set
            % that to an invalid value
            tag = obj.Tag;
            % Give a current session a chance to shutdown.
            if obj.isPossiblyRunning()
                obj.pStopLabsAndDisconnect();
            end
            % Indicate that we are finished with this session
            obj.CurrentInteractiveType = 'none';

            adaptor = obj.pGetClusterAdaptor( sched );
            jobs    = adaptor.findJobs( sched, tag, obj.UserName );

            if isempty(jobs)
                fprintf( '%s\n\n', getString( message( ...
                         'parallel:convenience:NoExtantInteractiveJobs', ...
                                             type ) ) );
            else
                [~, undoc] = pctconfig();
                if ~undoc.preservejobs
                    fprintf( '%s\n\n', getString( message( ...
                             'parallel:convenience:DestroyingInteractiveJobs', ...
                                                 length(jobs), type ) ) );
                    adaptor.deleteJobs( jobs );
                else
                    % Respect the instruction to preserve the jobs. But since
                    % this is 'close force', try at least to cancel them
                    cancel( jobs );
                end
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/client2lab.m
        function client2lab(~, clientvarname, labidxes, labvarname)
        %client2lab Client code to transfer a serializable variable to labs.
        %   Caller should have made sure that:
        %   - labidxes is a vector of integers
        %   - isvarname(clientvarname) and isvarname(labvarname) return true
        %   - pmode is currently running

            import com.mathworks.toolbox.distcomp.pmode.SessionFactory;

            % Verify that the variable exists in the base workspace.
            if ~evalin('base', sprintf('exist(''%s'', ''var'')', clientvarname))
                error( message( 'parallel:convenience:ClientToLabNoSuchVariable', clientvarname ) );
            end

            % Only send to one of the labs.  It will then use MPI to send to the others.
            firstLab = min(labidxes);
            try
                session = SessionFactory.getCurrentSession();
                labs = session.getLabs();
                transfer = session.getTransfer();
                sender = transfer.getDataSender();
                manager = transfer.getTransferManager();
                % Prepare the tracker and sender objects for the transfer.
                % Note: During debug sessions, you must overwrite the value of the following
                % variable, otherwise the labs will halt the transfer on your first
                % breakpoint.
                isDebugging = false;
                monitor = manager.initiateTransfer(sender, firstLab, isDebugging);
            catch E
                error( message( 'parallel:convenience:PmodeNotRunning' ) );
            end

            % Create error messages that are specific to a transfer from client to
            % lab. Store function handles that can then be evaluated on the client.
            errorMsgStruct = struct('undefinedVariable', ...
                                    @() message( ...
                                        'parallel:convenience:VariableUndefinedInClient', clientvarname ), ...
                                    'failedToSerialize', ...
                                    @() message( ...
                                        'parallel:convenience:FailedToSerializeVariable', clientvarname ) );

            % Execute parallel.internal.pool.client2lab on all the labs.  This will cause firstLab to
            % send us a "ok to start sending" message.
            cmd = sprintf('parallel.internal.pool.client2lab(%s, [ %s ], ''%s'');', ...
                          num2str(monitor.getTransferSeqNumber()), ...
                          num2str(labidxes), labvarname);
            labs.eval(cmd, monitor.getLabsCompletionObserver()); 

            % From this point onwards, the data send operation is identical to that when
            % the client is sending data to a lab, so let a package method do the sending.
            parallel.internal.pool.doInteractiveSend(...
                clientvarname, sender, monitor, errorMsgStruct);
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/isPossiblyRunning.m
        function running = isPossiblyRunning( obj )
        %isPossiblyRunning Check if an interactive session is currently running.
        %   Return true if obj's status indicates that we have an open connection to 
        %   lab 1.  
        %   Note that this method does not check whether the connection is intact and
        %   the labs are running and responsive.

        % Note that we must be very careful not to make this test strict because the
        % user cannot run 'pmode exit' when this test returns false.
            propsToLookAt = {'ConnectionManager', 'ParallelJob'};
            % Say we are running if any of the properties is non-empty or the GUI is
            % open
            % NB: Use of property access because MCOS does not support 'get':
            running = any(~cellfun(@(p) isempty(obj.(p)), propsToLookAt)) ...
                      || obj.IsGUIOpen;

            if ~running
                % Check the last possibility: The session object may exist.
                session = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession;
                running = ~isempty(session) && session.isSessionRunning;
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/lab2client.m
        function lab2client(~, labvarname, labidx, clientvarname)
        %lab2client Client code to transfers a variable from lab to client.
        %   Caller should have made sure that lab is an integer, and that
        %   isvarname(clientvarname) and isvarname(labvarname) return true.

            import com.mathworks.toolbox.distcomp.pmode.SessionFactory;

            try
                session = SessionFactory.getCurrentSession();
                labs = session.getLabs();
                transfer = session.getTransfer();
                manager = transfer.getTransferManager();
                receiver = transfer.getDataReceiver();
                % Prepare the tracker and receiver objects for the transfer.
                % Note: During debug sessions, you must overwrite the value of the following
                % variable, otherwise the labs will halt the transfer on your first
                % breakpoint.
                isDebugging = false;
                monitor = manager.initiateTransfer(receiver, labidx, isDebugging);
            catch E
                error( message( 'parallel:convenience:PmodeNotRunning' ) );
            end

            try
                % Execute lab2client on all the labs to start the send.
                cmd = sprintf('parallel.internal.pool.lab2client(%d, ''%s'', %d);', ...
                              monitor.getTransferSeqNumber(), labvarname, labidx);
                labs.eval(cmd, monitor.getLabsCompletionObserver());
            catch err
                monitor.setLocalMError(err.identifier, err.message);
                rethrow(err);
            end

            % At this point, the data receive operation is identical to that when
            % the lab is sending data to a client, so let a package method do the receive.
            serializedValue = parallel.internal.pool.doInteractiveReceive(receiver, monitor);

            try
                out = distcompdeserialize(serializedValue);
                assignin('base', clientvarname, out);
            catch E
                errID = 'parallel:convenience:TransferFailed';
                errMsgObj = message( errID, clientvarname );
                errMsg = getString( errMsgObj );
                monitor.setLocalMError(errID, errMsg);
                error( errMsgObj );
            end

            %
            % We are now done, and have to wait till the remote side is done.
            monitor.setLocalFinished();
            iLoopUntilErrorOrTrue(monitor, @() monitor.isRemoteFinished());
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/start.m
        function start(obj, type, nlabs, sched, gui, filedependencies)
        %start Start interactive session and show the GUI command window.
        %   Start interactive session, or detect the presence of an active session.  Perform
        %   all the setup and message display needed.  Displays the GUI.


        % Error if we are already running an interactive job
            if obj.isPossiblyRunning()
                error( message( 'parallel:convenience:ConnectionOpen', obj.CurrentInteractiveType ) );
            end

            % Error if we are currently the wrong interactive type
            obj.pCheckAndSetInteractiveType(type)

            if nargin > 4
                openGUI = strcmp(gui, 'opengui');
            else
                openGUI = true;
            end

            % Run the Java garbage collector to clean up leaked file 
            % descriptors on Linux -- See G904795 for details
            cleanupJavaGC = onCleanup(@()java.lang.System.gc());
            
            % ANY error in this function will trigger this onCleanup which calls
            % pStopLabsAndDisconnect - it is only at the very end of this function that
            % we say startup is complete.
            obj.IsStartupComplete = false;
            cleanupOnFailure = onCleanup(@() iCleanupIfStartupFailed(obj));

            try
                connMgr = sched.pCreateConnectionManager();
                obj.ConnectionManager = connMgr;
            catch err
                rethrow(err);
            end

            % We need the name of this machine to connect back to from the parallel
            % job.  Get from pctconfig as it allows the user to override the default
            % hostname.
            [~, undoc] = pctconfig();

            % Build the connection info object that we are going to send to the remote
            % machines. This contains an InetSocketAddress object and a UUID that
            % identifies this connection attempt. The undoc method below allows us to
            % simulate the situation where we get the wrong IP for the local machine
            % and still expect the remote machine to undertake a lookup to see if they
            % can resolve the correct address. This simulation only makes sense if
            % we are sending a ServerSocketConnectInfo to the workers
            connInfo = connMgr.getInfoToConnect();
            if ~isempty( undoc.hostinetaddress ) && connInfo.getClass().getSimpleName().equals('ServerSocketConnectInfo')
                connInfo = connMgr.getInfoToConnect().createMangledConnectionInfo( undoc.hostinetaddress );
            end

            THROW_WITH_CAUSE = @(id, err, varargin) throwAsCaller( ...
                addCause( ...
                    MException( id, '%s', ...
                                getString( message( id, varargin{:} ) ) ), ...
                    err ) );

            % Remove finished and failed jobs.  Warn about queued and pending jobs.
            try
                obj.pRemoveOldJobs(sched);
            catch err
                % Need to close the ConnectionManager
                THROW_WITH_CAUSE( 'parallel:convenience:FailedToDeleteOldJobs', err );
            end

            adaptor = obj.pGetClusterAdaptor( sched );
            % At this point, we know that the configuration worked, so we can print messages
            % to the user.  Make sure we don't leave this function without printing the
            % final newline.
            cleanup = onCleanup(@() fprintf('\n'));

            % Create and store the parallel job.
            try
                adaptor.buildParallelJob(obj, sched, nlabs, connInfo);
                if strcmp(type, 'matlabpool')
                    % The adaptor knows to append paths and prepend files
                    adaptor.addFilesAndPaths( obj.ParallelJob, filedependencies, iGenerateMatlabpoolPath );
                end
                submit(obj.ParallelJob);
            catch err
                THROW_WITH_CAUSE( 'parallel:convenience:FailedToStartInteractiveSession', ...
                                  err, type );
            end

            % Wait for the labs to connect back to the client.
            if isempty(nlabs)
                nlabs = inf; % Set to match the expectations of pGetSockets.
            end
            try
                connections = obj.pGetSockets(nlabs);
            catch err
                rethrow(err);
            end

            % Create the Java objects for this interactive session.
            try
                % SessionFactory takes ownership of the Connections as well as the Session.
                com.mathworks.toolbox.distcomp.pmode.SessionFactory.createClientSession(...
                    connections, obj.ConnectionManager.getLocalInstance);
                session = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession();
                % Now that we have a session we should add the file dependencies so
                % that they are well known to subsequent changes
                fda = session.getFileDependenciesAssistant;
                filedependencies = adaptor.getFileDeps( obj.ParallelJob );
                % Only
                cwd = pwd;
                for i = 1:numel( filedependencies )
                    fda.addClientDependency( cwd, filedependencies{i} );
                end
                % Once created we should loop waiting for all labs to finish starting
                % up
                while ~session.waitForSessionToStart(100, java.util.concurrent.TimeUnit.MILLISECONDS);
                end
            catch err
                THROW_WITH_CAUSE( 'parallel:convenience:FailedToInitializeInteractiveSession', err );
            end


            % For a matlabpool session tell the session to listen for Path and Clear
            % notification and send them onto the labs
            if strcmp(type, 'matlabpool')
                try
                    session.startSendPathAndClearNotificationToLabs;
                catch err
                    THROW_WITH_CAUSE( 'parallel:convenience:FailedToInitializeInteractiveSession', err );
                end
            end

            % Only open the GUI if the start command requested it
            if openGUI
                try
                    com.mathworks.toolbox.distcomp.parallelui.ParallelUI.start(session.getLabs());
                catch err
                    THROW_WITH_CAUSE( 'parallel:convenience:FailedToInitializeInteractiveSession', err );
                end
            end
            obj.IsGUIOpen = openGUI;

            try
                fprintf('%s', getString( message( ...
                    'parallel:convenience:ConnectedToWorkers', session.getPoolSize() ) ) );
                obj.IsStartupComplete = true;
            catch err %#ok<NASGU>
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/stopLabsAndDisconnect.m
        function stopLabsAndDisconnect(obj, type, gui)
        %stopLabsAndDisconnect Stop all the labs and perform client cleanup
        %  Send the stop signal to all the labs so they exit and the parallel job
        %  finishes.  Clean up sockets and streams.  Closes the GUI unless closeGUI
        %  is false.


        % We don't need to stop if it isn't already running.
            if ~obj.isPossiblyRunning()
                error( message( 'parallel:convenience:InteractiveSessionNotRunning', type, type ) );
            end

            if strcmp(type, 'force')
                % Special case that is used to indicate that we don't care if we are
                % coming from 'pmode' or 'matlabpool' - so no need to check the and set
                % the interactive type.
            else
                % Error if we are currently the wrong interactive type
                obj.pCheckAndSetInteractiveType(type)
            end

            if (nargin < 3)
                closeGUI = true;
            else
                closeGUI = ~strcmp(gui, 'leaveguiopen');
            end

            obj.pStopLabsAndDisconnect(closeGUI);
        end
    end
    methods ( Hidden )
        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pRemoveOldJobs.m
        function pRemoveOldJobs(obj, sched)
        %Remove old jobs that are either finished or failed.
        %   Also warn about jobs that are in the scheduler's queue.

        % This method is tested, hence Hidden rather than private
            [~, undoc] = pctconfig();
            if undoc.preservejobs
                return;
            end

            adaptor = obj.pGetClusterAdaptor( sched );
            jobs    = adaptor.findJobs( sched, obj.Tag, obj.UserName );

            if isempty(jobs)
                return;
            end
            state = jobs.get({'State'});
            old = strcmp(state, 'finished') | strcmp(state, 'failed');
            running = strcmp(state, 'running');
            pendingOrQueued = strcmp(state, 'pending') | strcmp(state, 'queued');
            type = obj.CurrentInteractiveType;
            if any(old)
                adaptor.deleteJobs( jobs(old) );
            end
            if any(running) || any(pendingOrQueued)
                explain = '';
                schedConfig = adaptor.getSchedConfig( sched );
                switch type
                  case 'pmode'
                    if ~isempty(schedConfig)
                        explain = getString( message( 'parallel:convenience:PmodeCleanup', ...
                                                      schedConfig ) );
                    end
                  case 'matlabpool'
                    if ~isempty(schedConfig)
                        explain = getString( message( 'parallel:convenience:MatlabpoolProfileCleanup', ...
                                                      schedConfig ) );
                    else
                        explain = getString( message( 'parallel:convenience:MatlabpoolNoProfileCleanup' ) );
                    end
                end
                if any(running)
                    if any(pendingOrQueued)
                        warning( message( 'parallel:convenience:RunningPendingJobsExist', ...
                                          nnz(running), type, nnz(pendingOrQueued), explain ) );
                    else
                        warning( message( 'parallel:convenience:RunningJobsExist', ...
                                          nnz(running), type, explain ) );
                    end
                else
                    warning( message( 'parallel:convenience:PendingJobsExist', ...
                                      nnz(pendingOrQueued), type, explain ) );
                end
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pCheckAndSetInteractiveType.m
        function pCheckAndSetInteractiveType( obj, type )
        %pCheckCorrectInteractiveType Check if type can make a call

            currentType = obj.CurrentInteractiveType;
            % Cannot use this to set type to none
            if ~strcmp(type, 'none')
                if strcmp(type, currentType)
                    return
                end
                if strcmp(currentType, 'none')
                    obj.CurrentInteractiveType = type;
                    obj.Tag = ['Created_by_' type];
                    return
                end
            end
            error( message( 'parallel:convenience:InvalidCallState', ...
                            type, currentType, currentType, type ) );
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetClusterAdaptor.m
        function adaptor = pGetClusterAdaptor( ~, schedOrJob )
        % Return a collection of function handles for dealing with jobs/schedulers etc.

            if isa( schedOrJob, 'parallel.Cluster' ) || isa( schedOrJob, 'parallel.Job' )
                findJobs          = @( sched, tag, username ) ...
                    sched.findJob( 'ApiTag', tag, 'Username', username );
                deleteJobs        = @( jobs ) delete( jobs );
                buildParallelJob  = @iCreateCommunicatingJob;
                getTaskIds        = @(tasks) get( tasks, {'Id'} );
                getJobDebugLog    = @(pjob) iGetDebugLog( 'Parent', 'cluster', pjob );
                getSchedConfig    = @(sched) sched.Profile;
                addFilesAndPaths  = @(job, files, paths) ...
                    iAddFilesAndPaths( job, 'AttachedFiles', files, 'AdditionalPaths', paths );
                configDescription = 'profile';
                getFileDeps       = @(job) job.AttachedFiles;
            else
                findJobs          = @( sched, tag, username ) ...
                    sched.findJob( 'Tag', tag, 'UserName', username );
                deleteJobs        = @( jobs ) destroy( jobs );
                buildParallelJob  = @pCreateParallelJob;
                getTaskIds        = @(tasks) get( tasks, {'ID'} );
                getJobDebugLog    = @(pjob) iGetDebugLog( 'parent', 'scheduler', pjob );
                getSchedConfig    = @(sched) sched.Configuration;
                addFilesAndPaths  = @(job, files, paths) ...
                    iAddFilesAndPaths( job, 'FileDependencies', files, 'PathDependencies', paths );
                configDescription = 'configuration';
                getFileDeps       = @(job) job.FileDependencies;
            end

            adaptor = struct( 'findJobs'          , findJobs, ...
                              'deleteJobs'        , deleteJobs, ...
                              'buildParallelJob'  , buildParallelJob, ...
                              'getTaskIds'        , getTaskIds, ...
                              'getJobDebugLog'    , getJobDebugLog, ...
                              'getSchedConfig'    , getSchedConfig, ...
                              'addFilesAndPaths'  , addFilesAndPaths, ...
                              'configDescription' , configDescription, ...
                              'getFileDeps'       , getFileDeps );
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pStopLabsAndDisconnect.m
        function pStopLabsAndDisconnect(obj, closeGUI)
        %stopLabsAndDisconnect Stop all the labs and perform client cleanup
        %  Send the stop signal to all the labs so they exit and the parallel job
        %  finishes.  Clean up sockets and streams.  Closes the GUI unless closeGUI
        %  is false.

        % This method does not throw any errors.

            % Run the Java garbage collector to clean up leaked file 
            % descriptors on Linux -- See G904795 for details
            cleanupJavaGC = onCleanup(@()java.lang.System.gc());

            if (nargin < 2)
                closeGUI = true;
            end

            constants = parallel.internal.pool.Constants();
            timeToWait = constants.clientTimeBetweenStopAndDestroyJob;

            canLabsStop = false;
            if ~isempty(com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession)
                % Rely on this method not throwing any errors.  The disp statement at the end
                % will therefore always print the newline matching this fprintf statement.
                fprintf( '%s ', getString( message( 'parallel:convenience:SendingStopSignal' ) ) );
                canLabsStop = com.mathworks.toolbox.distcomp.pmode.SessionFactory.destroyClientSession;
            end

            try
                % Disable the command input window.
                com.mathworks.toolbox.distcomp.parallelui.ParallelUI.suspend();
            catch err
                dctSchedulerMessage(1, ['Failed to disable the GUI due to the following ', ...
                                    'error:\n%s'], err.message);
            end

            if ~isempty(obj.ParallelJob)
                if canLabsStop
                    % Give the labs a few seconds to finish their labBarrier and exit before
                    % destroying the job.
                    try
                        obj.ParallelJob.wait('finished', timeToWait);
                    catch err
                        dctSchedulerMessage(2, ['Failed to wait for job to finish due to the ', ...
                                            'following error:\n%s'], err.message);
                    end
                end

                try
                    [~, undoc] = pctconfig();
                    if ~undoc.preservejobs
                        adaptor = obj.pGetClusterAdaptor( obj.ParallelJob );
                        adaptor.deleteJobs( obj.ParallelJob );
                    end
                catch err
                    dctSchedulerMessage(2, ['Failed to destroy the job due to the ', ...
                                        'following error:\n%s'], err.message);
                end
            end
            % We have finished with this parallel job - so forget about it
            obj.ParallelJob = [];

            if ~isempty(obj.ConnectionManager)
                try
                    obj.ConnectionManager.close;
                catch err
                    dctSchedulerMessage(2, ['Failed to close the server socket due to the ', ...
                                        'following error:\n%s'], err.message);
                end
            end
            % Finished with the ConnectionManager - forget about it
            obj.ConnectionManager = [];

            if closeGUI && obj.IsGUIOpen
                try
                    com.mathworks.toolbox.distcomp.parallelui.ParallelUI.stop();
                catch err
                    dctSchedulerMessage(1, ['Failed to stop the GUI due to the ', ...
                                        'following error:\n%s'], err.message);
                end
                obj.IsGUIOpen = false;
            end
            % Finally set the current interactive type to 'none' to
            % indicate that no-one is using the current client
            obj.CurrentInteractiveType = 'none';

            % This disp statement adds a newline that complement messages printed above.
            disp( getString( message( 'parallel:convenience:Stopped' ) ) );
        end
    end

    methods ( Access = private )

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m
        function connections = pGetSockets(obj, expNlabs)
        %   Either return a socket, or throw an error. Rewrite the error message in case
        %   we throw an error.
        %   The parallel job must have been created before calling this method.

        % Loop whilst waiting for the job to connect back. The ServerSocketChannel
        % within ConnectionManager is set to not block.
            startTime = clock();

            count = 0;

            while count < expNlabs
                connection = iGetSingleConnection(obj, startTime);

                labidx   = connection.getRemoteInstance().getLabIndex();
                nlabs    = connection.getRemoteInstance().getNumberOfLabs();

                if count == 0
                    connections = javaArray('com.mathworks.toolbox.distcomp.pmode.shared.Connection', nlabs);
                end
                connections(labidx) = connection;
                count = count + 1;
                if ~isfinite(expNlabs)
                    expNlabs = nlabs;
                end
            end
        end

        %#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pCreateParallelJob.m
        function pCreateParallelJob( obj, sched, desiredNumlabs, sockAddr )
        %pCreateParallelJob Create a parallel job to start an interactive session and store in obj.ParallelJob.
        %   The parallel job will connect back to the specified sockAddr
        %   and is tagged with obj.Tag.
        %   Use desiredNumlabs if it is non-empty.

        % Create parallel job to attach to this client - note that configuration is
        % first in the list so that what follows overrides it's values
            if ~isempty(desiredNumlabs)
                numWorkersArgs = struct('MaximumNumberOfWorkers', desiredNumlabs, ...
                                        'MinimumNumberOfWorkers', desiredNumlabs);
            else
                numWorkersArgs = struct;
            end
            pjob = sched.createMatlabPoolJob(...
                'Configuration', sched.Configuration, ...
                'Tag', obj.Tag, ...
                numWorkersArgs);

            pjob.pSetInteractiveJob(true);
            obj.ParallelJob = pjob;


            % The task to run will connect back to the SocketAddress below
            pjob.createTask(@distcomp.nop, 0, ...
                            {sockAddr}, ...
                            'Configuration', sched.Configuration);
        end
    end
end

% --------------------------------------------------------------------------
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetClusterAdaptor.m iGetDebugLog
function logMsg = iGetDebugLog( parentPropName, clusterClass, pjob )
    sched = pjob.(parentPropName);
    if ismethod(sched, 'getDebugLog')
        logMsg = sprintf('%s\n%s', ...
                         getString( message( 'parallel:convenience:CommunicatingJobDebugLog' ) ), ...
                         sched.getDebugLog(pjob));
    else
        logMsg = getString( message( 'parallel:convenience:NoDebugLogAvailable', clusterClass ) );
    end
end

% --------------------------------------------------------------------------
% Append paths; prepend files for matlabpool.
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetClusterAdaptor.m iAddFilesAndPaths
function iAddFilesAndPaths( job, filesProp, files, pathsProp, paths )
    oldPaths        = job.(pathsProp);
    job.(pathsProp) = [ oldPaths(:); paths(:) ];
    if ~isempty( files )
        oldFiles        = job.(filesProp);
        job.(filesProp) = [ files(:) ; oldFiles(:) ];
    end
end

% --------------------------------------------------------------------------
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetClusterAdaptor.m iCreateCommunicatingJob
function iCreateCommunicatingJob( obj, sched, desiredNumlabs, sockAddr )
% Compare with: pCreateParallelJob
% Create parallel job to attach to this client - note that configuration is
% first in the list so that what follows overrides it's values
    if ~isempty(desiredNumlabs)
        numWorkersArgs = { 'NumWorkersRange', desiredNumlabs };
    else
        numWorkersArgs = {};
    end

    % NB: we do not pass on the 'Profile' from the cluster as this is implicit in
    % API-2.

    % Create parallel job to attach to this client - note that configuration is
    % first in the list so that what follows overrides its values
    pjob = sched.createCommunicatingJob(...
        'Tag', obj.Tag, ...
        numWorkersArgs{:}, ...
        'Type', 'Pool' );

    % 'ApiTag' is Hidden, and cannot be specified as a constructor argument.
    pjob.ApiTag = obj.Tag;

    pjob.hSetInteractive(true);
    obj.ParallelJob = pjob;

    % The task to run will connect back to the SocketAddress below
    pjob.createTask( @distcomp.nop, 0, ...
                     {sockAddr} );
end

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m iGetSingleConnection
function connectionAndNumberOfLabs = iGetSingleConnection(obj, startTime)
    WAITING_FOR_SOCKET = true;
    constants = parallel.internal.pool.Constants;

    accepted = [];

    lastJobCheckTime = clock;

    while WAITING_FOR_SOCKET

        accepted           = obj.ConnectionManager.activelyAccept();
        WAITING_FOR_SOCKET = isempty( accepted );

        if WAITING_FOR_SOCKET && etime(clock, startTime) > obj.JobStartupTimeout
            iThrowTimeoutError(obj);
        end

        allowedToStart = com.mathworks.toolbox.distcomp.pmode.SessionFactory.sAllowedToStart.getAndSet(true);
        if ~allowedToStart
            dctSchedulerMessage(1, 'Interrupting session startup.');
            error( message( 'parallel:convenience:InteractiveSessionInterrupted' ) );
        end

        if WAITING_FOR_SOCKET
            pause( 0.01 );
        else
            dctSchedulerMessage(2, 'Received a socket connection.');
        end

        if etime( clock, lastJobCheckTime ) > constants.serverSocketSoTimeout/1000;
            dctSchedulerMessage(3, 'Checking parallel job status.');
            iThrowIfBadParallelJobStatus(obj);
            lastJobCheckTime = clock;
        end

    end % while WAITING_FOR_SOCKET

    connectionAndNumberOfLabs = accepted;
end

%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m iThrowIfBadParallelJobStatus
function iThrowIfBadParallelJobStatus(obj)
% Throw an error if the parallel job is not running.
    jobState = iGetJobState(obj.ParallelJob);
    if iIsJobWaitingOrRunning(jobState)
        % Job is still queued or running, so there is nothing for us to do here.
        return;
    end
    dctSchedulerMessage(1, iGetJobStatusDescription(obj, obj.ParallelJob));
    if strcmpi(jobState, 'destroyed')
        error( message( 'parallel:convenience:InteractiveJobWasDeleted' ) );
    end
    if strcmpi(jobState, 'unavailable')
        error( message( 'parallel:convenience:InteractiveJobUnavailable' ) );
    end
    % We now know that the job has completed and that we have a valid job object.
    msg = iGetErrorMessage(obj.ParallelJob);

    try
        failed = strcmpi(jobState, 'failed');
    catch err %#ok<NASGU>
        failed = false;
    end
    if ~isempty(msg)
        error( message( 'parallel:convenience:InteractiveJobFailedWithMessage', msg ) );
    end
    if failed
        error( message( 'parallel:convenience:InteractiveJobFailedNoMessage' ) );
    else
        error( message( 'parallel:convenience:InteractiveJobFinishedNoMessage' ) );
    end
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m iThrowTimeoutError
function iThrowTimeoutError(obj)
% Throw an error due to timeout exceeded.
    try
        running = strcmpi(obj.ParallelJob.State, 'running');
    catch err %#ok<NASGU>
        running = false;
    end
    if running
        error( message( 'parallel:convenience:ConnectionTimeoutExceeded', ...
                        obj.JobStartupTimeout ) );
    else
        error( message( 'parallel:convenience:JobStartTimeoutExceeded', ...
                        obj.JobStartupTimeout ) );
    end
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m iIsJobWaitingOrRunning
function waitingOrRunning = iIsJobWaitingOrRunning(jobState)
% Check if the job has finished - if so something has gone wrong.
    waitingOrRunning = any(strcmpi(jobState, {'running' 'queued'}));
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m iGetErrorMessage
function msg = iGetErrorMessage(pjob)
% Return the error message of the task causing the job to fail.
% Never throw an error.
    try
        msgs = get(pjob.Tasks, {'ErrorMessage'}); % Always returns a cell array.
                                                  % The task that caused the job to failed is the one that should
                                                  % not start with the string secondaryFailureMsg.  We also exclude empty
                                                  % error messages.
        secondaryFailureMsg = 'The parallel job was cancelled because the task';
        realCause = cellfun(@isempty, regexp(msgs, secondaryFailureMsg)) ...
            & ~cellfun(@isempty, msgs);
        % If all is well, realCause is not empty.
        if any(realCause)
            msg = msgs{find(realCause, 1, 'first')};
        else
            % Our analysis failed, so just use the first non-empty error message
            % instead.
            msg = msgs{find(~cellfun(@isempty, msgs), 1, 'first')};
        end
    catch err %#ok<NASGU>
        msg = '';
    end
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m iGetJobState
function jobState = iGetJobState(pjob)
    try
        jobState = pjob.State;
    catch err %#ok<NASGU>
        jobState = 'unavailable';
    end
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/pGetSockets.m iGetJobStatusDescription
function msg = iGetJobStatusDescription(obj, pjob)

    adaptor = obj.pGetClusterAdaptor( pjob );
    try
        state = pjob.State;
        stateMsg = getString( message( 'parallel:convenience:JobInState', state ) );
    catch err %#ok<NASGU>
        stateMsg = getString( message( 'parallel:convenience:CouldNotGetJobState' ) );
    end
    try
        msgs = get(pjob.Tasks, {'ErrorMessage'});
        ind = ~cellfun(@isempty, msgs);

        if any(ind)
            % Create a matrix containing alternating the ids and messages.
            ids = adaptor.getTaskIds( pjob.Tasks(ind) );
            msgs = msgs(ind);
            errorMsgsCell = cellfun( @(taskID, errMsg) ...
                                     getString( message( 'parallel:convenience:ErrorMessageForTask', ...
                                                         taskID, errMsg ) ), ...
                                     ids, msgs, 'UniformOutput', false );
            errorMsgs = sprintf( '%s\n', errorMsgsCell{:} );
        else
            errorMsgs = getString( message( 'parallel:convenience:NoTaskErrorMessages' ) );
        end
    catch err %#ok<NASGU>
        errorMsgs = getString( message( 'parallel:convenience:CouldNotAccessTaskErrorMessage' ) );
    end

    try
        logMsg = adaptor.getJobDebugLog( pjob );
    catch err %#ok<NASGU>
        logMsg = getString( message( 'parallel:convenience:CouldNotRetrieveDebugLog' ) );
    end

    msg = sprintf('%s\n', stateMsg, errorMsgs, logMsg);
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/start.m iCleanupIfStartupFailed
function iCleanupIfStartupFailed(obj)
    if ~obj.IsStartupComplete
        % Run pre-cancel hook if it exists

        preCancelHook = getenv( 'MDCE_QE_PRE_CLIENT_CANCEL_FCN' );
        if ~isempty( preCancelHook )
            try
                dctSchedulerMessage( 2, ['Pre-cancel hook is defined : About ', ...
                                    'to eval string: %s'], preCancelHook );
                eval( preCancelHook );
            catch E
                dctSchedulerMessage( 2, 'Hook threw an error. Report: %s\n', ...
                                     getReport(E) );
            end
        end

        obj.pStopLabsAndDisconnect();
    end
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/start.m iGenerateMatlabpoolPath
function cellPath  = iGenerateMatlabpoolPath
% Find all elements of the matlab path that are not under $MATLABROOT

% Convert the path the a cell array of strings
    cellPath = textscan(path, '%s', 'delimiter', pathsep);
    cellPath = cellPath{1};

    if isdeployed
        % When deployed we also remove any toolbox code under ctfroot
        rootsToRemove = {matlabroot, fullfile( ctfroot, 'toolbox' )};
    else
        rootsToRemove = {matlabroot};
    end

    for n = 1:length( rootsToRemove )
        thisRoot = rootsToRemove{n};
        % Which parts of path start with thisRoot
        toRemove = strncmp(cellPath, thisRoot, numel(thisRoot));
        cellPath(toRemove) = [];
    end
end
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%#cf matlab/toolbox/distcomp/@distcomp/@interactiveclient/lab2client.m iLoopUntilErrorOrTrue
function iLoopUntilErrorOrTrue(monitor, func)
% This function only returns normally when func returns true is true and there
% are no errors.
    while true
        err = monitor.getOutsideLocalMError();
        if ~isempty(err)
            error(char(err.getErrorIdentifier()), '%s', char(err.getErrorMessage()));
        end
        if func()
            break;
        end
        pause(0.01);
    end
end
