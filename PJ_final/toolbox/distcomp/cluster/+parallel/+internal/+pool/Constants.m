%Constants Constant values used in matlabpool setup

% Copyright 2012 The MathWorks, Inc.
classdef Constants
    properties ( Constant )
        % The timeout to wait between sending the stop signal to the server and 
        % destroying the job.
        clientTimeBetweenStopAndDestroyJob = 30;

        % The SO_TIMEOUT we use for the regular sockets.  In milliseconds.
        socketSoTimeout = 10;

        % The SO_TIMEOUT we use for the server socket.  In milliseconds.
        serverSocketSoTimeout = 2000;

        % The server socket backlog is used in construction of the client's
        % ServerSocket, and also the labs ensure that no more than this number of
        % simultaneous connections are attempted.
        connectionBacklog = 20;

        % The time in seconds we wait the first time we find that the pmode port is
        % unavailable.  We will double the wait time on every failed attempt.
        startWaitTimeForPort = 0.1;

        % The number of times we try to create a server socket on the pmodeport.
        maxTriesForPort = 7;

        % The interval used for all polling when creating matlabpools. In
        % milliseconds
        matlabpoolPollInterval = 100;

        % The timeout used for all polling and other active waits when creating
        % matlabpools. In milliseconds.
        matlabpoolTimeout = 30000;

        % The number of attempts to connect and have a successful handshake
        % when creating matlabpools.
        handshakeConnectAttempts = 3;

        % The timeout used for waits during handshakes while creating
        % matlabpools. In milliseconds.
        handshakeTimeout = 3000; % must be less than matlabpoolTimeout/handshakeConnectAttempts;

    end
end
