% AbstractDiscoverer Base class for all cluster discoverers
%   Copyright 2012 The MathWorks, Inc.

classdef AbstractDiscoverer < handle
    events
        ClusterFound
    end
    
    properties (Dependent)
        Type
        Category
    end
    
    properties (Access = private)
        SchedulerType
    end
    
    methods
        function val = get.Type(obj)
            val = obj.SchedulerType.Name;
        end
        
        function val = get.Category(obj)
            val = obj.SchedulerType.ClusterCategory;
        end
    end
    
    methods
        function obj = AbstractDiscoverer(schedulerType)
            validateattributes(schedulerType, {'parallel.internal.types.SchedulerType'}, {'scalar'});
            obj.SchedulerType = schedulerType;
        end
    end
    
    methods (Abstract)
        % discover - discover clusters
        % Fires ClusterFound events whenever a new cluster 
        % is found.  Terminates discovery if the supplied
        % terminateEarlyFcn returns true.
        discover(obj, terminateEarlyFcn);
    end
end