% MJSComputeCloud - Perform discovery for MJSComputeCloud
%
%    Copyright 2012 The MathWorks, Inc.

classdef MJSComputeCloud < parallel.internal.discover.AbstractDiscoverer

    methods
        function obj = MJSComputeCloud()
            import parallel.internal.types.SchedulerType
            obj = obj@parallel.internal.discover.AbstractDiscoverer( SchedulerType.MJSComputeCloud );
        end
    end

    methods % overrides of Abstract methods in AbstractDiscoverer
        function discover( obj, terminateEarlyFcn )
            % discover - discover MJSComputeCloud clusters
            % Fires ClusterFound events for each cluster
            % returned from the call to cloud center.
            validateattributes(terminateEarlyFcn, {'function_handle'}, {'scalar'});
            clusters = parallel.cluster.MJSComputeCloud.hDiscoverClusters;
            if ~isempty( clusters )
                for ii = 1:numel( clusters )
                    obj.notifyFound( clusters(ii) );
                end
            end
        end
    end

    methods( Access = private )
        function notifyFound( obj, cluster )
            propNames = { 'Name', 'NumWorkers', 'Identifier', 'Certificate' };
            propVals = { cluster.Name, cluster.NumWorkers, cluster.Identifier, cluster.Certificate};
            evtData = parallel.internal.discover.ClusterFoundEventData.createForMJSComputeCloud( ...
                propNames, propVals, {'Identifier', 'Certificate'} );
            obj.notify( 'ClusterFound', evtData );
        end
    end
end
