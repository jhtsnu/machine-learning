% DiscoveryManager - Manages the cluster discovery for all cluster types

%   Copyright 2012 The MathWorks, Inc.

classdef DiscoveryManager < handle
    events
        ClusterFound
    end
    
    properties (Access = private)
        Discoverers = {}
    end

    methods
        function discoverAll(obj, varargin)
        % discoverAll Discover all cluster types
        % discoverAll(obj)
        % discoverAll(obj, terminateEarlyFcn) Terminate the discovery
        % early if terminateEarlyFcn returns true.
            obj.doDiscovery(obj.Discoverers, varargin{:});
        end
        
        function discoverNetwork(obj, varargin)
        % discoverNetwork Discover all clusters on the network
        % discoverNetwork(obj)
        % discoverNetwork(obj, terminateEarlyFcn) Terminate the discovery
        % early if terminateEarlyFcn returns true.
            import parallel.internal.types.ClusterCategory
            isNetwork = cellfun(@(x) isequal(x.Category, ClusterCategory.Network), obj.Discoverers);
            obj.doDiscovery(obj.Discoverers(isNetwork), varargin{:});
        end

        function discoverCloud(obj, varargin)
        % discoverCloud Discover cloud clusters
        % discoverCloud(obj)
        % discoverCloud(obj, terminateEarlyFcn) Terminate the discovery
        % early if terminateEarlyFcn returns true.
            import parallel.internal.types.ClusterCategory
            isCloud = cellfun(@(x) isequal(x.Category, ClusterCategory.Cloud), obj.Discoverers);
            obj.doDiscovery(obj.Discoverers(isCloud), varargin{:});
        end
        
        function registerDiscoverer(obj, discoverer)
        % registerDiscoverer Register a discoverer
            validateattributes(discoverer, ...
                {'parallel.internal.discover.AbstractDiscoverer'}, {'scalar'});
            obj.Discoverers = [obj.Discoverers, {discoverer}];
        end
    end
    
    methods (Access = private)
        function doDiscovery(obj, discoverers, terminateEarlyFcn)
            if nargin > 2
                validateattributes(terminateEarlyFcn, {'function_handle'}, {'scalar'});
            else
                terminateEarlyFcn = @() false; 
            end

            for ii = 1:numel(discoverers)
                if terminateEarlyFcn()
                    return;
                end
                currDiscoverer = discoverers{ii};
                lh = event.listener(currDiscoverer, 'ClusterFound', ...
                    @(~, evtData) obj.findProfileAndNotifyClusterFound(evtData)); %#ok<NASGU> hold onto listener
                currDiscoverer.discover(terminateEarlyFcn);
            end 
        end

        function findProfileAndNotifyClusterFound(obj, evtData)
            % Find all of the profiles that correspond to the event data.
            p = parallel.Settings;
            allProfiles = p.Profiles;
            correspondingProfiles = {};
            for ii = 1:numel(allProfiles)
                schedComp = allProfiles(ii).getSchedulerComponent();
                sameType = strcmp(schedComp.Type, evtData.Type);
                if sameType
                    alreadyDiscovered = schedComp.hIsClusterAlreadyDiscovered(...
                        evtData.SchedulerComponentPropNames, evtData.SchedulerComponentPropValues);
                    if alreadyDiscovered
                        correspondingProfiles = [correspondingProfiles, allProfiles(ii).Name]; %#ok<AGROW>
                    end
                end
            end
            
            evtData.CorrespondingProfiles = correspondingProfiles;
            obj.notify('ClusterFound', evtData);
        end
    end
end