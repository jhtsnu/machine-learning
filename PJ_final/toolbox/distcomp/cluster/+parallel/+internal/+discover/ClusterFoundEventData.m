% ClusterFoundEventData - event data for the ClusterFound event used for discovery

%   Copyright 2012 The MathWorks, Inc.

classdef ClusterFoundEventData < event.EventData
    properties 
        Type
        CorrespondingProfiles = {}
    end
    
    properties (SetAccess = private)
        % The properties of the cluster that were found.
        ClusterPropNames
        ClusterPropValues
    end
    
    properties (Dependent)
        % The properties that are to be stored into any scheduler
        % component that is created for this cluster.
        SchedulerComponentPropNames
        SchedulerComponentPropValues
    end
    
    properties (Constant, Access = private)
        % Properties that are required for all cluster types.
        RequiredProperties = {'Name', 'NumWorkers'};
    end
    
    properties (Access = private)
        % Logical array indicating which of the ClusterPropNames
        % should be stored in the scheduler component.
        ShouldStoreInSchedulerComponent
    end
    
    methods
        function names = get.SchedulerComponentPropNames(obj)
            names = obj.ClusterPropNames(obj.ShouldStoreInSchedulerComponent);
        end
        
        function vals = get.SchedulerComponentPropValues(obj)
            vals = obj.ClusterPropValues(obj.ShouldStoreInSchedulerComponent);
        end
    end
    
    methods (Hidden, Static)
        function obj = createForHPCServer(clusterPropNames, clusterPropValues, clusterPropNamesForSchedComp)
        % Create a ClusterFoundEventData for HPC Server
            additionalRequiredProps = {'Host'};
            obj = parallel.internal.discover.ClusterFoundEventData(...
                'HPCServer', ...
                clusterPropNames, clusterPropValues, ...
                clusterPropNamesForSchedComp, additionalRequiredProps);
        end

        function obj = createForMJS(clusterPropNames, clusterPropValues, clusterPropNamesForSchedComp)
        % Create a ClusterFoundEventData for MJS
            additionalRequiredProps = {'Host', 'SecurityLevel'};
            obj = parallel.internal.discover.ClusterFoundEventData(...
                'MJS', ...
                clusterPropNames, clusterPropValues, ...
                clusterPropNamesForSchedComp, additionalRequiredProps);
        end

        function obj = createForMJSComputeCloud(clusterPropNames, clusterPropValues, clusterPropNamesForSchedComp)
        % Create a ClusterFoundEventData for MJSComputeCloud
            additionalRequiredProps = {};
            obj = parallel.internal.discover.ClusterFoundEventData(...
                'MJSComputeCloud', ...
                clusterPropNames, clusterPropValues, ...
                clusterPropNamesForSchedComp, additionalRequiredProps);
        end
    end
    
    methods (Access = private)
        function obj = ClusterFoundEventData(type, clusterPropNames, clusterPropValues, ...
            clusterPropNamesForSchedComp, additionalRequiredProps)
        % type - the type of cluster
        % clusterPropNames - names for the all the discoverable properties associated with this cluster
        % clusterPropValues - values for the all the discoverable properties associated with this cluster
        % clusterPropNamesForSchedComp - the names of the properties that should be used to create a 
        %                                corresponding scheduler component.  These are the names relative
        %                                to the cluster object.
        % additionalRequiredProps - any additional properties that are required for this cluster type
        
            import parallel.internal.discover.ClusterFoundEventData
            import parallel.internal.customattr.PropSet
            
            % Only do validateattributes for arguments that were supplied to the create* methods.
            validateattributes(type, {'char'}, {'row'});
            validateattributes(clusterPropNames, {'cell'}, {});
            validateattributes(clusterPropValues, {'cell'}, {'numel', numel(clusterPropNames)});
            validateattributes(clusterPropNamesForSchedComp, {'cell'}, {});
        
            % Ensure that the required properties are in the clusterPropNames
            requiredProps = [ClusterFoundEventData.RequiredProperties, additionalRequiredProps];
            
            if ~isempty(setdiff(requiredProps, clusterPropNames))
                error(message('parallel:internal:cluster:ClusterFoundEventDataMissingRequiredProps', ...
                    sprintf(' %s', requiredProps{:})));
            end

            obj.Type = type;
            obj.ClusterPropNames = clusterPropNames;
            obj.ClusterPropValues = clusterPropValues;
            
            [~, clusterPropIdx] = intersect(clusterPropNames, clusterPropNamesForSchedComp);
            obj.ShouldStoreInSchedulerComponent = false(size(clusterPropNames));
            obj.ShouldStoreInSchedulerComponent(clusterPropIdx) = true;
        end
    end
end