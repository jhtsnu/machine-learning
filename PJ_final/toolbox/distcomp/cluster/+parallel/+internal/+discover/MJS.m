% MJS - Perform discovery for MJS
%
%    Copyright 2012 The MathWorks, Inc.

classdef MJS < parallel.internal.discover.AbstractDiscoverer

    methods
        function obj = MJS()
            import parallel.internal.types.SchedulerType
            obj = obj@parallel.internal.discover.AbstractDiscoverer( SchedulerType.MJS );
        end
    end

    methods % overrides of Abstract methods in AbstractDiscoverer
        function discover( obj, terminateEarlyFcn )
        % discover - discover MJS clusters
        % Fires ClusterFound events whenever a new cluster 
        % is found.  Terminates discovery if the supplied
        % terminateEarlyFcn returns true.
            validateattributes(terminateEarlyFcn, {'function_handle'}, {'scalar'});
            obj.doMulticastLookup( terminateEarlyFcn );
        end
    end

    methods ( Static, Hidden )
        function oldVal = currentMulticastTimeout( newVal )
            persistent multicastTimeout;
            if isempty( multicastTimeout )
                multicastTimeout = 10;
            end
            oldVal = multicastTimeout;
            if nargin > 0
                validateattributes( newVal, {'numeric'}, {'integer'} );
                multicastTimeout = newVal;
            end
        end
    end

    methods ( Access = private )

        function doMulticastLookup( obj, terminateEarlyFcn )
            foundJMs = containers.Map();

            % Use multicast to find all the job managers on the network. More than
            % one multicast lookup will be required.
            t = tic;
            while ( toc( t ) < parallel.internal.discover.MJS.currentMulticastTimeout ...
                    && ~terminateEarlyFcn() )
                try
                    % Providing an empty host and name will do a multicast lookup on the local subnet.
                    jobManagers = ...
                        parallel.internal.cluster.JobManagerFinder.getAllJobManagerProxies( '', '' );
                    % We're only interested in job managers of the correct version.
                    jobManagers = ...
                        parallel.internal.cluster.JobManagerFinder.filterProxiesByVersion( jobManagers );
                catch err
                    % If there's a multicast lookup failure here we don't want it to bubble up so
                    % just log it and carry on.
                    if strcmp( err.identifier, 'parallel:cluster:jobmanager:MulticastServiceNotFound' )
                        dctSchedulerMessage( 2, ['Multicast lookup failed, the exception was: ' err.getReport] );
                        continue;
                    else
                        rethrow( err );
                    end
                end
                % If we've found some job managers then notify our listeners.
                if ~isempty( jobManagers )
                    for ii = 1:numel( jobManagers )
                        thisJM = jobManagers(ii);
                        thisID = char( thisJM.getID );
                        if ~foundJMs.isKey( thisID )
                            foundJMs(thisID) = thisJM;
                            obj.notifyFound( thisJM );
                        end
                    end
                end
            end
        end

        function notifyFound( obj, jmprx )
            try
                propNames = { 'Name', 'Host', 'NumWorkers', 'SecurityLevel' };
                propVals = { char( jmprx.getName ), char( jmprx.getHostName ), ...
                    jmprx.getNumIdleWorkers + jmprx.getNumBusyWorkers, jmprx.getSecurityLevel };
                evtData = parallel.internal.discover.ClusterFoundEventData.createForMJS( ...
                    propNames, propVals, {'Host', 'Name'} );
                obj.notify( 'ClusterFound', evtData );
            catch err
                % Ignore clusters that error at this stage, as a user won't be able to use them.
                failureMessage = sprintf( 'A cluster was found but threw an exception when we tried to get information from it, the exception was: %s', err.getReport );
                dctSchedulerMessage( 2, failureMessage );
            end
        end
    end
end
