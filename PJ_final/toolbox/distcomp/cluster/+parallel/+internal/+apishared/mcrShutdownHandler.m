function mcrShutdownHandler( input )
% MCRSHUTDOWNHANDLER
%
% MCRSHUTDOWNHANDLER( 'initialize' ) creates a persistent mlocked
% onCleanup object with a cleanup function that calls
% MCRShutdownHandler.runShutdownHooks().
%
% MCRSHUTDOWNHANDLER( 'run' ) forces the cleanup function to run.

% Copyright 2008-2012 The MathWorks, Inc.

% Do nothing if we are not deployed.
if ~isdeployed
    return;
end

persistent cleanup;

narginchk(1,1) ;

switch input
    case 'initialize'
        import com.mathworks.toolbox.distcomp.util.MatlabRefStore;
        import com.mathworks.toolbox.distcomp.util.MCRShutdownHandler;

        % Make sure all clients try and init the MatlabRefStore.
        % This will fail if we try and init from a different MCR
        % than previously called this.
        MatlabRefStore.initMatlabRef();

        MCRShutdownHandler.registerForCurrentMCR();

        if isempty( cleanup )
            % Initialize the MCRShutdownHandler
            MCRShutdownHandler.initializeForCurrentMCR();
            % onCleanup objects causing problems - see G475153
            % cleanup = onCleanup( @iRunShutdownHooks );
            cleanup = true;
        end
        mlock;
  case 'run'
      cleanup = [];
      iRunShutdownHooks();
  otherwise
      error(message('parallel:cluster:InvalidMCRShutdownArgument'));
end

function iRunShutdownHooks
import com.mathworks.toolbox.distcomp.util.MCRShutdownHandler;

MCRShutdownHandler.runShutdownHooksForCurrentMCR();
