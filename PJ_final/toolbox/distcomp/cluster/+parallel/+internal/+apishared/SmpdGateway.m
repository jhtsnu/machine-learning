% Gateway to the java SMPD daemon manager as used by the local scheduler.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) SmpdGateway
    properties ( Constant, GetAccess = private )
        PassPhrase = sprintf( 'MATLAB_%d', feature( 'getpid' ) );
    end
    methods ( Static, Access = private )

        function tf = shouldAttemptSmpd()
        % shouldAttemptSmpd - check the user hasn't disabled SMPD
            tf = distcomp.feature( 'LocalUseMpiexec' );
        end

        function tf = launchSmpd()
        % launchSmpd - actually attempt to launch the SMPD daemon
        % return true if we succeeded.
            import parallel.internal.apishared.SmpdGateway;

            persistent smpdAvailable
            if isempty( smpdAvailable )
                % Get the java-side process manager
                mgr = com.mathworks.toolbox.distcomp.local.SmpdDaemonManager.getManager();

                % Choose the full path to smpd(.exe)
                cmd = fullfile( matlabroot, 'bin', dct_arch, 'smpd' );

                % Give smpd a temp filename to write stuff into if it sees fit.
                smpdFile = [tempname '.smpd'];

                % The full smpd command-line:
                %   "-anyport" says open any port - we're only expecting loopback connections,
                %     so this should be fine.
                %   "-phrase MATLAB_<pid>" keeps things unique
                %   "-smpdfile <tempname>.smpd" tells smpd to dump stuff in there if needed
                %   "-d 0" says run in the foreground, printing nothing as debug info.
                cmdAndArgs = { cmd, '-anyport', '-phrase', SmpdGateway.PassPhrase, ...
                               '-smpdfile', smpdFile, '-d', '0' };

                % Additions to the environment injected here, namely the PID to
                % watch, and set up MLM_LICENSE_FILE for the workers to match
                % whatever is currently in lmsearchpath.
                names = { 'MPICH_PID_SENTINEL' };
                values = { num2str( system_dependent( 'getpid' ) ) };
                isLicensedOnline = parallel.internal.cluster.getOnlineLicenseInfo();
                if ~isLicensedOnline
                    names = [ names, 'MLM_LICENSE_FILE' ];
                    values = [ values, {feature( 'lmsearchpath' )} ];
                end

                % The java code will attempt to delete the smpdfile at a later stage.
                try
                    smpdAvailable = mgr.supplyLaunchInformation( cmdAndArgs, names, values, smpdFile );
                catch E %#ok<NASGU>
                    smpdAvailable = false;
                end
            end
            tf = smpdAvailable;
        end
    end

    methods ( Static )
        function tf = canUseSmpd()
        % canUseSmpd - should we use SMPD for local scheduler parallel jobs
        % returns true if the user hasn't disabled SMPD, and we successfully launched
        % SMPD. Warns if we tried and failed to launch SMPD.
            import parallel.internal.apishared.SmpdGateway;

            if ~SmpdGateway.shouldAttemptSmpd()
                tf = false;
            else
                tf = SmpdGateway.launchSmpd();
                if ~tf
                    warning(message('parallel:cluster:LocalNoSMPD'));
                end
            end
        end

        function cmdArray = getMpiexecArgs( envNames )
        % getMpiexecArgs - return a cell array of arguments for use with mpiexec
        % - envNames: the list of environment variable names to forward
        % returns
        % - cmdArray: cell array of arguments to mpiexec.
            import parallel.internal.apishared.SmpdGateway;

            % Calculate the full path to the mpiexec executable
            cmd = fullfile( matlabroot, 'bin', dct_arch, 'mpiexec' );

            % Build the genvlist portion
            eNames = sprintf( '%s,', envNames{:} );
            eArgs = { '-genvlist', eNames(1:end-1) };

            % Concoct the complete argument list
            cmdArray = [cmd, eArgs, {'-phrase', SmpdGateway.PassPhrase, '-l'  }];
        end
    end
end
