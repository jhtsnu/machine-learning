% Shared implementation of load and diary for jobs created using 'batch' method
% or function.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) BatchJobMethods

    properties ( Constant, GetAccess = private )
        ScriptFcn = @parallel.internal.cluster.executeScript;
    end

    methods ( Static )
        
        function tf = isBatchScript( job )
        %isBatchScript Return true if the batch job is running a script         
            tf = isequal(job.Tasks(1).Function, ...
                         parallel.internal.apishared.BatchJobMethods.ScriptFcn);
        end
        
        function tf = isBatchJob( job )
        %isBatchJob Return true if job is an API1 or API2 batch job
        %   isBatchJob(job) requires a scalar input argument.
            if isa( job, 'parallel.Job' )
                tag = job.hGetProperty( 'ApiTag' );
            else
                tag = job.Tag;
            end
            tf = isequal( tag, parallel.internal.cluster.AbstractBatchHelper.JobTag );
        end

        function desc = describeBatchJob( job )
        %describeBatchJob Return simple description of a batch job
        %   describeBatchJob(job)
            if ~ parallel.internal.apishared.BatchJobMethods.isBatchJob( job )
                error( message( 'parallel:convenience:NotBatchJob' ) );
            end
            if parallel.internal.apishared.BatchJobMethods.isBatchScript( job )
                msg = 'parallel:convenience:BatchScriptDescription';
            else
                msg = 'parallel:convenience:BatchFunctionDescription';
            end
            % using 'char' handles strings and function handles.
            arg = char( job.Tasks(1).InputArguments{1} );
            desc = getString( message( msg, arg ) );
        end

        function out = load( job, varargin )
        % Helper for various batchJob.load(...) syntaxes.  NB: job can be an
        % API-1 or API-2 job, we must take care only to use common APIs.

        % If load ran anything other than a script, then it is not supported.
        % errorIfNotBatchJob has already checked for a single task.
            if ~ parallel.internal.apishared.BatchJobMethods.isBatchScript( job )
                error(message('parallel:convenience:BatchLoadNotSupported'));
            end

            % Has the job finished
            jobState = get(job, 'State');
            if ~strcmpi(jobState, {'finished' 'failed'})
                error(message('parallel:convenience:BatchLoadInvalidJobState', jobState));
            end

            % Did the job fail or the task error
            taskError = job.Tasks(1).Error;
            % Deal first with something going wrong outside user code
            if isempty(taskError) && strcmpi(jobState, 'failed')
                error(message('parallel:convenience:BatchJobFailed'));
            end
            % Error occurred in user code and was returned to us
            if ~isempty(taskError)
                errorToThrow = MException(...
                    message('parallel:convenience:ErrorRunningBatchJob', taskError.getReport()));
                if strcmp(taskError.identifier, 'MATLAB:UndefinedFunction')
                    errorToThrow = iMaybeHandleUndefinedFunctionError(errorToThrow, taskError, job);
                end
                throw(errorToThrow);
            end

            out = iGetSingleTaskScriptWorkspace(job, varargin{:});

        end

        function diary( taskOutputText, varargin )
        % Helper for batchJob.diary('someFile') and batchJob.diary().

            narginchk( 1, 2 );

            if nargin == 1
                disp( taskOutputText );
                return
            end

            filename = varargin{1};
            validateattributes( filename, {'char'}, ...
                                {'row'}, 'diary', 'filename', 2 );

            % Open the diary file
            [fid, errMsg] = fopen(filename, 'a');

            if fid == -1
                error(message('parallel:convenience:CannotAppendToFile', filename, errMsg));
            end

            fcloser = onCleanup( @() fclose(fid) );
            fwrite(fid, taskOutputText, 'char');
        end
    end
end

% ---------------------------------------------------------------------------------
%
% ---------------------------------------------------------------------------------
function errorToThrow = iMaybeHandleUndefinedFunctionError(errorToThrow, taskError, job)
% Provide a better error message in the case that we have an UndefinedFunction error.

% First check if we can find the function on the client that couldn't be found on
% the worker.
% For a MATLAB:UndefinedFunction error, there should be only one argument - the name
% of the function that could not be found.
args = taskError.arguments;
if numel(args) ~= 1
    % In the case that we don't have the number of arguments we are expecting, we
    % have to fall back on the standard error message.
    return;
end

functionName = args{1};
taskFunction = job.Tasks(1).hGetUserTaskFunctionForDisplay();

if feature('hotlinks')
    batchLink = '<a href="matlab: help batch">batch</a>';
else
    batchLink = 'batch';
end

% Now check if auto-attach is enabled.
if job.AutoAttachFiles
    errorToThrow = ...
        MException(message('parallel:convenience:ErrorRunningBatchJobWithAutoAttach', ...
            taskError.message, functionName, taskFunction, functionName, batchLink));
else
    errorToThrow = ...
        MException(message('parallel:convenience:ErrorRunningBatchJobWithoutAutoAttach', ...
            taskError.message, functionName, taskFunction, functionName, batchLink));
end
    
end

% ---------------------------------------------------------------------------------
%
% ---------------------------------------------------------------------------------
function workspaceOut = iGetSingleTaskScriptWorkspace(job, varargin)
% Get the workspace out of a single batch script

% After this we know the job ran successfully - get the first output
% which is the full workspace struct.
    workspaceOut = job.Tasks(1).OutputArguments{1};

    % Deal with any sub-selection from varargin
    if numel(varargin) > 0
        workspaceOut = iSelectVariables(workspaceOut, varargin);
    end
end

% ---------------------------------------------------------------------------------
%
% ---------------------------------------------------------------------------------
function ws = iSelectVariables(ws, args)
% Test that all the inputs we are parsing are strings
    isStringFun = @(x) ischar(x) && isvector(x) && size(x, 1) == 1;
    if ~all(cellfun(isStringFun, args))
        error(message('parallel:convenience:BatchLoadInvalidArgument'))
    end
    % Three types of args could be passed in -regexp, var* and var.
    % We need to partition these. Firstly get all regexp args, which
    % will follow an arg of -regexp
    regexpIndex = find(strcmp(args, '-regexp'), 1, 'first');
    origPatterns = args;
    % Did we get a -regexp?
    if ~isempty(regexpIndex)
        regexpArgs = args(regexpIndex+1:end);
        args = args(1:regexpIndex-1);
    else
        regexpArgs = {};
    end
    % Remove any invalid strings
    valid = ~cellfun('isempty', regexp(args, '^[a-zA-Z\*][a-zA-Z0-9_\*]*$', 'once'));
    % Indicate that there are invalid strings
    for i = find(~valid)
        warning(message('parallel:convenience:VariablePatternNotFound', origPatterns{ i }));
    end
    % Next, look for anything with a * in it
    star = ~cellfun('isempty', regexp(args, '\*', 'once')) & valid;
    name  = valid & ~star;
    % We end up with 1 set of searches
    % Put ^ at the beginning of the search and $ at the end
    nameArgs = regexprep(args(name), '.*', '^$0\$');
    % Put ^ at the beginning of the search and $ at the end and replace * with .*
    starArgs = regexprep(regexprep(args(star), '\*', '.*'), '.*', '^$0\$');
    % Create the complete list of patterns and the original patterns they were
    % derived from to print out in error messages
    patterns = [nameArgs starArgs regexpArgs];
    origPatterns = [origPatterns(name) origPatterns(star) origPatterns(regexpIndex+1:end)];
    % Get the field names from the workspace structure
    wsNames = fieldnames(ws); 
    found = false(numel(wsNames), 1);
    % Loop over the patterns looking for variables that fit them
    for i = 1:numel(patterns)
        foundThis = ~cellfun('isempty', regexp(wsNames, patterns{i}, 'once'));
        found = found | foundThis;
        if ~any(foundThis)
            warning(message('parallel:convenience:VariablePatternNotFound', origPatterns{ i }));
        end
    end
    % Get the correct variables from the structure
    wsVars = struct2cell(ws);
    ws = cell2struct(wsVars(found), wsNames(found), 1);
end
