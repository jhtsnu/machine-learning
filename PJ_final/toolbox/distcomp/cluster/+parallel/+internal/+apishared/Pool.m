% Pool - helpers relating to setting up and tearing down MATLABPOOL.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) Pool
    methods ( Hidden, Static )
        function isPoolTask = instantiate( isInteractive, args )
        % We need to decide what sort of matlabpool job we are. There are
        % interactive and batch matlabpool jobs. If we are interactive then all
        % labs will be connecting back to the client - whos socket address will be
        % provided in the input args to the task. If we are batch then lab 1 needs
        % to become the client and all the rest need to communicate back to them

            if isInteractive
                interactiveObject = parallel.internal.pool.InteractiveWorker();
                isPoolTask = true;
                obj = parallel.internal.pool.SessionManager.setSessionObject( interactiveObject );
                obj.start(args{:});
            else
                % Starting the MatlabPool client and labs
                leadingTaskNum = 1;
                if labindex == 1
                    interactiveObject = parallel.internal.pool.PoolClient();
                    isPoolTask = false;
                else
                    interactiveObject = parallel.internal.pool.PoolWorker();
                    isPoolTask = true;
                end
                obj = parallel.internal.pool.SessionManager.setSessionObject( interactiveObject );
                obj.start(leadingTaskNum);
            end

        end

        function OK = shutdown( isPoolTask )
        % Shutdown the pool.
            if ~isPoolTask
                % Get the interactive client object
                obj = parallel.internal.pool.SessionManager.getSessionObject();
                % Stop the labs if we are the client
                OK = obj.stopLabsAndDisconnect;
                % Delete the interactive object
                parallel.internal.pool.SessionManager.clearSessionObject();
            else
                OK = true;
            end
        end
    end
end
