% Interface to Job initialization data

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden, Sealed ) JobInitData
    methods ( Static )
        function data = getData( job )
            data = getJobInitData( job );
        end
        function setData( job, data )
            setJobInitData( job, data );
        end
    end
end
