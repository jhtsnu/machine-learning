% DataLocConv class - converts between API1 and API2 DataLocations

% Copyright 2011-2012 The MathWorks, Inc.
classdef (Hidden) DataLocConv
    properties (Constant, GetAccess = private)
        Api1PcField = 'pc';
        Api2PcField = 'windows';
        Api1UnixField = 'unix';
        Api2UnixField = 'unix';

        Api1FieldNames = {parallel.internal.apishared.DataLocConv.Api1PcField, ...
            parallel.internal.apishared.DataLocConv.Api1UnixField};
        Api2FieldNames = {parallel.internal.apishared.DataLocConv.Api2PcField, ...
            parallel.internal.apishared.DataLocConv.Api2UnixField};
    end

    methods (Static)
        function api1Val = toApi1(api2CharOrStruct)
        % toApi1 - convert the supplied API2 DataLocation to API1
            import parallel.internal.apishared.DataLocConv;
            iValidateDataLocation(api2CharOrStruct, DataLocConv.Api2FieldNames);
            if ischar(api2CharOrStruct)
                api1Val = api2CharOrStruct;
            else
                api1Val = iToApi1Struct(api2CharOrStruct);
            end
        end
        
        function api1Struct = toApi1Struct(api2CharOrStruct)
        % toApi1Struct - convert the supplied API2 DataLocation to an API1 structure
            import parallel.internal.apishared.DataLocConv;
            iValidateDataLocation(api2CharOrStruct, DataLocConv.Api2FieldNames);
            api1Struct = iToApi1Struct(api2CharOrStruct);
        end

        function api2Val = toApi2(api1CharOrStruct)
        % toApi2 - convert the supplied API1 DataLocation to API2
            import parallel.internal.apishared.DataLocConv;
            iValidateDataLocation(api1CharOrStruct, DataLocConv.Api1FieldNames);
            if ischar(api1CharOrStruct)
                api2Val = api1CharOrStruct;
            else
                api2Val = iToApi2Struct(api1CharOrStruct);
            end
        end
        
        function api2Struct = toApi2Struct(api1CharOrStruct)
        % toApi2Struct - convert the supplied API1 DataLocation to an API2 structure
            import parallel.internal.apishared.DataLocConv;
            iValidateDataLocation(api1CharOrStruct, DataLocConv.Api1FieldNames);
            api2Struct = iToApi2Struct(api1CharOrStruct);
        end
    end
end

function iValidateDataLocation(charOrStruct, allowedFields)
if (ischar(charOrStruct) && isrow(charOrStruct)) || ...
    (isstruct(charOrStruct) && isequal(sort(fieldnames(charOrStruct)), sort(allowedFields(:))))
    return;
end
throwAsCaller(MException(message('parallel:cluster:DataLocConvInvalidDataLocation', sprintf( '%s ', allowedFields{ : } ))));
end

function api1Struct = iToApi1Struct(api2CharOrStruct)
import parallel.internal.apishared.DataLocConv;
api1Struct = iToDataLocStruct(api2CharOrStruct, ...
    DataLocConv.Api2PcField, DataLocConv.Api2UnixField, ... 
    DataLocConv.Api1PcField, DataLocConv.Api1UnixField);
end

function api2Struct = iToApi2Struct(api1CharOrStruct)
import parallel.internal.apishared.DataLocConv;
api2Struct = iToDataLocStruct(api1CharOrStruct, ...
    DataLocConv.Api1PcField, DataLocConv.Api1UnixField, ...
    DataLocConv.Api2PcField, DataLocConv.Api2UnixField);
end

function newStruct = iToDataLocStruct(charOrStruct, oldPcField, oldUnixField, newPcField, newUnixField)
if isstruct(charOrStruct)
    pcVal = charOrStruct.(oldPcField);
    unixVal = charOrStruct.(oldUnixField);
else
    if ispc
        pcVal = charOrStruct;
        unixVal = '';
    else
        pcVal = '';
        unixVal = charOrStruct;
    end
end
newStruct = struct(newPcField, pcVal, newUnixField, unixVal);
end

