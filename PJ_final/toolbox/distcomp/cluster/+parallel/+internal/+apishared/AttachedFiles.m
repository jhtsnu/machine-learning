% AttachedFiles - deal with AttachedFiles / FileDependencies

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) AttachedFiles
    methods ( Static )
        function [fileData, attachedFiles] = zipFiles( attachedFiles, tempDir )
        % zipFiles - zip up the non-empty entries in attachedFiles into
        % fileData. Returns the resolved non-empty entries.
            
            % API1 calls this function without a tempDir.
            if nargin < 2
                tempDir = [];
            end

            % Strip empties before handing to iGetZipData
            attachedFiles = attachedFiles( ~cellfun( @isempty, attachedFiles ) );
            if isempty( attachedFiles )
                fileData = int8([]);
            else
                [fileData, attachedFiles] = iGetZipData( attachedFiles, tempDir );
            end
        end
        
        function [zipName, cleanupObject] = createZipFile( attachedFiles, tempDir )
            [zipName, cleanupObject] = iZipFiles( attachedFiles, tempDir );
        end

        function [convertedFiles, pathMap] = generateTempDirectoryStructure( attachedFiles, pathMap )
        % generateTempDirectoryStructure - transform the attached file paths into the required
        %   directory structure for the zip archive to be sent to the workers, which will be 
        %   rebuilt at their end in the AttachedFilesFolder.  If a pathMap is supplied, then 
        %   the convertedFiles will reuse the locations specified in the pathMap; additional
        %   locations will be added to the pathMap.
        %
        %   Root paths (i.e. / or c:\) are removed or replaced, to make the paths relative to
        %   the zip archive root.
        %
        %   convertedFiles - a cell array of strings of the same length as attachedFiles with
        %   the paths converted.
        %
        %   pathMap - a map of original path roots to new path roots in the zip archive
        %   (i.e. excluding the file name), as paths are truncated to prevent paths getting
        %   too long for windows (which has a limit of 260 characters for a path). All files
        %   in a particular directory are guaranteed to be in the same directory when the
        %   paths have been converted.
        %
        %   All slashes are converted to '/'.
        %
        %   For example (here we mix unix and windows paths as an example, although this
        %   would not happen in practice), given
        %       attachedFiles = { 'c:\path\to\a\file1.m', '/a/path/to/a/directory', ...
        %           '/other/path/to/a/file2.m', '/other/path/to/a/file3.m' }
        %
        %   the outputs would be:
        %
        %       convertedFiles = { 'c/tp123456/file.m', 'tp654321', 'tpabcdef/file2.m', ...
        %           'tpabcdef/file3.m' };
        %
        %       pathMap = AttachedFileMap[ 'c:\path\to\a' -> 'c/tp123456', ...
        %                                  '/a/path/to/a/directory' -> 'tp654321', ...
        %                                  '/other/path/to/a' -> 'tpabcdef' ];
        %
        %   Note: If the generated path name (tp123456) ends up with the path being
        %   longer than the original, we use the original root path (but still converting
        %   drive letters) e.g. 's:\a\1.m' -> 's/a/1.m' NOT 's/tp123456/1.m'
        %

            if nargin < 2
                % Create map to store the original file paths and the new generated paths
                % which will be stored in the zip archive.
                pathMap = parallel.internal.apishared.AttachedFileMap();
            end

            % Windows can always handle UNIX style slashes, but UNIX cannot handle
            % windows style slashes, so always convert to UNIX slashes ('/').
            attachedFiles = cellfun( @(x) strrep( x, '\', '/' ), attachedFiles, ...
                'UniformOutput', false );

            [allPathStarts, allPathEnds] = cellfun( @iSplitAttachedFilePaths, attachedFiles, ...
                'UniformOutput', false );
            
            convertedFiles = cell( size( attachedFiles ) );
            % To keep the path lengths shorter than the maximum file path limit on
            % windows we need to truncate the paths. However we need to keep the
            % relative position of files in a directory, and of +packages, @Classes
            % and private directories.
            for i = 1:numel( attachedFiles )
                pathStart = allPathStarts{i};
                pathEnd = allPathEnds{i};

                % Check if we've already created a new path to store a previous file.
                if pathMap.isKey( pathStart )
                    newPathStart = pathMap.get( pathStart );
                    newPath = fullfile( newPathStart, pathEnd );
                else
                    % truncate the path
                    [newPath, newPathStart] = iMaybeTruncatePath( attachedFiles{i}, ...
                        pathStart, pathEnd );
                    if isempty( pathStart )
                        % If the pathStart is empty, we must just have a bare file.
                        % So map it to itself.
                        pathMap.put( pathEnd, pathEnd );
                    else
                        % put the path we've used into the map
                        pathMap.put( pathStart, newPathStart );
                    end
                end
                convertedFiles{i} = newPath;
            end
        end

        function [tempDir, cleanup] = copyAllFilesToTempDir( sourcePaths, destPaths )
            assert( length(sourcePaths) == length(destPaths) );

            % destPaths should be a list of relative paths to the root.

            % Create a temporary directory so we can copy the files there and create
            % the directory structure we want.
            tempDir = tempname();
            iMkDirOrError( tempDir );
            cleanup = onCleanup( @() iRmDirOrError( tempDir, 's' ) );

            for ii = 1:length( sourcePaths )
                destPaths{ii} = fullfile( tempDir, destPaths{ii} );
                newDir = fileparts( destPaths{ii} );
                iMkDirOrError( newDir );
                try
                    iCopyFileAndRetry( sourcePaths{ii}, destPaths{ii} );
                catch E
                    % We failed to copy the file even after retrying.
                    throw( E );
                end
            end
        end

        function fcns = convertFunctionHandleForDependencyAnalysis( toConvert )
        % Given a function handle, return the names of functions or paths to
        % functions to be analyzed by matlab.depfun.internal.requirements.
        %
        % The function may return either function names, or in the case of a
        % sub- or nested- function, it will return the path to the parent file.
        %
        % In general this may return more than one function as output, as we
        % may have been given an anonymous function that calls several other
        % functions.
            if isempty( toConvert )
                fcns = {};
                return;
            end
            fInfo = functions( toConvert );
            if strcmpi( fInfo.type, 'scopedfunction' ) || strcmpi( fInfo.type, 'nested' )
                % We have a subfunction or nested function, depfun cannot
                % resolve these, so instead use the parent function to
                % calculate the requirements.  This will overestimate the
                % dependencies of this function.  There is an enhancement
                % request for depfun to support subfunctions and nested
                % functions in g863848.
                fcns = fInfo.file;
            elseif strcmpi( fInfo.type, 'anonymous' )
                expression = fInfo.function;
                workspace = fInfo.workspace{:};
                % We may also have an anonymous function, or an arbitrary
                % expression from batch, so call 'whatToAnalyze' to figure
                % out what to pass to depfun.
                fcns = parallel.internal.apishared.whatToAnalyze( expression, workspace );
            else
                fcns = func2str( toConvert );
            end
        end

        function files = calculateAttachedFiles( paths )
        % Use requirements to calculate the dependencies of the provided files, and
        % return the paths of the dependencies.
            % If we end up with nothing to analyze at this point return early.
            if isempty( paths )
                files = {};
                return;
            end
            % We require a cellstr, but will accept a plain string.
            if ~iscellstr( paths )
                if ( ischar( paths ) && isrow( paths ) )
                    paths = { paths };
                else
                    error( message( 'parallel:internal:CalcAttachedFilesInputs' ) );
                end
            end
            try
                dctSchedulerMessage(6, 'Calculating requirements for %s', ...
                    sprintf( '''%s ''', paths{:} ) );
                % If paths includes only toolbox files, or built-ins, requirements will warn.
                % Our users are not interested in this warning, so turn it off.
                warningState = warning( 'off', 'MATLAB:Completion:AllInputsExcluded' );
                warningCleanup = onCleanup( @() warning( warningState ) );
                deps = parallel.internal.apishared.calculateDependencies( paths );
                if ~isempty( deps )
                    files = { deps.path };
                    % Remove empty paths
                    files = files( ~cellfun( @isempty, files ) );
                    % Depfun can give paths relative to matlabroot, so resolve the paths to
                    % absolute paths here.
                    files = cellfun( @iResolveDepfunPath, files, 'UniformOutput', false );
                    files = cellfun( ...
                        @parallel.internal.apishared.FilenameUtils.safeWhich, files, ...
                            'UniformOutput', false );
                else
                    files = {};
                end
            catch e
                % This is a warning, as we may be able to run the task function even
                % though depfun failed.
                warning( message( 'parallel:cluster:DepfunError', ...
                    sprintf( '  %s\n', paths{:} ), e.message ) );
                files = {};
            end
        end

        function attachedFiles = filterToolboxAndBuiltin( attachedFiles )
        % filterToolboxAndBuiltin - given a list of attached files, filter out all
        % those that are names of builtins or that reside under the MATLAB toolbox
        % directory.
            filterFcn = @(x) exist( x, 'builtin' ) || iIsUnderToolboxRoot( x );
            toFilter = cellfun( filterFcn, attachedFiles );
            attachedFiles = attachedFiles( ~toFilter );
        end

        function pathOut = resolvePath( pathIn )
        % resolvePath - Resolve the given path to an absolute path for an attached file.
        %
        % The given path may point to a file or directory, may or may not be on the
        % MATLAB search path and may or may not be relative.
        %
        % This function guarantees that its outputs will not return empty if passed
        % to 'which'. It does not guarantee that the output is an absolute path, as it
        % may also be the name of a built-in function.
        %
        % The precedence rules we are enforcing here given an attached file 'test' are:
        %   (0) Take whatever the result from which gives.
        %   If that fails, or it finds a built-in or something under 'toolbox':
        %   (1) ./test.m <- A MATLAB file in the current working directory
        %   (2) ./test <- A non-MATLAB file in the current working directory
        %   (3) ./test/ <- A sub-directory
        %   Finally the file under toolbox or the name of the built-in.
        %   (4) [matlabroot]/toolbox/sometoolbox/test.m <- A MATLAB file under toolbox root
        %   OR
        %   (4) test <- A built-in function (this is just an example 'test' is not a built-in)
        
            % (0) - call safeWhich which won't error, and may or may not succeed in finding
            %       the path.
            [whichPath, whichSuccess] = parallel.internal.apishared.FilenameUtils.safeWhich( pathIn );
            % (4) - If 'which' finds a toolbox file, the user probably did not
            % mean to attach this.
            isUnderToolboxRoot = iIsUnderToolboxRoot( whichPath );
            % Undocumented built-ins (like 'feature') do not have a path.
            isBuiltin = exist( whichPath, 'builtin' );
            if whichSuccess && ~( isUnderToolboxRoot || isBuiltin )
                % (0) - 'which' succeeded in finding a file based on the path
                % and it wasn't a toolbox function or a built-in.
                pathOut = whichPath;
                return;
            end
            % If which did not succeed in resolving the path, or it found a file under
            % the toolbox root or a built-in then try to resolve it ourselves.
            try
                % (1) - (3) - if we got here, we've probably got a relative path to a file
                % or directory, try and make an absolute path out of it.
                absPath = parallel.internal.apishared.FilenameUtils.getAbsolutePath( pathIn );
            catch E %#ok<NASGU>
                % Getting the absolute path failed, carry on with the input path.
                absPath = pathIn;
            end
            % Check the existence of the new absolute path we've constructed.
            pathExists = exist( absPath, 'file' );
            if pathExists
                % We have found a file or folder with this absolute path.
                pathOut = absPath;
            elseif whichSuccess
                pathOut = whichPath;
            else
                % Neither which nor getAbsolutePath could find the input - error.
                error( message( 'parallel:cluster:FileDoesNotExist', pathIn ) );
            end
        end

        function formattedFiles = formatAttachedFilesForDisplay( attachedFiles )
        % Format the attached files provided for display.
        %
        % Relative paths are used where possible to reduce the
        % horizontal extent of the display.
            formattedFiles = cellfun(@iFormatAttachedFileForDisplay, ...
                attachedFiles, 'UniformOutput', false);
            % This concatenates all the formatted strings into one string
            % to pass as an argument to the message catalog.
            formattedFiles = [formattedFiles{:}];
        end

        function files = addAuthFilesIfDeployed( files )
        % addAuthFilesIfDeployed - add .auth files if we are deployed.
            if isdeployed
                dctSchedulerMessage( 2, 'Searching for .auth files' );
                authFiles = cell( size( files ) );
                numAuthFiles = 0;
                for n = 1:numel( files )
                    thisFile = parallel.internal.apishared.FilenameUtils.safeWhich( files{n} );
                    dctSchedulerMessage( 4, sprintf( 'Checking %s...', thisFile ) );
                    % Does this file have a .auth file?
                    % foo.mexext ---> foo_mexext.auth
                    [location, name, ext] = fileparts( thisFile );
                    possibleAuthFile = fullfile( location, [name, '_', ext(2:end), '.auth'] );
                    dctSchedulerMessage( 4, sprintf( 'Looking for %s...', possibleAuthFile ) );
                    % If the file exists, we'll add it to the FileDependencies as well.
                    if exist( possibleAuthFile , 'file' )
                        dctSchedulerMessage( 4, sprintf( 'Found %s', possibleAuthFile ) );
                        numAuthFiles = numAuthFiles + 1;
                        authFiles{numAuthFiles} = possibleAuthFile;
                    else
                        dctSchedulerMessage( 4, sprintf( 'Did not find %s', possibleAuthFile ) );
                    end
                end
                filesToAdd = authFiles(1:numAuthFiles);
                files = [files(:); filesToAdd(:)];
            end
        end

        function files = expandAttachedFiles( files )
        % AttachedFiles can be a mixture of files and directories to attach to the
        % job. For dependency analysis we need a list of all the files.
            fileFilterFcn = @iCodeOnly;
            files = cellfun( ...
                @(x) parallel.internal.apishared.FilenameUtils.listAll( x, fileFilterFcn ), ...
                files, 'UniformOutput', false );
            files = [ files{:} ];
        end
        
    end
end

% -------------------------------------------------------------------------
function out = iCodeOnly( fileIn )
% When checking dependencies of attached files, we only want to look through
% code files. This is a workaround for g871715 which means depfun can't
% handle this for us.
    [~, ~, ext] = fileparts( fileIn );
    out = strcmpi( ext, '.m' );
end

% -------------------------------------------------------------------------
function [zipbytes, files] = iGetZipData(files, tempDir)
% iGetZipData - Zip the supplied files and retrieve the data into a uint8 
% array.

    [zipname, cleanupObject] = iZipFiles(files, tempDir); %#ok<NASGU>
    % Use a try/catch here rather than another cleanup object for fclose
    % because we want to be sure that the fclose happens before the file
    % is deleted.
    fid = fopen(zipname,'r');
    try
        zipbytes = fread(fid,'int8=>int8');
        fclose(fid);
    catch err
        fclose(fid);
        rethrow(err);
    end
end

% -------------------------------------------------------------------------
function [zipname, cleanupObject, files] = iZipFiles(files, tempDir)
% iZipFiles - Zip the supplied files into the supplied temp directory.
% The files and root dir arguments behave the same way as they do in 
% the ZIP command.

    % All files are given as relative paths, so that zip maintains their
    % structure.

    % Make sure that no directories end in a file separator as the behaviour of
    % zip with absolute or relative paths is different under these circumstances 
    % Note that we need to escape the filesep for windows for the regexp
    % expression to do the right thing.  On unix, escaping the filesep has
    % no effect and is safe to do.
    files = regexprep(files, ['\' filesep '$'], '');

    zipname = [tempname '.zip'];
    zip( zipname, files, tempDir );
    cleanupObject = onCleanup( @() iCleanupFile(zipname) );
end

% -------------------------------------------------------------------------
function iCleanupFile(filename)
    warningState = warning('off', 'MATLAB:DELETE:Permission');
    delete(filename);
    warning(warningState);
    % If the zip file still exists then lets delete it later
    if exist(filename, 'file') && usejava('jvm')
        file = java.io.File(filename);
        com.mathworks.toolbox.distcomp.util.FileDeleter.getInstance.deleteFileLater(file);
    end
end

function pathOut = iResolveDepfunPath( pathIn )
% iResolveDepfunPath - Resolve paths returned by matlab.depfun.internal.requirements
%
% Depfun returns absolute paths OR paths relative to matlabroot. If the
% relative paths it gives are not on the path or in the current directory, they
% are not picked up by 'which'. So this function attempts to construct paths that
% 'which' can handle by prepending the matlabroot, if 'exist' cannot find the file.
% NOTE: We are relying on the fact that if 'exist' returns true for a given path
% 'which' will be able to resolve the path, even if that path is not absolute.

if ~exist( pathIn, 'file' )
    if isdeployed
        testPath = fullfile( ctfroot, pathIn );
    else
        testPath = fullfile( matlabroot, pathIn );
    end
    if exist( testPath, 'file' )
        pathOut = testPath;
    else
        % If we can't resolve the path, return it as is.
        % TODO: This shouldn't happen, depfun should always return paths to things
        % we can resolve. Perhaps this should error?
        pathOut = pathIn;
    end
else
    pathOut = pathIn;
end
end

% -------------------------------------------------------------------------
function [pathStart, pathEnd] = iSplitAttachedFilePaths( pathIn )

% Split the path up to the first +package, @Class, private directory or file.
isDir = exist( pathIn, 'dir' ) == 7;
% If it is not found on disk as a directory, either we can't find it, or it's a file
% in either case we want to treat it as a file.
treatAsFile = ~isDir;
if treatAsFile
    % Now we know we have a file, strip the file from the path,
    % as specialSplitPath MUST take a path to a directory not a
    % file.
    [pathToSplit, fname, ext] = fileparts( pathIn );
else
    pathToSplit = pathIn;
end
[pathStart, pathEnd] = ...
    parallel.internal.apishared.FilenameUtils.specialSplitPath( pathToSplit );
if treatAsFile
    % Add the file back on to the pathEnd.
    pathEnd = fullfile( pathEnd, [fname, ext] );
end
end

% -------------------------------------------------------------------------
function [newPath, newPathStart] = iMaybeTruncatePath( pathIn, pathStartIn, pathEndIn )

[~, newPathStart] = fileparts( tempname );
newPath = fullfile( newPathStart, pathEndIn );
% If the new path we've created is longer than the original
% then simply keep the original path.
if length( newPath ) > length( pathIn )
    % We must always return relative paths, so that zip() maintains
    % the directory structure, so we must make sure the attached file
    % is specified by a relative path by stripping its root if it exists.
    newPath = parallel.internal.apishared.FilenameUtils.stripRoot( pathIn );
    newPathStart = parallel.internal.apishared.FilenameUtils.stripRoot( pathStartIn );
end

end

% -------------------------------------------------------------------------
function stringOut = iFormatAttachedFileForDisplay( attachedFile )
% Display a single attached file. We try to use a path relative to the
% current working directory to minimize the width of the displayed path.
relativePath = ...
    parallel.internal.apishared.FilenameUtils.getRelativePath( pwd, attachedFile );
stringOut = sprintf( '  %s\n', relativePath );
end

% -------------------------------------------------------------------------
function iMkDirOrError( newDir, varargin )
% Wrapper around mkdir to check the directory has been created and error if not.
    if ~exist( newDir, 'dir' )
        [status, msg, messageid] = mkdir( newDir, varargin{:} );
        if ~status || ~exist( newDir, 'dir' )
            mkdirError = MException( messageid, '%s', msg );
            err = MException( message( 'parallel:cluster:CouldNotCreateTempDir' ) );
            err = err.addCause( mkdirError );
            throw( err );
        end
    end
end

% -------------------------------------------------------------------------
function iRmDirOrError( toRemove, varargin )
% Wrapper around rmdir to check the directory has been removed and error if not.
    if exist( toRemove, 'dir' )
        [status, msg, messageid] = rmdir( toRemove, varargin{:} );
        if ~status || exist( toRemove, 'dir' )
            rmDirError = MException( messageid, '%s', msg );
            err = MException( message( 'parallel:cluster:CouldNotRemoveTempDir', toRemove ) );
            err = err.addCause( rmDirError );
            throw( err );
        end
    end
end

% -------------------------------------------------------------------------
function iCopyFileAndRetry( sourcePath, destPath )
% Copy the file from the source to the destination, retrying if it fails.
    try
        copyfile( sourcePath, destPath );
    catch E
        % This is a diagnostic, we do not want to assume anything about
        % sourcePath or destPath, so we use the form of exist with only
        % one input and log the result.
        sourceExist = exist( sourcePath ); %#ok<EXIST>
        destExist = exist( destPath ); %#ok<EXIST>
        dctSchedulerMessage( 1, ...
            sprintf( ['Copying files failed, retrying.\n' ...
            'Source: %s (exist: %d),\nDestination: %s (exist: %d),\n' ...
            'Original error: %s'], ...
            sourcePath, sourceExist, destPath, destExist, E.getReport() ) );
        copyfile( sourcePath, destPath );
    end
end

% -------------------------------------------------------------------------
function isUnderToolboxRoot = iIsUnderToolboxRoot( pathIn )
% Determine whether a given path points to a location under the MATLAB
% toolbox directory.
    findToolboxRoot = strfind( pathIn, toolboxdir('') );
    isUnderToolboxRoot = ~isempty( findToolboxRoot ) && findToolboxRoot == 1;
end
