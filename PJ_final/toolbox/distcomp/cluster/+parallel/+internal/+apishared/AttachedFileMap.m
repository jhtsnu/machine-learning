% AttachedFileMap - A wrapper around StableCharMap for storing the AttachedFiles map.
%
% The AttachedFileMap maps client paths to paths on the workers.
%
% We have to deal with mixed operating systems, so all keys and values are converted 
% to have '/' as the path separator.

%   Copyright 2012 The MathWorks, Inc.

classdef AttachedFileMap < handle
    properties ( Access = private )
        BackingMap
    end

    properties ( Dependent )
        Length
    end

    methods
        function obj = AttachedFileMap()
            obj.BackingMap = parallel.internal.apishared.StableCharMap();
        end

        function val = get( obj, key )
            val = obj.BackingMap.get( iConvertSlashes( key ) );
        end

        function val = get.Length( obj )
            val = obj.BackingMap.Length;
        end

        function put( obj, key, value )
            % We need to translate the values as well, as these are the
            % translated paths on the worker.
            obj.BackingMap.put( iConvertSlashes( key ), iConvertSlashes( value ) );
        end

        function key = isKey( obj, key )
            key = obj.BackingMap.isKey( iConvertSlashes( key ) );
        end

        function val = keys( obj )
            val = obj.BackingMap.keys();
        end

        function val = values( obj )
            val = obj.BackingMap.values();
        end
    end
end

function key = iConvertSlashes( key )
    key = strrep( key, '\', '/' );
end
