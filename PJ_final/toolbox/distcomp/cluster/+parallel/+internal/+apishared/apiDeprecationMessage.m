function warningCleanup = apiDeprecationMessage(oldName, newName)
% Show the deprecation message for API1 as a warning or error

%   Copyright 2012 The MathWorks, Inc.

persistent callers;
warningCleanup = [];

try
    % Turn off the stack for the warnings.
    s = warning( 'off', 'backtrace' );
    backtraceCleanup = onCleanup( @() warning(s) );

    % We only want to warn for each API1 function once, so we need to keep
    % a map of functions that have already warned.
    if isempty( callers )
        callers = {};
    end

    deprecationMessageId = 'parallel:cluster:FunctionToBeRemoved';
    % If the warning is off, this means we are in a nested call to an API1
    % function from another API1 function. In which case we do not want to
    % warn, or add ourselves to the map of functions that have warned.
    w = warning( 'query', deprecationMessageId );
    if ~any(strcmp( callers, oldName )) && strcmp( w.state, 'on' )
        % For R2013a we warn. This will become an error later.
        if nargin < 2 && strcmp( oldName, 'createJob' )
            % createJob needs a different message.
            % The warning state of createJob is not important, because
            % no PCT code calls createJob with zero inputs.
            warning( message('parallel:cluster:CreateJobZeroInputs') );
        else
            warning( message(deprecationMessageId, oldName, newName) );
        end

        callers{end+1} = oldName;
    end
    
    % The API1 functions call each other, return an onCleanup object which
    % will turn off the parallel:cluster:FunctionToBeRemoved warning for the
    % duration of the calling function.
    if strcmp( w.state, 'on' )
        warning( 'off', deprecationMessageId );
        warningCleanup = onCleanup( @() warning( 'on', deprecationMessageId ) );
    end
catch E %#ok<NASGU>
    % Failing to warn about a deprecated function should not be fatal.
end

end
