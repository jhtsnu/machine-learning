% JobFilesInfo - information about the files required by a job and tasks.

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden, Sealed ) JobFilesInfo

    properties ( SetAccess = immutable )
        RootDirectory      % Name of root directory on this client

        % All following properties are cellstr of relative paths
        ClusterConfigFiles % files required for cluster to function
        JobInputFiles      % files not modified during job execution
        JobOutputFiles     % files modified during job execution
        JobStateFiles      % state indicator file, maybe modified during job execution
        TaskFiles          % files relating to tasks (actually just the directory 
                           % containing all tasks)
    end

    methods
        function obj = JobFilesInfo( storage, jobLocation, numTasks )
        % This is essentially a 'helper' to distcomp.filestorage, but do not
        % wish to add this stuff there.
            validateattributes( storage, {'distcomp.filestorage'}, {'scalar'} );
            validateattributes( jobLocation, {'char'}, {'row'} );

            obj.RootDirectory      = storage.StorageLocation;
            obj.ClusterConfigFiles = {storage.MetadataFilename};

            % Ask the file storage for the appropriate job file extension for
            % state
            jobStateFile = [jobLocation, ...
                            storage.pGetExtensionsForFields('job', 'state')];

            % Knowledge that's there's only one input-only file right here.
            jobInputFile = [jobLocation, ...
                            storage.pGetExtensionsForFields( 'job', 'createtime' )];

            % Get all the job files and remove the jobStateFiles from them.  The
            % remainder are considered "data" files.
            allJobFiles  = strcat( jobLocation, storage.Extensions );

            % Remove state / input file
            jobOutFiles = allJobFiles( ~strcmpi( allJobFiles, jobStateFile ) );
            jobOutFiles = jobOutFiles( ~strcmpi( jobOutFiles, jobInputFile ) );

            % All the task files are considered to be data files
            if numTasks > 0
                taskFiles = { jobLocation };
            else
                taskFiles = {};
            end

            obj.JobInputFiles  = {jobInputFile};
            obj.JobOutputFiles = jobOutFiles;
            obj.JobStateFiles  = {jobStateFile};
            obj.TaskFiles      = taskFiles;
        end
    end
end
