% HPCServerUtils - Utilities for HPC Server

%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) HPCServerUtils
    methods (Static)
        function clientVersion = getDefaultClientVersion()
            if distcomp.HPCServerSchedulerConnection.testClientCompatibilityWithMicrosoftAPI
                clientVersion = distcomp.HPCServerSchedulerConnection.getAPIVersion;
            elseif distcomp.CCSSchedulerConnection.testClientCompatibilityWithMicrosoftAPI
                clientVersion = distcomp.CCSSchedulerConnection.getAPIVersion;
            else
                error(message('parallel:cluster:HPCServerClientUtilitiesNotInstalled'));
            end
        end
    
        function headNodes = findHeadNodesFromAD(clusterVersion)
            import parallel.internal.types.HPCServerClusterVersion
            validateattributes(clusterVersion, {'char'}, {'row'});
            
            clusterVersion = HPCServerClusterVersion.fromName(clusterVersion);
            headNodes = '';
            
            % The magic filter strings to use to find the head nodes.
            % '(keywords=Version2)' gives all the HPC Server 2008/HPC Server 2008 R2 head nodes.
            % '(keywords=Version3)' gives all the HPC Server 2008 R2 head nodes.
            % Without the '(keywords=Version*)', you get all head nodes from 
            % CCS, HPC Server 2008 and HPC Server 2008 R2
            % Note that '(keywords=Version1)' returns nothing.
            % Note also that we currently do not distinguish between HPC Server 2008 and HPC Server 2008 R2,
            % so a cluster version of 'HPCServer2008' will return both flavors of HPC Server head node.
            allVersionsFilterString = '(&(serviceClassName=MicrosoftComputeCluster))';
            hpcServerFilterString = '(&(serviceClassName=MicrosoftComputeCluster)(keywords=Version2))';

            try
                % Add the Active Directory Services assembly
                NET.addAssembly('System.DirectoryServices');
            catch err
                ex = MException(message('parallel:cluster:HPCServerUtilsFailedToLoadADSearchLibraries'));
                ex = ex.addCause(err);
                throw(ex);
            end
                
            try
                % Create the directory searcher
                ds = System.DirectoryServices.DirectorySearcher();
                cleanup = onCleanup(@() ds.Dispose());

                hpcServerHeadnodes = iSearchActiveDirectoryForComputers(ds, hpcServerFilterString);
                if clusterVersion == HPCServerClusterVersion.CCS
                    allHeadnodes = iSearchActiveDirectoryForComputers(ds, allVersionsFilterString);
                    headNodes = setdiff(allHeadnodes, hpcServerHeadnodes);
                elseif clusterVersion == HPCServerClusterVersion.HPCServer2008
                    headNodes = hpcServerHeadnodes;
                else
                    assert(false);
                end
            catch err
                ex = MException(message('parallel:cluster:HPCServerUtilsADSearchFailed'));
                ex = ex.addCause(err);
                throw(ex);
            end
        end

        function headNode = findHeadNodeFromEnvironment()
            headNode  = getenv('CCP_SCHEDULER');
        end
        
        function [connection, newConnectionMade] = getServerConnection(host, versionString)
            import parallel.internal.types.HPCServerClusterVersion
            validateattributes(host, {'char'}, {'row'});
            validateattributes(versionString, {'char'}, {'row'});
            clusterVersion = HPCServerClusterVersion.fromName(versionString);
            [connection, newConnectionMade] = iGetServerConnection(host, clusterVersion);        
        end
        
        function [api2PropNames, vals, propertyFilename] = getValuesFromClusterEnvironment(serverConnection)
            validateattributes(serverConnection, {'distcomp.AbstractMicrosoftSchedulerConnection'}, {'scalar'});
            % Note that we read the property file only once per MATLAB session
            [api2PropNames, vals, propertyFilename] = iGetPropsFromClusterEnvironment(serverConnection);
        end
        
        function [userWithDomain, userOnly] = getUsername()
            net = actxserver('WScript.Network');
            userOnly = net.UserName;
            userWithDomain = sprintf('%s\\%s', net.UserDomain, userOnly);
        end
    end
end

%----------------------------------------------
function dnsNames = iSearchActiveDirectoryForComputers(directorySearcher, filterString)
dnsNames = '';
try
    % Find all nodes matching the filter string
    directorySearcher.Filter = filterString;
    allNodes = directorySearcher.FindAll();
    cleanup = onCleanup(@() allNodes.Dispose());
catch err
    ex = MException(message('parallel:cluster:HPCServerUtilsADFilterStringSearchFailed', filterString));
    ex = ex.addCause(err);
    throw(ex);
end

numNodes = allNodes.Count;
if numNodes == 0
    return;
end

dnsNames = cell(numNodes, 1);
for ii = 1:numNodes
    try
        props = allNodes.Item(ii-1).Properties;
        dnsName = props.Item('servicednsname');
        % There really ought to be a DNS name otherwise we wouldn't have found
        % the machine, so this is purely for paranoia
        if dnsName.Count > 0
            dnsNames{ii} = char(dnsName.Item(0));
        end
    catch err %#ok<NASGU>
        % Shouldn't error, but if we do, just ignore that computer
        % and move on to the next one.
    end
end

% Keep only the non-empty and unique ones.
dnsNames = dnsNames(~cellfun(@isempty, dnsNames));
dnsNames = unique(lower(dnsNames));
end

%----------------------------------------------
function [connection, newConnectionMade] = iGetServerConnection(host, clusterVersion)
import parallel.internal.types.HPCServerClusterVersion
validateattributes(host, {'char'}, {'row'});
validateattributes(clusterVersion, {'parallel.internal.types.HPCServerClusterVersion'}, {'scalar'})

% A map with keys whose names are the cluster version strings.  Each
% key's value is a map whose keys are hostnames and whose values are
% AbstractMicrosoftSchedulerConnections
persistent serverConnectionMap

if isempty(serverConnectionMap)
    serverConnectionMap = containers.Map();
    [~, clusterVersionStrings] = enumeration('parallel.internal.types.HPCServerClusterVersion');
    % Create the maps
    for ii = 1:numel(clusterVersionStrings)
        serverConnectionMap(clusterVersionStrings{ii}) = containers.Map();
    end
end

newConnectionMade = false;

try
    connectionMap = serverConnectionMap(clusterVersion.Name);
    % See if we have a scheduler connection in the Map for the desired name
    upperHostname = upper(host);
    if connectionMap.isKey(upperHostname)
        connection = connectionMap(upperHostname);
    else
        % create a new SchedulerConnection, connect it and add it
        % to the map.
        connection = iCreateServerConnection(clusterVersion);
        connection.connect(host);
        newConnectionMade = true;
        connectionMap(upperHostname) = connection; %#ok<NASGU>
    end
catch err
    ex = MException(message('parallel:cluster:HPCServerUtilsUnableToContactHeadNode', clusterVersion.Name, host));
    ex = ex.addCause(err);
    throw(ex);
end
end

%----------------------------------------------
function connection = iCreateServerConnection(clusterVersion)
import parallel.internal.types.HPCServerClusterVersion
validateattributes(clusterVersion, {'parallel.internal.types.HPCServerClusterVersion'}, {'scalar'})

if clusterVersion == HPCServerClusterVersion.CCS
    connection = distcomp.CCSSchedulerConnection;
elseif clusterVersion == HPCServerClusterVersion.HPCServer2008
    connection = distcomp.HPCServerSchedulerConnection;
else
    assert(false);
end
end

%----------------------------------------------
function [api2Props, vals] = iReadPropertyFile(filename)
import parallel.internal.customattr.PropSet

validateattributes(filename, {'char'}, {'row'});
if ~exist(filename, 'file')
    error(message('parallel:cluster:HPServerUtilsFailedToFindPropertiesFile', filename));
end

fid = fopen(filename, 'r');
if fid < 0
    error(message('parallel:cluster:HPServerUtilsFailedToOpenPropertiesFile', filename));
end
closeFile = onCleanup(@() fclose(fid));

% The file uses '#' for comments and has the format:
% <PROPERTY>=<VALUE>
% The values may contain spaces.
propsAndVals = textscan(fid, '%s%s', 'delimiter', '=', 'commentstyle', '#');
if length(propsAndVals{1}) ~= length(propsAndVals{2})
    error(message('parallel:cluster:HPCServerUtilsInvalidPropertyFile', filename));
end

propsInFile = propsAndVals{1};
valsInFile = propsAndVals{2};

% Strip off leading and trailing whitespace from the props and values
propsInFile = strtrim(propsInFile);
valsInFile = strtrim(valsInFile);

% Convert the properties to API2 properties and values 
[api2Props, vals] = iConvertPropsInPropertyFile(propsInFile, valsInFile);
% Make sure only 1 value exists for each prop
pvPairs = PropSet.namesValuesToPv(api2Props, vals);
[api2Props, vals] = PropSet.amalgamate({}, pvPairs{:});
end

%----------------------------------------------
function [api2Props, vals] = iConvertPropsInPropertyFile(propsInFile, valsInFile)
validateattributes(propsInFile, {'cell'}, {'numel', numel(valsInFile)});
validateattributes(valsInFile, {'cell'}, {});

persistent propConverter
if isempty(propConverter)
    noop = @(x) x;
    propConverter = { ...
        % prop name in file,             API2 prop name,        value conversion Fcn
        'CLUSTER_MATLAB_ROOT',           'ClusterMatlabRoot',   noop; ...
        'NUMBER_MATLAB_WORKER_LICENSES', 'NumWorkers',          @str2double;...
        'JOB_TEMPLATE',                  'JobTemplate',         noop;...
        'DATA_LOCATION_ROOT',            'JobStorageLocation',  @iGetDataLocationFromRoot;...
        'USE_SOA_JOB_SUBMISSION',        'UseSOAJobSubmission', @iStringToLogical; ...
        'CLUSTER_NAME',                  'Name',                noop; ...
        'USE_MATHWORKS_HOSTED_LICENSING','RequiresMathWorksHostedLicensing', @iStringToLogical; ...
        };
end    

api2Props = cell(size(propsInFile));
vals = cell(size(propsInFile));
failedToConvertIdx = false(size(propsInFile));
for ii = 1:numel(propsInFile)
    propIdx = strcmp(propConverter(:, 1), propsInFile{ii});
    api2Props{ii} = propConverter{propIdx, 2};
    convertFcn = propConverter{propIdx, 3};
    try
        vals{ii} = convertFcn(valsInFile{ii});
    catch err %#ok<NASGU>
        failedToConvertIdx(ii) = true;
    end
end

% Warn and remove any props that we failed to convert
if any(failedToConvertIdx)
    failedToConvertProps = propsInFile(failedToConvertIdx);
    warning(message('parallel:cluster:HPCServerUtilsFailedToConvertValuesInPropertyFile', strtrim( sprintf( '%s ', failedToConvertProps{ : } ) )));
    api2Props(failedToConvertIdx) = [];
    vals(failedToConvertIdx) = [];
end
end

%---------------------------------------------------------
function dataLocation = iGetDataLocationFromRoot(dataLocationRoot)
import parallel.internal.apishared.HPCServerUtils
if isempty(dataLocationRoot);
    dataLocation = dataLocationRoot;
    return;
end

if ~exist(dataLocationRoot, 'dir')
    error(message('parallel:cluster:HPCServerUtilsFailedToFindDataLocationRoot', dataLocationRoot));
end

% The username is appended to the dataLocationRoot to give the JobStorageLocation
[~, username] = HPCServerUtils.getUsername();
dataLocation = fullfile(dataLocationRoot, username);
% Create the user's data location if necessary.
if exist(dataLocation, 'dir') ~= 7
    [~, msg, messageID] = mkdir(dataLocationRoot, username);
    
    if exist(dataLocation, 'dir') ~= 7
        mkdirError = MException(messageID, '%s', msg);
        err = MException(message('parallel:cluster:HPCServerUtilsFailedToCreateDataLocation', dataLocation));
        err = err.addCause(mkdirError);
        throw(err);
    end
end
end

%---------------------------------------------------------
function [names, vals, filename] = iGetPropsFromClusterEnvironment(serverConnection)
% Get the MDCS_PROPERTY_FILE_<version> cluster environment variable
% from the cluster.  If it is not empty, then read the file that it
% is pointing to and set the cluster properties appropriately

persistent propMap
if isempty(propMap)
    propMap = containers.Map();
end

% Use the SchedulerHostname as keys into the map.  This is OK because
% a single host will never run more than 1 version of HPC Server and 
% have more than 1 set of cluster environment variables, even though 
% we may use different scheduler version connections to access that 
% host.
upperHostname = upper(serverConnection.SchedulerHostname);
if ~propMap.isKey(upperHostname)
    pctVersion = char(com.mathworks.toolbox.distcomp.util.Version.VERSION_STRING);
    propertyFileVariable = sprintf('MDCS_PROPERTY_FILE_%s', pctVersion);
    propertyFilename = serverConnection.getClusterEnvironmentVariable(propertyFileVariable);
    names = {};
    vals = {};
    if ~isempty(propertyFilename)
        try
            [names, vals] = iReadPropertyFile(propertyFilename);
        catch err
            warning(message('parallel:cluster:HPCServerUtilsFailedToReadPropertiesFile', propertyFilename, err.message));
        end
    end
    propMap(upperHostname) = {names, vals, propertyFilename};
end

namesAndVals = propMap(upperHostname);
names        = namesAndVals{1};
vals         = namesAndVals{2};
filename     = namesAndVals{3};

iEnsureJobStorageLocationExists(names, vals);
end

%---------------------------------------------------------
function val = iStringToLogical(str)
val = logical(str2double(str));
end

%---------------------------------------------------------
function iEnsureJobStorageLocationExists(names, vals)
% If JobStorageLocation is specified in the cluster environment, then
% ensure that it actually exists.

jobStorageLocationIdx = strcmp(names, 'JobStorageLocation');
if ~any(jobStorageLocationIdx)
    % JobStorageLocation not defined
    return;
end

location = vals{jobStorageLocationIdx};
if exist(location, 'dir') == 7
    % JobStorageLocation already exists
    return;
end

s = warning('off', 'backtrace');
warning(message('parallel:cluster:HPCServerUtilsRecreateJobStorageLocation', location));
warning(s);

[~, msg, messageID] = mkdir(location);
if exist(location, 'dir') ~= 7
    mkdirError = MException(messageID, '%s', msg);
    err = MException(message('parallel:cluster:HPCServerUtilsFailedToCreateDataLocation', location));
    err = err.addCause(mkdirError);
    throw(err);
end
end

