% LsfUtils - a collection of utilities for LSF schedulers/clusters.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) LsfUtils

    methods ( Hidden, Static )
    
        function aHandler = messageHandler( jobID, taskID, initialMessageNumber )
        %messageHandler Return a new LSF message handler
            if nargin < 3
                initialMessageNumber = 0;
            end

            if isempty( taskID )
                % Parallel execution
                initialMessageNumber = labindex-1;
                messageIncrement = numlabs;
                jobString = jobID;
            else
                messageIncrement = 1;
                jobString = sprintf('%s[%s]', jobID, taskID);
            end
            messageNumber = initialMessageNumber;
            aHandler = @nMessageHandler;
            if ispc
                quote = '"';
            else
                quote = '''';
            end

            function nMessageHandler(stringToPost)
            % Display the message so that it ends up in the debug log
                disp( stringToPost );
                % Strip characters that would cause problems with the command line - 
                % any quotes or newlines
                stringToPost = regexprep( stringToPost, '[''"\n\r]', '' );
                dctSystem(sprintf('bpost -i %d -d %s%s%s "%s"', messageNumber, quote, stringToPost, quote, jobString));
                messageNumber = messageNumber + messageIncrement;
            end
        end
        
        function environmentSetup()
        % Some environment variables will cause our LSF integration to
        % fail. These must be unset rather than set to empty before we can
        % proceed with job submission
            pctunsetenv('BSUB_QUIET');
        end

        function [masterName, clusterName] = getMasterAndClusterNames()
        % Better check that there actually is an lsf scheduler available - call the
        % LSF lsid command to get information about my cluster
            [FAILED, out] = dctSystem('lsid');
            % Possible that lsid isn't a file I can execute
            if FAILED
                if ispc
                    % Let's try and detect the 'not found' error which indicates that LSF
                    % isn't on the path - need to search for special strings in the system
                    % output to pick up these situations
                    searchString = 'is not recognized as an internal or external command';
                    NOT_FOUND = ~isempty(regexp(out, searchString, 'once'));
                else
                    % Not sure what shell we are using so try calling which in /bin/sh
                    % to see what it thinks of the output
                    [FOUND, whichOut] = dctSystem('/bin/sh -c "which lsid"');
                    switch lower(computer)
                      case {'glnx86', 'glnxa64'}
                        % On linux the exit code of which is sufficient to indicate it
                        % the command lsid exists or not
                        NOT_FOUND = FOUND ~= 0;
                      otherwise % sol2, mac, hpux?
                                % BSD-like platforms however don't have the same which and we
                                % need to parse the output of which for the string 'no lsid in'
                        NOT_FOUND = ~isempty(regexpi(whichOut, 'no lsid in'));
                    end
                end
                if NOT_FOUND
                    error(message('parallel:cluster:LsfUnableToFindService'));
                else
                    error(message('parallel:cluster:LsfErrorRunningLsid', out));
                end
            end

            allEndIndex = regexp(out, '\n') - 1;
            % Lets try and parse the output from out to get the relevant information
            clusterStr = 'My cluster name is ';
            masterStr  = 'My master name is ';

            startClusterIndex = regexp(out, clusterStr) + numel(clusterStr);
            endClusterIndex   = min(allEndIndex(allEndIndex > startClusterIndex));

            startMasterIndex  = regexp(out, masterStr) + numel(masterStr);
            endMasterIndex    = min(allEndIndex(allEndIndex > startMasterIndex));

            clusterName = out(startClusterIndex:endClusterIndex);
            masterName  = out(startMasterIndex:endMasterIndex);
        end

        function string = makeTaskIdString( taskIds )
        % makeTaskIdString - given a list of Task Ids, return the string needed
        % by LSF during job submission.
            validateattributes( taskIds, {'double'}, {} );

            % Ensure taskIds is a row before we continue
            taskIds = taskIds(:).';

            [IDs, index] = sort(taskIds);
            numTasks = numel(taskIds);

            % Check that the IDs were originally in a monotonic order
            if ~isequal(index, 1:numel(index))
                % Should never get here
                error(message('parallel:cluster:LsfInvalidState'));
            end

            % The format of the string is 1-N1,N2,N3-N4,N5,N6 ...

            % Deduce the difference between subsequent ID's
            deltaIDs = diff(IDs);
            % Define a cell array to hold the output - starts of ranges will be the
            % first in a series of diffs of 1.
            out = cell(numTasks, 1);
            % Ensure we start with a range and not in a range
            lastDiff = 2;
            % Loop over each diff
            for i = 1:numel(deltaIDs)
                % Get this ID and the difference to the next
                thisID = IDs(i);
                thisDiff = deltaIDs(i);
                if thisDiff > 1
                    % If the diff to the next is more than 1 then we are either at the
                    % end of a range or on a singleton number, so simply print the
                    % number followed by a comma
                    out{i} = sprintf('%d,', thisID);
                elseif thisDiff == 1
                    % If the diff is one is this the start of a range or are we in a
                    % range. If the last wasn't 1 then this is the start of a range
                    if lastDiff > 1
                        out{i} = sprintf('%d-', thisID);
                    end
                else
                    % Should never get here
                    error(message('parallel:cluster:LsfDuplicateTaskIds'));
                end
                % Remember the last difference
                lastDiff = thisDiff;
            end
            % Finally always add the last number, irrespective of what has happened
            out{numTasks} = sprintf('%d', IDs(numTasks));
            % Now remove all the empty cells
            out(cellfun('isempty', out)) = [];
            % And concatenate the whole lot together.
            string = strcat(out{:});
        end

        function OK = isAcceptableBkillError( bkillString )
            OK = false;
            % Job already finished is OK because we might be destroying or
            % cancelling a job that has finished
            OK = OK || ~isempty(regexp(bkillString, ': Job has already finished', 'once'));
            % After a time LSF forgets about jobs - we don't. Thus LSF might be out
            % of date with us.
            OK = OK || ~isempty(regexp(bkillString, ': No matching job found', 'once'));
            % LSF seems to incorrectly indicate an error when this is returned
            OK = OK || ~isempty(regexp(bkillString, ': Operation is in progress', 'once'));
        end

        function [FAILED, numPendRunFail, failedLsfIndex] = getStateInformation( lsfJobId )
            numPendRunFail = [0 0 0];
            failedLsfIndex = [];

            % Ask LSF about this job
            [FAILED, out] = dctSystem(sprintf('bjobs %d', lsfJobId));
            if FAILED
                warning(message('parallel:cluster:LsfBjobsFailed', out));
                return
            end

            % How many PEND, PSUSP, USUSP, SSUSP, WAIT
            numPendRunFail(1) = numel(regexp(out, 'PEND|PSUSP|USUSP|SSUSP|WAIT'));
            % How many RUN strings - UNKWN started running and then comms was lost
            % with the sbatchd process.
            numPendRunFail(2) = numel(regexp(out, 'RUN|UNKWN'));
            % How many DONE, EXIT, ZOMBI strings
            numPendRunFail(3) = numel(regexp(out, 'EXIT|ZOMBI'));

            if numPendRunFail(3) > 0
                % Get the LSF task ID of the failed tasks so we can set
                % those tasks to failed.
                failedLsfIndex = iParseBjobsOutputForExit(out, numPendRunFail(3));
            end
        end

        function lsfID = extractLsfJobId( out )
        % Extract the LSF job ID as a numeric value.
            jobNumberStr = regexp(out, 'Job <[0-9]*>', 'once', 'match');
            if isempty( jobNumberStr )
                error(message('parallel:cluster:LsfFailedToParseJobId', out));
            end
            lsfID = sscanf(jobNumberStr(6:end-1), '%d');
        end

        function [filesToCopy, remoteTaskLoc, logRelToStorage] = ...
                calcFilesToCopy( storageLocation, jobLocation )
            % TODO:later reconcile with JobFilesInfo
            localJobLoc   = [storageLocation filesep jobLocation];
            remoteJobLoc  = ['%J.mdce/' jobLocation];
            localTaskLoc  = [localJobLoc filesep 'Task%I'];
            remoteTaskLoc = [remoteJobLoc '/Task%I'];
            logRelToStorage = [jobLocation, '/Task%I.log'];
            % And create the correct file transfer commands - we are going to copy
            % the Job zip file and the Task zip file to the relevant host - they
            % will be named LSB_JOBID.Job.zip and LSB_JOBID.LSB_JOBINDEX.Task.zip.
            % The far end will then unzip them into a directory called
            % LSB_JOBID.mdce. We will then need to
            filesToCopy = {...
                ['-f "' localJobLoc '.zip > %J.Job.zip"'] ...
                ['-f "' localJobLoc filesep 'Task%I.zip > %J.%I.Task.zip"'] ...
                ['-f "' localJobLoc  '.common.mat  < ' remoteJobLoc  '.common.mat"'] ...
                ['-f "' localJobLoc  '.out.mat     < ' remoteJobLoc  '.out.mat"'] ...
                ['-f "' localJobLoc  '.state.mat   < ' remoteJobLoc  '.state.mat"'] ...
                ['-f "' localTaskLoc '.common.mat  < ' remoteTaskLoc '.common.mat"'] ...
                ['-f "' localTaskLoc '.out.mat     < ' remoteTaskLoc '.out.mat"'] ...
                ['-f "' localTaskLoc '.state.mat   < ' remoteTaskLoc '.state.mat"'] ...
                ['-f "' localTaskLoc '.log         < ' remoteTaskLoc '.log"'] ...
                ['-f "' localTaskLoc '.diary.txt   < ' remoteTaskLoc '.diary.txt"'] ...
                          };
            filesToCopy = sprintf('%s ', filesToCopy{:});
        end

        function scriptOnCluster = ...
                copyWrapperScript( wrapper, clusterOsType, dlStruct, jobLocation )
            % Deal with the copying the wrapper script into the job subdirectory.

            import parallel.internal.apishared.FilenameUtils

            if exist( wrapper, 'file' ) ~= 2
                error(message('parallel:cluster:LsfNoWrapperScript', wrapper));
            end

            [~, shortPart, ext] = fileparts( wrapper );

            [clientWrapper, scriptOnCluster] = FilenameUtils.jobSpecificFilename( ...
                clusterOsType, dlStruct, jobLocation, [shortPart, ext], false );

            [success, msg] = copyfile( wrapper, clientWrapper );
            if ~success
                error(message('parallel:cluster:LsfCopyWrapperFailed', wrapper, clientWrapper, msg));
            end

            % We are actually testing the client here - whereas it's really the worker
            % OS type that matters. But, PC workers don't care about the execute
            % bit, and windows clients default to copying files with "+x" set for
            % UNIX workers.
            if isunix
                [success, msg] = fileattrib( clientWrapper, '+x' );
                if ~success
                    error(message('parallel:cluster:LsfChmodWrapperFailed', clientWrapper, msg));
                end
            end
        end

        function lsfID = callBsub( cmdLine )

            import parallel.internal.apishared.LsfUtils

            storedEnv = distcomp.pClearEnvironmentBeforeSubmission();
            cleanup = onCleanup( @() distcomp.pRestoreEnvironmentAfterSubmission( storedEnv ) );

            [FAILED, out] = dctSystem(cmdLine);
            if FAILED
                error(message('parallel:cluster:LsfBsubInvocationFailed', out));
            end
            lsfID = LsfUtils.extractLsfJobId( out );
        end
        function callBresume( lsfID, optTaskIdString )
        % Now set the job actually running by moving it from the PSUSP state to the
        % PEND state
            if nargin == 1
                arg = sprintf( '%d', lsfID );
            else
                arg = sprintf( '%d[%s]', lsfID, optTaskIdString );
            end

            [FAILED, out] = dctSystem(sprintf('bresume "%s"', arg));
            if FAILED
                error(message('parallel:cluster:LsfBresumeInvocationFailed', out));
            end

        end
    end
end

function iFailedLsfIndex = iParseBjobsOutputForExit(out, numFailed)
% We are expecting out to look something like this
%
% JOBID   USER    STAT  QUEUE      FROM_HOST   EXEC_HOST   JOB_NAME   SUBMIT_TIME
% 7005    jlmarti EXIT  normal     uk-martinj-    -        Job2[1]    Jul 26 11:16
% 7005    jlmarti EXIT  normal     uk-martinj-    -        Job2[2]    Jul 26 11:16
% 7005    jlmarti EXIT  normal     uk-martinj-    -        Job2[3]    Jul 26 11:16
% 7005    jlmarti EXIT  normal     uk-martinj-    -        Job2[4]    Jul 26 11:16

% Convert the output to a cell array of lines
    lout = regexp(out, '.*?\n', 'match');
    % Remove the header line
    lout(1) = [];
    % Loop over the lines looking for EXIT or ZOMBI
    iFailedLsfIndex = zeros(1, numFailed);
    iFailedCount = 1;
    for i = 1:numel(lout)
        thisLine = lout{i};
        if ~isempty(regexp(thisLine, 'EXIT|ZOMBI', 'once'))
            % Split into whitespace delimited cells
            thisLineCell = regexp(thisLine, '[^\s]+', 'match');
            jobIndexCellStr = {};
            % Need to check that thisLineCell{7} has [] with a number inside
            if numel(thisLineCell) >= 7
                jobIndexCellStr = regexp(thisLineCell{7}, '\[([0-9]+)\]', 'tokens', 'once');
            end
            % Did our first attempt to get the jobIndexCellStr work?
            if isempty(jobIndexCellStr)
                jobIndexCellStr = regexp(thisLine, '\[([0-9]+)\]', 'tokens', 'once');
            end
            try
                iFailedLsfIndex(iFailedCount) = str2double(jobIndexCellStr{1});
                iFailedCount = iFailedCount + 1;
            catch err %#ok<NASGU>
                      % Remove this one from the possible list since we failed to get
                      % an LSF job array index
                iFailedLsfIndex(iFailedCount) = [];
            end
        end
    end
end
