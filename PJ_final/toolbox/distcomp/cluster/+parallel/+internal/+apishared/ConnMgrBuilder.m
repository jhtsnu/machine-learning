% ConnMgrBuilder - builds instances of
% com.mathworks.toolbox.distcomp.pmode.io.ConnectionManager

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) ConnMgrBuilder

    methods ( Static, Access = private )
        function [portrange, useEphemeral, constants, hostname] = commonSetup()
        % commonSetup() - calculate common properties needed by both methods

            cfg          = pctconfig();
            portrange    = cfg.portrange;
            % portrange will be either [minPort, maxPort] or 0
            useEphemeral = isscalar(portrange);
            constants    = parallel.internal.pool.Constants();
            hostname     = cfg.hostname;
        end
    end
    methods ( Static )

        function connMgr = buildForMJS( brokerConnectInfo, ...
                                        workerAcceptInfoTemplate, ...
                                        useSecureMatlabPool )
        % buildForMJS() - build a ConnectionManager to be used by jobmanager/MJS.
        % Input arguments must be extracted from the jobmanager/MJS object
        % prior to invoking this method.
            import com.mathworks.toolbox.distcomp.pmode.poolmessaging.ConnectionManager;
            import parallel.internal.apishared.ConnMgrBuilder;

            %todo maybe move the useEphemeral logic into commonSetup()
            [portrange, useEphemeral, constants, hostname] = ConnMgrBuilder.commonSetup();

            if useEphemeral
                minport = 0;
                maxport = 0;
            else    
                minport = portrange(1);
                maxport = portrange(2);
            end
            
            clientPeerInstance = ConnectionManager.createClientPeerInstance();
            acceptInfo = ConnectionManager.createAcceptInfo( ...
                    clientPeerInstance, minport, maxport, constants.connectionBacklog, ...
                    brokerConnectInfo, workerAcceptInfoTemplate, ...
                    useSecureMatlabPool, ...
                    constants.matlabpoolPollInterval, ...
                    constants.handshakeTimeout, ...
                    constants.handshakeConnectAttempts);
            
            connMgr = ConnectionManager.buildClientConnManager( ...
                    hostname, clientPeerInstance, acceptInfo);
        end

        function connMgr = buildForCJS()
        % buildForCJS() - build a ConnectionManager for CJS-type clusters/schedulers.
            import com.mathworks.toolbox.distcomp.pmode.poolmessaging.ConnectionManager;
            import parallel.internal.apishared.ConnMgrBuilder;

            [portrange, useEphemeral, constants, hostname] = ConnMgrBuilder.commonSetup();

            if useEphemeral
                minport = 0;
                maxport = 0;
            else    
                minport = portrange(1);
                maxport = portrange(2);
            end

            useSecureMatlabPool = false;
            
            clientPeerInstance = ConnectionManager.createClientPeerInstance();
            acceptInfo = ConnectionManager.createAcceptInfo( ...
                    clientPeerInstance, minport, maxport, constants.connectionBacklog, ...
                    useSecureMatlabPool,...
                    constants.handshakeTimeout, ...
                    constants.handshakeConnectAttempts);
            connMgr = ConnectionManager.buildClientConnManager( ...
                    hostname, clientPeerInstance, acceptInfo);
        end
    end
end
