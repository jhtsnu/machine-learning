% WorkerCommand - a collection of utilities for computing the worker command to
% run on a cluster in various situations.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) WorkerCommand

    methods ( Static, Access = private )
        function mlcmd = prependMatlabRoot( mlcmd, clusterMLRoot, osType )
        % prependMatlabRoot if specified.
            import parallel.internal.apishared.FilenameUtils;
            if ~isempty( clusterMLRoot )
                mlcmd = fullfile( clusterMLRoot, 'bin', mlcmd );
                mlcmd = FilenameUtils.fixSlashes( mlcmd, osType );
            end
        end

        function cmd = getCtfxLauncherPath()
        % Get the path (relative to ClusterMatlabRoot) to ctfxlauncher
            if ispc
                cmd = fullfile( dct_arch, 'ctfxlauncher.exe' );
            elseif ismac
                cmd = fullfile( dct_arch, 'ctfx_starter.app', 'Contents', 'MacOS', 'ctfxlauncher' );
            else
                cmd = fullfile( dct_arch, 'ctfxlauncher' );
            end
        end

        function cmd = getMatlabPathRelativeToRoot()
        % Get the path (relative to ClusterMatlabRoot/bin) to MATLAB executable
            if ispc
                cmd = fullfile( dct_arch, 'MATLAB.exe' );
            else
                cmd = fullfile( dct_arch, 'MATLAB' );
            end
        end

    end

    methods ( Static )
        function [worker, mlcmd, args] = defaultCommand( clusterOsTypeStr, clusterMLRoot, jobIsCommunicating )
        % defaultCommand - return the default worker command to run for most schedulers
        % - clusterOsTypeStr: defines the clusterOsType
        % - clusterMLRoot: is the MATLABROOT on the cluster, or '' if workers are on the path
        % - jobIsCommunicating: is a logical scalar defining whether the workers are launched '-parallel'
        % returns:
        % - worker: the "old-style" single string
        % - mlcmd: just the MATLAB/worker portion as a single string
        % - args: space-separated arguments for mlcmd

            import parallel.internal.apishared.OsType;
            import parallel.internal.apishared.WorkerCommand;

            clusterOsType = OsType.fromName( clusterOsTypeStr ); % validates
            validateattributes( clusterMLRoot, {'char'}, {} );
            validateattributes( jobIsCommunicating, {'logical'}, {'scalar'} );

            if clusterOsType == OsType.MIXED && ~isempty( clusterMLRoot )
                error(message('parallel:cluster:MixedOsClusterMatlabRoot'));
            end

            if clusterOsType == OsType.PC
                mlcmd = 'worker.bat';
            else
                mlcmd = 'worker';
            end
            if jobIsCommunicating
                args = ' -parallel';
            else
                args = '';
            end

            % We can now create the old-style MatlabCommandToRun
            worker = [ mlcmd, args ];

            % Finally, if necessary, prepend the ClusterMatlabRoot
            mlcmd = WorkerCommand.prependMatlabRoot( mlcmd, clusterMLRoot, clusterOsType );
        end

        function [mlcmd, argsCell] = localCommand( clusterMLRoot, needLicenseArg )
        % localCommand - the command to be used by the local scheduler
        % - clusterMLRoot is the MATLABROOT to be used
        % - needLicenseArg true if the "-c" argument should be supplied. Ignored if deployed.
        % returns:
        % - mlcmd: the full path to the worker process
        % - argsCell: cellstr of arguments

            import parallel.internal.apishared.OsType;
            import parallel.internal.apishared.WorkerCommand;

            % This is the point at which we inject the knowledge that workers
            % are not allowed to submit jobs to a local scheduler - this is a
            % single point through which all API-1 and API-2 local submissions
            % must pass.
            if system_dependent( 'isdmlworker' )
                % Can't start local workers from a worker
                error( message( 'parallel:cluster:NoLocalSubmissionsFromWorkers' ) );
            end

            validateattributes( clusterMLRoot, {'char'}, {} );

            % This is the function the worker will run
            worker_function = 'distcomp_evaluate_filetask';

            % The workers are always headless, but require a different
            % argument on Windows to Linux and Mac.
            if ispc
                headless = '-noFigureWindows';
            else
                headless = '-nodisplay';
            end

            % If running under the LXE, then workers must too.
            if feature('islxe') 
                lxe = '-lxe';
            else 
                lxe = '';
            end

            % Set the prototypical mlcmd - later we'll prepend ClusterMatlabRoot if set
            if isdeployed
                % If we are deployed then we start workers using ctfxlauncher
                mlcmd = WorkerCommand.getCtfxLauncherPath();
                % Need to get some information from the current application
                % to pass on to the worker process. Assert that these have been set in the
                % current deployed application so that we don't get a errors when
                % starting the worker.
                componentID = getmcruserdata( 'AppComponentUUID' );
                assert( ~isempty( componentID ), 'AppComponentUUID should not be empty' );
                useComponentCache = getmcruserdata( 'UseComponentCache' );
                assert( ~isempty( useComponentCache ), 'UseComponentCache should not be empty'  );
                % Pass these arguments to ctfxlauncher.
                argsCell = {...
                    '-dmlworker', headless, lxe, '-r', worker_function, ...
                    '-ctfroot', ctfroot(), '-compid', componentID, '-usecache', num2str( useComponentCache )};
            else
                % In a normal MATLAB we can simply launch another MATLAB.
                mlcmd = WorkerCommand.getMatlabPathRelativeToRoot();
                argsCell = {...
                    '-dmlworker', headless, lxe, '-r', worker_function };

                % Some situations require the license search path appending here, but
                % this only really makes sense if we are using traditional license
                % files.  In theory, the -licmode arg overrides the -c flag, but we
                % still won't define both.
                isLicensedOnline = parallel.internal.cluster.getOnlineLicenseInfo();
                if isLicensedOnline
                    argsCell = [ argsCell, '-licmode', 'online'];
                elseif needLicenseArg
                    argsCell = [ argsCell, '-c', feature( 'lmsearchpath' ) ];
                end
            end

            mlcmd = WorkerCommand.prependMatlabRoot( mlcmd, clusterMLRoot, OsType.forClient() );
        end

        function [worker, mlcmd, args] = hpcServerCommand( clusterOsTypeStr, clusterMLRoot, useSOA, jobIsCommunicating )
        % ccsCommand - the command to be used by CCS/HPCServer clusters
        % - clusterOsTypeStr: must be 'pc'/'windows' (old code used to check...)
        % - clusterMLRoot: MATLABROOT to use
        % - useSOA: flag whether to use SOA submission
        % - jobIsCommunicating: is this a parallel job

            import parallel.internal.apishared.OsType;
            import parallel.internal.apishared.WorkerCommand;

            clusterOsType = OsType.fromName( clusterOsTypeStr );
            validateattributes( clusterMLRoot, {'char'}, {} );
            validateattributes( useSOA, {'logical'}, {'scalar'} );
            validateattributes( jobIsCommunicating, {'logical'}, {'scalar'} );

            if clusterOsType ~= OsType.PC
                error(message('parallel:cluster:HPCServerIncompatibleSetting'));
            end

            % Set the prototypical mlcmd - later we'll prepend ClusterMatlabRoot if set
            mlcmd = 'worker.bat';

            % Choose the args for mlcmd
            if jobIsCommunicating
                args = ' -parallel';
            else
                % this is a distributed job, so check for SOA
                if useSOA
                    % The C# code in MathWorks.MdcsService.MLWorkerWrapper will replace
                    % this channel name with something sensible
                    ipcChannelName = '%PCTIPC_CHANNEL_NAME%';
                    args = sprintf(' -ipc %s', ipcChannelName);
                else
                    args = '';
                end
            end

            % We can now create the old-style MatlabCommandToRun
            worker = [ mlcmd, args ];

            mlcmd = WorkerCommand.prependMatlabRoot( mlcmd, clusterMLRoot, clusterOsType );
        end
    end

end
