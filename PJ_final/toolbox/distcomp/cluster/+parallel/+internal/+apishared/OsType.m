% OsType enumeration - handles interpreting strings and returning client type.

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden, Sealed ) OsType
    enumeration
        PC    ('pc',    'windows')
        UNIX  ('unix',  'unix')
        MIXED ('mixed', 'mixed')
    end
    
    properties ( SetAccess = immutable )
        Api1Name
        Api2Name
    end
    
    methods
        function obj = OsType( api1Name, api2Name)
            obj.Api1Name = api1Name;
            obj.Api2Name = api2Name;
        end
    end
    
    methods ( Static )
        function e = fromName( name )
        % fromName - return an enumerated value from the given name
        % name must be one of {'pc', 'windows', 'unix', 'mixed'}
            import parallel.internal.apishared.OsType;

            name = validatestring( name, {'pc', 'windows', 'unix', 'mixed'}, ...
                                   'OsType.fromName', 'name', 1 );
            switch name
              case { OsType.PC.Api1Name,    OsType.PC.Api2Name }
                e = OsType.PC;
              case { OsType.UNIX.Api1Name,  OsType.UNIX.Api2Name }
                e = OsType.UNIX;
              case { OsType.MIXED.Api1Name, OsType.MIXED.Api2Name }
                e = OsType.MIXED;
            end
        end
        function e = forClient()
        % forClient - return the OsType for the client
            import parallel.internal.apishared.OsType;

            if ispc
                e = OsType.PC;
            else
                e = OsType.UNIX;
            end
        end
    end
end
