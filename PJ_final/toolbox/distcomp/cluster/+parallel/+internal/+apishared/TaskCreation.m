% TaskCreation - helpers for interpreting arguments when creating tasks in

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden, Sealed ) TaskCreation

    methods ( Static, Access = private )

        function assertScalarJob( njobs )
        % Throw an error if the job is not a scalar
            if njobs ~= 1
                error(message('parallel:job:ScalarJobRequiredForCreateTask'));
            end
        end

        function assertArgsInCell( argsIn )
        % Throw an error if input args not a cell
            if ~iscell(argsIn)
                error(message('parallel:job:ArgsInMustBeCell'));
            end
        end

        function argsIn = checkArgsIn( argsIn, isVectorized )
        % check and rationalize input arguments depending on whether we're
        % vectorized.
            ok = true;
            if isVectorized
                if ~all(cellfun(@isvector, argsIn(:)) | cellfun(@isempty, argsIn(:)))
                    ok = false;
                end
            else
                if ~(isvector(argsIn) || isempty(argsIn))
                    ok = false;
                end
                % Convert to a 1 x 1 cell array
                argsIn = {argsIn};
            end
            if ~ok
                error(message('parallel:job:CreateTaskInputArgsCells'));
            end
        end

        function taskFcn = checkTaskFcn( taskFcn, argsInSize )
        % Ensure the taskFcn is sized as per argsIn, also check contents of
        % taskFcn

            if ~iscell(taskFcn)
                % Make task function the same size as the argsIn
                taskFcn = repmat({taskFcn}, argsInSize);
            else
                % Need to check that task function is of the correct size
                if ~isequal(size(taskFcn), argsInSize)
                    error(message('parallel:job:CreateTaskFunctionBadSize'));
                end
            end
            % Get class of all input task functions, check that all are valid
            taskClass = cellfun(@class, taskFcn, 'uniformOutput', false);
            if ~all(ismember(taskClass(:), {'function_handle', 'char'}))
                error(message('parallel:job:CreateTaskFunctionConsistency'));
            end
        end

        function numArgsOut = checkNumArgsOut( numArgsOut, argsInSize )
        % Ensure that numArgsOut is sized as per argsIn, also check the
        % contents.
            if ~isnumeric(numArgsOut) || isempty(numArgsOut)
                error(message('parallel:job:CreateTaskInvalidNumOutputs'));
            end
            if numel(numArgsOut) == 1
                % Expand
                numArgsOut = repmat(numArgsOut, argsInSize);
            else
                % Need to check that numArgsOut is of the correct size
                if ~isequal(size(numArgsOut), argsInSize)
                    error(message('parallel:job:CreateTaskNumOutputsBadSize'));
                end
            end
            if any(numArgsOut(:) < 0)
                error(message('parallel:job:CreateTaskNegativeNumOutputArgs'));
            end
        end
    end

    methods ( Static )
        function [taskFcn, numArgsOut, argsIn, setArgs] = ...
                createTaskArgCheck(njobs, taskFcn, numArgsOut, argsIn, varargin)
            % createTaskArgCheck - check all input arguments for createTask() in
            % API1 and API2, rationalize vectorized calls, and return the
            % results. Intention is call site transforms:
            % createTask( j, taskFcn, numArgsOut, argsIn, ... )
            % into
            % createTaskArgCheck( numel(j), taskFcn, numArgsOut, argsIn, ... )
            % so as to avoid passing different classes of job to the apishared
            % layer

            import parallel.internal.apishared.TaskCreation;

            % Check arguments, but don't use standard nargchk so users don't
            % get confused between input/output arguments to createTask and nargin/nargout
            % required by their function handle.
            parallel.internal.cluster.checkNumberOfArguments('input', 3, inf, ...
                                                             nargin, 'createTask');

            if nargin < 4
                argsIn = {};
            end

            % Ensure we haven't been passed a job array
            TaskCreation.assertScalarJob( njobs );

            % First check the input arguments
            TaskCreation.assertArgsInCell( argsIn );

            % Are all the input arguments also cell arrays? If they are then this is a
            % vectorized call and should be treated as such
            IS_VECTORIZED = ~isempty(argsIn) && all(cellfun(@iscell, argsIn(:)));

            % Rationalize argsIn, it will always be a cell after this call:
            argsIn        = TaskCreation.checkArgsIn( argsIn, IS_VECTORIZED );

            % Rationalize taskFcn
            taskFcn       = TaskCreation.checkTaskFcn( taskFcn, size(argsIn) );

            % Rationalize and error check numArgsOut
            numArgsOut    = TaskCreation.checkNumArgsOut( numArgsOut, size(argsIn) );

            % Args to set are simply the remainder.
            setArgs       = varargin;
        end
    end

end
