% EnvVars - helper functions to do with environment variables

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) EnvVars
    methods ( Static )
        function [names, values] = createLocalClusterEnvironment( immutableNames )
        % createLocalClusterEnvironment - build names/values for the Local
        % cluster. These are the names/values that are not automatically
        % included in the worker's environment.

            % Get the names to add from the environment
            env = dct_psfcns('environ');
            % Find everything in the environment from the beginning of the line
            % up to the first = sign
            names  = regexp(env, '^.*?(?==)', 'match', 'once');
            % The values are the rest
            values = regexp(env, '(?<==).*$', 'match', 'once');

            % Ignore any names that we are told are 'immutable' - these are the
            % names that the cluster wishes to enforce.
            [names, idx] = setdiff( names, immutableNames );
            values       = values(idx);

            % Always convert to columns for the java end
            names  = names(:);
            values = values(:);

            % Finally, trim anything that we don't need by comparing current
            % values with those seen by Java.
            javaValueOkPredicate = @(name, value) ...
                isequal( value, char( java.lang.System.getenv( name ) ) );
            keep   = ~ cellfun( javaValueOkPredicate, names, values );
            names  = names( keep );
            values = values( keep );
        end
    end
end
