% ProjectComponentDisplayer - Displayer for Project Components

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) ProjectComponentDisplayer < parallel.internal.settings.NamedNodeDisplayer
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract NamedNodeDisplayer properties
        CommonDisplayProps = {'Name', 'AutoAttachFiles', 'AttachedFiles', 'AdditionalPaths', ...
            'CaptureDiary', 'NumWorkersRange', ...
            'JobFinishedFcn', 'JobQueuedFcn', 'JobRunningFcn', ...
            'TaskFinishedFcn', 'TaskRunningFcn', ...
            'JobTimeout', 'TaskTimeout'};
            
        % TODO:OneDay - what else makes sense in the vector display?  AttachedFiles
        % and AdditionalPaths are probably to lengthy to display in a table
        VectorDisplayProps = {'Name', 'NumWorkersRange'};
        VectorTableColumns = { ...
            % 'Title',         'Resizeable', 'MinimumWidth'
            'Name',            true,         length('Name'); ...
            'NumWorkersRange', false,        length('NumWorkersRange'); ...
        }
        DisplayHelper = parallel.internal.display.DisplayHelper( length('AdditionalPaths') )
    end

    methods % constructor
        function obj = ProjectComponentDisplayer(someObj)
            obj@parallel.internal.settings.NamedNodeDisplayer(someObj, ...
                'parallel.settings.ProjectComponent');
        end
    end
    
    methods (Access = protected) % implementation for abstract NamedNodeDisplayer methods
        function displayProperties(obj, heading, propsToDisp)
            obj.DisplayHelper.displayMainHeading(heading);
            obj.DisplayHelper.displayProperty('AutoAttachFiles', propsToDisp.AutoAttachFiles);
            obj.DisplayHelper.displayProperty('AttachedFiles',   propsToDisp.AttachedFiles);
            obj.DisplayHelper.displayProperty('AdditionalPaths', propsToDisp.AdditionalPaths);
            obj.DisplayHelper.displayProperty('CaptureDiary',    propsToDisp.CaptureDiary);
            
            obj.DisplayHelper.displaySubHeading('Communicating Job and MJS Job Properties')
            obj.DisplayHelper.displayProperty('NumWorkersRange', propsToDisp.NumWorkersRange);

            obj.DisplayHelper.displaySubHeading('MJS Job Properties')
            obj.DisplayHelper.displayProperty('JobTimeout',      propsToDisp.JobTimeout);
            obj.DisplayHelper.displayProperty('JobQueuedFcn',    propsToDisp.JobQueuedFcn);
            obj.DisplayHelper.displayProperty('JobRunningFcn',   propsToDisp.JobRunningFcn);
            obj.DisplayHelper.displayProperty('JobFinishedFcn',  propsToDisp.JobFinishedFcn);

            obj.DisplayHelper.displaySubHeading('MJS Task Properties')
            obj.DisplayHelper.displayProperty('TaskTimeout',     propsToDisp.TaskTimeout);
            obj.DisplayHelper.displayProperty('TaskRunningFcn',  propsToDisp.TaskRunningFcn);
            obj.DisplayHelper.displayProperty('TaskFinishedFcn', propsToDisp.TaskFinishedFcn);
        end
        
        function tableData = getTableData(obj, toDisp, getPropsFcn)
            tableData = cell(numel(toDisp), size(obj.VectorTableColumns, 1));
            for ii = 1:numel(toDisp) 
                currToDisp = toDisp(ii);
                propsToDisp = getPropsFcn(currToDisp, obj.VectorDisplayProps);
                tableData(ii, :) = {propsToDisp.Name, propsToDisp.NumWorkersRange};
            end
        end
    end
end
