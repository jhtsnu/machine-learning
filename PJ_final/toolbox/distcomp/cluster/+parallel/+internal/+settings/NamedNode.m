% NamedNode - A named object that has properties stored in a Settings node 
% whose name is the same as the name of the object.
% Base class for Profiles and Associated Components.
 
%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc public methods and properties
classdef (Hidden) NamedNode < parallel.internal.settings.CustomSettingsGetSet & ...
                              parallel.internal.settings.CustomSettingsPropDisp
    %---------------------------------------------------
    % Normal class properties
    %---------------------------------------------------
    properties (Access = protected) % Implementation of CustomSettingsGetSet Abstract properties
        NodeFacade
    end

    properties (Dependent, AbortSet)
        % The name of the object.
        % Name needs to have AbortSet on so that we don't 
        % change the settings tree unnecessarily.
        Name 
    end

    properties (Access = protected)
        % The listener for changes to the underlying Settings keys
        KeysChangedOnNodeListener
        % The listener for the underlying Settings node being destroyed
        % by someone else (not by us deleting it).  Note that this is 
        % NOT listening to the Settings node's ObjectBeingDestroyed event.
        NodeHasBeenDestroyedListener
    end

    events 
        % NameChanged is fired when the name of the Settings node changes 
        % (i.e. when the NamedNode is moved to a different Settings node).
        % Examples of listeners: NamedNodeCache - to ensure that the 
        % keys in the cache are kept up-to-date with the names 
        % of the Settings node
        NameChanged
        % SettingsNodeDestroyed is fired when the underlying Settings node
        % is destroyed.
        % NB Calling delete() on this NamedNode will not fire
        % the SettingsNodeDestroyed event.
        % Examples of listeners: Profile - to ensure that the 
        % Validation Status for a destroyed profile is removed 
        % from the status cache.
        SettingsNodeDestroyed
        % SettingsNodeChanged is fired when any aspect of the Settings node is 
        % changed:
        %   * when the name of Settings node changes
        %   * when the underlying Settings node is destroyed
        %   * when any of the values of the keys are changed
        %   * when any child nodes/keys are added/removed (TODO:later
        %     waiting for Settings support)
        % NB Calling delete() on this NamedNode will not fire
        % the SettingsNodeChanged event.
        % Examples of listeners: ProfileExpander - to ensure 
        % that the cached PV Pairs from profiles/components 
        % the correct latest values
        SettingsNodeChanged
    end

    methods % Property get/set methods
        function name = get.Name(obj)
            name = obj.NodeFacade.NodeName;
        end
        
        function set.Name(obj, newName)
            if ~isvarname(newName)
                error(message('parallel:settings:NamedNodeNameNotVarName'))
            end
            obj.errorIfInvalidName(newName);
            oldName = obj.Name;
            try
                obj.moveNode(newName);
                obj.notifyNameChanged(oldName, newName);
            catch err
                ex = MException(message('parallel:settings:FailedToChangeName', obj.Name, newName));
                ex = ex.addCause(err);
                throw(ex);
            end
        end
    end
    
    methods (Abstract, Hidden, Static)
        obj = hBuildFromSettings(name, settingsNode, parentSettingsNode, varargin);
    end
    
    methods (Access = protected) % Constructor
        function obj = NamedNode(name, settingsNode, parentSettingsNode) 
            import parallel.internal.settings.CustomSettingsGetSet
            import parallel.internal.settings.SettingsNodeFacade
            
            validateattributes(name, {'char'}, {'row'});
            validateattributes(settingsNode, {'Settings'}, {'scalar'});
            validateattributes(parentSettingsNode, {'Settings'}, {'scalar'});

            obj.NodeFacade = SettingsNodeFacade(settingsNode, name, parentSettingsNode);
            % Keys that are unset in Settings are not persisted when MATLAB is restarted.
            % Therefore, we must ensure all the properties that are stored in Settings 
            % actually exist as keys under this Settings node before we go any further.
            [~, ~, settingsProps, constraints] = obj.hGetPublicSettingsProperties();
            CustomSettingsGetSet.addMissingKeysWithType(obj.NodeFacade, settingsProps, constraints);
            obj.attachListenersToNode();
        end
    end
    
    methods % Destructor
        function delete(objOrObjs, varargin)
        % delete(obj) - delete the current NamedNode.  This removes the Settings node from the 
        % Settings tree.
        % varargin is specified to ensure we get dispatched correctly; but,
        % we don't support any additional arguments other than the magic
        % string 'invalidateOnly' which allows us to invalidate the handle
        % but not destroy the underlying data.
            narginchk(1, 2);

            if nargin == 2
                if ~isequal( varargin{1}, 'invalidateOnly' )
                    error(message('parallel:settings:InvalidDeleteSyntax'));
                end
                fullDestruction = false;
            else
                fullDestruction = true;
            end
            
            
            for ii = 1:length(objOrObjs)
                currObj = objOrObjs(ii);
                if fullDestruction
                    if currObj.NodeFacade.IsValid && currObj.NodeFacade.hasAnyFactoryValues()
                        error(message('parallel:settings:DeleteNodeHasFactoryValues', currObj.Name));
                    end
                    try
                        currObj.NodeFacade.delete();
                    catch err
                        % NB Must error here because a Profile doesn't get deleted
                        % if the Settings node can't be deleted (e.g. because of factory settings)
                        ex = MException(message('parallel:settings:FailedToDeleteSettingsNode', currObj.Name));
                        ex = ex.addCause(err);
                        throw(ex);
                    end
                end
                delete@parallel.internal.settings.CustomSettingsGetSet(currObj);
            end
        end
    end

    methods
        function tf = isequal(obj1, obj2)
        % isequal(obj1, obj2) - NamedNodes are equal if the underlying Settings nodes 
        % themselves are equal
            sizesMatch = isequal(size(obj1), size(obj2));
            classAndSizeMatch = sizesMatch && ...
                isequal(class(obj1), class(obj2)) && ...
                isa(obj1, 'parallel.internal.settings.NamedNode');
            if classAndSizeMatch
                tf = true;
                for ii = 1:numel(obj1)
                    tf = tf && isequal(obj1(ii).NodeFacade, obj2(ii).NodeFacade);
                end
            else 
                tf = false;
            end
        end
        
        function saveAs(obj, newName)
        % saveAs(obj, newName) - save the current NamedNode as a new NamedNode
        % with a different name.  This is like "duplicate" - i.e a Settings node 
        % with the newName will be created, but the current Settings node is not 
        % deleted.  Note that the collapsed values of the current Settings node
        % are copied to the new Settings node
            validateattributes(newName, {'char'}, {'row'});
            if ~isvarname(newName)
                error(message('parallel:settings:NamedNodeNameNotVarName'))
            end
            newNodeFacade = obj.createSiblingNodeForCopy(newName);
            try
                obj.NodeFacade.copyCollapsedValuesToNode(newNodeFacade);
            catch err
                newNodeFacade.delete();
                throw(err);
            end
        end
        
        function export(objOrObjs, filename)
        % export(objOrObjs, filename) - export the collapsed values to the 
        % specified file.
            validateattributes(filename, {'char'}, {'row'}, 'export', 'filename');
            try
                nodeFacadesToExport = [objOrObjs.NodeFacade];
                nodeFacadesToExport.exportNode(filename);
            catch err
                throw(err);
            end
        end
    end
    
    methods (Sealed)
        function saveToLevel(obj, level)
        % saveToLevel(obj, level) - save the user level values to the 
        % specified level.
            import parallel.internal.types.SettingsLevel
            validateattributes(level, {'char'}, {'row'});
            try
                level = SettingsLevel.fromNameI(level);
                obj.NodeFacade.saveUserToLevel(level);
            catch err
                throw(err);
            end
        end
        
        function unsetAll(obj, level)
        % unset(obj, level) - unset all the property values at the 
        % specified level.  If no level is specified, the 
        % values are unset at the user level
            import parallel.internal.types.SettingsLevel
            if nargin < 2
                level = SettingsLevel.User;
            else
                validateattributes(level, {'char'}, {'row'});
                level = SettingsLevel.fromNameI(level);
            end
            try
                obj.NodeFacade.unsetNode(level);
            catch err
                throw(err);
            end
        end
        
        function val = formatDispatcher(obj, displayHelper, valDisplayLength, formatter)
        % Format objects of this type and its subtypes for display
            val = formatter(displayHelper,  obj , valDisplayLength);
        end
    end
    
    methods (Hidden)
        function restorePoint = hGetRestorePoint(obj, level)
        % restorePoint = hGetRestorePoint(obj, level) - get a structure that represents
        % the current state of the node so that it can be used for restoring
        % if necessary.  If no level is supplied, then User is assumed.
            import parallel.internal.types.SettingsLevel
            if nargin < 2
                level = SettingsLevel.User;
            else
                validateattributes(level, {'parallel.internal.types.SettingsLevel'}, {'scalar'});
            end
            
            if ~level.CanReallyGet || ~level.CanReallySet
                error(message('parallel:settings:NamedNodeRestoreInvalidLevel'));
            end
            
            gettableProps = obj.hGetPublicSettingsProperties();
            userVals = cellfun(@(x) obj.get(x, level.Name), gettableProps, ...
                'UniformOutput', false);
            restorePoint.(level.Name) = cell2struct(userVals, gettableProps, 2);
        end
        
        function hRestore(obj, restorePoint)
        % hRestore(obj, restorePoint) - restore the node from a restorePoint
        % that was generated by hGetRestorePoint
            import parallel.internal.types.SettingsLevel
            import parallel.settings.DefaultValue
            validateattributes(restorePoint, {'struct'}, {'scalar'});
            
            levels = fieldnames(restorePoint);
            try
                cellfun(@(x) SettingsLevel.fromNameI(x), levels, 'UniformOutput', false);
            catch err
                error(message('parallel:settings:NamedNodeRestoreInvalidStruct'));
            end
            
            [~, settableProps] = obj.hGetPublicSettingsProperties();
            try
                for ii = 1:numel(levels)
                    currLevel = levels{ii};
                    restoreStruct = restorePoint.(currLevel);
                    propsToRestore = fieldnames(restoreStruct);
                    % Work out which are actually settable and which should be 
                    % set and which should be unset
                    props = intersect(settableProps(:), propsToRestore);
                    vals = cellfun(@(x) restoreStruct.(x), props, 'UniformOutput', false);
                    propsToUnsetIdx = cellfun(@(x) DefaultValue.isDefault(x), vals);            
                    
                    cellfun(@(x) obj.unset(x, currLevel), props(propsToUnsetIdx));
                    cellfun(@(x, y) obj.set(x, y, currLevel), ...
                        props(~propsToUnsetIdx), vals(~propsToUnsetIdx));
                end
            catch err
                ex = MException(message('parallel:settings:NamedNodeFailedToRestore', obj.Name));
                ex = ex.addCause(err);
                throw(ex);
            end
        end
    end

    methods (Sealed, Hidden)
        function hInvalidate(objOrObjs)
        %hInvalidate Invalidate named node objects without destroying underlying data.
            delete(objOrObjs, 'invalidateOnly');
        end
    end

    methods (Abstract, Access = protected)
        errorIfInvalidName(obj, newName)
    end

    methods (Access = protected)
        function notifyNameChanged(obj, oldName, newName)
        % Fire the NameChanged and SettingsNodeChanged events
            eventData = parallel.internal.settings.NameChangedEventData(oldName, newName);
            obj.notify('NameChanged', eventData);
            obj.notifySettingsNodeChanged();
        end
        function notifySettingsNodeDestroyed(obj)
        % Fire the SettingsNodeDestroyed and SettingsNodeChanged events
            obj.notify('SettingsNodeDestroyed');
            obj.notifySettingsNodeChanged();
        end
        function notifySettingsNodeChanged(obj)
        % Fire the SettingsNodeChanged event
            obj.notify('SettingsNodeChanged');
        end
    end
    
    methods (Sealed, Access = protected)
        function moveNode(obj, newName)
            if obj.NodeFacade.hasAnyFactoryValues()
                error(message('parallel:settings:MoveNodeHasFactoryValues', obj.Name));
            end

            newNodeFacade = obj.createSiblingNodeForCopy(newName);
            try
                % copyAllLevelsToNode may fail if the user does not have write 
                % access to all of the Settings levels in which this NamedNode 
                % is defined (e.g. workgroup)
                obj.NodeFacade.copyAllLevelsToNode(newNodeFacade);
                % Try to delete the existing node facade first before settings
                % the new named node.  Note that this really deletes the Settings 
                % node.  Ensure that we unset the listeners before the delete to
                % prevent the node destroyed listener from firing.  
                obj.KeysChangedOnNodeListener.delete();
                obj.NodeHasBeenDestroyedListener.delete();
                obj.NodeFacade.delete();
                obj.NodeFacade = newNodeFacade;
                % Make sure we attach listeners to the new node facade
                obj.attachListenersToNode();
            catch err
                newNodeFacade.delete();
                % Make sure we attach listeners back to the original node facade
                obj.attachListenersToNode();
                rethrow(err);
            end
        end
        
        function [p, v] = getCollapsedPropertiesNoDefaultValues(obj, p)
        % get the collapsed property values, ignoring any that are 
        % the DefaultValue (i.e. unset)
            v = obj.get(p);
            hasValue = ~cellfun(@(x) parallel.settings.DefaultValue.isDefault(x), v);
            p = p(hasValue);
            v = v(hasValue);
        end
    end
    
    methods (Access = private)
        function attachListenersToNode(obj)
        % Attach listeners to changes the Settings Node
            import parallel.internal.customattr.Reflection

            % Attach listeners to changes in the public-settable keys.
            % The lifetime of these listeners is the same as obj.KeysChangedOnNodeListener
            % and obj.NodeHasBeenDestroyedListener.
            [allProps, useGetSet] = Reflection.getPublicProperties(obj);
            propsUsingGetSet = allProps(useGetSet);
            obj.KeysChangedOnNodeListener = obj.NodeFacade.addChangeListenerToKeys(...
                propsUsingGetSet, @(~, ~) obj.notifySettingsNodeChanged());
            % When the Settings node is destroyed, fire the SettingsNodeDestroyed event
            obj.NodeHasBeenDestroyedListener = event.listener(obj.NodeFacade, ...
                'NodeHasBeenDestroyed', @(~, ~) obj.notifySettingsNodeDestroyed());
        end
        
        function newNodeFacade = createSiblingNodeForCopy(obj, newName)
            import parallel.internal.settings.CustomSettingsGetSet
            newNodeFacade = obj.NodeFacade.createSiblingNode(newName);
            try
                [~, ~, settingsProps, constraints] = obj.hGetPublicSettingsProperties();
                CustomSettingsGetSet.addMissingKeysWithType(newNodeFacade, settingsProps, constraints);
            catch err
                newNodeFacade.delete();
                rethrow(err);
            end
        end
    end
end

