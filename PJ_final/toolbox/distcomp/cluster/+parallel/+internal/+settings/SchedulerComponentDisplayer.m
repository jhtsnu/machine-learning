% SchedulerComponentDisplayer - Displayer for Scheduler Components
% Note that there are displayers for specific scheduler component types
% and this displayer is intended only for vector display of scheduler components

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) SchedulerComponentDisplayer < parallel.internal.settings.NamedNodeDisplayer
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract NamedNodeDisplayer properties
        CommonDisplayProps = {'Name', 'Type'};
        VectorDisplayProps = {'Name', 'Type'};
        VectorTableColumns = { ...
            % 'Title', 'Resizeable', 'MinimumWidth'
            'Name',    true,         length('Name'); ...
            'Type',    false,        length('HPCServer'); ...
        }
        DisplayHelper = parallel.internal.display.DisplayHelper( length('Type') )
    end

    methods 
        function obj = SchedulerComponentDisplayer(someObj)
            obj@parallel.internal.settings.NamedNodeDisplayer(someObj, ...
                'parallel.settings.SchedulerComponent');
        end
    end
    
    methods (Access = protected) % implementation for abstract NamedNodeDisplayer methods
        function displayProperties(obj, heading, propsToDisp)
            % NB It's pretty unlikely that we'd end up displaying a SchedulerComponent
            % because if it's a single SchedulerComponent, then it should have gone 
            % into one of CJS/MJS/LocalComponentDisplayer.
            obj.DisplayHelper.displayMainHeading(heading);
            obj.DisplayHelper.displayProperty('Type', propsToDisp.Type);
        end
        
        function tableData = getTableData(obj, toDisp, getPropsFcn)
            tableData = cell(numel(toDisp), size(obj.VectorTableColumns, 1));
            for ii = 1:numel(toDisp) 
                currToDisp = toDisp(ii);
                propsToDisp = getPropsFcn(currToDisp, obj.VectorDisplayProps);
                tableData(ii, :) = {propsToDisp.Name, propsToDisp.Type};
            end
        end
    end
end
