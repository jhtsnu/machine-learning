% MJSHelper - Helper functions for Mathworks Job Scheduler and Compute Cloud

%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) MJSHelper

    methods ( Static )
    
        function [props, vals] = resolveTimeouts(props, vals)
            % If both DefaultTimeout and JobTimeout are defined, then
            % JobTimeout wins.  If JobTimeout is not defined, but 
            % DefaultTimeout is defined, then the DefaultTimeout value is
            % used for the JobTimeout.  Same behaviour for TaskTimeout.
            % Note that by the time JobTimeout or TaskTimeout get into here,
            % they've already been reduced to just "Timeout"
            defaultTimeoutIdx = strcmp('DefaultTimeout', props);
            timeoutIdx = strcmp('Timeout', props);

            % Nothing to do if none of the timeouts are defined
            if ~any(defaultTimeoutIdx | timeoutIdx)
                return;
            end

            if any(timeoutIdx)
                idxToUse = find(timeoutIdx, 1, 'last');
                timeoutToUse = vals{idxToUse};
            else
                idxToUse = find(defaultTimeoutIdx, 1, 'last');
                timeoutToUse = vals{idxToUse};
            end

            % Remove Job/TaskTimeout and DefaultTimeout from the props
            % and replace with a simple "Timeout"
            props(defaultTimeoutIdx | timeoutIdx) = [];
            vals(defaultTimeoutIdx | timeoutIdx) = [];

            props = [props, 'Timeout'];
            vals = [vals, timeoutToUse];
        end
 
    end

end