% ValidationEventData - Event data for Validation updates

%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) ValidationEventData < event.EventData
    properties (SetAccess = immutable)
        % The name of the profile
        Profile = ''
        % The stage name
        Stage = ''
        % A ValidationStatus enum representing the status
        StatusEnum = parallel.internal.types.ValidationStatus.NotRun
    end
    
    properties
        % The description of the status
        Description = ''
        % The command window output from the validation
        CommandWindowOutput = ''
        % The debugLog(s) for a job - this is always a cell array
        DebugLogCellstr = {}
        % Errors that occurred
        Errors = {}
    end
    
    properties (Dependent)
        % Validation Status string
        Status
        % The error report without any hyperlinks
        ErrorReportNoLinks
        % The error report with or without hyperlinks as appropriate
        % for the current display options
        ErrorReport
        % The debug log as a string
        DebugLog
    end
    
    methods
        function status = get.Status(obj)
            status = obj.StatusEnum.Name;
        end
        
        function report = get.ErrorReportNoLinks(obj)
            getReportFcn = @(x) x.getReport('basic', 'hyperlinks', 'off');
            report = iGetErrorReport(obj.Errors, getReportFcn);
        end
        
        function report = get.ErrorReport(obj)
            getReportFcn = @(x) x.getReport('basic', 'hyperlinks', 'on');
            report = iGetErrorReport(obj.Errors, getReportFcn);
        end
        
        function log = get.DebugLog(obj)
            if isempty(obj.DebugLogCellstr)
                log = '';
            else
                log = strtrim(sprintf('%s\n', obj.DebugLogCellstr{:}));
            end
        end
    end
    
    methods (Static)
        function obj = createOverallValidationStatus(profileName, statusEnum)
        % createOverallValidationStatus(profileName, statusEnum) - create event 
        % data for the overall validation status
            import parallel.internal.settings.ValidationEventData

            obj = ValidationEventData(profileName, '', statusEnum);
        end
        
        function obj = createStageRunning(profileName, stageName)
        % createRunning(profileName, stageName) - create event data 
        % for stage running
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus

            obj = ValidationEventData(profileName, stageName, ...
                ValidationStatus.Running);
        end
        
        function obj = createStageFailedErrorThrown(profileName, stageName, err, ...
            commandWindowOutput, debugLog)
        % createFailedWithError(profileName, stageName, err, debugLog) - create event data 
        % for stage failed because an error was thrown
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus
            validateattributes(err, {'MException'}, {'scalar'});
            validateattributes(commandWindowOutput, {'char'}, {});
            if ~iscell(debugLog)
                debugLog = {debugLog};
            end
            if ~iscellstr(debugLog)
                error(message('parallel:settings:ValidationEventDataDebugLogNotString'));
            end
            
            obj = ValidationEventData(profileName, stageName, ...
                ValidationStatus.Failed);
            obj.Description = getString(message('parallel:validation:StageHadMException'));
            obj.CommandWindowOutput = commandWindowOutput;
            obj.Errors = {err};
            obj.DebugLogCellstr = debugLog;
        end
        
        function obj = createStageFailed(profileName, stageName, description)
        % createFailedCannotRun(profileName, stageName, description) - create event data 
        % for stage failed because it cannot be run
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus
            validateattributes(description, {'char'}, {});
            
            obj = ValidationEventData(profileName, stageName, ValidationStatus.Failed);
            obj.Description = description;
        end
        
        function obj = createStagePassed(profileName, stageName, commandWindowOutput)
        % createPassed(profileName, stageName, commandWindowOutput) - create event data 
        % for stage passed.
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus
            validateattributes(commandWindowOutput, {'char'}, {});
            
            obj = ValidationEventData(profileName, stageName, ...
                ValidationStatus.Passed);
            obj.CommandWindowOutput = commandWindowOutput;
        end
       
        function obj = createStageUnsuccessful(profileName, stageName, ...
            description, commandWindowOutput, errs, debugLog)
        % createUnsuccessful(profileName, stageName, ...
        %    commandWindowOutput, errs, debugLog) - create event data for 
        % stage failed where the stage ran to completion, but the results 
        % were not as expected
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus

            validateattributes(description, {'char'}, {});
            validateattributes(commandWindowOutput, {'char'}, {});
            validateattributes(errs, {'cell'}, {});
            if ~iscell(debugLog)
                debugLog = {debugLog};
            end
            if ~iscellstr(debugLog)
                error(message('parallel:settings:ValidationEventDataDebugLogNotString'));
            end
            
            obj = ValidationEventData(profileName, stageName, ...
                ValidationStatus.Failed);
            obj.Description = description;
            
            obj.CommandWindowOutput = commandWindowOutput;
            obj.DebugLogCellstr     = debugLog;
            obj.Errors              = errs;
        end
        
        function obj = createStageSkipped(profileName, stageName)
        % createSkipped(profileName, stageName) - create event 
        % data for stage skipped because a previous stage errored.
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus
            obj = ValidationEventData(profileName, stageName, ...
                ValidationStatus.Skipped);
            obj.Description = getString(message('parallel:validation:ValidationSkippedPreviousFailure'));
        end

        function obj = createStageSkippedUnsupported(profileName, stageName)
        % createSkipped(profileName, stageName) - create event 
        % data for stage skipped because it isn't supported.
        % TODO:later remove when Mpiexec is removed.
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus
            obj = ValidationEventData(profileName, stageName, ...
                ValidationStatus.Skipped);
            obj.Description = getString(message('parallel:validation:ValidationSkippedNotSupported'));
        end
    end
    
    methods (Access = private)
        function obj = ValidationEventData(profileName, stageName, statusEnum)
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName,   {'char'}, {});
            validateattributes(statusEnum,  {'parallel.internal.types.ValidationStatus'}, {'scalar'});

            obj.Profile     = profileName;
            obj.Stage       = stageName;
            obj.StatusEnum  = statusEnum;
            % Set the default description
            obj.Description = getString(message('parallel:validation:ValidationStatus', obj.Status));
        end
    end
end



function report = iGetErrorReport(errs, getReportFcn)
if isempty(errs)
    report = '';
else
    errReports = cellfun(getReportFcn, errs, ...
        'UniformOutput', false);
    report = strtrim(sprintf('%s\n', errReports{:}));
end
end
