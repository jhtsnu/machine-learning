
% NamedNodesCollection - A collection of named nodes

%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) NamedNodesCollection < parallel.internal.settings.AbstractNodesCollection
    properties (GetAccess = private, SetAccess = immutable)
        % The type of named node stored in this collection
        Type
        % The top level Settings node for this collection.
        % All nodes in the collection are children of this 
        % root node.
        TopLevelRawSettingsNode

        % Function handles for creating nodes
        EmptyNodeFcn
        NodeBuilderFcn
    end
    
    properties (Access = private)
        % Array of nodes
        CachedNodes
        % Cell array of listeners
        CachedNodeListeners
    end
    
    properties (Dependent, GetAccess = private)
        CachedNodeNames
    end
    
    methods
        function names = get.CachedNodeNames(obj)
            names = {obj.CachedNodes.Name};
        end
    end
    
    methods
        function obj = NamedNodesCollection(type, topLevelSettingsNode, NodeBuilderFcn, emptyFcn)
            validateattributes(type, {'char'}, {'row'});
            validateattributes(topLevelSettingsNode, {'Settings'}, {'scalar'});
            validateattributes(NodeBuilderFcn, {'function_handle'}, {'scalar'});
            validateattributes(emptyFcn, {'function_handle'}, {'scalar'});
            obj.Type = type;
            obj.TopLevelRawSettingsNode = topLevelSettingsNode;
            obj.EmptyNodeFcn = emptyFcn;
            obj.NodeBuilderFcn = NodeBuilderFcn;
            obj.CachedNodes = emptyFcn();
            obj.CachedNodeListeners = {};
        end
        
        function delete(obj)
            % Ensure that we invalidate the handles of all the nodes
            % in the cache
            obj.CachedNodes.hInvalidate();
        end

        function nodes = getAll(obj)
        % getAll - Get the NamedNodes that are defined under the TopLevelRawSettingsNode
            obj.refreshCacheIfRequired();
            nodes = obj.CachedNodes;
            if ~isempty(nodes)
                % Return a sorted list in alphabetical order.
                [~, sortOrder] = sort(lower(obj.CachedNodeNames));
                nodes = nodes(sortOrder);
            end
        end

        function namedNode = createNew(obj, name, pvPairs)
        % createNew - Build a new NamedNode
            validateattributes(pvPairs, {'cell'}, {});
            if ~isvarname(name)
                error(message('parallel:settings:NamedNodeNameNotVarName'))
            end
            
            [propNames, propVals, level] = iAmalgamateAndExtractLevel(pvPairs);
            % TODO:later make this node hidden
            obj.TopLevelRawSettingsNode.addNode(name);
            try
                childNode = obj.TopLevelRawSettingsNode.(name);
                namedNode = obj.NodeBuilderFcn(name, childNode, obj.TopLevelRawSettingsNode);
            catch err
                % Remove the node if we couldn't create the object
                obj.TopLevelRawSettingsNode.removeNode(name);
                ex = MException(message('parallel:settings:NamedNodesBuildNewFailed', obj.Type, name));
                ex = ex.addCause(err);
                throwAsCaller(ex);
            end
            
            % Set the P-V pairs
            try
                cellfun(@(x, y) namedNode.set(x, y, level.Name), propNames, propVals);
            catch err
                % delete the profile to remove it from the settings tree
                % if we couldn't set the P-V pairs
                namedNode.delete();
                ex = MException(message('parallel:settings:NamedNodesBuildNewFailed', obj.Type, name));
                ex = ex.addCause(err);
                throwAsCaller(ex);
            end
            obj.addNodeToCache(namedNode);
        end
        
        function tf = nameExists(obj, name)
        % tf = nameExists(obj, name) - does the name exist?
        % Refreshes the cache if it is not up-to-date.
            validateattributes(name, {'char'}, {'row'});
            obj.refreshCacheIfRequired();
            allNames = obj.CachedNodeNames;
            tf = any(strcmp(name, allNames));
        end

        function names = getAllNames(obj)
        % getAllNames - get the names of all the NamedNodes.
        % Refreshes the cache if it is not up-to-date.
            obj.refreshCacheIfRequired();
            nodes = obj.getAll();
            names = {nodes.Name};
        end
        
        function node = getFromName(obj, name)
        % getFromName - get a specific node from its name.  
        % Returns an empty node if no node with that name can be found.
        % Does not refresh the cache, but instead loads only the specified 
        % name to facilitate faster retrieval of nodes when using 
        % >> p = parallel.Settings
        % >> p.findProfile('Name', 'foo');
            [nodeExistsInCache, node] = obj.getNodeFromCacheNoRefresh(name);
            if nodeExistsInCache
                return;
            end

            % If the node isn't in the cache, then check to 
            % see if that name actually exists as a child
            % of the current root.  If it does, then try 
            % construct the node, otherwise we'll just return 
            % empty.  Don't try to do a refreshCache here to avoid
            % loading all children.
            children = properties(obj.TopLevelRawSettingsNode);
            if any(strcmp(name, children))
                nodeWasRemoved = obj.removeNodeOrEnsureInCache(name);
                if nodeWasRemoved
                    warning(message('parallel:settings:NamedNodesDeletedInvalidChild', name, obj.TopLevelRawSettingsNode.SettingsName, obj.Type));
                else
                    % Node must exist in the cache, so no need to 
                    % check again.
                    [~, node] = obj.getNodeFromCacheNoRefresh(name);
                end
            else
                node = obj.EmptyNodeFcn();
            end
        end
        
        function deleteNodes(obj, names)
        % deleteNodes(obj, names) - removes the specified named
        % nodes from the cache and deletes the actual node (i.e. removes 
        % from Settings).  Does not throw errors if the names do not
        % exist.
            if isempty(names)
                return;
            end
            if ~iscell(names)
                names = {names};
            end
            if ~iscellstr(names)
                error(message('parallel:settings:DeleteNamedNodesNameNotString'));
            end
            
            for ii = 1:numel(names)
                currName = names{ii};
                node = obj.getFromName(currName);
                if ~isempty(node)
                    node.delete();
                end
            end
        end
        
        function deleteAllNodes(obj)
        % deleteAllNodes(obj) - deletes all the nodes from the cache
        % and removes the actual Settings nodes.
            obj.refreshCacheIfRequired();
            nodes = obj.CachedNodes;
            if ~isempty(nodes)
                nodes.delete();
            end
        end
        
        function [addedNames, origNames, haveNamesChanged] = addFromCollection(obj, otherCollection, ...
            nameSuffix, level, usedNames)
        % [addedNames, origNames, haveNamesChanged] = addFromCollection(obj, otherCollection, ...
        %   nameSuffix, level, usedNames)
        % Add the nodes from the otherCollection to this collection at the specified level.   
        % Any nodes with the whose name clashes with one in this collection or with any of the 
        % names in usedNames will be given an new name inspired by the original name and the 
        % nameSuffix.  Returns the names of all the nodes that were added, their original names 
        % and a boolean flag indicating if the name has changed.
            validateattributes(otherCollection, {'parallel.internal.settings.NamedNodesCollection'}, {'scalar'});
            validateattributes(nameSuffix, {'char'}, {});
            validateattributes(level, {'parallel.internal.types.SettingsLevel'}, {'scalar'});
            if nargin < 5
                usedNames = {};
            elseif ~iscellstr(usedNames)
                error(message('parallel:settings:NamedNodesCollectionAddUsedNames'));
            end
            if isequal(obj, otherCollection)
                error(message('parallel:settings:NamedNodesCollectionAddFromSame'));
            end
            if ~isequal(obj.Type, otherCollection.Type)
                error(message('parallel:settings:NamedNodesCollectionAddFromWrongType', otherCollection.Type, obj.Type));
            end
            if isequal(obj.TopLevelRawSettingsNode, otherCollection.TopLevelRawSettingsNode)
                error(message('parallel:settings:NamedNodesCollectionAddFromSameSettingsNode'));
            end
            
            origNames        = otherCollection.getAllNames();
            addedNames       = cell(size(origNames));
            haveNamesChanged = false(size(origNames));
            haveAddedIdx     = false(size(origNames));
            for ii = 1:numel(origNames)
                currName = origNames{ii};
                % The source settings node that we need to add to our TopLevelRawSettingsNode
                otherSettingsNode = otherCollection.TopLevelRawSettingsNode.(currName);
                % Check for name clashes with our names
                [currName, haveNamesChanged(ii)] = obj.getUnusedName(currName, nameSuffix, usedNames);
                % Add the new node to our TopLevelRawSettingsNode
                obj.TopLevelRawSettingsNode.addNode(currName);
                addedNames{ii} = currName;
                destSettingsNode = obj.TopLevelRawSettingsNode.(currName);
                try
                    % Merge the contents of the other node onto ours.
                    matlab.internal.mergeSettings(destSettingsNode, otherSettingsNode, level.Name);
                    haveAddedIdx = true;
                catch err
                    % Remove the settings node that was just added to the TopLevelRawSettingsNode, 
                    % but has not yet had its values merged from the otherCollection
                    obj.TopLevelRawSettingsNode.removeNode(currName);
                    % Remove the nodes that have already been added
                    cellfun(@(x) obj.TopLevelRawSettingsNode.removeNode(x), addedNames(haveAddedIdx));
                    ex = MException(message('parallel:settings:NamedNodesMergeSettingsFailed', currName));
                    ex = ex.addCause(err);
                    throw(ex);
                end
            end
            % Now refresh the cache so that we get the newly added nodes.
            obj.refreshCacheIfRequired();
        end
    end
    
    methods (Access = private)
        function [nodeExists, node] = getNodeFromCacheNoRefresh(obj, nodeName)
            obj.removeInvalidCachedNodes();
            nodeIdx = strcmp(nodeName, obj.CachedNodeNames);
            nodeExists = any(nodeIdx) && sum(nodeIdx) == 1;
            if nodeExists
                node = obj.CachedNodes(nodeIdx);
            else
                node = obj.EmptyNodeFcn();
            end
        end

        function refreshCacheIfRequired(obj)
            % Cache is up to date if the names match with the child nodes
            obj.removeInvalidCachedNodes();
            names = properties(obj.TopLevelRawSettingsNode);
            needsRefresh = ~isequal(sort(names(:)), sort(obj.CachedNodeNames(:)));

            if needsRefresh
                obj.refreshCache();
            end
        end
        
        function refreshCache(obj)
        % Refresh the cache with the current child nodes of the root node.
        % Note that nodes are automatically removed from the cache
        % when the underlying Settings node is deleted.
            children = properties(obj.TopLevelRawSettingsNode);

            % Remove stale nodes from the cache
            % NB Invalid handles have already been removed from the cache.
            staleNodes = setdiff(obj.CachedNodeNames, children);
            cellfun(@(x) obj.removeNodeFromCache(x), staleNodes);

            % Then add any missing children to the cache.
            % Keep track of any children that aren't valid profiles/components
            invalidChildren = {};
            deletedChildren = {};
            for ii = 1:numel(children)
                currNodeName = children{ii};
                try
                    nodeWasRemoved = obj.removeNodeOrEnsureInCache(currNodeName);
                    if nodeWasRemoved
                        deletedChildren{end+1} = currNodeName; %#ok<AGROW>
                    end
                catch err %#ok<NASGU>
                    invalidChildren{end+1} = currNodeName; %#ok<AGROW>
                end
            end
            % Warn about any invalid/deleted nodes
            if ~isempty(deletedChildren)
                warning(message('parallel:settings:NamedNodesDeletedInvalidChildren', obj.TopLevelRawSettingsNode.SettingsName, strtrim( sprintf( '%s ', deletedChildren{ : } ) )));
            end
            if ~isempty(invalidChildren)
                warning(message('parallel:settings:NamedNodesFailedToRemoveInvalidChildren', obj.TopLevelRawSettingsNode.SettingsName, strtrim( sprintf( '%s ', invalidChildren{ : } ) )));
            end     
        end
        
        function wasRemoved = removeNodeOrEnsureInCache(obj, nodeName)
        % See if the supplied nodeName is a valid named node and ensure it is in 
        % the cache.  If the nodeName is not a valid named node, then remove it
        % from the TopLevelRawSettingsNode.
            if obj.TopLevelRawSettingsNode.existKey(nodeName)
                % Can't be a valid profile/component if it is a key, so remove it
                obj.TopLevelRawSettingsNode.removeKey(nodeName);
                wasRemoved = true;
                return;
            end

            existingNames = obj.CachedNodeNames;
            wasRemoved = false;
            if ~any(strcmp(nodeName, existingNames))
                try
                    % Construct the namedNode and store it in the cache.
                    namedNode = obj.NodeBuilderFcn(nodeName, obj.TopLevelRawSettingsNode.(nodeName), obj.TopLevelRawSettingsNode);
                    obj.addNodeToCache(namedNode);
                catch err %#ok<NASGU>
                    % wasn't a valid named node
                    obj.TopLevelRawSettingsNode.removeNode(nodeName);
                    wasRemoved = true;
                end
            end
        end

        function addNodeToCache(obj, namedNode)
            import parallel.internal.settings.NamedNodeEventData
            
            obj.CachedNodeListeners{end + 1} = obj.addListenersForCachedNode(namedNode);
            obj.CachedNodes(end + 1) = namedNode;
            
            obj.notify('NodeAdded', NamedNodeEventData(namedNode));
        end

        function listeners = addListenersForCachedNode(obj, namedNode)
            % Make sure we remove the namedNode from the cache if the user 
            % deletes it so that we don't have stale NamedNodes in the cache.
            % Also make sure we change the cache if the user renames the 
            % namedNode.
            nodeName = namedNode.Name;
            destroyListener = event.listener(namedNode, 'ObjectBeingDestroyed', ...
                @(~, ~) obj.removeNodeFromCache(nodeName));
            nodeDestroyedListener = event.listener(namedNode, 'SettingsNodeDestroyed', ...
                @(~, ~)  obj.removeNodeFromCache(nodeName));
            nameChangedListener = event.listener(namedNode, 'NameChanged', ...
                @(namedNodeSrc, ~) obj.reattachListenersForCachedNode(namedNodeSrc));
            listeners = [destroyListener, nodeDestroyedListener, nameChangedListener];
        end

        function removeNodeFromCache(obj, nodeName)
            % Remove the node and the listeners from the arrays
            obj.removeInvalidCachedNodes();
            nodeIdx = strcmp(nodeName, obj.CachedNodeNames);
            obj.removeNodesAndEnsureConsistentCache(nodeIdx);
        end
        
        function reattachListenersForCachedNode(obj, namedNode)
            % Find the existing node and make sure we re-attach listeners 
            % based on the new name
            obj.removeInvalidCachedNodes();
            nodeIdx = strcmp(namedNode.Name, obj.CachedNodeNames);
            obj.CachedNodeListeners{nodeIdx} = obj.addListenersForCachedNode(namedNode);
        end
        
        function removeInvalidCachedNodes(obj)
            % remove any invalid nodes first
            invalidNodes = ~arrayfun(@isvalid, obj.CachedNodes);
            obj.removeNodesAndEnsureConsistentCache(invalidNodes);
        end
        
        function removeNodesAndEnsureConsistentCache(obj, idxToRemove)
        % Remove nodes from the cache and ensure that an empty cache is 
        % always 0x0
            obj.CachedNodes(idxToRemove) = [];
            obj.CachedNodeListeners(idxToRemove) = [];
            if isempty(obj.CachedNodes)
                obj.CachedNodes = obj.EmptyNodeFcn();
                obj.CachedNodeListeners = {};
            end
        end
    end
end

function [props, vals, level] = iAmalgamateAndExtractLevel(pvPairs)
% Amalgamate the PV pairs and extract the level
import parallel.internal.types.SettingsLevel
import parallel.internal.customattr.PropSet

[levelIsSpecified, level, pvPairs] = PropSet.extractOneProperty('Level', pvPairs{:});
% Default to user level if none supplied
if ~levelIsSpecified
    level = SettingsLevel.User;
else
    level = SettingsLevel.fromNameI(level);
    SettingsLevel.errorIfLevelsNotSettable(level);
end

[props, vals] = parallel.internal.customattr.PropSet.amalgamate({}, pvPairs{:});
end

