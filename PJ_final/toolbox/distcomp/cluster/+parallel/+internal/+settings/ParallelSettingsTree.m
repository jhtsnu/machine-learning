% ParallelSettingsTree - Class to hold names and sub-nodes of the parallel Settings node.

% Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) ParallelSettingsTree    
    properties (Constant)
        ParallelNodeName = 'parallel';
        ProfilesNodeName = 'profiles';
        SchedulerComponentsNodeName = 'schedulercomponents';
        ProjectComponentsNodeName = 'projectcomponents';
        ClientSettingsNodeName = 'client';
        DeployedSettingsNodeName = 'deployed';
        InternalSettingsNodeName = 'internal';
        ProfilesReadOnlySchedNodeName = 'profilesWithReadOnlySched';

        ParallelNode = iGetParallelSettingsNode();
        % The following are all sub-nodes of the ParallelNode
        ProfilesNode = iGetProfilesSettingsNode();
        SchedulerComponentsNode = iGetSchedulerComponentsSettingsNode();
        ProjectComponentsNode = iGetProjectComponentsSettingsNode();
        ClientSettingsNode = iGetClientSettingsNode();
        InternalNode = iGetInternalNode();
        ProfilesReadOnlySchedNode = iGetProfilesReadOnlySchedNode();
    end
    
    methods (Static)
        % Use get* methods to access deployed nodes to avoid creating the 
        % nodes unless they are really needed
        function node = getDeployedNode()
            import parallel.internal.settings.ParallelSettingsTree
            node = iGetDeployedNode();
        end
        
        function node = getDeployedProfilesNode()
            import parallel.internal.settings.ParallelSettingsTree
            node = iGetDeployedSubNode(ParallelSettingsTree.ProfilesNodeName);
        end

        function node = getDeployedSchedulerComponentsNode()
            import parallel.internal.settings.ParallelSettingsTree
            node = iGetDeployedSubNode(ParallelSettingsTree.SchedulerComponentsNodeName);
        end
        function node = getDeployedProjectComponentsNode()
            import parallel.internal.settings.ParallelSettingsTree
            node = iGetDeployedSubNode(ParallelSettingsTree.ProjectComponentsNodeName);
        end
    end
end

%---------------------------------------------------
function parallelNode = iGetParallelSettingsNode()
import parallel.internal.settings.ParallelSettingsTree
% Ensure we have the settings tree set up correctly first

settingsRoot = Settings;
parallelNode = settingsRoot.(ParallelSettingsTree.ParallelNodeName);
end

%---------------------------------------------------
function profilesNode = iGetProfilesSettingsNode()
import parallel.internal.settings.ParallelSettingsTree
parallelRoot = ParallelSettingsTree.ParallelNode;
profilesNode = parallelRoot.(ParallelSettingsTree.ProfilesNodeName);
end

%---------------------------------------------------
function componentsNode = iGetSchedulerComponentsSettingsNode()
import parallel.internal.settings.ParallelSettingsTree
parallelRoot = ParallelSettingsTree.ParallelNode;
componentsNode = parallelRoot.(ParallelSettingsTree.SchedulerComponentsNodeName);
end

%---------------------------------------------------
function componentsNode = iGetProjectComponentsSettingsNode()
import parallel.internal.settings.ParallelSettingsTree
parallelRoot = ParallelSettingsTree.ParallelNode;
componentsNode = parallelRoot.(ParallelSettingsTree.ProjectComponentsNodeName);
end

%---------------------------------------------------
function node = iGetClientSettingsNode()
import parallel.internal.settings.ParallelSettingsTree
parallelRoot = ParallelSettingsTree.ParallelNode;
node = parallelRoot.(ParallelSettingsTree.ClientSettingsNodeName);
end

%---------------------------------------------------
function node = iGetInternalNode()
import parallel.internal.settings.ParallelSettingsTree
parallelRoot = ParallelSettingsTree.ParallelNode;
node = parallelRoot.(ParallelSettingsTree.InternalSettingsNodeName);
end

%---------------------------------------------------
function node = iGetProfilesReadOnlySchedNode()
import parallel.internal.settings.ParallelSettingsTree
internalNode = ParallelSettingsTree.InternalNode;
node = internalNode.(ParallelSettingsTree.ProfilesReadOnlySchedNodeName);
end

%---------------------------------------------------
function deployedNode = iGetDeployedNode()
% NB The deployed node only contains the profiles and components
% and not the client stuff.
import parallel.internal.settings.ParallelSettingsTree
persistent sDeployedNode

if isempty(sDeployedNode)
    sDeployedNode = matlab.internal.createStandaloneRoot(ParallelSettingsTree.DeployedSettingsNodeName);
    nodeNames = { ...
        ParallelSettingsTree.ProfilesNodeName, ...
        ParallelSettingsTree.SchedulerComponentsNodeName, ...
        ParallelSettingsTree.ProjectComponentsNodeName};
    cellfun(@(x) sDeployedNode.addNode(x, 'hidden'), nodeNames);

    % NB all scheduler component nodes must be visible
    schedCompsNode = sDeployedNode.(ParallelSettingsTree.SchedulerComponentsNodeName);
    [~, schedTypeNames] = enumeration('parallel.internal.types.SchedulerType');
    cellfun(@(x) schedCompsNode.addNode(x, 'visible'), schedTypeNames);
end

deployedNode = sDeployedNode;
end

%---------------------------------------------------
function node = iGetDeployedSubNode(nodeName)
import parallel.internal.settings.ParallelSettingsTree
deployedNode = iGetDeployedNode();
node = deployedNode.(nodeName);
end
