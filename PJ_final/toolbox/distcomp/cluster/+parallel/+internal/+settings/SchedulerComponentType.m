% SchedulerComponentType - Class to get interesting information for scheduler components

%   Copyright 2011 The MathWorks, Inc.

classdef SchedulerComponentType
    methods (Static)
        function buildFcn = getBuildFunction(types)
            validateattributes(types, {'parallel.internal.types.SchedulerType'}, {});
            buildFcn = arrayfun(@(x) iGetSchedulerInfo(x), types, 'UniformOutput', false);
            if numel(types) == 1
                buildFcn  = buildFcn {1};
            end
        end
        
        function emptyFcn = getEmptyFunction(types)
            validateattributes(types, {'parallel.internal.types.SchedulerType'}, {});
            [~, emptyFcn] = arrayfun(@(x) iGetSchedulerInfo(x), types, 'UniformOutput', false);
            if numel(types) == 1
                emptyFcn = emptyFcn{1};
            end
        end
    end
end

%-----------------------------------------------------
function [buildFcn, emptyFcn] = iGetSchedulerInfo(schedType)
import parallel.internal.types.SchedulerType
import parallel.settings.schedulerComponent.*
validateattributes(schedType, {'parallel.internal.types.SchedulerType'}, {'scalar'});

persistent infoMap
if isempty(infoMap)
    infoMap = containers.Map();
end

if ~infoMap.isKey(schedType.Name)
    switch schedType
        case SchedulerType.Generic
            buildFcn = @Generic.hBuildFromSettings;
            emptyFcn = @Generic.empty;
        case SchedulerType.HPCServer
            buildFcn = @HPCServer.hBuildFromSettings;
            emptyFcn = @HPCServer.empty;
        case SchedulerType.Local
            buildFcn = @Local.hBuildFromSettings;
            emptyFcn = @Local.empty;
        case SchedulerType.LSF
            buildFcn = @LSF.hBuildFromSettings;
            emptyFcn = @LSF.empty;
        case SchedulerType.MJS
            buildFcn = @MJS.hBuildFromSettings;
            emptyFcn = @MJS.empty;
        case SchedulerType.MJSComputeCloud
            buildFcn = @MJSComputeCloud.hBuildFromSettings;
            emptyFcn = @MJSComputeCloud.empty;
        case SchedulerType.Mpiexec
            buildFcn = @Mpiexec.hBuildFromSettings;
            emptyFcn = @Mpiexec.empty;
        case SchedulerType.PBSPro
            buildFcn = @PBSPro.hBuildFromSettings;
            emptyFcn = @PBSPro.empty;
        case SchedulerType.Torque
            buildFcn = @Torque.hBuildFromSettings;
            emptyFcn = @Torque.empty;
        otherwise
            assert( false, 'Unhandled cluster type: %s', schedType.Name );
    end
    
    infoMap(schedType.Name) = {buildFcn, emptyFcn};
end

schedInfo = infoMap(schedType.Name);
buildFcn         = schedInfo{1};
emptyFcn         = schedInfo{2};
end

