% Enumeration of possible sources of a given value

% Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) ValueType
    enumeration
        Factory
        Profile
        User
    end
end
