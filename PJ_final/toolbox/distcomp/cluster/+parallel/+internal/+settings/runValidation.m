function runValidation(profileName)
% runValidation(profileName) - Run validation for the profile with the specified name.  
% Used by the Rerun link in the ValidationDisplayer

%   Copyright 2011-2012 The MathWorks, Inc.

import parallel.internal.settings.ValidationDisplayer
validateattributes(profileName, {'char'}, {'row'});

p = parallel.Settings;
if ~p.hProfileExists(profileName)
    p.hThrowNonExistentProfile(profileName)
end

validator = parallel.settings.Profile.hGetRegisteredValidator();
if isempty(validator)
    warning(message('parallel:settings:NoRegisteredProfileValidator'));
else
    % Ensure that validation results are displayed to the command 
    % windows when validation is launched through the API.
    displayer = ValidationDisplayer(validator); %#ok<NASGU> - hold onto displayer object
    validator.validate(profileName);
end
