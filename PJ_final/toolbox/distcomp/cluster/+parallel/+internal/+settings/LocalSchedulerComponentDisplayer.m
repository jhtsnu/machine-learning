% LocalSchedulerComponentDisplayer - Displayer for the Local Scheduler Component

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) LocalSchedulerComponentDisplayer < parallel.internal.settings.NamedNodeDisplayer
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract NamedNodeDisplayer properties
        CommonDisplayProps = {'Name', 'Type', 'NumWorkers', 'JobStorageLocation'};
        VectorDisplayProps = {'Name', 'Type', 'NumWorkers'};
        VectorTableColumns = { ...
            % 'Title',    'Resizeable', 'MinimumWidth'
            'Name',       true,         length('Name'); ...
            'Type',       false,        length('Local'); ...
            'NumWorkers', false,        length('Use Default'); ...
        }
        DisplayHelper = parallel.internal.display.DisplayHelper( length('JobStorageLocation') )
    end

    methods 
        function obj = LocalSchedulerComponentDisplayer(someObj)
            obj@parallel.internal.settings.NamedNodeDisplayer(someObj, ...
                'parallel.settings.schedulerComponent.Local');
        end
    end
    
    methods (Access = protected) % implementation for abstract NamedNodeDisplayer methods
        function displayProperties(obj, heading, propsToDisp)
            obj.DisplayHelper.displayMainHeading(heading);
            obj.DisplayHelper.displayProperty('Type',       propsToDisp.Type);
            obj.DisplayHelper.displayProperty('NumWorkers', propsToDisp.NumWorkers);
            if isstruct(propsToDisp.JobStorageLocation)
                % It's very, very unlikely that a Local Scheduler Component will have
                % a structure value, but if it is, then just display the 
                % datalocation for the current platform
                if ispc
                    dataLocToDisp = propsToDisp.JobStorageLocation.windows;
                else
                    dataLocToDisp = propsToDisp.JobStorageLocation.unix;
                end
            else
                dataLocToDisp = propsToDisp.JobStorageLocation;
            end
            % If the value isn't Use Default, then don't allow formatting to 
            % avoid truncation of the datalocation
            doFormatDataLocation = parallel.settings.DefaultValue.isDefault(dataLocToDisp);
            obj.DisplayHelper.displayProperty('JobStorageLocation', dataLocToDisp, doFormatDataLocation);
        end
        
        function tableData = getTableData(obj, toDisp, getPropsFcn)
            tableData = cell(numel(toDisp), size(obj.VectorTableColumns, 1));
            for ii = 1:numel(toDisp) 
                currToDisp = toDisp(ii);
                propsToDisp = getPropsFcn(currToDisp, obj.VectorDisplayProps);
                tableData(ii, :) = {propsToDisp.Name, propsToDisp.Type, propsToDisp.NumWorkers};
            end
        end
    end
end
