% ThirdPartyCJSComponentDisplayer - Displayer for 3rd party CJS Components

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) ThirdPartyCJSComponentDisplayer < parallel.internal.settings.NamedNodeDisplayer
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract NamedNodeDisplayer properties
        CommonDisplayProps = {'Name', 'Type', 'NumWorkers', 'ClusterMatlabRoot', ...
                              'JobStorageLocation', 'RequiresMathWorksHostedLicensing', ...
                              'LicenseNumber'};
        VectorDisplayProps = {'Name', 'Type', 'NumWorkers'};
        VectorTableColumns = { ...
            % 'Title',    'Resizeable', 'MinimumWidth'
            'Name',       true,         length('Name'); ...
            'Type',       false,        length('HPCServer'); ...
            'NumWorkers', false,        length('Use Default'); ...
        }
        DisplayHelper = parallel.internal.display.DisplayHelper( length('ParallelSubmissionWrapperScript') )
    end

    methods 
        function obj = ThirdPartyCJSComponentDisplayer(someObj)
            obj@parallel.internal.settings.NamedNodeDisplayer(someObj, ...
                'parallel.settings.schedulerComponent.ThirdPartyCJS');
        end
    end
    
    methods (Access = protected) % implementation for abstract NamedNodeDisplayer methods
        function displayProperties(obj, heading, propsToDisp)
            obj.DisplayHelper.displayMainHeading(heading);
            obj.DisplayHelper.displayProperty('Type',              propsToDisp.Type);
            obj.DisplayHelper.displayProperty('NumWorkers',        propsToDisp.NumWorkers);
            obj.DisplayHelper.displayProperty('ClusterMatlabRoot', propsToDisp.ClusterMatlabRoot);
            if isstruct(propsToDisp.JobStorageLocation)
                % Don't allow formatting to avoid truncation of the datalocation
                doFormatDataLocation = false;
                obj.DisplayHelper.displayProperty('JobStorageLocation (Windows)', ...
                    propsToDisp.JobStorageLocation.windows, doFormatDataLocation);
                obj.DisplayHelper.displayProperty('JobStorageLocation (Unix)', ...
                    propsToDisp.JobStorageLocation.unix, doFormatDataLocation);
            else
                % If the value isn't Use Default, then don't allow formatting to 
                % avoid truncation of the datalocation
                doFormatDataLocation = parallel.settings.DefaultValue.isDefault(propsToDisp.JobStorageLocation);
                obj.DisplayHelper.displayProperty('JobStorageLocation', propsToDisp.JobStorageLocation, doFormatDataLocation);
            end
            obj.DisplayHelper.displayProperty('RequiresMathWorksHostedLicensing', propsToDisp.RequiresMathWorksHostedLicensing);
            obj.DisplayHelper.displayProperty('LicenseNumber', propsToDisp.LicenseNumber);
        end
        
        function tableData = getTableData(obj, toDisp, getPropsFcn)
            tableData = cell(numel(toDisp), size(obj.VectorTableColumns, 1));
            for ii = 1:numel(toDisp) 
                currToDisp = toDisp(ii);
                propsToDisp = getPropsFcn(currToDisp, obj.VectorDisplayProps);
                tableData(ii, :) = {propsToDisp.Name, propsToDisp.Type, propsToDisp.NumWorkers};
            end
        end
    end
end
