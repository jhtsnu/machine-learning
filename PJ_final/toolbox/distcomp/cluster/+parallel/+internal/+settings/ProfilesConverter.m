% ProfilesConverter - Convert profiles to configuration structures and vice versa.
 
%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) ProfilesConverter
    properties (Constant, GetAccess = private)
        ProfileFromConfigurationName = 'ProfileFromConfiguration';
    end

    methods (Static)
        function setConfigurationStructOnProfile(profile, configStruct)
        % setConfigurationStructOnProfile(profile, configStruct)
        %   Modify the values on the supplied profile to match those in the supplied 
        %   configuration structure.  If the profile does not already have a project 
        %   component, then one is created, even if a project component is not strictly
        %   required.
        %   This is used for upgrading configurations to profiles and for interim
        %   support of configurations-as-profiles from the config UI.
        
            import parallel.internal.apishared.ConversionContext
            import parallel.internal.settings.ProfilesConverter
            iErrorIfNotValidConfigStruct(configStruct);

            % Get the components for this profile
            origPCName = profile.ProjectComponent;
            [schedComp, projComp, createdProjComp] = iGetOrCreateComponents(profile);            
            % Convert the config properties to their profile versions and work out
            % which apply to the different components
            clusterType = ConversionContext.ClusterType.convertToApi2(configStruct.Values.findResource.Type);
            [configProps, configVals]   = iGetConvertedPropsFromConfigStruct(configStruct, clusterType);
            [scPropsToSet, scValsToSet] = iGetConfigPropsToSetOnComponent(schedComp, configProps, configVals);
            [pcPropsToSet, pcValsToSet] = iGetConfigPropsToSetOnComponent(projComp, configProps, configVals);

            % Set the properties on the components, deleting or restoring component
            % values as appropriate if anything goes wrong.
            didSetSchedComp = false;
            try
                oldScVals = schedComp.get(scPropsToSet);
                schedComp.set(scPropsToSet, scValsToSet);
                didSetSchedComp = true;
                projComp.set(pcPropsToSet, pcValsToSet);
            catch err
                if didSetSchedComp
                    schedComp.set(scPropsToSet, oldScVals);
                end
                if createdProjComp
                    profile.ProjectComponent = origPCName;
                    projComp.delete();
                end
                rethrow(err);
            end
        end
        
        function profile = createProfileFromConfigurationStruct(configStruct)
        % profile = createProfileFromConfigurationStruct(configStruct)
        %   Create a new profile and components for the supplied configuration structure.
        %   The created profile always has both a scheduler component and project component,
        %   even if a project component is not strictly required.
        %   This is used for upgrading configurations to profiles.
            import parallel.internal.apishared.ConversionContext
            import parallel.internal.settings.ProfilesConverter
            iErrorIfNotValidConfigStruct(configStruct);

            % Make sure the profile name is unique
            p = parallel.Settings;
            profileName = configStruct.Name;
            % If the original config name isn't a valid profile name, then use
            % the magic string to avoid weird names coming out of genvarname.
            if ~p.hIsValidName(profileName)
                profileName = ProfilesConverter.ProfileFromConfigurationName;
            end
            profileName = p.hGetUnusedProfileName(profileName);
            [scName, pcName] = ProfilesConverter.deriveComponentNamesForProfile(profileName);
            % Create the components
            clusterType = ConversionContext.ClusterType.convertToApi2(configStruct.Values.findResource.Type);
            schedComp = p.createSchedulerComponent(clusterType, scName);
            try
                projComp = p.createProjectComponent(pcName);
            catch err
                schedComp.delete();
                rethrow(err);
            end

            [configProps, configVals]   = iGetConvertedPropsFromConfigStruct(configStruct, clusterType);
            [scPropsToSet, scValsToSet] = iGetConfigPropsToSetOnComponent(schedComp, configProps, configVals);
            [pcPropsToSet, pcValsToSet] = iGetConfigPropsToSetOnComponent(projComp, configProps, configVals);
            try
                schedComp.set(scPropsToSet, scValsToSet);
                projComp.set(pcPropsToSet, pcValsToSet);
                % Create the profile - the configuration always contains a description, so always set it
                profile = p.createProfile(profileName, scName, ...
                    'Description', configStruct.Values.Description, 'ProjectComponent', pcName);
            catch err
                schedComp.delete();
                projComp.delete();
                rethrow(err);
            end
        end
        
        function configStruct = getConfigurationStructureFromProfile(profile)
        % configStruct = getConfigurationStructureFromProfile(profile)
        %   Get the equivalent configuration structure for the supplied profile.
        %   This is used for support of configurations-as-profiles from API1 and
        %   the config UI.
            import parallel.internal.apishared.ConversionContext
            import parallel.internal.customattr.PropSet
            import parallel.internal.settings.ProfilesConverter
            import parallel.internal.settings.ProfileExpander
            
            % Get the current P-V pairs
            schedComp = profile.getSchedulerComponent();
            description = iConvertUseDefaultToEmptyString(profile.Description);
            [clusterProps, clusterVals] = ProfileExpander.getClusterProperties(profile.Name);
            [jobProps, jobVals]         = ProfileExpander.getJobProperties(profile.Name);
            [taskProps, taskVals]       = ProfileExpander.getTaskProperties(profile.Name);
            
            % Convert the prop names to API1
            schedType    = ConversionContext.ClusterType.convertToApi1(schedComp.Type);
            % Ensure that we can actually convert to API1 for this type
            [~, eNames] = enumeration('parallel.internal.apishared.ConversionContext');
            if ~strcmp(schedComp.Type, eNames)
                error(message('parallel:settings:CannotConvertSchedulerTypeToApi1', ...
                    schedComp.Type));
            end
            clusterProps = ConversionContext.(schedComp.Type).convertToApi1(clusterProps);
            jobProps     = ConversionContext.Job.convertToApi1(jobProps);
            taskProps    = ConversionContext.Task.convertToApi1(taskProps);
            % Add the type to the front of the cluster props
            clusterProps = ['Type', clusterProps];
            clusterVals  = [schedType, clusterVals];
            % Convert the cluster values to API1
            [clusterProps, clusterVals] = iConvertClusterValuesToApi1(clusterProps, clusterVals);
            [jobProps, jobVals] = iConvertWorkerLimitsToMinMax(jobProps, jobVals);
            
            % Create the configurations structure
            [findResourceSection, schedSection, jobSection, pJobSection, taskSection] = iGetConfigurationSectionNames(schedType);
            findResourceStruct  = iCreateConfigSectionStruct(findResourceSection, clusterProps, clusterVals);
            schedulerStruct     = iCreateConfigSectionStruct(schedSection,        clusterProps, clusterVals);
            jobStruct           = iCreateConfigSectionStruct(jobSection,          jobProps,     jobVals);
            parallelJobStruct   = iCreateConfigSectionStruct(pJobSection,         jobProps,     jobVals);
            taskStruct          = iCreateConfigSectionStruct(taskSection,         taskProps,    taskVals);
            
            configValueStruct = struct( ...
                'Description',  description, ...
                'findResource', findResourceStruct, ...
                'scheduler',    schedulerStruct, ...
                'job',          jobStruct,   ...
                'paralleljob',  parallelJobStruct, ...
                'task',         taskStruct);
            configStruct = struct('Name', profile.Name, 'Values', configValueStruct);
        end
        
        function [scName, pcName] = deriveComponentNamesForProfile(profileName)
        % deriveComponentNamesForProfile - Get sensible names for the components 
        % associated with a profile
            import parallel.internal.settings.ProfilesConverter
            validateattributes(profileName, {'char'}, {'row'});
            % Get unique component names for the supplied profile name
            scName = sprintf('%sSchedulerComponent', profileName);
            pcName = sprintf('%sProjectComponent', profileName);

            % Make sure the profile names are unique
            p = parallel.Settings;
            scName = p.hGetUnusedSchedulerComponentName(scName);
            pcName = p.hGetUnusedProjectComponentName(pcName);
        end
    end
end

%---------------------------------------------------
function [schedComp, projComp, createdPc] = iGetOrCreateComponents(profile)
% Get the profile's components, creating new components where necessary.
import parallel.internal.settings.ProfilesConverter
[~, pcName] = ProfilesConverter.deriveComponentNamesForProfile(profile.Name);
createdPc = false;
p = parallel.Settings;
% NB getSchedulerComponent will never return an empty scheduler component
schedComp = profile.getSchedulerComponent();
projComp = profile.getProjectComponent();

if isempty(projComp)
    try
        projComp = p.createProjectComponent(pcName);
        createdPc = true;
        profile.ProjectComponent = pcName;
    catch err
        if createdPc
            projComp.delete();
        end
        rethrow(err);
    end
end
end

%---------------------------------------------------
function iErrorIfNotValidConfigStruct(configStruct)
iErrorIfNotField(configStruct, 'Name');
iErrorIfNotField(configStruct, 'Values');
iErrorIfNotField(configStruct.Values, 'Description');
iErrorIfNotField(configStruct.Values, 'findResource');
iErrorIfNotField(configStruct.Values, 'scheduler');
iErrorIfNotField(configStruct.Values, 'job');
iErrorIfNotField(configStruct.Values, 'paralleljob');
iErrorIfNotField(configStruct.Values, 'task');
iErrorIfNotField(configStruct.Values.findResource, 'Type');
end

%---------------------------------------------------
function iErrorIfNotField(configStruct, fieldname)
if ~isfield(configStruct, fieldname)
    error(message('parallel:settings:MissingFieldInConfigurationStructure', fieldname));
end
end

%---------------------------------------------------
function [props, vals] = iConvertClusterValuesToApi1(props, vals)
import parallel.internal.apishared.DataLocConv

[props, vals] = iConvertValue(props, vals, ...
    'DataLocation', @DataLocConv.toApi1);
[props, vals] = iConvertValue(props, vals, ...
    'ClusterOsType', @(x) iConvertOs(x, 'Api1Name'));
end

%---------------------------------------------------
function [props, vals] = iConvertClusterValuesToApi2(props, vals)
import parallel.internal.apishared.DataLocConv

[props, vals] = iConvertValue(props, vals, ...
    'JobStorageLocation', @DataLocConv.toApi2);
[props, vals] = iConvertValue(props, vals, ...
    'OperatingSystem', @(x) iConvertOs(x, 'Api2Name'));
end

%---------------------------------------------------
function newVal = iConvertOs(oldVal, osTypePropName)
% Convert oldVal into a parallel.internal.apishared.OsType and
% get the new value using the osTypePropName from the osType.
% 
% e.g. Convert to API1
% iConvertOs(val, 'Api1Name')
import parallel.internal.apishared.OsType
osType = OsType.fromName(oldVal);
newVal = osType.(osTypePropName);
end

%---------------------------------------------------
function [props, vals] = iConvertValue(props, vals, propToConvert, convertFcn)
validateattributes(props, {'cell'}, {});
validateattributes(vals, {'cell'}, {'numel', numel(props)});
propIdx = strcmp(props, propToConvert);
if any(propIdx)
    idx = find(propIdx, 1, 'first');
    newVal = convertFcn(vals{idx});
    vals{propIdx} = newVal;
end
end

%---------------------------------------------------
function [props, vals] = iConvertWorkerMinMaxToLimits(props, vals, configName)
% [props, vals] = iConvertWorkerMinMaxToLimits(props, vals)
%   If props contains API1 MinimumNumberOfWorkers and/or MaximumNumberOfWorkers, 
%   then these are removed and converted to the corresponding API2 NumWorkersRange.  
%   The NumWorkersRange prop/val is appended to the end of the prop/val 
%   arrays.
%   Props and vals are always returned as a column.
validateattributes(props, {'cell'}, {});
validateattributes(vals, {'cell'}, {'numel', numel(props)});
minWorkersIdx = strcmp(props, 'MinimumNumberOfWorkers');
maxWorkersIdx = strcmp(props, 'MaximumNumberOfWorkers');
minMaxIdx     = minWorkersIdx | maxWorkersIdx;
if any(minMaxIdx)
    if any(minWorkersIdx)
        idx = find(minWorkersIdx, 1, 'first');
        minWorkers = vals{idx};
    else
        minWorkers = 1;
    end
    if any(maxWorkersIdx)
        idx = find(maxWorkersIdx, 1, 'first');
        maxWorkers = vals{idx};
    else
        maxWorkers = inf;
    end
    % Remove Min/Max Workers
    props(minMaxIdx) =  [];
    vals(minMaxIdx)  =  [];
    if maxWorkers < minWorkers
        warning(message('parallel:settings:InvalidMinMaxWorkerValuesInConfiguration', ...
            minWorkers, maxWorkers, configName));
    else
        % And add NumWorkersRange to the end
        props(end + 1) = {'NumWorkersRange'};
        vals(end + 1)  = {[minWorkers maxWorkers]};
    end
end
props = props(:);
vals  = vals(:);
end

%---------------------------------------------------
function [props, vals] = iConvertWorkerLimitsToMinMax(props, vals)
% [newProps, newVals] = iConvertWorkerLimitsToMinMax(props, vals)
%   If props contains API2 NumWorkersRange, then these are removed and 
%   converted to the corresponding API1 Min/MaximumNumberOfWorkers.  
%   The Min/MaximumNumberOfWorkers props/vals are appended to the end 
%   of the prop/val arrays.
%   Props and vals are always returned as a column.
validateattributes(props, {'cell'}, {});
validateattributes(vals, {'cell'}, {'numel', numel(props)});
workerLimitsIdx = strcmp(props, 'NumWorkersRange');
if any(workerLimitsIdx)
    idx = find(workerLimitsIdx, 1, 'first');
    workerLimits = vals{idx};
    minWorkers = workerLimits(1);
    maxWorkers = workerLimits(end);
    % Remove WorkerNumLimits
    props(workerLimitsIdx) = [];
    vals(workerLimitsIdx)  = [];
    % And add min/max workers to the end
    props(end + 1: end + 2) = {'MinimumNumberOfWorkers', 'MaximumNumberOfWorkers'};
    vals(end + 1: end + 2)  = {minWorkers, maxWorkers};
end
props = props(:);
vals  = vals(:);
end

%---------------------------------------------------
function value = iConvertUseDefaultToEmptyString(value)
if parallel.settings.DefaultValue.isDefault(value)
    value = '';
end
end

%---------------------------------------------------
function [combinedProps, combinedVals] = iCombineJobProps(configName, jobProps, jobVals, pJobProps, pJobVals)
% It is possible that properties are different for job and paralleljob for 
% a configuration that was created manually (e.g. by our tests).  When the 
% config UI encounters such a situation, it uses the values from the job section
% and ignores the paralleljob section.  Conversion to profiles will do the same
% thing.
%
% NB The configurations UI does not allow you enter different values
% for job/paralleljob on a configuration, so customers should never
% encounter this problem, unless they have been mucking around with the 
% preferences directly. 

[~, jobUnionIdx, pJobUnionIdx] = union(jobProps, pJobProps);
combinedProps = [jobProps(jobUnionIdx); pJobProps(pJobUnionIdx)];
for ii = 1:numel(combinedProps)
    currProp = combinedProps{ii};
    jobIdx = strcmp(currProp, jobProps);
    pJobIdx = strcmp(currProp, pJobProps);
    if any(jobIdx) && any(pJobIdx)
        if ~isequal(jobVals(jobIdx), pJobVals(pJobIdx))
            warning(message('parallel:settings:DifferentJobParallelJobValuesInConfiguration', currProp, configName));
            % Set the paralleljob value to be the same as the job
            pJobVals(pJobIdx) = jobVals(jobIdx);
        end
    end
end
combinedVals = [jobVals(jobUnionIdx); pJobVals(pJobUnionIdx)];
% Now convert min/max workers to limits
[combinedProps, combinedVals] = iConvertWorkerMinMaxToLimits(combinedProps, combinedVals, configName);
end

%---------------------------------------------------
function [configProps, configVals] = iGetConvertedPropsFromConfigStruct(configStruct, clusterType)
% Get the props and values out of the configuration, converting prop names to API2.
import parallel.internal.apishared.ConversionContext
import parallel.internal.apishared.DataLocConv
% Convert properties in the structure to their V2 equivalents
[frProps, frVals]       = iConvertPropsInStructToApi2(configStruct.Values.findResource, ConversionContext.(clusterType));
[schedProps, schedVals] = iConvertPropsInStructToApi2(configStruct.Values.scheduler,    ConversionContext.(clusterType));
[jobProps, jobVals]     = iConvertPropsInStructToApi2(configStruct.Values.job,          ConversionContext.Job);
[pJobProps, pJobVals]   = iConvertPropsInStructToApi2(configStruct.Values.paralleljob,  ConversionContext.Job);
[taskProps, taskVals]   = iConvertPropsInStructToApi2(configStruct.Values.task,         ConversionContext.Task);

% Convert the sched values to use API2
[schedProps, schedVals] = iConvertClusterValuesToApi2(schedProps, schedVals);
% Combine the job and parallel job props, dealing with min/max workers -> limits
[jobProps, jobVals] = iCombineJobProps(configStruct.Name, jobProps, jobVals, pJobProps, pJobVals);

% Combine findResource and Scheduler props
[~, frIdx, schedIdx] = union(frProps, schedProps);
schedProps = [frProps(frIdx); schedProps(schedIdx)];
schedVals  = [frVals(frIdx);  schedVals(schedIdx)];
% NB there is an assumption that there are no user-settable
% scheduler, job or task properties that have the same name
configProps = [schedProps; jobProps; taskProps];
configVals  = [schedVals;  jobVals;  taskVals];
end

%---------------------------------------------------
function [props, vals] = iConvertPropsInStructToApi2(propStruct, context)
% Convert properties in the configuration struct to their API2 equivalents
props = fieldnames(propStruct);
vals = struct2cell(propStruct);
props = context.convertToApi2(props);
props = props(:);
vals = vals(:);
end

%---------------------------------------------------
function configSectionStruct = iCreateConfigSectionStruct(configSectionName, props, vals)
% Create the configuration structure for the specified section of the configuration.
props = props(:);
vals = vals(:);
% Get all the props for this section of the configuration
allConfigProps = distcomp.configpropstorage.getConfigurableProperties(configSectionName);
allConfigProps = allConfigProps(:);
% Get the props that need to be stored in the struct
[~, propIdx, configIdx] = intersect(props, allConfigProps);
if isempty(propIdx)
    % cell2struct({}, {}, 1) returns a 0x1 struct array.
    % Make sure we return a 0x0 struct array to make comparison
    % will the original struct easier.
    configSectionStruct = struct([]);
else
    configSectionStruct = cell2struct(vals(propIdx), allConfigProps(configIdx), 1);
    % NB the order of names in the struct must match the order returned from getConfigurableProperties
    configSectionStruct = orderfields(configSectionStruct, allConfigProps(sort(configIdx)));
end
end

%---------------------------------------------------
function [findResource, sched, job, pJob, task] = iGetConfigurationSectionNames(schedType)
% Get the correct configuration section names for the different types of API1 schedulers
% These section names need to be the same as the ones in 
% distcomp.configpropstorage.pGetAllConfigurableProperties
if strcmp(schedType, 'jobmanager')
    findResource = 'JobManagerFindResource';
    sched = schedType;
    job = 'job';
    pJob = 'paralleljob';
    task = 'task';
else
    if strcmp(schedType, 'mpiexec')
        sched = 'mpiexec';
    elseif strcmp(schedType, 'hpcserver')
        sched = 'ccsscheduler';
    else
        sched = sprintf('%sscheduler', schedType);
    end
    findResource = 'findResource';
    job = 'simplejob';
    pJob = 'simpleparalleljob';
    task = 'simpletask';
end
end

%---------------------------------------------------
function [propsToSet, valsToSet] = iGetConfigPropsToSetOnComponent(component, configProps, configVals)
% Get the list of props and values that need to be set on the supplied component
import parallel.internal.customattr.Reflection
settableProps = Reflection.getUserSettableProperties(component);
settableProps = settableProps(:);
configProps   = configProps(:);
configVals    = configVals(:);
[~, profileIdx, configIdx] = intersect(settableProps, configProps);
propsToSet = settableProps(profileIdx);
valsToSet  = configVals(configIdx);
end
