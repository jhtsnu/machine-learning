% ProfileDisplayer - Displayer for Profiles

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) ProfileDisplayer < parallel.internal.settings.NamedNodeDisplayer
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract NamedNodeDisplayer properties
        CommonDisplayProps = {'Name', 'Description', 'SchedulerComponent', 'ProjectComponent', 'ValidationStatus'};
        VectorDisplayProps = {'Name', 'Description', 'SchedulerComponent', 'ProjectComponent', 'ValidationStatus'};
        VectorTableColumns = { ...
            % 'Title',            'Resizeable', 'MinimumWidth'
            'Name',               true,         numel('Name'); ...
            'Description',        true,         numel('Description'); ...
            'SchedulerComponent', true,         numel('SchedulerComponent'); ...
            'ProjectComponent',   true,         numel('ProjectComponent'); ...
            'ValidationStatus',   false,        numel('ValidationStatus'); ...
        }
        DisplayHelper = parallel.internal.display.DisplayHelper( length('SchedulerComponent') )
    end

    methods % constructor
        function obj = ProfileDisplayer(someObj)
            obj@parallel.internal.settings.NamedNodeDisplayer(someObj, ...
                'parallel.settings.Profile');
        end
    end
    
    methods (Access = protected) % implementation for abstract NamedNodeDisplayer methods
        function displayProperties(obj, heading, propsToDisp)
            obj.DisplayHelper.displayMainHeading(heading);
            obj.DisplayHelper.displayProperty('Description',        propsToDisp.Description);
            obj.DisplayHelper.displayProperty('SchedulerComponent', propsToDisp.SchedulerComponent);
            obj.DisplayHelper.displayProperty('ProjectComponent',   propsToDisp.ProjectComponent);
            obj.DisplayHelper.displayProperty('ValidationStatus',   propsToDisp.ValidationStatus);
        end
        
        function tableData = getTableData(obj, toDisp, getPropsFcn)
            tableData = cell(numel(toDisp), size(obj.VectorTableColumns, 1));
            for ii = 1:numel(toDisp) 
                currToDisp = toDisp(ii);
                propsToDisp = getPropsFcn(currToDisp, obj.VectorDisplayProps);
                tableData(ii, :) = {propsToDisp.Name, propsToDisp.Description, ...
                    propsToDisp.SchedulerComponent, propsToDisp.ProjectComponent, ...
                    propsToDisp.ValidationStatus};
            end
        end
    end
end
