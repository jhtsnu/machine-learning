% NameChangedEventData - Event Data for a Named Node's NameChanged event
 
%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) NameChangedEventData < event.EventData
    properties
        OldName
        NewName
    end
    
    methods
        function obj = NameChangedEventData(oldName, newName)
            obj.OldName = oldName;
            obj.NewName = newName;
        end
    end
end