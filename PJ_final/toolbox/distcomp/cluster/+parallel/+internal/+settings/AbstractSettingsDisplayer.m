% AbstractSettingsDisplayer - base displayer class for all objects with properties stored in Settings

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden) AbstractSettingsDisplayer < parallel.internal.display.Displayer
    methods % constructor
        function obj = AbstractSettingsDisplayer(someObj, requiredType)
            obj@parallel.internal.display.Displayer(someObj, requiredType);
        end
    end
    
    methods % overrides of methods in Displayer
        function doDisplay(obj, toDisp, name, level)
        % doDisplay(obj, toDisp, name) - display the collapsed values
        % of the object
        %
        % doDisplay(obj, toDisp, name, level) - display the values
        % at the specified level.
        
            import parallel.internal.display.Displayer
            if nargin < 4
                % normal display without a level
                doDisplay@parallel.internal.display.Displayer(obj, toDisp, name);
                return;
            end
        
            obj.errorIfWrongType(toDisp);
            Displayer.displayInputName(name);
            obj.doDisp(toDisp, level);
        end
        
        function doDisp(obj, toDisp, level)
        % doDisp(obj, toDisp) - disp the collapsed values of the object
        %
        % doDisp(obj, toDisp, level) - disp the values at the specified
        % level.
            import parallel.internal.display.Displayer
            import parallel.internal.types.SettingsLevel
            
            if nargin < 3
                % normal display without a level
                doDisp@parallel.internal.display.Displayer(obj, toDisp);
                return;
            end

            obj.errorIfWrongType(toDisp);
            % Creating a SettingsLevel from the string will error if the 
            % string isn't a valid level
            settingsLevel = SettingsLevel.fromNameI(level);
            SettingsLevel.errorIfLevelsNotGettable(settingsLevel);

            if numel(toDisp) == 0
                % don't care about displaying levels for empty objects
                obj.displayEmptyObject(toDisp);
            elseif numel(toDisp) == 1
                obj.doSingleDisplayForLevel(toDisp, level);
            else
                obj.doVectorDisplayForLevel(toDisp, level);
            end
            
            isLoose = Displayer.isLooseSpacing();
            if isLoose
                fprintf('\n');
            end
        end
    end
    
    methods (Abstract, Access = protected)
        doSingleDisplayForLevel(obj, toDisp, level);
        doVectorDisplayForLevel(obj, toDisp, level);
    end
    
    methods (Static, Access = protected) % overrides of methods in Displayer
        function propStruct = getDisplayPropsAsStruct(toDisp, propNames, varargin)
        % Gets the specified properties from the toDisp object and
        % returns them as a struct whose fieldnames are the property
        % names.
        % The only permitted varargin is the level at which to retrieve the
        % property values.
            if nargin < 3
                % normal display without a level
                propStruct = getDisplayPropsAsStruct@parallel.internal.display.Displayer(toDisp, propNames);
                return;
            end
            
            level = varargin{1};
            % Work out which properties are stored in Settings and can be 
            % accessed at specific levels and which cannot.
            gettableSettingsProps = toDisp.hGetPublicSettingsProperties();
            propsNoLevels = setdiff(propNames, gettableSettingsProps);
            propsWithLevels = intersect(propNames, gettableSettingsProps);
            
            % Get the props that have no level
            valsNoLevels = toDisp.get(propsNoLevels);
            % Get the other props at the specified level
            valsWithLevels = cellfun(@(x) toDisp.get(x, level), propsWithLevels, ...
                'UniformOutput', false);
            propStruct = cell2struct([valsNoLevels, valsWithLevels], [propsNoLevels, propsWithLevels], 2);
        end
    end
    
    methods (Access = protected)
        function [names, values] = getSpecificDisplayProps(~, toDisp, varargin)
        % getSpecificDisplayProps - get display properties that are specific to 
        % this object.
        % The only permitted varargin is the level at which to retrieve the
        % property values.

            narginchk(1, 3);
            [names, values] = toDisp.hCustomizeDisplay(varargin{:}); 
        end
        % todo: get rid of varargin and use a different way to call hCustomizeDisplay (esp from Settings)
        function displaySpecificItems( obj, toDisp, varargin )
            % Displays items that are specific to the object by asking the
            % object for its display items.  Subclasses will call this
            % from doSingleDisplay()
            if ~isscalar(toDisp)
                error(message('parallel:cluster:DisplaySpecificItemsNotScalar'));
            end
            
            % Get the items that are specific to the type of object.
            [names, values] = obj.getSpecificDisplayProps(toDisp, varargin{:} ); 
            if numel( names ) ~= numel (values )
                error(message('parallel:cluster:DisplayInternalNameValueMismatch', obj.Class));
            end
            if ~isempty( names )
                obj.DisplayHelper.displaySubHeading( '%s Specific Properties', obj.ClassDisplayName );
                for ii = 1:numel( names )
                    obj.DisplayHelper.displayProperty( names{ii}, values{ii} );
                end
            end
        end
    end
end