% AbstractConfigSerializer - Base class for configurations serializers
%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden) AbstractConfigSerializer
    methods (Abstract, Static)
        instance = getInstance()
    end

    methods (Abstract)
        newName = clone(obj, name, proposedName)
        newName = createNew(obj, proposedName, type)
        [name, nameInFile, values] = createNewFromFile(obj, filename)
        deleteConfig(obj, configName)
        rename(obj, oldName, newName)
        names = getAllNames(obj)
        name = getDefaultName(obj)
        setDefaultName(obj, configName)
        configStruct = getConfigurationStruct(obj, configName)
        saveConfigurationStruct(obj, configName, newValues)
        cacheCounter = getCacheCounter(obj)
    end
    
    methods 
        function addUndoStateListener(~, ~)
            % default to no listener support
        end
        function redo(~)
            % default to no listener support
        end
        function redoAll(~)
            % default to no listener support
        end
        function removeUndoStateListener(~, ~)
            % default to no listener support
        end
        function undo(~)
            % default to no listener support
        end
        function undoAll(~)
            % default to no listener support
        end
    end
end