% AbstractValidator - Base class for validators that are registered with Profiles

% Copyright 2011 The MathWorks, Inc.

classdef (Hidden) AbstractValidator < handle
    properties (Constant, Access = private)
        % Default status for new profiles
        DefaultStatus = parallel.internal.types.ValidationStatus.NotRun;
    end

    events (NotifyAccess = 'protected')
        ValidationStarted
        ValidationFinished
        ValidationStageUpdated
    end
    
    methods (Abstract)
        status = getStatus(obj, profileName)
        setStatus(obj, profileName, value)
        deleteStatus(obj, profileName)
        validate(obj, profileName)
    end
    
    methods (Sealed, Static)
        function status = getDefaultStatus()
            status = parallel.internal.settings.AbstractValidator.DefaultStatus;
        end
    end
end
