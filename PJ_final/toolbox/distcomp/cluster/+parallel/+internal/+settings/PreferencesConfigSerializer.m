% PreferencesConfigSerializer - Serializer for configurations that are stored in Preferences
%
% Use pctconfig('useprofiles', <true/false>) to switch between using (API2) 
% Profiles-backed configurations and (API1) preferences-backed configurations

% Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) PreferencesConfigSerializer < parallel.internal.settings.AbstractConfigSerializer
    methods (Static)
        function instance = getInstance()
            persistent sInstance
            if isempty(sInstance)
                sInstance = parallel.internal.settings.PreferencesConfigSerializer;
            end
            instance = sInstance;
        end
    end

    methods % implementation of AbstractConfigSerializer Abstract methods
        function newName = clone(~, name, proposedName)
        % clone - clone a configuration.  Returns the actual name of
        % the new configuration
            validateattributes(name, {'char'}, {'row'});
            validateattributes(proposedName, {'char'}, {'row'});
            newName = distcomp.configserializer.clone(name, proposedName);
        end
        
        function newName = createNew(~, proposedName, ~)
        % createNew - create a new configuration of the specified type.  
        % Returns the actual name of the new configuration
            validateattributes(proposedName, {'char'}, {'row'});
            newName = distcomp.configserializer.createNew(proposedName);
        end
        
        function [name, nameInFile, values] = createNewFromFile(~, filename)
        % createNewFromFile - import the configuration from the supplied file
        % Returns the actual name of the new configuration, the name of the 
        % configuration/profile in the file and a structure containing the 
        % configuration values.  
        % The file must be an exported configuration.  It cannot be a profile.
            validateattributes(filename, {'char'}, {'row'});
            [nameInFile, values] = distcomp.configserializer.loadconfigfile(filename);
            name = distcomp.configserializer.createNew(sprintf('%s_import', nameInFile), nameInFile);
        end
        
        function deleteConfig(~, configName)
        % deleteConfig - delete the config with the specified name
            validateattributes(configName, {'char'}, {'row'});
            distcomp.configserializer.deleteConfig(configName);
        end
                
        function rename(~, oldName, newName)
        % rename - rename the specified config.  Errors if the newName is already
        % in use.
            validateattributes(oldName, {'char'}, {'row'});
            validateattributes(newName, {'char'}, {'row'});
            distcomp.configserializer.rename(oldName, newName);
        end
        
        function names = getAllNames(~)
        % getAllNames - get the names of all configurations
            names = distcomp.configserializer.getAllNames();
        end
        
        function name = getDefaultName(~)
        % getDefaultName - get the name of the default configuration
            name = distcomp.configserializer.getCurrentName();
        end
        
        function setDefaultName(~, configName)
        % setDefaultName - set the name of the default configuration
            validateattributes(configName, {'char'}, {'row'});
            distcomp.configserializer.setCurrentName(configName);
        end
        
        function configStruct = getConfigurationStruct(~, configName)
        % getConfigurationStruct - get the configuration structure for the 
        % configuration with the specified name.  The returned structure
        % has fields 'findResource', 'scheduler', 'job', 'paralleljob', 'task'.
        % (i.e. it is the "Values" section of the configuration.
            validateattributes(configName, {'char'}, {'row'});
            configStruct = distcomp.configserializer.load(configName);
        end
        
        function saveConfigurationStruct(~, configName, newValues)
        % saveConfigurationStruct - save the new values for the specified
        % configuration.  The supplied structure is the "Values" section of the 
        % configuration and should have fields 'findResource', 'scheduler', 'job',
        % 'paralleljob', 'task'.
            validateattributes(configName, {'char'}, {'row'});
            validateattributes(newValues, {'struct'}, {'scalar'});
            distcomp.configserializer.save(configName, newValues);
        end
        
        function cacheCounter = getCacheCounter(~)
        % getCacheCounter - get the cache counter for the configurations.  
        % The counter is increased every time a configuration is saved.
            cacheCounter = distcomp.configserializer.getCacheCounter();
        end
    end
    
    methods % overrides of AbstractConfigSerializer methods
        function addUndoStateListener(~, listener)
            distcomp.configserializer.addUndoStateListener(listener);
        end
        function redo(~)
            distcomp.configserializer.redo();
        end
        function redoAll(~)
            distcomp.configserializer.redoAll();
        end
        function removeUndoStateListener(~, listener)
            distcomp.configserializer.removeUndoStateListener(listener);
        end
        function undo(~)
            distcomp.configserializer.undo();
        end
        function undoAll(~)
            distcomp.configserializer.undoAll();
        end
    end

    methods (Access = private)
        function obj = PreferencesConfigSerializer()
        end
    end
end