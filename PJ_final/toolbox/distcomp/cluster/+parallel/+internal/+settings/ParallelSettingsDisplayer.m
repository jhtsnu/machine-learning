% ParallelSettingsDisplayer - Displayer for the parallel.Settings object

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) ParallelSettingsDisplayer < parallel.internal.settings.AbstractSettingsDisplayer

    properties (SetAccess = immutable, GetAccess = protected)
        DisplayHelper = parallel.internal.display.DisplayHelper( length('SchedulerComponents') )
    end

    properties (Constant, GetAccess = private)
        % When we have pctconfig, include Hostname and PortRange in the display
        CommonDisplayProps = {'DefaultProfile', ...
            'Profiles', 'SchedulerComponents', 'ProjectComponents'};

        % LevelDisplayProps = {'DefaultProfile', 'Hostname', 'PortRange'};
        LevelDisplayProps = {'DefaultProfile'};
    end

    methods % constructor
        function obj = ParallelSettingsDisplayer(someObj)
            obj@parallel.internal.settings.AbstractSettingsDisplayer(someObj, ...
                'parallel.Settings');
        end
    end
    
    methods (Sealed, Access = protected) % implementation for abstract Displayer methods
        function doSingleDisplay(obj, toDisp)
            import parallel.internal.settings.AbstractSettingsDisplayer
            import parallel.internal.settings.ParallelSettingsDisplayer
            propsToDisp = AbstractSettingsDisplayer.getDisplayPropsAsStruct(toDisp, ParallelSettingsDisplayer.CommonDisplayProps);
            
            obj.doMainDisplay('Parallel Settings Information', propsToDisp);
            obj.doProfilesDisplay(propsToDisp);
        end
        
        function doVectorDisplay(obj, toDisp)
            % Vector display just displays the dimension because parallel.Settings is a singleton.
            % NB don't use ClassDisplayName here to ensure the full "parallel.Settings" is displayed
            % instead of just "Settings"
            obj.DisplayHelper.displayDimension(size(toDisp), class(toDisp));
        end
    end
    
    methods (Sealed, Access = protected) % implementation for abstract AbstractSettingsDisplayer methods
        function doSingleDisplayForLevel(obj, toDisp, level)
            import parallel.internal.settings.AbstractSettingsDisplayer
            import parallel.internal.settings.ParallelSettingsDisplayer
            propsToDisp = AbstractSettingsDisplayer.getDisplayPropsAsStruct(toDisp, ParallelSettingsDisplayer.LevelDisplayProps, level);
 
            heading = sprintf('Parallel Settings Information (%s Level)', level);
            obj.doMainDisplay(heading, propsToDisp);
            obj.doProfilesDisplay(propsToDisp);
        end
        
        function doVectorDisplayForLevel(obj, toDisp, ~)
            obj.doVectorDisplay(toDisp);
        end
    end
    
    methods (Access = private)
        function doMainDisplay(obj, heading, propsToDisp)
            obj.DisplayHelper.displayMainHeading(heading);
            obj.DisplayHelper.displayProperty('DefaultProfile', propsToDisp.DefaultProfile);
            % obj.DisplayHelper.displayProperty('Hostname', propsToDisp.Hostname);
            % obj.DisplayHelper.displayProperty('PortRange', propsToDisp.PortRange);
        end
        
        function doProfilesDisplay(obj, propsToDisp)
            obj.DisplayHelper.displaySubHeading('Profiles and Components');
            obj.DisplayHelper.displayProperty('Profiles',            propsToDisp.Profiles);
            obj.DisplayHelper.displayProperty('SchedulerComponents', propsToDisp.SchedulerComponents);
            obj.DisplayHelper.displayProperty('ProjectComponents',   propsToDisp.ProjectComponents);
        end
    end

end