% TypedNodesCollection - Collection of typed NamedNodesCollections

% Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) TypedNodesCollection < parallel.internal.settings.AbstractNodesCollection
    properties (GetAccess = private, SetAccess = immutable)        % Map to stash node collections based on types
        NodeCollectionMap
        % The default empty function to use when returning empty from this collection
        DefaultEmptyFcn
    end
    
    methods
        function obj = TypedNodesCollection(types, topSettingsNode, builderFcns, emptyFcns, defaultEmptyFcn)
        % A TypedNodesCollection has a collection of NamedNodesCollections referenced by type.
        % types - cell array of names of the types
        % topSettingsNode - the root Settings nodes for all of the NamedNodesCollections.  It is 
        %            assumed the topSettingsNode has a child node for each of the specified types
        % builderFcns - array of function handles for the builders of the NamedNode.  
        %               One per type.
        % emptyFcns - array of function handles for the empty functions of the NamedNode.
        %             One per type.
        % defaultEmptyFcn - the empty function to use when returning empty from this collection
        
            import parallel.internal.settings.NamedNodesCollection
            
            validateattributes(topSettingsNode, {'Settings'}, {'scalar'});
            if ~iscellstr(types)
                error(message('parallel:settings:TypedNodesCollections'));
            end
            validateattributes(builderFcns, {'cell'}, {'numel', numel(types)});
            validateattributes(emptyFcns, {'cell'}, {'numel', numel(types)});
            validateattributes(defaultEmptyFcn, {'function_handle'}, {'scalar'})
            
            obj.DefaultEmptyFcn = defaultEmptyFcn;
            % Create the map of the all the collections for the different 
            % types
            obj.NodeCollectionMap = containers.Map();
            for ii = 1:numel(types)
                currType = types{ii};
                nodeCollection = NamedNodesCollection(currType, topSettingsNode.(currType), ...
                    builderFcns{ii}, emptyFcns{ii});
                % Add a NodeAdded listener to the collection to
                % ensure that our NodeAdded event is fired.
                nodeCollection.addlistener('NodeAdded', @(~, evtData) obj.notify('NodeAdded', evtData));
                obj.NodeCollectionMap(currType) = nodeCollection;
            end
        end
    end
    
    methods
        function nodes = getAll(obj)
        % getAll - Get the NamedNodes that are defined in this collection
            collections = values(obj.NodeCollectionMap);
            nodesByType = cellfun(@(x) x.getAll, collections, 'UniformOutput', false);
            nodes = [nodesByType{:}];
            if isempty(nodes)
                nodes = obj.DefaultEmptyFcn();
            end
        end
        
        function node = createNew(obj, type, name, pvPairs)
        % createNew - Build a new NamedNode of the specified type
            import parallel.internal.settings.TypedNodesCollection
            validateattributes(type, {'char'}, {'row'});
            validateattributes(name, {'char'}, {'row'});
            validateattributes(pvPairs, {'cell'}, {});
            if ~obj.NodeCollectionMap.isKey(type)
                allTypes = keys(obj.NodeCollectionMap);
                error(message('parallel:settings:InvalidNamedNodeType', type, strtrim( sprintf( '%s ', allTypes{ : } ) )));
            end
            
            collection = obj.NodeCollectionMap(type);
            node = collection.createNew(name, pvPairs);
        end
        
        function names = getAllNames(obj)
        % getAllNames - get the names of all the nodes
            collections = values(obj.NodeCollectionMap);
            allNames = cellfun(@(x) x.getAllNames(), collections, 'UniformOutput', false);
            names = [allNames{:}];
        end

        function node = getFromName(obj, name)
        % getFromName - get a specific node from its name.  
        % Returns an empty node if no node with that name can be found.
            validateattributes(name, {'char'}, {'row'});
            collections = values(obj.NodeCollectionMap);
            for ii = 1:numel(collections)
                node = collections{ii}.getFromName(name);
                if ~isempty(node)
                    return;
                end
            end
            % Must be empty at this point if we haven't returned yet
            % so make sure we return an empty node of a sensible type.
            node = obj.DefaultEmptyFcn();
        end

        function tf = nameExists(obj, name)
        % tf = nameExists(obj, name) - does the name exist?
            validateattributes(name, {'char'}, {'row'});
            collections = values(obj.NodeCollectionMap);
            tf = cellfun(@(x) x.nameExists(name), collections);
            tf = any(tf);
        end

        function deleteNodes(obj, names)
        % deleteNodes(obj, names) - removes the specified named
        % nodes from the cache and deletes the actual node (i.e. removes 
        % from Settings).  Does not throw errors if the names do not
        % exist.
            collections = values(obj.NodeCollectionMap);
            cellfun(@(x) x.deleteNodes(names), collections);
        end

        function deleteAllNodes(obj)
        % deleteAllNodes(obj) - deletes all the nodes from the cache
        % and removes the actual Settings nodes.
            collections = values(obj.NodeCollectionMap);
            cellfun(@(x) x.deleteAllNodes(), collections);
        end
        
        function [addedNames, origNames, haveNamesChanged] = addFromCollection(obj, otherCollection, ...
            nameSuffix, level, usedNames)
        % [addedNames, origNames, haveNamesChanged] = addFromCollection(obj, otherCollection, ...
        %   nameSuffix, level, usedNames)
        % Add the nodes from the otherCollection to this collection at the specified level.   
        % Any nodes with the whose name clashes with one in this collection or with any of the 
        % names in usedNames will be given an new name inspired by the original name and the 
        % nameSuffix.  Returns the names of all the nodes that were added, their original names 
        % and a boolean flag indicating if the name has changed.
            validateattributes(otherCollection, {'parallel.internal.settings.TypedNodesCollection'}, {'scalar'});
            validateattributes(nameSuffix, {'char'}, {});
            validateattributes(level, {'parallel.internal.types.SettingsLevel'}, {'scalar'});
            if nargin < 5
                usedNames = {};
            elseif ~iscellstr(usedNames)
                error(message('parallel:settings:NamedNodesCollectionAddUsedNames'));
            end
            if isequal(obj, otherCollection)
                error(message('parallel:settings:TypedNodesCollectionAddFromSame'));
            end
                
            types          = keys(obj.NodeCollectionMap);
            otherTypes     = keys(otherCollection.NodeCollectionMap);
            differentTypes = setdiff(otherTypes, types);
            commonTypes    = intersect(otherTypes, types);
            if ~isempty(differentTypes)
                error(message('parallel:settings:TypedNodesCollectionAddDifferentTypes'));
            end

            addedNames       = cell(size(commonTypes));
            origNames        = cell(size(commonTypes));
            haveNamesChanged = cell(size(commonTypes));
            allNames         = obj.getAllNames();
            allUsedNames     = [usedNames(:); allNames(:)]';
            for ii = 1:numel(commonTypes)
                currType = commonTypes{ii};
                try
                    [addedNames{ii}, origNames{ii}, haveNamesChanged{ii}] = ...
                        obj.NodeCollectionMap(currType).addFromCollection(...
                            otherCollection.NodeCollectionMap(currType), nameSuffix, ...
                            level, allUsedNames);
                    allUsedNames = [allUsedNames, addedNames{ii}]; %#ok<AGROW>
                catch err
                    % Delete the nodes that have already been added
                    try
                        cellfun(@(x, y) obj.deleteNodesFromCollection(x, y), ...
                            commonTypes, addedNames);
                    catch err
                        warning(message('parallel:settings:TypedNodesAddFailedToDelete'));
                    end
                    rethrow(err);
                end
            end
            
            addedNames        = [addedNames{:}];
            origNames         = [origNames{:}];
            haveNamesChanged  = [haveNamesChanged{:}];
        end
    end
    
    methods (Access = private)
        function deleteNodesFromCollection(obj, type, names)
            collection = obj.NodeCollectionMap(type);
            collection.deleteNodes(names)
        end
    end
end

