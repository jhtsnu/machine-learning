
%   Copyright 2011 The MathWorks, Inc.

classdef NamedNodeEventData < event.EventData

    properties (SetAccess = immutable)
        NamedNode
    end
    
    methods
        function obj = NamedNodeEventData(namedNode)
            validateattributes(namedNode, ...
                {'parallel.internal.settings.NamedNode'}, {'scalar'});
            obj.NamedNode = namedNode;
        end
    end
end