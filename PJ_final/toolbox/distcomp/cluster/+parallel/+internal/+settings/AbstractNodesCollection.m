% AbstractNodesCollection - Base class for named nodes collections

%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden) AbstractNodesCollection < handle
    methods (Abstract)
        nodes = getAll(obj);
        namedNode = createNew(obj, varargin);
        tf = nameExists(obj, name);
        names = getAllNames(obj);
        node = getFromName(obj, name);
        deleteNodes(obj, names);
        deleteAllNodes(obj);
        [addedNames, origNames, haveNamesChanged] = addFromCollection(obj, ...
            otherCollection, nameSuffix, level, usedNames);
    end

    events
        NodeAdded
    end
    
    methods (Access = protected)
        function [name, hasNameChanged] = getUnusedName(obj, name, nameSuffix, otherUsedNames)
        % [name, hasNameChanged] = getUnusedName(obj, name, nameSuffix) - get 
        % an unused name, adding the nameSuffix to the original name if there
        % is a clash.
            validateattributes(name, {'char'}, {'row'});
            validateattributes(nameSuffix, {'char'}, {});
            validateattributes(otherUsedNames, {'cell'}, {});
            if ~iscellstr(otherUsedNames)
                error(message('parallel:settings:NamedNodesCollectionAddUsedNames'));
            end
            
            hasNameChanged = false;
            if obj.nameExists(name) || any(strcmp(name, otherUsedNames))
                proposedName = sprintf('%s%s', name, nameSuffix);
                allObjNames = obj.getAllNames();
                usedNames = [allObjNames(:); otherUsedNames(:)]';
                name = genvarname(proposedName, usedNames);
                hasNameChanged = true;
            end
        end
    end
end
