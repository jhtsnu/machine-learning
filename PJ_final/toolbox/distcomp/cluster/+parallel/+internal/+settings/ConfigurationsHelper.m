% ConfigurationsHelper - main entry point for all configurations related tasks.  
% Predominantly used by the configUI and also by distcomp.configuration to get 
% current configurations serializer (for backwards compatibility of Profiles).
%
% Use pctconfig('useprofiles', <true/false>) to switch between using (API2) 
% Profiles-backed configurations and (API1) preferences-backed configurations

% Copyright 2011 The MathWorks, Inc.
classdef (Sealed, Hidden) ConfigurationsHelper
    methods (Static)
        function name = createNew(type)
        % createNew(type) - create a new configuration with the
        % specified (API1) type.  Returns the name of the 
        % created configuration
            validateattributes(type, {'char'}, {'row'});
            name = distcomp.configuration.createNew(type);
        end
        
        function deleteConfig(name)
        % deleteConfig(name) - deletes the configuration with the
        % specified name
            validateattributes(name, {'char'}, {'row'});
            distcomp.configuration.deleteConfig(name);
        end

        function name = importFromFile(filename)
        % importFromFile(filename) - Import a configuration from
        % the specified file.  If 'useprofiles' is false in the pctconfig
        % then the file type must be a configurations .mat file.  
        % If 'useprofiles' is true, then the file can be either a 
        % configurations .mat file or a Profile .xml file.
            validateattributes(filename, {'char'}, {'row'});
            name = distcomp.configuration.importFromFile(filename);
        end

        function names = getAllNames()
        % getAllNames() - Get the names of all the configurations
            import parallel.internal.settings.ConfigurationsHelper;
            ser = ConfigurationsHelper.getCurrentSerializer();
            names = ser.getAllNames();
        end

        function ser = getCurrentSerializer()
        % getCurrentSerializer() - returns the correct serializer to use
        % based on the current 'useprofiles' option in pctconfig
            import parallel.internal.settings.PreferencesConfigSerializer
            import parallel.internal.settings.ProfilesConfigSerializer
            [~, undoc] = pctconfig();
            if undoc.useprofiles
                ser = ProfilesConfigSerializer.getInstance();
            else
                ser = PreferencesConfigSerializer.getInstance();
            end
        end

        function javaRef = getJavaReference(name)
        % getJavaReference(name) - get the Java reference for the configuration
        % with the supplied name
            validateattributes(name, {'char'}, {'row'});
            javaRef = distcomp.configuration.getJavaReference(name);
        end

        function addUndoStateListener(listener)
        % addUndoStateListener(listener) - add an undo listener (supported
        % for API1 only)
            import parallel.internal.settings.ConfigurationsHelper;
            ser = ConfigurationsHelper.getCurrentSerializer();
            ser.addUndoStateListener(listener);
        end
        
        function removeUndoStateListener(listener)
        % removeUndoStateListener(listener) - remove an undo listener (supported
        % for API1 only)
            import parallel.internal.settings.ConfigurationsHelper;
            ser = ConfigurationsHelper.getCurrentSerializer();
            ser.removeUndoStateListener(listener);
        end
        
        function redoAll()
        % redoAll - redo all actions (supported for API1 only)
            import parallel.internal.settings.ConfigurationsHelper;
            ser = ConfigurationsHelper.getCurrentSerializer();
            ser.redoAll();
        end

        function undoAll()
        % undoAll - undo all actions (supported for API1 only)
            import parallel.internal.settings.ConfigurationsHelper;
            ser = ConfigurationsHelper.getCurrentSerializer();
            ser.undoAll();
        end
    end
end

