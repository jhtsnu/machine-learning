% MJSComponentDisplayer - Displayer for MJS Component

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) MJSComponentDisplayer < parallel.internal.settings.NamedNodeDisplayer
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract NamedNodeDisplayer properties
        % NB Other MJS Props are defined in parallel.settings.schedulerComponent.MJS.hCustomizeDisplay
        CommonDisplayProps = {'Name', 'Type'};
        VectorDisplayProps = {'Name', 'Type', 'Host', 'Username'};
        VectorTableColumns = { ...
            % 'Title' , 'Resizeable', 'MinimumWidth'
            'Name',     true,         length('Name'); ...
            'Type',     false,        length('Type'); ...
            'Host',     true,         length('Host'); ...
            'Username', true,         length('Username'); ...
        }
        DisplayHelper = parallel.internal.display.DisplayHelper( length('DefaultTimeout') )
    end

    methods 
        function obj = MJSComponentDisplayer(someObj)
            obj@parallel.internal.settings.NamedNodeDisplayer(someObj, ...
                'parallel.settings.schedulerComponent.MJS');
        end
    end

    methods (Access = protected) % implementation for abstract NamedNodeDisplayer methods
        function displayProperties(obj, heading, propsToDisp)
            obj.DisplayHelper.displayMainHeading(heading);
            obj.DisplayHelper.displayProperty('Type', propsToDisp.Type);
        end

        function tableData = getTableData(obj, toDisp, getPropsFcn)
            tableData = cell(numel(toDisp), size(obj.VectorTableColumns, 1));
            for ii = 1:numel(toDisp) 
                currToDisp = toDisp(ii);
                propsToDisp = getPropsFcn(currToDisp, obj.VectorDisplayProps);
                tableData(ii, :) = {propsToDisp.Name, propsToDisp.Type, ...
                    propsToDisp.Host, propsToDisp.Username};
            end
        end
    end
end
