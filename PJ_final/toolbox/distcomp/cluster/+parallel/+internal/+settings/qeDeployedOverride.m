function tf = qeDeployedOverride(varargin)
% Back door for making it look as if we are in a deployed
% application

%   Copyright 2011 The MathWorks, Inc.

% NB This file must be mlocked to prevent clear functions
% from clearing the qeTestDeployed value.
mlock
persistent qeTestDeployed
narginchk(0, 1);
if isempty(qeTestDeployed)
    qeTestDeployed = false;
end

if nargin == 0
    tf = qeTestDeployed;
else
    validateattributes(varargin{1}, {'logical'}, {'scalar'});
    tf = qeTestDeployed;
    qeTestDeployed = varargin{1};
end
end
