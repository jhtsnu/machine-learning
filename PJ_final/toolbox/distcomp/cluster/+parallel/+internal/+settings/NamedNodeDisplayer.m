% NamedNodeDisplayer - base displayer class for all named nodes

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden) NamedNodeDisplayer < parallel.internal.settings.AbstractSettingsDisplayer
    properties (Abstract, SetAccess = immutable, GetAccess = protected)
        CommonDisplayProps
        VectorDisplayProps
        VectorTableColumns
    end

    methods % constructor
        function obj = NamedNodeDisplayer(someObj, requiredType)
            obj@parallel.internal.settings.AbstractSettingsDisplayer(someObj, requiredType);
        end
    end
    
    methods (Sealed, Access = protected) % implementation for abstract Displayer methods
        function doSingleDisplay(obj, toDisp)
            import parallel.internal.settings.AbstractSettingsDisplayer
            propsToDisp = AbstractSettingsDisplayer.getDisplayPropsAsStruct(toDisp, obj.CommonDisplayProps);
            heading = sprintf('%s Information', propsToDisp.Name);
            obj.displayProperties(heading, propsToDisp);
            obj.displaySpecificItems(toDisp);
        end
        
        function doVectorDisplay(obj, toDisp)
            import parallel.internal.settings.AbstractSettingsDisplayer
            getPropsFcn = @(x, y) AbstractSettingsDisplayer.getDisplayPropsAsStruct(x, y);
            headingText = obj.ClassDisplayName;
            obj.reallyDoVectorDisplay(toDisp, getPropsFcn, headingText);
        end
    end

    methods (Sealed, Access = protected) % implementation for abstract AbstractSettingsDisplayer methods
        function doSingleDisplayForLevel(obj, toDisp, level)
            import parallel.internal.settings.AbstractSettingsDisplayer
            propsToDisp = AbstractSettingsDisplayer.getDisplayPropsAsStruct(toDisp, obj.CommonDisplayProps, level);
            heading = sprintf('%s (%s) Information', propsToDisp.Name, level);
            obj.displayProperties(heading, propsToDisp);
            obj.displaySpecificItems(toDisp, level);
        end
        
        function doVectorDisplayForLevel(obj, toDisp, level)
            import parallel.internal.settings.AbstractSettingsDisplayer
            getPropsFcn = @(x, y) AbstractSettingsDisplayer.getDisplayPropsAsStruct(x, y, level);
            headingText = sprintf('%s (%s)', obj.ClassDisplayName, level);
            obj.reallyDoVectorDisplay(toDisp, getPropsFcn, headingText);
        end
    end
    
    methods (Sealed, Access = protected)
        function reallyDoVectorDisplay(obj, toDisp, getPropsFcn, headingText)
            tableData = obj.getTableData(toDisp, getPropsFcn);
            obj.DisplayHelper.displayDimensionHeading(size(toDisp), headingText);
            obj.DisplayHelper.displayTable(obj.VectorTableColumns, tableData, obj.DisplayHelper.DisplayIndexColumn);
        end
    end
    
    methods (Abstract, Access = protected)
        displayProperties(obj, heading, propsToDisp)
        tableData = getTableData(obj, toDisp, getPropsFcn)
    end
end