
%   Copyright 2010 The MathWorks, Inc.

classdef DisplayFormatter
% Helper class for formatting jobmanager related properties for display

    methods (Static)
        function runningDuration = formatRunningDuration(diffInMillis)
            % Formats the running duration given in milliseconds into a human readable
            % string.
            %
            % initialize outputs
            runningDuration = '';

            % Convert from milliseconds to seconds
            diffInSecs = floor(diffInMillis/1000);
            % Convert running duration to a readable format
            if diffInSecs > 0
                secPerMin = 60;
                secPerHour = 60*60;
                secPerDay = 24*secPerHour;

                days = floor(diffInSecs/secPerDay);
                diffInSecs = diffInSecs - days*secPerDay;

                hours = floor(diffInSecs/secPerHour);
                diffInSecs = diffInSecs - hours*secPerHour;

                mins = floor(diffInSecs/secPerMin);

                secs = diffInSecs - mins*secPerMin;
                runningDuration = sprintf('%d days %dh %dm %ds', days, hours, mins, secs);
            end
        end

        function workerName = formatWorkerName(name, hostname)
            % formats the worker name and hostname for display, truncating the worker
            % name in the case that it is long.

            nTruncate = 20;
            if numel(name) < nTruncate
                workerName = [name ' on ' hostname];
            else
                workerName = [name(1:nTruncate) '.. on ' hostname];
            end
        end
    end
end
