% MJSSupport - holds access proxies, invokes methods on them; caches MCOS
% job/task objects.

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) MJSSupport < handle

    properties ( Constant, Access = private )
        JavaEventIdToStateEnum = iBuildJavaEventIdToStateEnumMap();
    end

    properties ( Access = private )
        CachedName % The Name of the jobmanager, used as root key into Cache
        LookupURL  % The resolved(?) LookupURL of the jobmanager. Not needed?
        CertAlias  % The alias the certificate has been stored with       
    end
    
    properties ( Transient, Access = private)
        Cache      % TwoLevelCache for caching Job/Task objects and Workers
        JmAccess   % Access for 'jobmanager' properties/methods
        JobAccess  % Access for independent job properties/methods
        PJobAccess % Access for communicating job properties/methods
        TaskAccess % Access for Task properties / methods
        ProxyToUddAdaptor          % The event adaptor added to the JmAccess
        ProxyToUddAdaptorListener  % The Listener on the ProxyToUddAdaptor
    end

    methods ( Access = private )
        function obj = MJSSupport( jmAccess, certificateAlias )
            obj.initAccess( jmAccess );
            obj.Cache = parallel.internal.cluster.TwoLevelCache();
            if nargin > 1
                obj.CertAlias = certificateAlias;
            end
        end

        function initAccess( obj, jmAccess )
            try
                % Get the job/task accesses from the JmAccess. If these remote calls fail
                % we don't update anything in obj.
                jobAccess  = jmAccess.getJobAccess();
                pjobAccess = jmAccess.getParallelJobAccess();
                taskAccess = jmAccess.getTaskAccess();
            catch E
                % These are the first remote calls to the JM we've made, so
                % it's the opportunity to detect certificate problems.
                iHandlePossibleSSLHandshakeFailure( E, jmAccess );
            end

            % Dispose of any previous JmAccess
            obj.disposeAccess();

            % Now we have everything OK, update the fields
            obj.JmAccess   = jmAccess;
            obj.JobAccess  = jobAccess;
            obj.PJobAccess = pjobAccess;
            obj.TaskAccess = taskAccess;

            obj.CachedName = char( obj.JmAccess.getName() );
            obj.LookupURL  = char( obj.JmAccess.getLookupURL() );

            obj.initProxyToUddAdaptor();

            % Add our adaptor to the proxy's event forwarder
            obj.JmAccess.addEventAdaptor( obj.ProxyToUddAdaptor );
        end

        function initProxyToUddAdaptor(obj)
        % Get a UDD version of the java object that will dispatch callback events
        % into the UDD objects. This java object is exported such that the jobmanager
        % can send us events.
            obj.ProxyToUddAdaptor = handle(com.mathworks.toolbox.distcomp.uddadaptor.ProxyToUddAdaptor);
            % We need a mechanism to get the actual events from a java RMI call directly
            % into matlab - this is done via the java Bean interface of this java object
            % However we also need to be careful of the lifetime of this java object, so
            % We are very explicit in decoupling using listeners rather than matlab
            % Callbacks because this means that we don't store a reference to objectroot
            % in the actual java layer - and hence clear classes will continue to work
            obj.ProxyToUddAdaptorListener = handle.listener(...
                obj.ProxyToUddAdaptor, ...
                'ProxyToUddAdaptorEvent', ...
                {@iDispatchJavaEvent, obj});
            % Undocumented option to set the recursion limit on callbacks to 1024 - this
            % is a silly number that will be bounded by the RecursionLimit in MATLAB.
            % Hopefully our demos will continue to work provided the main RecursionLimit
            % is upped appropriately
            set(obj.ProxyToUddAdaptorListener, 'RecursionLimit', 1024);
        end

        function disposeAccess( obj )
            if ~isempty( obj.JmAccess )
                % We are no longer interested in any events
                obj.JmAccess.removeEventAdaptor( obj.ProxyToUddAdaptor );
                % Release reference to proxy
                obj.JmAccess.dispose();
                obj.JmAccess = [];
            end
        end

        function userIdentity = promptForUserIdentity( obj, defaultName )
            % Prompt for a new Username
            userIdentity = obj.JmAccess.promptForIdentity( defaultName );
        end

    end

    methods
        function delete( obj )
            obj.disposeAccess();
            iRemoveCertificate( obj.CertAlias );
        end

        function waitForMJS( obj, host, name )
        % Wait until the MJS cluster "name" looked up via "host" is responsive.
        % Don't use the LookupURL property as the lookup URL the proxy
        % knows about may not be the URL a client wants to use.
            waitInterval = 30;
            % We need to perform a new communications check each iteration.
            forceCheck = true;
            while ~obj.checkTwoWayCommunications( forceCheck )
                updated = obj.updateJmAccess( host, name );
                if updated
                    return;
                end
                pause( waitInterval )
            end
        end

        function updated = updateJmAccess( obj, host, name )
        % Attempt to update the JmAccess backing this object.
        % Returns true if update was successful.
        % Throws an error if a different cluster is found as
        % this means the cluster as restarted "-clean".
            updated = true;
            try
                % Try to get a new Proxy
                jmprx = parallel.internal.cluster.JobManagerFinder.getOneJobManagerProxy( host, name );
            catch E %#ok
                updated = false;
                return;
            end

            % Check we are still talking to the same jobmanager
            currentClusterID = obj.JmAccess.getID;
            newClusterID     = jmprx.getID;
            if ~isequal( currentClusterID, newClusterID )
                ex = MException(message('parallel:cluster:MJSChanged', char( obj.JmAccess.getName ), char( obj.JmAccess.getHostName )));
                throw( ex );
            end

            try
                % Create a new Access with the new proxy
                newAccess = obj.JmAccess.createNewClient( jmprx );
                % And (re-)initialize with the new access
                obj.initAccess( newAccess );
            catch E %#ok
                updated = false;
                return;
            end
        end
    end

    methods ( Static )
        % Build a MJSSupport from a proxy
        function support = buildFromProxy( jmprx, username )

            jmaccess = com.mathworks.toolbox.distcomp.mjs.client.JobManagerClient( jmprx );
            support  = parallel.internal.cluster.MJSSupport( jmaccess );

            userIdentity = support.promptForUserIdentity( username );
            support.setClusterProperties( 'Username', userIdentity );
        end

        % Build a MJSSupport object for distcomp_evaluate_task
        function support = buildOnWorker( jmprx, credentials )
            import com.mathworks.toolbox.distcomp.mjs.client.JobManagerClient;
            import com.mathworks.toolbox.distcomp.auth.credentials.consumer.CredentialConsumerFactory;
            import com.mathworks.toolbox.distcomp.auth.credentials.store.SingleUserCredentialStore;
            import com.mathworks.toolbox.distcomp.auth.credentials.CredentialRole;

            authToken = credentials.getCredentialsForRole(CredentialRole.AUTH_TOKEN);
            % On a Worker we don't want to use the default CredentialStore or CredentialConsumerFactory
            consumerFactory = CredentialConsumerFactory.TRIVIAL_FACTORY;
            credentialStore = SingleUserCredentialStore( authToken );
            jmaccess        = JobManagerClient( jmprx, consumerFactory, credentialStore );
            support         = parallel.internal.cluster.MJSSupport( jmaccess );
            username        = char( credentials.getUserIdentity.getSimpleUsername );

            if jmaccess.requireWebLicensing
                % NB if the desktop client was set up correctly in distcomp_evaluate_task
                % then this should never error because we'll already have the correct login
                % token from the credentials that were passed in.
                support.errorIfNotLoggedIn();
            end

            support.setClusterProperties('Username', username );
        end
        
        % Build a MJSSupport from a JobManagerClient. Used by tests.
        function support = buildFromClient( jmaccess )
            support = parallel.internal.cluster.MJSSupport( jmaccess );
        end

        % Build a MJSSupport
        function support = build( host, name, username, promptForPassword, certificate )
            import com.mathworks.toolbox.distcomp.mjs.client.JobManagerClient;
            % The certificate needs to be installed before the lookup is done.
            certAlias = iInstallCertificate( certificate );
            try
                jmprx    = parallel.internal.cluster.JobManagerFinder.getOneJobManagerProxy( host, name );
                jmaccess = JobManagerClient( jmprx );
                support  = parallel.internal.cluster.MJSSupport( jmaccess, certAlias );
                if jmaccess.requireWebLicensing
                    % NB This will actually error if the user doesn't log in.
                    support.errorIfNotLoggedIn();
                end
            catch E
                % If we haven't created a MJSSupport no one will remove
                % the certificate, so we do it here.
                iRemoveCertificate( certAlias );

                % Make it look like error came from the MJS constructor
                throwAsCaller( E );
            end

            userIdentity = support.promptForUserIdentity( username );

            support.setClusterProperties( ...
                {'Username',   'PromptForPassword'}, ...
                {userIdentity, promptForPassword } );
        end

    end

    methods ( Static, Access = private )
        % Wrappers around MJSMethods.performSets/performGets

        function v = wrapPerformGets( access, uuid, variant, propNameOrNames )
        % Simple wrapper around performGets to deal with vectorization of
        % propNameOrNames.
            import parallel.internal.cluster.MJSMethods

            vcell = iscell( propNameOrNames );
            if vcell
                propNames = propNameOrNames;
            else
                propNames = { propNameOrNames };
            end
            v = MJSMethods.performGets( access, uuid, variant, propNames );
            if ~vcell
                v = v{1};
            end
        end

        function wrapPerformSets( access, uuid, variant, propNameOrNames, valOrVals )
        % Simple wrapper around MJSMethods.performSets
            import parallel.internal.cluster.MJSMethods

            pcell = iscell( propNameOrNames );
            vcell = iscell( valOrVals );

            if pcell
                assert( vcell );
                propNames = propNameOrNames;
                values    = valOrVals;
            else
                % In this case, valOrVals might already be a cell -
                % e.g. AdditionalPaths.
                propNames = { propNameOrNames };
                values    = { valOrVals };
            end
            assert( numel( propNames ) == numel( values ) );

            MJSMethods.performSets( access, uuid, variant, propNames, values );
        end
    end

    methods % ClusterSupport
        function pauseQueue( obj )
            obj.JmAccess.pauseQueue();
        end

        function resumeQueue( obj )
            obj.JmAccess.resumeQueue();
        end

        function logout( obj, userIdentity )
            obj.JmAccess.revokeAuthentication( userIdentity );
        end

        function changePassword( obj, userIdentity )
            obj.JmAccess.changeCredentialsOfExistingUser( userIdentity );
        end

        function promoteJob( obj, jobSId )
            [~, jobUUID] = obj.unpackJobAccessInfo( jobSId );
            obj.JmAccess.promote( jobUUID(1) );
        end

        function demoteJob( obj, jobSId )
            [~, jobUUID] = obj.unpackJobAccessInfo( jobSId );
            obj.JmAccess.demote( jobUUID(1) );
        end

        function getClusterLogs( obj, saveLocation )
            import parallel.internal.apishared.FilenameUtils;
            % Use a temporary hidden file to prevent it appearing in the
            % desktop browser before it has been fully written.
            [saveDir, filename, ext] = fileparts( saveLocation );
            saveDir = FilenameUtils.getAbsolutePath( saveDir );
            if ~exist( saveDir, 'dir' )
                error(message('parallel:cluster:ClusterLogDirNotExist', saveDir));
            end
            if ~FilenameUtils.isWritable( saveDir );
                error(message('parallel:cluster:ClusterLogDirNotWritable', saveDir));
            end
            tempName = ['.' filename ext '.part'];
            tempSaveLocation = fullfile( saveDir, tempName );
            % The job manager zips its logs and writes to the given location.
            future = obj.JmAccess.getClusterLogs( tempSaveLocation );
            % Make sure the future is cancelled if there is an exception.
            % This is safe to call if the future has finished.
            futureCanceller = onCleanup( @()future.cancel(true) );
            % Allow the user to Ctrl-C
            while ~future.isDone
                pause( 1 );
            end
            % We need to call future.get even though there is no return value
            % so that we can propagate errors.
            future.get();
            if exist( tempSaveLocation, 'file' )
                movefile( tempSaveLocation, saveLocation );
            end
        end

        function w = getWorkers( obj, mjs, type )
        % getWorkers - type must be 'BusyWorkers' or 'IdleWorkers'.
        % Returns a list of workers of the given type.
        % Caches worker objects.
            import parallel.cluster.MJSWorker
            import parallel.internal.types.Variant
            import parallel.internal.cluster.MJSSupport

            validatestring( type, {'BusyWorkers', 'IdleWorkers'} );

            workerProxArray = MJSSupport.wrapPerformGets( obj.JmAccess, [], ...
                                                          Variant.Cluster, type );
            if numel( workerProxArray ) == 0
                w = MJSWorker.empty();
            else
                workersCell = cell( 1, length( workerProxArray ) );
                for ii = 1:length( workerProxArray )
                    workerUuidStr = char( workerProxArray(ii).getID() );
                    if ~obj.Cache.isKey( workerUuidStr )
                        workersCell{ii} = MJSWorker( mjs, workerProxArray(ii) );
                        obj.Cache.add( obj.CachedName, workerUuidStr, workersCell{ii} );
                    else
                        workersCell{ii} = obj.Cache.retrieve( workerUuidStr );
                    end
                end
                % Return a column (N-by-1) vector
                w = [ workersCell{:} ].';
            end
        end

        function val = getOperatingSystem( obj, mjs )
            import parallel.internal.apishared.OsType
            import parallel.internal.types.Variant
            import parallel.internal.cluster.MJSSupport
            
            allWorkers = [obj.getWorkers( mjs, 'BusyWorkers' ); obj.getWorkers( mjs, 'IdleWorkers' )];
            if isempty(allWorkers)
                % If there are no workers started, use the operating system
                % of the computer hosting the MJS 
                uniqueComputerTypes = MJSSupport.wrapPerformGets( obj.JmAccess, [], ...
                                                          Variant.Cluster, 'OperatingSystem' );
            else
                workerComputerTypes = arrayfun( @(w)w.ComputerType, allWorkers, 'UniformOutput', false );
                uniqueComputerTypes = unique( workerComputerTypes );
            end
            % This is same test 'ispc' uses.
            ispc                = arrayfun( @(c)strncmp( c, 'PC', 2 ), uniqueComputerTypes );
            if isempty( ispc )
                val = '';
            elseif all( ispc )
                val = OsType.PC.Api2Name;
            elseif ~any( ispc )
                val = OsType.UNIX.Api2Name;
            else
                val = OsType.MIXED.Api2Name;
            end
        end

        function val = getClusterProperties( obj, propNames )
        % Get properties of the jobmanager itself.

            import parallel.internal.cluster.MJSSupport
            import parallel.internal.types.Variant

            val = MJSSupport.wrapPerformGets( obj.JmAccess, [], ...
                                              Variant.Cluster, propNames );
        end

        function setClusterProperties( obj, propNames, propVals )
        % Set properties of the jobmanager itself.

            import parallel.internal.cluster.MJSSupport
            import parallel.internal.types.Variant

            MJSSupport.wrapPerformSets( obj.JmAccess, [], ...
                Variant.Cluster, ...
                propNames, propVals );
        end


        function certificate = getEncodedCertificate( obj )
            certificate = iGetEncodedCertificate( obj.CertAlias );
        end

        function val = requiresMathWorksHostedLicensing( obj )
            val = obj.JmAccess.requireWebLicensing();
        end
    end
    methods 
        function job = getJobFromUUID( obj, mjs, jobUUID, jobTypeOrVariant )
        % Used in all places that need to build a Job object.

            import parallel.internal.types.Variant

            %{
            validateattributes( mjs, {'parallel.cluster.MJS', 'parallel.cluster.MJSComputeCloud'}, {'scalar'} );
            validateattributes( jobUUID, {'net.jini.id.Uuid'}, {'scalar'} );
            validateattributes( jobTypeOrVariant, {'numeric', ...
                                'parallel.internal.types.Variant'}, {'scalar'} );
            %}

            uuidStr        = char( jobUUID );
            if obj.Cache.isKey( uuidStr )
                job        = obj.Cache.retrieve( uuidStr );
            else
                if isa( jobTypeOrVariant, 'parallel.internal.types.Variant' )
                    variant = jobTypeOrVariant;
                else
                    variants = iGetJobVariantForType( jobTypeOrVariant );
                    variant  = variants{1};
                end
                uuidArr    = javaArray( 'net.jini.id.Uuid', 1 );
                uuidArr(1) = jobUUID;
                jobSId     = { variant, uuidArr };
                jobId      = obj.getJobProperties( jobSId, 'ID' );
                job        = mjs.hBuildChild( variant, jobId, jobSId );

                obj.Cache.add( obj.CachedName, uuidStr, job );
            end
        end

        function task = getTaskFromUUID( obj, job, jobUUID, taskUUID )
        % Used by all places that need to build a Task.

        %{
            validateattributes( job, {'parallel.Job'}, {'scalar'} );
            validateattributes( jobUUID, {'net.jini.id.Uuid'}, {'scalar'} );
            validateattributes( taskUUID, {'net.jini.id.Uuid'}, {'scalar'} );
        %}
            taskUuidStr        = char( taskUUID );
            if obj.Cache.isKey( taskUuidStr )
                task           = obj.Cache.retrieve( taskUuidStr );
            else
                taskVariant    = parallel.internal.types.Variant.Task;
                jobUuidStr     = char( jobUUID );
                taskUuidArr    = javaArray( 'net.jini.id.Uuid', 1 );
                taskUuidArr(1) = taskUUID;
                taskSId        = { taskVariant, taskUuidArr };
                taskId         = obj.getTaskProperties( taskSId, 'ID' );
                task           = job.hBuildChild( taskId, taskSId );

                obj.Cache.add( jobUuidStr, taskUuidStr, task );
            end
        end

        function jl = getJobs( obj, mjs )
        % Return a list of jobs (or a roughly appropriately typed empty) for a
        % given jobmanager.

        %{
            validateattributes( mjs, {'parallel.cluster.MJS','parallel.cluster.MJSComputeCloud'}, {'scalar'} );
        %}
            typeAndIdArray = obj.JmAccess.getJobs();
            [uuids, types] = iGetIdAndType( typeAndIdArray );
            jobsCell       = cell( 1, length( uuids ) );

            for ii = 1:length( uuids )
                jobsCell{ii} = obj.getJobFromUUID( mjs, uuids(ii), types(ii) );
            end

            if isempty( jobsCell )
                % Choose one of the subclasses.
                jl = parallel.job.MJSIndependentJob.empty();
            else
                jl = [ jobsCell{:} ];
            end

        end
        
        function varargout = getJobsByState( obj, mjs )

        %{
            validateattributes( mjs, {'parallel.cluster.MJS','parallel.cluster.MJSComputeCloud'}, {'scalar'} );
        %}
            import com.mathworks.toolbox.distcomp.workunit.WorkUnit;

            states = [WorkUnit.PENDING_STATE, WorkUnit.QUEUED_STATE, ...
                WorkUnit.RUNNING_STATE, WorkUnit.FINISHED_STATE];

            % Get jobs in all states.
            typeAndIdArrays = obj.JmAccess.getJobs( states );
            uuids = cell( numel( states ), 1 );
            types = uuids;
            jobsCell = uuids;

            % Loop over each state and convert job UUIDs into job objects.
            for ii = 1:length( uuids )
                [uuids{ii}, types{ii}] = iGetIdAndType( typeAndIdArrays(ii) );
                thisUuid = uuids{ii};
                thisTypes = types{ii};
                jobsCell{ii} = cell( 1, length( uuids{ii} ) );
                for jj = 1:length( uuids{ii} )
                    jobsCell{ii}{jj} = obj.getJobFromUUID( mjs, thisUuid(jj), thisTypes(jj) );
                end
                if isempty( jobsCell{ii} )
                    jobsCell{ii} = { parallel.job.MJSIndependentJob.empty() };
                end
            end

            if nargout == 4
                varargout = jobsCell;
            else
                varargout = { jobsCell(:) };
            end

        end

        function [ p, q, r, f ] = enumerateJobsInStates( obj )
        % Return the number of jobs in Pending, Queued, Running and Finished states
        
            import com.mathworks.toolbox.distcomp.workunit.WorkUnit
        
            statesToRetrieve = [ WorkUnit.PENDING_STATE, WorkUnit.QUEUED_STATE, WorkUnit.RUNNING_STATE, WorkUnit.FINISHED_STATE ];
            JobIdsAndTypes = cell(obj.JmAccess.getJobs(statesToRetrieve));
            if isempty(JobIdsAndTypes)
                [p,q,r,f]=deal(0,0,0,0);
            else
                p = numel(JobIdsAndTypes{1});
                q = numel(JobIdsAndTypes{2});
                r = numel(JobIdsAndTypes{3});
                f = numel(JobIdsAndTypes{4});
            end
        end

        function job = buildJob( obj, mjs, variant, entitlement, optUserName )
        % Construct a brand new job given a job variant and optionally a
        % Username. Returns the job object.

            import parallel.internal.cluster.MJSSupport
            import parallel.internal.cluster.DefaultProperties

            jobProps                  = DefaultProperties.getNewJobPropStruct();
            jobProps.Timeout          = intmax( 'int64' );
            jobProps.MaxWorkers       = intmax('int32');
            jobProps.MinWorkers       = 1;
            jobProps.Restart          = false;
            jobProps.AttachedFileData = []; % Sadly, really does need to be double-empty
            jobProps.Name             = []; % Instruct java to calculate name
            
            %% Job owner
            %  Get userName from job arguments: check for input argument pair where the
            % key is 'UserName', get the username value and remove the pair from the
            % argument list (otherwise it would get set again later on).
            if nargin > 4
                jobProps.Username = optUserName;
            else
                %% If userName is unset try to set it to the user currently set in the
                %  proxy object (which must never be null or empty).
                userIdentity = obj.JmAccess.getCurrentUser();
                if isempty(userIdentity)
                    error(message('parallel:cluster:MJSInternalError'));
                end
                jobProps.Username = userIdentity.getSimpleUsername();
            end
            jobUUID = iBuildJob( jobProps, variant, obj.JmAccess, entitlement );
            job     = obj.getJobFromUUID( mjs, jobUUID, variant );
        end

        function [ok, msg] = checkTwoWayCommunications( obj, forceCheck )
        % c.f.: @distcomp/@jobmanager/pCheckTwoWayCommunications
            if nargin < 2
                forceCheck = true;
            end

            % JmAccess is a JobManagerClient, which wraps a JobManagerLocal.
            proxyObject = obj.JmAccess.getJobManager();

            if ~forceCheck && isa( proxyObject, 'com.mathworks.toolbox.distcomp.jobmanager.TestedJobManagerProxy' ) 
                testResult = proxyObject.getTwoWayCommunicationResults();
                ok = testResult.isSuccess();
            else
                % Creating a tested job manager proxy performs a two-way communication check.
                testedJM = com.mathworks.toolbox.distcomp.jobmanager.TestedJobManagerProxy.create( proxyObject );
                testResult = testedJM.getTwoWayCommunicationResults();
                ok = testResult.isSuccess();
            end
            if ok
                msg = '';
            else
                msg = char( testResult.getErrorMessage() );
            end
        end

        function connMgr = buildConnectionManager( obj, useSecureMatlabPool )
        % c.f.: @distcomp/@jobmanager/pCreateConnectionManager
            brokerConnectInfo        = obj.JmAccess.getBrokerServerSocketConnectInfo;
            workerAcceptInfoTemplate = obj.JmAccess.getWorkerAcceptInfoTemplate;

            connMgr = parallel.internal.apishared.ConnMgrBuilder.buildForMJS( ...
                brokerConnectInfo, workerAcceptInfoTemplate, ...
                useSecureMatlabPool );
        end

    end

    methods ( Access = private )
        function [variant, jobUUID, jobAccess] = unpackJobAccessInfo( obj, jobSId )
        % Helper for Job support methods to pick the right access.
            import parallel.internal.types.Variant

            variant = jobSId{1};
            jobUUID = jobSId{2};
            switch variant
              case Variant.IndependentJob
                jobAccess = obj.JobAccess;
              case { Variant.CommunicatingSPMDJob, ...
                     Variant.CommunicatingPoolJob }
                jobAccess = obj.PJobAccess;
              otherwise
                assert( false );
            end
        end
    end

    methods  % Callback support
        function registerForJobEvents( obj, mjs, jobSId )
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            try
                obj.JmAccess.attachToListenableObject( jobAccess, jobUUID );
                obj.stashCluster( mjs );
            catch err
                throw( distcomp.handleJavaException( mjs, err ) );
            end
        end
        function unregisterForJobEvents( obj, mjs, jobSId )
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            try
                obj.unstashCluster( mjs );
                obj.JmAccess.detachFromListenableObject( jobAccess, jobUUID );
            catch err %#ok<NASGU>
                % We should not treat unregistering for events as an error.
                % This can be triggered if someone else deletes a job or
                % task and we are still registered for eventing.
            end
        end
        function registerForTaskEvents( obj, mjs, taskSId )
            [~, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            try
                obj.JmAccess.attachToListenableObject( taskAccess, taskUUID );
                obj.stashCluster( mjs );
            catch err
                throw( distcomp.handleJavaException( mjs, err ) );
            end
        end
        function unregisterForTaskEvents( obj, mjs, taskSId )
            [~, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            try
                obj.unstashCluster( mjs );
                obj.JmAccess.detachFromListenableObject( taskAccess, taskUUID );
            catch err %#ok<NASGU>
                % We should not treat unregistering for events as an error.
                % This can be triggered if someone else deletes a job or
                % task and we are still registered for eventing.
            end
        end
        function stashCluster( ~, mjs )
            iCallbackStash( 'stash', mjs );
        end
        function unstashCluster( ~, mjs )
            iCallbackStash( 'unstash', mjs );
        end
    end

    methods % JobSupport
            % NB: 'jobSId' is a 2-cell containing { variant, jobUUID }
        function v = getJobProperties( obj, jobSId, propName )
            import parallel.internal.cluster.MJSSupport

            [variant, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            v = MJSSupport.wrapPerformGets( jobAccess, jobUUID, variant, propName );
        end

        function setJobProperties( obj, jobSId, propName, val )
            import parallel.internal.cluster.MJSSupport

            [variant, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            MJSSupport.wrapPerformSets( jobAccess, jobUUID, variant, propName, val );
        end

        function tf = isLeadingTask( obj, jobSId, taskSId )
        % cf:distcomp/job/pIsLeadingTask
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            taskUUID                = taskSId{2};
            tf = logical( jobAccess.isLeadingTask( jobUUID, taskUUID ) );
        end

        function cw = buildCancelWatchdog( obj, jobSId, msg, timeout )
        % Used during MJSCommunicatingJob connect/accept
            import com.mathworks.toolbox.distcomp.util.CancelWatchdog

            [~, jobUUID, jobaccess] = obj.unpackJobAccessInfo( jobSId );
            cw = CancelWatchdog( jobaccess, jobUUID, msg, timeout );
        end

        function tl = getTasks( obj, job, jobSId )
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            rawTaskIDs = jobAccess.getTasks( jobUUID );
            rawTaskIDs = rawTaskIDs(1); % Unpack first element of array
            if isempty( rawTaskIDs )
                tl = parallel.task.MJSTask.empty();
            else
                tasksCell = cell( 1, length( rawTaskIDs ) );
                for ii = 1:length( rawTaskIDs )
                    tasksCell{ii} = obj.getTaskFromUUID( job, jobUUID(1), rawTaskIDs(ii) );
                end
                tl = [ tasksCell{:} ];
            end
        end

        function tasks = buildTasks( obj, job, jobSId, taskInfoCell )
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );

            nTasks = numel( taskInfoCell );
            taskInfoArr = javaArray( ...
                'com.mathworks.toolbox.distcomp.workunit.TaskInfo', nTasks );
            for ii = 1:nTasks
                ti = taskInfoCell{ii};
                taskInfoArr(ii) = ti.Info;
            end
            jobUUIDs = javaArray('net.jini.id.Uuid', nTasks);
            jobUUIDs(:) = jobUUID(1);
            taskUUIDs = jobAccess.createTask( jobUUIDs, taskInfoArr );
            tasksc = cell( 1, nTasks );
            for ii = 1:nTasks
                tasksc{ii} = obj.getTaskFromUUID( job, jobUUID(1), taskUUIDs(ii) );
            end
            tasks = [ tasksc{:} ];
        end

        function listenerInfoArray = createListenerInfoArrayForAllEvents( obj )
            listenerInfoArray = obj.JmAccess.createListenerInfoArrayForAllEvents();
        end

        function cancelOneJob( obj, jobSId, cancelException )
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            jobAccess.cancel( jobUUID, cancelException.message );
        end

        function destroyOneJob( obj, jobSId )
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            jobAccess.ensureDestroyed( jobUUID );
            obj.Cache.remove( obj.CachedName, char( jobUUID ) );
        end

        function submitJob( obj, job, jobSId )
            [~, jobUUID, jobAccess] = obj.unpackJobAccessInfo( jobSId );
            mjsPrepareForSubmission( job, jobAccess, jobUUID );
            jobAccess.submit( jobUUID, [] );
        end

        function info = getJobDataStoreInfo( obj )
            info = iGetDataStoreInfo( obj.JobAccess );
        end
    end

    methods ( Access = private )
        function [variant, taskUUID, taskAccess] = unpackTaskAccessInfo( obj, taskSId )
            variant    = taskSId{1};
            taskUUID   = taskSId{2};
            taskAccess = obj.TaskAccess;
        end
        
        function errorIfNotLoggedIn( ~ )
            desktopClient = parallel.internal.webclients.currentDesktopClient();
            if ~desktopClient.isLoggedIn();
                error( message( 'parallel:cluster:ClusterRequiresWebLicensing' ) );
            end
        end
    end

    methods % TaskSupport
            % NB: 'taskSId' is a 2-cell containing { variant, taskUUID }
        function w = getWorkerForTask( obj, mjs, taskSId )
        % Retrieve the MJSWorker object for a task.
            import parallel.cluster.MJSWorker

            workerPropArray = obj.getTaskProperties( taskSId, 'WorkerProperties' );
            workerProp      = workerPropArray(1);

            if isempty( workerProp )
                w = MJSWorker.empty();
            else
                workerUuidStr = char( workerProp.getID() );
                if ~obj.Cache.isKey( workerUuidStr )
                    w = MJSWorker( mjs, workerProp );
                    obj.Cache.add( obj.CachedName, workerUuidStr, w );
                else
                    w = obj.Cache.retrieve( workerUuidStr );
                end
            end
        end

        function v = getTaskProperties( obj, taskSId, propName )
        % Get one or more properties of a task
            import parallel.internal.cluster.MJSSupport

            [variant, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            v = MJSSupport.wrapPerformGets( taskAccess, taskUUID, variant, propName );
        end

        function setTaskProperties( obj, taskSId, propName, val )
        % Set one or more properties of a task
            import parallel.internal.cluster.MJSSupport

            [variant, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            MJSSupport.wrapPerformSets( taskAccess, taskUUID, variant, ...
                                        propName, val );
        end

        function destroyOneTask( obj, jobSId, taskSId )
        % Destroy a given task
            [~, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            [~, jobUUID]              = obj.unpackJobAccessInfo( jobSId );
            taskAccess.ensureDestroyed( taskUUID );
            obj.Cache.remove( char( jobUUID ), char( taskUUID ) );
        end

        function cancelOneTask( obj, taskSId, cancelException )
            [~, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            taskAccess.cancel( taskUUID, cancelException.message );
        end

        function rerunOrCancelOneTask( obj, taskSId, cancelException )
            [~, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            taskAccess.rerunOrCancel( taskUUID, cancelException.message );
        end

        function submitTaskResult( obj, taskSId, outputItemArray, ...
                                   errorBytes, error_message, error_id, ...
                                   diaryItemArray )
            % Submit the results following task execution
            [~, taskUUID, taskAccess] = obj.unpackTaskAccessInfo( taskSId );
            taskAccess.submitResult( taskUUID, outputItemArray, errorBytes, ...
                                     error_message, error_id, diaryItemArray );
        end

        function info = getTaskDataStoreInfo( obj )
            info = iGetDataStoreInfo( obj.TaskAccess );
        end
    end
end


% --------------------------------------------------------------------------
% Given a structure containing all the job properties we need, a variant and the
% jmAccess, actually invoke the appropriate create*Job on the access to build a
% new job. Return the new job's UUID.
function jobUUID = iBuildJob( jobProps, variant, jmAccess, entitlement )
    import com.mathworks.toolbox.distcomp.workunit.JobInfo
    import com.mathworks.toolbox.distcomp.workunit.JobMLType
    import parallel.internal.types.Variant

    switch variant
      case Variant.IndependentJob
        jobProps.Type = JobMLType.CLUSTER_STANDARD_JOB;
      case Variant.CommunicatingSPMDJob
        jobProps.Type = JobMLType.CLUSTER_PARALLEL_JOB;
      case Variant.CommunicatingPoolJob
        jobProps.Type = JobMLType.CLUSTER_MATLABPOOL_JOB;
      otherwise
        assert( false );
    end

    jobInfo = JobInfo( jobProps.Name, jobProps.Timeout, jobProps.Tag, ...
                       jobProps.MaxWorkers, jobProps.MinWorkers, ...
                       jobProps.Restart, jobProps.AutoAttachFiles, ...
                       jobProps.DependentFiles, jobProps.AttachedFiles, ...
                       jobProps.AttachedFilePaths, jobProps.AdditionalPaths, ...
                       jobProps.ProductKeys, jobProps.AttachedFileData, ...
                       jobProps.JobData, jobProps.Type, jobProps.ExecutionMode, ...
                       jobProps.Username, [] );

    if jmAccess.requireWebLicensing
        jobInfo = iAddWebLicensingInfo( jobInfo, entitlement );
    end

    switch variant
      case Variant.IndependentJob
        jobUUID = jmAccess.createJob( jobInfo );
      case Variant.CommunicatingSPMDJob
        jobUUID = jmAccess.createParallelJob( jobInfo );
      case Variant.CommunicatingPoolJob
        serializedNopFunction = distcompserialize(@distcomp.nop);
        serializedEmptyCell   = distcompserialize({});
        jobUUID = jmAccess.createMatlabPoolJob( ...
            jobInfo, serializedNopFunction, serializedEmptyCell);
      otherwise
        assert( false );
    end
end

% --------------------------------------------------------------------------
% Add web licensing information to a job info
function jobInfo = iAddWebLicensingInfo(jobInfo, entitlement)
    import com.mathworks.toolbox.distcomp.auth.credentials.WebLicenseCredentials;
    import com.mathworks.toolbox.distcomp.auth.credentials.store.SimpleCredentialProvider;
    import com.mathworks.toolbox.distcomp.auth.credentials.UserIdentity;
    userIdentity = UserIdentity( jobInfo.getUserName );
    
    desktopClient = parallel.internal.webclients.currentDesktopClient();
    loginToken = desktopClient.LoginToken;
    userName = desktopClient.UserName;
    webId = entitlement.Id;
    licenseNumber = entitlement.LicenseNumber;
    % Create a web license credentials object to pass securely to the job manager.
    licenseInfo = WebLicenseCredentials(userIdentity, userName, ...
        loginToken, webId, licenseNumber);
    credentialProvider = SimpleCredentialProvider(licenseInfo);
    jobInfo.setLocalCredentialProvider(credentialProvider);
end

% --------------------------------------------------------------------------
% Return a java array of UUIDs and a double-array of numeric type values given a
% typeAndIdArray as returned by the JmAccess.getJobs() method.
function [uuids, types] = iGetIdAndType(typeAndIdArray)
    numJobs = numel(typeAndIdArray);
    if numJobs > 0
        % Create the UUID array to be of the correct type
        uuids = javaArray('net.jini.id.Uuid', numJobs);
    else
        % Return a java null which is the empty array
        uuids = [];
    end
    types = zeros(size(uuids));
    for i = 1:numJobs
        uuids(i) = typeAndIdArray(i).getJobID;
        types(i) = typeAndIdArray(i).getJobType;
    end
end

% --------------------------------------------------------------------------
% iGetJobVariantForType - given a list of types as returned by the JmAccess,
% convert that to a cell array of Variant.
function vars = iGetJobVariantForType( types )
    import parallel.internal.types.Variant
    import com.mathworks.toolbox.distcomp.workunit.JobMLType

    vars = cell( 1, length( types ) );
    for ii = 1:length( types )
        if JobMLType.isStandardJob( types(ii) )
            vars{ii} = Variant.IndependentJob;
        elseif JobMLType.isParallelJob( types(ii) )
            vars{ii} = Variant.CommunicatingSPMDJob;
        elseif JobMLType.isMatlabpoolJob( types(ii) )
            vars{ii} = Variant.CommunicatingPoolJob;
        else
            assert( false );
        end
    end
end


% --------------------------------------------------------------------------
% iInstallCertificate - install the certificate into the certificate store
% and return the alias used to identify it.
function certificateAlias = iInstallCertificate( certificate )
    import com.mathworks.toolbox.distcomp.mjs.security.ClientCertificateStore

    certificateAlias = '';
    if ~isempty( certificate )
        if exist( certificate, 'file' )
            certificate = java.io.File( certificate );
        end
        certificateAlias = char( ClientCertificateStore.getInstance.putCertificate( certificate ) );
    end
end

% --------------------------------------------------------------------------
% iGetEncodedCertificate - get the base64 encoded certificate from the
% certificate store.
function encodedCertificate = iGetEncodedCertificate( certificateAlias )
    import com.mathworks.toolbox.distcomp.mjs.security.ClientCertificateStore
    encodedCertificate = '';
    if ~isempty( certificateAlias )
        encodedCertificate = char( ClientCertificateStore.getInstance.getEncodedCertificateEntry( certificateAlias ) );
    end
end

% --------------------------------------------------------------------------
% iRemoveCertificate - remove the certificate identified by the alias from
% the certificate store.
function iRemoveCertificate( certificateAlias )
    import com.mathworks.toolbox.distcomp.mjs.security.ClientCertificateStore
    if ~isempty( certificateAlias )
        ClientCertificateStore.getInstance.removeCertificateEntry( certificateAlias );
    end
end

% --------------------------------------------------------------------------
% iDispatchJavaEvent - the callback for the ProxyToUddAdaptorListener
function iDispatchJavaEvent(src, event, supportObj)

    % Get the actual event from the UDD wrapped event
    event = event.JavaEvent;

    % Make sure that once this function is finished we let the java layer
    % know that we are finished
    o = onCleanup(@() src.matlabEventFinished(event));
    try
        uuidStr = char( event.getSource );
        if supportObj.Cache.isKey( uuidStr )
            jobOrTask = supportObj.Cache.retrieve( uuidStr );
            stateEnum = supportObj.JavaEventIdToStateEnum( event.getID );
            iFireStateEvent( jobOrTask, stateEnum );
        end
    catch err
        % Do nothing - swallow the error silently
        dctSchedulerMessage(5, 'Error dispatching event. %s', err.getReport);
    end
end

% --------------------------------------------------------------------------
% iFireStateEvent - fire the state event
function iFireStateEvent( jobOrTask, stateEnum )

% If the callback throws an error the warning contains whatever is in
% lasterror unless it has been reset. See G776395.
lastError        = lasterror( 'reset' );                   %#ok<LERR>
restoreLastError = onCleanup( @()lasterror( lastError ) ); %#ok<LERR>

% Fire the event with a clean last error.
jobOrTask.hFireStateEvent( stateEnum );

end

% --------------------------------------------------------------------------
% iBuildJavaEventIdToStateEnumMap - build the map used to look up the State
% enum value for a Java event ID.
function m = iBuildJavaEventIdToStateEnumMap()
    import com.mathworks.toolbox.distcomp.distcompobjects.DistcompListenable;
    import parallel.internal.types.States;
    m = containers.Map('KeyType', 'double', 'ValueType', 'any');
    m(DistcompListenable.EVENT_QUEUED_STATE) = States.Queued;
    m(DistcompListenable.EVENT_RUNNING_STATE) = States.Running;
    m(DistcompListenable.EVENT_FINISHED_STATE) = States.Finished;
end

% --------------------------------------------------------------------------
% iCallbackStash - stash/unstash clusters that have active callbacks
function iCallbackStash( action, cluster )
    assert( isa( cluster, 'parallel.Cluster' ), ...
        sprintf( 'Expected a parallel.Cluster but got a %s', class( cluster ) ) );

    persistent STASHED_CLUSTERS
    if isempty( STASHED_CLUSTERS )
        STASHED_CLUSTERS = parallel.internal.cluster.CountedStash();
    end
    switch action
      case 'stash'
        c = STASHED_CLUSTERS.stash( cluster ); %#ok<NASGU>
      case 'unstash'
        c = STASHED_CLUSTERS.unstash( cluster ); %#ok<NASGU>
    end
end

%--------------------------------------------------------------------------
% Detects SSLHandshake errors, and therefore problems with the certificate.
function iHandlePossibleSSLHandshakeFailure( E, jmprx )
    [isJavaEx, exceptionType, causes] = isJavaException( E );
    if isJavaEx ...
            && strcmp( exceptionType, 'java.rmi.ConnectIOException' ) && ~isempty( causes ) ...
            && strcmp( causes{1}, 'javax.net.ssl.SSLHandshakeException' )
        error(message('parallel:cluster:SSLHandshakeFailed', char( jmprx.getName ), char( jmprx.getHostName )));
    else
        rethrow( E );
    end
end

%-------------------------------------------------------------------------
% Get data store info back from a job or task access object
function info = iGetDataStoreInfo( access )
    info = struct('DataStoreSize', NaN, 'DataStoreExportPort', NaN);
    info.DataStoreSize = access.getDataStoreSize;
    info.DataStoreExportPort = access.getDataStoreExportPort;
end
