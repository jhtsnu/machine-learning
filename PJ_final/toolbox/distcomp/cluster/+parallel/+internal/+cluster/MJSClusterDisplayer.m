% MJSClusterDisplayer - Displayer subclass to display MJS objects.

%   Copyright 2011 The MathWorks, Inc.

classdef ( Hidden, Sealed ) MJSClusterDisplayer < parallel.internal.cluster.ClusterDisplayer

    methods
        function obj = MJSClusterDisplayer(someObj)
            obj@parallel.internal.cluster.ClusterDisplayer(someObj, class(someObj));
        end
    end

    methods (Access = private)
        function doInvalidSingleDisplay( obj, invalidToDisp )
            obj.DisplayHelper.displayMainHeading('Invalid %s Cluster Information', invalidToDisp.Type);
            obj.DisplayHelper.displayProperty('Type', invalidToDisp.Type);
            obj.DisplayHelper.displayProperty('State', invalidToDisp.State);
        end
    end

    methods (Access = protected)
        function doSingleDisplay(obj, toDisp)
            import parallel.internal.types.States;

            if ~States.isValidState( toDisp.State )
                % Do not attempt to access any further info.
                obj.doInvalidSingleDisplay( toDisp );
                return
            end

            doSingleDisplay@parallel.internal.cluster.ClusterDisplayer( obj, toDisp );
        end
    end
    
    methods( Access = protected )
        function [p, q, r, f] = getNumJobs(~,toDisp)
            [p, q, r, f] = toDisp.hEnumerateJobsInStates();
        end
    end


end
