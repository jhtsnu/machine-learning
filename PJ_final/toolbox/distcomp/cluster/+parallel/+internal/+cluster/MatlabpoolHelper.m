classdef MatlabpoolHelper
% Helper class for activities related to matlabpool

%  Copyright 2010-2012 The MathWorks, Inc.

    properties (Constant)
        % The valid actions for matlabpool.  Be sure to update the
        % getAllActions() method if this list of actions changes.
        OpenAction = 'open';
        CloseAction = 'close';
        SizeAction = 'size';
        ExitLeaveGuiOpenAction = 'exitleaveguiopen';
        AddFileDependenciesAction = 'addattachedfiles';
        UpdateFileDependenciesAction = 'updateattachedfiles';

        FoundNoParseActionErrorIdentifier = 'parallel:lang:matlabpool:FoundNoParseAction';
    end

    properties (Constant, GetAccess = private)
        DefaultActionArgs = struct('Configuration', '', ...
            'UserSuppliedConfiguration', false, ...
            'Scheduler', []);
        DefaultOpenArgs = parallel.internal.cluster.MatlabpoolHelper.getDefaultOpenArgs();
        DefaultCloseArgs = parallel.internal.cluster.MatlabpoolHelper.getDefaultCloseArgs();
        DefaultAddFileDependenciesArgs = parallel.internal.cluster.MatlabpoolHelper.getDefaultAddFileDependenciesArgs();
        Converter = parallel.internal.apishared.ConversionContext.Matlabpool;
    end

    methods (Static, Access = private)
        % Methods to support constant properties above
        function openArgs = getDefaultOpenArgs()
            import parallel.internal.cluster.MatlabpoolHelper;
            openArgs = MatlabpoolHelper.DefaultActionArgs;
            openArgs.NumLabs = [];
            openArgs.FileDependencies = {};
        end
        function closeArgs = getDefaultCloseArgs()
            import parallel.internal.cluster.MatlabpoolHelper;
            closeArgs = MatlabpoolHelper.DefaultActionArgs;
            closeArgs.IsForce = false;
        end
        function addFileArgs = getDefaultAddFileDependenciesArgs()
            import parallel.internal.cluster.MatlabpoolHelper;
            addFileArgs = MatlabpoolHelper.DefaultActionArgs;
            addFileArgs.ListOfFiles = {};
        end
    end

    methods (Static)
        % -------------------------------------------------------------------------
        % getAllActions
        % -------------------------------------------------------------------------
        function validActions = getAllActions()
            import parallel.internal.cluster.MatlabpoolHelper;
            validActions = {MatlabpoolHelper.OpenAction, ...
                MatlabpoolHelper.CloseAction, ...
                MatlabpoolHelper.SizeAction, ...
                MatlabpoolHelper.ExitLeaveGuiOpenAction, ...
                MatlabpoolHelper.AddFileDependenciesAction, ...
                MatlabpoolHelper.UpdateFileDependenciesAction};
            validApi1 = MatlabpoolHelper.Converter.convertToApi1( validActions );
            validActions = unique( [validActions(:); validApi1(:)] );
        end

        % -------------------------------------------------------------------------
        % resolveAction - ensure any API1 actions are converted back to API2
        % -------------------------------------------------------------------------
        function action = resolveAction( action )
            import parallel.internal.cluster.MatlabpoolHelper;

            action = MatlabpoolHelper.Converter.convertToApi2( action );
        end

        % -------------------------------------------------------------------------
        % openMatlabpoolForScheduler
        % -------------------------------------------------------------------------
        % Quick way of opening matlabpool when we know exactly which
        % scheduler to use and we have no other arguments to specify.
        % Used from checkConfigOk
        function openMatlabpoolForScheduler(sched)
            import parallel.internal.cluster.MatlabpoolHelper;
            MatlabpoolHelper.doOpen(sched, MatlabpoolHelper.DefaultOpenArgs);
        end

        % -------------------------------------------------------------------------
        % parseInputsAndCheckOutputs
        % -------------------------------------------------------------------------
        % Parse input arguments from matlabpool the function and check that the
        % number of output arguments is valid.
        function parsedArgs = parseInputsAndCheckOutputsForFunction( ...
            profHelper, nRequestedArgsOut, varargin)

            import parallel.internal.cluster.MatlabpoolHelper;

            allowedActions = MatlabpoolHelper.getAllActions();

            testAndReturnConfigurationFcn = ...
                @(x) MatlabpoolHelper.checkConfigOk(profHelper, x);

            parsedArgs = MatlabpoolHelper.parseMatlabpoolInputs( ...
                profHelper, allowedActions, testAndReturnConfigurationFcn, varargin{:});

            MatlabpoolHelper.checkMatlabpoolNargout(parsedArgs, nRequestedArgsOut);
        end
        % -------------------------------------------------------------------------
        % parseInputsAndCheckOutputs
        % -------------------------------------------------------------------------
        % Parse input arguments from matlabpool the cluster/scheduler method and
        % check that the number of output arguments is valid.
        function parsedArgs = parseInputsAndCheckOutputsForMethod( ...
            profHelper, nRequestedArgsOut, varargin)

            import parallel.internal.cluster.MatlabpoolHelper;

            allowedActions = { MatlabpoolHelper.OpenAction };
            testAndReturnConfigurationFcn = ...
                @(x) MatlabpoolHelper.checkNoConfigSupplied(profHelper, x);

            parsedArgs = MatlabpoolHelper.parseMatlabpoolInputs( ...
                profHelper, allowedActions, testAndReturnConfigurationFcn, varargin{:});
            MatlabpoolHelper.checkMatlabpoolNargout(parsedArgs, nRequestedArgsOut);
        end

        % -------------------------------------------------------------------------
        % doMatlabpool
        % -------------------------------------------------------------------------
        % Actually run the relevant matlabpool command.  sched is only required if
        % parsedArgs.Action is OpenAction.
        function argsout = doMatlabpool(parsedArgs, sched)
            import parallel.internal.cluster.MatlabpoolHelper;
            argsout = {};
            switch parsedArgs.Action
              case MatlabpoolHelper.OpenAction
                assert(~isempty(sched), 'parallel:lang:matlab:NoSchedulerSupplied', ...
                       'A scheduler must be supplied for matlabpool %s.', ...
                       MatlabpoolHelper.OpenAction);
                MatlabpoolHelper.doOpen(sched, parsedArgs.ActionArgs);
              case MatlabpoolHelper.CloseAction
                MatlabpoolHelper.doClose(sched, parsedArgs.ActionArgs);
              case MatlabpoolHelper.SizeAction
                argsout{1} = MatlabpoolHelper.doSize();
              case MatlabpoolHelper.AddFileDependenciesAction
                MatlabpoolHelper.doAddFileDependencies(parsedArgs.ActionArgs);
              case MatlabpoolHelper.UpdateFileDependenciesAction
                MatlabpoolHelper.doUpdateFileDependencies();
              case MatlabpoolHelper.ExitLeaveGuiOpenAction % Undocumented.
                MatlabpoolHelper.doExitLeaveGuiOpen();
              otherwise
                error(message('parallel:lang:matlabpool:InvalidAction', parsedArgs.Action));
            end
        end

        function [tf, sz, id] = isPoolRunningOnEquivalentCluster( cluster )
        % isPoolRunningOnThisCluster TRUE if an interactive pool for this
        % session is open on a cluster equivalent to the supplied cluster.
        % NB: the test is for EQUIVALENCE (via hEquivalentWorkers).
        % Returns:
        % tf - TRUE if pool is running on an equivalent cluster.
        % sz - size of pool (valid only when TF == TRUE)
        % id - job ID of pool (valid only when TF == TRUE)
            import parallel.internal.cluster.MatlabpoolHelper
            tf = false;
            sz = MatlabpoolHelper.doSize();
            id = -1;
            if sz > 0
                client = parallel.internal.pool.SessionManager.getSessionObject();
                pjob   = client.ParallelJob;
                id     = pjob.ID;
                mpclus = pjob.Parent;
                tf     = isa( cluster, 'parallel.Cluster' ) && ... % Only implemented for API2
                         isa( mpclus, 'parallel.Cluster' ) && ...
                         hEquivalentWorkers( cluster, mpclus );
            end
        end
        
        function maybeUpdateAttachedFiles(files)
            if ~iscell(files)
                files = {files};
            end

            % Get the FileDependenciesAssistant from the current session
            fda = iGetFileDependenciesAssistant();
            [~, ~, ~, jFilesToUpdate] = iGetJavaFilesToAddAndUpdate(fda, files);
            if isempty(jFilesToUpdate)
                return;
            end

            % Only update those files that have already been added
            changedFilesMap = fda.findSpecificChangedFiles(pwd, jFilesToUpdate);
            iSendChangedFilesToWorkers(changedFilesMap, fda);
        end
        
        function addOrUpdateAttachedFiles(files)
            if ~iscell(files)
                files = {files};
            end
            % Get the FileDependenciesAssistant from the current session
            fda = iGetFileDependenciesAssistant();
            [filesToAdd, ~, ~, jFilesToUpdate] = iGetJavaFilesToAddAndUpdate(fda, files);

            if ~isempty(filesToAdd)
                parallel.internal.cluster.MatlabpoolHelper.addCanonicalizedFiles(filesToAdd);
            end
            
            if ~isempty(jFilesToUpdate)
                % Only update those files that have already been added
                changedFilesMap = fda.findSpecificChangedFiles(pwd, jFilesToUpdate);
                iSendChangedFilesToWorkers(changedFilesMap, fda);
            end
        end
    end

    methods (Static, Access = private) % Methods that actually run the matlabpool stuff
        % -------------------------------------------------------------------------
        % parseMatlabpoolInputs
        % -------------------------------------------------------------------------
        % testAndReturnConfigurationFcn is a function handle that takes in a
        % configuration name and returns the configuration name to use (if a
        % configuration is allowed), and errors if the configuration is not allowed.
        function parsedArgs = parseMatlabpoolInputs(...
            profHelper, actionsToParse, testAndReturnConfigurationFcn, varargin)
            import parallel.internal.cluster.MatlabpoolHelper;
            % Ensure that actionsToParse is a cell array of strings
            if ischar(actionsToParse)
                actionsToParse = cellstr(actionsToParse);
            end

            validActions = MatlabpoolHelper.getAllActions();

            assert(all(ismember(actionsToParse, validActions)));

            % Want a way to distinguish between a supplied action and an inferred
            % action (where the first argument isn't a validAction and so open is
            % deduced)
            actionFound = true;
            % No action at all means it defaults to 'open'
            if numel(varargin) == 0
                action = MatlabpoolHelper.OpenAction;
                actionFound = false;
            else
                action = varargin{1};
                % Now check if the first element of varargin is a valid action
                if ischar(action) && (size(action, 2) == numel(action)) && any(strcmpi(action, validActions))
                    % Remove varargin{1} because it was a valid action
                    varargin(1) = [];
                else
                    % If it isn't then set the action to be start
                    action = MatlabpoolHelper.OpenAction;
                    actionFound = false;
                end
            end

            % Ensure we resolve the action back to one of the MatlabpoolHelper
            % Constant values.
            action = MatlabpoolHelper.resolveAction(lower(action));

            % Error here if the action we identified is not one of the ones
            % we wish to continue parsing
            if ~any(strcmpi(action, actionsToParse))
                error(message(MatlabpoolHelper.FoundNoParseActionErrorIdentifier, action));
            end

            actionArgs = MatlabpoolHelper.DefaultActionArgs;

            switch action
              case MatlabpoolHelper.OpenAction
                MatlabpoolHelper.errorIfNotOnClient(action);
                actionArgs = MatlabpoolHelper.parseOpenArgs(actionFound, actionsToParse, varargin{:});

                % do the configuration test - this will error if the config doesn't pass the test
                actionArgs.Configuration = testAndReturnConfigurationFcn(actionArgs.Configuration);

                % We have a message for each of 'profile' and 'configuration', so use the property
                % name to access the message (which requires capitalizing the property name.)
                profilePropertyName = profHelper.PropertyName;
                if strcmp( profilePropertyName, 'profile' )
                    startingWithMsgId = 'parallel:convenience:StartingWithProfile';
                elseif strcmp( profilePropertyName, 'configuration' )
                    startingWithMsgId = 'parallel:convenience:StartingWithConfiguration';
                else
                    assert( false, ['Unknown profile property name: ' profilePropertyName] );
                end

                % Get the scheduler from the configuration, if available
                if ~isempty(actionArgs.Configuration)
                    fprintf( '%s ', getString( message( startingWithMsgId, ...
                        'matlabpool', actionArgs.Configuration ) ) );
                    try
                        % Get the scheduler from the configuration
                        actionArgs.Scheduler = profHelper.buildScheduler( actionArgs.Configuration );
                    catch E
                        % Make sure we print a newline before displaying the error message.
                        fprintf( '\n' );
                        rethrow( E );
                    end
                else
                    fprintf( '%s ', getString( message( 'parallel:convenience:StartingNoProfile', 'matlabpool' ) ) );
                end

              case MatlabpoolHelper.CloseAction
                MatlabpoolHelper.errorIfNotOnClient(action);
                actionArgs = MatlabpoolHelper.parseCloseArgs(varargin{:});

                % do the configuration test - this will error if the config doesn't pass the test
                actionArgs.Configuration = testAndReturnConfigurationFcn(actionArgs.Configuration);

                % Get the scheduler from the configuration if we're doing close force
                if actionArgs.IsForce
                    actionArgs.Scheduler = profHelper.buildScheduler( actionArgs.Configuration );
                end

              case MatlabpoolHelper.SizeAction
                MatlabpoolHelper.errorIfArgs(action, numel(varargin));

              case MatlabpoolHelper.AddFileDependenciesAction
                MatlabpoolHelper.errorIfNotOnClient(action);
                MatlabpoolHelper.errorIfNotSingleCellString(action, varargin{:});

                actionArgs = MatlabpoolHelper.DefaultAddFileDependenciesArgs;
                actionArgs.ListOfFiles = varargin{1};

              case MatlabpoolHelper.UpdateFileDependenciesAction
                MatlabpoolHelper.errorIfNotOnClient(action);
                % UpdateFileDependencies takes no additional arguments.
                MatlabpoolHelper.errorIfArgs(action, numel(varargin));

              case MatlabpoolHelper.ExitLeaveGuiOpenAction % Undocumented.
                MatlabpoolHelper.errorIfNotOnClient(action);
                % exitleaveguiopen takes no additional arguments.
                MatlabpoolHelper.errorIfArgs(action, numel(varargin));

              otherwise
                error(message('parallel:lang:matlabpool:InvalidAction', action));
            end

            parsedArgs.Action = action;
            parsedArgs.ActionArgs = actionArgs;
        end

        % ------------------------------------------------------------
        % checkMatlabpoolNargout
        % ------------------------------------------------------------
        % Check that the nargout requested by the user is appropriate.
        function checkMatlabpoolNargout( parsedArgs, matlabpoolNargout )
            import parallel.internal.cluster.MatlabpoolHelper;
            switch parsedArgs.Action
              case MatlabpoolHelper.SizeAction
                maxnargout = 1;
              otherwise
                maxnargout = 0;
            end
            if matlabpoolNargout > maxnargout
                error(message('parallel:lang:matlabpool:TooManyOutputArguments', parsedArgs.Action));
            end
        end

        % -------------------------------------------------------------------------
        % doOpen
        % -------------------------------------------------------------------------
        % Opens a matlabpool on the supplied scheduler
        function doOpen(sched, parsedOpenArgs)
            client = parallel.internal.pool.SessionManager.getSessionObject();
            client.start('matlabpool', parsedOpenArgs.NumLabs, sched, 'nogui', parsedOpenArgs.FileDependencies);
            dctPathAndClearNotificationGateway('on');
            % Endeavor to force the remote workers to pick-up the same working
            % directory as we are in - this is used for all schedulers other than
            % the local one, which starts out in the right place.
            cd(pwd);
        end

        % -------------------------------------------------------------------------
        % doClose
        % -------------------------------------------------------------------------
        function doClose(sched, parsedCloseArgs)
            import parallel.internal.cluster.MatlabpoolHelper

            client = parallel.internal.pool.SessionManager.getSessionObject();

            if parsedCloseArgs.IsForce
                assert(~isempty(sched), 'parallel:lang:matlab:NoSchedulerSupplied', ...
                       'A scheduler must be supplied for matlabpool %s.', ...
                       MatlabpoolHelper.CloseAction);

                dctPathAndClearNotificationGateway('off');
                client.cleanup('matlabpool', sched);
            else
                % Exit and quit take no additional arguments.
                dctPathAndClearNotificationGateway('off');
                client.stopLabsAndDisconnect('matlabpool');
            end
        end

        % -------------------------------------------------------------------------
        % doSize
        % -------------------------------------------------------------------------
        function sz = doSize()
            % doSize Return the size of the matlabpool or 0 if it is not open.
            session = com.mathworks.toolbox.distcomp.pmode.SessionFactory.getCurrentSession;
            if ~isempty( session ) && session.isSessionRunning() && session.isPoolManagerSession()
                client = parallel.internal.pool.SessionManager.getSessionObject();

                % Size of a pmode interactive client is ALWAYS zero - otherwise ask the
                % session how big its pool is.
                if isa( client, 'parallel.internal.pool.InteractiveClient' ) && ...
                        strcmp( client.CurrentInteractiveType, 'pmode' )
                    sz = 0;
                else
                    sz = session.getPoolSize();
                end
                return;
            end
            sz = 0;
        end

        % -------------------------------------------------------------------------
        % doExitLeaveGuiOpen
        % -------------------------------------------------------------------------
        function doExitLeaveGuiOpen()
            client = parallel.internal.pool.SessionManager.getSessionObject();
            dctPathAndClearNotificationGateway('off');
            client.stopLabsAndDisconnect('force', 'leaveguiopen');
        end

        % -------------------------------------------------------------------------
        % doAddFileDependencies
        % -------------------------------------------------------------------------
        function doAddFileDependencies(parsedAddFileDependenciesArgs)
            listOfFiles = parsedAddFileDependenciesArgs.ListOfFiles;
            listOfFiles = distcomp.pCanonicalizeFileDependenciesList(listOfFiles);
            parallel.internal.cluster.MatlabpoolHelper.addCanonicalizedFiles(listOfFiles)
        end
        
        % -------------------------------------------------------------------------
        % addCanonicalizedFiles
        % -------------------------------------------------------------------------
        function addCanonicalizedFiles(listOfFiles)
            import com.mathworks.toolbox.distcomp.pmode.*
            import parallel.internal.apishared.AttachedFiles

            isDir = false(numel(listOfFiles), 1);
            % Check each correctly references a file or directory
            for i = 1:numel(listOfFiles)
                thisFile = listOfFiles{i};
                if ~exist(thisFile, 'file')
                    error(message('parallel:lang:matlabpool:InvalidDependency', thisFile));
                end
                % If it exists then check if it is a dir or a file
                isDir(i) = exist(thisFile, 'dir');
            end
            fda = iGetFileDependenciesAssistant();

            % Work out where the remote files are supposed to go.
            client = parallel.internal.pool.SessionManager.getSessionObject();
            pjob = client.ParallelJob;
            % Note that even though pathMap is a handle, we must explicitly set it again
            % on the job so that it gets serialized correctly.
            [relativeFilesPaths, pathMap] = AttachedFiles.generateTempDirectoryStructure(listOfFiles, pjob.AttachedFilePaths);
            pjob.AttachedFilePaths = pathMap;

            % Create a linked list to track the list of completion observers
            lo = java.util.LinkedList();
            % Loop backwards because we expect the first added to be at the top of the
            % path not the end
            cwd = pwd;
            for i = numel(listOfFiles):-1:1
                thisFile = listOfFiles{i};
                thisRelativePath = relativeFilesPaths{i};
                if isDir(i)
                    % Send directories by zipping them up and sending the zip. Note
                    % that the cleanup object will take responsibility for removing the zip file
                    % once the message has been received by all
                    [tempFilesDir, tempDirCleanupObj] = AttachedFiles.copyAllFilesToTempDir({thisFile}, {thisRelativePath}); %#ok<NASGU>
                    [zipName, cleanupObject] = AttachedFiles.createZipFile(thisRelativePath, tempFilesDir); %#ok<NASGU>
                    obs = fda.addNewDirectoryDependency(zipName, cwd, thisFile, thisRelativePath);
                else
                    % Send files as is.
                    obs = fda.addNewFileDependency(cwd, thisFile, thisRelativePath);
                end
                % Make sure we retain the observer for each requested transfer
                lo.add( obs );
            end
            [transferErrors, erroredFilenames] = iWaitForTransfersToComplete(lo);
            if isempty(transferErrors)
                return
            end
            errorToThrow = MException(message('parallel:lang:matlabpool:FileTransferError'));
            for ii = 1:numel(transferErrors)
                errorToThrow = errorToThrow.addCause( ...
                    MException(message('parallel:lang:matlabpool:UnableToCompleteTransfer', ...
                        erroredFilenames{ii}, transferErrors{ii})));
            end
            throw(errorToThrow);
        end

        % -------------------------------------------------------------------------
        % doUpdateFileDependencies
        % -------------------------------------------------------------------------
        function doUpdateFileDependencies()
            try
                fda = iGetFileDependenciesAssistant();
            catch err
                if strcmp(err.identifier, 'parallel:lang:matlabpool:PoolNotRunning')
                    % Do nothing if there isn't a session running - simply return
                    return
                else
                    rethrow(err);
                end
            end
            % Find files that have changed since we last looked
            mapFiles = fda.findChangedFiles;
            iSendChangedFilesToWorkers(mapFiles, fda);
        end

        % -------------------------------------------------------------------------
        % parseOpenArgs
        % -------------------------------------------------------------------------
        % Parse the input arguments to matlabpool open.
        %   Varargin should be one of:
        %   {}, {config, numlabs}, {config}, {numlabs}
        %   Potentially followed by PV pair - 'FileDependencies', {fileDependencies}
        %   Return the action args that can be passed to doMatlabpool.
        function actionArgs = parseOpenArgs(OPEN_ACTION_SUPPLIED, validActions, varargin)

            import parallel.internal.cluster.MatlabpoolHelper;
            actionArgs = MatlabpoolHelper.DefaultOpenArgs;
            if isempty(varargin)
                return;
            end

            % Allowed to specify FileDependencies via param/value pair:
            % - Full string matching
            % - Case sensitive
            % - Must be as param/value pair - CAN NOT be a struct or cell
            allowedProps = {'FileDependencies', 'AttachedFiles'};
            allowedPropsStr = sprintf(' %s', allowedProps{:});
            allowedPropsMsg = getString(message('parallel:lang:matlabpool:ValidParametersForAction', ...
                MatlabpoolHelper.OpenAction, allowedPropsStr));
            % If matlabpool was called without open it is possible that the user
            % mistyped the action, and we need to let them know the valid actions.
            if ~OPEN_ACTION_SUPPLIED
                allowedPropsMsg = getString(message('parallel:lang:matlabpool:ValidParametersForActionOpenAssumed', ...
                    MatlabpoolHelper.OpenAction, allowedPropsStr, MatlabpoolHelper.OpenAction, sprintf('\n\t%s', validActions{:})));
            end
            % Split inputs into "initial" and "P-V" args at first allowedProp
            initialArgs = {};
            pvArgs = {};
            % Since varargin is not a cellstr have to do this the long way.
            if ischar( varargin{1} ) && any( strcmp( varargin{1}, allowedProps ) )
                pvArgs = varargin;
            elseif length( varargin ) > 1 && ischar( varargin{2} ) && any( strcmp( varargin{2}, allowedProps ) )
                initialArgs = varargin(1);
                pvArgs = varargin(2:end);
            elseif length( varargin ) > 2
                initialArgs = varargin(1:2);
                pvArgs = varargin(3:end);
            else
                initialArgs = varargin;
            end

            % initialArgs has length 0, 1 or 2
            if length( initialArgs ) == 1
                if isnumeric( initialArgs{1} )
                    actionArgs.NumLabs = initialArgs{1};
                else
                    actionArgs.NumLabs = str2double( initialArgs{1} );
                    if ~isfinite( actionArgs.NumLabs )
                        actionArgs.NumLabs = [];
                        actionArgs.Configuration = initialArgs{1};
                    end
                end
            elseif length( initialArgs ) == 2
                actionArgs.Configuration = initialArgs{1};
                if isnumeric( initialArgs{2} )
                    actionArgs.NumLabs = initialArgs{2};
                else
                    actionArgs.NumLabs = str2double( initialArgs{2} );
                    % What if this didn't convert to a double
                    if ~isfinite( actionArgs.NumLabs )
                        if ischar( initialArgs{2} )
                            err = MException(message('parallel:lang:matlabpool:InputNotValidHere', initialArgs{ 2 }, MatlabpoolHelper.OpenAction, allowedPropsMsg));
                        else
                            err = MException(message('parallel:lang:matlabpool:InvalidOpenParameter', MatlabpoolHelper.OpenAction, allowedPropsMsg));
                        end
                        throw(err);
                    end
                end
            end

            actionArgs.UserSuppliedConfiguration = ~isempty(actionArgs.Configuration);

            try
                [allProps, allValues] = parallel.internal.convertToPVArrays( pvArgs{:} );
            catch err
                newErr = MException(message('parallel:lang:matlabpool:InvalidOpenParameter', MatlabpoolHelper.OpenAction, allowedPropsMsg));
                newErr = newErr.addCause(err);
                throw(newErr);
            end

            for n = 1:length(allProps)
                thisProp = allProps{n};
                thisValue = allValues{n};
                switch thisProp
                    case {'FileDependencies', 'AttachedFiles'}
                        actionArgs.FileDependencies = thisValue;
                    otherwise
                        err = MException(message('parallel:lang:matlabpool:InvalidInputParameter', thisProp, MatlabpoolHelper.OpenAction, allowedPropsMsg));
                        throw(err);
                end
            end

            if ~isempty(actionArgs.NumLabs) && ~iIsIntegerScalar(actionArgs.NumLabs, 1, realmax)
                err = MException(message('parallel:lang:matlabpool:InvalidPoolSize'));
                throw(err);
            end
        end

        % -------------------------------------------------------------------------
        % parseCloseArgs
        % -------------------------------------------------------------------------
        % Parse the input arguments to matlabpool close.
        %   Varargin should be one of:
        %   {}, {config} {force}
        %   Return the action args that can be passed to doMatlabpool.
        function actionArgs = parseCloseArgs(varargin)
            import parallel.internal.cluster.MatlabpoolHelper;
            if numel(varargin) > 2
                error(message('parallel:lang:matlabpool:InvalidNumInputs', MatlabpoolHelper.CloseAction));
            end

            actionArgs = MatlabpoolHelper.DefaultCloseArgs;
            % No input arguments is a normal close
            if nargin == 0
                return;
            end
            % OK - check that all inputs are row strings
            if ~all(cellfun(@ischar, varargin) & cellfun(@(C) size(C, 2), varargin) == cellfun(@numel, varargin))
                error(message('parallel:lang:matlabpool:InvalidNumInputs', MatlabpoolHelper.CloseAction));
            end
            % The first string must be 'force'
            if ~strcmpi(varargin{1}, 'force')
                error(message('parallel:lang:matlabpool:InvalidCloseSyntax', MatlabpoolHelper.CloseAction, MatlabpoolHelper.CloseAction, varargin{ 1 }));
            end
            actionArgs.IsForce = true;
            if nargin > 1
                actionArgs.Configuration = varargin{2};
            end

            actionArgs.UserSuppliedConfiguration = ~isempty(actionArgs.Configuration);
        end
    end

    methods (Static, Access = protected) % Methods for generating errors
        % -------------------------------------------------------------------------
        % errorIfNotSingleCellString
        % -------------------------------------------------------------------------
        function errorIfNotSingleCellString(action, varargin)
        % Ensure only two inputs
            narginchk(2,2);
            % Get the cell string - we know there is only one input
            cellOfStrings = varargin{1};
            % Check it is a cell that contains only strings
            if ~iscellstr(cellOfStrings)
                error(message('parallel:lang:matlabpool:CellstrRequired', action));
            end
        end

        % -------------------------------------------------------------------------
        % errorIfArgs
        % -------------------------------------------------------------------------
        function errorIfArgs(action, nargs)
            if nargs ~= 0
                error(message('parallel:lang:matlabpool:NoArgumentsAllowed', action));
            end
        end

        % -------------------------------------------------------------------------
        % errorIfNotOnClient
        % -------------------------------------------------------------------------
        function errorIfNotOnClient(action)
            if system_dependent('isdmlworker')
                error(message('parallel:lang:matlabpool:RunOnLabs', action));
            end
        end

        % -------------------------------------------------------------------------
        % getTransferError
        % -------------------------------------------------------------------------
        function err = getTransferError( messageObserver )
            % Get the transfer errors from this file send to see if we should
            % indicate something went wrong.
            transferErrors = messageObserver.getSourceAndTransferErrors.toArray;
            if isempty(transferErrors)
                err = '';
            else
                % Get all the transfer errors as strings in a cell array. This will
                % contain the ProcessInstance first and the Exception error message
                % second
                messages = cell(2, numel(transferErrors));
                for j = 1:numel(transferErrors)
                    % Get the char version of the ProcessInstance from the Pair
                    messages{1, j} = char(transferErrors(j).getFirst);
                    % Get the char version of the ProcessInstance from the Pair
                    messages{2, j} = char(transferErrors(j).getSecond.getMessage);
                end
                err = sprintf('\n%s: %s', messages{:});
            end
        end

        function configName = checkConfigOk( profHelper, configName )
            if isempty( configName )
                configName = profHelper.getDefaultName();
            elseif ~profHelper.nameExists( configName )
                profHelper.throwNoSuchConfig( configName );
            end
        end

        function config = checkNoConfigSupplied( profHelper, config )
            if isempty( config )
                % Ok, wasn't supplied
            elseif profHelper.nameExists( config )
                error(message('parallel:lang:matlabpool:ConfigNotAllowed', profHelper.PropertyName));
            else
                error(message('parallel:lang:matlabpool:UnexpectedArgument', config));
            end
        end
    end
end

% -------------------------------------------------------------------------
% iIsIntegerScalar
% -------------------------------------------------------------------------
function valid = iIsIntegerScalar(value, lowerBound, upperBound)
%iIsIntegerScalar Check if input is a scalar integer within the specified
%bounds.
valid = iIsIntegerVector(value, lowerBound, upperBound) ...
    && isscalar(value);
end
% -------------------------------------------------------------------------
% iIsIntegerVector
% -------------------------------------------------------------------------
function valid = iIsIntegerVector(value, lowerBound, upperBound)
%iIsIntegerScalarVector Check if input is a vector of integers within the
%specified bounds.
valid = isnumeric(value) && isreal(value) && isvector(value) ...
    && all((value >= lowerBound)) ...
    && all(value <= upperBound) && ~any(isnan(value)) ...
    && all(fix(value) == value);
end


% -------------------------------------------------------------------------
% iGetFileDependenciesAssistant
% -------------------------------------------------------------------------
function fda = iGetFileDependenciesAssistant()
import com.mathworks.toolbox.distcomp.pmode.*
% Get the FileDependenciesAssistant from the current session
session = SessionFactory.getCurrentSession;
if isempty(session) || ~session.isSessionRunning
    error(message('parallel:lang:matlabpool:PoolNotRunning'));
end
fda = session.getFileDependenciesAssistant;
end

% -------------------------------------------------------------------------
% iSendChangedFilesToWorkers
% -------------------------------------------------------------------------
function iSendChangedFilesToWorkers(mapFiles, fda)
if mapFiles.isEmpty()
    % If there is nothing to change then we can return early
    return
end
% Ask the FileDependenciesAssistant to send this list of changed files -
% getting back a list of observers to those sends
lo = fda.sendChangedFiles(mapFiles);
[transferErrors, erroredFilenames]= iWaitForTransfersToComplete(lo);
if ~isempty(transferErrors)
    for ii = 1:numel(transferErrors)
        warning(message('parallel:lang:matlabpool:UnableToCompleteUpdate', ...
            erroredFilenames{ii}, transferErrors{ii}));
    end
end

% If all was successful then update the time that we last checked at.
fda.fLastCheckTime = floor(java.lang.System.currentTimeMillis/1000);
end

% -------------------------------------------------------------------------
% iWaitForTransfersToComplete
% -------------------------------------------------------------------------
function [transferErrors, erroredFilenames]= iWaitForTransfersToComplete(observers)
import com.mathworks.toolbox.distcomp.pmode.*
% Create a linked list to track the completion of the file sends
lf = java.util.LinkedList();
% Wait for all the transfers to complete - but ALWAYS loop so that this is
% interruptible
while ~FileTransferObserver.waitForCompletionOfObserverList( observers, lf, 1, java.util.concurrent.TimeUnit.SECONDS )
end
% Convert to an array for handling in MATLAB - now that java is finished
% with the variable
lf = lf.toArray;
numFinished = numel(lf);
transferErrors = cell(numFinished, 1);
erroredFilenames = cell(numFinished, 1);
for ii = 1:numFinished
    % Get the transfer errors from this file send to see if we should
    % indicate something went wrong.
    transferErrors{ii} = parallel.internal.cluster.MatlabpoolHelper.getTransferError( lf(ii) );
    if ~isempty(transferErrors{ii})
        % Which file were we trying to send across.
        erroredFilenames{ii} = char(lf(ii).getFile);
    end
end

empties = cellfun(@isempty, transferErrors);
transferErrors = transferErrors(~empties);
erroredFilenames = erroredFilenames(~empties);
end

% -------------------------------------------------------------------------
% iGetJavaFilesToAddAndUpdate
% -------------------------------------------------------------------------
function [filesToAdd, jFilesToAdd, filesToUpdate, jFilesToUpdate] = iGetJavaFilesToAddAndUpdate(fda, files)
jFiles = cellfun(@(x) java.io.File(x), files, 'UniformOutput', false);
jFiles = [jFiles{:}];
filesAlreadyAttached = arrayfun(@(x) fda.dependencyExists(x), jFiles);

filesToAdd = files(~filesAlreadyAttached);
jFilesToAdd = jFiles(~filesAlreadyAttached);

filesToUpdate = files(filesAlreadyAttached);
jFilesToUpdate = jFiles(filesAlreadyAttached);
end