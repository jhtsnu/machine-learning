function checkNumberOfArguments(inOrOutMode, minArgs, maxArgs, actualNumArgs, functionName)

% Function to check if the number of input/output arguments.  Use
% this over the standard nargchk and nargoutchk for distcomp functions that
% take input arguments of the form (@fcn, numArgsOut, argsIn) in order
% to minimize user's confusion about which input/output arguments we are 
% referring to.
%
% Uses a message like: "Not enough input arguments for <functionName> were supplied".
% 

%  Copyright 2010-2012 The MathWorks, Inc.

%inOrOutMode must be either 'input' or 'output'
allowedStrings = {'input'; 'output'};
assert(any(strcmpi(inOrOutMode, allowedStrings)), ...
        'inOrOutMode must be one of the following: %s.', ...
        sprintf('%s ', allowedStrings{:}));

if (actualNumArgs >= minArgs) && (actualNumArgs <= maxArgs)
    return;
end
    
if strcmpi(inOrOutMode, 'input')
    if actualNumArgs < minArgs 
        errId = 'parallel:cluster:NotEnoughInputsForFunction';
    else
        errId = 'parallel:cluster:TooManyInputsForFunction';
    end
else
    if actualNumArgs < minArgs 
        errId = 'parallel:cluster:NotEnoughOutputsForFunction';
    else
        errId = 'parallel:cluster:TooManyOutputsForFunction';
    end
end

ex = MException(message(errId, functionName));
throwAsCaller(ex);

