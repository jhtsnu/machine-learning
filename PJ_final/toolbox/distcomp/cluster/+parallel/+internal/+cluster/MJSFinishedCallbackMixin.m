
%   Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) MJSFinishedCallbackMixin < parallel.internal.cluster.MJSCallbackMixin
    
    properties( Dependent )
        %FinishedFcn Callback executed when the job or task finishes
        FinishedFcn
    end
    
    properties( Access = private )
        % The "raw" callback function specified by the user.
        UserSpecifiedFinishedFcn
        
        % Listens to Finished event and calls the Callback fcn
        CallbackListener
        
        % Listens to Finished event and, since no more events can be fired,
        % unregisters this object for events.
        AutoEventUnregistrationListener
    end
    
    events( Hidden, ListenAccess = public, NotifyAccess = protected )
        Finished
    end
    
    properties ( Constant, Access = protected )
        FinishedEventName = 'Finished';
    end
    
    methods
        function obj = MJSFinishedCallbackMixin()
            obj.CallbackListener = event.listener( obj, obj.FinishedEventName, obj.NoOpCallback );
            obj.CallbackListener.Enabled = false;
            
            obj.AutoEventUnregistrationListener = event.listener( obj, obj.FinishedEventName, @obj.unregisterForAllEvents );
            obj.AutoEventUnregistrationListener.Enabled = false;
        end
        
        function val = get.FinishedFcn( obj )
            val = obj.UserSpecifiedFinishedFcn;
        end
        
        function set.FinishedFcn( obj, val )
            try
                obj.setCallbackFcn( 'FinishedFcn', obj.CallbackListener, val );
                obj.UserSpecifiedFinishedFcn = val;
            catch err
                % Get the internal details off the stack
                throw(err);
            end            
        end
    end
    
    methods( Access = protected )
        function enableAutoEventUnregistration( obj )
            obj.AutoEventUnregistrationListener.Enabled = true;
        end
        
        function disableAutoEventUnregistration( obj )
            obj.AutoEventUnregistrationListener.Enabled = false;
        end
        
        function unregisterForAllEvents( obj, varargin )
            % varargin because it is used as a listener callback and so
            % will be passed source and event.
            if obj.RegistrationCount > 0
                obj.doUnregisterForEvents();
                obj.RegistrationCount = 0;
            end
        end
    end
    
end

