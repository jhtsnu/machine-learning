% Displayer for Workers

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) WorkerDisplayer < parallel.internal.display.Displayer
    properties (Constant, GetAccess = private)
        DisplayClass = 'parallel.Worker';
        VectorDisplayProps = { 'Host', 'ComputerType' };
        VectorTableColumns = { ...
          % 'Title',        'Resizeable', 'MinimumWidth'
            'Host',         true,          length( 'hostname.subdomain.domain.tld' );
            'ComputerType', false,         length( 'ComputerType' ) };
    end

    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract Displayer properties
        DisplayHelper = parallel.internal.display.DisplayHelper( length('ComputerType') )
    end

    methods
        function obj = WorkerDisplayer(someObj)
            obj@parallel.internal.display.Displayer(someObj, 'parallel.Worker');
        end
    end
    methods (Access = protected)
        function doSingleDisplay(obj, toDisp)
            import parallel.internal.display.Displayer
            import parallel.internal.cluster.WorkerDisplayer
            
            obj.DisplayHelper.displayMainHeading(obj.formatDocLink(obj.DisplayClass));
            obj.displaySpecificItems(toDisp);
        end

        function doVectorDisplay(obj, toDisp)
            import parallel.internal.display.Displayer
            import parallel.internal.cluster.WorkerDisplayer

            tableData = cell(numel(toDisp), size(WorkerDisplayer.VectorTableColumns, 1));
            for ii = 1:numel(toDisp)
                currToDisp  = toDisp(ii);
                if isvalid(currToDisp)
                    propsToDisp = Displayer.getDisplayPropsAsStruct(...
                        currToDisp, WorkerDisplayer.VectorDisplayProps);
                    tableData(ii, :) = {propsToDisp.Host, propsToDisp.ComputerType};
                else
                    tableData(ii, :) = {parallel.internal.display.Displayer.DeletedString, ''};
                end
            end

            obj.DisplayHelper.displayDimensionHeading(size(toDisp), obj.DisplayClass);
            obj.DisplayHelper.displayTable(WorkerDisplayer.VectorTableColumns, tableData, obj.DisplayHelper.DisplayIndexColumn);
        end
    end
end
