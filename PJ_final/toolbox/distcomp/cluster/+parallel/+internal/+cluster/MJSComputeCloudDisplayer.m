% MJSComputeCloudDisplayer - Displayer subclass to display MJSComputeCloud objects.

%   Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) MJSComputeCloudDisplayer < parallel.internal.cluster.ClusterDisplayer

    properties (Constant, GetAccess = private)
        VectorDisplayProps = {'Type', 'Profile' 'Name'};
        VectorTableColumns = { ...
            % 'Title', 'Resizeable', 'MinimumWidth'
            '',        true,         length('1'); ...% This is the index column for the vector display
            'Type',    false,        length('MJSComputeCloud'); ...
            'Profile', true,         length('Profile'); ...
            'Name',    true,         length('Name'); ...
        }
    end

    methods
        function obj = MJSComputeCloudDisplayer(someObj)
            obj@parallel.internal.cluster.ClusterDisplayer(someObj, class(someObj));
        end
    end

    methods (Access = protected)
        
        function doVectorDisplay(obj, toDisp)
            import parallel.internal.display.Displayer
            import parallel.internal.cluster.MJSComputeCloudDisplayer

            tableData = cell(numel(toDisp), size(MJSComputeCloudDisplayer.VectorTableColumns, 1));
            for ii = 1:numel(toDisp)
                if isvalid(toDisp(ii))
                    propsToDisp = Displayer.getDisplayPropsAsStruct(toDisp(ii), MJSComputeCloudDisplayer.VectorDisplayProps);
                    tableData(ii, :) = {...
                        toDisp(ii).hGetLink(ii), ...
                        propsToDisp.Type, ...
                        propsToDisp.Profile, ...
                        propsToDisp.Name};
                else
                    tableData(ii, :) = {parallel.internal.display.Displayer.DeletedString, '', ''};
                end
            end

            obj.DisplayHelper.displayDimensionHeading(size(toDisp), obj.formatDocLink(class(toDisp)));
            obj.DisplayHelper.displayTable(MJSComputeCloudDisplayer.VectorTableColumns, tableData, obj.DisplayHelper.DoNotDisplayIndexColumn);
        end
    end
    
    methods( Access = protected )
        function [p, q, r, f] = getNumJobs(~,toDisp)
            [p, q, r, f] = toDisp.hEnumerateJobsInStates();
        end
    end

end
