function OK = wait( workunit, state, timeout )
% wait - general implementation of "wait"

% Copyright 2011 The MathWorks, Inc.

try
    startTime = clock;
    while true
        OK = (workunit.StateEnum >= state);
        if OK || etime(clock, startTime) > timeout
            break
        end
        pause(1);
    end
catch err
    % The object might become invalid during the waitForEvent. Only
    % bother with errors if the object is still valid
    if isvalid( workunit )
        rethrow( err );
    end
end
end
