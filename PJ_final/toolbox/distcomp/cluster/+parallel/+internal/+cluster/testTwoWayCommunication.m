function testTwoWayCommunication(jobManagerProxy)
; %#ok Undocumented
% Helper to set up and test two way communication with the job manager

%   Copyright 2011 The MathWorks, Inc.

jobManager = distcomp.createObjectsFromProxies(jobManagerProxy, ...
    @distcomp.jobmanager, distcomp.getdistcompobjectroot);
jobManager.pCheckTwoWayCommunications();
