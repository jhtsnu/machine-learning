function [myHostName, machineToWorkerMapping, myLabIndex] = getMachineToWorkerMapping()
% GETMACHINETOLABMAPPING build an array of workers on this host.
% Performs collective operations.

% Copyright 2012 The MathWorks, Inc.

myHostName = java.net.InetAddress.getLocalHost();

% perform a global gather operation to create a cluster-wide machine
% hostname to worker mapping
machineToWorkerMapping = gcat({char(myHostName); labindex});

% Record the lab index for later use (helpful if we are in a parfor loop).
myLabIndex = labindex;

end
