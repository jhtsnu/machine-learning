% TwoLevelCache - a cache supporting hierarchical removal
%
% Designed for caching jobs and tasks under a cluster object.  When adding and
% removing entries to/from the cache, a parent 'key' must be supplied. When
% removing entries, any child entries will also be removed.

% Copyright 2011 The MathWorks, Inc.

classdef ( Sealed, Hidden ) TwoLevelCache < handle

    properties ( GetAccess = private, SetAccess = immutable )
        MainMap  % Stores key --> value
        ChildMap % Stores parent key --> set of child keys
    end

    methods ( Access = { ?parallel.internal.cluster.MJSSupport, ...
                         ?parallel.internal.cluster.CJSSupport } )
        function obj = TwoLevelCache()
            obj.MainMap  = containers.Map();
            obj.ChildMap = containers.Map();
        end
        function addOrReplace( obj, parentId, childId, childObj )
        % addOrReplace - add an object, or replace an existing object
        % parentId: the parent's key
        % childId:  the child's key
        % childObj: the object to be cached
            if obj.isKey( childId )
                % remove then re-add
                obj.remove( parentId, childId );
            end
            obj.add( parentId, childId, childObj );
        end
        function add( obj, parentId, childId, childObj )
        % addToCache - add an object to the cache. Error if key exists
        % parentId: the parent's key
        % childId:  the child's key
        % childObj: the object to be cached
        %{
            validateattributes( parentId, {'char'}, {} );
            validateattributes( childId,  {'char'}, {'row'} );
            %}
            cm = obj.ChildMap;
            if isKey( cm, parentId )
                childSet = cm( parentId );
                didAdd   = childSet.add( childId );
                if ~didAdd
                    error(message('parallel:cluster:CacheConsistencyAdd'));
                end
            else
                childSet = java.util.HashSet();
                childSet.add( childId );
                cm( parentId ) = childSet; %#ok<NASGU> handle
            end
            obj.MainMap( childId ) = childObj;
        end
        function childObj = retrieve( obj, childId )
        % retrieve - get an object from the cache
        % childId: the key of the child to retrieve
        % childObj: the retrieved child
        % Throws: if the cache doesn't contain the key

        %{
            validateattributes( childId,  {'char'}, {'row'} );
            %}
            if ~obj.isKey( childId )
                error(message('parallel:cluster:CacheConsistencyRetrieve'));
            end
            childObj = obj.MainMap( childId );
        end
        function [tf, childObj] = retrieveIfPresent( obj, childId )
            m  = obj.MainMap;
            tf = isKey( m, childId );
            if tf
                childObj = m( childId );
            else
                childObj = [];
            end
        end
        function tf = isKey( obj, childId )
        % isKey - does the cache contain this child
        %{
            validateattributes( childId,  {'char'}, {'row'} );
            %}
            tf = obj.MainMap.isKey( childId );
        end
        function remove( obj, parentId, childId )
        % remove - remove an entry and any sub-entries
        % parentId: the parent of object to remove
        % childId: the object to remove
        %{
            validateattributes( parentId, {'char'}, {} );
            validateattributes( childId,  {'char'}, {'row'} );
            %}
            if ~ ( obj.MainMap.isKey( childId ) && ...
                   obj.ChildMap.isKey( parentId ) )
                error(message('parallel:cluster:CacheConsistencyRemove'));
            end

            % Remove ourselves from our parent's child list
            siblingSet = obj.ChildMap( parentId );
            didRemove  = siblingSet.remove( childId );
            if ~ didRemove
                error(message('parallel:cluster:CacheConsistencyRemove'));
            end

            % If we have children, remove them. This is a two-level cache, so we
            % don't recurse here, although we could to generalize.
            if obj.ChildMap.isKey( childId )
                childSet   = obj.ChildMap( childId );
                childSetIt = childSet.iterator();
                while childSetIt.hasNext()
                    val = char( childSetIt.next() );
                    obj.MainMap.remove( val );
                end
                obj.ChildMap.remove( childId );
            end

            % Finally, actually remove the main entry for this child
            obj.MainMap.remove( childId );
        end

        function vals = getAllValues( obj )
        %getAllValues Return all contained values
            vals = obj.MainMap.values();
        end
        function clear( obj )
        %CLEAR empty all cached values
            obj.MainMap.remove( obj.MainMap.keys() );
            obj.ChildMap.remove( obj.ChildMap.keys() );
        end
    end

end
