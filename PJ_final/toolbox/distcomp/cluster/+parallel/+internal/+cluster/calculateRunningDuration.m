function [diffInMillis, startdate] = calculateRunningDuration(obj)
; %#ok Undocumented
% Calculate the running duration of the object from the start and finish
% times.

%   Copyright 2010 The MathWorks, Inc.

% Initialize outputs
diffInMillis = 0;
startdate = obj.StartTime;
if isempty(startdate) % just still has not started
    % return if job has not started execution
    return;
end

try
    SDF = java.text.SimpleDateFormat('E MMM dd H:m:s z yyyy', java.util.Locale.US);

    finishdate = obj.FinishTime;
    if isempty(finishdate)
        % get the current time
        JDATEFinish = java.util.Date;
    else
        JDATEFinish = SDF.parse(finishdate);
    end

    JDATEStart = SDF.parse(startdate);
    
    diffInMillis = JDATEFinish.getTime - JDATEStart.getTime;
catch err %#ok<NASGU>
end

end

