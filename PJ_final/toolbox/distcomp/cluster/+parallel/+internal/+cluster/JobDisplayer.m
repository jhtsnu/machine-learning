% JobDisplayer - Displayer to handle subclasses of parallel.Job

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden) JobDisplayer < parallel.internal.display.Displayer

    properties (Constant, GetAccess = private)
        BaseDisplayClass = 'parallel.Job';
        VectorDisplayProps = {'ID', 'Type', 'State', 'FinishTime', 'Username', 'Tasks'};
        VectorTableColumns = { ...
            % 'Title',    'Resizeable', 'MinimumWidth'
            'ID',         true,         length('ID'); ...
            'Type',       false,        length('Communicating'); ...
            'State',      false,        length('unavailable'); ...
            'FinishTime', false,        length('mmm dd HH:MM:SS'); ...
            'Username',   true,         length('Username'); ...
            % assuming users won't have more than 100000 tasks in their jobs....!
            'Tasks',     false,        length('Tasks'); ...
        }
    end
    
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract Displayer properties
        DisplayHelper 
    end

    methods
        function obj = JobDisplayer(someObj)
            obj@parallel.internal.display.Displayer(someObj, parallel.internal.cluster.JobDisplayer.BaseDisplayClass); 
            obj.DisplayHelper = parallel.internal.display.DisplayHelper( length('Task ID of Errors'), obj.ShowLinks );
        end
    end

    methods (Access = private)
        function doInvalidSingleDisplay( obj, invalidToDisp )
            obj.DisplayHelper.displayMainHeading(getString( message('parallel:cluster:InvalidJobID', invalidToDisp.ID)));
            
            obj.DisplayHelper.displayProperty('Type', invalidToDisp.Type);
            obj.DisplayHelper.displayProperty('State', invalidToDisp.State);
        end
    end
    
    methods (Access = protected)
        
        function doSingleDisplay(obj, toDisp)
            import parallel.internal.display.Displayer
            import parallel.internal.cluster.JobDisplayer
            import parallel.internal.types.States
            % TODO:later consider adding whether or not job is a batch job or not

            if ~States.isValidState( toDisp.State )
                % Do not attempt to access any further info.
                obj.doInvalidSingleDisplay( toDisp );
                return
            end
            % For the moment, for jobs, we only want to display help for the common base class 
            obj.DisplayHelper.displayMainHeading( obj.formatDocLink(parallel.internal.cluster.JobDisplayer.BaseDisplayClass) );
            obj.DisplayHelper.displaySubHeading(getString(message('parallel:cluster:Properties')));
            obj.displaySpecificItems(toDisp);
            [p, r, f] = toDisp.findTask();
            erroredTaskIndex = logical([]);
            if ~isempty(f)
                erroredTaskIndex = ~cellfun(@isempty, hGetDisplayPropertiesNoError(f, {'Error'}));
            end
            obj.DisplayHelper.displaySubHeading('%s', char(obj.hGetTasksLink(toDisp, getString(message('parallel:cluster:AssociatedTasks')))));
            obj.DisplayHelper.displayProperty('Number Pending',    numel(p));
            obj.DisplayHelper.displayProperty('Number Running',    numel(r));
            obj.DisplayHelper.displayProperty('Number Finished',   numel(f));
            obj.DisplayHelper.displayHTMLArray('Task ID of Errors', obj.hGetLinkArrayFromTasks(f(erroredTaskIndex)));
        end
        function doVectorDisplay(obj, toDisp)
            import parallel.internal.display.Displayer
            import parallel.internal.cluster.JobDisplayer
            import parallel.internal.types.States

            tableData = cell(numel(toDisp), size(JobDisplayer.VectorTableColumns, 1));
            for ii = 1:numel(toDisp)
                currToDisp = toDisp(ii);
                if isvalid(currToDisp) 
                    if States.isValidState( currToDisp.State )
                        propsToDisp = Displayer.getDisplayPropsAsStruct(...
                            currToDisp, JobDisplayer.VectorDisplayProps);
                        if numel(propsToDisp.Tasks > 0)
                            tasksLink = obj.hGetTasksLink(currToDisp, numel(propsToDisp.Tasks));
                        else
                            tasksLink = numel(propsToDisp.Tasks);
                        end
                        dataRow = { ...
                            obj.hGetLink(currToDisp, propsToDisp.ID), ...
                            propsToDisp.Type, ...
                            propsToDisp.State, ...
                            obj.DisplayHelper.formatTimeString(propsToDisp.FinishTime), ...
                            propsToDisp.Username, ...
                            tasksLink ...
                            };
                    else
                        dataRow = { currToDisp.ID, currToDisp.Type, currToDisp.State, ...
                                    '', '', '' };
                    end
                else
                    dataRow = {'', '', parallel.internal.display.Displayer.DeletedString, '', '', ''};
                end
                tableData(ii, :) = dataRow;
            end
            % In the vector display we only want to display help for the common base class
            obj.DisplayHelper.displayDimensionHeading(size(toDisp), obj.formatDocLink(JobDisplayer.BaseDisplayClass));
            obj.DisplayHelper.displayTable(JobDisplayer.VectorTableColumns, tableData, obj.DisplayHelper.DisplayIndexColumn);
        end
        
    end
    
    methods ( Hidden, Sealed )
        
        function tasksLink = hGetTasksLink(obj, toDisp, displayValue)
            if obj.ShowLinks
                matlabFunction = sprintf('parallel.internal.cluster.JobDisplayer.displayTasks(''%s'')', ...
                    serialize(parallel.internal.display.JobMemento(toDisp)));
                tasksLink = parallel.internal.display.HTMLDisplayType(displayValue, matlabFunction);
            else
                 % Getting a link will always return an HTMLDisplayType,
                 % but if we are not building a hyperlink, we do not
                 % include the matlab command. 
                tasksLink = parallel.internal.display.HTMLDisplayType(displayValue);
            end
        end
        
        function taskLinks = hGetLinkArrayFromTasks(obj, tasks)
            if ~isempty(tasks)
                taskLinksCell = arrayfun( @(x) obj.hGetLink(x, x.ID), tasks, 'UniformOutput', false );
                % arrayfun cannot output an array of HTMLDisplayType, so we need to convert to a cell array and back
                taskLinks = [taskLinksCell{:}];
            else
                % Getting a link will always return an HTMLDisplayType
                taskLinks = parallel.internal.display.HTMLDisplayType.empty;
            end
        end

    end
    
    methods ( Hidden, Static )
        % Display tasks when the hyperlink is clicked in the job display
        function displayTasks(serializedMemento)
            try
                job = createObjectFromMemento(parallel.internal.display.Memento.deserialize(serializedMemento));
                disp(job.Tasks)
            catch err
                dctSchedulerMessage( 1, ...
                    'Failed to create Job from memento, original error: %s', err.getReport() );
                if feature('hotlinks')
                    helpString = '<a href="matlab: help parallel.Job">help parallel.Job</a>';
                else
                    helpString =  'help parallel.Job';
                end
                error(message('parallel:cluster:LinkDispFailure', getString(message('parallel:cluster:AssociatedTasks')), getString(message('parallel:cluster:Job')), helpString));
            end
        end

        
    end
end
