% CJSSupport - intermediary between CJSCluster and distcomp.fileserializer.
% Handles:
% - getting/setting properties on jobs and tasks
% - building new jobs/tasks on disk
% - removing tasks from disk
% - caching jobs/task MCOS objects

%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) CJSSupport < handle
    properties ( SetAccess = immutable )
        % Given that jobs and tasks store a handle to the CJSSupport object, it
        % is imperative that this class cannot be "reseated" to a different file
        % location. Hence all these properties are immutable.

        % A distcomp.filestorage object to access the files on disk.
        Storage

        % A distcomp.fileserializer object for use with the Storage
        Serializer

        % A TwoLevelCache of all jobs and tasks for this Cluster.
        Cache
    end

    properties ( Constant, GetAccess = private )
        % Various constant mappings between API1 property names and new API
        % properties.

        % Map object property names to fields understood by storage
        PropertyMap          = iGetPropMap();

        % Default values to fabricate in case it's not possible to read certain
        % values.
        MissingValueDefaults = { { 'state' , parallel.internal.types.States.Unavailable } };

        % "Map" for Variant / api1-constructor-string correspondence table
        VariantToCtor       = iVariantToCtor();
    end

    methods % Constructor
        function obj = CJSSupport( storage )
        % Constructor takes a single distcomp.filestorage argument
            validateattributes( storage, {'distcomp.filestorage'}, {'scalar'} );
            obj.Storage    = storage;
            obj.Serializer = distcomp.fileserializer( obj.Storage );
            obj.Cache      = parallel.internal.cluster.TwoLevelCache();
        end
    end

    methods % Cluster support

        function invalidateAllChildren( obj, ~ )
            valsc = obj.Cache.getAllValues();
            cellfun( @hInvalidate, valsc );
            obj.Cache.clear();
        end

        function jl = getJobs( obj, cjscluster, clusterSId )
        % Build and return the jobs present in this Cluster.
            validateattributes( cjscluster, {'parallel.cluster.CJSCluster'}, { 'scalar' } );
            validateattributes( clusterSId, {'char'}, {} );

            variantsMustBeDerived = [];
            emptyJobList          = parallel.job.CJSIndependentJob.empty();
            jl = obj.getChildren( ...
                cjscluster, clusterSId, emptyJobList, variantsMustBeDerived );
        end

        function jlCell = getJobsByState( obj, cjscluster, clusterSId, statesCell )
        % Return a cell array of jobs matching a particular state as specified by
        % statesCell, which contains string values.
            import parallel.internal.cluster.CJSSupport

            variantsMustBeDerived = [];
            emptyJobList          = parallel.job.CJSIndependentJob.empty();
            jlCell = obj.getChildrenByState( ...
                cjscluster, clusterSId, emptyJobList, variantsMustBeDerived, statesCell );
        end

        function job = getJobFromLocation( obj, cluster, jobSId )
        % Used to build a single specific job object from a specified location
        % within the Cluster storage - used as part of the execution framework.
        % used by distcomp_evaluate_filetask

            import parallel.internal.cluster.CJSSupport

            validateattributes( cluster, {'parallel.cluster.CJSCluster'}, {'scalar'} );
            validateattributes( jobSId, {'char'}, {'row'} );

            [proxy, ctor] = obj.Storage.getProxyByName( jobSId );
            variant       = CJSSupport.variantFromCtorName( func2str( ctor ) );
            jobID         = proxy.getEntityID();
            job           = cluster.hBuildChild( variant, jobID, jobSId );
        end

        function task = getTaskFromLocation( obj, job, taskSId )
        % Used to build a single specific job object from a specified location within
        % the Cluster storage - used as part of the execution framework.
        % used by distcomp_evaluate_filetask.
            validateattributes( job, {'parallel.Job'}, { 'scalar' } );
            validateattributes( taskSId, {'char'}, { 'row' } );

            % NOTE: 'getProxyByName' has unfortunate 'nargout' behaviour that
            % conflicts with serializeForSubmission. serializeForSubmission
            % doesn't send across Job##/matlab_metadata.mat. Asking for the
            % 'constructor' output of getProxyByName fails in that case, but
            % since we're making a task, we don't need it anyway.
            proxy   = obj.Storage.getProxyByName( taskSId );
            variant = parallel.internal.types.Variant.Task;
            taskID  = proxy.getEntityID();
            task    = job.hBuildChild( variant, taskID, taskSId );
        end

        function diaryFile = getDiaryFileForTask( obj, taskLoc )
        % Used to get the location of the diary file so we can stream diary output
        % to it.
            taskType = parallel.internal.types.Variant.Task;
            diaryProp = parallel.internal.cluster.CJSSupport.mapCJSProperties( 'Diary' );
            diaryFile = obj.Storage.pGetFileForStreaming( taskLoc, taskType.Name, diaryProp{1} );
        end

        function job = buildJob( obj, cjscluster, clusterSId, variant )
        % Used to construct a new Job from scratch in the storage, given a variant.

            validateattributes( cjscluster, {'parallel.cluster.CJSCluster'}, {'scalar'} );
            validateattributes( clusterSId, {'char'}, {} );
            validateattributes( variant, {'parallel.internal.types.Variant'}, {'scalar'} );

            import parallel.internal.cluster.CJSSupport
            import parallel.internal.cluster.DefaultProperties
            import parallel.internal.types.Variant

            ctorName = CJSSupport.ctorNameFromVariant( variant );
            proxy    = obj.Storage.createProxies( clusterSId, 1, str2func( ctorName ) );
            jobID    = proxy.getEntityID();
            jobSId   = iCalculateChildSId( clusterSId, variant, jobID );
            job      = cjscluster.hBuildChild( variant, jobID, jobSId );

            newJobProps         = DefaultProperties.getNewJobPropStruct();
            newJobProps.Name    = char(proxy.getName);
            newJobProps.Version = char(com.mathworks.toolbox.distcomp.util.Version.VERSION_STRING);
            newJobProps.JobSchedulerData = [];
            if variant ~= Variant.IndependentJob
                % NB storage still uses min/max rather than the limits pair.
                newJobProps.MinNumWorkers = 1;
                newJobProps.MaxNumWorkers = Inf;
            end
            initNames = fieldnames( newJobProps );
            initVals  = struct2cell( newJobProps );
            obj.initProperties( variant, jobSId, initNames, initVals );
            % use addOrReplace because an obsolete object may be in the cache
            % using this jobSId.
            obj.Cache.addOrReplace( clusterSId, jobSId, job );
        end

        function result = getJobFilesInfo( obj, jobSId, numTasks )
        % Return a JobFilesInfo for this job - for RemoteClusterAccess.
            result = parallel.internal.apishared.JobFilesInfo( ...
                obj.Storage, jobSId, numTasks );
        end
    end

    methods ( Access = private, Static )
        function exceptionDispatch( E, propNames )
        % Dispatch exceptions coming from accessing the file storage
            if iscell( propNames )
                descr = sprintf( '"%s" ', propNames{:} );
            else
                descr = sprintf( '"%s" ', propNames );
            end
            if isequal( E.identifier, 'parallel:cluster:FileSerializerCannotOpenStateFile' )
                E2 = MException(message('parallel:cluster:StorageAccessIOError', descr));
            else
                E2 = MException(message('parallel:cluster:UnexpectedStorageAccessError', descr));
            end
            E2 = addCause( E2, E );
            throwAsCaller( E2 );
        end
    end
    methods ( Access = private )
        function [v, wasMapped] = mapFailedGets( ~, ~, propNames )
        % Use MissingValueDefaults to fill out properties that could not be read from
        % the storage for whatever reason.
            defaults    = parallel.internal.cluster.CJSSupport.MissingValueDefaults;
            v           = cell( 1, length( propNames ) );
            wasMapped   = false( 1, length( propNames ) );
            for ii = 1:length( defaults )
                matches = strcmp( defaults{ii}{1}, propNames );
                if any( matches )
                    v{ matches }         = defaults{ii}{2};
                    wasMapped( matches ) = true;
                end
            end
        end

        function initProperties( obj, variant, sId, fields, values )
        % When building a new Job/Task, initialize the properties for the first
        % time in the storage.
        % - obj: CJSSupport object
        % - variant: a Variant
        % - sId: storage ID
        % - fields: scalar/cell of char fields to init
        % - values: scalar/cell of field values to init
            import parallel.internal.cluster.CJSSupport;
            try
                [mappedFields, ~, ~, preSetFcns] = CJSSupport.mapCJSProperties( fields );
                if ~iscell( values )
                    values = { values };
                end
                gotPreSetFcn         = ~cellfun( @isempty, preSetFcns );
                values(gotPreSetFcn) = cellfun( @(fcn, val) fcn( val ), ...
                                                preSetFcns(gotPreSetFcn), ...
                                                values(gotPreSetFcn), ...
                                                'UniformOutput', false );
                obj.Serializer.createFields( iSerializerStruct( variant, sId ), ...
                                             mappedFields, values );
            catch E
                CJSSupport.exceptionDispatch( E, fields );
            end
        end

        function values = getProperties( obj, variant, sId, propNames )
        % Get multiple properties on a job/task simultaneously. Map the property
        % names.
        % - obj: CJSSupport object
        % - variant: a Variant
        % - sId: storage ID
        % - propNames: scalar/cell of char fields to get

            import parallel.internal.cluster.CJSSupport

            [mapped, cellRet, postGetFcns] = CJSSupport.mapCJSProperties( propNames );
            try
                values = obj.Serializer.getFields( iSerializerStruct( variant, sId ), mapped );
                % Apply the postGetFcns as required.
                gotPostGetFcn         = ~cellfun( @isempty, postGetFcns );
                values(gotPostGetFcn) = cellfun( @(fcn, val) fcn( val ), ...
                                                 postGetFcns(gotPostGetFcn), ...
                                                 values(gotPostGetFcn), ...
                                                 'UniformOutput', false );
            catch E %#ok<NASGU>

                % Something went wrong. We do not allow exceptions to propagate
                % from getProperties, we simply map the values.
                values = cell( size( mapped ) );
                for ii = 1:length( mapped )
                    % We may have more luck accessing properties one at a time
                    try
                        values(ii) = obj.Serializer.getFields( ...
                            iSerializerStruct( variant, sId ), mapped(ii) );
                        if ~isempty( postGetFcns{ii} )
                            fcn = postGetFcns{ii};
                            values(ii) = fcn( values(ii) );
                        end
                    catch E2
                        % Sometimes it's OK if we fail to get the value (e.g. for state).
                        % If not, then rethrow the error
                        [values(ii), wasMapped] = obj.mapFailedGets( variant, mapped(ii) );
                        if ~wasMapped
                            rethrow(E2);
                        end
                    end
                end
            end
            if ~cellRet
                values = values{1};
            end
        end

        function setProperties( obj, variant, sId, propNames, values )
        % Set multiple properties from a job/task simultaneously.
        % - obj: CJSSupport object
        % - variant: a Variant
        % - sId: storage ID
        % - propNames: scalar/cell of char fields to get
        % - values: scalar/cell of values to set.
            import parallel.internal.cluster.CJSSupport

            [mappedNames, ~, ~, preSetFcns] = CJSSupport.mapCJSProperties( propNames );
            if ~iscell( propNames )
                values = { values };
            end
            % Assert in terms of mappedNames since this is definitely a cell.
            assert( length( mappedNames ) == length( values ) );

            gotPreSetFcn           = ~cellfun( @isempty, preSetFcns );
            values( gotPreSetFcn ) = cellfun( @(fcn, val) fcn( val ), ...
                                              preSetFcns( gotPreSetFcn ), ...
                                              values( gotPreSetFcn ), ...
                                              'UniformOutput', false );
            obj.Serializer.putFields( iSerializerStruct( variant, sId ), mappedNames, values );
        end

        function childCells = getChildrenByState( obj, parent, parentSId, emptyList, forceVariant, statesCell )
        % Start by getting the children and their SId.

            import parallel.internal.cluster.CJSSupport

            [childList, childSIds] = obj.getChildren( parent, parentSId, emptyList, forceVariant );
            if isempty( childList )
                childCells = repmat( {childList}, 1, numel( statesCell ) );
                return
            end
            % For the purposes of iSerializerStruct, we only care about the variant of
            % the first child.
            serStructs = iSerializerStruct( childList(1).Variant, childSIds );

            % Note: here we ignore the PostGetFcn - we're dealing with the raw
            % state on disk.
            mapped = CJSSupport.mapCJSProperties( {'StateEnum'} );
            try
                states = obj.Serializer.getFields( serStructs, mapped );
            catch E %#ok<NASGU>
                    % need to get 'states' the long/slow way.
                states = arrayfun( @(j) j.StateEnum.Name, childList, 'UniformOutput', false );
            end

            % Build the cell return by matching against statesCell.
            childCells = cellfun( @(state) childList( strcmp( state, states ) ), ...
                                  statesCell, 'UniformOutput', false );
        end

        function [childList, childSIds] = getChildren( obj, parent, parentSId, emptyList, forceVariant )
        % Return a list of jobs/tasks for the specified cluster/job.
        % - obj: CJSSupport object
        % - parent: Cluster/Job object
        % - parentSId: Storage ID of Cluster/Job
        % - emptyList: a strongly-typed list in case no children are found.
        % - forceVariant: if you wish to force the variant of the children
        %   - getTasks does this because the 'ctor' returned by the filestorage may be bogus.
            import parallel.internal.types.Variant;
            import parallel.internal.cluster.CJSSupport;

            % Get the list of "proxies" and variants (via the API1 constructors)
            [proxs, ctors] = obj.Storage.findProxies( parentSId );
            if ~isempty( forceVariant )
                variants = repmat( {forceVariant}, 1, numel( proxs ) );
            elseif iscell( ctors )
                ctorsstr = cellfun( @func2str, ctors, 'UniformOutput', false );
                variants = cellfun( @CJSSupport.variantFromCtorName, ctorsstr, ...
                                    'UniformOutput', false );
            elseif isempty( ctors )
                variants = {};
            else
                variants = repmat( {CJSSupport.variantFromCtorName( func2str( ctors ) )}, ...
                                   1, numel( proxs ) );
            end

            % Either retrieve from the cache, or build new MCOS objects for the
            % children.
            childList = cell( 1, length( proxs ) );
            childSIds = cell( 1, length( proxs ) );
            cache     = obj.Cache;
            for ii=1:length( proxs )
                childID  = proxs(ii).getEntityID();
                childSId = iCalculateChildSId( parentSId, variants{ii}, childID );
                childSIds{ii} = childSId;
                [gotchild, oldchild] = cache.retrieveIfPresent( childSId );
                if gotchild
                    gotchild = isvalid( oldchild ) && ... % ignore invalidated objects
                        oldchild.Id == childID;
                    if gotchild
                        childList{ii} = oldchild;
                    else
                        % Invalid entry in cache, remove
                        cache.remove( parentSId, childSId );
                    end
                end
                if ~gotchild
                    % Must build new MCOS object and cache it
                    newchild = parent.hBuildChild( variants{ii}, childID, childSId );
                    % Here we know for sure that the object isn't in the cache,
                    % so just add it.
                    cache.add( parentSId, childSId, newchild );
                    childList{ii} = newchild;
                end
            end
            if isempty( childList )
                childList = emptyList;
            else
                childList = [ childList{:} ];
            end
        end

        function setJobAttachedFiles( obj, jobsid, jobvariant, rawdata )
        % Special-case handling for AttachedFiles.
            % Resolve the paths and check they all exist so that we can fail early if
            % we've been given invalid paths.
            resolved = cellfun( ...
                @parallel.internal.apishared.AttachedFiles.resolvePath, rawdata, ...
                    'UniformOutput', false );
            try
                obj.setProperties( jobvariant, jobsid, ...
                                               { 'AttachedFiles' }, ...
                                               { resolved } );
            catch E
                % revert to "sensible defaults"
                obj.setProperties( jobvariant, jobsid, ...
                                               { 'AttachedFiles' }, ...
                                               { {} } );
                throw(E);
            end
        end

    end


    methods % JobSupport
        function v = getJobProperties( obj, jobsid, jobvariant, propName )
            v = obj.getProperties( jobvariant, jobsid, propName );
        end
        function setJobProperties( obj, jobsid, jobvariant, propName, val )
        % Need to special-case AttachedFiles because this is somewhat
        % complicated. Could generalize this special-casing later if more things
        % need it.
            if iscell( propName )
                afIdx = strcmp( 'AttachedFiles', propName );
                if any( afIdx )
                    % Pick the last one
                    afVal = val{ find( afIdx, 1, 'last' ) };
                    obj.setJobAttachedFiles( jobsid, jobvariant, afVal );

                    % Remove the AttachedFiles from propName
                    propName = propName( ~afIdx );
                    val      = val( ~afIdx );
                end
            elseif isequal( propName, 'AttachedFiles' )
                obj.setJobAttachedFiles( jobsid, jobvariant, val );
                return
            end
            obj.setProperties( jobvariant, jobsid, propName, val );
        end

        function attachFileDataToJob( obj, jobsid, jobvariant, filedata )
            import parallel.internal.apishared.AttachedFiles

            try
                obj.setProperties( jobvariant, jobsid, ...
                                               { 'AttachedFileData' }, ...
                                               { filedata           } );
            catch E
                % revert to "sensible defaults"
                obj.setProperties( jobvariant, jobsid, ...
                                               { 'AttachedFileData' }, ...
                                               { int8([])           } );
                throw(E);
            end
        end

        function tl = getTasks( obj, job, jobsid )
            variantToForce = parallel.internal.types.Variant.Task;
            emptyTaskList  = parallel.task.CJSTask.empty();
            tl = obj.getChildren( job, jobsid, emptyTaskList, variantToForce );
        end

        function tlCell = getTasksByState( obj, job, jobSId, statesCell )
            import parallel.internal.cluster.CJSSupport

            variantToForce = parallel.internal.types.Variant.Task;
            emptyTaskList  = parallel.task.CJSTask.empty();
            tlCell         = obj.getChildrenByState( ...
                job, jobSId, emptyTaskList, variantToForce, statesCell );
        end

        function tasks = buildTasks( obj, job, jobSId, taskFcnCell, numArgsOutArr, argsInCell )
        % Helper function to construct a single array of tasks.
        % - obj: CJSSupport object
        % - job: parallel.job.CJS*Job object
        % - jobSId: job Storage ID
        % - taskFcnCell: cell array of new tasks' 'Function'
        % - numArgsOutArr: array of new tasks' 'NumOutputArguments'
        % - argsInCell: cell array of new tasks' 'InputArguments'
            import parallel.internal.cluster.CJSSupport
            import parallel.internal.cluster.DefaultProperties

            nTasks  = numel( taskFcnCell );
            variant = parallel.internal.types.Variant.Task;
            proxies = obj.Storage.createProxies( jobSId, nTasks );

            tasksCell = cell( 1, nTasks );
            for ti = 1:nTasks
                taskID  = proxies(ti).getEntityID();
                taskSId = iCalculateChildSId( jobSId, variant, taskID );
                task    = job.hBuildChild( variant, taskID, taskSId );

                % Set up the properties for the new task, overriding those that we
                % are told.
                newTaskProps                    = DefaultProperties.getNewTaskPropStruct();
                newTaskProps.Name               = char(proxies(ti).getName);
                newTaskProps.Function           = taskFcnCell{ti};
                newTaskProps.NumOutputArguments = numArgsOutArr(ti);
                newTaskProps.InputArguments     = argsInCell{ti};

                obj.initProperties( variant, taskSId, fieldnames( newTaskProps ), ...
                                    struct2cell( newTaskProps ) );
                % use addOrReplace because an obsolete object may be in the
                % cache using this taskSId.
                obj.Cache.addOrReplace( jobSId, taskSId, task );
                tasksCell{ti} = task;
            end
            tasks = [ tasksCell{:} ];
        end

        function prepareJobForSubmission( obj, job, jobSId )
        % Called when a job is about to be submitted.
            cjsPrepareForSubmission( job, obj, jobSId );
        end

        function [ names, values ] = getJobSubmissionEnvVars( obj, jobSId )
        % Environment variables common to all tasks in a job, or a communicating
        % job.
        % - obj: CJSSupport
        % - jobSId: job storage ID
        % returns:
        % - names: cell array of environment names
        % - values: cell array of environment values
            names = { 'MDCE_STORAGE_CONSTRUCTOR', ... % TODO:later consider where this might live
                      'MDCE_STORAGE_LOCATION', ...
                      'MDCE_JOB_LOCATION' };
            [loc, ctor] = obj.Storage.getSubmissionStrings();
            locurlenc   = parallel.internal.urlencode( loc );
            values      = { ctor, locurlenc, jobSId };
        end

        function prop = getJobSubmissionSetprop( obj, jobSId, taskSIds )
        % build a distcomp.setprop for generic scheduler submission. Most other
        % schedulers should use getJobSubmissionEnvVars.
            [loc, ctor] = obj.Storage.getSubmissionStrings();

            prop = distcomp.setprop();

            prop.pSetStorageStrings( loc, ctor, ...
                                     jobSId, taskSIds );
        end

        function [ names, values ] = getTaskSubmissionEnvVars( ~, taskSId )
        % Task-specific environment variables for submission
        % - obj: CJSSupport
        % - taskSId: task Storage ID
        % returns:
        % - names: cell array of environment names
        % - values: cell array of environment values
            names  = { 'MDCE_TASK_LOCATION' };
            values = { taskSId };
        end

        function serializeForSubmission( obj, jobSId, jobVariant )
        % Simply pass through to the storage version
            obj.Storage.serializeForSubmission( iSerializerStruct( jobVariant, jobSId ) );
        end

        function [jobLog, relLog] = getCommJobLogFile( obj, jobSId )
        % Get a communicating job log file location
        % - obj: CJSSupport
        % - jobSId: storage ID of communicating job
        % returns:
        % - jobLog - absolute location on the client of the log file
        % - relLog - same thing relative to storage root
            relLog = iCorrectSlash( fullfile( jobSId, [jobSId '.log'] ) );
            jobLog = iCorrectSlash( fullfile( obj.getRootStorageDir(), relLog ) );
        end
        function [taskLog, relLog] = getIndJobTaskLogFile( obj, ~, taskSId )
        % Get a independent task file location
        % - obj: CJSSupport
        % - jobSId: storage ID of independent job (unused)
        % - taskSId: storage ID of task
        % returns:
        % - taskLog - absolute location on the client of the log file
        % - relLog - same thing relative to storage root
            relLog  = iCorrectSlash( [taskSId '.log'] );
            taskLog = iCorrectSlash( fullfile( obj.getRootStorageDir(), relLog ) );
        end
        function jobDir = getJobDirOnClient( obj, jobSId )
        % Return the directory on the client containing job files
        % - obj: CJSSupport
        % - jobSId: job storage ID
        % returns:
        % - jobDir: directory on the client
            jobDir = iCorrectSlash( fullfile( obj.getRootStorageDir(), jobSId ) );
        end
        function jobDir = getJobDirForOs( obj, jobSId, os )
        % Return the directory on the client containing job files
        % - obj: CJSSupport
        % - jobSId: job storage ID
        % returns:
        % - jobDir: directory on the client
            dataLocStruct = obj.Storage.getStorageLocationStruct();
            root = '';
            switch os
              case 'windows'
                root = dataLocStruct.pc;
              case 'unix'
                root = dataLocStruct.unix;
            end
            if isempty( root )
                error(message('parallel:cluster:StorageLocationOnCluster', os));
            end
            jobDir = fullfile( root, jobSId );
            jobDir = parallel.internal.apishared.FilenameUtils.fixSlashes( jobDir, os );
        end
        function location = getRootStorageDir( obj )
        % Return the root directory of job/task storage on the client
        % - obj: CJSSupport object
        % returns:
        % - location: directory on client disk of jobs/tasks
        % TODO:later could we conceive of storageLocation.remote or similar?
            sls = obj.getRootStorageStruct();
            if ispc
                location  = sls.pc;
            else
                location  = sls.unix;
            end
            location = iCorrectSlash( location );
        end

        function sls = getRootStorageStruct( obj )
        % getRootStorageStruct - return the JobStorageLocation in struct form.
            sls = obj.Storage.getStorageLocationStruct();
        end

        function [logText, logExists] = readCommJobLogFile( obj, jobSId, jobVar )
        % Read a communicating job logfile, given a scalar jobSId
            jobState = obj.getJobProperties( jobSId, jobVar, 'StateEnum' );
            fileName = obj.getCommJobLogFile( jobSId );
            [logText, logExists] = iReadLogOrWarn( jobState, fileName );
        end
        function [logText, logExists] = readIndJobTaskLogFile( obj, jobSId, jobVar, taskSId )
        % Read an independent job task logfile, given jobSId and taskSId
            jobState = obj.getJobProperties( jobSId, jobVar, 'StateEnum' );
            fileName = obj.getIndJobTaskLogFile( jobSId, taskSId );
            [logText, logExists] = iReadLogOrWarn( jobState, fileName );
        end
        function [logText, logExists] = readSpecificLogFile( ~, jobState, fileName )
            [logText, logExists] = iReadLogOrWarn( jobState, fileName );
        end
        function logText = readTaskJavaLogsIfPresent( obj, ~, taskSIds, taskIDs )
        % Read any java log files that may have been created. Args:
        % - obj: clusterStorage
        % - jobSId: <unused...>
        % - taskSIds: cell array of task SIds
        % - taskIDs: *cell array* of task IDs (storage cannot deduce that!)
            javaLogBaseNames = cellfun( @(x) obj.getTaskJavaLogBaseIfPossible( x ), ...
                                        taskSIds, 'UniformOutput', false );
            logText      = cellfun( @iReadJavaTaskLogIfPresent, javaLogBaseNames, ...
                                    taskIDs, 'UniformOutput', false );
            logText      = sprintf( '%s', logText{:} );
        end
        function logFile = getTaskJavaLogBaseIfPossible( obj, taskSId )
            logFile = fullfile( obj.getRootStorageDir(), ...
                                sprintf( '%s.java.log', taskSId ) );
        end
        function destroyJob( obj, clusterSId, jobSId )
        % Remove the job (and tasks) from disk and Cache
            obj.Cache.remove( clusterSId, jobSId );
            obj.Storage.destroyLocation( jobSId );
        end
        function aLock = lock( obj, jobSId, jobVariant )
            aLock = obj.Serializer.lock( iSerializerStruct( jobVariant, jobSId ) );
        end
        function release( obj, aLock )
            obj.Serializer.release( aLock );
        end
    end

    methods % TaskSupport
        function v = getTaskProperties( obj, tasksuuids, propName )
            v = obj.getProperties( parallel.internal.types.Variant.Task, ...
                                   tasksuuids, propName );
        end
        function setTaskProperties( obj, tasksuuids, propName, val )
            obj.setProperties( parallel.internal.types.Variant.Task, ...
                               tasksuuids, propName, val );
        end
        function destroyTask( obj, jobSId, taskSId )
        % Remove the task from disk and Cache
            obj.Cache.remove( jobSId, taskSId );
            obj.Storage.destroyLocation( taskSId );
        end
    end

    methods ( Static, Access = private )
        function [mappedCell, isCell, postGetFcnCell, preSetFcnCell] = ...
                mapCJSProperties( propNames )
            % Look up in the static maps everything needed to deal with various
            % properties. propNames must be one or more API2 property names.

            import parallel.internal.cluster.CJSSupport;

            persistent MAP
            if isempty( MAP )
                % Cache because this method is called many times, and MCOS
                % property access is slow.
                MAP = parallel.internal.cluster.CJSSupport.PropertyMap;
            end

            isCell = iscell( propNames );
            if ~isCell;
                propNames = { propNames };
            end

            nProps         = numel( propNames );
            mappedCell     = cell( size( propNames ) );
            postGetFcnCell = cell( size( propNames ) );
            preSetFcnCell  = cell( size( propNames ) );
            for ii = 1:nProps
                vals = MAP( propNames{ii} );
                [ mappedCell{ii}, postGetFcnCell{ii}, preSetFcnCell{ii} ] = ...
                    deal( vals{:} );
            end
        end

        function ctorName = ctorNameFromVariant( variant )
        % Given a Variant, return the API1 constructor name
        % - variant: scalar Variant
        % returns:
        % - ctorName: API1 constructor name
            map      = parallel.internal.cluster.CJSSupport.VariantToCtor;
            match    = find( [map{:,1}] == variant );
            assert( numel(match) == 1 );
            ctorName = map{match,2};
        end

        function variant = variantFromCtorName( ctorName )
        % Map an API1 constructor to a Variant
        % - ctorName: API1 constructor name
        % returns:
        % - variant: scalar Variant
            map     = parallel.internal.cluster.CJSSupport.VariantToCtor;
            match   = find( strcmp( ctorName, map(:,2) ) );
            assert( numel(match) == 1 );
            variant = map{match,1};
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tf = iIsJobVariant( variant )
    persistent JOB_VARS
    if isempty( JOB_VARS )
        import parallel.internal.types.Variant
        JOB_VARS = { Variant.IndependentJob, ...
                     Variant.CommunicatingSPMDJob, ...
                     Variant.CommunicatingPoolJob };
    end
    switch variant
      case JOB_VARS
        tf = true;
      otherwise
        tf = false;
    end
end
function tf = iIsTaskVariant( variant )
    persistent TASK_VAR
    if isempty( TASK_VAR )
        import parallel.internal.types.Variant
        TASK_VAR = Variant.Task;
    end
    tf = variant == TASK_VAR;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build a child StorageID
function ret = iCalculateChildSId( parentSId, childVariant, childID )

    % Disable these checks as they are rather slow, and only internal code calls
    % this method.

    % validateattributes( parentSId, {'char'}, {} );
    % validateattributes( childVariant, {'parallel.internal.types.Variant'}, {'scalar'} );
    % validateattributes( childID, {'double'}, {'scalar'} );
    if iIsJobVariant( childVariant )
        if isempty( parentSId )
            % Get here because a cluster's 'SId' is ''.
            ret = sprintf( 'Job%d', childID );
        else
            % Never get here because cluster SId is empty.
            assert( false );
        end
    elseif iIsTaskVariant( childVariant )
        ret = sprintf( '%s/Task%d', parentSId, childID );
    else
        error(message('parallel:cluster:CJSClusterBadObjectType', variant.Name));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Return a structure to mimic a job/task for the fileserializer.
function ret = iSerializerStruct( variant, sId )
    import parallel.internal.types.Variant

    % Disable these checks as they are rather slow, and only internal code calls
    % this method.

    if iIsJobVariant( variant )
        type = 'job';
    elseif iIsTaskVariant( variant )
        type = 'task';
    else
        error(message('parallel:cluster:CJSClusterBadObjectType', variant.Name));
    end
    ret = struct( 'pGetEntityType', type, ...
                  'pGetEntityLocation', sId );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iGetPropMap - define the property map from API2 property name to the name as
% understood by the serializer layer, including pre-set and post-get functions
function propMap = iGetPropMap()
    noop              = [];
    statePostGetFcn   = @parallel.internal.types.States.fromName;
    statePreSetFcn    = @(x) x.Name;
    addPathsPreSetFcn = @iMaybeReplaceWithUnc;
    workerToStruct    = @iGetWorkerStruct;
    structToWorker    = @parallel.cluster.CJSWorker.hBuildFromStruct;
    tmp = { ...
        ... % API2 name          , serializer name              , postGetFcn, preSetFcn
        'AttachedFiles'      , 'filedependencies'           , @iColumnify, noop;
        'AttachedFileData'   , 'filedata'                   , noop, noop;
        'AttachedFilePaths'  , 'filedeppaths'               , @iColumnify, noop;
        'AdditionalPaths'    , 'pathdependencies'           , @iColumnify, addPathsPreSetFcn;
        'AutoAttachFiles'    , 'autoattachfiles'            , noop, noop;
        'DependentFiles'     , 'autoattachedfiles'          , @iColumnify, noop;
        'CaptureDiary'       , 'capturecommandwindowoutput' , noop, noop;
        'CreateTime'         , 'createtime'                 , noop, noop;
        'Diary'              , 'commandwindowoutput'        , noop, noop;
        'Error'              , 'errorstruct'                , noop, noop;
        'ErrorIdentifier'    , 'erroridentifier'            , noop, noop;
        'ErrorMessage'       , 'errormessage'               , noop, noop;
        'FinishTime'         , 'finishtime'                 , noop, noop;
        'Function'           , 'taskfunction'               , noop, noop;
        'InputArguments'     , 'argsin'                     , noop, noop;
        'JobData'            , 'jobdata'                    , noop, noop;
        'JobSchedulerData'   , 'jobschedulerdata'           , noop, noop;
        'ProductKeys'        , 'productkeys'                , noop, noop;
        'ExecutionMode'      , 'execmode'                   , noop, noop;
        'MaxNumWorkers'      , 'maxworkers'                 , noop, noop;
        'MinNumWorkers'      , 'minworkers'                 , noop, noop;
        'Name'               , 'name'                       , noop, noop;
        'NumOutputArguments' , 'nargout'                    , noop, noop;
        'OutputArguments'    , 'argsout'                    , noop, noop;
        'StartTime'          , 'starttime'                  , noop, noop;
        'StateEnum'          , 'state'                      , statePostGetFcn, statePreSetFcn;
        'SubmitTime'         , 'submittime'                 , noop, noop;
        'Tag'                , 'tag'                        , noop, noop;
        'ApiTag'             , 'apitag'                     , noop, noop;
        'Username'           , 'username'                   , noop, noop;
        'Version'            , 'version'                    , noop, noop;
        'Worker'             , 'worker'                     , structToWorker, workerToStruct;
          };
    propMap = containers.Map;
    for ii = 1:size(tmp, 1)
        propMap(tmp{ii,1}) = tmp(ii,2:end);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iVariantToCtor - define the mapping from Variant to the API1 constructor
% string.
function values = iVariantToCtor()
    import parallel.internal.types.Variant;
    values = { Variant.IndependentJob       , 'distcomp.simplejob';
               Variant.CommunicatingSPMDJob , 'distcomp.simpleparalleljob';
               Variant.CommunicatingPoolJob , 'distcomp.simplematlabpooljob';
               Variant.Task                 , 'distcomp.simpletask' };
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ensure value is a column, even in the case of empty data.
function val = iColumnify( val )
    val = reshape( val, numel( val ), 1 );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simply defer to apishared.FilenameUtils.
function string = iCorrectSlash(string)
    string = parallel.internal.apishared.FilenameUtils.fixSlashes( string );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iMaybeReplaceWithUnc - called prior to setting AdditionalPaths
function val = iMaybeReplaceWithUnc( val )
    if ispc
        val = cellfun( @dctReplaceDriveWithUNCPath, val, 'UniformOutput', false );
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iGetWorkerStruct - if the value is non-empty, return the worker struct
function val = iGetWorkerStruct( val )
    if isa( val, 'parallel.cluster.CJSWorker' )
        val = val.hGetStruct();
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read a logfile, or warn if the job is in a terminal state and the logfile
% cannot be found.
function [logText, fileExists] = iReadLogOrWarn( jobState, fileName )
    import parallel.internal.types.States;
    fileExists = ( exist( fileName, 'file' ) == 2 );
    jobTerminal = jobState.isTerminal();
    if fileExists
        logText = sprintf( 'LOG FILE OUTPUT:\n%s', iReadLogText( fileName ) );
    else
        if jobTerminal
            % TODO:message catalog?
            logText = sprintf( 'Unable to find log file "%s".\n', fileName );
        else
            logText = '';
            warning(message('parallel:cluster:NoLogFilePresent'));
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read one or more java logs if they exist. We are handed
% 'Job3/Task7.java.log', and we should read and return
% 'Job3/Task7.java.log.0', 'Job3/Task7.java.log.1', ...
function logText = iReadJavaTaskLogIfPresent( fileNameBase, taskID )

    % list all files with the correct initial piece
    files       = dir( [fileNameBase, '.*'] );

    % Put the file names back together with the directory part
    fileNames   = strcat( [ fileparts( fileNameBase ), filesep ], { files.name } );

    % Find those files which end in .[0-9] - others are .3.lck or similar.
    filesMatch  = cellfun( @any, regexp( fileNames, '^.*\.[0-9]+$' ) );

    if any( filesMatch )
        logTextCell = cellfun( @iReadLogText, fileNames(filesMatch), ...
                               'UniformOutput', false );

        % TODO:message catalog?
        logText = sprintf( '\nJAVA LOG FILE OUTPUT FOR TASK %d:\n%s', ...
                           taskID, sprintf( '%s\n', logTextCell{:} ) );
    else
        logText = '';
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Read a log file from disk, error if file cannot be read.
function logText = iReadLogText( fileName )
    [fHandle, msg] = fopen( fileName, 'rt' );
    if fHandle == -1
        error(message('parallel:cluster:CouldNotOpenFile', fileName, msg));
    end
    handleCloser = onCleanup( @() fclose(fHandle) );
    logText = fread( fHandle, Inf, 'char' );
    logText( logText == sprintf( '\r' ) ) = [];
    logText = char( logText.' );
end
