% MJSJobMethods - helpers common to MJS Job classes

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) MJSJobMethods
    methods ( Static )
        function tasks = createTask( job, jobSupport, jobSId, ...
                                     allowMultipleTasks, fcn, nout, varargin )
        % This method engenders the knowledge that only jobs in states >=
        % 'pending' but not 'finished' may have tasks added to them.
            import parallel.internal.apishared.TaskCreation
            import parallel.internal.cluster.DefaultProperties
            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.internal.types.States

            [taskFcns, numArgsOut, argsIn, setArgs] = ...
                TaskCreation.createTaskArgCheck(numel(job), fcn, nout, varargin{:});

            jobState = job.StateEnum;
            if jobState < States.Pending || jobState >= States.Finished
                error(message('parallel:job:MJSJobAddTaskState'));
            end

            if ~allowMultipleTasks && numel( taskFcns ) > 1
                error(message('parallel:job:BadNumberOfTasksToCreate', job.Type));
            end

            % Pass on the 'Profile' from the Cluster, prepend this.
            if ~isempty( job.Parent.Profile )
                setArgs = [ 'Profile', job.Parent.Profile, setArgs ];
            end
            [names, values] = ConstructorArgsHelper.resolveTaskBuildArguments( ...
                ?parallel.task.MJSTask, setArgs{:} );

            taskInfoCell = cell( 1, numel( taskFcns ) );
            for ti = 1:numel( taskFcns )
                t = DefaultProperties.getNewTaskPropStruct();
                t.Function           = taskFcns{ti};
                t.NumOutputArguments = numArgsOut(ti);
                t.InputArguments     = argsIn{ti};
                % NB: following not defined in DefaultProperties
                t.MaximumRetries     = 1;
                t.Timeout            = []; % special value, see iBuildTaskInfo
                t.Name               = ''; % implies: use default name
                for jj = 1:numel( names )
                    t.(names{jj}) = values{jj};
                end
                % Handle MaxNumRetries specially
                if isfield( t, 'MaxNumRetries' )
                    t.MaximumRetries = t.MaxNumRetries;
                    t = rmfield( t, 'MaxNumRetries' );
                end
                
                % iBuildTaskInfo tells us which fields in 't' have not been
                % handled.
                [taskInfoCell{ti}, unhandledNames] = iBuildTaskInfo( t );

                if iContainsCallbacks( names )
                    % Create a ListenerInfo array from the ProxyToUddAdaptor and
                    % put it in the TaskInfo holder. This has to happen before the
                    % task is created so that callbacks are fired (g794117).
                    listenerInfoArray = jobSupport.createListenerInfoArrayForAllEvents;
                    taskInfoCell{ti}.Info.setListenerInfo( listenerInfoArray );
                end
            end

            tasks = jobSupport.buildTasks( job, jobSId, taskInfoCell );

            for ti = 1:numel( taskFcns )
                % We now need to apply the 'setArgs' for those things that
                % weren't already handed in iBuildTaskInfo.
                setNames = intersect( unhandledNames, names );
                try
                    for jj = 1:numel( setNames )
                        tasks(ti).(setNames{jj}) = t.(setNames{jj});
                    end
                catch E
                    % It's somewhat tricky to get here since the
                    % resolveTaskBuildArguments stage has already checked
                    % constraints.
                    delete(tasks(ti:end));
                    throwAsCaller( E );
                end
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Return a structure containing the TaskInfo object, and a 'backing' object that
%is required simply for lifetime management of the byte buffers.  We also return
%a list of names that weren't used in the TaskInfo, as these will need to be
%set.
function [ti, unhandledNames] = iBuildTaskInfo( obj )
    import com.mathworks.toolbox.distcomp.workunit.TaskInfo;
    import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem;

    allFieldNames = fieldnames( obj );
    handledNames  = { 'InputArguments', 'Function', 'CaptureDiary', ...
                      'MaximumRetries', 'NumOutputArguments', ...
                      'Timeout', 'Name' };
    % If we get any worker-side 'unhandledNames', we have a problem. Client-side
    % stuff is fine.
    unhandledNames = setdiff( allFieldNames, handledNames );

    % handle 'special' Timeout value
    if isempty( obj.Timeout )
        % pick default
        timeout = intmax('int64');
    else
        % Scale user value in seconds to milliseconds for TaskInfo
        timeout = obj.Timeout * 1000;
    end

    logLevel = 0;

    backing{1} = distcompserialize(obj.InputArguments);
    backing{2} = distcompserialize(obj.Function);
    inData   = ByteBufferItem(distcompMxArray2ByteBuffer(backing{1}));
    funcData = ByteBufferItem(distcompMxArray2ByteBuffer(backing{2}));
    funcName = parallel.internal.cluster.generateTaskFunctionString( obj.Function );
    ti.Info  = TaskInfo( obj.Name, ...
                         timeout, ...
                         obj.CaptureDiary, ...
                         obj.MaximumRetries, ...
                         obj.NumOutputArguments, ...
                         inData, ...
                         funcData, ...
                         funcName, ...
                         logLevel, ...
                         [] );
    ti.Backing = backing;
end

function hasCallbacks = iContainsCallbacks( names )
    callbackNames = { 'RunningFcn', 'FinishedFcn' };
    hasCallbacks = false;
    if any( ismember( callbackNames, names ) )
        hasCallbacks = true;
    end
end
