% MJSQueuedCallbackMixin

%   Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) MJSQueuedCallbackMixin < parallel.internal.cluster.MJSCallbackMixin

    properties ( Dependent )
        %QueuedFcn Callback executed when the job is queued
        QueuedFcn
    end
        
    properties( Access = private )
        % The "raw" callback function specified by the user.
        UserSpecifiedQueuedFcn
        
        % Listens to Queued event and calls the Callback fcn
        CallbackListener
    end
    
    events( Hidden, ListenAccess = public, NotifyAccess = protected )
        Queued
    end
    
    properties ( Constant, Access = protected )
        QueuedEventName = 'Queued';
    end

    methods
        function obj = MJSQueuedCallbackMixin() 
            obj.CallbackListener = event.listener( obj, obj.QueuedEventName, obj.NoOpCallback );
            obj.CallbackListener.Enabled = false;
        end
        
        function val = get.QueuedFcn( obj )
            val = obj.UserSpecifiedQueuedFcn;
        end
        
        function set.QueuedFcn( obj, val )
            try
                obj.setCallbackFcn( 'QueuedFcn', obj.CallbackListener, val );
                obj.UserSpecifiedQueuedFcn = val;
            catch err
                % Get the internal details off the stack
                throw(err);
            end
        end
    end
    
end
