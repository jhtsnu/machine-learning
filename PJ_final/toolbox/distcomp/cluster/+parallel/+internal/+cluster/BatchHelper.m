% Helper Class for activities related to batch. This instance provides the API1
% behaviour.

%  Copyright 2010-2012 The MathWorks, Inc.

classdef BatchHelper < parallel.internal.cluster.AbstractBatchHelper

    properties ( Constant, GetAccess = protected )
        % We never allow vectorized batch
        AllowVectorizedBatch = false;
    end
    
    methods
        function obj = BatchHelper( varargin )
            obj@parallel.internal.cluster.AbstractBatchHelper( varargin{:} );
        end

        function job = doBatch(obj, sched)
        % doBatch Actually creates and submits the job for the batch
        % command/method to the provided scheduler.  Decide which job
        % constructor to use.
            if obj.PoolSize > 0
                jobConstructor = @createMatlabPoolJob;
            else
                jobConstructor = @createJob;
            end
            % Create a normal DCT job with the appropriate PoolSize
            jobPVPairs = obj.getCreateJobInputs;
            job = jobConstructor(sched, jobPVPairs{:});

            % If anything fails from here we need to destroy the job as it won't be
            % passed back to the user
            try
                [functionToRun, numArgsOut, allArgsIn, taskPVPairs] = ...
                    obj.getCreateTaskInputs( 'CaptureCommandWindowOutput', ...
                                             'Configuration' );
                % Create a task that will call executeScript
                job.createTask(functionToRun, numArgsOut, allArgsIn, taskPVPairs{:});
                % Submit the job
                job.submit;
            catch exception
                % Any error - we need to destroy the job
                [~, undoc] = pctconfig();
                if ~undoc.preservejobs
                    job.destroy
                end
                rethrow(exception)
            end
        end

        function doDebugDisplay(obj)
        % doDebugDisplay For debug purposes only.
        % Display all the properties in a nice format.  Use a custom method name
        % so as not to override the default display behaviour (since this
        % displays private properties)
            fprintf('\tProperty\t\tUserSpec?\tValue\n');
            fprintf('\t--------\t\t---------\t-----\n');
            fprintf('\tConfiguration\t\t%d\t\t%s\n',  obj.UserSpecifiedConfiguration, obj.Configuration);
            fprintf('\tPoolSize\t\t%d\t\t%d\n', obj.UserSpecifiedPoolSize, obj.PoolSize);
            fprintf('\tWorkerCWD\t\t%d\t\t%s\n', obj.UserSpecifiedWorkerCWD, obj.WorkerCWD);
            fprintf('\tJobName\t\t\t%d\t\t%s\n', obj.UserSpecifiedJobName, obj.JobName);
            fprintf('\tAutoAttachFiles\t\t\t%d\t\t%d\n', obj.UserSpecifiedAutoAttachFiles, obj.AutoAttachFiles);
            fprintf('\tFileDependencies\t%d\t\t%s\n', obj.UserSpecifiedFileDependencies, sprintf('%s ', obj.FileDependencies{:}));
            fprintf('\tPathDependencies\t%d\t\t%s\n', obj.UserSpecifiedPathDependencies, sprintf('%s ', obj.PathDependencies{:}));
            fprintf('\tCaptureDiary\t\t%d\t\t%d\n', obj.UserSpecifiedCaptureDiary, obj.CaptureDiary);

            fprintf('\n\tIsRunScript: %d\n', obj.IsRunScript);
            fprintf('\t%d Tasks:\n\n', numel(obj.TasksToRun))

            if obj.IsRunScript
                fprintf('\tScriptName\tWorkspace Fields\n');
                fprintf('\t----------\t----------------\n');
            else
                fprintf('\tFunction\tnargout\tnargin\n');
                fprintf('\t--------\t-------\t------\n');
            end

            for ii = 1:numel(obj.TasksToRun)
                task = obj.TasksToRun(ii);
                if obj.IsRunScript
                    wkspacefields = fields(task.WorkspaceIn);
                    fprintf('\t%s\t%s\n', task.ScriptName, sprintf('%s ', wkspacefields{:}));
                else
                    functionName = task.FunctionToRun;
                    if isa(functionName, 'function_handle')
                        functionName = func2str(functionName);
                    end
                    fprintf('\t%s\t%d\t%d\n', functionName, task.NumArgsOut, numel(task.ArgsIn));
                end
            end
        end
    end

    methods (Access = private)
        % -------------------------------------------------------------------------
        % getCreateJobInputs
        % -------------------------------------------------------------------------
        % Determines the correct job constructor and creates the PV-pairs that
        % should be used with that constructor
        function pvPairs = getCreateJobInputs(obj)
        % It is OK to set the Tag first because this it is not a
        % configurable property, so there is no danger of the
        % configuration changing this value.
            pvPairs = {'Tag', obj.JobTag};
        % Configuration must be before the remaining properties, otherwise
        % values set in the configuration will override any user-specified ones.
            if obj.UserSpecifiedConfiguration || ~isempty(obj.Configuration)
                pvPairs = [pvPairs, 'Configuration', obj.Configuration];
            end
            % Only add the other job-related properties if the user specified them, or they are not
            % empty (i.e. we set them).  This helps us distinguish between the case where values
            % are empty because the user did not specify them, or if the user explicitly set them
            % to empty.
            if obj.UserSpecifiedJobName || ~isempty(obj.JobName)
                pvPairs = [pvPairs, 'Name', obj.JobName];
            end
            if obj.UserSpecifiedFileDependencies || ~isempty(obj.FileDependencies)
                pvPairs = [pvPairs, 'FileDependencies', {obj.FileDependencies}];
            end
            if obj.UserSpecifiedPathDependencies || ~isempty(obj.PathDependencies)
                pvPairs = [pvPairs, 'PathDependencies', {obj.PathDependencies}];
            end
            if obj.PoolSize > 0
                pvPairs = [pvPairs, 'MaximumNumberOfWorkers', obj.PoolSize+1, ...
                                    'MinimumNumberOfWorkers', obj.PoolSize+1];
            end
        end
    end
end
