
%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden) MJSRunningCallbackMixin < parallel.internal.cluster.MJSCallbackMixin

    properties ( Dependent )
        %RunningFcn Callback executed when the job or task starts running
        RunningFcn
    end
    
    properties( Access = private )
        % The "raw" callback function specified by the user.
        UserSpecifiedRunningFcn
        
        % Listens to Running event and calls the Callback fcn
        CallbackListener
    end
    
    events( Hidden, ListenAccess = public, NotifyAccess = protected )
        Running
    end

    properties ( Constant, Access = protected )
        RunningEventName = 'Running';
    end

    methods
        function obj = MJSRunningCallbackMixin() 
            obj.CallbackListener = event.listener( obj, obj.RunningEventName, obj.NoOpCallback );           
            obj.CallbackListener.Enabled = false;
        end
        
        function val = get.RunningFcn( obj )
            val = obj.UserSpecifiedRunningFcn;
        end
        
        function set.RunningFcn( obj, val )
            try
                obj.setCallbackFcn( 'RunningFcn', obj.CallbackListener, val );
                obj.UserSpecifiedRunningFcn = val;
            catch err
                % Get the internal details off the stack
                throw(err);
            end
        end
    end
    
end
