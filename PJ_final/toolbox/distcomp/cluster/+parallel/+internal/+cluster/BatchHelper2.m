% Helper Class for activities related to batch in API2

%  Copyright 2010-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) BatchHelper2 < parallel.internal.cluster.AbstractBatchHelper

    properties ( Constant, GetAccess = protected )
        % We never allow vectorized batch
        AllowVectorizedBatch = false;
    end

    methods
        function obj = BatchHelper2( varargin )
            obj@parallel.internal.cluster.AbstractBatchHelper( varargin{:} );
        end
        function job = doBatch(obj, sched)

            validateattributes( sched, {'parallel.Cluster'}, {'scalar'} );
            if obj.PoolSize > 0
                jobConstructor = @(cluster, args) ...
                    cluster.createCommunicatingJob( args{:}, 'Type', 'Pool' );
            else
                jobConstructor = @(cluster, args) ...
                    cluster.createJob( args{:} );
            end
            jobPVPairs = obj.getCreateJobInputs;
            job        = jobConstructor(sched, jobPVPairs);

            % Mirror the Tag through to ApiTag.
            job.hSetProperty( 'ApiTag', job.Tag );

            % If anything fails from here we need to destroy the job as it won't be
            % passed back to the user
            try
                % No need for Profile, applies automatically.
                [functionToRun, numArgsOut, allArgsIn, taskPVPairs] = ...
                    obj.getCreateTaskInputs( 'CaptureDiary', '' );
                % Create a task that will call executeScript
                job.createTask(functionToRun, numArgsOut, allArgsIn, taskPVPairs{:});
                % Submit the job
                job.submit;
            catch exception
                % Any error - we need to destroy the job
                [~, undoc] = pctconfig();
                if ~undoc.preservejobs
                    job.delete();
                end
                rethrow(exception)
            end
        end
    end
    methods (Access = private)
        function pvPairs = getCreateJobInputs(obj)
        % getCreateJobInputs - Determines the correct job constructor and creates the
        % PV-pairs that should be used with that constructor

        % It is OK to set the Tag first because this it is not a
        % configurable property, so there is no danger of the
        % configuration changing this value.
        % Configuration must be before the remaining properties, otherwise
        % values set in the configuration will override any user-specified ones.
            pvPairs = { 'Tag', obj.JobTag };

            % NB: no need for Profile, applies automatically

            % Only add the other job-related properties if the user specified them, or they are not
            % empty (i.e. we set them).  This helps us distinguish between the case where values
            % are empty because the user did not specify them, or if the user explicitly set them
            % to empty.
            if obj.UserSpecifiedJobName || ~isempty(obj.JobName)
                pvPairs = [pvPairs, 'Name', obj.JobName];
            end
            if obj.UserSpecifiedAutoAttachFiles
                pvPairs = [pvPairs, 'AutoAttachFiles', obj.AutoAttachFiles];
            end
            if obj.UserSpecifiedFileDependencies || ~isempty(obj.FileDependencies)
                pvPairs = [pvPairs, 'AttachedFiles', {obj.FileDependencies}];
            end
            if obj.UserSpecifiedPathDependencies || ~isempty(obj.PathDependencies)
                pvPairs = [pvPairs, 'AdditionalPaths', {obj.PathDependencies}];
            end
            if obj.PoolSize > 0
                workerNumLims = repmat( obj.PoolSize + 1, 1, 2 );
                pvPairs = [pvPairs, 'NumWorkersRange', workerNumLims];
            end
        end
    end
end
