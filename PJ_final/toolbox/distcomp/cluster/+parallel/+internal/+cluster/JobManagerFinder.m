
%   Copyright 2012 The MathWorks, Inc.

classdef JobManagerFinder

    methods ( Static )
        function jmprx = getOneJobManagerProxy( host, name )
            jmprx = iGetJobManagerProxy( host, name);
        end

        function proxyManagers = getAllJobManagerProxies( host, name )
            proxyManagers = iGetJobManagerProxies( host, name );
        end

        function proxyManagers = filterProxiesByVersion( proxyManagers )
            proxyManagers = iTrimBadVersionManagers( proxyManagers );
        end
    end

end


% --------------------------------------------------------------------------
% iGetJobManagerProxy - returns one proxy or errors
function jmprx = iGetJobManagerProxy( host, name )
    proxyManagers = iGetJobManagerProxies( host, name );
    [goodManagers, badManagers] = iTrimBadVersionManagers( proxyManagers );
    jmprx         = iGetOneProxyOrError( goodManagers, badManagers, host, name );

    % Make sure cached proxies are disposed of at MCR shutdown
    jmprx.addShutdownHookForCurrentMCR();
end

% --------------------------------------------------------------------------
% iGetJobManagerProxies
function proxyManagers = iGetJobManagerProxies( host, name )
    import com.mathworks.toolbox.distcomp.service.ServiceAccessor
    try
        proxyManagers = ServiceAccessor.getJobManagers( host, name );
    catch err
        proxyManagers = [];
        iHandleServiceAccessorException(err);
    end
end

% --------------------------------------------------------------------------
% iGetOneProxyOrError - if the resulting list of proxies is exactly 1 element
% long, return that or else error.
function proxy = iGetOneProxyOrError( proxyManagers, badProxies, host, name )
    ex = MException.empty();
    switch numel( proxyManagers )
      case 0
        ex = iCheckBadProxies( badProxies, host, name );
      case 1
        % ok
      otherwise
        descriptionFcn  = @(jmp)sprintf('Name: %s, Host: %s', char(jmp.getName), char(jmp.getHostName) );
        descriptionCell = arrayfun( descriptionFcn, proxyManagers, 'UniformOutput', false );
        descriptionStr  = sprintf('%s\n', descriptionCell{:});
        ex = MException( 'parallel:cluster:MJSMultipleFound', ...
                         'Multiple MJS clusters were found:\n%s', descriptionStr );
    end
    if ~isempty( ex )
        throwAsCaller( ex );
    end
    proxy = proxyManagers(1);
end

% --------------------------------------------------------------------------
% iCheckBadProxies - No proxies were found, check the bad proxies' versions
% and return an appropriate error.
function ex = iCheckBadProxies( badProxies, host, name )
    import com.mathworks.toolbox.distcomp.util.Version;
    if ~isempty( name )
        badProxies = iFilterProxiesByName( badProxies, name );
    end
    if isempty( badProxies )
        if isempty( host ) && isempty( name )
            ex = MException(message('parallel:cluster:MJSNotFound'));
        elseif ~isempty( host ) && isempty( name )
            ex = MException(message('parallel:cluster:MJSNotFoundOnHost', host));
        elseif ~isempty( name ) && isempty ( host )
            ex = MException(message('parallel:cluster:MJSNotFoundWithName', name));
        else
            ex = MException(message('parallel:cluster:MJSNotFoundFullySpecified', name, host));
        end
    else
        descriptionFcn = @(jmp) getString( message( 'parallel:cluster:MJSClusterRunningVersion', ...
            char(jmp.getName), char(jmp.getHostName), char(jmp.getVersionString) ) );
        descriptionCell = arrayfun( descriptionFcn, badProxies, 'UniformOutput', false );
        descriptionStr = sprintf('%s\n', descriptionCell{:});
        if isempty( host ) && isempty( name )
            ex = MException(message('parallel:cluster:MJSIncorrectVersionFound', char( Version.VERSION_STRING )));
        elseif ~isempty( host ) && isempty( name )
            ex = MException(message('parallel:cluster:MJSIncorrectVersionFoundOnHost', char( Version.VERSION_STRING ), host, descriptionStr));
        else
            ex = MException(message('parallel:cluster:MJSIncorrectVersionFoundWithName', char( Version.VERSION_STRING ), name, descriptionStr));
        end
    end
end

function proxyManagers = iFilterProxiesByName( proxyManagers, name )
    OK = arrayfun( @(pm) strcmp( char(pm.getName), name ), proxyManagers );
    proxyManagers = proxyManagers( OK );
end

% --------------------------------------------------------------------------
% iTrimBadVersionManagers - remove proxyManagers that fail the version check
function [goodManagers, badManagers] = iTrimBadVersionManagers( proxyManagers )
    OK = arrayfun( @(pm) logical( pm.checkVersion() ), proxyManagers );
    goodManagers = proxyManagers( OK );
    badManagers = proxyManagers( ~OK );
end

% --------------------------------------------------------------------------
function iHandleServiceAccessorException(err)
% c.f. iHandleServiceAccessorException in findResource.

    import com.mathworks.toolbox.distcomp.service.ServiceAccessor

    exception = ServiceAccessor.getStashedException();

    if ~isempty(exception) && ...
            isa(exception, 'com.mathworks.toolbox.distcomp.util.HasMatlabErrorId')
        err = MException( char(exception.getMatlabErrorId()), ...
                          '%s', char(exception.getLocalizedMessage()) );
    end
    throwAsCaller( err );
end
