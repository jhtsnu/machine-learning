% MJSMethods - a wrapper object that knows how to modify data prior to setting
% on one of the 'access proxy' objects used by MJS.
%
% There are static access methods to get hold of the right MJSMethods object for
% your combination of object type (expressed as a Variant), and the property
% name you're interested in.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Sealed, Hidden ) MJSMethods < handle
    properties ( GetAccess = private, SetAccess = immutable )
        NeedsUUID  % Does GetFcn/SetFcn require the UUID argument
        GetFcn     % function to invoke on the access to get a value
        PostGetFcn % function to transform the return value of GetFcn
        SetFcn     % function to invoke on the access to set a value
        PreSetFcn  % function to apply prior to calling SetFcn
    end

    methods ( Access = private )
        function obj = MJSMethods( needsUUID, getF, postGetF, setF, preSetF )
        % Use assertions here as MJSMethods are only ever build during static
        % initialization.

            narginchk( 3, 5 );
            %{
            validateattributes( needsUUID, {'logical'}, {'scalar'} );
            validateattributes( getF, {'function_handle'}, {'scalar'} );
            validateattributes( postGetF, {'function_handle'}, {'scalar'} );
            %}

            obj.NeedsUUID    = needsUUID;
            obj.GetFcn       = getF;
            obj.PostGetFcn   = postGetF;
            if nargin < 5
                setF = []; preSetF = [];
            else
                narginchk( 5, 5 );
                %{
                validateattributes( setF, {'function_handle'}, {'scalar'} );
                validateattributes( preSetF, {'function_handle'}, {'scalar'} );
                %}
            end
            obj.SetFcn     = setF;
            obj.PreSetFcn  = preSetF;
        end

        function applySet( obj, access, uuid, val )
            if isempty( obj.SetFcn )
                error(message('parallel:cluster:MJSUnableToSetProperty'));
            end
            assert( isempty( uuid ) == ~obj.NeedsUUID );

            if obj.NeedsUUID
                obj.SetFcn( access, uuid, obj.PreSetFcn( val ) );
            else
                obj.SetFcn( access, obj.PreSetFcn( val ) );
            end
        end

        function v = applyGet( obj, access, uuid )
        % Get a value, throws errors if things cannot be 'got' - with the
        % exception of StateEnum which knows how to handle certain error cases.
            if isempty( obj.GetFcn )
                error(message('parallel:cluster:MJSUnableToGetProperty'));
            else
                assert( isempty( uuid ) == ~obj.NeedsUUID );
                if obj.NeedsUUID
                    v = obj.PostGetFcn( obj.GetFcn( access, uuid ) );
                else
                    v = obj.PostGetFcn( obj.GetFcn( access ) );
                end
            end
        end
    end

    methods ( Static, Access = ?parallel.internal.cluster.MJSSupport )
        function vcell = performGets( access, uuid, variant, propNameCell )
        % Perform multiple 'gets', efficiently using the info struct if
        % possible.

        %{
            validateattributes( variant, ...
                                {'parallel.internal.types.Variant'}, ...
                                {'scalar'} );
            validateattributes( propNameCell, {'cell'}, {} );
        %}

            % Preallocate return
            vcell     = cell( 1, length( propNameCell ) );
            % Check how many properties could be extracted from Info
            if numel( propNameCell ) > 1
                infoProps = iGetInfoProps( variant );
                useInfo   = ismember( propNameCell, infoProps );
            else
                % Short-cut for the common case of single property access
                useInfo   = false;
            end

            if sum( useInfo ) > 1
                % Get the 'Info', and extract stuff from there
                meth = iGetMethod( variant, 'Info' );
                info = meth.applyGet( access, uuid );
                info = info(1);
            else
                useInfo = false( 1, length( propNameCell ) );
                info    = [];
            end

            for ii = 1:length( propNameCell )
                % This relies on the fact that Info method names match the same
                % methods on the access.
                meth = iGetMethod( variant, propNameCell{ii} );
                if useInfo( ii )
                    % NB: can't use applyGet here because the Info does not
                    % accept the UUID, but the method may believe one is
                    % required.
                    vcell{ii} = meth.PostGetFcn( meth.GetFcn( info ) );
                else
                    vcell{ii} = meth.applyGet( access, uuid );
                end
            end
        end

        function performSets( access, uuid, variant, propNameCell, valsCell )
        % Perform multiple 'sets', not yet possible to make this efficient
        % wrt. using info struct.
        %{
            validateattributes( variant, ...
                                {'parallel.internal.types.Variant'}, ...
                                {'scalar'} );
            validateattributes( propNameCell, {'cell'}, {} );
            validateattributes( valsCell, {'cell'}, {'numel', numel(propNameCell)} );
        %}

            for ii = 1:length( propNameCell )
                meth = iGetMethod( variant, propNameCell{ii} );
                meth.applySet( access, uuid, valsCell{ii} );
            end
        end
    end
    properties ( Constant, GetAccess = private )
        % Constant maps of property name to MJSMethods instance.
        TaskMap    = iTaskMap();
        JobMap     = iJobMap();
        ClusterMap = iClusterMap();
        WorkerMap  = iWorkerMap();
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Return those properties which can be accessed from the "Info" property of an
% object.
function props = iGetInfoProps( variant )
    import parallel.internal.types.Variant;

    jobtaskprops = { 'ID', 'Name', 'StateEnum', 'CreateTime', 'StartTime', 'FinishTime' };

    switch variant
      case Variant.Task
        props = [ jobtaskprops, ...
                  { 'NumOutputArguments', 'ErrorMessage', ... % 'Error' should be here, but isn't because we must synthesize it.
                    'ErrorIdentifier', 'MaximumRetries' } ];
      case { Variant.IndependentJob, ...
             Variant.CommunicatingSPMDJob, ...
             Variant.CommunicatingPoolJob }
        props = [ jobtaskprops, ...
                  { 'Tag', 'MinNumWorkers', 'MaxNumWorkers', ...
                    'AttachedFiles', 'AdditionalPaths' 'SubmitTime', ...
                    'Username', 'ExecutionMode' } ];
      case Variant.Cluster
        props = { 'AllHostAddresses', 'BusyWorkers', 'Host', ...
                  'IdleWorkers', 'Name', 'NumWorkers', 'NumBusyWorkers', ...
                  'NumIdleWorkers', 'StateEnum', 'ClusterLogLevel', 'OperatingSystem' };
      case Variant.Worker
        props = { 'AllHostAddresses', 'ComputerMLType', 'CurrentJobAndTask', ...
                  'Host', 'LastJobAndTask', 'Name', 'StateEnum' };
      otherwise
        error(message('parallel:cluster:MJSUnhandledVariant'));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Cache maps per class from propName to MJSMethods object
function method = iGetMethod( variant, propName )
    import parallel.internal.cluster.MJSMethods
    import parallel.internal.types.Variant

    % Seems silly, but this piece of code gets hit hard.
    persistent V_TASK V_JOB V_CLUSTER V_WORKER
    persistent M_TASK M_JOB M_CLUSTER M_WORKER
    if isempty( V_TASK )
        V_TASK    = Variant.Task;
        V_JOB     = { Variant.IndependentJob, ...
                      Variant.CommunicatingSPMDJob, ...
                      Variant.CommunicatingPoolJob };
        V_CLUSTER = Variant.Cluster;
        V_WORKER  = Variant.Worker;
        M_TASK    = MJSMethods.TaskMap;
        M_JOB     = MJSMethods.JobMap;
        M_CLUSTER = MJSMethods.ClusterMap;
        M_WORKER  = MJSMethods.WorkerMap;
    end

    switch variant
      case V_TASK
        map = M_TASK;
      case V_JOB
        map = M_JOB;
      case V_CLUSTER
        map = M_CLUSTER;
      case V_WORKER
        map = M_WORKER;
      otherwise
        error(message('parallel:cluster:MJSUnhandledVariant'));
    end

    % NB: No defense here against bad key.
    method = map(propName);
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrappers around the MJSMethods constructors which inject the first argument
function m = iBuildNeedsUUID( varargin )
    m = parallel.internal.cluster.MJSMethods( true, varargin{:} );
end
function m = iBuildNoUUID( varargin )
    m = parallel.internal.cluster.MJSMethods( false, varargin{:} );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% A collection of tiny functions to avoid creating anonymous functions with
% unfortunate workspaces.
function x = iNoOp( x )
end
function x = iDeserDefaultCell( x )
    x = iDeserDataItem( x, cell(1,0) );
end
function x = iDeserDefaultDouble( x )
    x = iDeserDataItem( x, [] );
end
function x = iToJavaString( x )
    x = dctJavaArray( java.lang.String( x ) );
end
function x = iReturnEmpty( varargin )
    x = [];
end
function x = iSelectFirst( x )
    x = x(1);
end
function x = iTypecastToUint8( x )
    x = typecast( x, 'uint8' );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the map for Tasks
function tm = iTaskMap()
    tm                         = containers.Map;
    ctor                       = @iBuildNeedsUUID;
    noop                       = @iNoOp;
    tm( 'NumFailures' )        = ctor( @getAttemptedNumberOfRetries   , @double );
    tm( 'CaptureDiary' )       = ctor( @getCaptureCommandWindowOutput , noop,  ...
                                       @setCaptureCommandWindowOutput , noop );
    tm( 'CreateTime' )         = ctor( @getCreateTime                 , @iChar );
    tm( 'DependentFiles' )     = ctor( @getAutoAttachedFileList       , @iCellColumn, ...
                                       @iSetDependentFiles            , noop );
    tm( 'Diary' )              = ctor( @iGetDiary                     , noop );
    % Note that because cancellation does not correctly set the Error field, we
    % must use a specific method to retrieve that.
    tm( 'Error' )              = ctor( @iGetError                     , noop );
    tm( 'ErrorIdentifier' )    = ctor( @getErrorIdentifier            , @iChar );
    tm( 'ErrorMessage' )       = ctor( @getErrorMessage               , @iChar );
    tm( 'FailureInfo' )        = ...
                                 ctor( @getFailedAttemptInformation   , ...
                                       @iWrapFailedAttemptInfo );
    tm( 'FinishTime' )         = ctor( @getFinishTime                 , @iChar );
    tm( 'Function' )           = ctor( @getMLFunction            , ...
                                       @iDeserDefaultDouble      , ...
                                       @iSetFunctionAndFuncStr   , noop );
    tm( 'FunctionName' )       = ctor( @getMLFunctionName             , @iChar );
    tm( 'ID' )                 = ctor( @getNum                        , @double );
    tm( 'Info' )               = ctor( @getWorkUnitInfoSmallItems     , noop );
    tm( 'InputArguments' )     = ctor( @getInputData             , ...
                                       @iDeserDefaultCell        , ...
                                       @iSetInputArguments       , noop );
    tm( 'MaximumRetries' )     = ctor( @getMaximumNumberOfRetries     , @double,  ...
                                       @setMaximumNumberOfRetries     , noop );
    tm( 'Name' )               = ctor( @getName                       , @iChar, ...
                                       @setName                       , @iToJavaString);
    tm( 'NumOutputArguments' ) = ctor( @getNumOutArgs                 , @double, ...
                                       @setNumOutArgs, noop );
    tm( 'OutputArguments' )    = ctor( @getOutputData            , ...
                                       @iDeserDefaultCell );
    tm( 'StartTime' )          = ctor( @getStartTime                  , @iChar );
    tm( 'StateEnum' )          = ctor( @iGetWorkUnitState             , noop );
    tm( 'Timeout' )            = ctor( @getTimeout                    , @iUnpackTimeout, ...
                                       @setTimeout                    , @iPackTimeout );
    tm( 'WorkerProxies' )      = ctor( @getWorker                     , noop );
    tm( 'WorkerProperties' )   = ctor( @getWorkerProperties           , noop );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the map for Job information
function jm = iJobMap()
    jm                      = containers.Map();
    ctor                    = @iBuildNeedsUUID;
    noop                    = @iNoOp;

    jm( 'AdditionalPaths' )  = ctor( @getPathList               , @iCellColumn, ...
                                     @setPathList               , @iSetupAddPaths );
    jm( 'ApiTag' )           = ctor( @getApiTag                 , @iChar, ...
                                     @setApiTag                 , @iToJavaString );
    jm( 'AutoAttachFiles' )  = ctor( @getAutoAttachFiles        , noop, ...
                                     @setAutoAttachFiles        , noop );
    jm( 'AttachedFileData' ) = ctor( @iGetFileDepData           , noop, ...
                                     @iSetAttachedFileData      , noop );
    jm( 'AttachedFilePaths' )= ctor( @getAttachedFilePaths      , @iDeserDefaultDouble, ...
                                     @iSetAttachedFilePaths     , noop );
    jm( 'AttachedFiles' )    = ctor( @getFileDepPathList        , @iCellColumn, ...
                                     @iSetAttachedFileList      , noop );
    jm( 'AuthorizedUsers' )  = ctor( @getAuthorisedUsers        , @iCell, ...
                                     @setAuthorisedUsers        , noop );
    jm( 'CreateTime' )       = ctor( @getCreateTime             , @iChar );
    jm( 'DependentFiles' )   = ctor( @getAutoAttachedFileList   , @iCellColumn, ...
                                     @iSetDependentFiles        , noop );
    jm( 'ExecutionMode' )    = ctor( @getMATLABExecutionMode    , @int32, ...
                                     @setMATLABExecutionMode    , noop );
    jm( 'FinishTime' )       = ctor( @getFinishTime             , @iChar );
    jm( 'ID' )               = ctor( @getNum                    , @double );
    jm( 'Info' )             = ctor( @getWorkUnitInfoSmallItems , noop );
    jm( 'JobData' )          = ctor( @getJobScopeData           , ...
                                     @iDeserDefaultDouble       , ...
                                     @iSetJobData               , ...
                                     noop );
    jm( 'MaxNumWorkers' )    = ctor( @getMaxWorkers             , ...
                                     @iMapWorkersToMATLAB, ...
                                     @setMaxWorkers             , ...
                                     @iMapWorkersFromMATLAB );
    jm( 'MinNumWorkers' )    = ctor( @getMinWorkers             , ...
                                     @iMapWorkersToMATLAB, ...
                                     @setMinWorkers             , ...
                                     @iMapWorkersFromMATLAB );
    jm( 'Name' )             = ctor( @getName                   , @iChar, ...
                                     @setName                   , @iToJavaString );
    jm( 'ParallelTag' )      = ctor( @iReturnEmpty              , noop, ... % No 'get'
                                     @setParallelTag            , @iChar );
    jm( 'ParallelInfo' )     = ctor( @getParallelJobSetupInfo   , @iSelectFirst ); % Select the first element
    jm( 'ProductKeys' )      = ctor( @getProductList  ,         ...
                                     @iTypecastToUint8,         ...
                                     @setProductList  ,         ...
                                     noop );
    jm( 'RestartWorker' )    = ctor( @isRestartWorker           , noop, ...
                                     @setRestartWorker          , noop );
    jm( 'StartTime' )        = ctor( @getStartTime              , @iChar );
    jm( 'StateEnum' )        = ctor( @iGetWorkUnitState         , noop );
    jm( 'SubmitTime' )       = ctor( @getSubmitTime             , @iChar );
    jm( 'Tag' )              = ctor( @getTag                    , @iChar, ...
                                     @setTag                    , @iToJavaString );
    jm( 'Timeout' )          = ctor( @getTimeout                , @iUnpackTimeout, ...
                                     @setTimeout                , @iPackTimeout );
    jm( 'Username' )         = ctor( @getUserName               , @iChar, ...
                                     @setUserName               , @iToJavaString );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the map for Cluster (MJS) information
function cm = iClusterMap()
    cm                       = containers.Map();
    ctor                     = @iBuildNoUUID;
    noop                     = @iNoOp;
    cm( 'AllHostAddresses' ) = ctor( @getAllHostAddresses    , @iHostAddresses );
    cm( 'BusyWorkers' )      = ctor( @getBusyWorkerProperties, noop );
    cm( 'ClusterMatlabRoot' )= ctor( @getClusterMatlabRoot   , @iChar);
    cm( 'ClusterLogLevel' )  = ctor( @getClusterLogLevel  , @double, ...
                                     @setClusterLogLevel  , noop);
    cm( 'Host' )             = ctor( @getHostName            , @iChar );
    cm( 'IdleWorkers' )      = ctor( @getIdleWorkerProperties, noop );
    cm( 'Info' )             = ctor( @getServiceInfo         , noop );
    cm( 'HasSecureCommunication' ) ...
                             = ctor( @isUsingSecureCommunication, noop );
    cm( 'LookupURL' )        = ctor( @getLookupURL           , noop );
    cm( 'Name' )             = ctor( @getName                , @iChar );
    cm( 'NumBusyWorkers' )   = ctor( @getNumBusyWorkers      , @double );
    cm( 'NumIdleWorkers' )   = ctor( @getNumIdleWorkers      , @double );
    cm( 'NumWorkers' )       = ctor( @iGetNumWorkers         , noop );
    cm( 'PromptForPassword' )= ctor( @iGetPromptForPassword  , noop, ...
                                     @iSetPromptForPassword  , noop );
    cm( 'RequiresMathWorksHostedLicensing' ) ...
                             = ctor( @requireWebLicensing    , noop );
    cm( 'SecurityLevel' )    = ctor( @getSecurityLevel       , @double );
    cm( 'StateEnum' )        = ctor( @iGetProcessState       , @iMapClusterState );
    cm( 'Username' )         = ctor( @getCurrentUser         , @iChar, ...
                                     @setCurrentUser         , @iUserIdentity );
    cm( 'VersionNumber' )    = ctor( @getVersionNumber       , noop );
    cm( 'VersionString' )    = ctor( @getVersionString       , @iChar );
    cm( 'OperatingSystem' )  = ctor( @getComputerMLType      , @iChar );
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the map for Worker information
function wm = iWorkerMap()
    wm                        = containers.Map();
    ctor                      = @iBuildNoUUID;
    noop                      = @(x) x;
    wm( 'AllHostAddresses' )  = ctor( @getAllHostAddresses  , @iHostAddresses );
    wm( 'ComputerType' )      = ctor( @getComputerMLType    , @iChar );
    wm( 'CurrentJobAndTask' ) = ctor( @getCurrentJobAndTask , noop );
    wm( 'FileDependencyDir' ) = ctor( @getFileDependencyDir , noop );
    wm( 'Host' )              = ctor( @getHostName          , @iChar );
    wm( 'Info' )              = ctor( @getServiceInfo       , noop );
    wm( 'LastJobAndTask' )    = ctor( @getLastJobAndTask    , noop );
    wm( 'LogLevel' )          = ctor( @getLogLevel          , noop );
    wm( 'Name' )              = ctor( @getName              , @iChar );
    wm( 'NodeInfo' )          = ctor( @getNodeInfo          , noop );
    wm( 'ParallelPortInfo' )  = ctor( @getParallelPortInfo  , noop );
    wm( 'SecurityLevel' )     = ctor( @getSecurityLevel     , noop );
    wm( 'ServiceInfo' )       = ctor( @getServiceInfo       , noop );
    wm( 'StateEnum' )         = ctor( @iGetProcessState     , @iMapWorkerState );
    wm( 'VersionNumber' )     = ctor( @getVersionNumber     , noop );
    wm( 'VersionString' )     = ctor( @getVersionString     , noop );
    wm( 'WorkerDir' )         = ctor( @getWorkerDir         , noop );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SET/GET WRAPPERS
% A few values need special handling when setting stuff rather than simply
% setting or getting things from the access
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iGetError - return the Error as an MException. Need to check ErrorMessage and
% ErrorIdentifier too as per @distcomp/@task/pGetError
function val = iGetError( access, uuid )
    info       = access.getWorkUnitInfoSmallItems( uuid );
    info       = info(1);
    errorBytes = info.getErrorStruct();
    errID      = char(info.getErrorIdentifier());
    errMessage = char(info.getErrorMessage());

    if ~isempty(errorBytes)
        val = distcompdeserialize(errorBytes);
    else
        val = MException.empty();
    end
    % If the error itself is empty, but either of the ErrorMessage
    % or ErrorIdentifier are non-empty, then build the appropriate
    % MException from these
    if isempty(val) && ~(isempty(errID) && isempty(errMessage))
        val = MException(errID, '%s', errMessage);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set wrapper for task Function and FunctionName.
function iSetFunctionAndFuncStr( access, uuid, rawValue )
    % need to hang on to "s" as it's backing the data item, hence NASGU:
    [itemArray, s] = iWrapInByteBufferItem( rawValue ); %#ok<NASGU>
    fcnStr = parallel.internal.cluster.generateTaskFunctionString( rawValue );
    access.setMLFunction( uuid, itemArray );
    access.setMLFunctionName( uuid, fcnStr );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set wrapper for JobData - need great care handling the lifetime of data
% backing byte buffers.
function iSetJobData( access, uuid, rawValue )
    % need to hang on to "s" as it's backing the data item, hence NASGU:
    [v, s] = iWrapInByteBufferItem( rawValue ); %#ok<NASGU>
    setJobScopeData( access, uuid, v );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set wrapper for Task InputArguments
function iSetInputArguments( access, uuid, rawValue )
    % need to hang on to "s" as it's backing the data item, hence NASGU:
    [v, s] = iWrapInByteBufferItem( rawValue ); %#ok<NASGU>
    setInputData( access, uuid, v );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set wrapper for AttachedFilePaths
function iSetAttachedFilePaths( access, uuid, rawValue )
    % need to hang on to "s" as it's backing the data item, hence NASGU:
    [v, s] = iWrapInByteBufferItem( rawValue ); %#ok<NASGU>
    setAttachedFilePaths( access, uuid, v );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Set attached files data and list of files.
function iSetAttachedFileData( access, uuid, filedata )
    if isempty( filedata )
        filedata = int8([]);
    end
    bb = iWrapInByteBufferItemNoSerialization( filedata );
    access.setFileDepData( uuid, bb );
end

function iSetAttachedFileList( access, uuid, v )
    % Resolve the file paths and then check that they all exist.
    resolved = cellfun( ...
        @parallel.internal.apishared.AttachedFiles.resolvePath, v, ...
            'UniformOutput', false );
    val = iConvertCellArrayOfStringsForJava( resolved );
    access.setFileDepPathList( uuid, val );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Store the list of dependent files for the task
function iSetDependentFiles( access, uuid, rawValue )
    v = rawValue( ~cellfun( @isempty, rawValue ) );
    val = iConvertCellArrayOfStringsForJava( v );
    access.setAutoAttachedFileList( uuid, val );
end

function val = iConvertCellArrayOfStringsForJava( val )
    if isempty( val )
        % Need to make a 1 x 0 array of java.lang.String[][]
        val = javaArray('java.lang.String', 1, 1);
        val(1) = '';
    else
        % Need to ensure the cell array of strings is 1 x nStrings
        % otherwise the java layer gets upset
        val = reshape(val, 1, numel(val));
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iGetFileDepData - return the filedependencies data
function data = iGetFileDepData( access, uuid )
    itemList = access.getFileDepData( uuid );
    data = itemList(1).getData;
    if ~isempty(data) && data.limit > 0
        % Define the type of the output as int8 to conform with the
        % input type required by zip.
        data = distcompByteBuffer2MxArray(data, int8([]));
    else
        data = int8([]);
    end
    itemList(1).delete();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iGetState - return StateEnum for a Job or Task
% c.f: @distcomp/@(job|task)/pGetState.m
function state = iGetWorkUnitState( access, uuid )
    import parallel.internal.types.States
    jobOrTaskDestroyedID = 'com.mathworks.toolbox.distcomp.storage.WorkUnitNotFoundException';

    try
        stateNum = access.getState( uuid );
        state    = iMapWorkUnitState( stateNum );
    catch E
        state = States.Unavailable; % default, might be overwritten

        [isJavaError, exceptionType, causes] = isJavaException( E );
        if isJavaError
            % Find the original cause of the exception
            if isempty(causes)
                cause = exceptionType;
            else
                cause = causes{end};
            end
            if isequal( cause, jobOrTaskDestroyedID )
                state = States.Destroyed;
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iGetProcessState - state of jobmanager or worker
function stateNum = iGetProcessState( proxy )
    try
        stateNum = proxy.getState();
    catch E %#ok<NASGU>
        % Anything other than 0 (Running) and 1 (Paused) is mapped to
        % Unavailable - doesn't really matter why we can't get the state
        stateNum = NaN;
    end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get NumWorkers for a Cluster. NB "access" could be an access or an info.
function v = iGetNumWorkers( accessOrInfo )
% This is slightly ugly - ensure we always go via the ServiceInfo to guarantee
% consistent information.
    if isa( accessOrInfo, ...
            'com.mathworks.toolbox.distcomp.jobmanager.JobManagerServiceInfo' )
        info = accessOrInfo;
    else
        info = accessOrInfo.getServiceInfo();
    end
    v = double( getNumBusyWorkers( info ) ) + ...
        double( getNumIdleWorkers( info ) );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Get PromptForPassword for a cluster
function v = iGetPromptForPassword( access )
    v = access.getCredentialConsumerFactory().isInteractive();
end

function iSetPromptForPassword( access, promptForPassword )
    access.getCredentialConsumerFactory().setInteractive( promptForPassword );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% VALUE TRANSFORMATIONS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% unpack a timeout value from the database value
function matlabValue = iUnpackTimeout( javaValue )
    if isequal( javaValue, intmax('int64') )
        matlabValue = Inf;
    else
        matlabValue = double( javaValue ) / 1000; % convert to seconds
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% pack a MATLAB timeout value for the database
function javaValue = iPackTimeout( matlabValue )
    val = matlabValue * 1000; % convert to milliseconds

    % Get INTMAX for int64 to check for Inf
    INTMAX_I64 = intmax('int64');

    if ~isfinite(val) || val > INTMAX_I64
        javaValue = INTMAX_I64;
    else
        javaValue = int64(val);
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ensure that empty strings turn into the canonical empty string.
function s = iChar( s )
    s = char( s );
    if isempty( s )
        s = '';
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ensure the return is a cell array
function c = iCell( arrayOrEmpty )
    if isempty( arrayOrEmpty )
        c = {};
    else
        c = cell( arrayOrEmpty );
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ensure the return is a cell column
function c = iCellColumn( arrayOrEmpty )
    if isempty( arrayOrEmpty )
        c = {};
    else
        c = cell( arrayOrEmpty );
        c = reshape( c, numel( c ), 1 );
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Map number of workers to and from MATLAB and Java
function val = iMapWorkersToMATLAB( iVal )
    if isequal( iVal, intmax( 'int32' ) )
        val = Inf;
    else
        val = double( iVal );
    end
end

function val = iMapWorkersFromMATLAB( iVal )
% NB int32 cast correctly handles Inf, so all we need is simply:
    val = int32( iVal );
end

function userIdentity = iUserIdentity( userNameOrIdentity )
    import com.mathworks.toolbox.distcomp.auth.credentials.UserIdentity
    if isa(userNameOrIdentity, 'com.mathworks.toolbox.distcomp.auth.credentials.UserIdentity')
        userIdentity = userNameOrIdentity;
    else
        userIdentity = UserIdentity(userNameOrIdentity);
    end    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Deserialize from within a data item
function val = iDeserDataItem( dataItem, default )
    data = dataItem(1).getData;
    if ~isempty(data) && data.limit > 0
        val = distcompdeserialize(data);
    else
        val = default;
    end
    dataItem(1).delete();
end

function val = iGetDiary( access, uuid )
    dataItem = access.getCommandWindowOutput( uuid );
    val = iGetDiaryFromBuffer( dataItem );
end

function val = iGetDiaryFromBuffer( dataItem )
    default = '';
    data = dataItem(1).getData;
    if ~isempty(data) && data.limit > 0
        dataItem(1).rewind;
        data = dataItem(1).getData(dataItem(1).getNumBytes)';
        % The diary output has been streamed to the job manager in UTF-16.
        % Java's bytes are signed and native2unicode expects uint8.
        val = native2unicode(typecast(data, 'uint8'), 'UTF-16LE');
    else
        val = default;
    end
    dataItem(1).delete();
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% State transformations - return string and enumeration variants
function state = iMapWorkUnitState( val )
    import com.mathworks.toolbox.distcomp.workunit.WorkUnit;
    import parallel.internal.types.States;
    switch double(val)
      case WorkUnit.PENDING_STATE
        state = States.Pending;
      case WorkUnit.QUEUED_STATE
        state = States.Queued;
      case WorkUnit.RUNNING_STATE
        state = States.Running;
      case WorkUnit.FINISHED_STATE
        state = States.Finished;
      otherwise
        assert( false );
    end
end
function state = iMapClusterState( val )
    import com.mathworks.toolbox.distcomp.jobmanager.JobManager;
    import parallel.internal.types.States;
    switch double(val)
      case JobManager.RUNNING_STATE
        state = States.Running;
      case JobManager.PAUSED_STATE
        state = States.Paused;
      case JobManager.UNAVAILABLE_STATE
        state = States.Unavailable;
      otherwise
        state = States.Unavailable;
    end
end
function state = iMapWorkerState( val )
    import com.mathworks.toolbox.distcomp.worker.Worker;
    import parallel.internal.types.States;
    switch double(val)
      case Worker.RUNNING_STATE
        state = States.Running;
      case Worker.PAUSED_STATE
        state = States.Paused;
      otherwise
        state = States.Unavailable;
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build one or more failed attempt information wrappers
function val = iWrapFailedAttemptInfo( array )
    import parallel.task.MJSFailureInfo

    val = [];
    if numel(array) > 0
        taskInfos = array(1);
        val = cell( 1, numel( taskInfos ) );
        for ii = 1:numel(taskInfos)
            infoObj = taskInfos(ii);
            diary = iGetDiaryFromBuffer( infoObj.getCommandWindowOutput() );
            workerData = infoObj.getWorker();
            if ~isempty( workerData )
                % TODO:later - should this use a simple worker object thing?
                worker = char( workerData.getName() );
                workerHost = char( workerData.getHostName() );
            else
                worker = '';
                workerHost = '';
            end
            infoStruct = struct( 'StartTime', char( infoObj.getStartTime() ), ...
                                 'ErrorIdentifier', char( infoObj.getErrorIdentifier() ), ...
                                 'ErrorMessage', char( infoObj.getErrorMessage() ), ...
                                 'Diary', diary, ...
                                 'Worker', worker, ...
                                 'WorkerHost', workerHost );

            val{ii} = MJSFailureInfo.hBuild( infoStruct );
        end
        val = [val{:}];
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Map the java array of Strings into a cell array, stripping loopback addresses.
function c = iHostAddresses( addresses )
% Input is a java.lang.String[]
    import java.net.InetAddress

    validAddresses = cell(1, numel(addresses));
    for ii = 1:numel(addresses)
        jAddress = InetAddress.getByName(addresses(ii));
        if ~jAddress.isLoopbackAddress
            validAddresses{ii} = char(jAddress.getHostAddress);
        end
    end
    validAddresses(cellfun(@isempty, validAddresses)) = [];
    c = validAddresses;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrapper around setting additional paths
function v = iSetupAddPaths( incell )
    if ispc
        v = cellfun( @dctReplaceDriveWithUNCPath, incell, ...
                     'UniformOutput', false );
    else
        v = incell;
    end
    if isempty( v )
        v = javaArray('java.lang.String', 1, 1);
        v(1) = '';
    else
        v = reshape( v, 1, numel( v ) );
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% MISC UTILITIES
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrap a piece of data in a ByteBufferItem - also returns "s" which the caller
% must hang onto until they've finished using the ByteBuffer "v".
function [v, s] = iWrapInByteBufferItem( val )
    s = distcompserialize( val );
    v = iWrapInByteBufferItemNoSerialization( s );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Wrap a piece of data in a ByteBufferItem with no serialization
function v = iWrapInByteBufferItemNoSerialization( rawValue )
    import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem

    item = ByteBufferItem( distcompMxArray2ByteBuffer( rawValue ) );
    v = dctJavaArray( item,...
                      'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData');
end
