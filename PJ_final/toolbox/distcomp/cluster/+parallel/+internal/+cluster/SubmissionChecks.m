% SubmissionChecks - a series of pre-submission checks

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) SubmissionChecks
    methods ( Static )

        function warnIfPoolOpenOnThisCluster( cluster, minWorkersNeeded )
        % warnIfPoolOpenOnThisCluster - issue a warning if submitting a job and
        % MATLABPOOL is open and consuming all resources. This check is
        % specifically intended to protect users from the problem of opening a
        % matlabpool and then submitting batch jobs.

            import parallel.internal.cluster.MatlabpoolHelper

            [isOnCluster, poolSize] = MatlabpoolHelper.isPoolRunningOnEquivalentCluster( cluster );
            if isOnCluster
                % We're trying to submit to the same place
                freeWorkers = cluster.NumWorkers - poolSize;
                if minWorkersNeeded > freeWorkers
                    if ~isempty( cluster.Profile )
                        warning( message( 'parallel:job:ClusterProfileAlreadyRunningMatlabpool', ...
                                          cluster.Profile ) );
                    else
                        warning( message( 'parallel:job:ClusterAlreadyRunningMatlabpool' ) );
                    end
                end
            end
        end

        function [tasks, numTasks] = checkIndependentTasksBeforeSubmission( job )
        % checkIndependentTasksBeforeSubmission - check there's at least one task
            validateattributes( job, {'parallel.IndependentJob'}, {'scalar'} );

            tasks    = job.Tasks;
            numTasks = numel( tasks );
            if numTasks < 1
                error(message('parallel:job:JobSubmissionWithNoTasks'));
            end
        end

        function [tasks, numTasks] = checkCommunicatingTasksBeforeSubmission( cjob )
        % checkCommunicatingTasksBeforeSubmission - must be exactly 1 task
            validateattributes( cjob, {'parallel.CommunicatingJob'}, {'scalar'} );
            tasks    = cjob.Tasks;
            numTasks = numel( tasks );
            if numTasks ~= 1
                error(message('parallel:job:CommunicatingJobNeedsOneTask'));
            end
        end

        function maxW = checkWorkerLimsForCommunicatingJob( cluster, cjob )
        % checkMinMaxWorkersForCommunicatingJob - checks that NumWorkersRange
        % are ok for this cluster.

            validateattributes( cluster, {'parallel.Cluster'}, {'scalar'} );
            validateattributes( cjob, {'parallel.CommunicatingJob'}, {'scalar'} );

            workerLims = cjob.NumWorkersRange;
            minW = workerLims(1);
            maxW = workerLims(2);

            % The following cases should never be hit due to the constraints on
            % NumWorkersRange:
            if maxW < 1
                error(message('parallel:job:UnexpectedBadNumWorkersRange'));
            end

            % Return the number of workers that we think should be requested
            maxW = min( maxW, cluster.NumWorkers );

            if minW > cluster.NumWorkers
                if isempty( cluster.Profile )
                    % TODO:doc review
                    error(message('parallel:job:TooManyWorkersRequested', minW, cluster.NumWorkers));
                else
                    error(message('parallel:job:TooManyWorkersRequestedForProfile', minW, cluster.Profile, cluster.NumWorkers));
                end
            end
        end
    end
end
