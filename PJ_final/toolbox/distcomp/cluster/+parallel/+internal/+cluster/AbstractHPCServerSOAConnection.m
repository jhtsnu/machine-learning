classdef AbstractHPCServerSOAConnection < handle
    %AbstractHPCServerSOAConnection Abstract base class for connections to HPC Server 2008 SOA APIs
    %   Abstract base class for SOA connections to Microsoft scheduler using the Microsoft APIs.

    %  Copyright 2010-2012 The MathWorks, Inc.

    properties (Constant, GetAccess = protected)
        IsClientCorrectPlatform = ispc();
        ServiceType = 'Mathworks.MdcsServiceProxy.IMdcsService';
        ServiceName = parallel.internal.cluster.AbstractHPCServerSOAConnection.getServiceName;
        MicrosoftSessionAssembly = 'Microsoft.Hpc.Scheduler.Session';
        FoundMicrosoftHpcSchedulerSessionLibrary = parallel.internal.cluster.AbstractHPCServerSOAConnection.addMicrosoftHpcSchedulerSessionAssembly;
        % Used to inform HPC Server that the SOA client (i.e. MATLAB) is not a console application.
        IsConsoleApplication = false;
        % Used to tell HPC Server which handle to use as the parent window
        % for the credentials dialog. Setting to 0 means HWND_DESKTOP is
        % used as the parent.
        ParentWindow = 0;
    end
    
    properties (Access = protected)
        % The Microsoft.Hpc.Scheduler.Scheduler connection that will be used to
        % talk to the HPC Server scheduler
        SchedulerConnection;
        % The hostname of the scheduler to which we are connected
        SchedulerHostname;
    end
    
    methods (Static, Access = private) % Methods to support Constant properties.
        % Methods in this section should NEVER be called directly - use the constant properties instead.
        
        %---------------------------------------------------------------
        % getServiceName
        %---------------------------------------------------------------
        % Get the name of the service that will actually run.  This corresponds to
        % the name of the file that is found in %CCP_HOME%\ServiceRegistration.
        function serviceName = getServiceName
            % The service name is based on the pct version number.
            pctVersion = char(com.mathworks.toolbox.distcomp.util.Version.VERSION_STRING);
            serviceName = sprintf('MdcsServiceForPCT%s', pctVersion);
        end
        %---------------------------------------------------------------
        % addMicrosoftHpcSchedulerSessionAssembly
        %---------------------------------------------------------------
        % NB This will only get called once by the constant 
        % FoundMicrosoftHpcSchedulerSessionLibrary property.  
        % FoundMicrosoftHpcSchedulerSessionLibrary will "persist" until
        % "clear classes" is called.
        function assemblyIsAdded = addMicrosoftHpcSchedulerSessionAssembly
            import parallel.internal.cluster.AbstractHPCServerSOAConnection
            if ~AbstractHPCServerSOAConnection.IsClientCorrectPlatform
                assemblyIsAdded = false;
                return;
            end
            assemblyName = AbstractHPCServerSOAConnection.MicrosoftSessionAssembly;
            try
                NET.addAssembly(assemblyName);
                assemblyIsAdded = true;
            catch err 
                assemblyIsAdded = false;
                dctSchedulerMessage(5, 'Failed to load %s.\nReason:%s\n%s', assemblyName, err.getReport(), ...
                    AbstractHPCServerSOAConnection.getNetworkInstallWarningMessage());
            end
        end
    end
    
    methods (Static, Access = protected)
        %---------------------------------------------------------------
        % foundRequiredLibraries
        %---------------------------------------------------------------
        function allFound = foundRequiredLibraries
            import parallel.internal.cluster.AbstractHPCServerSOAConnection
            % Must be running on windows in order to find the required libraries.
            allFound = AbstractHPCServerSOAConnection.IsClientCorrectPlatform && ...
                AbstractHPCServerSOAConnection.FoundMicrosoftHpcSchedulerSessionLibrary;
        end
        
        %---------------------------------------------------------------
        % getNetworkInstallWarningMessage
        %---------------------------------------------------------------
        % returns the message about network security for .NET assemblies if we
        % detect that we are running from a network install.
        function msg = getNetworkInstallWarningMessage
            % If we are running from a network share then it is possible the security settings
            % are not set up correctly.
            % TODO - If running on a Network Share then try running the caspol code (addAssembliesToCaspol.bat)?
            if iIsNetworkInstall
                msg = getString(message('parallel:cluster:FullTrustDotNET'));
            else
                msg = '';
            end
        end
        
        %---------------------------------------------------------------
        % isServiceConfigInDirectory
        %---------------------------------------------------------------
        function serviceFound = isServiceConfigInDirectory(directory)
            % The presence of <serviceName>.config in the directory
            % indicates that the service exists.
            import parallel.internal.cluster.*
            listing = dir(directory);
            allServices = {listing(:).name};
            serviceFound = any(strncmp(allServices, ...
                AbstractHPCServerSOAConnection.ServiceName, ...
                length(AbstractHPCServerSOAConnection.ServiceName)));
        end
    end
    
    methods (Abstract, Static) % Abstract, static methods
        isCompatible = isClientCompatible(schedulerVersion);
    end
    
    methods (Abstract, Static, Access = protected) %Abstract, static, protected methods
        % The version of the Microsoft SOA API that is required
        version = getRequiredMicrosoftAPIVersion;
        numJobs = getExpectedNumberOfSOAJobs;
    end
    
    methods
        %---------------------------------------------------------------
        % constructor
        %---------------------------------------------------------------
        function obj = AbstractHPCServerSOAConnection(schedulerConnection, schedulerHostname)
            import parallel.internal.cluster.*
            if ~AbstractHPCServerSOAConnection.foundRequiredLibraries
                error(message('parallel:cluster:HPCServerSOALibrariesNotLoaded'));
            end
            
            % Must have 2 input args
            narginchk(2, 2);
            % And schedulerConnection must be of type Microsoft.Hpc.Scheduler.Scheduler
            if ~isa(schedulerConnection, 'Microsoft.Hpc.Scheduler.Scheduler')
                error(message('parallel:cluster:HPCServerInvalidConnection'));
            end
            
            % Check for version compatibility before going any further.
            schedulerVersion = double(schedulerConnection.GetServerVersion().Major);
            if ~obj.isClientCompatible(schedulerVersion)
                error(message('parallel:cluster:HPCServerIncorrectSchedulerVersion', schedulerVersion, obj.getRequiredMicrosoftAPIVersion()));
            end

            % NB there is an assumption that the connection that we receive is already
            % connected to the correct scheduler.  Unfortunately, Microsoft's API does
            % not give us a nice way of doing this.
            % TODO - a hack may be to get the cluster environment variables and
            % get the value of CCP_CLUSTER_NAME.  CCP_CLUSTER_NAME is present by default and
            % it appears that it is read-only, but there is no documentation to indicate that
            % this is really the case.  In fact,  Microsoft's documentation seems to imply
            % that CCP_CLUSTER_NAME _can_ be changed, although it is not something that users
            % would generally wish to do:
            % http://technet.microsoft.com/en-us/library/cc720153%28WS.10%29.aspx
            obj.SchedulerConnection = schedulerConnection;

            obj.SchedulerHostname = schedulerHostname;
            % Check to see if broker nodes are available - this determines whether or not
            % the cluster can run SOA jobs
            if ~obj.brokerNodesAvailable
                error(message('parallel:cluster:HPCServerSOANotSupported', schedulerHostname))
            end
        end
        
        %---------------------------------------------------------------
        % submitJob
        %---------------------------------------------------------------
        function [jobIDs, jobName] = submitJob(obj, jobID, jobLocation, ...
                taskIDs, taskLocations, taskLogLocations, jobTemplate, ...
                matlabExe, matlabArgs, jobEnvironmentVariables, username, password)
            
            dotnetEnvironmentVariables = iConvertCellstrToDotNetDictionaryString(jobEnvironmentVariables);
            % And now do the actual job submission
            [jobIDs, jobName] = obj.doJobSubmission(jobID, jobLocation, ...
                taskIDs, taskLocations, taskLogLocations, jobTemplate, ...
                matlabExe, matlabArgs, dotnetEnvironmentVariables, username, password);
        end
        
        %---------------------------------------------------------------
        % isServiceLikelyInstalledCentrally
        %---------------------------------------------------------------
        function serviceFound = isServiceLikelyInstalledCentrally(obj)
            % What is our best guess about whether the service has
            % been installed centrally?
            % 
            % See if CCP_SERVICEREGISTRATION_PATH is defined as an 
            % environment variable.  If so, see if the service config
            % file is in the path.  The service config file is called
            % MdcsServiceForPCT<version>.config
            %
            % Note that the CCP_SERVICEREGISTRATION_PATH may contain
            % shared and local folders.  If the client is not a cluster node
            % and one of the local folders defined in CCP_SERVICEREGISTRATION_PATH
            % happens to exist, then that folder will be searched.  This could
            % result in a false positive.
            serviceFound = false;
            serviceRegistrationClusterVariable = 'CCP_SERVICEREGISTRATION_PATH';
            serviceRegistrationPath = '';
            try
                allVariables = obj.SchedulerConnection.EnvironmentVariables;
                % convert the variables from a Microsoft.Hpc.Scheduler.INameValueCollection to a generic list 
                % because INameValueCollection.GetEnumerator() doesn't seem to work.
                envsList = NET.createGeneric('System.Collections.Generic.List', ...
                    {'Microsoft.Hpc.Scheduler.NameValue'}, allVariables);

                for ii = 0:envsList.Count-1
                    currEnv = envsList.Item(ii);
                    if strcmpi(char(currEnv.Name), serviceRegistrationClusterVariable)
                        serviceRegistrationPath = char(currEnv.Value);
                        break;
                    end
                end
            catch err %#ok<NASGU>
                % Do nothing - if we've found the variable, then we will use it.
                % If we haven't found it, the serviceRegistrationPath will just be
                % empty
            end
            
            if isempty(serviceRegistrationPath)
                return;
            end
            
            % Split up the path into individual directories and see if the
            % service file is contained in the directory.  Note that the path
            % will never contain "", even if there are spaces in the folder names.
            allDirectories = regexp(serviceRegistrationPath, pathsep, 'split');
            for ii = 1:numel(allDirectories)
                currDir = allDirectories{ii};
                if ~exist(currDir, 'dir')
                    continue;
                end
                
                serviceFound = obj.isServiceConfigInDirectory(currDir);
                if serviceFound
                    break;
                end
            end
        end
    end
    
    methods (Abstract)
        closeJob(obj, hpcServerJobID, jobName);
    end
    
    
    methods (Abstract, Access = protected)
        [jobIDs, jobName] = doJobSubmission(obj, jobID, jobLocation, ...
            taskIDs, taskLocations, taskLogLocations, jobTemplate, ...
            matlabExe, matlabArgs, environmentVariableDictionary, username, password);
    end
    
    
    methods (Access = private)
        %---------------------------------------------------------------
        % brokerNodesAvailable
        %---------------------------------------------------------------
        function available = brokerNodesAvailable(obj)
            try
                brokerNodes = obj.SchedulerConnection.GetNodesInNodeGroup('WCFBrokerNodes');
                available = brokerNodes.Count > 0;
            catch err %#ok<NASGU>
                available = false;
            end
        end
    end
end

%---------------------------------------------------------------------
% iIsNetworkInstall
%---------------------------------------------------------------------
function result = iIsNetworkInstall
result = false;

fullMatlabRoot = dctReplaceDriveWithUNCPath(matlabroot);
if ~isempty(regexp(fullMatlabRoot, '^\\\\', 'once'))
    result = true;
end
end

%---------------------------------------------------------------------
% iConvertCellstrToDotNetDictionaryString
%---------------------------------------------------------------------
function dotNetDictionaryString = iConvertCellstrToDotNetDictionaryString(cellArrayOfStrings)
dictionarySize = size(cellArrayOfStrings, 1);
dotNetDictionaryString = NET.createGeneric('System.Collections.Generic.Dictionary', ...
    {'System.String', 'System.String'}, dictionarySize);
for ii = 1:dictionarySize
    dotNetDictionaryString.Add(cellArrayOfStrings{ii, 1}, cellArrayOfStrings{ii,2});
end
end
