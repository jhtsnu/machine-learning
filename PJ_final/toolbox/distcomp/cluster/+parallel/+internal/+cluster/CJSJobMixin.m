% CJSJobMixin Mixin class providing hidden/controlled access methods for CJS*Job
% classes.

% Copyright 2011-2012 The MathWorks, Inc.

classdef CJSJobMixin < handle
    properties (Transient, SetAccess = immutable, GetAccess = protected)
        Support
        SupportID
    end

    methods ( Access = protected )
        function obj = CJSJobMixin( support, jobsid )
            obj.Support   = support;
            obj.SupportID = jobsid;
        end
    end

    methods ( Hidden, Access = ?parallel.internal.cluster.CJSSupport )
        function t = hBuildChild( job, ~, taskid, tasksid )
            t = parallel.task.CJSTask( job, taskid, tasksid, job.Support );
        end
    end

    methods ( Hidden, Access = { ...
        ?parallel.internal.cluster.JobStateHelper, ...
        ?parallel.cluster.CJSCluster, ...
        ?parallel.job.CJSIndependentJob } )
        function s = hGetStateFromTasks( job )
            import parallel.internal.cluster.CJSJobMethods
            s = CJSJobMethods.getJobStateFromTasks( job );
        end
    end

    methods ( Hidden )
        function hSetPropertyNoCheck( obj, propName, val )
            obj.Support.setJobProperties( obj.SupportID, obj.Variant, propName, val );
        end
        function v = hGetProperty( obj, propName )
            try
                v = obj.Support.getJobProperties( obj.SupportID, obj.Variant, propName );
            catch err
                throwAsCaller(err);
            end
        end
        function sid = hGetSupportID( obj )
            sid = obj.SupportID;
        end
    end

    methods ( Hidden, Access = ?parallel.cluster.CJSCluster )
        function hSetTerminalStateFromCluster( obj, state )
            import parallel.internal.cluster.CJSJobMethods

            CJSJobMethods.setJobTerminalStateFromCluster( obj, state );
            % If we get here, we are being informed that the job is terminal, so
            % we should maybe-update our tasks with finish time information.
            if state == parallel.internal.types.States.Finished
                iMaybeUpdateTaskFinishTimes( obj.Tasks, ...
                                             obj.hGetProperty( 'FinishTime' ) );
            end
        end
    end
    methods ( Access = protected )
        function varargout = getTasksByStateImpl( obj )
            import parallel.internal.types.States;

            statesStr = { States.Pending.Name, ...
                          States.Running.Name, ...
                          States.Finished.Name, ...
                          States.Failed.Name };

            prff = obj.Support.getTasksByState( obj, obj.SupportID, statesStr );
            % Need to concatenate Finished/Failed
            if nargout == 3
                varargout = { prff(1), prff(2), {[prff{3:end}]} };
            else
                varargout = { [ prff(1:2), {[prff{3:end}]} ] };
            end
        end
    end
end

% --------------------------------------------------------------------------
% If we find out that a job has finished from the Cluster, we should ensure that
% the task finish times are set.
function iMaybeUpdateTaskFinishTimes( tasks, jobFinishTime )
    import parallel.internal.types.States

    for ii = 1:length( tasks )
        thisTask = tasks(ii);
        if thisTask.hGetProperty( 'StateEnum' ) == States.Finished && ...
                isempty( thisTask.hGetProperty( 'FinishTime' ) )
            thisTask.hSetPropertyNoCheck( 'FinishTime', jobFinishTime );
        end
    end
end
