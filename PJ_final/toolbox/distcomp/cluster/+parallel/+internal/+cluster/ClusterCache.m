% ClusterCache - A singleton instance to hold cluster mementos and objects.
% 

% Copyright 2012 The MathWorks, Inc.

classdef ( Hidden ) ClusterCache < parallel.internal.cluster.Cache
    
    properties ( Constant, Hidden )
        Instance = parallel.internal.cluster.Cache( 3 );
    end
    
end
