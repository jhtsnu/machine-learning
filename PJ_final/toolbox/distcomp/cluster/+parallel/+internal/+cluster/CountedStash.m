% A place to stash object instances to maintain a reference to that instance.
%
% Each call to stash with the same key increments a count, and unstashing this
% key decrements the count. When the count reaches zero the reference to the key
% is dropped.

% Copyright 2011 The MathWorks, Inc.
classdef( Sealed ) CountedStash < handle
    
    properties( Access = private )
        Entries = struct( 'key', {}, 'count', {} );
    end
    
    methods
        function count = stash( this, key )
            idx = this.findEntry( key );
            if ~any( idx )
                idx = this.newEntry( key );
            end
            
            count = this.getCount( idx ) + 1;
            this.setCount( idx, count );
        end
        
        function count = unstash( this, key )
            idx = this.findEntry( key );
            assert( any( idx ), 'No such key' );
            
            count = this.getCount( idx ) - 1;
            if count == 0
                this.removeEntry( idx );
            else
                this.setCount( idx, count );
            end
        end
    end
    
    methods( Access = private )
        function idx = findEntry( obj, key )
            idx = arrayfun( @(s)s.key == key, obj.Entries );           
        end
        function idx = newEntry( obj, key )
            idx = length(obj.Entries) + 1;
            obj.Entries(idx) = struct( 'key', key, 'count', 0 );
        end
        function removeEntry( obj, idx )
            obj.Entries(idx) = [];
        end
        function c = getCount( obj, idx  )
            c = obj.Entries( idx ).count;
        end
        function setCount( obj, idx, count )
            obj.Entries( idx ).count = count;
        end
    end    
end
