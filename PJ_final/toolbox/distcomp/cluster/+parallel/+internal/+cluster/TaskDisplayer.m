% TaskDisplayer - Displayer for parallel.Task

% Copyright 2011-2012 The MathWorks, Inc.

classdef TaskDisplayer < parallel.internal.display.Displayer
    properties (Constant, GetAccess = private)
        BaseDisplayClass = 'parallel.Task';
        VectorDisplayProps = {'ID', 'State', 'FinishTime', 'Function', 'Error'};
        VectorTableColumns = { ...
            % 'Title',    'Resizeable', 'MinimumWidth'
            'ID',         true,         length('ID'); ...
            'State',      false,        length('unavailable'); ...
            'FinishTime', false,        length('mmm dd HH:MM:SS'); ...
            'Function',   true,         length('Function'); ...
            'Error',      false,        length('Error'); ...
            }
    end

    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract Displayer properties
        DisplayHelper 
    end
 
    methods (Static)   
        function obj = buildForTask( hetTask )
        %buildForTask - build for all task classes
        %   simply uses the default.
            obj = parallel.internal.cluster.TaskDisplayer( hetTask );
        end
        function obj = buildForCJS( cjsTask )
            obj = parallel.internal.cluster.TaskDisplayer( cjsTask );
        end
        
        function obj = buildForMJS( mjsTask )
            obj = parallel.internal.cluster.TaskDisplayer( mjsTask );
        end
    end
    
    methods 
        function obj = TaskDisplayer(someObj)
            obj@parallel.internal.display.Displayer(someObj, parallel.internal.cluster.TaskDisplayer.BaseDisplayClass);
            obj.DisplayHelper = parallel.internal.display.DisplayHelper( length('Running Duration'), obj.ShowLinks );
        end
    end
    
    methods (Access = private)    
        function doInvalidSingleDisplay( obj, invalidToDisp )
            obj.DisplayHelper.displayMainHeading(getString(message('parallel:cluster:InvalidTaskID', invalidToDisp.ID)));
            
            obj.DisplayHelper.displayProperty('State', invalidToDisp.State);
        end
    end

    methods (Access = protected)        
        function doSingleDisplay(obj, toDisp)
            import parallel.internal.types.States

            if ~States.isValidState( toDisp.State )
                % Do not attempt to access any further info.
                obj.doInvalidSingleDisplay( toDisp );
                return
            end
            
            % For the moment, for tasks, we only want to display help for the common base class
            obj.DisplayHelper.displayMainHeading('%s %s \n', ...
                obj.formatDocLink(parallel.internal.cluster.TaskDisplayer.BaseDisplayClass), getString( message('MATLAB:ClassText:DISPLAY_WITH_PROPS_LABEL') ));
            
            obj.displaySpecificItems(toDisp);
        end

        function doVectorDisplay(obj, toDisp)
            import parallel.internal.cluster.TaskDisplayer
            import parallel.internal.types.States

            tableData = cell(numel(toDisp), size(TaskDisplayer.VectorTableColumns, 1));
            for ii = 1:numel(toDisp)
                currToDisp = toDisp(ii);
                if isvalid(currToDisp)
                    if States.isValidState( currToDisp.State )
                        propsToDisp = obj.getDisplayPropsAsStruct( toDisp(ii), TaskDisplayer.VectorDisplayProps );
                        dataRow = {obj.hGetLink(currToDisp, propsToDisp.ID), ...
                                   propsToDisp.State, ...
                                   obj.DisplayHelper.formatTimeString(propsToDisp.FinishTime), ...
                                   propsToDisp.Function, ...
                                   iGetErrorVectorDisplayValue(propsToDisp.Error)};
                    else
                        dataRow = {currToDisp.ID, currToDisp.State, ...
                                   '', '', ''};
                    end
                else
                    dataRow = {'', parallel.internal.display.Displayer.DeletedString, '', '', ''};
                end
                tableData(ii, :) = dataRow;
            end
            
            % In the vector display we only want to display help for the common base class
            obj.DisplayHelper.displayDimensionHeading(size(toDisp), obj.formatDocLink(TaskDisplayer.BaseDisplayClass) );
            obj.DisplayHelper.displayTable(TaskDisplayer.VectorTableColumns, tableData, obj.DisplayHelper.DisplayIndexColumn);
        end
        
    end
    
end

function displayValue = iGetErrorVectorDisplayValue(anError)
if isempty( anError )
    displayValue = '';
else
    displayValue = 'Error';
end
end
