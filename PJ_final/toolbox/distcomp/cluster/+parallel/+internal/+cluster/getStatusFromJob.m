function jobStatus = getStatusFromJob(jobOrJobs, jobStatesCell)
; %#ok Undocumented
% Inputs:
% jobOrJobs - array of jobs
% jobStatesCell - cell array of states of those jobs
% Outputs:
% array of job status structs.

% Copyright 2011-2012 The MathWorks, Inc.

if isempty( jobOrJobs )
    jobStatus = struct();
    return
end

% Get all the tasks in one findTask call.
[pending, running, finished] = findTask( jobOrJobs );

% Task IDs of the finished tasks
finishedTaskIDs = [ finished.ID ].';

% Get all Errors from finished tasks only
errors   = get( finished, {'Error'} );
goterror = ~cellfun( @isempty, errors );

% Get the job IDs of the pending, running, and finished tasks. Note that
% task.Parent.ID is a relatively quick operation because the storage doesn't
% need to be consulted.
pendTaskJobID     = arrayfun( @(t) t.Parent.ID, pending );
runningTaskJobID  = arrayfun( @(t) t.Parent.ID, running );
finishedTaskJobID = arrayfun( @(t) t.Parent.ID, finished );

% Get the job IDs as a column:
jobIDs = [ jobOrJobs.ID ].';

% Work out how many tasks are in each state per job by counting up from each
% list of tasks how many match a particular job ID.
pendTasksPerJob       = arrayfun( @(id) sum( pendTaskJobID == id ), jobIDs );
runningTasksPerJob    = arrayfun( @(id) sum( runningTaskJobID == id ), jobIDs );
finishedTasksPerJob   = arrayfun( @(id) sum( finishedTaskJobID == id ), jobIDs );
erroredTasksPerJob    = arrayfun( @(id) sum( goterror & finishedTaskJobID == id ), jobIDs );
successfulTasksPerJob = finishedTasksPerJob - erroredTasksPerJob;
totalTasksPerJob      = pendTasksPerJob + runningTasksPerJob + finishedTasksPerJob;

% For each job, extract the task errors and task IDs
[erroredTaskErrors, erroredTaskIds] = arrayfun( ...
    @(id) iTaskErrors(id, finishedTaskJobID, finishedTaskIDs, errors, goterror ), jobIDs, ...
    'UniformOutput', false );

jobStatus = struct('State'                  , jobStatesCell, ...
                   'TotalTasks'             , num2cell( totalTasksPerJob ), ...
                   'PendingTasks'           , num2cell( pendTasksPerJob ), ...
                   'RunningTasks'           , num2cell( runningTasksPerJob ), ...
                   'FinishedTasks'          , num2cell( finishedTasksPerJob ), ...
                   'FinishedTasksNoError'   , num2cell( successfulTasksPerJob ), ...
                   'FinishedTasksWithError' , num2cell( erroredTasksPerJob ), ...
                   'ErroredTaskErrors'      , erroredTaskErrors, ...
                   'ErroredTaskIDs'         , erroredTaskIds );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inputs:
% jobID - a scalar job ID
% jobIDofTasks - list of job IDs corresponding to the list of taskIDs
% taskIDs - list of task IDs
% taskErrors - cell array of Errors from the tasks
% taskHasError - logical vector indicating if each task has an error.
% Outputs:
% errs - array of MException (may be empty)
% ids - array of task IDs ([] in the case of no errors)
function [errs, ids] = iTaskErrors( jobID, jobIDofTasks, taskIDs, taskErrors, taskHasError )

% first, find the tasks we're dealing with
idx = jobIDofTasks == jobID & taskHasError;
if any( idx )
    % Return cell array of error in instead of array because the error can
    % be a matlab.exception.JavaException as well
    errs = taskErrors(idx).';
    ids = taskIDs( idx );
else
    errs = {};
    ids  = [];
end
end
