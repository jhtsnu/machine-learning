% CancelException - helpers to build the MException with which to cancel a job
% or task.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) CancelException
    methods ( Static )
        function e = getDefaultForTask()
            [user, host] = iGetUserHost();
            e = MException(message('parallel:task:UserCancellation', user, host));
        end
        function e = getWithUserMessageForTask( userMessage )
            [user, host] = iGetUserHost();
            e = MException(message('parallel:task:UserCancellationWithMessage', user, host, userMessage));
        end
        function e = getDefaultForJob()
            [user, host] = iGetUserHost();
            e = MException(message('parallel:job:UserCancellation', user, host));
        end
        function e = getWithUserMessageForJob( userMessage )
            [user, host] = iGetUserHost();
            e = MException(message('parallel:job:UserCancellationWithMessage', user, host, userMessage));
        end
    end
end

function [userStr, hostStr] = iGetUserHost()
    userStr = char(java.lang.System.getProperty('user.name'));
    hostStr = char(java.net.InetAddress.getLocalHost.getCanonicalHostName);
end
