function funcStr = generateTaskFunctionString(funcHandle)

%   Copyright 2010 The MathWorks, Inc.

; %#ok Undocumented
% Returns a string representing the task function given the function
% handle, ensuring that it begins with an '@'.

funcStr = char(funcHandle);
if ~isempty(funcStr) && funcStr(1) ~= '@'
    funcStr = ['@' funcStr];
end