function [isLicensedOnline, token, entitlementID] = getOnlineLicenseInfo()

%   Copyright 2012 The MathWorks, Inc.

licInfo = internal.matlab.licensing.getLicMode();

isLicensedOnline = strcmp(licInfo.licmode, 'online');
token = licInfo.token;
entitlementID = licInfo.entitlement_id;