% CustomPropDisp - base class implementing display functionality

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) CustomPropDisp < handle

    methods ( Sealed ) % must be sealed to be invokable from Heterogeneous subclasses
        function disp( x )
            d = iGetDisplayer(x);
            d.doDisp( x );
        end
        function display( x )
            d = iGetDisplayer(x);
            d.doDisplay( x, inputname(1) );
        end
    end

    methods ( Hidden )
        function propValues = hGetDisplayPropertiesNoError(obj, propNames, vectorizedGetFunc)
            if nargin<3
                vectorizedGetFunc = @get;
            end
            
            returnCharOnly = ischar( propNames );
            
            if returnCharOnly
                propNames = {propNames};
            end
            
            try
                propValues = feval( vectorizedGetFunc, obj, propNames );
            catch err %#ok<NASGU>
                % See if we have any better luck getting each prop
                % individually.
                propValues = cellfun(@(x) obj.getSingleDisplayPropertyNoError(x), ...
                    propNames, 'UniformOutput', false);
            end
            if returnCharOnly
                propValues = propValues{1};
            end
        end
    end

    methods (Access = private)
        function v = getSingleDisplayPropertyNoError(obj, propName)
            v = '';
            try
                v = obj.get(propName);
            catch err %#ok<NASGU>
            end
        end
    end
end

% --------------------------------------------------------------------------
% Return the displayer for a given object
function d = iGetDisplayer( obj )
    persistent sDISP_MAP
    persistent sINVALID_DISP
    if isempty( sDISP_MAP )
        sDISP_MAP     = containers.Map;
        sINVALID_DISP = parallel.internal.display.InvalidDisplayer();
    end
    key = class( obj );

    % Also handles empty obj.
    if ~isvalid( obj )
        d = sINVALID_DISP;
    elseif sDISP_MAP.isKey( key )
        d = sDISP_MAP( key );
    else
        d = iGetDisplayerFromClass( obj );
        sDISP_MAP( key ) = d;
    end
end

function d = iGetDisplayerFromClass( obj )
    if isa( obj, 'parallel.cluster.MJS' ) 
        d = parallel.internal.cluster.MJSClusterDisplayer( obj );
    elseif isa( obj, 'parallel.cluster.MJSComputeCloud' )
        d = parallel.internal.cluster.MJSComputeCloudDisplayer( obj );
    elseif isa( obj, 'parallel.Cluster' )
        d = parallel.internal.cluster.ClusterDisplayer( obj );
    elseif isa( obj, 'parallel.Job' )
        d = parallel.internal.cluster.JobDisplayer( obj );
    elseif isa( obj, 'parallel.task.CJSTask' )
        d = parallel.internal.cluster.TaskDisplayer.buildForCJS( obj );
    elseif isa( obj, 'parallel.task.MJSTask' )
        d = parallel.internal.cluster.TaskDisplayer.buildForMJS( obj );
    elseif isa( obj, 'parallel.Task' )
        % het array of task
        d = parallel.internal.cluster.TaskDisplayer.buildForTask( obj );
    elseif isa( obj, 'parallel.Worker' )
        d = parallel.internal.cluster.WorkerDisplayer( obj );
    else
        warning(message('parallel:cluster:NoDisplayAvailable', class( obj )));
        d = struct( 'doDisp',    @(x,y) disp( x.get ), ...
                    'doDisplay', @(x,y) disp( x.get ) );
    end
end
