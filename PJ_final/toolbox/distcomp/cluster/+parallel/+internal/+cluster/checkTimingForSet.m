function checkTimingForSet( stateEnum, timing, propName )
% checkTimingForSet - used by Job and Task to check whether a property can be
% set.
% - stateEnum: the parallel.internal.types.States of the object
% - timing: the timing portion of a constraint
% - propName: the name of the property being set (for error messages)

% Copyright 2011 The MathWorks, Inc.

validateattributes( stateEnum, {'parallel.internal.types.States'}, {'scalar'} );
validateattributes( timing,    {'char'}, {'row'} ); % 'row' enforces non-empty
validateattributes( propName,  {'char'}, {'row'} );

switch timing
  case 'presubmission'
    % Can only be set while pending
    if stateEnum ~= parallel.internal.types.States.Pending
        error(message('parallel:cluster:PropertyOnlySetWhilePending', propName));
    end
  otherwise
    error(message('parallel:cluster:UnknownTimingConstraint', timing));
end

end
