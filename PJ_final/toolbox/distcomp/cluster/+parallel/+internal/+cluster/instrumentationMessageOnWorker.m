function instrumentationMessageOnWorker(varargin)
; %#ok Undocumented
%INSTRUMENTATIONMESSAGEONWORKER Logs instrumenting messages only if 
%   running on a worker.
%   Uses instrumentationMessage to do the logging.
%
%  INSTRUMENTATIONMESSAGEONWORKER(VARARGIN)
%

%  Copyright 2010 The MathWorks, Inc.

% $Revision: 1.1.6.1 $  $Date: 2010/07/19 12:49:53 $

mlock
persistent isWorker

if isempty(isWorker)
    isWorker = system_dependent('isdmlworker');
end

if isWorker
    % Just use instrumentationMessage to do the logging.  
    % NB instrumentationMessage does not throw errors
    parallel.internal.cluster.instrumentationMessage(varargin{:});
end


