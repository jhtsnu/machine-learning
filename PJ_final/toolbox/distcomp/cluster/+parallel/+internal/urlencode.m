function urlOut = urlencode( urlIn )
%URLENCODE Replace special characters with escape characters URLs need
%
% Example:
%   encodedUrl = parallel.internal.urlencode( url )
%
%   decodedUrl = parallel.internal.urldecode( encodedUrl );
%
% See also parallel.internal.urldecode

% Copyright 2010 The MathWorks, Inc.

urlOut = char(java.net.URLEncoder.encode(urlIn,'UTF-8'));
