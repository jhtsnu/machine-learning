function runprop = localMpiexecTask( runprop )

% Copyright 2011-2012 The MathWorks, Inc.

dctSchedulerMessage(2, 'In parallel.internal.decode.localMpiexecTask');

labIndexStr         = num2str( labindex );
storageConstructor  = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation     = parallel.internal.urldecode( getenv('MDCE_STORAGE_LOCATION') );
jobLocation         = getenv('MDCE_JOB_LOCATION');
taskLocation        = [jobLocation filesep 'Task' labIndexStr ];

set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.Local', ...
    'FallbackSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation);

% Ensure that on unix we mask SIGINT and SIGSTOP as the shell 
% will send these signals to all members of the main MATLAB
% process group. Thus we will replace the handlers with functions
% that do nothing so we are unaffected by these signals
parallel.internal.apishared.LocalUtils.installLocalSignalHandler();

end
