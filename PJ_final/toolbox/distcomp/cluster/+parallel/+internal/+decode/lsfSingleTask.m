function runprop = lsfSingleTask( runprop )
% lsfSingleTask - decode function

% Copyright 2007-2012 The MathWorks, Inc.

JOB_ID  = getenv('LSB_JOBID');
TASK_ID = getenv('LSB_JOBINDEX');

% Get a function handle to set status messages for lsf on this particular
% task - this will set the default message handler to be lsfSet
if ~isempty(getenv('MDCE_DEBUG'))
    aHandler = parallel.internal.apishared.LsfUtils.messageHandler( JOB_ID, TASK_ID );
    setSchedulerMessageHandler(aHandler);
end
dctSchedulerMessage(2, 'In lsfSingleTask with JOB_ID : %s and TASK_ID : %s', JOB_ID, TASK_ID);

storageConstructor = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation    = parallel.internal.urldecode( getenv('MDCE_STORAGE_LOCATION') );
jobLocation        = getenv('MDCE_JOB_LOCATION');
taskLocation       = [jobLocation filesep 'Task' TASK_ID ];

% Need to tell the job runner where it's dependency directory is
dependencyDir = [tempdir JOB_ID '.' TASK_ID '.mdce.temp'];

set(runprop, ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation, ...
    'DependencyDirectory', dependencyDir, ...
    'LocalSchedulerName', 'parallel.cluster.LSF', ...
    'FallbackSchedulerName', 'parallel.cluster.TaskRunner');
