function runprop = localParallelTask(runprop)

% Copyright 2004-2012 The MathWorks, Inc.

% Get a function handle to set status messages for lsf on this particular
% task - this will set the default message handler to be lsfSet
dctSchedulerMessage(2, 'In parallel.internal.decode.localParallelTask');

sentialLocation     = getenv('MDCE_SENTINAL_LOCATION');
numLabsStr          = getenv('MDCE_NUMLABS');
labIndexStr         = getenv('MDCE_LABINDEX');

storageConstructor  = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation     = parallel.internal.urldecode( getenv('MDCE_STORAGE_LOCATION') );
jobLocation         = getenv('MDCE_JOB_LOCATION');
taskLocation        = [jobLocation filesep 'Task' labIndexStr ];


set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.Local', ...
    'FallbackSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation);

% Ensure that on unix we mask SIGINT and SIGSTOP as the shell 
% will send these signals to all members of the main MATLAB
% process group. Thus we will replace the handlers with functions
% that do nothing so we are unaffected by these signals
parallel.internal.apishared.LocalUtils.installLocalSignalHandler();

try
    parallel.internal.apishared.LocalUtils.parallelConnectAccept( ...
        sentialLocation, str2double(labIndexStr), str2double(numLabsStr));
catch err
    dctSchedulerMessage(1, 'Error in iParallelConnection :\n%s', ...
                        err.message);
    rethrow(err);
end
