function runprop = hpcserverSoaTask(runprop)

% Copyright 2011-2012 The MathWorks, Inc.

dctSchedulerMessage(2, 'In parallel.internal.decode.hpcserverSoaTask');

% We only expect a single nargin in runprop.Varargin.  This is the serialized 
% arguments received over the IPC channel
serializedArgs = runprop.DecodeArguments{1};
% get the taskLocation from the serialized arguments
[taskLocation, performJobInit, dependencyDir] = iDeserializeArgs(serializedArgs);

% Get the other runprop fields from the environment variables.
storageConstructor = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation    = parallel.internal.urldecode( getenv('MDCE_STORAGE_LOCATION') );
jobLocation        = getenv('MDCE_JOB_LOCATION');

% Never remove the dependency dir.  This will be done in C# code
set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.HPCServer', ...
    'FallbackSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation, ...
    'DependencyDirectory', dependencyDir, ...
    'ExitOnTaskFinish', false, ...
    'AppendPathDependencies', performJobInit, ...
    'AppendFileDependencies', performJobInit, ...
    'IsFirstTask', performJobInit, ...
    'CleanUpDependencyDirOnTaskFinish', false);

    

function [taskLocation, performJobInit, dependencyDir] = iDeserializeArgs(serializedArgs)
dctSchedulerMessage(2, 'About to deserialize args')
% The serializedArgs will consist of multiple strings delimited by a newline character.
argsStr = char(serializedArgs);
% We expect only 3 args: <taskLocation>\n<isFirstTask>\n<dependencyDir>
args = textscan(argsStr, '%s', 'delimiter', '\n');
args = args{1};
if length(args) ~= 3
    error(message('parallel:cluster:HPCServerSoaDecodeTooManySerializedArgs', length( args )));
end

[taskLocationStr, performJobInitStr, dependencyDirStr] = args{:};

% get rid of any leading/trailing whitespace from the raw strings
taskLocation = strtrim(taskLocationStr);
dependencyDir = strtrim(dependencyDirStr);
performJobInitStr = strtrim(performJobInitStr);

try
    % Convert the performJobInit value from string to number and then to logical
    performJobInit = logical(str2double(performJobInitStr));
catch err
    newErr = MException(message('parallel:cluster:HPCServerSoaDecodeBadPerformJobInitArg'));
    newErr = newErr.addCause(err);
    throw(newErr);
end
