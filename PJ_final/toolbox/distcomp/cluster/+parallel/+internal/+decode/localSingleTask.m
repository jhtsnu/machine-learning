function runprop = localSingleTask(runprop)

% Copyright 2004-2012 The MathWorks, Inc.

% Get a function handle to set status messages for lsf on this particular
% task - this will set the default message handler to be lsfSet
dctSchedulerMessage(2, 'In parallel.internal.decode.localSingleTask');


storageConstructor = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation    = parallel.internal.urldecode( getenv('MDCE_STORAGE_LOCATION') );
jobLocation        = getenv('MDCE_JOB_LOCATION');
taskLocation       = getenv('MDCE_TASK_LOCATION');
    
set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.Local', ...
    'FallbackSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation);

% Ensure that on unix we mask SIGINT and SIGSTOP as the shell 
% will send these signals to all members of the main MATLAB
% process group. Thus we will replace the handlers with functions
% that do nothing so we are unaffected by these signals
parallel.internal.apishared.LocalUtils.installLocalSignalHandler();

end
