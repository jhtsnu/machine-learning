function runprop = hpcserverParallelTask(runprop)

% Copyright 2011-2012 The MathWorks, Inc.

dctSchedulerMessage(2, 'In parallel.internal.decode.hpcserverParallelTask');

ccpJobId  = getenv('CCP_JOBID');
taskId = num2str(labindex);

storageConstructor = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation    = parallel.internal.urldecode(getenv('MDCE_STORAGE_LOCATION'));
jobLocation        = getenv('MDCE_JOB_LOCATION');
taskLocation       = [jobLocation filesep 'Task' taskId ];
    
% Need to tell the job runner where it's dependency directory is
dependencyDir = [tempdir ccpJobId '.' taskId '.mdce.temp'];

set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.HPCServer', ...
    'FallbackSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation, ...
    'DependencyDirectory', dependencyDir);
