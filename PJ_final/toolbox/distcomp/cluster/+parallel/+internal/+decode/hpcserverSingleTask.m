function runprop = hpcserverSingleTask(runprop)

% Copyright 2011-2012 The MathWorks, Inc.

dctSchedulerMessage(2, 'In parallel.internal.decode.hpcserverSingleTask');

storageConstructor = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation    = parallel.internal.urldecode(getenv('MDCE_STORAGE_LOCATION'));
jobLocation        = getenv('MDCE_JOB_LOCATION');
taskLocation       = getenv('MDCE_TASK_LOCATION');
    

% Need to tell the job runner where it's dependency directory is
ccpJobId  = getenv('CCP_JOBID');
ccpTaskId = getenv('CCP_TASKID');
dependencyDir = [tempdir ccpJobId '.' ccpTaskId '.mdce.temp'];

set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.HPCServer', ...
    'FallbackSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ....
    'TaskLocation', taskLocation, ...
    'DependencyDirectory', dependencyDir);

end