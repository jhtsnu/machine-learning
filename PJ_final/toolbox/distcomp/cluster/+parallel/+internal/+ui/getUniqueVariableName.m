function uniqueVarName = getUniqueVariableName(varName, who_output)
% GETUNIQUEVARIABLENAME - returns an unique variable name

%  Copyright 2011 The MathWorks, Inc.
%  $Revision: 1.1.6.1 $    $Date: 2011/03/14 02:46:57 $

% Validate inputs
validateattributes(varName, {'char'}, {'nonempty'}, 'getUniqueVariableName', 'varName', 1);

isExistInBase = any(strcmp(who_output, varName));

% Check if the variable already exists
if ~isExistInBase
    uniqueVarName = varName;
    return
end

% If the variable name already exists in the base workspace, then
% append an underscore to the variable name (e.g Job1_1)
counter = 0;

while isExistInBase
    counter = counter + 1;
    uniqueVarName = sprintf('%s_%d', varName, counter);
    isExistInBase = any(strcmp(who_output, uniqueVarName));    
end