function showMoreInfo(jobVarName)
% SHOWMOREINFO - Shows more information on working with the batch job

%  Copyright 2011 The MathWorks, Inc.

% Validate inputs
validateattributes(jobVarName, {'char'},  {'nonempty'}, 'showMoreInfo', 'jobVarName', 1);

fprintf('\n');
fprintf('   To wait for the batch job to finish:\n');
iDisplayCodeWithLink(sprintf('wait(%s)',jobVarName));

fprintf('   To display the diary from the batch job:\n');
iDisplayCodeWithLink(sprintf('diary(%s)',jobVarName));

fprintf('   To load workspace variables from the batch job:\n');
iDisplayCodeWithLink(sprintf('load(%s)',jobVarName))

fprintf('   To remove the batch job from the scheduler:\n');
if parallel.internal.ui.shouldUseAPI2()
    iDisplayCodeWithLink(sprintf('delete(%s)',jobVarName))
else
    iDisplayCodeWithLink(sprintf('destroy(%s)',jobVarName))
end

batchHelp = 'batch';
if (feature('hotlinks'))
    % Make it a hyperlink if hotlinks is enabled
    batchHelp = sprintf('<a href ="matlab: help %s">%s</a>',batchHelp,batchHelp);   
end
fprintf('   See Also: %s \n', batchHelp);


function iDisplayCodeWithLink(code)
% Displays the link for displaying the code and running the code in the
% MATLAB command line

if (feature('hotlinks'))
    % Make it a hyperlink if hotlinks is enabled
    str = sprintf('<a href ="matlab: parallel.internal.ui.showCode(''%s''), %s">%s</a>\n\n', code, code, code);
else
    str = sprintf('%s\n\n',code); 
end

fprintf('   %s', str);




