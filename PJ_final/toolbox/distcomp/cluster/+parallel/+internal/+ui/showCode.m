function showCode(code)
% SHOWCODE - Shows the code in the command window and command history

%  Copyright 2011 The MathWorks, Inc.
%  $Revision: 1.1.6.1 $    $Date: 2011/04/12 20:22:24 $ 

com.mathworks.mde.cmdhist.CmdHistory.getInstance().add(code);
fprintf('>> %s\n',code)

