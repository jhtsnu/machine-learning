function [allUsersJobTableRows, individualUserJobTableRows] = getJobTableRows(profileName)
% GETJOBTABLEROWS - gets the job table rows for all users and individual user for the profile

%   Copyright 2011-2012 The MathWorks, Inc.

% Get the cluster and deduce the correct username
clus = parallel.internal.ui.getCluster(profileName);
if isprop(clus, 'Username')
    username = clus.Username;
else
    username = distcomp.pGetDefaultUsername();
end

% Get all jobs in a single call
jobs = findJob( clus );

% Pass the username in to iGetJobTableRows so that we don't duplicate effort -
% this method will filter.
[allUsersJobTableRows, individualUserJobTableRows] = iGetJobTableRows(jobs, username);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [allUsersJobTableRows, individualUserJobTableRows] = iGetJobTableRows(jobs, filterUserName)
% IGETJOBTABLEROWS - Create java job table rows for the jobs

% Initialize the all user and individual user table rows to empty lists.
allUsersJobTableRows = java.util.ArrayList;
individualUserJobTableRows = java.util.ArrayList;

if isempty( jobs )
    return
end

% Get job IDs directly - a local call
jobIDs = [jobs.ID].';
% Get all storage-backed properties in one call
data = get( jobs, {'Username', 'SubmitTime', 'FinishTime', 'State'} );

userNames    = data(:,1);
userMatch    = strcmp( userNames, filterUserName );
submitTimes  = data(:,2);
finishTimes  = data(:,3);
states       = data(:,4);
descriptions = parallel.internal.ui.getJobDescription(jobs);
javaJobTypes = iGetJavaJobTypes(jobs);

jobStatesStructureArray = parallel.internal.cluster.getStatusFromJob(jobs, states);

for iLoop = 1:numel(jobs)

    jobStatesStructure = jobStatesStructureArray(iLoop);

    errMessages = cellfun(@(eachErroredTaskErrors) eachErroredTaskErrors.message,...
        jobStatesStructure.ErroredTaskErrors, 'UniformOutput', false);

    eachJobState = com.mathworks.toolbox.distcomp.ui.jobmonitor.JobStateModel(...
        jobStatesStructure.State,...
        jobStatesStructure.TotalTasks,...
        jobStatesStructure.PendingTasks,...
        jobStatesStructure.RunningTasks,...
        jobStatesStructure.FinishedTasks,...
        jobStatesStructure.FinishedTasksNoError,...
        jobStatesStructure.FinishedTasksWithError,...
        jobStatesStructure.ErroredTaskIDs,...
        errMessages);

    thisRow = com.mathworks.toolbox.distcomp.ui.jobmonitor.JobTableRowModel(...
        jobIDs(iLoop),...
        descriptions{iLoop},...
        userNames{iLoop},...
        submitTimes{iLoop},...
        finishTimes{iLoop},...
        eachJobState,...
        javaJobTypes{iLoop});
    allUsersJobTableRows.add( thisRow );
    if userMatch(iLoop)
        individualUserJobTableRows.add( thisRow );
    end
end

function javaJobTypes = iGetJavaJobTypes(jobs)
% IGETJAVAJOBTYPES - Gets the java job types

import com.mathworks.toolbox.distcomp.ui.jobmonitor.JobType
import parallel.internal.apishared.BatchJobMethods
import parallel.internal.types.Variant

% Note: get Type as cell array
jobTypes = {jobs.Type};

javaJobTypes = cell(numel(jobs),1);

for iLoop = 1:numel(jobs)
    
    job = jobs(iLoop);
    jobType = jobTypes{iLoop};
    
    % Check if the job is a batch job
    % (IndependentJob/CommunicatingPoolJob)
    if BatchJobMethods.isBatchJob(job)
        if BatchJobMethods.isBatchScript( job )
            javaJobTypes{iLoop} = JobType.BATCH_JOB_ON_SCRIPT;
        else
            javaJobTypes{iLoop} = JobType.BATCH_JOB_ON_FUNCTION;
        end
    elseif isequal(jobType, Variant.IndependentJob.Name)
        javaJobTypes{iLoop} = JobType.INDEPENDENT_JOB;
    elseif isequal(jobType, Variant.CommunicatingSPMDJob.Name)
        javaJobTypes{iLoop} = JobType.COMMUNICATING_SPMD_JOB;
    elseif isequal(jobType, Variant.CommunicatingPoolJob.Name)
        javaJobTypes{iLoop} = JobType.COMMUNICATING_POOL_JOB;
        % MATLAB pool is a type of CommunicatingPoolJob
        if isequal(job.ApiTag, 'Created_by_matlabpool')
            if iIsInteractiveMatlabpool(job)
                javaJobTypes{iLoop} = JobType.CURRENT_INTERACTIVE_MATLABPOOL;
            else
                javaJobTypes{iLoop} = JobType.MATLABPOOL;
            end
        end      
    else
        error(message('parallel:ui:JobMonitorUnsupportedJobType', jobType));
    end
end  

function flag = iIsInteractiveMatlabpool(matlabpoolJob)
% IISINTERACTIVEMATLABPOOL - Determine if the matlabpool job is the
% interactive matlab pool on the client

% If there is no matlabpool open, exit early
if matlabpool('size')<=0
   flag = false; 
   return
end

% Get the profile name of the interactive MATLAB pool
interactiveObject = parallel.internal.pool.SessionManager.getSessionObject();
profileNameInteractiveObject = '';
if ~isempty(interactiveObject) && ~isempty(interactiveObject.ParallelJob)
    profileNameInteractiveObject = interactiveObject.ParallelJob.Parent.Profile;
end 

% Get the profile name of the job
profileNameJob = matlabpoolJob.Parent.Profile;

% Check if the profile names match and are not empty
flag = ~isempty(profileNameInteractiveObject) && ...
    isequal(profileNameInteractiveObject, profileNameJob);

