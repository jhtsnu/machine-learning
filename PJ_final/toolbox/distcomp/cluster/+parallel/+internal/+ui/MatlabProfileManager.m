classdef MatlabProfileManager < handle
    % MATLABPROFILEMANAGER - MATLAB profile manager
    
    %   Copyright 2011-2012 The MathWorks, Inc.
    
    properties (Access = 'private')
        % Note: The ObjectsWithCallbackListeners and CallbackListeners must
        % be cell arrays of the same length
        ObjectsWithCallbackListeners;
        CallbackListeners;
        
        JavaProfileManager;
    end
    
    methods (Access = 'private')
        
        function obj = MatlabProfileManager()
            % MATLABPROFILEMANAGER - private constructor to the MATLAB
            % profile manager
            
            obj.ObjectsWithCallbackListeners = {};
            obj.CallbackListeners = {};
        end
        
        function delete(obj)
            
            if ~isempty(obj.CallbackListeners)
                allListeners = obj.CallbackListeners;
                for iLoop = 1: length(allListeners)                 
                    cellfun(@delete, allListeners{iLoop});
                end
                obj.CallbackListeners = {};
            end
            
            if ~isempty(obj.JavaProfileManager)
                obj.JavaProfileManager.destroy();
                obj.JavaProfileManager = {};
            end
            
            obj.ObjectsWithCallbackListeners = {};
        end        
    end
    
    methods (Static, Access = 'private')
        
        function instance = getInstance()
            % getInstance - Get the instance of the MATLAB profile manager
            
            % Need to mlock this so that the listeners are not cleared
            mlock
            persistent sInstance
            
            if isempty(sInstance)
                sInstance = parallel.internal.ui.MatlabProfileManager();
            end
            
            instance = sInstance;
        end
    end
    
    methods (Static)
        
        function [profileList, schedulerCompList, projectCompList, defaultProfile] = initialize(javaProfileManager)
            % INITIALIZE - Gets the profile, scheduler components and project
            % components information to initialize the UI.
            
            % Set the java profile manager
            import parallel.internal.ui.MatlabProfileManager
            matlabProfileManager = MatlabProfileManager.getInstance();
            matlabProfileManager.JavaProfileManager = javaProfileManager;
            
            pctSettings = parallel.Settings;
            
            allProfiles = pctSettings.Profiles;
            allSchedulerComponents = pctSettings.SchedulerComponents;
            allProjectComponents = pctSettings.ProjectComponents;
            
            profileList = iCreateJavaProfilesArray(allProfiles);
            schedulerCompList = iCreateJavaSchedulerComponentsArray(allSchedulerComponents);
            projectCompList = iCreateJavaProjectComponentsArray(allProjectComponents);
            defaultProfile = pctSettings.DefaultProfile;
            
            % Note add listeners afterwards because getting the
            % profiles,scheduler components or project components causes
            % the listeners to be triggered during initialization.
            profileAddedListener = iCreateListeners(pctSettings, 'ProfileAdded', @iHandleProfileAdded);
            scAddedlistener = iCreateListeners(pctSettings, 'SchedulerComponentAdded', @iHandleSchedulerComponentAdded);
            pcAddedlistener = iCreateListeners(pctSettings, 'ProjectComponentAdded', @iHandleProjectComponentAdded);
            defaultChangedlistener = iCreateListeners(pctSettings, 'DefaultProfileChanged', @iHandleDefaultProfileChanged);
            
            iCacheCallbackListener(pctSettings, {profileAddedListener; scAddedlistener; pcAddedlistener; defaultChangedlistener});
        end
        
        function [profileObj, schObj]= addProfile(schedulerComponentType, suggestedProfileName)
            % addProfile - Adds a profile with a new scheduler component
            
            pctSettings = parallel.Settings;
            profileNames = iGetNames('Profile');
            
            if nargin == 1
                % E.g. LocalProfile1
                profileBaseName = sprintf('%sProfile', schedulerComponentType);
                suggestedProfileName = sprintf('%s%d', profileBaseName, 1);
            else
                % E.g. MyClusterName, MyClusterName1
                profileBaseName = suggestedProfileName;
            end
            
            newProfileName = iGetUniqueName(profileNames, profileBaseName,...
                suggestedProfileName);
            
            schCompNames = iGetNames('SchedulerComponent');
            schCompSuffix = 'SchedulerComponent';
            % E.g. mjsSchedulerComponent
            newSchedulerComponentName = iGetUniqueName(schCompNames, sprintf('%s%s', newProfileName, schCompSuffix));
            
            schObj = pctSettings.createSchedulerComponent(schedulerComponentType, newSchedulerComponentName);
            profileObj = pctSettings.createProfile(newProfileName, newSchedulerComponentName);
        end
        
        function cloneProfile(profileNameToClone)
            % cloneProfile - Clones the profile with the scheduler component and project
            % component (if need)
            
            import parallel.settings.DefaultValue
            import parallel.internal.ui.MatlabProfileManager
            
            % Clone the profile
            profileToClone = iFindObject('Profile', profileNameToClone);
            
            % Clone the scheduler component
            [~, schCompName] = MatlabProfileManager.clone('SchedulerComponent', profileToClone.SchedulerComponent);
            
            
            % Clone the project component if it is not UseDefault
            if ~DefaultValue.isDefault(profileToClone.ProjectComponent)
                [~, prjCompName] = MatlabProfileManager.clone('ProjectComponent', profileToClone.ProjectComponent);
            end
            
            suffix = '_Copy';
            baseName = iGetBaseName(profileNameToClone, suffix);
            profileName = iGetUniqueName(iGetNames('Profile'),...
                sprintf('%s%s', baseName, suffix));
            pctSettings = parallel.Settings;
            
            additionalPVPairs = {};
            if ~DefaultValue.isDefault(profileToClone.ProjectComponent)
                additionalPVPairs{end+1} = 'ProjectComponent';
                additionalPVPairs{end+1} = prjCompName;
            end
            
            if ~DefaultValue.isDefault(profileToClone.Description)
                additionalPVPairs{end+1} = 'Description';
                additionalPVPairs{end+1} = profileToClone.Description;
            end
            
            pctSettings.createProfile(profileName, schCompName, additionalPVPairs{:});
        end
        
        function removeProfile(profileNameToRemove)
            % removeProfile -  Deletes a profile with it's scheduler component and
            % project component if it is not being shared
            
            import parallel.internal.ui.MatlabProfileManager
            import parallel.settings.DefaultValue
            
            profileToDelete = iFindObject('Profile', profileNameToRemove);
            
            projectComponentName = profileToDelete.ProjectComponent;
            schedulerComponentName = profileToDelete.SchedulerComponent;
            
            % Remove the profile first
            MatlabProfileManager.remove('Profile', profileNameToRemove);
            
            % Remove the components if they are not being used/shared
            
            pctSettings = parallel.Settings;
            listOfProjectComponentsUsed = {pctSettings.Profiles.ProjectComponent};
            listOfSchedulerComponentsUsed = {pctSettings.Profiles.SchedulerComponent};
            
            % Delete the project component if it is not UseDefault and not shared/used
            if ~DefaultValue.isDefault(projectComponentName) && ...
                    ~iIsComponentUsed(projectComponentName, listOfProjectComponentsUsed)
                MatlabProfileManager.remove('ProjectComponent', projectComponentName);
            end
            
            % Delete the scheduler component if it is not UseDefault and not localSchedulerComponent and not shared/used
            if ~DefaultValue.isDefault(schedulerComponentName) && ...
                    ~strcmp(schedulerComponentName, 'localSchedulerComponent') && ...
                    ~iIsComponentUsed(schedulerComponentName, listOfSchedulerComponentsUsed)
                MatlabProfileManager.remove('SchedulerComponent', schedulerComponentName);
            end
        end
        
        function renameProfile(oldName, newName)
            % renameProfile - Renames the profile
            
            import parallel.internal.ui.MatlabProfileManager
            MatlabProfileManager.rename('Profile', oldName, newName);
        end
        
        function mlObj = add(objectType, newName, schedulerComponentType)
            % ADD - Adds the profile, scheduler components or project
            % components
            assert('iAdd is currently not being used.');
            
            pctSettings = parallel.Settings;
            % Make function Add profile with sch component
            if strcmp(objectType, 'Profile')
                mlObj = pctSettings.createProfile(newName, 'localSchedulerComponent');
            elseif strcmp(objectType, 'SchedulerComponent')
                mlObj  = pctSettings.createSchedulerComponent(schedulerComponentType, newName);
            elseif strcmp(objectType, 'ProjectComponent')
                mlObj  = pctSettings.createProjectComponent(newName);
            end
        end
        
        function [mlObj, newName] = clone(objectType, objectNameToClone)
            % CLONE - Clones the profile, scheduler components or project
            % components
            
            objectToClone = iFindObject(objectType, objectNameToClone);
            suffix = '_Copy';
            baseName = iGetBaseName(objectNameToClone, suffix);
            newName = iGetUniqueName(iGetNames(objectType),...
                sprintf('%s%s', baseName, suffix));
            objectToClone.saveAs(newName);
            mlObj = iFindObject(objectType, newName);
        end
        
        function rename(objectType, oldName, newName)
            % rename - Renames the scheduler components or project
            % components or profiles
            
            objToRename = iFindObject(objectType, oldName);
            objToRename.Name = newName;
        end
        
        function remove(objectType, name)
            % REMOVE - removes the profile, scheduler components or project
            % components
            
            objToDelete = iFindObject(objectType, name);
            objToDelete.delete();
        end
        
        function importAll(fileName)
            % importAll - Imports all profiles, scheduler components and project
            % components
            
            [~, ~, ext] = fileparts(fileName);
            isSettingsFile = ~strcmpi(ext, '.mat') ;
            
            parallel.internal.settings.importProfileOrConfiguration(fileName, isSettingsFile);
        end
        
        function export(objectType, objectNameToExport, fileName)
            % EXPORT - Exports the profile, scheduler components or project
            % components
            
            objectToExport = iFindObject(objectType, objectNameToExport);
            objectToExport.export(fileName);
        end
        
        function exportAll(fileName)
            % exportAll - Exports all profiles, scheduler components and project
            % components
            
            pctSettings = parallel.Settings;
            objectToExport = pctSettings.Profiles;
            
            % Export all the profiles with it's scheduler components and project
            % components
            objectToExport.export(fileName);
        end
        
        function setValues(profileName, profileProperties, profileConstraints, profileValues,...
                schedulerCompName, schedulerCompProperties, schedulerCompConstraints, schedulerCompValues,...
                projectCompName, projectCompProperties, projectCompConstraints, projectCompValues)
            
            % Update the profile object
            if ~iscell(profileProperties)
                profileProperties = {profileProperties};
            end
            if ~iscell(profileValues)
                profileValues = {profileValues};
            end
            if ~iscell(profileConstraints)
                profileConstraints = {profileConstraints};
            end
            
            profile = iFindObject('Profile', profileName);
            oldState1 = profile.hGetRestorePoint();
            try
                cellfun(@(propName, constraintType, propValue)iSetValueFromJava(profile, propName, constraintType, propValue), profileProperties, profileConstraints, profileValues);
            catch Mexp
                profile.hRestore(oldState1);
                rethrow(Mexp)
            end
            
            % Update the scheduler component object
            schedulerComp = iFindObject('SchedulerComponent', schedulerCompName);
            oldState2 = schedulerComp.hGetRestorePoint();
            try
                cellfun(@(propName, constraintType, propValue)iSetValueFromJava(schedulerComp, propName, constraintType, propValue), schedulerCompProperties, schedulerCompConstraints, schedulerCompValues);
            catch Mexp
                profile.hRestore(oldState1); % Restore the profile as well
                schedulerComp.hRestore(oldState2);
                rethrow(Mexp)
            end
            
            if isempty(projectCompName) && isempty(projectCompProperties)
                return;
            end
            
            newProjectCompCreated = false;
            if isempty(projectCompName) && ~isempty(projectCompProperties)
                pctSettings = parallel.Settings;
                
                projectCompBaseName = sprintf('%s%s', profileName, 'ProjectComponent');
                newName = iGetUniqueName(iGetNames('ProjectComponent'), projectCompBaseName);
                
                projectComp = pctSettings.createProjectComponent(newName);
                newProjectCompCreated = true;
            else
                projectComp = iFindObject('ProjectComponent', projectCompName);
            end
                        
            oldState3 = projectComp.hGetRestorePoint();
            
            try
                cellfun(@(propName, constraintType, propValue)iSetValueFromJava(projectComp, propName, constraintType, propValue), projectCompProperties, projectCompConstraints, projectCompValues);
                
                if newProjectCompCreated
                    profile.ProjectComponent = projectComp.Name;
                end
            catch Mexp
                profile.hRestore(oldState1); % Restore the profile as well
                schedulerComp.hRestore(oldState2);  % Restore the scheduler component as well
                if newProjectCompCreated
                    projectComp.delete();
                else
                    projectComp.hRestore(oldState3);
                end
                rethrow(Mexp);
            end
        end
        
        function discoverClusters()
            controller = parallel.internal.ui.DiscoverClusterWizardController.getDefaultController();
            controller.startWizard();
        end
        
        function setDefaultProfile(newProfileName)
            parallel.defaultClusterProfile(newProfileName);
        end
    end
end

function flag = iIsComponentUsed(componentName, listOfUsedComponents)
% iIsComponentUsed - Determines if the components is still being used

import parallel.settings.DefaultValue

nonDefaultIndex = cellfun(@(eachName) ~DefaultValue.isDefault(eachName), listOfUsedComponents);
allUsedComponentNames = listOfUsedComponents(nonDefaultIndex);
flag = any(strcmp(componentName, allUsedComponentNames));
end

function names = iGetNames(objectType)
% iGetNames - Gets the Profiles, scheduler components or project
% components names

pctSettings = parallel.Settings;
if strcmp(objectType, 'Profile')
    names = pctSettings.hGetAllProfileNames();
elseif strcmp(objectType, 'SchedulerComponent')
    names = pctSettings.hGetAllSchedulerComponentNames();
elseif strcmp(objectType, 'ProjectComponent')
    names = pctSettings.hGetAllProjectComponentNames();
end
end

function obj = iFindObject(objectType, name)
% iFindObject - Find the parallel object with the name NAME

pctSettings = parallel.Settings;

if strcmp(objectType, 'Profile')
    obj = pctSettings.findProfile('Name',name);
elseif strcmp(objectType, 'SchedulerComponent')
    obj = pctSettings.findSchedulerComponent('Name',name);
elseif strcmp(objectType, 'ProjectComponent')
    obj = pctSettings.findProjectComponent('Name',name);
end

if isempty(obj)
    error(message('parallel:ui:ProfileManagerNonExistentType', objectType, name));
end
end

function baseName = iGetBaseName(name, suffix)
% iGetBaseName - Get the base name from the name

% Remove all trailing and leading white spaces
% Invalid name will be dealt with genvarname in iGetUniqueName
name = strtrim(name);

startSuffixIndex = strfind(name, suffix);
if isempty(startSuffixIndex)
    % Does not contain any suffix e.g. local
    baseName = name;
    return
end

% There may be multiple suffix inside the string e.g. local_Copy_Copy1
startSuffixIndex = startSuffixIndex(end);
lengthStartTillEndSuffix = startSuffixIndex- 1 + length(suffix);

if length(name) == lengthStartTillEndSuffix
    % Contains suffix at the end and contains no numbers at the end
    % e.g. local_Copy or local_Copy_Copy
    baseName = name(1:startSuffixIndex - 1);
    return
end

% Might have a name which contains a basename and a number
afterSuffixString = name(lengthStartTillEndSuffix+1 : end);

afterSuffixNum = str2double(afterSuffixString);
if ~isnan(afterSuffixNum)
    % Contains numbers after suffix e.g. local_Copy1 or local_Copy_Copy1
    baseName = name(1: startSuffixIndex-1);
    return
end

% Return basename e.g local_CopyABD, local_Copy_123
baseName = name;
end

function newName = iGetUniqueName(namesList, baseName, suggestedName)
% iGetUniqueName - gets an unique name with one indexing e.g Name1, Name2,

if nargin < 3
    % If a suggested name is not provided then use the baseName
    suggestedName = baseName;
end

counter = 1;
newName = suggestedName;
while any(strcmp(newName, namesList))
    counter = counter + 1;
    newName = sprintf('%s%d', baseName, counter);
end

% Check if it is a valid MATLAB variable name (length is too long)
if ~isvarname(newName)
    newName = genvarname(newName, namesList);
end

end

function javaProfiles = iCreateJavaProfilesArray(profileObjs)
% iCreateJavaProfilesArray - Creates the java list of profiles

javaProfiles = java.util.ArrayList();

if iscell(profileObjs)
    profileObjs = [profileObjs{:}];
end

for iLoop = 1: numel(profileObjs)
    javaProfiles.add(iCreateJavaProfile(profileObjs(iLoop)));
end

end

function javaSchedulerComponents = iCreateJavaSchedulerComponentsArray(schedulerComponentObjs)
% iCreateJavaSchedulerComponentsArray - Creates the java list of scheduler
% components

javaSchedulerComponents = java.util.ArrayList();

if iscell(schedulerComponentObjs)
    schedulerComponentObjs = [schedulerComponentObjs{:}];
end

for iLoop = 1: numel(schedulerComponentObjs)
    javaSchedulerComponents.add(iCreateJavaSchedulerComponent(schedulerComponentObjs(iLoop)));
end

end

function javaProjectComponents = iCreateJavaProjectComponentsArray(projectComponentObjs)
% iCreateJavaProjectComponentsArray - Creates the java list of project
% Components

javaProjectComponents = java.util.ArrayList();

if iscell(projectComponentObjs)
    projectComponentObjs = [projectComponentObjs{:}];
end

for iLoop = 1: numel(projectComponentObjs)
    javaProjectComponents.add(iCreateJavaProjectComponent(projectComponentObjs(iLoop)));
end

end

function javaProfile = iCreateJavaProfile(profileObj)
% iCreateJavaProfile - Creates the java profile

if isempty(profileObj)
    javaProfile = com.mathworks.toolbox.distcomp.ui.model.MatlabNullValue();
    return
end

profileName = profileObj.Name;

infoProvider = com.mathworks.toolbox.distcomp.ui.profile.model.PropertyInfoProvider.getInstance();
propertyInfoList = infoProvider.getProfilePropertyInfoList();

[javaPropertyList, propertyListeners] = iCreatePropertyList(profileObj, propertyInfoList);

javaProfile = com.mathworks.toolbox.distcomp.ui.profile.model.Profile(profileName, javaPropertyList);

objectDestroyedListener = iCreateListeners(profileObj, 'ObjectBeingDestroyed', @(~,~)iHandleObjectBeingDeleted(javaProfile));
nameChangedListener = iCreateListeners(profileObj, 'NameChanged', @(~, eventData)iHandleNameChanged(javaProfile, eventData));
additionalListeners = {objectDestroyedListener; nameChangedListener};

allListeners = [propertyListeners; additionalListeners];
iCacheCallbackListener(profileObj, allListeners);

end

function javaSchedulerComponent = iCreateJavaSchedulerComponent(schedulerComponentObj)
% iCreateJavaSchedulerComponent - Creates the java scheduler component

if isempty(schedulerComponentObj)
    javaSchedulerComponent = com.mathworks.toolbox.distcomp.ui.model.MatlabNullValue();
    return
end

schedulerComponentName = schedulerComponentObj.Name;
schedulerComponentType = schedulerComponentObj.Type;

infoProvider = com.mathworks.toolbox.distcomp.ui.profile.model.PropertyInfoProvider.getInstance();
propertyInfoList = infoProvider.getSchedulerPropertyInfoList(schedulerComponentType);

[javaPropertyList, propertyListeners] = iCreatePropertyList(schedulerComponentObj, propertyInfoList);

javaSchedulerComponent = com.mathworks.toolbox.distcomp.ui.profile.model.SchedulerComponent(schedulerComponentName, schedulerComponentType, javaPropertyList);

objectDestroyedListener = iCreateListeners(schedulerComponentObj, 'ObjectBeingDestroyed', @(~,~)iHandleObjectBeingDeleted(javaSchedulerComponent));
nameChangedListener = iCreateListeners(schedulerComponentObj, 'NameChanged', @(~, eventData)iHandleNameChanged(javaSchedulerComponent, eventData));
additionalListeners = {objectDestroyedListener; nameChangedListener};

allListeners = [propertyListeners; additionalListeners];

iCacheCallbackListener(schedulerComponentObj, allListeners);

end

function javaProjectComponent = iCreateJavaProjectComponent(projectComponentObj)
% iCreateJavaProjectComponent - Creates the java project component

if isempty(projectComponentObj)
    javaProjectComponent = com.mathworks.toolbox.distcomp.ui.model.MatlabNullValue();
    return
end

projectComponentName = projectComponentObj.Name;

infoProvider = com.mathworks.toolbox.distcomp.ui.profile.model.PropertyInfoProvider.getInstance();
propertyInfoList = infoProvider.getProjectPropertyInfoList();

[javaPropertyList, propertyListeners]= iCreatePropertyList(projectComponentObj, propertyInfoList);

javaProjectComponent = com.mathworks.toolbox.distcomp.ui.profile.model.ProjectComponent(projectComponentName, javaPropertyList);

objectDestroyedListener = iCreateListeners(projectComponentObj, 'ObjectBeingDestroyed', @(~,~)iHandleObjectBeingDeleted(javaProjectComponent));
nameChangedListener = iCreateListeners(projectComponentObj, 'NameChanged', @(~, eventData)iHandleNameChanged(javaProjectComponent, eventData));
additionalListeners = {objectDestroyedListener; nameChangedListener};

allListeners = [propertyListeners; additionalListeners];

iCacheCallbackListener(projectComponentObj, allListeners);
end

function iHandleProfileAdded(~,evtData)
% IHANDLEPROFILEADDED - Handle when a profile is added

import parallel.settings.DefaultValue
import parallel.internal.ui.MatlabProfileManager

profileManager = MatlabProfileManager.getInstance();
javaProfileManager = profileManager.JavaProfileManager;

profileObj = evtData.NamedNode;

javaObject = iCreateJavaProfile(profileObj);
javaMethodEDT('addObject', javaProfileManager, javaObject);

end

function iHandleSchedulerComponentAdded(~,evtData)
% iHandleSchedulerComponentAdded - Handle when a scheduler component is added

import parallel.settings.DefaultValue
import parallel.internal.ui.MatlabProfileManager

profileManager = MatlabProfileManager.getInstance();
javaProfileManager = profileManager.JavaProfileManager;

schedulerComponentObj = evtData.NamedNode;

javaObject = iCreateJavaSchedulerComponent(schedulerComponentObj);
javaMethodEDT('addObject', javaProfileManager, javaObject);

end

function iHandleProjectComponentAdded(~,evtData)
% iHandleProjectComponentAdded - Handle when a project component is added

import parallel.settings.DefaultValue
import parallel.internal.ui.MatlabProfileManager

profileManager = MatlabProfileManager.getInstance();
javaProfileManager = profileManager.JavaProfileManager;

projectComponentObj = evtData.NamedNode;

javaObject = iCreateJavaProjectComponent(projectComponentObj);
javaMethodEDT('addObject', javaProfileManager, javaObject);

end

function [javaPropertyList, allListeners] = iCreatePropertyList(mlObj, propertyInfoList)
% iCreatePropertyList - creates the java property list

javaPropertyList = java.util.ArrayList();
numProperties = propertyInfoList.size();

allListeners = cell(numProperties, 1);
for iLoop = 1 : numProperties
    eachPropertyInfo = propertyInfoList.get(iLoop-1);
    [property, listeners]= iCreateProperty(mlObj, eachPropertyInfo);
    allListeners{iLoop} = listeners;
    javaPropertyList.add(property);
end

end

function [property, listener] = iCreateProperty(mlObject, propertyInfo)
% iCreateProperty - Create the java property

propertyName = char(propertyInfo.getName());

constraintType = char(propertyInfo.getConstraint().getConstraintType().toString());
javaData =  iGetValueForJava(mlObject, propertyName, constraintType);
propertyStorage = com.mathworks.toolbox.distcomp.ui.model.ValueBackedPropertyStorage(javaData);
property = com.mathworks.toolbox.distcomp.ui.model.Property(propertyStorage, propertyInfo);

listener = iCreateListeners(mlObject, 'SettingsNodeChanged', @(~,~)iHandleSettingsNodeChanged(mlObject, propertyName, constraintType, propertyStorage));
end

function iHandleSettingsNodeChanged(mlObject, propertyName, constraintType, propertyStorage)
% iHandleSettingsNodeChanged - Handle value changed from java

% Workaround for PostSet - update the value in the property storage
javaData = iGetValueForJava(mlObject, propertyName, constraintType);
propertyStorage.set(javaData);
end

function iHandleObjectBeingDeleted(javaObject)

% clean up callback listeners
import parallel.internal.ui.MatlabProfileManager
profileManager = MatlabProfileManager.getInstance();
invalidIndex = ~cellfun(@isvalid, profileManager.ObjectsWithCallbackListeners);

profileManager.ObjectsWithCallbackListeners(invalidIndex) = [];
listeners = profileManager.CallbackListeners{invalidIndex};

% Delete each listener
cellfun(@delete, listeners);
profileManager.CallbackListeners(invalidIndex) = [];

% Delete the object from the profile manager
javaMethodEDT('deleteObject', profileManager.JavaProfileManager, javaObject);
end

function iHandleNameChanged(javaObject, namedChangedEventData)

import parallel.internal.ui.MatlabProfileManager
profileManager = MatlabProfileManager.getInstance();

newName = namedChangedEventData.NewName;

% Rename the object in the profile manager
javaMethodEDT('renameObject', profileManager.JavaProfileManager, javaObject, newName);

end

function iHandleDefaultProfileChanged(~,~)
% iHandleDefaultProfileChanged - Handles when the default profile has
% changed

pctSettings = parallel.Settings;
defaultProfileName = pctSettings.DefaultProfile;

import parallel.internal.ui.MatlabProfileManager
profileManager = MatlabProfileManager.getInstance();
javaMethodEDT('handleDefaultProfileChanged', profileManager.JavaProfileManager, defaultProfileName);
end

function listener = iCreateListeners(mlObj, eventName, callback)
% ICREATELISTENERS - creates callback listeners

    listener = mlObj.addlistener(eventName, callback);
end

function iCacheCallbackListener(mlObj, callbackListeners)
% ICACHECALLBACKLISTENER - caches the callback listeners

import parallel.internal.ui.MatlabProfileManager
profileManager = MatlabProfileManager.getInstance();

profileManager.ObjectsWithCallbackListeners{end+1} = mlObj;

% Note: Appending the new callback listeners directly to the object 
% has performance issues 
allCallbackListeners = profileManager.CallbackListeners;
allCallbackListeners{end+1} = callbackListeners;
profileManager.CallbackListeners = allCallbackListeners;
end

function javaData = iGetValueForJava(mlObj, property, constraintType)
% iGetValueForJava - Gets the value for java

value = mlObj.(property);
javaData = iMassageDataForJava(value, constraintType);
end

function iSetValueFromJava(mlObj, property, constraintType, javaData)
% iSetValueFromJava - sets the value on the MATLAB object from java

mlData = iMassageDataForML(javaData, constraintType);
if isa(mlObj, 'parallel.internal.settings.CustomSettingsGetSet') && ...
        isa(mlData, 'parallel.settings.DefaultValue')
    mlObj.unset(property);
else
    mlObj.(property)  = mlData;
end
end

function javaData = iMassageDataForJava(mlData, constraintType)
% iMassageDataForJava - Converts the MATLAB data into a java friendly data

if isa(mlData, 'parallel.settings.DefaultValue')
    javaData = com.mathworks.toolbox.distcomp.ui.model.DefaultValue();
    return;
end

switch (lower(constraintType))
    case {'string', 'nonemptystring'}
        % Resolving the issue: single string is converted to java Character
        javaData = java.lang.String(mlData);
    case 'cellstr'
        % Handle a array of string (AttachedFiles and AdditionalPaths)
        javaData = mlData;
    case 'logicalscalar'
        % Resolving the issue: A logical value is converts into an boolean array in Java.
        javaData = java.lang.Boolean(mlData);
    case 'callback'
        % Convert function handle to string
        % Convert any function handles in a cell into string for callbacks
        % Resolving the issue: single string is converted to java Character
        javaData = java.lang.String(distcomp.typechecker.callback2string(mlData));
    case 'positiveintscalar'
        % Resolving the issue: A double value is converts into an double array in Java.
        javaData = java.lang.Double(mlData);
    case 'workerlimits'
        if isscalar(mlData)
            % Resolving the issue: A double value is converts into an double array in Java.
            javaData = java.lang.Double(mlData);
        else
            % Convert array of double to Double java array
            javaData = javaArray('java.lang.Double', numel(mlData));
            for iLoop = 1: numel(mlData)
                javaData(iLoop) = java.lang.Double(mlData(iLoop));
            end
        end
    case 'enum'
        javaData = java.lang.String(mlData);
    case 'datalocation'
        if isstruct(mlData)
            javaData = java.lang.String(sprintf('struct(''windows'', ''%s'', ''unix'', ''%s'')', mlData.windows, mlData.unix));
        else
            javaData = java.lang.String(mlData);
        end
    otherwise
        javaData = mlData;
end
end

function mlData = iMassageDataForML(javaData, constraintType)
% iMassageDataForML - Converts the java data into a MATLAB friendly data

if isa(javaData, 'com.mathworks.toolbox.distcomp.ui.model.DefaultValue')
    mlData = parallel.settings.DefaultValue;
    return;
end

switch (lower(constraintType))
    case {'string', 'nonemptystring'}
        mlData = javaData;
    case 'cellstr'
        mlData = cell(1, length(javaData));
        for iLoop = 1: length(javaData)
            mlData{iLoop}  = char(javaData(iLoop));
        end
    case 'logicalscalar'
        mlData = javaData;
    case 'callback'
        mlData = distcomp.typechecker.string2callback(javaData);
    case 'positiveintscalar'
        mlData = javaData;
    case 'workerlimits'
        if iscell(javaData) && all(cellfun(@isnumeric, javaData))
            % Java Double array is passed back as cell array of numeric to
            % MATLAB
            mlData = [javaData{:}];
        else
            mlData = javaData;
        end
    case 'enum'
        mlData = javaData;
    case 'datalocation'
        if ~isempty(regexp(javaData, '^\s*struct\s*(.*)', 'once' ))
            mlData = eval(javaData);
        else
            mlData = javaData;
        end
    otherwise
        mlData = javaData;
end
end
