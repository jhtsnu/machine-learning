function description = getJobDescription(jobs)
% GETJOBDESCRIPTION - gets the description of the job for the job monitor
% ui

%   Copyright 2012 The MathWorks, Inc.

import parallel.internal.apishared.BatchJobMethods
import parallel.internal.types.Variant

% Note: get Type as cell array
jobTypes = {jobs.Type};

description = cell(numel(jobs),1);

for iLoop = 1:numel(jobs)
    
    job = jobs(iLoop);
    jobType = jobTypes{iLoop};
    
    % Check if the Tag and the hidden ApiTag don't match
    if ~strcmp(job.Tag, job.ApiTag) && ~isempty(job.Tag)
        % Use the tag as the description
        description{iLoop} = job.Tag;
        continue;
    end
    
    % Batch jobs are a type of IndependentJob and also check for a MATLAB
    % pool batch job (CommunicatingPoolJob)
    if BatchJobMethods.isBatchJob(job)
        description{iLoop} = BatchJobMethods.describeBatchJob(job);
    elseif isequal(jobType, Variant.IndependentJob.Name)
        description{iLoop} = getString(message('parallel:ui:JobMonitorIndependentJob'));
    elseif isequal(jobType, Variant.CommunicatingSPMDJob.Name)
        description{iLoop} = getString(message('parallel:ui:JobMonitorCommunicatingJob'));
    elseif isequal(jobType, Variant.CommunicatingPoolJob.Name)
        description{iLoop} = getString(message('parallel:ui:JobMonitorPoolJob'));
        % MATLAB pool is a type of CommunicatingPoolJob
        if isequal(job.ApiTag, 'Created_by_matlabpool')
            description{iLoop} = getString(message('parallel:ui:JobMonitorMatlabPool'));
        end
    else
        error(message('parallel:ui:JobMonitorUnsupportedJobType', jobType));
    end
end
end