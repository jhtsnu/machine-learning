function schedOrCluster = getCluster(profileName)
% getCluster - Gets a cluster from a profile

%   Copyright 2011-2012 The MathWorks, Inc.

if parallel.internal.ui.shouldUseAPI2()
    schedOrCluster = parcluster(profileName);
else
    schedOrCluster = findResource('scheduler', 'configuration', profileName); %#ok<REMFF1>
    if isempty(schedOrCluster)
        error(message('parallel:cluster:JobMonitorHelperNoCluster', profileName));
    end

    if numel(schedOrCluster) > 1
        error(message('parallel:cluster:JobMonitorHelperMultipleClusters', profileName));
    end
end
