
%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) ValidationManager
    % ValidationManager - Object for the validation manager. Allows the UI to
    % start or stop the validation. Also redirects the validation events to
    % java.
    
    properties (GetAccess = 'private', SetAccess='immutable')
        % Validator MATLAB object
        Validator
        
        % The java validation manager object that UI is listening to
        JavaValidationManager
    end
    
    methods
        
        function obj = ValidationManager(validationManager)
            % ValidationManager - Validation Manager for the UI
            
            validateattributes(validationManager, ...
                {'com.mathworks.toolbox.distcomp.ui.profile.model.ValidationManager'}, {'scalar'});
            
            validator = parallel.settings.Profile.hGetRegisteredValidator();
            
            obj.Validator = validator;
            obj.JavaValidationManager = validationManager;
            
            % Attach listeners to the validator object
            obj.Validator.addlistener('ValidationStarted', ...
                @(~, evnt) obj.handleValidationStarted(evnt));
            obj.Validator.addlistener('ValidationFinished', ...
                @(~, evnt) obj.handleValidationFinished(evnt));
            obj.Validator.addlistener('ValidationStageUpdated', ...
                @(~, evnt) obj.handleValidationStageUpdated(evnt));
            
            % Set the user cancelled function
            obj.Validator.setUserCancelledFunction(@obj.hasUserCancelled);
        end
        
        function validate(obj, profileName, loginDialogConsumer)
            % VALIDATE - Start the validation of the profile
            restoreConsumerObj = iSetDesktopLoginConsumerForValidation(loginDialogConsumer); %#ok<NASGU> - keep hold of cleanup object
            obj.Validator.validate(profileName);
        end
        
        function flag = hasUserCancelled(obj)
            % hasUserCancelled - determine if the user has cancelled the
            % validation
            flag = obj.JavaValidationManager.hasUserCancelledValidation();
        end
        
        function handleValidationStarted(obj, eventData)
            % handleValidationStarted - fires the validation has started
            validateattributes(eventData, ...
                {'parallel.internal.settings.ValidationEventData'}, {'scalar'});
            
            % Note: Reset the user cancelled validation flag (in case the
            % user tries to validate from the command line)
            obj.JavaValidationManager.resetUserCancelledValidation();
            
            profileName = eventData.Profile;
            
            javaEvent = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationEventData.createValidationStartedEvent(profileName);
            obj.JavaValidationManager.fireValidationStarted(javaEvent);
        end
        
        function handleValidationFinished(obj, eventData)
            % handleValidationFinished - fires the validation has finished
            validateattributes(eventData, ...
                {'parallel.internal.settings.ValidationEventData'}, {'scalar'});
            
            % Note: Reset the user cancelled validation flag (in case the
            % user tries to validate from the command line)
            obj.JavaValidationManager.resetUserCancelledValidation();
            
            profileName = eventData.Profile;
            
            status = iGetJavaStatus(eventData.StatusEnum);
            
            javaEvent = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationEventData.createValidationFinishedEvent(profileName, status);
            obj.JavaValidationManager.fireValidationFinished(javaEvent);
        end
        
        function handleValidationStageUpdated(obj, eventData)
            % handleValidationStageUpdated - fires the validation stage has
            % updated
            
            validateattributes(eventData, ...
                {'parallel.internal.settings.ValidationEventData'}, {'scalar'});
            
            profileName = eventData.Profile;
            
            stage =  iGetJavaStage(eventData.Stage);
            status = iGetJavaStatus(eventData.StatusEnum);
            
            description = eventData.Description;
            commandWindowOutput = eventData.CommandWindowOutput;
            errorReport = eventData.ErrorReportNoLinks;
            debugLog = eventData.DebugLog;
            
            javaEvent = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationEventData.createValidationStageEvent(profileName, stage, status, ...
                description, commandWindowOutput, errorReport, debugLog);
            obj.JavaValidationManager.fireValidationStageUpdated(javaEvent);
        end
    end    
end

function javaStatus = iGetJavaStatus(status)
% iGetJavaStatus - get the java version of the validation status

switch status
    case parallel.internal.types.ValidationStatus.NotRun
        javaStatus = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStatus.NOT_RUN;
    case parallel.internal.types.ValidationStatus.Running
        javaStatus = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStatus.RUNNING;
    case parallel.internal.types.ValidationStatus.Passed
        javaStatus = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStatus.PASSED;
    case parallel.internal.types.ValidationStatus.Failed
        javaStatus = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStatus.FAILED;
    case parallel.internal.types.ValidationStatus.Skipped
        javaStatus = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStatus.SKIPPED;
    case parallel.internal.types.ValidationStatus.Stopped
        javaStatus = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStatus.STOPPED;
    otherwise
        error(message('parallel:ui:ValidationUnknownStatus'));
end

end

function javaStage = iGetJavaStage(stage)
% iGetJavaStage - get the java version of the validation stage

switch lower(stage)
    case 'parcluster'
        javaStage = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStage.PARCLUSTER;
    case 'independent'
        javaStage = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStage.INDEPENDENT_JOB;
    case 'spmd'
        javaStage = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStage.COMMUNICATING_SPMD_JOB;
    case 'pool'
        javaStage = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStage.COMMUNICATING_POOL_JOB;
    case 'matlabpool'
        javaStage = com.mathworks.toolbox.distcomp.ui.profile.model.ValidationStage.MATLABPOOL;
    otherwise
        error(message('parallel:ui:ValidationUnknownStage', stage));
end

end

function cleanupObj = iSetDesktopLoginConsumerForValidation(loginConsumer)
% Set the login dialog consumer to the validation manager, so that the
% login dialog is modal to that, rather than the MATLAB desktop (g779939)
client = parallel.internal.webclients.currentDesktopClient();

% Get the current login dialog consumer, so we can reset it on cleanup.
originalLoginDialogConsumer = client.LoginDialogConsumer;
client.LoginDialogConsumer = loginConsumer;
cleanupObj = onCleanup(@() nClearDialogConsumer);
    function nClearDialogConsumer()
        client.LoginDialogConsumer = originalLoginDialogConsumer;
    end
end
