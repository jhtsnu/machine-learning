function [defaultProfile, displayCmdHistory, job, jobVarName] = runAsBatchJob(filename)
% RUNASBATCHJOB - creates the batch job and displays the show more info hyperlink

%  Copyright 2011-2012 The MathWorks, Inc.

% Validate inputs
validateattributes(filename, {'char'}, {'nonempty'}, 'runAsBatchJob', 'filename', 1);

% Get the filename from the filename
[~, filename] = fileparts(filename);

if parallel.internal.ui.shouldUseAPI2()
    defaultProfile = parallel.defaultClusterProfile();
else
    defaultProfile = pctDefaultParallelConfig;
end
evalCmd = sprintf('batch(''%s'', ''Profile'', ''%s'', ''Matlabpool'', %d);', filename, defaultProfile, 0);

% Note: Need to evalin in base so that it picks up the base variables
job = evalin('base',evalCmd);

% Assign the job to the base
who_output = evalin('base','who');
jobVarName = parallel.internal.ui.getUniqueVariableName(sprintf('job%d',job.ID), who_output);
assignin('base',jobVarName,job);

displayCmdHistory = sprintf('%s = %s', jobVarName, evalCmd);

% Display the command that was run
fprintf('>> %s\n', displayCmdHistory);

% Display the more information hyperlink
fprintf('\n');
fprintf(['   <a href ="matlab: parallel.internal.ui.showMoreInfo(''%s'')">', ...
        'For more information on working with this job</a>\n\n'], ...
        jobVarName);

