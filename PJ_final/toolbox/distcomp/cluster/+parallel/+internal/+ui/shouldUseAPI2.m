function tf = shouldUseAPI2() %(profileName)
% Helper function for UI-related to code to determine if we should
% API1 or API2.

%   Copyright 2011 The MathWorks, Inc.

[~, undoc] = pctconfig();
tf = undoc.useprofiles;
end
