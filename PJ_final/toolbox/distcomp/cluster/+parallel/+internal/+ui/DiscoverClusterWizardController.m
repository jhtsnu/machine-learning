classdef DiscoverClusterWizardController
    %DISCOVERCLUSTERCONTROLLER - Controller for the Discover Cluster Wizard
    
    %   Copyright 2012 The MathWorks, Inc.
    
    properties (GetAccess = 'private', SetAccess='immutable')
        
        % DiscoverClusterWizardModel
        DiscoverClusterWizardModel
        
        % UIs are listening to
        DiscoverClusterWizard
        
        % Listeners
        EventListeners
    end
    
    methods
        
        function obj = DiscoverClusterWizardController()
            % DISCOVERCLUSTERWIZARDCONTROLLER - Controller for the Discover Cluster Wizard
            
            dt = javaMethod('getInstance', 'com.mathworks.mde.desk.MLDesktop');
            
            obj.DiscoverClusterWizardModel = com.mathworks.toolbox.distcomp.ui.discover.model.DiscoverClusterWizardModel();
            obj.DiscoverClusterWizard = com.mathworks.toolbox.distcomp.ui.discover.DiscoverClusterWizard(dt.getMainFrame, obj.DiscoverClusterWizardModel);
            
            % Add listeners to the callbacks
            obj.EventListeners = cell(3,1);
            
            callback = handle(obj.DiscoverClusterWizard.getCreateProfileCallback);
            obj.EventListeners{1} = handle.listener(callback, 'delayed', @obj.handleCreateProfileCallback);
            
            callback = handle(obj.DiscoverClusterWizard.getStartDiscoveryCallback);
            obj.EventListeners{2} = handle.listener(callback, 'delayed', @obj.handleStartDiscoveryCallback);
            
            callback = handle(obj.DiscoverClusterWizard.getSetDefaultProfileCallback);
            obj.EventListeners{3} = handle.listener(callback, 'delayed', @obj.handleSetDefaultProfileCallback);
        end
        
        function startWizard(obj)
            % startWizard - start the wizard
            
            import parallel.internal.ui.DiscoverClusterWizardController
            
            % reset the model
            javaMethodEDT('reset', obj.DiscoverClusterWizardModel);
            
            % Return to the first page
            javaMethodEDT('firstPage', obj.DiscoverClusterWizard);
            
            % Set the dialog in the center of the screen
            javaMethodEDT('setLocationRelativeTo', obj.DiscoverClusterWizard, []);
            
            % Show the dialog
            javaMethodEDT('setModal',obj.DiscoverClusterWizard, DiscoverClusterWizardController.wizardModality());            
            
            % Show the dialog
            javaMethodEDT('setVisible',obj.DiscoverClusterWizard, true);
        end
        
    end
    
    methods (Hidden, Static)
        
        function controller = getDefaultController()
            % getDefaultController - Get the default/stored controller
            
            persistent sDefaultController
            if isempty(sDefaultController)
                % Create one if it does not exist
                sDefaultController = parallel.internal.ui.DiscoverClusterWizardController();
            end
            controller = sDefaultController;
        end
        
        function modal = wizardModality(value)
            % wizardModality - Get/Set the discover clusters wizard's
            % modality
            
            persistent sModality
            if (isempty(sModality))
                % default value
                sModality = true;
            end            
            
            if nargin>0
                modal = sModality; % Return the old modality value
               sModality = value; % Update the old modality value
            else
              modal = sModality; % Return the current modality value
            end
            
            
        end        
    end
    
    methods (Access = 'private')
        
        function handleStartDiscoveryCallback(obj, ~, ~)
            % handleStartDiscoveryCallback - Handles the start discovery
            % callback
            
            % Turn off warning in the MATLAB Command window
            oldWarnState = warning('off'); %#ok<WNOFF>
            
            % Add onCleanup to deal with user aborting the discovery using
            % CTRL+C.
            cleanupObj = onCleanup(@() iCleanupStartDiscoveryCallback(obj.DiscoverClusterWizardModel, oldWarnState));
            
            discovererManager = parallel.internal.discover.currentDiscoveryManager();
            
            % Attach listeners to the discoverer object
            listener = event.listener(discovererManager, 'ClusterFound', ...
                @obj.handleClusterFound); %#ok<NASGU>
            
            userCancelledCallback = @() obj.DiscoverClusterWizardModel.hasUserCancelled;
            
            try
                
                % Discover on network
                if obj.DiscoverClusterWizardModel.isDiscoverNetwork()
                    discovererManager.discoverNetwork(userCancelledCallback);
                end
                
                % Discover on cloud
                if obj.DiscoverClusterWizardModel.isDiscoverCloud()
                    cleanupObjLoginConsumer = iSetDesktopLoginConsumerForDiscovery(obj.DiscoverClusterWizard); %#ok<NASGU>
                    discovererManager.discoverCloud(userCancelledCallback);
                end
                
            catch Mexp
                
                errorTitle = getString(message('parallel:ui:DiscoverClusterErrorTitle'));
                errorLabel = getString(message('parallel:ui:DiscoverClusterErrorLabel'));
                
                % Create error dialog
                import com.mathworks.toolbox.distcomp.ui.DetailsDialog
                import javax.swing.JOptionPane
                errDialog = DetailsDialog(obj.DiscoverClusterWizard,...
                    errorTitle,...
                    errorLabel,...
                    JOptionPane.ERROR_MESSAGE,...
                    Mexp.message);
                errDialog.setLocationRelativeTo(obj.DiscoverClusterWizard);
                
                % Show the dialog
                javaMethodEDT('setVisible',errDialog, true);
            end
        end
        
        function handleClusterFound(obj, ~, eventData)
            % handleClusterFound - Handle cluster found events
            
            clusterPropertyNames = eventData.ClusterPropNames;
            clusterPropertyValues = eventData.ClusterPropValues;
            
            clusterName = iFindPropertyValue('Name', clusterPropertyNames, clusterPropertyValues);
            numWorkers = iFindPropertyValue('NumWorkers', clusterPropertyNames, clusterPropertyValues);
            securityLevel = iFindPropertyValue('SecurityLevel', clusterPropertyNames, clusterPropertyValues);
            
            % Some scheduler type do not have a security level (e.g. HPCServer)
            if isempty(securityLevel)
                isSecure = false;
            else
                isSecure  = securityLevel > 0;
            end
            
            % Get the host name
            switch eventData.Type
                case {'MJS', 'HPCServer'}
                    hostName = iFindPropertyValue('Host', clusterPropertyNames, clusterPropertyValues);
                case 'MJSComputeCloud'
                    % Note: We don't want to show the actual hostname in
                    % the cloud
                    hostName = getString(message('parallel:ui:DiscoverClusterInCloud'));
                otherwise
                    error(message('parallel:ui:DiscoverClusterUnknownClusterType', eventData.Type));
            end
            
            correspondingProfiles = eventData.CorrespondingProfiles;
            schedulerPropertyNames = eventData.SchedulerComponentPropNames;
            schedulerPropertyValues = eventData.SchedulerComponentPropValues;
            
            discoverClusterData = com.mathworks.toolbox.distcomp.ui.discover.DiscoverClusterData(eventData.Type,...
                clusterName, hostName, numWorkers, correspondingProfiles, isSecure, schedulerPropertyNames, schedulerPropertyValues);
            javaMethodEDT('addCluster', obj.DiscoverClusterWizardModel, discoverClusterData);
        end
            
        function handleCreateProfileCallback(obj, ~, ~)
            % handleCreateProfileCallback - Handle create profile callback
            
            selectedCluster = obj.DiscoverClusterWizardModel.getSelectedCluster();
            
            schedulerComponentType = char(selectedCluster.getType());
            schedulerPropertyNames = cell(selectedCluster.getSchedulerPropertyNames());
            schedulerPropertyValues = cell(selectedCluster.getSchedulerPropertyValues());
            
            % Use the cluster name as a default name for MJSComputeCloud
            % else default naming scheme (e.g. MJSProfile1)
            args = {};
            if strcmp(schedulerComponentType, 'MJSComputeCloud')
                args = {char(selectedCluster.getClusterName())};               
            end
            
            % Create the profile
            [profileObj, schObj] = parallel.internal.ui.MatlabProfileManager.addProfile(schedulerComponentType, args{:});
            
            profileObj.Description = getString(message('parallel:ui:DiscoverClusterDescription'));
            schObj.hSetFromCluster(schedulerPropertyNames, schedulerPropertyValues);
            
            % Update the profile was created
            javaMethodEDT('setCreatedProfileName', obj.DiscoverClusterWizardModel, profileObj.Name);
        end
        
        function handleSetDefaultProfileCallback(obj, ~, ~)
            % handleSetDefaultProfileCallback - handle set the profile as default callback
            
            newProfileName = char(obj.DiscoverClusterWizardModel.getCreatedProfileName());
            setDefault = obj.DiscoverClusterWizardModel.isCreatedProfileAsDefault();
            
            if setDefault
                pctSettings = parallel.Settings;
                pctSettings.DefaultProfile = newProfileName;
            end
        end
        
    end
end

function propertyValue = iFindPropertyValue(propertyName, propertyNameList, propertyValueList)
% iFindPropertyValue - Helper function to find the property value from a list

propertyNameIndex = strcmp(propertyName,propertyNameList);

if any(propertyNameIndex)
    propertyValue = propertyValueList{propertyNameIndex};
else
    propertyValue = [];
end

end

function iCleanupStartDiscoveryCallback(discoverWizardModel, oldWarnState)
% iCleanupStartDiscoveryCallback -  Clean up for Start Discovery Callback

% Fire discover done
javaMethodEDT('discoveryDone', discoverWizardModel)

% Restore warning state
warning(oldWarnState)
end

function cleanupObj = iSetDesktopLoginConsumerForDiscovery(parent)

loginConsumer = com.mathworks.toolbox.distcomp.mwlogin.LoginParentProvider(parent);
% Set the login dialog consumer to the Discover Clusters Wizard, so that the
% login dialog is modal to that, rather than the MATLAB desktop (g779939)
client = parallel.internal.webclients.currentDesktopClient;
oldConsumer  = client.LoginDialogConsumer;
client.LoginDialogConsumer = loginConsumer;
cleanupObj = onCleanup(@() restoreDialogConsumer);
    function restoreDialogConsumer()
        client.LoginDialogConsumer = oldConsumer ;
    end
end

