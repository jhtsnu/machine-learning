function varargout = jobMonitorHelper(option, varargin)
% JOBMONITORHELPER - utility helper file for the Job Monitor UI

%  Copyright 2011-2012 The MathWorks, Inc.

validateattributes(option, {'char'}, {'nonempty'}, 'jobMonitorHelper', 'option', 1);

switch (option)
    case 'open'
        % Launches the Job Monitor UI
        % Note: Need to call on the EDT Thread or the model fires before
        % the UI is ready
        jmClient = javaMethodEDT('getInstance','com.mathworks.toolbox.distcomp.ui.desk.JobMonitorDesktopClient');
        javaMethodEDT('showUI',jmClient);
    case 'update'
        % Callback for the update
        profileName = varargin{1};
        errorMsg = '';
        errorID = '';
        
        try
            % Get the list of JobTableRows
            [allUsersJobTableRows, individualUserJobTableRows] = ...
                parallel.internal.ui.getJobTableRows(profileName);
        catch Mexp
            errorMsg = Mexp.message;
            errorID = Mexp.identifier;
            allUsersJobTableRows = java.util.Collections.emptyList;
            individualUserJobTableRows = java.util.Collections.emptyList;
        end

        varargout{1} = allUsersJobTableRows;
        varargout{2} = individualUserJobTableRows;
        varargout{3} = errorMsg;
        varargout{4} = errorID;
    case 'cancelJob'
        % Callback for the canceling a job
        profileName = varargin{1};
        jobID = varargin{2};
        job = iFindJob(profileName, jobID);
        cancel(job)
    case 'deleteJob'
        % Callback for the destroying a job
        profileName = varargin{1};
        jobID = varargin{2};
        job = iFindJob(profileName, jobID);
        iDeleteJob(job);
    case 'showErroredTasksInJob'
        % Callback for showing the basic error report of the errored task
        profileName = varargin{1};
        jobID = varargin{2};
        erroredTaskIDs = varargin{3};
        job = iFindJob(profileName, jobID);
        
        iDisplayTaskBasicErrorMessage(job, erroredTaskIDs);
        commandwindow; % Brings command window to the front
    case 'showJobDetails'
        % Callback for the assigning a job to the base workspace
        profileName = varargin{1};
        jobID = varargin{2};
        template = '%s';
        iFindJobAndRunCode(profileName, jobID, template);
    case 'fetchOutputs'
        % Callback for the fetchOutputs a job to the base workspace
        profileName = varargin{1};
        jobID = varargin{2};

        jobVarName = iFindJobInWorkspace(profileName, jobID);
        outputVarName = sprintf('%s_output', jobVarName);
        
        template = [outputVarName ' = fetchOutputs(%s)'];
        iFindJobAndRunCode(profileName, jobID, template);
    case 'loadVariables'
        % Callback for the load variables on a job to the base workspace
        profileName = varargin{1};
        jobID = varargin{2};
        template = 'load(%s)';
        iFindJobAndRunCode(profileName, jobID, template);
    case 'showDiary'
        % Callback for the show diary of a job
        profileName = varargin{1};
        jobID = varargin{2};
        template = 'diary(%s)';
        iFindJobAndRunCode(profileName, jobID, template);
    case 'closeMATLABPool'
        % Callback for the fetchOutputs a job to the base workspace
        code = sprintf('matlabpool(''close'')');
        iRunCodeInCommandWindow(code);
    otherwise
        error(message('parallel:cluster:JobMonitorHelperUnknownOption', option));
end

end

function job = iFindJob(profileName, jobID)
% iFindJob - finds the job using the profile name and job ID.

% Get the cluster
clus = parallel.internal.ui.getCluster(profileName);

allJobs = clus.Jobs;
allJobIDs = [allJobs.ID];
[intersectJobIDs, idx] = intersect(allJobIDs, jobID);

nonExistantJobIDs = setxor(intersectJobIDs, jobID);
if ~isempty(nonExistantJobIDs)
    jodIDStr = sprintf('%d, ', nonExistantJobIDs);
    jodIDStr = jodIDStr(1:end-2);
    error(message('parallel:ui:JobMonitorNonExistentJobs',jodIDStr));
end
job = allJobs(idx);

end

function iDisplayCodeFindJob(profileName, jobVarName, jobID)
% Displays the MATLAB code used to the find the job

findClusterCode = iGetFindClusterCode(profileName);
iDisplayCode(findClusterCode);
findJobCode = sprintf('%s = myCluster.findJob(''ID'',%d);', jobVarName, jobID);
iDisplayCode(findJobCode);
end

function iDisplayCode(code)
% Displays the code and adds to the cmd history

fprintf('>> %s\n', code);
% Add MATLAB code to the history window
com.mathworks.mde.cmdhist.CmdHistory.getInstance().add(java.lang.String(code));
end

function iDisplayTaskBasicErrorMessage(job, taskIDs)
% Displays the basic error report of the errored task

for iLoop = 1:numel(taskIDs)
    task = job.Tasks(taskIDs(iLoop)); %#ok<NASGU>
    displayStr = evalc('display(task)');
    displayStr = strrep(displayStr,'task =','');
    fprintf('%s',displayStr)
end

end

function iDeleteJob(job)
% Deletes the job

if parallel.internal.ui.shouldUseAPI2()
    job.delete();
else
    job.destroy();
end
end

function code = iGetFindClusterCode(profileName)
% Get the correct code to find a cluster.
if parallel.internal.ui.shouldUseAPI2()
    code = sprintf('myCluster = parcluster(''%s'');', profileName);
else
    code = sprintf('myCluster = findResource(''scheduler'', ''configuration'',''%s'');', ...
        profileName);
end
end

function jobVarName = iFindJobInWorkspace(profileName, jobID)
% Finds the job in the base workspace

job = iFindJob(profileName, jobID);

% Determine if there are any variables in the base workspace that might be
% the same job object
whosOutput = evalin('base','whos');
index = arrayfun(@(eachVariable) (prod(eachVariable.size) == 1 && isequal(eachVariable.class, class(job))), whosOutput);
whosOutputMatch = whosOutput(index);
for iLoop = 1:numel(whosOutputMatch)
   jobVarName = whosOutputMatch(iLoop).name;
    % Check if the job is the same (profile name and id)
    jobInBase = evalin('base', jobVarName);
    if isvalid(jobInBase) && isequal(jobInBase.ID, jobID) && ...
            strcmp(jobInBase.Parent.Profile, profileName)
        return
    end
end

% Create a new variable in the base workspace
jobVarName = sprintf('job%d', jobID);
% Assign the job to the base
whoOutput = evalin('base','who');
jobVarName = parallel.internal.ui.getUniqueVariableName(jobVarName, whoOutput);
assignin('base',jobVarName,job);

% Display the code to the command line
iDisplayCodeFindJob(profileName, jobVarName, jobID); % Put >> and add to command window
end

function iFindJobAndRunCode(profileName, jobID, template)
% Common code for finding the job and running the job

jobVarName = iFindJobInWorkspace(profileName, jobID);
code = sprintf(template, jobVarName);
iRunCodeInCommandWindow(code);
end


function iRunCodeInCommandWindow(code)
% iRunCodeInCommandWindow - Common code for running code in command window

iDisplayCode(code);
evalin('base', code);
commandwindow; % Brings command window to the front
end
