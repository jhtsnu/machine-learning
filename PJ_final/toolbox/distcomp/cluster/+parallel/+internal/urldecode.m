function urlOut = urldecode(urlIn)
%URLDECODE Replace URL-escaped strings with their original characters
%
% Example:
%   encodedUrl = parallel.internal.urlencode( url )
%
%   decodedUrl = parallel.internal.urldecode( encodedUrl );
%
% See also parallel.internal.urlencode

% Copyright 2010 The MathWorks, Inc.

urlOut = char(java.net.URLDecoder.decode(urlIn,'UTF-8'));