% This class provides static methods for filtering a lists of objects.

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden, Sealed ) WorkUnitFinder

    methods ( Static, Access = private )
        function funits = filter( units, names, values )
        % Implementation of find:
        %   filter "units" and by names/values.
        %   If a given "unit" doesn't have all properties in "names", it can never
        %   match.
        %   Matching is performed using "isequal".

            import parallel.internal.customattr.Reflection;

            % Seed the match by picking those jobs which have all the named properties
            match = Reflection.elementwiseHasAllProperties( units(:), names );

            % Next, refine the filtered list
            for ii = 1:length( names )
                if ~any( match )
                    break;
                end
                oldMatch = match;
                % Only examine those units currently in the running, note we pass the property
                % name in as a cell to "get" to force gotValues to be a cell.
                gotValues = get( units( oldMatch ), names(ii) );
                % Update "match" based on more matching.
                match( oldMatch ) = cellfun( @(x) isequal( x, values{ii} ), ...
                                             gotValues );
            end
            funits = units( match );
        end
    end

    methods ( Static )
        function funits = find( units, args )
        % FIND - find objects matching a set of criteria
        %    matches = find( objs, PVPairsCell )
        %    filters the list "objs" of type CustomPropTypes by the P-V pairs specified
        %    as a cell array.
        %
        %    This is intended to support findJob()/findTask() and similar
        %    methods.

            import parallel.internal.customattr.PropSet
            import parallel.internal.customattr.WorkUnitFinder

            validateattributes( units, {'parallel.internal.customattr.CustomPropTypes'}, {} );

            [filterNames, filterValues] = PropSet.pvToNamesValues( args{:} );
            funits = WorkUnitFinder.filter( units, filterNames, filterValues );
        end
    end
end
