% Constraints - static methods to deal with constraints.

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden, Sealed ) Constraints
    methods ( Static )
        function [simpleConstraint, timingConstraint] = parseConstraint( fullConstraint )
            validateattributes( fullConstraint, {'char'}, {} );
            [simpleConstraint, timingConstraint] = strtok( fullConstraint, '|' );
            if ~isempty( timingConstraint )
                % strip the '|'
                timingConstraint = timingConstraint(2:end);
            end
        end
    end
end
