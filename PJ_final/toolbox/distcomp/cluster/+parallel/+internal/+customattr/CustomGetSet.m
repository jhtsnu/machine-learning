% CustomGetSet - base class supporting get() and set() using CustomPropTypes.
% Subclasses of this class must still overload the abstract methods of
% CustomPropTypes:
% - val = hGetProperty( obj, propName ) % return a property by name
% - hSetProperty( obj, propName, val )  % set a property by name
% - hSetPropertyNoCheck( obj, propName, val ) % set a property by name, do not check the value
%
% Note that methods implemented by this class are Sealed to allow use with
% heterogeneous arrays.

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden ) CustomGetSet < parallel.internal.customattr.CustomPropTypes

    methods ( Access = protected, Sealed )
        function checkValidVisiblePropNames( objOrObjs, userProps )
        % Subclasses may wish to call this to see if some user-supplied properties are
        % valid for an object.
            import parallel.internal.customattr.Reflection;
            import parallel.internal.customattr.GetSetImpl;
            visibleProps = Reflection.getPublicProperties( objOrObjs );
            GetSetImpl.errorIfInvalidProps( objOrObjs, visibleProps, userProps );
        end
    end

    methods ( Hidden, Sealed )
        function vcell = get( varargin )
            import parallel.internal.customattr.GetSetImpl;
            try
                vcell = GetSetImpl.getImpl( varargin{:} );
            catch E
                throw( E );
            end
        end

        function varargout = set( varargin )
            import parallel.internal.customattr.GetSetImpl;
            try
                [ varargout{1:nargout} ] = GetSetImpl.setImpl( varargin{:} );
            catch E
                throw( E );
            end
        end
    end

    methods
        function obj = saveobj( obj )
            warning( message( 'parallel:cluster:CannotSaveCorrectly', ...
                              class( obj ) ) );
        end
    end
    methods ( Static )
        function obj = loadobj( obj )
            warning( message( 'parallel:cluster:CannotLoadCorrectly', ...
                              class( obj ) ) );
        end
    end

    methods ( Hidden )
        function [ values, handled ] = hVectorisedGet( obj, propsToAccess, handled, existingValues )
        % Overridable method to allow classes to implement their own
        % vectorised get of non-PCTGetSet properties.
        % Only get the values for which the corresponding element of
        % handled is false.
            helperFunc = @(a,b,c) iVectorisedGetHelper(obj,a,b,c);
            [values, handled] = cellfun( helperFunc , propsToAccess, handled, existingValues, ...
                                         'UniformOutput', false );            
        end
    end

end
%--------------------------------------------------------------------------
% iVectorisedGetHelper: helper function for bulk getting of access
% properties
function [val, handled] = iVectorisedGetHelper(obj, name, alreadyHandled, existingValue)   
    if alreadyHandled
        val = existingValue;
        handled = alreadyHandled;
    else
        getFcn = @(x) obj.(x);
        val = getFcn(name);
        handled = true;
    end
end


