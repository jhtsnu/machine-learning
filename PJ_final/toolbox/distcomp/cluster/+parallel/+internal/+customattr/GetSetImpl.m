% Helper class implementing get() and set()

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden, Sealed ) GetSetImpl

    methods ( Static, Access = private )
        function setMultipleProperties( obj, useGetSet, propNames, values )
        % Set multiple properties on a scalar 'obj'
            import parallel.internal.customattr.Reflection

            % Check all values
            cellfun( @(n,v) Reflection.deriveAndCheckConstraint( obj, n, v ), ...
                     propNames, values );

            % Set get/set properties in one pass
            obj.hSetProperty( propNames(useGetSet), values(useGetSet) );

            % Set the remainder
            for ii = find( ~(useGetSet(:).') )
                obj.(propNames{ii}) = values{ii};
            end
        end

        function errorIfSetAccessFail( objOrObjs, settableProps, userProps )
        % Throw an error if an attempt is made to set() a property which is not
        % public-settable.

            if ~iscellstr( userProps )
                error(message('parallel:cluster:GetSetPropNameString'));
            end
            badProps = setdiff( userProps, settableProps );
            if ~isempty( badProps )
                badPropsStr = strtrim( sprintf( '%s ', badProps{:} ) );
                error(message('parallel:cluster:GetSetBadPropAccess', class( objOrObjs ), badPropsStr));
            end
        end
    end

    methods ( Hidden, Static )
        function errorIfInvalidProps( objOrObjs, validProps, userProps )
        % Given an array of objects, and a list of valid properties of that
        % object, throw an error if any of the userProps are not on that valid
        % list.

            if ~iscellstr( userProps )
                error(message('parallel:cluster:GetSetPropNameString'));
            end
            badProps = setdiff( userProps, validProps );
            if ~isempty( badProps )
                badPropsStr = strtrim( sprintf( '%s ', badProps{:} ) );
                error(message('parallel:cluster:GetSetBadProps', class( objOrObjs ), badPropsStr));
            end
        end
        function vcell = getImpl( objOrObjs, props )
        % "get" is vectorized over both objOrObjs and props
        % - objOrObjs can be a scalar or vector
        % - props can be a string or a cellstr defining the properties
        %
        % in the case where objOrObjs is an array, the properties must be
        % applicable to the array type - in effect the parent "Heterogeneous"
        % type.

            import parallel.internal.customattr.Reflection;
            import parallel.internal.customattr.GetSetImpl;

            validateattributes( objOrObjs, {'parallel.internal.customattr.CustomPropTypes'}, {} );
            iErrorIfAnyInvalidHandles( objOrObjs );

            % use "getAllPublicProperties" so we can "get" Hidden properties.
            [validProps, useGetSet] = Reflection.getAllPublicProperties( objOrObjs );

            % return a structure array in the case of 1-arg get
            returnStruct = ( nargin == 1 );
            returnCell   = true; % default
            if nargin == 1
                % 1-arg "get" just shows visible properties, i.e. we omit the
                % Hidden properties.  This form of get is not allowed to error.
                props = Reflection.getPublicProperties( objOrObjs );
                allowGetErrors = false;
            else
                if ~iscell( props )
                    props = { props };
                    returnCell = false;
                end
                GetSetImpl.errorIfInvalidProps( objOrObjs, validProps, props );
                allowGetErrors = true;
            end
            % Pre-allocate the return.
            vcell = cell( numel( objOrObjs ), numel( props ) );
            if isempty(vcell)
                return;
            end

            % Pre-computation of which things we can "hGetProperty", and which
            % we must access directly.
            propsToGet       = intersect( props, validProps(useGetSet) );
            propsToAccess    = setdiff( props, validProps(useGetSet) );
            uniqueNames      = [ propsToGet, propsToAccess ];
            [~, reverseIdxs] = ismember( props, uniqueNames );
            
            vals_access_default  = cell(1,numel(propsToAccess));
            handled_default      = num2cell( false(1,numel(propsToAccess)) );
            
            for ii = 1:numel( objOrObjs )
                vals_get         = iGetProperties( objOrObjs(ii), propsToGet, allowGetErrors );
                if ~isempty(propsToAccess)
                    vals_access  = iAccessProperties( objOrObjs(ii), propsToAccess, ...
                                            handled_default, vals_access_default, allowGetErrors );
                else
                    vals_access  = {};
                end
                allVals          = [vals_get, vals_access];
                vcell( ii, : )   = allVals( reverseIdxs );
            end
            % Structure the return as appropriate.
            if returnStruct
                vcell = cell2struct( vcell, props, 2 );
            elseif numel( objOrObjs ) == 1 && ~returnCell
                vcell = vcell{1,1};
            end
        end
        
        function varargout = setImpl( objOrObjs, varargin )
        % set one or more properties, using right-most-wins semantics. No
        % properties "append". So, set(obj, 'Foo', 1, 'Foo', 2) is equivalent to
        % set(obj, 'Foo', 2).
            import parallel.internal.customattr.Reflection;
            import parallel.internal.customattr.GetSetImpl;
            import parallel.internal.customattr.PropSet;

            validateattributes( objOrObjs, {'parallel.internal.customattr.CustomPropTypes'}, {} );
            iErrorIfAnyInvalidHandles( objOrObjs );

            if nargout <= 1 && nargin == 1
                % a = set( thing ); return a struct with all the settable
                % property names and {} as the value.
                settableProps = Reflection.getUserSettableProperties( objOrObjs );
                empties = repmat( {{}}, 1, numel( settableProps ) );
                varargout{1} = cell2struct( empties, settableProps, 2 );
                return
            elseif nargout <= 1 && nargin == 2
                % [a] = set( thing, 'Prop' ); - we should list allowed values,
                % but in fact simply return {}
                if isstruct( varargin{1} )
                    % User is trying to give a single struct where we should set
                    % names and values. But we don't support that.
                    error(message('parallel:cluster:SetStructNotSupported'));
                end
                if ~ischar( varargin{1} )
                    error(message('parallel:cluster:PropertyNameMustBeChar'));
                end
                varargout{1} = {};
                return
            else
                % set( thing, 'Prop', value, 'Prop2', value2, ... );
                narginchk( 3, Inf );
                nargoutchk( 0, 0 );
            end

            if nargin == 3 && iscell( varargin{1} ) && iscell( varargin{2} )
                % set( thing, {'Prop1', 'Prop2'}, {value1, value2} );
                if numel( varargin{1} ) ~= numel( varargin{2} )
                    error(message('parallel:cluster:SetCellArrayLengthConsistency'));
                end
                % Convert the cell arrays into P-V pairs
                pvPairs = [varargin{1}(:)'; varargin{2}(:)'];
                pvPairs = pvPairs(:).';
            else
                % Here handle disallowed cases. Must be P-V pairs.
                if mod(numel(varargin),2) ~= 0
                    error(message('parallel:cluster:PropertyValueInPairs'));
                end
                if any( ~cellfun( @ischar, varargin(1:2:end) ) )
                    error(message('parallel:cluster:PropertyNameMustBeChar'));
                end
                pvPairs = varargin;
            end

            try
                % Enforce rightmost-wins for set() here.
                [p, v] = PropSet.amalgamate( {}, pvPairs{:} );
            catch E
                % error in the case where not in pairs, or not strings
                throw( E );
            end

            % Get all properties to allow set() on Hidden properties.
            [settableProps, allProps] = Reflection.getAllUserSettableProperties( objOrObjs );
            GetSetImpl.errorIfInvalidProps( objOrObjs, allProps, p );
            GetSetImpl.errorIfSetAccessFail( objOrObjs, settableProps, p );
            useGetSet       = Reflection.propertyUsesGetSet( objOrObjs, p );
            % Single try-catch around all settings - so we bail the first time a
            % "set" fails.
            try
                for ii = 1:length( objOrObjs )
                    o = objOrObjs(ii);
                    GetSetImpl.setMultipleProperties( o, useGetSet, p, v );
                end
            catch E
                throw( E );
            end
        end
    end
end

%-----------------------------------------------------
function iErrorIfAnyInvalidHandles( objOrObjs )
    if ~all( isvalid( objOrObjs ) )
        error( message( 'MATLAB:class:InvalidHandle' ) );
    end
end

%-----------------------------------------------------
function v = iGetProperties( obj, name, allowErrors )
    try
        v = hGetProperty( obj, name );
    catch err
        if allowErrors
            rethrow(err);
        else
            % Do each one individually
            v = cellfun( @(x) iGetSinglePropertyNoErrors( obj, x ), ...
                         name, 'UniformOutput', false );
        end
    end
end

%-----------------------------------------------------
function v = iAccessProperties( obj, name, handled, existingValue, allowErrors )
    try
        v  = obj.hVectorisedGet(name, handled, existingValue);
    catch err
        if allowErrors
            rethrow( err );
        else
            % Access each one individually
            accessFunc = @(x, y, z) iAccessSinglePropertyNoErrors( obj, x, y, z );
            v = cellfun( accessFunc, name, handled, existingValue, ...
                         'UniformOutput', false);
        end
    end
end

%-----------------------------------------------------
function v = iGetSinglePropertyNoErrors( obj, name )
    v = [];
    try
        v = obj.hGetProperty( name );
    catch err
        dctSchedulerMessage( 4, 'Failed to get the value for ''%s'': %s', ...
            name, err.getReport() );
    end
end

%-----------------------------------------------------
function v = iAccessSinglePropertyNoErrors( obj, name, handled, existingValue )
    v = [];
    try
        v = obj.hVectorisedGet( {name}, {handled}, {existingValue} );
        v = v{1};
    catch err
        dctSchedulerMessage( 4, 'Failed to get the value for ''%s'': %s', ...
            name, err.getReport() );
    end
end
