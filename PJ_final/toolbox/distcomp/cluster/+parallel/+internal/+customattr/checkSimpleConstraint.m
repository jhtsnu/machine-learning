function checkSimpleConstraint( constraint, name, val )
% checkSimpleConstraint - check non-timing portion of a constraint
%   Called by checkConstraint with only the name and value
%
%   Valid constraints are:
%     - datalocation: value must be either a string, or a struct with fields
%        {'windows', 'unix'}
%        (or 'pc', 'unix') % PCTAPI1: remove this line
%     - numericscalar: any numeric class, scalar
%     - positivescalar: any numeric class, scalar value >= 0
%     - positiveintscalar: any numeric class, scalar value >= 0, value == round(value)
%     - string: char vector, size 1xN; or empty of any class
%     - nonemptystring: char vector, size 1xN, N > 0
%     - cellstr: iscellstr(val) returns true
%     - portrange: numeric vector of valid ports
%     - logicalscalar: a logical scalar
%     - callback: either a string or a callback function handle
%     - workerlimits: either: val > 0, or pair val(1)<=val(2) && val(1) > 0
%     and there's a catch-all constraint:
%     - <classname>: validates that "isa( value, <classname> )" returns true.
%

% Copyright 2011-2012 The MathWorks, Inc.

validateattributes( constraint, {'char'}, {} );
validateattributes( name, {'char'}, {} );

ME = [];
enumType = sscanf(constraint, 'enum:%s');
if ~isempty(enumType)
    constraint = 'enum';
end
switch lower(constraint)
  case 'datalocation'
    ME = iValidate( iIsDataLocation( val ), ...
                    'parallel:cluster:DataLocationConstraint', name );
  case 'numericscalar'
    ME = iValidate( iIsNumericScalar( val ),  ...
                    'parallel:cluster:NumericScalarConstraint', name );
  case 'positivescalar'
    ME = iValidate( iIsNumericScalar( val ) && val >= 0,  ...
                    'parallel:cluster:PositiveScalarConstraint', name );
  case 'positiveintscalar'
    ME = iValidate( iIsIntegerScalar( val ) && val >= 0,  ...
                    'parallel:cluster:PositiveIntScalarConstraint', name );
  case 'string'
    ME = iValidate( iIsStringOrEmpty( val ),  ...
                    'parallel:cluster:StringConstraint', name );
  case 'nonemptystring'
    ME = iValidate( iIsNonEmptyString( val ),  ...
                    'parallel:cluster:NonEmptyStringConstraint', name );
  case 'cellstr'
    ME = iValidate( iscellstr( val ),  ...
                    'parallel:cluster:CellStrConstraint', name );
  case 'portrange'
    ME = iValidatePortRange( val, name );
  case 'logicalscalar'
    ME = iValidate( islogical( val ) && isscalar( val ),  ...
                    'parallel:cluster:LogicalScalarConstraint', name );
  case 'callback'
    ME = iValidate( iIsCallback( val ),  ...
                    'parallel:cluster:CallbackConstraint', name );
  case 'workerlimits'
    ME = iValidate( iIsWorkerLimits( val ), ...
                    'parallel:cluster:WorkerNumberLimitsConstraint', name );
  case 'sharedfolders'
    ME = iValidate( iIsSharedFolders( val ), ...
                    'parallel:cluster:SharedFoldersConstraint', name );
  case 'function'
    ME = iValidate( iIsFunction( val ), ...
                    'parallel:cluster:FunctionConstraint', name );
  case 'enum'
    ME = iValidateEnum( enumType, val, name );
  case 'none'
    % Ok!
  otherwise
    % Assume "isa" constraint
    ME = iValidate( isa( val, constraint ),  ...
                    'parallel:cluster:ClassConstraint', name, constraint );
end

if ~isempty( ME )
    throw( ME );
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iValidate - if ~isOk, return an MException with the specified errorID.
function ME = iValidate( isOk, errId, varargin )
if ~isOk
    m = message( errId, varargin{:} );
    ME = MException( m.Identifier, '%s', m.getString() );
else
    ME = [];
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iIsFunction - either a function handle, or a simple string
function valid = iIsFunction( val )
valid = isa( val, 'function_handle' ) || ( ...
    ischar( val ) && ~isempty( val ) && isrow( val ) );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsNumericScalar( val )
valid = isnumeric( val ) && isscalar( val ) && isreal( val );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsIntegerScalar( val )
valid = iIsNumericScalar( val ) && round( val ) == val;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsNonEmptyString( val )
valid = ischar( val )  && isvector( val ) && size( val, 1 ) == 1;
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsScalarIntInRange( val, validRange )
valid = iIsIntegerScalar( val ) && ...
        val >= validRange( 1 ) && val <= validRange( 2 );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsDataLocation( val )
if ischar( val )
    valid = true;
elseif isa( val, 'struct' ) && isscalar( val )
    fn    = fieldnames( val );
    valid = isequal( sort( fn ), {'unix'; 'windows'} );
else
    valid = false;
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsStringOrEmpty( val )
valid = iIsNonEmptyString( val ) || ( ischar( val ) && isempty( val ) );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ME = iValidatePortRange( val, name )
validPortRange = [1, intmax('uint16')];    
isValidRange = isnumeric( val ) && numel( val ) == 2 ...
    && iIsScalarIntInRange( val(1), validPortRange ) ...
    && iIsScalarIntInRange( val(2), validPortRange ) ...
    && val(2) > val(1);

isEphemeral = iIsNumericScalar( val ) && val == 0;
ok = isValidRange || isEphemeral;
ME = [];
if ok && isunix && val( 1 ) < 1024 && ~isEphemeral
    warning(message('parallel:cluster:UsingWellKnownPortNumber'));
elseif ~ok
    % MException ctor requires numeric values to be "double".
    validPortRange = double( validPortRange );
    ME = MException(message('parallel:cluster:PortRangeConstraint', name, validPortRange( 1 ), validPortRange( 2 )));
end

end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsCallback( val )
if isempty( val )
    % Need to be able to unset a callback property, but we only allow certain
    % types of empty.
    valid = isnumeric( val ) || iscell( val ) || ischar( val );
else
    valid = iIsCallbackFunction( val ) || ...
            ( iscell( val ) && isvector( val ) && iIsCallbackFunction( val{1} ) );
end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsCallbackFunction( val )
valid = iIsNonEmptyString( val ) || isa( val, 'function_handle' );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsWorkerLimits( val )
if isnumeric( val ) && ...
        any( numel( val ) == [1 2] ) && ...
        all( val == round(val) ) && ...
        all( val > 0 ) && ...
        isfinite( val(1) ) % min value must be finite, max can be infinite
    if numel( val ) == 2
        valid = ( val(2) >= val(1) );
    else
        valid = true;
    end
else
    valid = false;
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function valid = iIsSharedFolders( val )
valid = iscell( val ) && ...
        size( val, 2 ) == 2 && ...
        all( cellfun( @iIsNonEmptyString, val(:, 1) ) ) && ...
        all( cellfun( @(x) ischar(x) || iscellstr(x) || isempty(x), val(:, 2) ) );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function ME = iValidateEnum( enumType, val, name )
persistent enumMap
if isempty( enumMap )
    enumMap = containers.Map();
end

ME = [];
if ~enumMap.isKey( enumType )
    mc = meta.class.fromName( enumType );
    if isempty( mc ) || ...
       ~any( strcmp( 'parallel.internal.types.NamedEnumeration', {mc.SuperclassList.Name} ) )
        ME = MException(message('parallel:cluster:EnumConstraintNotNamedEnumeration', enumType));
        return
    end
    
    enumVals = enumeration( enumType );
    names = arrayfun( @(x) x.Name, enumVals, 'UniformOutput', false );
    enumMap(enumType) = names;
end
allowedStrings = enumMap(enumType);
if ~any( strcmp( val, allowedStrings ) )
    ME = MException(message('parallel:cluster:EnumConstraint', name, strtrim( sprintf( '%s ', allowedStrings{ : } ) )));
end
end
