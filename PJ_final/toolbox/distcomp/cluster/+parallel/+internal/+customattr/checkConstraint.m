function checkConstraint( constraint, obj, name, val )
% checkConstraint - check that a proposed new value is valid otherwise error
%    This function is called by the parallel.internal.customattr.CustomPropTypes
%    implementation prior to calling hSetProperty() when a property is set using
%    "obj.Prop = val" notation
%
%    The first argument must either be a constraint, or [] if the constraint is
%    to be derived using parallel.internal.customattr.Reflection.
%
%    The second argument must be a scalar object derived from CustomPropTypes.
%
%    The third argument must be the name of property being set.
%
%    The fourth argument must be the proposed new value.
%
%    The syntax of a valid constraint is "constraint"
%    or "constraint|timing".
%
%    Valid constraints are defined in checkSimpleConstraint
%
%    The only valid value for "timing" is "presubmission". To support a timing
%    constraint, the supplied object must have a property "StateEnum" which
%    contains a value of type parallel.internal.types.States.

% Copyright 2011 The MathWorks, Inc.

validateattributes( obj, {'parallel.internal.customattr.CustomPropTypes'}, {'scalar'} );
validateattributes( name, {'char'}, {} );
validateattributes( constraint, {'char'}, {} );

% Check the timing first before going on to check value.
[constraint, ME] = iCheckTiming( obj, name, constraint );

if ~isempty( ME )
    throwAsCaller( ME );
end

% Check the underlying value.
try
    parallel.internal.customattr.checkSimpleConstraint( constraint, name, val );
catch E
    throwAsCaller( E );
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [constraint, ME] = iCheckTiming( obj, propName, constraint )

import parallel.internal.customattr.Constraints

ME = [];
[constraint, timing] = Constraints.parseConstraint( constraint );

if isempty( timing )
    return;
else
    try
        obj.hCheckTimingForSet( timing, propName );
    catch E
        ME = E;
    end
end
end

