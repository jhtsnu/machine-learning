% NamedEnumeration - Base class for all named enumerations that are used as PCTConstraints
 
%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden) NamedEnumeration
    properties (SetAccess = immutable)
        Name
    end
    
    methods 
        function obj = NamedEnumeration(name)
            obj.Name = name;
        end
    end
    
    methods (Static, Access = protected)
        function enumVal = getEnumFromNameI(enumClassName, name)
            name = iValidateNameAndClassName(enumClassName, name);
            [~, enumMap] = iGetNameToEnumMap(enumClassName);
            enumVal = iGetEnumsFromMap(enumMap, name, enumClassName);
        end
        
        function enumVal = getEnumFromName(enumClassName, name)
            name = iValidateNameAndClassName(enumClassName, name);
            enumMap = iGetNameToEnumMap(enumClassName);
            enumVal = iGetEnumsFromMap(enumMap, name, enumClassName);
        end
    end
end

%---------------------------------------------------
function name = iValidateNameAndClassName(enumClassName, name)
validateattributes(enumClassName, {'char'}, {'row'});
if ~iscell(name)
    name = {name};
end
if ~iscellstr(name)
    error(message('parallel:cluster:NamedEnumerationNameMustBeString'));
end
end

%---------------------------------------------------
function enumVal = iGetEnumsFromMap(enumMap, names, enumClassName)
validateattributes(names, {'cell'}, {});
validNames = enumMap.isKey(names);
if ~all(validNames)
    failedMatchName = names(~validNames);
    error(message('parallel:cluster:UnknownNamedEnumerationNames', enumClassName, strtrim( sprintf( ' %s', failedMatchName{ : } ) )));
end
enumVal = cellfun(@(x) enumMap(x), names, 'UniformOutput', false);
enumVal = [enumVal{:}]';
end

%---------------------------------------------------
function [nameEnumMap, nameEnumMapI] = iGetNameToEnumMap(enumClassName)
% Get the name to enum map for the supplied class.  The nameEnumMapI
% stores the Names in lower case.
persistent typeEnumMap
if isempty(typeEnumMap)
    typeEnumMap = containers.Map();
end

if ~typeEnumMap.isKey(enumClassName)
    nameEnumMap = containers.Map();
    nameEnumMapI = containers.Map();
    members = enumeration(enumClassName);
    for ii = 1:length(members)
        memberName = members(ii).Name;
        nameEnumMap(memberName) = members(ii);
        nameEnumMapI(lower(memberName)) = members(ii);
    end
    typeEnumMap(enumClassName) = {nameEnumMap, nameEnumMapI};
end

enumMaps = typeEnumMap(enumClassName);
nameEnumMap = enumMaps{1};
nameEnumMapI = enumMaps{2};
end
