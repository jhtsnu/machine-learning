% ValidationStatus - Enumeration for the validation status of profiles
 
%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) ValidationStatus < parallel.internal.types.NamedEnumeration
    enumeration
        NotRun  ('Not Run')
        Running ('Running')
        Passed  ('Passed')
        Failed  ('Failed')
        Skipped ('Skipped')
        Stopped ('Stopped')
    end
end