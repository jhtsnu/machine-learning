% MpiexecSetEnvMethod - Enumeration for Mpiexec EnvironmentSetMethod
 
%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) MpiexecSetEnvMethod < parallel.internal.types.NamedEnumeration
    enumeration
        env('-env')
        setenv('setenv')
    end
end