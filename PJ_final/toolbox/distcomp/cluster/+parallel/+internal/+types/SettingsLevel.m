% SettingsLevel - Enumeration for different settings levels
 
%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) SettingsLevel < parallel.internal.types.NamedEnumeration
    enumeration
        %         Name,        CanGet, CanSet, CanCallIsSet
        Session  ('session',   false, false, false); % Don't support session at all except in deployed
        User     ('user',      true,  true,  true);
        % NB workgroup level is not supported in 12b for Settings.  See g796418
        % Workgroup('workgroup', true,  true,  true);  % TODO:OneDay derive from Settings?
        Factory  ('factory',   true,  false, true);
        Collapsed('collapsed', true,  false, false);
    end
    
    properties (GetAccess = private, SetAccess = immutable)
        % Can we call "get" at this level?
        CanGet = true;
        % Can we call "set" or "unset" at this level?
        CanSet = false;
        % Can we call "isSet"at this level?
        CanCallIsSet = true;
    end
    
    properties (Dependent, SetAccess = immutable)
        CanReallyGet
        CanReallySet
    end
    
    methods
        function val = get.CanReallyGet(obj)
            import parallel.internal.types.SettingsLevel
            if obj == SettingsLevel.Session && iGetIsDeployed()
                val = true;
            else
                val = obj.CanGet;
            end
        end

        function val = get.CanReallySet(obj)
            import parallel.internal.types.SettingsLevel
            if obj == SettingsLevel.Session && iGetIsDeployed()
                val = true;
            else
                val = obj.CanSet;
            end
        end
    end
    
    
    methods (Static)
        function enumVal = fromNameI(name)
        % Return a SettingsLevel given a name, case-insensitive.  
        % Accepts a single name or a cell array of names. 
        % SettingsLevel.Collapsed is not matched because it is not
        % a "true" Settings level.
            import parallel.internal.types.NamedEnumeration
            import parallel.internal.types.SettingsLevel
            
            try
                enumVal = NamedEnumeration.getEnumFromNameI('parallel.internal.types.SettingsLevel', name);
                if any(enumVal == SettingsLevel.Collapsed)
                    error(message('parallel:settings:SettingsLevelFromNameCollapsed'));
                end
            catch err
                ex = MException(message('parallel:settings:InvalidSettingsLevel'));
                ex = ex.addCause(err);
                throw(ex);
            end
        end

        function errorIfLevelsNotSettable(levels)
            cannotSet = arrayfun(@(x) ~x.CanReallySet, levels);
            if any(cannotSet)
                firstErroringLevel = levels(find(cannotSet, 1, 'first'));
                error(message('parallel:settings:SettingsLevelNotWritable', firstErroringLevel.Name));
            end
        end

        function errorIfLevelsNotGettable(levels)
            cannotGet = arrayfun(@(x) ~x.CanReallyGet, levels);
            if any(cannotGet)
                firstErroringLevel = levels(find(cannotGet, 1, 'first'));
                error(message('parallel:settings:SettingsLevelNotReadable', firstErroringLevel.Name));
            end
        end

        function errorIfCannotIsSetLevels(levels)
            cannotCallIsSet = arrayfun(@(x) ~x.CanCallIsSet, levels);
            if any(cannotCallIsSet)
                firstErroringLevel = levels(find(cannotCallIsSet, 1, 'first'));
                error(message('parallel:settings:SettingsLevelCannotCallIsSet', firstErroringLevel.Name));
            end
        end
    end
    
    methods
        function obj = SettingsLevel(name, canGet, canSet, canCallIsSet)
            obj@parallel.internal.types.NamedEnumeration(name);
            obj.CanGet = canGet;
            obj.CanSet = canSet;
            obj.CanCallIsSet = canCallIsSet;
        end
    end
end

function tf = iGetIsDeployed()
tf = isdeployed() || parallel.internal.settings.qeDeployedOverride();
end

