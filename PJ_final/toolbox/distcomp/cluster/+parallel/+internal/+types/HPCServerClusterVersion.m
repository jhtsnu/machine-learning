% HPCServerClusterVersion - Enumeration for HPC Server Cluster version
 
%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) HPCServerClusterVersion < parallel.internal.types.NamedEnumeration
    enumeration
        CCS('CCS', 1)
        HPCServer2008('HPCServer2008', 2)
    end
    
    properties (SetAccess = immutable)
        VersionNumber
    end
    
    methods
        function obj = HPCServerClusterVersion(name, versionNumber)
            obj@parallel.internal.types.NamedEnumeration(name);
            obj.VersionNumber = versionNumber;
        end
    end
    
    methods (Static)
        function enumVal = fromName(name)
            import parallel.internal.types.NamedEnumeration
            validateattributes(name, {'char'}, {'row'});
            
            try
                enumVal = NamedEnumeration.getEnumFromName('parallel.internal.types.HPCServerClusterVersion', name);
            catch err
                ex = MException(message('parallel:cluster:InvalidHPCServerClusterVersion', name));
                ex = ex.addCause(err);
                throw(ex);
            end
        end
    end
end