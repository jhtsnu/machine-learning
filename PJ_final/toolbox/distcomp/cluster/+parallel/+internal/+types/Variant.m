% Variant - enumerated type of known entity types.

% Copyright 2011 The MathWorks, Inc.

classdef ( Sealed, Hidden ) Variant
    enumeration
        Cluster              ( 'cluster'     );
        Worker               ( 'worker'      );
        IndependentJob       ( 'independent' );
        CommunicatingSPMDJob ( 'spmd'        );
        CommunicatingPoolJob ( 'pool'        );
        Task                 ( 'task'        );
    end
    properties ( SetAccess = immutable )
        % The user-visible name of the Variant.
        Name = '';
    end
    properties ( Constant, GetAccess = private )
        % Map lower(name) --> enumerated value
        NameToValMapI = containers.Map();
    end
    methods ( Static )
        function e = fromNameI( name )
        % Return a Variant given a name, case-insensitive
            import parallel.internal.types.Variant

            validateattributes( name, {'char'}, {} );

            lowerName = lower( name );
            map       = Variant.NameToValMapI;
            if map.isKey( lowerName )
                e = map( lowerName );
            else
                error(message('parallel:cluster:UnknownVariantName', name));
            end
        end
    end
    methods
        function obj = Variant( name )
            import parallel.internal.types.Variant

            obj.Name             = name;
            map                  = Variant.NameToValMapI;
            map( lower( name ) ) = obj; %#ok<NASGU> map is a handle
        end
    end
end
