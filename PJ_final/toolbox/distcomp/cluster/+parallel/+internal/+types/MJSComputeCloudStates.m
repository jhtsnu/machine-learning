% Enumeration of the states which a MJS Compute Cloud Cluster can be in. This enumeration
% allows comparisons. These are the user-visible states of the cluster

% Copyright 2011 The MathWorks, Inc.

classdef MJSComputeCloudStates
    enumeration
        Stopped            ( 'offline'  , 0 )
        Starting           ( 'starting' , 1 )
        Ready              ( 'online'   , 2 )
        Stopping           ( 'stopping' , 3 )
        Deleted            ( 'deleted'  , 4 )
        CommunicationError ( 'communicationerror', 100)
        Error              ( 'error'    , 101)
    end
    properties ( SetAccess = immutable )
        Name    % The user-visible name of the State
    end
    properties ( SetAccess = immutable, GetAccess = private )
        Ordinal % For comparisons
    end
    methods ( Static )
        function e = fromName( name )
        % Return a State from a user-visible name.
            validateattributes( name, {'char'}, {} );
            map = iStateMap();
            if ~map.isKey( name )
                error(message('parallel:cluster:MJSComputeCloud:InvalidStateName', name));
            end
            e   = map(name);
        end
    end
    methods ( Access = private )
        function tf = compare( o1, o2, fh )
        % Supports comparison of State objects
            validateattributes( o1, {'parallel.internal.types.MJSComputeCloudStates'}, {'scalar'} );
            validateattributes( o2, {'parallel.internal.types.MJSComputeCloudStates'}, {'scalar'} );
            validateattributes( fh, {'function_handle'}, {} );
            tf = fh( o1.Ordinal, o2.Ordinal );
        end
    end
    methods
        function obj = MJSComputeCloudStates( name, ordinal )
            obj.Name    = name;
            obj.Ordinal = ordinal;
        end
        function tf = lt( obj1, obj2 ), tf = compare( obj1, obj2, @lt ); end
        function tf = gt( obj1, obj2 ), tf = compare( obj1, obj2, @gt ); end
        function tf = le( obj1, obj2 ), tf = compare( obj1, obj2, @le ); end
        function tf = ge( obj1, obj2 ), tf = compare( obj1, obj2, @ge ); end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Return a mapping from State name to State
function [nameToEnum] = iStateMap()
    persistent NAME_MAP
    if isempty( NAME_MAP )
        NAME_MAP = containers.Map();
        members  = enumeration( 'parallel.internal.types.MJSComputeCloudStates' );
        for ii = 1:length( members )
            NAME_MAP(members(ii).Name) = members(ii);
        end
    end
    nameToEnum = NAME_MAP;
end
