% SchedulerType - all scheduler types

%   Copyright 2011-2012 The MathWorks, Inc.

%#function parallel.internal.types.ClusterCategory
classdef SchedulerType < parallel.internal.types.NamedEnumeration
    enumeration
        Generic(         'Generic',         'parallel.cluster.Generic', ...
                         parallel.internal.types.ClusterCategory.Network )
        HPCServer(       'HPCServer',       'parallel.cluster.HPCServer', ...
                         parallel.internal.types.ClusterCategory.Network )
        Local(           'Local',           'parallel.cluster.Local', ...
                         parallel.internal.types.ClusterCategory.Local )
        LSF(             'LSF',             'parallel.cluster.LSF', ...
                         parallel.internal.types.ClusterCategory.Network )
        MJS(             'MJS',             'parallel.cluster.MJS', ...
                         parallel.internal.types.ClusterCategory.Network )
        MJSComputeCloud( 'MJSComputeCloud', 'parallel.cluster.MJSComputeCloud', ...
                         parallel.internal.types.ClusterCategory.Cloud )
        Mpiexec(         'Mpiexec',         'parallel.cluster.Mpiexec', ...
                         parallel.internal.types.ClusterCategory.Network )
        PBSPro(          'PBSPro',          'parallel.cluster.PBSPro', ...
                         parallel.internal.types.ClusterCategory.Network )
        Torque(          'Torque',          'parallel.cluster.Torque', ...
                         parallel.internal.types.ClusterCategory.Network )
    end
    
    properties ( SetAccess = immutable )
        ClusterClassName;
        ClusterCategory;
    end
    
    methods (Static)
        function enumVal = fromName(name)
        % Return a SchedulerComponentType given a name, case-sensitive.  
        % Accepts a single name or a cell array of names. 
            import parallel.internal.types.NamedEnumeration
            
            try
                enumVal = NamedEnumeration.getEnumFromName('parallel.internal.types.SchedulerType', name);
            catch err
                ex = MException(message('parallel:cluster:InvalidClusterType'));
                ex = ex.addCause(err);
                throw(ex);
            end
        end
    end

    methods 
        function obj = SchedulerType(name, clusterClassName, clusterCategory)
            obj@parallel.internal.types.NamedEnumeration(name);
            obj.ClusterClassName = clusterClassName;
            obj.ClusterCategory  = clusterCategory;
        end
    end
end
