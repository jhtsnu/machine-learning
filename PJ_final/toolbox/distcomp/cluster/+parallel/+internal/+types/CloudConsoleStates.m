% Enumeration of the states which a Cloud Console Cluster can be in. This enumeration
% allows comparisons. It is not intended to be the user-visible expression of
% the state of a cluster.

% Copyright 2011 The MathWorks, Inc.

classdef ( Hidden ) CloudConsoleStates
    enumeration
        Defined       ( 'DEFINED'        , 0 )
        Starting      ( 'STARTING'       , 1 )
        HeadnodeReady ( 'HEADNODE_READY' , 2 )
        Ready         ( 'READY'          , 3 )
        Stopping      ( 'STOPPING'       , 4 )
        Stopped       ( 'STOPPED'        , 5 )
        Deleted       ( 'DELETED'        , 6 )
        FailedToStop  ( 'FAILED_TO_STOP' , 100 )
        Failed        ( 'FAILED'         , 101 )
        Unknown       ( 'UNKNOWN'        , 200 )
    end
    properties ( SetAccess = immutable )
        Name    % The user-visible name of the State
    end
    properties ( SetAccess = immutable, GetAccess = private )
        Ordinal % For comparisons
    end
    methods ( Static )
        function e = fromName( name )
        % Return a State from a user-visible name.
            validateattributes( name, {'char'}, {} );
            map = iStateMap();
            if ~map.isKey( name )
                error(message('parallel:cluster:CloudConsole:InvalidStateName', name));
            end
            e   = map(name);
        end
    end
    methods ( Access = private )
        function tf = compare( o1, o2, fh )
        % Supports comparison of State objects
            validateattributes( o1, {'parallel.internal.types.CloudConsoleStates'}, {'scalar'} );
            validateattributes( o2, {'parallel.internal.types.CloudConsoleStates'}, {'scalar'} );
            validateattributes( fh, {'function_handle'}, {} );
            tf = fh( o1.Ordinal, o2.Ordinal );
        end
    end
    methods
        function obj = CloudConsoleStates( name, ordinal )
            obj.Name    = name;
            obj.Ordinal = ordinal;
        end
        function tf = lt( obj1, obj2 ), tf = compare( obj1, obj2, @lt ); end
        function tf = gt( obj1, obj2 ), tf = compare( obj1, obj2, @gt ); end
        function tf = le( obj1, obj2 ), tf = compare( obj1, obj2, @le ); end
        function tf = ge( obj1, obj2 ), tf = compare( obj1, obj2, @ge ); end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Return a mapping from State name to State
function [nameToEnum] = iStateMap()
    persistent NAME_MAP
    if isempty( NAME_MAP )
        NAME_MAP = containers.Map();
        members  = enumeration( 'parallel.internal.types.CloudConsoleStates' );
        for ii = 1:length( members )
            NAME_MAP(members(ii).Name) = members(ii);
        end
    end
    nameToEnum = NAME_MAP;
end
