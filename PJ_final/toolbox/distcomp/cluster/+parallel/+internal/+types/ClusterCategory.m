% ClusterCategory - all cluster categories

%   Copyright 2012 The MathWorks, Inc.

classdef ClusterCategory < parallel.internal.types.NamedEnumeration
    enumeration
        Local   ('Local')
        Network ('Network')
        Cloud   ('Cloud')
    end
end