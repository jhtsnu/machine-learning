% OperatingSystem - Enumeration for OperatingSystem
 
%   Copyright 2011 The MathWorks, Inc.

classdef (Hidden, Sealed) OperatingSystem < parallel.internal.types.NamedEnumeration
    enumeration
        Windows('windows')
        Unix('unix')
        Mixed('mixed')
    end
end