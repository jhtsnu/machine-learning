
%   Copyright 2012 The MathWorks, Inc.

classdef (Hidden) AttachedFiles < parallel.internal.display.FileList

    methods

        function obj = AttachedFiles(displayHelper, list, job)
            obj@parallel.internal.display.FileList(displayHelper, list, job);
        end

    end

    methods ( Access = protected )
        function value = getDisplayValue( ~, numFiles )
            value = getString( message( 'parallel:job:AttachedFilesLink', numFiles ) );
        end

        function link = getDisplayLink( obj, job, displayValue )
            if obj.DisplayHelper.ShowLinks
                matlabFunction = sprintf( ...
                    'parallel.internal.display.AttachedFiles.displayAttachedFiles(''%s'')', ...
                    serialize( parallel.internal.display.JobMemento( job ) ) );
                link = parallel.internal.display.HTMLDisplayType( displayValue, matlabFunction );
            else
                link = parallel.internal.display.HTMLDisplayType( displayValue );
            end
        end
    end

    methods ( Static )
        function displayAttachedFiles( serializedMemento )
            try
                job = createObjectFromMemento( ...
                    parallel.internal.display.Memento.deserialize( serializedMemento ) );
                disp( getString( message( 'parallel:job:AttachedFilesList', job.ID, ...
                    sprintf( '  %s\n', job.AttachedFiles{:} ) ) ) );
            catch err
                dctSchedulerMessage( 1, ...
                    'Failed to create Job from memento, original error: %s', err.getReport() );
                if feature( 'hotlinks' )
                    helpString = '<a href="matlab: help parallel.Job">help parallel.Job</a>';
                else
                    helpString = 'help parallel.Job';
                end
                error( message('parallel:cluster:LinkDispFailure', 'AttachedFiles', ...
                        getString( message( 'parallel:cluster:Job' ) ), helpString));
            end
        end
    end
end
