
%   Copyright 2012 The MathWorks, Inc.

classdef (Hidden) AutoAttachedFiles < parallel.internal.display.DisplayableItem

    properties ( SetAccess = immutable, GetAccess = private )
        Job
    end

    methods

        function obj = AutoAttachedFiles( displayHelper, job )
            obj@parallel.internal.display.DisplayableItem( displayHelper );
            obj.Job = job;
        end

        function displayInMATLAB( obj, name )
            if obj.DisplayHelper.ShowLinks
                % If we are showing links, show a link which will call
                % listAutoAttachedFiles() when clicked.
                displayValue = getString( message( 'parallel:job:AutoAttachedFilesLink' ) );
                matlabFunction = sprintf( ...
                    'parallel.internal.display.AutoAttachedFiles.displayAutoAttachedFiles(''%s'')', ...
                    serialize( parallel.internal.display.JobMemento( obj.Job ) ) );
                link = parallel.internal.display.HTMLDisplayType( displayValue, matlabFunction );
                obj.DisplayHelper.displayProperty( name, link );
            else
                % If we are not showing links, we cannot show the list of
                % auto attached files before submission, as it would potentially
                % slow down job display unreasonably.
                if obj.Job.StateEnum < parallel.internal.types.States.Queued
                    displayValue = getString( message( 'parallel:job:AutoAttachedPreSubmission' ) );
                else
                    % After submission, the auto attached files are cached
                    % so this is always a quick operation.
                    displayValue = obj.Job.hGetAutoAttachedFilesNoError();
                end
                obj.DisplayHelper.displayProperty( name, displayValue );
            end
        end

    end

    methods ( Static )
        function displayAutoAttachedFiles( serializedMemento )
            try
                job = createObjectFromMemento( ...
                    parallel.internal.display.Memento.deserialize( serializedMemento ) );
                job.listAutoAttachedFiles();
            catch err
                dctSchedulerMessage( 1, ...
                    'Failed to create Job from memento, original error: %s', err.getReport() );
                if feature( 'hotlinks' )
                    helpString = '<a href="matlab: help parallel.Job">help parallel.Job</a>';
                else
                    helpString = 'help parallel.Job';
                end
                error( message( 'parallel:cluster:LinkDispFailure', 'AutoAttachedFiles', ...
                    getString( message( 'parallel:cluster:Job' ) ), helpString ) );
            end
        end
    end
end
