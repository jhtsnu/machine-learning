% JobMemento - Used to construct job objects in order to display the associated tasks

% Copyright 2012 The MathWorks, Inc.

classdef ( Hidden ) JobMemento < parallel.internal.display.Memento
   
    properties ( SetAccess = immutable, GetAccess = private )
        ParentMemento
        ID
    end
    
    methods
        
        function obj = JobMemento(job)
            validateattributes( job, {'parallel.Job'}, {} );
            obj.ParentMemento = parallel.internal.display.ClusterMemento(job.Parent);
            obj.ID = job.ID;
        end
        
        function j = createObjectFromMemento(obj)
            j = findJob(createObjectFromMemento(obj.ParentMemento), 'ID', obj.ID);
        end
        
    end
end