% Link - The values which include links are displayed this way.

% Copyright 2012 The MathWorks, Inc.

classdef (Hidden) Link < parallel.internal.display.DisplayableItem
    properties (SetAccess = immutable, GetAccess = private)
        Value
    end

    methods

        function obj = Link( displayHelper, displayValue, linkObj ) 
            obj@parallel.internal.display.DisplayableItem(displayHelper);
            if displayHelper.ShowLinks
                obj.Value = hGetLink( linkObj, displayValue );
            else
                obj.Value = parallel.internal.display.HTMLDisplayType( displayValue );
            end
        end

        function displayInMATLAB(obj, name)
            obj.DisplayHelper.displayProperty(name, obj.Value);
        end
    end
end