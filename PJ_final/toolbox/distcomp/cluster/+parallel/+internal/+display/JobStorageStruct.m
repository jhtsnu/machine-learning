% JobStorageStruct - Used for property values which might be passed in as structs.
%currently only implemented for JobStorageLocation

% Copyright 2012 The MathWorks, Inc.

classdef (Hidden) JobStorageStruct < parallel.internal.display.DisplayableItem
    properties (SetAccess = immutable, GetAccess = private)
        Value
    end
    
    methods
        function obj = JobStorageStruct(displayHelper, value)
            obj@parallel.internal.display.DisplayableItem(displayHelper);
            obj.Value = value;
        end
        
        function displayInMATLAB(obj, name)
            import parallel.internal.display.DisplayableItem
            
            displayValue = obj.DisplayHelper.formatJobStorageLocation(obj.Value);
            obj.DisplayHelper.displayProperty(name, displayValue, DisplayableItem.DoNotFormatValue);
        end
    end
end