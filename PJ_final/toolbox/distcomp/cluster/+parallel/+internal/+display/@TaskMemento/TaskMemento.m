% TaskMemento - Used to construct task objects in order to display the task when the link is clicked in the job displayer

% Copyright 2012 The MathWorks, Inc.

classdef ( Hidden ) TaskMemento < parallel.internal.display.Memento
    
    properties ( SetAccess = immutable, GetAccess = private )
        ParentMemento
        ID
    end
    
    methods
        
        function obj = TaskMemento(task)
            validateattributes( task, {'parallel.Task'}, {} );
            obj.ParentMemento = parallel.internal.display.JobMemento(task.Parent);
            obj.ID = task.ID;
        end
        
        function t = createObjectFromMemento(obj)
            t = findTask(createObjectFromMemento(obj.ParentMemento), 'ID', obj.ID);
        end
        
    end
end