% BuiltInDisplayType - Used to format built-in types for display

% Copyright 2012 The MathWorks, Inc.

classdef ( Hidden ) BuiltInDisplayType < parallel.internal.display.DisplayType
    methods
        
        function obj = BuiltInDisplayType(displayValue)
            obj@parallel.internal.display.DisplayType(displayValue);
        end
        
        function displayText = char(obj)
            assert ( isscalar(obj), getString(message('parallel:cluster:CharOnVector')) );
            if (~ischar(obj.DisplayValue));
                error(message('parallel:cluster:DisplayUnexpectedFormatError'));
            end
            
            displayText = obj.DisplayValue;
        end
        
        function displayValueLength = length(obj)
            assert ( isscalar(obj), getString(message('parallel:cluster:LengthOnVector')) );
            displayValueLength = length(obj.DisplayValue);
        end
        
        function obj = formatDispatcher(obj, displayHelper, valDisplayLength, formatter)
            for i = 1:numel(obj)
                obj(i).DisplayValue = formatter(displayHelper,  obj(i).DisplayValue , valDisplayLength);
            end
        end
        
    end
end
