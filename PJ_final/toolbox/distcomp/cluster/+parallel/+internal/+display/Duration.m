% Duration - Used for determining running duration from job start time and job finish time.

% Copyright 2012 The MathWorks, Inc.

classdef (Hidden) Duration < parallel.internal.display.DisplayableItem
   
    properties (SetAccess = immutable, GetAccess = private)
        StartTimeValue
        FinishTimeValue
    end
    
    methods
    
        function obj = Duration(displayHelper, startTime, finishTime)
            obj@parallel.internal.display.DisplayableItem(displayHelper);
            obj.StartTimeValue = startTime;
            obj.FinishTimeValue = finishTime;
        end
        
        function displayInMATLAB(obj, name)
            runningDuration = obj.DisplayHelper.getRunningDuration(obj.StartTimeValue, obj.FinishTimeValue);
            obj.DisplayHelper.displayProperty(name, runningDuration);
        end
        
    end
    
end