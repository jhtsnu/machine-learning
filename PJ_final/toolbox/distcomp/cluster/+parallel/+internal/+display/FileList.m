
%   Copyright 2012 The MathWorks, Inc.

classdef ( Hidden ) FileList < parallel.internal.display.DisplayableItem

    properties ( Constant )
        % We do not want the list of files to get too long, so we replace the
        % list with a link to display the list if we have more than the maximum
        % number of items.
        MaxLengthFileList = 6;
    end

    properties ( SetAccess = immutable, GetAccess = private )
        Value
        LinkObj
    end

    methods

        function obj = FileList( displayHelper, list, linkObj )
            obj@parallel.internal.display.DisplayableItem( displayHelper );
            obj.Value = list;
            obj.LinkObj = linkObj;
        end

        function displayInMATLAB( obj, name )
            list = obj.Value;
            % Just display the list of files in the display if it is short enough,
            % or we are not showing links.
            if numel( list ) <= obj.MaxLengthFileList || ~obj.DisplayHelper.ShowLinks
                obj.DisplayHelper.displayProperty( name, list );
            else
                displayValue = obj.getDisplayValue( numel( list ) );
                % Create a link to list the files instead.
                link = obj.getDisplayLink( obj.LinkObj, displayValue );
                obj.DisplayHelper.displayProperty( name, link );
            end
        end

    end

    methods ( Abstract, Access = protected )
        value = getDisplayValue( obj, numFiles );
        link = getDisplayLink( obj, linkObj, displayValue );
    end

end
