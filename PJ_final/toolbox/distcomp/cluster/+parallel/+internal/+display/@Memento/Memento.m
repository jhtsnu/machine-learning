% Memento - The base class for mementos used in the object display

% Copyright 2012 The MathWorks, Inc.

classdef ( Hidden ) Memento
    
    methods ( Abstract )
        
        % All display types must have a method which guarantees to return
        % a char for display
        createObjectFromMemento(obj)
        
    end
    
    methods
        
        function serializedMemento = serialize(memento)
            % This method serializes data related to PCT objects and
            % compresses it for inclusion in hyperlinks in the object display.
            % For example, the PCT cluster object data is a struct containing
            % the type of cluster and a struct of sufficient PV pairs to make
            % a cluster object equivalent to the cluster being created when
            % this method is called.
            
            % Take some data related to the object and serialize it
            mementoxS = distcompserialize(memento);
            
            % Create a byte array output stream of the minimum possible size necessary
            % to contain the elements of the serialized data.
            byteArrayOutputStream = java.io.ByteArrayOutputStream( numel(mementoxS) );
            % Compress that stream by zipping it.
            zippedOutputStream = java.util.zip.GZIPOutputStream( byteArrayOutputStream );
            
            % Fill the zipped byte array output stream with the data, removing the
            % zeroes.
            zippedOutputStream.write( mementoxS, 0, numel(mementoxS) );
            zippedOutputStream.finish;
            zippedOutputStream.close;
            
            serializedMemento = sprintf( '%02X', typecast(byteArrayOutputStream.toByteArray, 'uint8') );
         
        end
    end
    
    methods( Static, Hidden )
        
        function memento = deserialize(serializedMemento)
            % This method uncompresses and deserializes the data related
            % to PCT objects in hyperlinks in the object display.
            % The PCT cluster object data is a struct containing the Type of cluster
            % object and a struct of sufficient PV pairs to make a cluster object
            % equivalent to the cluster being created when this method is called.
            
            
            hexStr = reshape(serializedMemento, 2, numel(serializedMemento)/2)';
            int8Data = typecast(uint8(hex2dec(hexStr)), 'int8');
            
            byteArrayInputStream = java.io.ByteArrayInputStream( int8Data );
            zippedInputStream = java.util.zip.GZIPInputStream( byteArrayInputStream );
            
            i = 1;
            mementoxS = uint8([]);
            while true
                x = zippedInputStream.read;
                if x < 0
                    break
                end
                mementoxS(i) = x;
                i = i + 1;
            end
            
            memento = distcompdeserialize( mementoxS );
        end
        
    end
end
