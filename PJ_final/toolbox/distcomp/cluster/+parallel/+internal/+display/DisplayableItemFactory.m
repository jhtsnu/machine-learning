% DisplayableItemFactory - Factory for creating different types of displayable items.

% Copyright 2012 The MathWorks, Inc.

classdef (Hidden) DisplayableItemFactory
    properties (SetAccess = 'immutable', GetAccess = 'private')
        DisplayHelper
    end
    methods
        function obj = DisplayableItemFactory(displayHelper)
            obj.DisplayHelper = displayHelper;
        end
    end
    methods
        
        function defaultItem = createDefaultItem(obj, value)
            defaultItem = parallel.internal.display.Default(obj.DisplayHelper, value);
        end

        function autoAttachedFilesItem = createAutoAttachedFilesItem(obj, job)
            autoAttachedFilesItem = parallel.internal.display.AutoAttachedFiles(obj.DisplayHelper, job);
        end

        function attachedFilesItem = createAttachedFilesItem(obj, list, job)
            attachedFilesItem = parallel.internal.display.AttachedFiles(obj.DisplayHelper, list, job);
        end

        function additionalPathsItem = createAdditionalPathsItem(obj, list, job)
            additionalPathsItem = parallel.internal.display.AdditionalPaths(obj.DisplayHelper, list, job);
        end
        
        function wrappedTextItem = createWrappedTextItem(obj, value)
            wrappedTextItem = parallel.internal.display.WrappedText(obj.DisplayHelper, value);
        end
        
        function jobStorageStructItem = createJobStorageStructItem(obj, value)
            jobStorageStructItem = parallel.internal.display.JobStorageStruct(obj.DisplayHelper, value);
        end
        
        function linkItem = createLinkItem(obj, displayValue, linkObj) 
            linkItem = parallel.internal.display.Link(obj.DisplayHelper, displayValue, linkObj);
        end
        
        % Calculating the running duration for a job or task
        function durationItem = createDurationItem(obj, startTime, finishTime)
            durationItem = parallel.internal.display.Duration(obj.DisplayHelper, startTime, finishTime);
        end
        
        function taskErrorItem = createTaskErrorItem(obj, error, parent)
            taskErrorItem = parallel.internal.display.TaskError(obj.DisplayHelper, error, parent);
        end
        
        % Separators do not have a typical display value. Instead, they
        % separate property groups.
        function separatorItem = createSeparator(obj)
            separatorItem = parallel.internal.display.Separator(obj.DisplayHelper);
        end
        
        % This function is called by the objects to make multiple displayable items
        function values = makeMultipleItems(obj, creator, x)
            % The creator must be a handle to a function in this class. 
            % If NOT then the  behaviour of this function is undefined. 
            values = cellfun(@(x) creator(obj, x), x, 'UniformOutput', false);
        end
    end
end
