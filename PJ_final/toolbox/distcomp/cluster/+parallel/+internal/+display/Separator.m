% Separator - Adds a separator between different property groups in the PCT object display.

% Copyright 2012 The MathWorks, Inc.

classdef (Hidden) Separator < parallel.internal.display.DisplayableItem
    
    methods
        
        function obj = Separator(displayHelper)
            obj@parallel.internal.display.DisplayableItem(displayHelper);
        end
        
        function displayInMATLAB(obj, ~)
            obj.DisplayHelper.displayPropertyGroupSeparator();
        end
        
    end
    
end