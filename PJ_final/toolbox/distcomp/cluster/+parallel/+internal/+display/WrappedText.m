% WrappedText - Used to display property values whose text needs to be wrapped.

% Copyright 2012 The MathWorks, Inc.

classdef (Hidden) WrappedText < parallel.internal.display.DisplayableItem
    properties (SetAccess = immutable, GetAccess = private)
        Value
    end
    
    methods
        function obj = WrappedText(displayHelper, value)
            obj@parallel.internal.display.DisplayableItem(displayHelper);
            obj.Value = value;
        end
        
        function displayInMATLAB(obj, name)
            import parallel.internal.display.DisplayableItem
            
            % Wrap the text so that we do not lose any information
            wrappedText = obj.DisplayHelper.wrapText(obj, obj.Value);
            displayValue = obj.DisplayHelper.formatCellStr(wrappedText);
            obj.DisplayHelper.displayProperty(name, displayValue , DisplayableItem.DoNotFormatValue);
        end
    end
end
