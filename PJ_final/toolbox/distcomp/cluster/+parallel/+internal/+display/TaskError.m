% TaskError - Used for formatting the error message in the Task display

% Copyright 2012 The MathWorks, Inc.

classdef (Hidden) TaskError < parallel.internal.display.DisplayableItem
    properties (SetAccess = immutable, GetAccess = private)
        Error
        Parent
    end
    
    methods
        function obj = TaskError(displayHelper, error, parent)
            obj@parallel.internal.display.DisplayableItem(displayHelper);
            obj.Error = error;
            obj.Parent = parent;
        end
        
        function displayInMATLAB(obj, ~)
           import parallel.internal.display.DisplayableItem
            
            if isempty(obj.Error)
                obj.DisplayHelper.displayProperty('ErrorIdentifier', '', DisplayableItem.DoNotFormatValue);
                obj.DisplayHelper.displayProperty('ErrorMessage',    '', DisplayableItem.DoNotFormatValue);
                return
            end
            
            obj.DisplayHelper.displayProperty('ErrorIdentifier', obj.Error.identifier, DisplayableItem.DoNotFormatValue);
            errMessage = obj.DisplayHelper.wrapText(obj.Error.message);
            errMessage = obj.DisplayHelper.formatCellStr(errMessage);
            obj.DisplayHelper.displayProperty('ErrorMessage', errMessage, DisplayableItem.DoNotFormatValue);
            
            if isempty(obj.Error.stack)
                return;
            end
            
            stackString = getErrorStackCellStr(obj);
            errStack = obj.DisplayHelper.formatCellStr(stackString);
            obj.DisplayHelper.displayProperty('Error Stack', errStack, DisplayableItem.DoNotFormatValue);
        end
        
        % Everything below is about handling the task errors 
        function stackString = getErrorStackCellStr(obj)
            import parallel.internal.display.Displayer;
            % Display the stack, hotlinking as required
            numStacks = numel(obj.Error.stack);
            stackString = cell(numStacks, 1);
            jobFiles = {};
            jobFilenames = {};
            for ii = 1:numStacks
                currStack = obj.Error.stack(ii);
                [~, erroredFilename] = fileparts(currStack.file);
                
                if iIsErrorStackFrameFromWorker(erroredFilename, currStack.name);
                    break;
                end
                
                if strcmp(currStack.name, erroredFilename)
                    fileDisplayName = erroredFilename;
                else
                    fileDisplayName = sprintf('%s>%s', erroredFilename, currStack.name);
                end
                stackString{ii} = sprintf('%s (line %d)', fileDisplayName , currStack.line);
                
                if obj.DisplayHelper.ShowLinks
                    if isempty(jobFiles) && isempty(jobFilenames)
                        % We'll need to show links to error stack frames, but preferably not
                        % from the temporary AttachedFiles location, so work out
                        % what was supplied with the job in the AttachedFiles and AdditionalPaths.
                        jobFilesAndPaths = [obj.Parent.AttachedFiles; obj.Parent.AdditionalPaths];
                        [jobFiles, jobFilenames] = iGetAllMATLABFiles(jobFilesAndPaths);
                    end
                    
                    % If there is no file to open, because, for example,
                    % the task is using an anonymous function, we should 
                    % not link to anything. 
                    if isempty( erroredFilename )   
                        continue;
                    end
                    
                    % If the file was supplied in the AttachedFiles / AdditionalPaths
                    % then use that as the link (so we have a hope of referencing it
                    % correctly on the client, otherwise use the actual
                    % file in the error stack.
                    matchingFile = find(strcmpi(jobFilenames, erroredFilename), 1, 'first');
                    if isempty(matchingFile)
                        linkFile = currStack.file;
                    else
                        linkFile = jobFiles{matchingFile};
                    end
                    stackString{ii} = sprintf('<a href="matlab: opentoline(''%s'', %d, 0)">%s</a>', ...
                        linkFile, currStack.line, stackString{ii});
                end
            end
            % Remove empty strings from the stack string cell array
            stackString(cellfun(@isempty, stackString)) = [];
            if isempty( stackString )
                % Get here if the user calls a function incorrectly
                stackString = {'<no stack>'};
            end
        end
    end
end

function isWorkerFrame = iIsErrorStackFrameFromWorker(file, fcn)
    % Current PCT Related stack frames
    %
    % For all schedulers:
    % File                        Function
    % evaluateWithNoErrors        evaluateWithNoErrors
    % NullEvaluator               evaluate
    % EvalcEvaluator              evaluate
    %
    % For file-based schedulers:
    % File                        Function
    % CJSStreamingEvaluator       evaluate
    % CapturingEvaluator          evaluate
    % dctEvaluateFunction         iEvaluateWithNoErrors
    % dctEvaluateFunction         dctEvaluateFunction
    % dctEvaluateTask             iEvaluateTask/nEvaluateTask
    % dctEvaluateTask             iEvaluateTask
    % dctEvaluateTask             dctEvaluateTask
    % distcomp_evaluate_filetask  iDoTask
    % distcomp_evaluate_filetask  distcomp_evaluate_filetask
    %
    % For MJS:
    % File                        Function
    % MJSStreamingEvaluator       evaluate
    % dctEvaluateFunction         iEvaluateWithNoErrors
    % dctEvaluateFunction         dctEvaluateFunction
    % dctEvaluateTask             iEvaluateTask/nEvaluateTask
    % dctEvaluateTask             iEvaluateTask
    % dctEvaluateTask             dctEvaluateTask
    % distcomp_evaluate_task      iDoTask
    % distcomp_evaluate_task      distcomp_evaluate_task
    %
    % For batch:
    % parallel.internal.cluster.executeScript.m
    % parallel.internal.cluster.executeFunction.m
    %
            
    isEvalNoErrorsFcn = strcmp(file, 'evaluateWithNoErrors') && ...
        (strcmp(fcn, 'evaluateWithNoErrors'));
    isEvaluator = (strcmp(file, 'NullEvaluator') || strcmp(file, 'EvalcEvaluator') || ...
        strcmp(file, 'CJSStreamingEvaluator') || strcmp(file, 'CapturingEvaluator') || ...
        strcmp(file, 'MJSStreamingEvaluator')) ...
        && strcmp(fcn, 'evaluate');
    isDctEvaluateFcn = strcmp(file, 'dctEvaluateFunction') && ...
        (strcmp(fcn, 'iEvaluateWithNoErrors') || strcmp(fcn, 'dctEvaluateFunction'));
    isDctEvaluateTask = strcmp(file, 'dctEvaluateTask') && ...
        (strcmp(fcn, 'iEvaluateTask/nEvaluateTask') || strcmp(fcn, 'iEvaluateTask') || strcmp(fcn, 'dctEvaluateTask'));
    isDistcompEvaluateFiletask = strcmp(file, 'distcomp_evaluate_filetask') && ...
        (strcmp(fcn, 'iDoTask') || strcmp(fcn, 'distcomp_evaluate_filetask'));
    isDistcompEvaluateTask = strcmp(file, 'distcomp_evaluate_task') && ...
        (strcmp(fcn, 'iDoTask') || strcmp(fcn, 'distcomp_evaluate_task'));
    isBatch = strcmp(file, 'executeScript') && strcmp(fcn, 'executeScript') || ...
        strcmp(file, 'executeFunction') && strcmp(fcn, 'executeFunction');
    
    isWorkerFrame = isEvalNoErrorsFcn || isEvaluator || isDctEvaluateFcn || ...
        isDctEvaluateTask || isDistcompEvaluateFiletask || ...
        isDistcompEvaluateTask || isBatch;
end
        
function [fullFilePaths, filenames] = iGetAllMATLABFiles(allLocations)
    fullFilePaths = {};
    for ii = 1:numel(allLocations)
        currLocation = allLocations{ii};
        if exist(currLocation, 'dir')
            relevantContents = what(currLocation);
            % We need to correctly concatenate all the contents of the
            % output of what together. So firstly get the outputs as a cell
            % array of vertical cell arrays, then concatenate that
            % together. The assumption below is that whatever structure
            % array is given to the arrayfun call below it will have a '.m'
            % field to dereference.
            namesCell =  arrayfun( @(x) x.m(:), relevantContents, 'UniformOutput' , false);
            names = vertcat(namesCell{:});
            fullMFilePaths = cellfun(@(x) fullfile(currLocation, x), names, ...
                'UniformOutput', false);
            fullFilePaths = [fullFilePaths; fullMFilePaths]; %#ok<AGROW>
            % TODO - can we bothered to traverse the packages too?
        elseif exist(currLocation, 'file')
            fullFilePaths = [fullFilePaths; currLocation]; %#ok<AGROW>
        end
    end
    % Get just the filename as well
    [~, filenames] = cellfun(@fileparts, fullFilePaths, 'UniformOutput', false);
end
