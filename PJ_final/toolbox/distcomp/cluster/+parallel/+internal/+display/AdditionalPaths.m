
%   Copyright 2012 The MathWorks, Inc.

classdef (Hidden) AdditionalPaths < parallel.internal.display.FileList

    methods

        function obj = AdditionalPaths(displayHelper, list, job)
            obj@parallel.internal.display.FileList(displayHelper, list, job);
        end

    end

    methods ( Access = protected )
        function value = getDisplayValue( ~, numPaths )
            value = getString( message( 'parallel:job:AdditionalPathsLink', numPaths ) );
        end

        function link = getDisplayLink( obj, job, displayValue )
            if obj.DisplayHelper.ShowLinks
                matlabFunction = sprintf( ...
                    'parallel.internal.display.AdditionalPaths.displayAdditionalPaths(''%s'')', ...
                    serialize( parallel.internal.display.JobMemento( job ) ) );
                link = parallel.internal.display.HTMLDisplayType( displayValue, matlabFunction );
            else
                link = parallel.internal.display.HTMLDisplayType( displayValue );
            end
        end
    end

    methods ( Static )
        function displayAdditionalPaths( serializedMemento )
            try
                job = createObjectFromMemento( ...
                    parallel.internal.display.Memento.deserialize( serializedMemento ) );
                disp( getString( message( 'parallel:job:AdditionalPathsList', job.ID, ...
                    sprintf( '  %s\n', job.AdditionalPaths{:} ) ) ) );
            catch err
                dctSchedulerMessage( 1, ...
                    'Failed to create Job from memento, original error: %s', err.getReport() );
                if feature( 'hotlinks' )
                    helpString = '<a href="matlab: help parallel.Job">help parallel.Job</a>';
                else
                    helpString = 'help parallel.Job';
                end
                error( message( 'parallel:cluster:LinkDispFailure', ...
                            'AdditionalPaths', getString( message( 'parallel:cluster:Job' ) ), helpString ) );
            end
        end
    end
end
