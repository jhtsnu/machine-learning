% Displayer - base class handling common display functionality

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) Displayer < handle
    properties (Abstract, SetAccess = immutable, GetAccess = protected)
        DisplayHelper
    end
    properties ( GetAccess = protected, SetAccess = immutable )
        Class
        ClassDisplayName
        RequiredType
        ShowLinks = feature( 'hotlinks' );
    end
    properties ( Constant, GetAccess = protected )
        DeletedString = 'deleted';
    end
    properties ( Constant )
        DisplayObjectFunction = 'parallel.internal.display.Displayer.displayObject'
    end
    methods
        function obj = Displayer( someObj, requiredType )
            obj.RequiredType = requiredType;
            obj.errorIfWrongType( someObj );
            obj.Class = class( someObj );
            displayName = regexp(requiredType, '\.', 'split');
            obj.ClassDisplayName = displayName{end};
        end
        function doDisplay( obj, toDisp, name )
            obj.errorIfWrongType( toDisp );
            obj.displayInputName( name );
            obj.doDisp( toDisp );
        end
        
        function doDisp( obj, toDisp )
            import parallel.internal.display.Displayer;
            obj.errorIfWrongType( toDisp );
            if numel( toDisp ) == 0
                obj.displayEmptyObject( toDisp );
            elseif numel( toDisp ) == 1
                obj.doSingleDisplay( toDisp );
            else
                obj.doVectorDisplay( toDisp );
            end
            isLoose = Displayer.isLooseSpacing();
            if isLoose
                fprintf( '\n' );
            end
        end
    end
    methods ( Abstract, Access = protected )
        doSingleDisplay( obj, toDisp );
        doVectorDisplay( obj, toDisp );
    end
    
    methods ( Sealed, Static, Access = protected )
        function displayInputName( name )
            import parallel.internal.display.Displayer;
            isLoose = Displayer.isLooseSpacing();
            if isLoose
                fprintf( '\n' );
            end
            if isempty( name )
                fprintf('ans = \n');
            else
                fprintf( '%s = \n', name );
            end
            if isLoose
                fprintf( '\n' );
            end
        end
        function tf = isLooseSpacing()
            tf = strcmp( get( 0, 'FormatSpacing' ), 'loose' );
        end
    end
    
    methods ( Static, Access = protected )
        function propStruct = getDisplayPropsAsStruct( toDisp, propNames, varargin )
            % Gets the specified properties from the toDisp object and
            % returns them as a struct whose fieldnames are the property
            % names.
            % Default version of getDisplayPropsAsStruct ignores the varargin.
            vals = toDisp.hGetDisplayPropertiesNoError( propNames );
            propStruct = cell2struct( vals, propNames, 2 );
        end
    end
    
    methods (Sealed, Access = protected)
        function classDocLink = formatDocLink(obj, fullyQualifiedClassNameString)
            
            displayNameArray = regexp( fullyQualifiedClassNameString, '\.', 'split' );
            displayName = displayNameArray{end};
          
            if obj.ShowLinks
                % Use the specific subclass name to build the hyperlink to
                % class documentation if we haven't been told otherwise
                classDocLink = sprintf('<a href="matlab: help %s">%s</a>', fullyQualifiedClassNameString, displayName);
            else
                classDocLink = displayName;
            end
        end
    end
    
    methods ( Access = protected )
        function displayEmptyObject( obj, toDisp )
            dimensionString = obj.DisplayHelper.formatEmptyDimension( size( toDisp ), obj.formatDocLink(obj.Class) ); 
            fprintf('%s\n', dimensionString);
        end
        
        function errorIfWrongType( obj, toDisp )
            if ~isa( toDisp, obj.RequiredType )
                error(message('parallel:job:DisplayExpectedDifferentType', obj.RequiredType, class( toDisp )));
            end
        end
        
        function displaySpecificItems(obj, toDisp)
            % Gets the specified properties from the toDisp object and
            % displays them.
            if ~isscalar(toDisp)
                error(message('parallel:cluster:DisplaySpecificItemsNotScalar'));
            end
            diFactory = parallel.internal.display.DisplayableItemFactory(obj.DisplayHelper);
            [objectPropertyMap, propNames] = toDisp.hGetDisplayItems(diFactory);
            if isempty(propNames)
                return;
            end
            for ii = 1:numel(propNames)
                displayValue = objectPropertyMap(propNames{ii});
                displayValue.displayInMATLAB(propNames{ii});
            end
        end
        
        function link = hGetLink(obj, toDisp,  displayValue)
            if obj.ShowLinks
                link = hGetLink(toDisp,  displayValue);
            else
                % Getting a link will always return an HTMLDisplayType,
                % but if we are not building a hyperlink, we do not
                % include the matlab command.
                link = parallel.internal.display.HTMLDisplayType(displayValue);
            end
        end
    end
    
    methods( Static )
        % Display object when the hyperlink is clicked in the display.
        function displayObject(serializedMemento)
            try
                disp(createObjectFromMemento(parallel.internal.display.Memento.deserialize(serializedMemento)))
            catch err
                if feature( 'hotlinks')
                    helpString1 = '<a href="matlab: help parallel.Cluster">parallel.Cluster</a>';
                    helpString2 = '<a href="matlab: help parallel.Job">parallel.Job</a>';
                    helpString3 = '<a href="matlab: help parallel.Task">parallel.Task</a>';
                else
                    helpString1 =  'parallel.Cluster';
                    helpString2 =  'parallel.Job';
                    helpString3 =  'parallel.Task';
                end
                error(message('parallel:cluster:BasicLinkDispFailure', helpString1, helpString2, helpString3));
            end
        end
    end
end
