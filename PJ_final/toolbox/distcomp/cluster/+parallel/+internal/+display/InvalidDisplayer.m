% InvalidDisplayer - Displayer for deleted handles

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) InvalidDisplayer < parallel.internal.display.Displayer
    properties (SetAccess = immutable, GetAccess = protected) % implementation for abstract Displayer properties
        DisplayHelper = parallel.internal.display.DisplayHelper( length('') )
    end

    methods
        function obj = InvalidDisplayer()
            % Pretend that the required type is a double (because
            % we are passing in []).  Note that this 'double' is never
            % checked because this class explicitly overrides doDisplay
            % and doDisp.
            obj@parallel.internal.display.Displayer( [], 'double' );
        end
        function doDisplay( ~, ~, name )
            disp( name );
            disp( getString(message('parallel:cluster:DeletedObject')) );
        end
        function doDisp( ~, ~, ~ )
            disp( getString(message('parallel:cluster:DeletedObject')) );
        end
    end

    methods( Access = protected )
        function doSingleDisplay( ~, ~ )
        end
        function doVectorDisplay( ~, ~ )
        end
    end
    
end
