% Validator - Class to hold information about the validation status for each profile

% Copyright 2011-2012 The MathWorks, Inc.

classdef (Hidden, Sealed) Validator < parallel.internal.settings.AbstractValidator
    properties (Constant)
        % Default status for new profiles
        DefaultStatus = parallel.internal.types.ValidationStatus.NotRun;
    end

    properties (Constant, GetAccess = private)
        % Stages of validation
        ParclusterStageName = 'parcluster';
        MatlabpoolStageName = 'matlabpool';
        % The job types used in the non-interactive job stages
        JobStageTypes = [parallel.internal.types.Variant.IndependentJob, ...
                        parallel.internal.types.Variant.CommunicatingSPMDJob, ...
                        parallel.internal.types.Variant.CommunicatingPoolJob];
        % Do failures in the non-interactive job stages terminate testing?
        % Allow a Communicating SPMD job to run even if independent failed
        % because we may get some interesting messages from the communicating
        % job.
        JobStageFailTerminateTesting = [false, true, true];
        
        MdceDebugVarName = 'MDCE_DEBUG';
    end

    properties (GetAccess = private, SetAccess = immutable)
        % The actual map where the validation status is stored
        StatusMap
    end
    
    properties (Access = private)
        IsCurrentlyValidating = false
        CurrentlyValidatingProfileName
        HasUserCancelledFcn = []
    end
    
    properties (Dependent, Access = private)
        IsUserCancelled
    end
    
    methods
        function value = get.IsUserCancelled(obj)
            if isempty(obj.HasUserCancelledFcn)
                value = false;
            else
                value = obj.HasUserCancelledFcn();
            end
        end
        
        function obj = Validator()
            obj.StatusMap = containers.Map();
        end
    end
    
    methods % Implementation of AbstractValidator abstract methods
        function status = getStatus(obj, profileName)
        % status = getStatus(obj, profileName) - get the status for the specified
        % profile
            import parallel.internal.validator.Validator
            validateattributes(profileName, {'char'}, {'row'});
            
            % If the profileName isn't in the map, then it must be
            % a new profile, so bung it in with status NotRun
            if ~obj.StatusMap.isKey(profileName)
                obj.StatusMap(profileName) = Validator.DefaultStatus;
            end
            status = obj.StatusMap(profileName);
        end

        function setStatus(obj, profileName, value)
        % setStatus(obj, profileName, value) - set the status for the specified
        % profile to the specified value
            import parallel.internal.validator.Validator
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(value, {'parallel.internal.types.ValidationStatus'}, {'scalar'});
            obj.StatusMap(profileName) = value;            
        end
        
        function deleteStatus(obj, profileName)
        % deleteStatus(obj, profileName) - remove the specified profile from the 
        % cache
            validateattributes(profileName, {'char'}, {'row'});
            if obj.StatusMap.isKey(profileName)
                obj.StatusMap.remove(profileName);
            end
        end
        
        function validate(obj, profileName)
        % validate(obj, profileName) - validate the specified profile
            import parallel.internal.validator.Validator
            import parallel.internal.types.Variant
            import parallel.internal.types.ValidationStatus
            validateattributes(profileName, {'char'}, {'row'});
           
            restoreEnvironmentObj = iSetEnvironmentForValidation();  %#ok<NASGU> - keep hold of cleanup object

            % Set the status to stopped if anything goes wrong.
            cleanupObj = onCleanup(@() obj.setCurrentlyValidatingProfileToStopped);

            % Actually run the validation
            obj.startValidation(profileName);
            hasPassed = obj.runValidationStages(profileName);
            if obj.IsUserCancelled
                obj.setCurrentlyValidatingProfileToStopped();
            else
                % Set the final validation status
                obj.finishValidation(profileName, hasPassed);
            end
        end
    end
    
    methods
        function setUserCancelledFunction(obj, fcn)
            if ~isempty(fcn)
                validateattributes(fcn, {'function_handle'}, {'scalar'});
            end
            
            obj.HasUserCancelledFcn = fcn;
        end
    end
    
    methods (Access = private)
        function setCurrentlyValidatingProfileToStopped(obj)
        % Set the currently validating profile's status to stopped
            import parallel.internal.types.ValidationStatus
            if obj.IsCurrentlyValidating
                profileName = obj.CurrentlyValidatingProfileName;
                status = ValidationStatus.Stopped;
                obj.StatusMap(profileName) = status;
                obj.resetCurrentlyValidating();
                obj.notifyValidationFinished(profileName, status);
            end
        end
        
        function setCurrentlyValidating(obj, profileName)
        % Store the details of the currently validating profile
            obj.IsCurrentlyValidating = true;
            obj.CurrentlyValidatingProfileName = profileName;
        end
        
        function resetCurrentlyValidating(obj)
        % Remove the details of the currently validating profile
            obj.IsCurrentlyValidating = false;
            obj.CurrentlyValidatingProfileName = '';
        end

        function startValidation(obj, profileName)
        % set the validation status to running
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus
            % Set the overall status to running
            status = ValidationStatus.Running;
            obj.StatusMap(profileName) = status;
            % Make sure we know we are currently validating
            obj.setCurrentlyValidating(profileName);
            
            eventData = ValidationEventData.createOverallValidationStatus(profileName, status);
            obj.notify('ValidationStarted', eventData);
        end
        
        function finishValidation(obj, profileName, hasPassed)
        % set the final validation status to passed or failed as appropriate
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.types.ValidationStatus
            
            if hasPassed
                status = ValidationStatus.Passed;
            else
                status = ValidationStatus.Failed;
            end
            obj.StatusMap(profileName) = status;
            obj.resetCurrentlyValidating();

            obj.notifyValidationFinished(profileName, status);
        end
        
        function hasPassed = runValidationStages(obj, profileName)
        % Actually run the full validation
            import parallel.internal.validator.Validator
            validateattributes(profileName, {'char'}, {'row'});

            % The parcluster stage
            [cluster, hasPassed] = obj.runFindClusterStage(profileName);
            if obj.IsUserCancelled
                return
            end
            % Store the job stage names in a variable because {Validator.JobStageTypes.Name}
            % doesn't gives you what you expect....
            jobStages = Validator.JobStageTypes;
            remainingStageNames = [{jobStages.Name}, Validator.MatlabpoolStageName];
            if ~hasPassed
                % Bail on the first stage that errors
                cellfun(@(x) obj.notifySkipStage(profileName, x), remainingStageNames);
                return;
            end
            % The non-interactive job stages
            for ii = 1:numel(Validator.JobStageTypes)
                currStage = Validator.JobStageTypes(ii);
                if ~cluster.hIsVariantSupported(currStage)
                    % Notify that this stage is being skipped, but don't
                    % change hasPassed in any way.
                    % TODO:later remove when Mpiexec is removed.
                    obj.notifySkipStageNotSupported(profileName, currStage.Name);
                    continue;
                end
                currStagePassed = obj.runJobStage(profileName, cluster, currStage);
                if obj.IsUserCancelled
                    return;
                end
                hasPassed = hasPassed && currStagePassed;
                remainingStageNames = setdiff(remainingStageNames, currStage.Name);
                if Validator.JobStageFailTerminateTesting(ii) && ~currStagePassed
                    cellfun(@(x) obj.notifySkipStage(profileName, x), remainingStageNames);
                    return;
                end
            end
            % The interactive matlabpool stage
            hasPassed = obj.runInteractiveMatlabpool(profileName, cluster) && hasPassed;
        end
        
        function [cluster, stagePassed] = runFindClusterStage(obj, profileName)
        % Run the parcluster stage
            import parallel.internal.validator.Validator
            validateattributes(profileName, {'char'}, {'row'});
            
            stagePassed = false;
            stageName = Validator.ParclusterStageName;
            cluster = [];
            obj.notifyStartStage(profileName, stageName);
            try
                parclusterFcn = @() parcluster(profileName); %#ok<NASGU> - used in evalc
                [commandWindowOutput, cluster] = evalc('parclusterFcn()');
                lastWarnMsg = lastwarn();
                if ~isempty(lastWarnMsg)
                    msg = getString(message('parallel:validation:ParclusterWarning', lastWarnMsg));
                    obj.notifyFailStageWithMessage(profileName, stageName, msg);
                else
                    obj.notifyPassStage(profileName, stageName, commandWindowOutput);
                    stagePassed = true;
                end
            catch err
                obj.notifyFailStageErrorThrown(profileName, stageName, err);
            end
        end
        
        function stagePassed = runJobStage(obj, profileName, cluster, jobVariant)
        % Run a job stage
            import parallel.internal.types.States
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(cluster, {'parallel.Cluster'}, {'scalar'});
            validateattributes(jobVariant, {'parallel.internal.types.Variant'}, {'scalar'});

            stagePassed = false;
            stageName = jobVariant.Name;
            obj.notifyStartStage(profileName, stageName);
            try
                [commandWindowOutput, job] = evalc('iCreateAndSubmitJob(cluster, jobVariant);');
                cleanup = onCleanup(@() job.delete());
            catch err
                obj.notifyFailStageErrorThrown(profileName, stageName, err);
                return;
            end
            
            try
                jobFinished = false;
                while ~obj.IsUserCancelled && ~jobFinished
                    [waitOutput, jobFinished] = evalc('job.wait(''finished'', 1);');
                    commandWindowOutput = [commandWindowOutput, waitOutput]; %#ok<AGROW>
                end
                if obj.IsUserCancelled
                    return
                end
            catch err
                [commandWindowOutput, debugLog] = iGetDebugLogNoError(job, commandWindowOutput);
                obj.notifyFailStageErrorThrown(profileName, stageName, err, commandWindowOutput, debugLog);
                return;
            end

            % Get the debug log as soon as the job is finished as it may provide extra 
            % information if anything else goes wrong later.
            [commandWindowOutput, debugLog] = iGetDebugLogNoError(job, commandWindowOutput);
            
            try
                allTaskErrors  = {job.Tasks.Error};
                taskErrors     = allTaskErrors(~cellfun(@isempty, allTaskErrors));
                
                if job.StateEnum == States.Finished && isempty(taskErrors)
                    [fetchOutput, outArgs] = evalc('job.fetchOutputs();');
                    commandWindowOutput = [commandWindowOutput, fetchOutput];
                    emptyOutArgs  = any(cellfun(@isempty, outArgs(:)));
                    if emptyOutArgs
                        obj.notifyFailJobStageEmptyOutputs(profileName, stageName, commandWindowOutput, ...
                            debugLog);
                    else
                        obj.notifyPassStage(profileName, stageName, commandWindowOutput);
                        stagePassed = true;
                    end
                else
                    obj.notifyFailUnsuccessfulJobStage(profileName, stageName, commandWindowOutput, ...
                        taskErrors, debugLog);
                end
            catch err
                obj.notifyFailStageErrorThrown(profileName, stageName, err, commandWindowOutput, debugLog);
            end
        end
        
        function stagePassed = runInteractiveMatlabpool(obj, profileName, cluster)
        % Run the interactive pool stage
            import parallel.internal.cluster.MatlabpoolHelper
            import parallel.internal.validator.Validator
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(cluster, {'parallel.Cluster'}, {'scalar'});

            stagePassed = false;
            stageName = Validator.MatlabpoolStageName;
            obj.notifyStartStage(profileName, stageName);
            client = parallel.internal.pool.SessionManager.getSessionObject();
            matlabpoolRunning = client.isPossiblyRunning();
            if matlabpoolRunning
                obj.notifyFailPoolAlreadyRunning(profileName);
                return;
            end
            
            % Don't use the matlabpool method or function here as that may throw an 
            % error telling you to run validation.
            openMatlabpoolFcn = @() MatlabpoolHelper.openMatlabpoolForScheduler(cluster); %#ok<NASGU> - used in evalc
            try
                commandWindowOutput = evalc('openMatlabpoolFcn()');
                client = parallel.internal.pool.SessionManager.getSessionObject();
                job = client.ParallelJob;
            catch err
                obj.notifyFailStageErrorThrown(profileName, stageName, err);
                return;
            end
            cleanup = onCleanup(@() iMatlabpoolCleanup(profileName));
            
            if obj.IsUserCancelled
                return
            end

            try
                spmdFcn = @() iDoSpmd(); %#ok<NASGU> - used in evalc
                [spmdOutput, hostnames] = evalc('spmdFcn()');
                commandWindowOutput = [commandWindowOutput, spmdOutput];
            catch err
                [commandWindowOutput, debugLog] = iGetDebugLogNoError(job, commandWindowOutput);
                obj.notifyFailStageErrorThrown(profileName, stageName, err, commandWindowOutput, debugLog);
                return;
            end

            % Get the debug log here in case anything goes wrong with closing the pool
            [commandWindowOutput, debugLog] = iGetDebugLogNoError(job, commandWindowOutput);
            
            try
                closeMatlabpoolFcn = @() matlabpool('close', 'force', profileName); %#ok<NASGU> - used in evalc
                closePoolOutput = evalc('closeMatlabpoolFcn()');
                commandWindowOutput = [commandWindowOutput, closePoolOutput];
                
                emptyOutArgs = any(cellfun(@isempty, hostnames));
                if emptyOutArgs
                    obj.notifyFailJobStageEmptyOutputs(profileName, stageName, commandWindowOutput, ...
                        debugLog);
                else
                    obj.notifyPassStage(profileName, stageName, commandWindowOutput);
                    stagePassed = true;
                end
            catch err
                obj.notifyFailStageErrorThrown(profileName, stageName, err, commandWindowOutput, debugLog);
            end
        end        
       
        function notifyStartStage(obj, profileName, stageName)
        % notify that a validation stage has started
            import parallel.internal.settings.ValidationEventData
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
        
            eventData = ValidationEventData.createStageRunning(profileName, stageName);
            obj.notify('ValidationStageUpdated', eventData);
        end
        
        function notifyPassStage(obj, profileName, stageName, commandWindowOutput)
        % notify that a validation stage has passed
            import parallel.internal.settings.ValidationEventData
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
            validateattributes(commandWindowOutput, {'char'}, {});

            eventData = ValidationEventData.createStagePassed(...
                profileName, stageName, commandWindowOutput);
            obj.notify('ValidationStageUpdated', eventData);        
        end
        
        function notifyFailStageErrorThrown(obj, profileName, stageName, err, commandWindowOutput, ...
            debugLog)
        % notify that a stage failed because an error was thrown
            import parallel.internal.settings.ValidationEventData
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
            validateattributes(err, {'MException'}, {'scalar'});
            
            if nargin < 5
                commandWindowOutput = '';
            else
                validateattributes(commandWindowOutput, {'char'}, {'row'});
            end
            if nargin < 6
                debugLog = '';
            else
                validateattributes(debugLog, {'char', 'cell'}, {'row'});
            end
            
            eventData = ValidationEventData.createStageFailedErrorThrown(...
                profileName, stageName, err, commandWindowOutput, debugLog);
            obj.notify('ValidationStageUpdated', eventData);
        end
        
        function notifyFailJobStageEmptyOutputs(obj, profileName, stageName, commandWindowOutput, ...
            debugLog)
        % notify that a job stage has failed because the job returned unexpected empty 
        % output arguments

            import parallel.internal.settings.ValidationEventData
            import parallel.internal.validator.Validator
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
            validateattributes(commandWindowOutput, {'char'}, {});
            validateattributes(debugLog, {'char', 'cell'}, {'row'});
            
            description = getString(message('parallel:validation:JobFinishedEmptyOutputs'));
            taskErrors = {};
            eventData = ValidationEventData.createStageUnsuccessful(profileName, stageName, ...
                description, commandWindowOutput, taskErrors, debugLog);
            obj.notify('ValidationStageUpdated', eventData);
        end
        
        function notifyFailUnsuccessfulJobStage(obj, profileName, stageName, commandWindowOutput, ...
            taskErrors, debugLog)
        % notify that a job stage has failed because the job did not run 
        % as expected
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.validator.Validator
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
            validateattributes(commandWindowOutput, {'char'}, {});
            validateattributes(taskErrors, {'cell'}, {});

            description = getString(message('parallel:validation:JobErrorOrNotFinished'));
            eventData = ValidationEventData.createStageUnsuccessful(profileName, stageName, ...
                description, commandWindowOutput, taskErrors, debugLog);
            obj.notify('ValidationStageUpdated', eventData);
        end
        
        function notifyFailStageWithMessage(obj, profileName, stageName, msg)
            import parallel.internal.settings.ValidationEventData
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
            validateattributes(msg, {'char'}, {});

            eventData = ValidationEventData.createStageFailed(...
                profileName, stageName, msg);
            obj.notify('ValidationStageUpdated', eventData);
        end

        function notifyFailPoolAlreadyRunning(obj, profileName)
        % fail the interactive matlabpool stage if a pool is already running
            import parallel.internal.settings.ValidationEventData
            import parallel.internal.validator.Validator
            description = getString(message('parallel:validation:PoolAlreadyOpen'));
            eventData = ValidationEventData.createStageFailed(...
                profileName, Validator.MatlabpoolStageName, description);
            obj.notify('ValidationStageUpdated', eventData);
        end
        
        function notifySkipStage(obj, profileName, stageName)
        % notify that a stage was skipped
            import parallel.internal.settings.ValidationEventData
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
            
            eventData = ValidationEventData.createStageSkipped(profileName, stageName);
            obj.notify('ValidationStageUpdated', eventData);
        end
        
        function notifySkipStageNotSupported(obj, profileName, stageName)
        % notify that a stage was skipped because it isn't supported
            import parallel.internal.settings.ValidationEventData
            validateattributes(profileName, {'char'}, {'row'});
            validateattributes(stageName, {'char'}, {'row'});
            
            eventData = ValidationEventData.createStageSkippedUnsupported(profileName, stageName);
            obj.notify('ValidationStageUpdated', eventData);
        end
        
        function notifyValidationFinished(obj, profileName, status)
        % notify that a stage was skipped
            import parallel.internal.settings.ValidationEventData
            eventData = ValidationEventData.createOverallValidationStatus(profileName, status);
            obj.notify('ValidationFinished', eventData);
        end
    end
end


%------------------------------------
function job = iCreateAndSubmitJob(cluster, jobType) %#ok<DEFNU> - used in evalc
import parallel.internal.types.Variant
validateattributes(cluster, {'parallel.Cluster'}, {'scalar'});
validateattributes(jobType, {'parallel.internal.types.Variant'}, {'scalar'});

switch jobType
    case Variant.IndependentJob
        job = cluster.createJob;
    case {Variant.CommunicatingSPMDJob, ...
          Variant.CommunicatingPoolJob}
        job = cluster.createCommunicatingJob('Type', jobType.Name);
    otherwise
        error(message('parallel:settings:ProfileValidationInvalidJobVariant', jobType.Name));
end

try
    job.createTask(@iGetLabInfo, 2, {});
    job.submit;
catch err
    job.delete();
    rethrow(err);
end
end

%------------------------------------
function [hostname, lab] = iGetLabInfo()
cfg = pctconfig();
hostname = cfg.hostname;
lab = labindex;
end

%------------------------------------
function log = iGetDebugLog(job) %#ok<DEFNU> used in evalc
if ~ismethod(job.Parent, 'getDebugLog')
    log = '';
    return;
end

if isa(job, 'parallel.CommunicatingJob')
    log = job.Parent.getDebugLog(job);
else
    log = arrayfun(@(x) job.Parent.getDebugLog(x), job.Tasks, ...
        'UniformOutput', false);
end
end

%------------------------------------
function hostnames = iDoSpmd()
spmd
    hostname = iGetLabInfo();
end  
hostnames = hostname(:);
end

%------------------------------------
function iMatlabpoolCleanup(profile)
client = parallel.internal.pool.SessionManager.getSessionObject();
if client.isPossiblyRunning()
    % Use evalc for the cleanup function to avoid any matlabpool related 
    % messages appearing in the command window during cleanup.
    closePoolFcn = @() matlabpool('close', 'force', profile); %#ok<NASGU> used in evalc
    evalc('closePoolFcn()');
end
end

%--------------------------------------
function cleanupObj = iSetEnvironmentForValidation()
import parallel.internal.validator.Validator
% Set MDCE_DEBUG to true before we actually validate so we get
% extra messages from the workers
oldMdceDebugValue = getenv(Validator.MdceDebugVarName);
setenv(Validator.MdceDebugVarName, 'true');
% And set the scheduler message handler to @disp on the client
% so we also get client messages
oldMessageHandler = setSchedulerMessageHandler(iGetMessageHandler(4));
% Get the lastwarn so we can see if there were any
% warnings during validation
[lastWarnMessage, lastWarnId] = lastwarn;
lastwarn('');

cleanupObj = onCleanup(@() nRestoreEnvironment);

    function nRestoreEnvironment()
        import parallel.internal.validator.Validator
        % Restore MDCE_DEBUG, the scheduler message handler and the last warn
        setenv(Validator.MdceDebugVarName, oldMdceDebugValue);
        setSchedulerMessageHandler(oldMessageHandler);
        lastwarn(lastWarnMessage, lastWarnId);
    end
end

%--------------------------------------
function fcn = iGetMessageHandler(atLevel) 
fcn = @nDispMessage; 
    function nDispMessage(msg, level) 
        if level <= atLevel 
            disp(msg); 
        end 
    end 
end


%--------------------------------------
function [commandWindowOutput, debugLog] = iGetDebugLogNoError(job, commandWindowOutput) %#ok<INUSL> - used in evalc
try
    [displayedOutput, debugLog] = evalc('iGetDebugLog(job);');
catch err
    s = warning('off','backtrace');
    warning('parallel:validation:FailedToGetDebugLog', err);
    displayedOutput = '';
    debugLog = '';
    warning(s);
end
commandWindowOutput = [commandWindowOutput, displayedOutput];
end
