
%   Copyright 2012 The MathWorks, Inc.

classdef CJSStreamingEvaluator < parallel.internal.evaluator.Evaluator

    properties ( SetAccess = immutable, GetAccess = private )
        OutputWriterStack
        DiaryFile
    end

    methods
        function obj = CJSStreamingEvaluator( outputWriterStack, diaryFile )
            obj.OutputWriterStack = outputWriterStack;
            obj.DiaryFile = diaryFile;
        end

        function [out, errOut, textOut] = evaluate( obj, fcn, nOut, args )
            % Create a file writer to write the diary output to the specified file.
            % Append to the file, as we may have written other output to this file,
            % e.g. from JobStartup or TaskStartup
            appendToFile = true;
            streamingWriter = java.io.BufferedWriter( ...
                java.io.FileWriter( obj.DiaryFile, appendToFile ) );
            % We only close the original stream and not the stack. We don't
            % want to close the stack because MVM still has a reference to it,
            % and we want to continue using it.
            cleaner = onCleanup(@() streamingWriter.close);
            dctSchedulerMessage(6, 'About to push FileWriter on stack');
            % Push it onto the stack so that the MVM feval redirects its output to it.
            obj.OutputWriterStack.pushWriter( streamingWriter );
            [out, errOut] = parallel.internal.evaluator.evaluateWithNoErrors( fcn, nOut, args );
            % Text output has already been streamed
            textOut = '';
            % Flush the stream before popping it off the stack.
            obj.OutputWriterStack.flush;
            obj.OutputWriterStack.popWriter;
            dctSchedulerMessage(6, 'FileWriter popped off stack');
        end

        function evaluator = createPoolShutdownEvaluator( ~ )
            % If we are in pool shutdown we can no longer write to the output writer, as
            % no-one will write to it, so use evalc instead.
            evaluator = parallel.internal.evaluator.EvalcEvaluator( true );
        end
    end
end
