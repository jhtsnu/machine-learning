
%   Copyright 2012 The MathWorks, Inc.

classdef EvalcEvaluator < parallel.internal.evaluator.Evaluator

    properties
        CaptureText
    end

    methods
        function obj = EvalcEvaluator( captureText )
            obj.CaptureText = captureText;
        end

        function [out, errOut, textOut] = evaluate( obj, fcn, nOut, args ) %#ok<INUSD> - used in evalc
            [textOut, out, errOut] = evalc( ...
                'parallel.internal.evaluator.evaluateWithNoErrors( fcn, nOut, args )' );
            if ~obj.CaptureText
                textOut = '';
            end
        end
    end

end
