
%   Copyright 2012 The MathWorks, Inc.

classdef Evaluator < handle

    methods ( Abstract )
        [out, errOut, textOut] = evaluate( obj, fcn, nOut, args )
    end

    methods
        function evaluator = createPoolShutdownEvaluator( obj )
            % By default return the same evaluator
            evaluator = obj;
        end
    end

end
