% Copyright 2012 The MathWorks, Inc.

classdef NullEvaluator < parallel.internal.evaluator.Evaluator

    properties ( Access = private )
        OutputWriterStack
    end

    methods
        function obj = NullEvaluator( outputWriterStack )
            obj.OutputWriterStack = outputWriterStack;
        end

        function [out, errOut, textOut] = evaluate( obj, fcn, nOut, args )
            nullOutputWriter = org.apache.commons.io.output.NullWriter;
            obj.OutputWriterStack.pushWriter( nullOutputWriter );
            [out, errOut] = parallel.internal.evaluator.evaluateWithNoErrors( fcn, nOut, args );
            textOut = '';
            obj.OutputWriterStack.popWriter;
        end
    end
end
