function [out, errOut] = evaluateWithNoErrors( fcn, nOut, args )
% evaluateWithNoErrors - Evaluate a function, capturing and returning
%   any error.

%   Copyright 2012 The MathWorks, Inc.

out = {};
try
    % MATLAB is not good at asking for zero output arguments, try for example 
    % clear
    % [x{1:0}] = feval(@matlabroot)
    % So we will distinguish between nOut == 0 and anything else
    if nOut > 0
        [out{1:nOut}] = feval(fcn, args{:});
    else
        feval(fcn, args{:});
    end
    errOut = MException.empty();
catch errOut
end
