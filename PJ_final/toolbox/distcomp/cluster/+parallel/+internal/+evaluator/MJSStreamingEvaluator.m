
%   Copyright 2012 The MathWorks, Inc.

classdef MJSStreamingEvaluator < parallel.internal.evaluator.Evaluator

    properties ( Constant, Hidden )
        WriteBufferSize = 16384; % Use a 16kB buffer for command window output
    end

    properties ( Access = private )
        OutputWriterStack
        TaskId
        TaskAccess
    end

    methods
        function obj = MJSStreamingEvaluator( outputWriterStack, taskId, taskAccess )
            obj.OutputWriterStack = outputWriterStack;
            obj.TaskId = taskId;
            obj.TaskAccess = taskAccess;
        end

        function [out, errOut, textOut] = evaluate( obj, fcn, nOut, args )
            % Set up the streaming task evaluator
            import com.mathworks.toolbox.distcomp.mjs.cwo.CWOStreamingWriter;
            streamingWriter = CWOStreamingWriter.create( obj.TaskId, obj.TaskAccess, obj.WriteBufferSize );
            % We only close the original stream and not the stack.  We don't
            % want to close the stack because MVM still has a reference to it,
            % and we want to continue using it.
            cleaner = onCleanup(@() streamingWriter.close);
            dctSchedulerMessage(6, 'About to push CWOStreamingWriter on stack');
            obj.OutputWriterStack.pushWriter( streamingWriter );
            [out, errOut] = parallel.internal.evaluator.evaluateWithNoErrors( fcn, nOut, args );
            % We've been streaming the text output, so return empty. This will
            % not be submitted to the job manager.
            textOut = '';
            % This flush will block until all the Diary output has been read by
            % the job manager. This means the task will not finish executing
            % until all the Diary output has been sent to the job manager. It
            % may still take time for the job manager to write it to its
            % database, so it won't necessarily be immediately available on the
            % client.
            obj.OutputWriterStack.flush;
            obj.OutputWriterStack.popWriter;
            dctSchedulerMessage(6, 'CWOStreamingWriter popped off stack');
        end

        function evaluator = createPoolShutdownEvaluator( ~ )
            % If we are in pool shutdown we can no longer write to the output writer, as
            % no-one will write to it, so use evalc instead.
            evaluator = parallel.internal.evaluator.EvalcEvaluator( true );
        end
    end

end
