
%   Copyright 2012 The MathWorks, Inc.

classdef CapturingEvaluator < parallel.internal.evaluator.Evaluator

    properties ( Access = private )
        OutputWriterStack
    end

    methods
        function obj = CapturingEvaluator( outputWriterStack )
            obj.OutputWriterStack = outputWriterStack;
        end

        function [out, errOut, textOut] = evaluate( obj, fcn, nOut, args )
            % Create a new capturing outputWriter
            capturingOutputWriter = java.io.StringWriter;
            cleaner = onCleanup(@() capturingOutputWriter.close);
            dctSchedulerMessage(6, 'About to push StringWriter on stack');
            obj.OutputWriterStack.pushWriter( capturingOutputWriter );
            [out, errOut] = parallel.internal.evaluator.evaluateWithNoErrors( fcn, nOut, args );
            % toString is the supported way to get the chars out of a StringWriter
            textOut = char(obj.OutputWriterStack.toString);
            obj.OutputWriterStack.flush;
            obj.OutputWriterStack.popWriter;
            dctSchedulerMessage(6, 'StringWriter popped off stack');
        end

        function evaluator = createPoolShutdownEvaluator( ~ )
            % If we are in pool shutdown we can no longer write to the output writer, as
            % no-one will write to it, so use evalc instead.
            evaluator = parallel.internal.evaluator.EvalcEvaluator( true );
        end

    end

end
