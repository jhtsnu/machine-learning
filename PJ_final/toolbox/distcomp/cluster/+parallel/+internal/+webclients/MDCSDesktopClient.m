% MDCSDesktopClient - Represents an MDCS user logged in to the desktop.
% Holds the desktop login token and the web license id.

%   Copyright 2011-2012 The MathWorks, Inc.

classdef (Sealed) MDCSDesktopClient < handle

    properties ( SetAccess = private, Dependent )
        LoginToken
        Entitlement
    end
    
    properties ( SetAccess = private )
        UserName = '';
    end
    
    properties ( GetAccess = private, SetAccess = immutable )
        IsChooseLicenseSupported = true;
    end

    properties ( Hidden )
        % Used to get correct focus behaviour from login dialog (see g779939)
        LoginDialogConsumer
    end
    
    properties ( Access = private )
        % EntitlementRetriever and LoginTokenProvider can be over-ridden for testing
        EntitlementRetriever = com.mathworks.toolbox.distcomp.wsclients.entitlements.MDCSEntitlementRetriever;
        LoginTokenProvider = parallel.internal.webclients.WebLoginTokenProvider();
    end

    methods
        function obj = MDCSDesktopClient( varargin )
        % MDCSDesktopClient(varargin) constructor.
        % Takes in P-V pairs and sets these accordingly (including private properties).
            import parallel.internal.customattr.PropSet

            if system_dependent( 'isdmlworker' )
                obj.LoginTokenProvider = parallel.internal.webclients.ErroringLoginTokenProvider( ...
                    'parallel:cluster:CannotUseMHLMFromWorkers' );
                obj.IsChooseLicenseSupported = false;
            end
            if isdeployed()
                obj.LoginTokenProvider = parallel.internal.webclients.ErroringLoginTokenProvider( ...
                    'parallel:cluster:CannotUseMHLMFromDeployed' );
                obj.IsChooseLicenseSupported = false;
            end
            [names, values] = PropSet.amalgamate( {}, varargin{:} );
            for ii = 1:numel(names)
                obj.(names{ii}) = values{ii};
            end
        end
        
        function token = get.LoginToken( obj )
        % get.LoginToken may cause the login dialog to be shown.
            % Caching of the login token is done at the java layer, so we always
            % call getDesktopLoginToken
            forceAuth = false;
            [token, obj.UserName] = obj.getDesktopLoginToken( forceAuth );
        end

        function val = get.Entitlement( obj )
        % Get a license entitlement.  May cause the license
        % selection dialog to be shown if the user has >1 license.
            val = obj.chooseLicenseEntitlement();
        end
        
        function token = forceNewAuthentication( obj )
            % Used from parallel.internal.webclients.invokeAndRetryOnAuthFailure
            % see g779230
            forceAuth = true;
            [token, obj.UserName] = obj.getDesktopLoginToken( forceAuth );
        end

        function [ok, entitlement, foundLicenseNumbers] = checkLicenseNumber( obj, licenseNumber )
        % Get all the entitlements from web services and check the license number
        % we've been given matches one of them. Otherwise error.  
            entitlements = parallel.internal.webclients.getEntitlements( obj.EntitlementRetriever );

            foundLicenseNumbers = {entitlements.LicenseNumber};
            licenseNumberIdx    = strcmp( licenseNumber, foundLicenseNumbers );
            entitlement         = entitlements(licenseNumberIdx);
            ok                  = any( licenseNumberIdx );
        end

        function logout( obj )
            obj.LoginTokenProvider.removeSavedToken();
        end
        
        function tf = isLoggedIn( obj )
            try
                tf = ~isempty( obj.LoginToken );
            catch err %#ok<NASGU>
                % Something went wrong getting the login token
                % so we can't be logged in.
                tf = false;
            end
        end
    end
    
    methods ( Static )
        function obj = buildWithFixedToken( token, varargin )
        % buildWithFixedToken( token )
        % buildWithFixedToken( token, username )
        % Build an MDCSDesktopClient with a fixed token and username.

            loginTokenProvider = parallel.internal.webclients.FixedLoginTokenProvider( ...
                token, varargin{:});
            
            obj = parallel.internal.webclients.MDCSDesktopClient( ...
                'LoginTokenProvider', loginTokenProvider);
        end
    end

    methods( Access = private )
        function [token, user] = getDesktopLoginToken( obj, forceAuth )
            [token, user] = obj.LoginTokenProvider.getUserAndToken(forceAuth, obj.LoginDialogConsumer);
        end
        
        function chosenLicense = chooseLicenseEntitlement( obj )
            [entitlements, javaEntitlements] = parallel.internal.webclients.getEntitlements( obj.EntitlementRetriever );
            if isempty( entitlements )
                E = MException( message( 'parallel:cluster:NoMDCSLicense' ) );
                throwAsCaller( E );
            elseif numel( entitlements ) == 1
                % Return the only entitlement
                chosenLicense = entitlements;
            elseif obj.IsChooseLicenseSupported
                chosenLicense = parallel.internal.webclients.entitlementSelectionDialog( entitlements, javaEntitlements );
                if isempty(chosenLicense)
                    E = MException( message( 'parallel:cluster:NoMDCSLicenseChosen' ) );
                    throwAsCaller( E );
                end
            else
                allLicenseNumbers = {entitlements.LicenseNumber};
                licenseNumberStr = [ sprintf( '  %s\n', allLicenseNumbers{1:end-1} ), sprintf( '  %s', allLicenseNumbers{end} ) ];
                E = MException( message( 'parallel:cluster:ChooseMDCSLicenseNotSupported', licenseNumberStr ) );
                throwAsCaller( E );
            end
        end
    end
end

