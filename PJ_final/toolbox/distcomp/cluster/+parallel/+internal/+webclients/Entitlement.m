
%   Copyright 2011-2012 The MathWorks, Inc.

classdef Entitlement
    properties( SetAccess = private )
        Id = '';
        LicenseNumber = '';
        Label = '';
    end
    
    methods
        function obj = Entitlement( id, licenseNumber, label )
            obj.Id = id;
            obj.LicenseNumber = licenseNumber;
            obj.Label = label;
        end
        
        function repr = toString( obj )
            if isempty( obj.Label )
                repr = obj.LicenseNumber;
            else
                repr = [obj.LicenseNumber ' - ' obj.Label];
            end
        end
        
    end
    
    methods(Static)
        function entitlement = createEntitlement(javaEntitlement)
            import parallel.internal.webclients.Entitlement;
            id = char(javaEntitlement.getId);
            licenseNumber = char(javaEntitlement.getLicenseNumber);
            label = char(javaEntitlement.getLabel);
            entitlement = Entitlement(id, licenseNumber, label);            
        end
    end
    
end
