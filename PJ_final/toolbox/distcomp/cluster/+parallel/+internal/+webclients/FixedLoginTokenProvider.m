% FixedLoginTokenProvider - Always return a fixed token and username

%   Copyright 2012 The MathWorks, Inc.

classdef FixedLoginTokenProvider < parallel.internal.webclients.AbstractLoginTokenProvider
    properties (Access = private)
        Username
        Token
    end
    
    methods
        function obj = FixedLoginTokenProvider(token, varargin)
        % Always returns a fixed token and username based on the supplied
        % constructor args.
        % FixedLoginTokenProvider(token)
        % FixedLoginTokenProvider(token, username)
            if nargin < 2
                username = '';
            else
                username = varargin{1};
            end
            obj.Username = username;
            obj.Token = token;
        end
        
        function [token, username] = getUserAndToken(obj, ~, ~)
            token = obj.Token;
            username = obj.Username;
        end

        function removeSavedToken(~)
        % Nothing to do for fixed provider
        end
    end
end
