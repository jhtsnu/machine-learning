function val = invokeAndRetryOnAuthFailure( webServiceFcn )
% Helper to make a web service call and retry if there was an authentication
% failure

%   Copyright 2011-2012 The MathWorks, Inc.

desktopClient = parallel.internal.webclients.currentDesktopClient();
token = desktopClient.LoginToken;
numAttempts = 2;
for ii = 1:numAttempts
    if isempty( token ) 
    % No update possible if we don't have a valid token
        val = [];
        return;
    end
    try
        val = webServiceFcn( token );
        break;
    catch E
        if isa(E, 'matlab.exception.JavaException') ...
            && isa(E.ExceptionObject, ...
                    'com.mathworks.toolbox.distcomp.wsclients.WSAuthenticationFailureException') ...
                && ii < numAttempts
            token = desktopClient.forceNewAuthentication();
            continue;
        end
        throwAsCaller( E );
    end
end
