% WebLoginTokenProvider - Use web services to get a valid login token.

%   Copyright 2012 The MathWorks, Inc.

classdef WebLoginTokenProvider < parallel.internal.webclients.AbstractLoginTokenProvider
    methods
        function [token, username] = getUserAndToken(~, forceAuth, dialogConsumer)
        % Returns a valid login token, or empty if the validation was cancelled.
        % The token is cached in the current session. To clear this token call
        % removeSavedToken().
            import com.mathworks.toolbox.distcomp.mwlogin.LoginTokenValidation;
            if isempty(dialogConsumer)
                loginTokenProvider = LoginTokenValidation(forceAuth);
            else
                loginTokenProvider = LoginTokenValidation(forceAuth, dialogConsumer);
            end

            username = '';
            token = '';
            userTokenPair = loginTokenProvider.validate;
            if ~isempty(userTokenPair)
                username = char(userTokenPair.getUser);
                token = char(userTokenPair.getToken);
            end
        end
        
        function removeSavedToken(~)
            import com.mathworks.widgets.login.LogOut
            import com.mathworks.toolbox.distcomp.mwlogin.LoginTokenValidation
            
            % This removes the saved preference to "Keep me logged on".
            l = LogOut;
            l.logOutMWA;

            % This removes the PCT persisted token for this session.
            LoginTokenValidation.clearPersistedToken;
        end
    end
end
