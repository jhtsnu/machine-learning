% ErroringLoginTokenProvider - Always error if asked to provide a username and token.

%   Copyright 2012 The MathWorks, Inc.

classdef ErroringLoginTokenProvider < parallel.internal.webclients.AbstractLoginTokenProvider
    properties (Access = private)
        ErrorID
    end

    methods
        function obj = ErroringLoginTokenProvider(errorID)
            obj.ErrorID = errorID;
        end
    
        function [token, user] = getUserAndToken(obj, ~, ~) %#ok<STOUT>
            error(message(obj.ErrorID));
        end
        function removeSavedToken(~)
        % Nothing to do for this provider.
        end
    end
end
