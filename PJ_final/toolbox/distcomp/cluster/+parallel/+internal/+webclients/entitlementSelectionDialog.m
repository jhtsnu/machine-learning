function e = entitlementSelectionDialog(entitlements, javaEntitlements)

%   Copyright 2011-2012 The MathWorks, Inc.

if usejava('awt')
    % Show the entitlement selection dialog
    e = iSelectEntitlementUI(javaEntitlements);
else
    % Show the entitlement selection at the command line
    e = iSelectEntitlementCmdLine(entitlements);
end



function selectedEntitlement = iSelectEntitlementUI(javaEntitlements)
% iSelectEntitlementUI - dialog for selecting an entitlement

import parallel.internal.webclients.Entitlement
selectedEntitlement = Entitlement.empty();

% Return the selected entitlement
dialog = javaObjectEDT('com.mathworks.toolbox.distcomp.ui.widget.EntitlementSelectionDialog', javaEntitlements);
javaMethodEDT('setVisible', dialog, true);
selectedJavaEntitlement = dialog.getEntitlement();
if ~isempty(selectedJavaEntitlement)
    selectedEntitlement = Entitlement.createEntitlement(selectedJavaEntitlement);
end


function selectedEntitlement = iSelectEntitlementCmdLine(entitlements)
% iSelectEntitlementCmdLine - cmd line for selecting an entitlement

entitlementStrings = cell(1, length(entitlements));

for ii = 1:length(entitlements)
    entitlementStrings{ii} = entitlements(ii).toString;
end

msg = {getString(message('parallel:ui:EntitlementSelectionLabel'))};

numLicenses = length(entitlementStrings);
fprintf('%s\n', msg{:});
for ii = 1:numLicenses
    fprintf('%d) %s\n', ii, entitlementStrings{ii});
end
s = input(getString(message('parallel:ui:EntitlementSelectionInput')), 's');
s = str2double(s);
if ~isscalar(s) || ~isnumeric(s) || rem(s,1)~=0 || s > numLicenses || s < 1
    error(message('parallel:cluster:InvalidEntitlementSelection'));
end

selectedEntitlement = entitlements(s);
