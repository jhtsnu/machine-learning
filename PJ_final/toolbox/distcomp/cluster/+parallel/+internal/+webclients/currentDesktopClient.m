function oldClient = currentDesktopClient(newClient)
% oldClient = currentDesktopClient(newClient)
% Get or set the current desktop client.

% Copyright 2012 The MathWorks, Inc.

if system_dependent('isdmlworker')
    % Must mlock on a worker because clear('functions') is called during
    % task setup.
    mlock; 
end
persistent desktopClient

if isempty(desktopClient)
    desktopClient = iGetDefaultDesktopClient();
end

oldClient = desktopClient;

if nargin > 0
    desktopClient = newClient;
    dctSchedulerMessage(6, 'Setting the desktop client to a new client with username %s', newClient.UserName);
end


%--------------------------------------------------------
function val = iGetDefaultDesktopClient()
persistent defaultClient
if isempty(defaultClient)
    defaultClient = parallel.internal.webclients.MDCSDesktopClient();
end
val = defaultClient;


