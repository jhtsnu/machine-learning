function [entitlements, javaEntitlementList] = getEntitlements( retriever )
; %#ok Undocumented

%   Copyright 2011-2012 The MathWorks, Inc.

import com.mathworks.toolbox.distcomp.wsclients.entitlements.MDCSEntitlementRetriever;
import parallel.internal.webclients.Entitlement;

if nargin < 1
    retriever = MDCSEntitlementRetriever;
end

getEntitlementsFcn = @(token) retriever.getEntitlements( token );

javaEntitlementList = parallel.internal.webclients.invokeAndRetryOnAuthFailure( ...
    getEntitlementsFcn );

numEntitlements = length(javaEntitlementList);
entitlements = repmat(Entitlement.empty, 1, numEntitlements);
for ii = 1:numEntitlements
    entitlements(ii) = Entitlement.createEntitlement(javaEntitlementList(ii));
end
end
