% AbstractLoginTokenProvider - base class for all login token providers

%   Copyright 2012 The MathWorks, Inc.

classdef AbstractLoginTokenProvider < handle
    methods (Abstract)
        % Return the token and user name.
        % forceAuth - whether or not to force authentication
        % dialogConsumer - the consumer for any dialogs that may be popped up.
        [token, username] = getUserAndToken(obj, forceAuth, dialogConsumer);
        % Remove the saved token to force another log in if required.
        removeSavedToken(obj);
    end
end