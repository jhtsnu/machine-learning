function oldClient = currentCloudCenterClient(newClient)
% oldClient = currentCloudCenterClient(newClient)
% Get or set the current cloud center client

% Copyright 2012 The MathWorks, Inc.

persistent cloudCenterClient

if isempty(cloudCenterClient)
    cloudCenterClient = iGetDefaultCloudCenterClient();
end

oldClient = cloudCenterClient;

if nargin > 0
    cloudCenterClient = newClient;
    dctSchedulerMessage(6, 'Setting a new cloud center client');
end

%-------------------------------------------------------
function val = iGetDefaultCloudCenterClient()
persistent defaultCloudCenterClient
if isempty(defaultCloudCenterClient)
    import com.mathworks.toolbox.distcomp.wsclients.cloudconsole.MJSCloudConsoleClientImpl

    defaultCloudCenterClient = MJSCloudConsoleClientImpl();
end
val = defaultCloudCenterClient;
