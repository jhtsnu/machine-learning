% DefaultValue - The default value is used when properties are unset at all Settings levels
 
%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden, Sealed) DefaultValue
    properties (Constant, Hidden, GetAccess = private)
        DisplayString = 'Use Default';
    end

    methods
        function disp(~)
            import parallel.settings.DefaultValue
            disp(DefaultValue.DisplayString);
        end
        
        function str = char(~)
            import parallel.settings.DefaultValue
            str = DefaultValue.DisplayString;
        end
        
        function val = formatDispatcher(obj, displayHelper, valDisplayLength, formatter)
            val = formatter(displayHelper,  obj , valDisplayLength);
        end
    end
    
    methods (Static)
        function tf = isDefault(aValue)
            tf = isa(aValue, 'parallel.settings.DefaultValue');
        end
    end
end