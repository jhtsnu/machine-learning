% PBS - Base class for PBS schedulers
 
%   Copyright 2011 The MathWorks, Inc.

% TODO:doc properties
classdef (Hidden) PBS < parallel.settings.schedulerComponent.ThirdPartyCJS
    properties (PCTGetSet, Transient, PCTConstraint = 'logicalscalar')
        HasSharedFilesystem
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'string')
        SubmitArguments
        CommunicatingJobWrapper
        RcpCommand
        ResourceTemplate
        RshCommand
    end

    methods (Access = protected) % Constructor
        function obj = PBS(name, typeEnum, node, parentNode)
            obj@parallel.settings.schedulerComponent.ThirdPartyCJS(name, ...
                typeEnum, node, parentNode)
        end
    end

    methods (Hidden) % overrides of CustomPropDisp
       function [n, f] = hCustomizeDisplay(obj, level)
           n = {'SubmitArguments', 'ResourceTemplate', 'CommunicatingJobWrapper', ...
                'HasSharedFilesystem', 'RshCommand', 'RcpCommand'};
                
            if nargin < 2
                f = obj.get(n);
            else
                f = cellfun(@(x) obj.get(x, level), n, 'UniformOutput', false);
            end
        end
    end
end
%#ok<*ATUNK> custom attributes
