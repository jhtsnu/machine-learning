% LSF - Scheduler Component for Platform LSF
 
%   Copyright 2011 The MathWorks, Inc.

% TODO:doc properties
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden, Sealed) LSF < parallel.settings.schedulerComponent.ThirdPartyCJS
    properties (PCTGetSet, Transient, PCTConstraint = 'enum:parallel.internal.types.OperatingSystem')
        OperatingSystem
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'string')
        SubmitArguments
        CommunicatingJobWrapper
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'logicalscalar')
        HasSharedFilesystem
    end

    methods (Hidden, Static) % Implementation of NamedNode Hidden, Static methods
        function obj = hBuildFromSettings(name, settingsNode, parentSettingsNode) 
            obj = parallel.settings.schedulerComponent.LSF(name, settingsNode, parentSettingsNode);
        end
    end

    methods (Access = private) % Constructor
        function obj = LSF(name, node, parentNode)
            import parallel.internal.types.SchedulerType
            obj@parallel.settings.schedulerComponent.ThirdPartyCJS(name, ...
                SchedulerType.LSF, node, parentNode);
        end
    end

    methods (Hidden) % overrides of CustomPropDisp
       function [n, f] = hCustomizeDisplay(obj, level)
            n = {'SubmitArguments', 'OperatingSystem', ...
                'CommunicatingJobWrapper', 'HasSharedFilesystem'};
            if nargin < 2
                f = obj.get(n);
            else
                f = cellfun(@(x) obj.get(x, level), n, 'UniformOutput', false);
            end
        end
    end
end
%#ok<*ATUNK> custom attributes
