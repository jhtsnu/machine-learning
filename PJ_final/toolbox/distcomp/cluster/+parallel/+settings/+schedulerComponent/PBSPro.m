% PBSPro - Scheduler Component for PBSPro
 
%   Copyright 2011 The MathWorks, Inc.

% TODO:doc properties
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden, Sealed) PBSPro < parallel.settings.schedulerComponent.PBS

    methods (Hidden, Static) % Implementation of NamedNode Hidden, Static methods
        function obj = hBuildFromSettings(name, settingsNode, parentSettingsNode) 
            obj = parallel.settings.schedulerComponent.PBSPro(name, settingsNode, parentSettingsNode);
        end
    end

    methods (Access = private) % Constructor
        function obj = PBSPro(name, node, parentNode)
            import parallel.internal.types.SchedulerType
            obj@parallel.settings.schedulerComponent.PBS(name, ...
                SchedulerType.PBSPro, node, parentNode);
        end
    end
end
%#ok<*ATUNK> custom attributes
