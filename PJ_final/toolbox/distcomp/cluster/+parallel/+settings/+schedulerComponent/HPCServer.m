% HPCServer - Scheduler Component for Microsoft HPC Server Schedulers
 
%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc properties
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden, Sealed) HPCServer < parallel.settings.schedulerComponent.ThirdPartyCJS
    properties (PCTGetSet, Transient, PCTConstraint = 'string')
        Host
        JobTemplate
        JobDescriptionFile
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'enum:parallel.internal.types.HPCServerClusterVersion')
        ClusterVersion
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'logicalscalar')
        UseSOAJobSubmission
    end
    
    methods (Hidden, Static) % Implementation of NamedNode Hidden, Static methods
        function obj = hBuildFromSettings(name, settingsNode, parentSettingsNode) 
            obj = parallel.settings.schedulerComponent.HPCServer(name, settingsNode, parentSettingsNode);
        end
    end

    methods (Access = private) % Constructor
        function obj = HPCServer(name, node, parentNode)
            import parallel.internal.types.SchedulerType
            obj@parallel.settings.schedulerComponent.ThirdPartyCJS(name, ...
                SchedulerType.HPCServer, node, parentNode);
        end
    end

    methods (Hidden) % overrides of CustomPropDisp
       function [n, f] = hCustomizeDisplay(obj, level)
            n = {'Host', 'ClusterVersion', 'UseSOAJobSubmission', ...
                'JobTemplate', 'JobDescriptionFile'};
            if nargin < 2
                f = obj.get(n);
            else
                f = cellfun(@(x) obj.get(x, level), n, 'UniformOutput', false);
            end
        end
    end
    
    methods (Access = protected) % Implementation of SchedulerComponent abstract methods
        function tf = isAlreadyDiscovered(obj, names, values)
            propsToCompare = 'Host';
            otherHost = obj.extractValuesFromNamesValues(propsToCompare, names, values);
            tf = obj.isEquivalentHost(otherHost);
        end
    end
end
%#ok<*ATUNK> custom attributes
