% MJSComputeCloud - Scheduler Component for MathWorks Cloud Console Job scheduler
 
%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc properties
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden, Sealed) MJSComputeCloud < parallel.settings.SchedulerComponent

    %---------------------------------------------------
    % Properties stored in Settings
    %---------------------------------------------------
    properties (PCTGetSet, Transient, PCTConstraint = 'string')
        Identifier
        Certificate
        LicenseNumber
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'positiveintscalar')
        DefaultTimeout
        MaximumRetries
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'logicalscalar')
        RestartWorker
    end
    
    %---------------------------------------------------
    % Normal class properties
    %---------------------------------------------------
    properties (Constant, Access = private)
        JobProperties  = {'DefaultTimeout', 'RestartWorker'};
        TaskProperties = {'DefaultTimeout', 'MaximumRetries'};
    end

    
    methods (Hidden, Static) % Implementation of NamedNode Hidden, Static methods
        function obj = hBuildFromSettings(name, settingsNode, parentSettingsNode) 
            obj = parallel.settings.schedulerComponent.MJSComputeCloud(name, settingsNode, parentSettingsNode);
        end
    end

    methods (Access = private) % Constructor
        function obj = MJSComputeCloud(name, node, parentNode)
            import parallel.internal.types.SchedulerType
            obj@parallel.settings.SchedulerComponent(name, ...
                SchedulerType.MJSComputeCloud, node, parentNode);
        end
    end
    
    methods (Hidden) % Overrides of SchedulerComponent methods
    
        function [p, v] = hGetClusterPVPairs(obj)
            % all gettable settings props except the JobProperties are cluster props
            import parallel.settings.schedulerComponent.MJSComputeCloud
            
            gettableProps = obj.hGetPublicSettingsProperties();
            p = setdiff(gettableProps, [MJSComputeCloud.JobProperties, MJSComputeCloud.TaskProperties]);
            [p, v] = obj.getCollapsedPropertiesNoDefaultValues(p);
        end
        
        function [p, v] = hGetJobPVPairs(obj, resolveWithProps, resolveWithVals)
            import parallel.settings.schedulerComponent.MJSComputeCloud
            import parallel.internal.settings.MJSHelper

            [p, v] = obj.getCollapsedPropertiesNoDefaultValues(MJSComputeCloud.JobProperties);
            p = [p(:); resolveWithProps(:)].';
            v = [v(:); resolveWithVals(:)].';
            [p, v] = MJSHelper.resolveTimeouts(p, v);
        end

        function [p, v] = hGetTaskPVPairs(obj, resolveWithProps, resolveWithVals)
            import parallel.settings.schedulerComponent.MJSComputeCloud
            import parallel.internal.settings.MJSHelper

            [p, v] = obj.getCollapsedPropertiesNoDefaultValues(MJSComputeCloud.TaskProperties);
            p = [p(:); resolveWithProps(:)].';
            v = [v(:); resolveWithVals(:)].';
            [p, v] = MJSHelper.resolveTimeouts(p, v);
        end

        function [n, f] = hCustomizeDisplay(obj, level)
            n = {'Identifier', 'Certificate', 'LicenseNumber', ...
                 'DefaultTimeout', 'RestartWorker', 'MaximumRetries'};
            if nargin < 2
                f = obj.get(n);
            else
                f = cellfun(@(x) obj.get(x, level), n, 'UniformOutput', false);
            end
        end
        
    end

    methods (Access = protected) % Implementation of SchedulerComponent abstract methods
        function tf = isAlreadyDiscovered(obj, names, values)
            propsToCompare = {'Identifier', 'Certificate'};
            vals = obj.extractValuesFromNamesValues(propsToCompare, names, values);
            clusterVals = obj.get(propsToCompare);
            tf = isequal(clusterVals(:), vals(:));
        end
    end
end
%#ok<*ATUNK> custom attributes
