% Local - Scheduler Component for Local Schedulers
 
%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc properties
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden, Sealed) Local < parallel.settings.SchedulerComponent

    properties (PCTGetSet, Transient, PCTConstraint = 'datalocation')
        JobStorageLocation
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'positiveintscalar')
        NumWorkers
    end

    methods (Hidden, Static) % Implementation of NamedNode Hidden, Static methods
        function obj = hBuildFromSettings(name, settingsNode, parentSettingsNode) 
            obj = parallel.settings.schedulerComponent.Local(name, settingsNode, parentSettingsNode);
        end
    end

    methods (Access = private) % Constructor
        function obj = Local(name, node, parentNode)
            import parallel.internal.types.SchedulerType
            obj@parallel.settings.SchedulerComponent(name, ...
                SchedulerType.Local, node, parentNode);
        end
    end
    
    methods (Access = protected) % Implementation of SchedulerComponent abstract methods
        function tf = isAlreadyDiscovered(~, ~)
            % Local is always already discovered.
            tf = true;
        end
    end
end
%#ok<*ATUNK> custom attributes
