% ThirdPartyCJS - Base class for all 3rd party schedulers
 
%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc properties
classdef (Hidden) ThirdPartyCJS < parallel.settings.SchedulerComponent
    properties (PCTGetSet, Transient, PCTConstraint = 'datalocation')
        JobStorageLocation
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'positiveintscalar')
        NumWorkers
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'string')
        ClusterMatlabRoot
        LicenseNumber
    end
    properties (PCTGetSet, Transient, PCTConstraint = 'logicalscalar')
        RequiresMathWorksHostedLicensing
    end

    methods (Access = protected)
        function obj = ThirdPartyCJS(name, typeEnum, node, parentNode)
            obj@parallel.settings.SchedulerComponent(name, typeEnum, node, parentNode);
        end
    end

    methods (Access = protected) % Implementation of SchedulerComponent abstract methods
        function tf = isAlreadyDiscovered(obj, pvPairs)
            % Most 3p schedulers are the same if they have the same Host, JobStorageLocation
            % and ClusterMatlabRoot
            propsToCompare = {'JobStorageLocation', 'ClusterMatlabRoot', 'Host'};
            vals = obj.extractValuesFromPV(propsToCompare, pvPairs);
            tf = isequal(obj.JobStorageLocation, vals{1}) && ...
                isequal(obj.ClusterMatlabRoot, vals{2}) && ...
                obj.isEquivalentHost(vals{3});
        end
    end
end
%#ok<*ATUNK> custom attributes
