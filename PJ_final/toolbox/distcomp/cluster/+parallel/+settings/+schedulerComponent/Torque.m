% Torque - Scheduler Component for Torque
 
%   Copyright 2011 The MathWorks, Inc.

% TODO:doc properties
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden) Torque < parallel.settings.schedulerComponent.PBS
    methods (Hidden, Static) % Implementation of NamedNode Hidden, Static methods
        function obj = hBuildFromSettings(name, settingsNode, parentSettingsNode) 
            obj = parallel.settings.schedulerComponent.Torque(name, settingsNode, parentSettingsNode);
        end
    end

    methods (Access = private) % Constructor
        function obj = Torque(name, node, parentNode)
            import parallel.internal.types.SchedulerType
            obj@parallel.settings.schedulerComponent.PBS(name, ...
                SchedulerType.Torque, node, parentNode);
        end
    end
end