% Profile - Define parameters and properties for connecting to clusters and creating jobs
 
%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc
% NB This class is hidden for now as the Profiles API is not yet documented
classdef (Hidden, Sealed) Profile < parallel.internal.settings.NamedNode
    %---------------------------------------------------
    % Properties stored in Settings
    %---------------------------------------------------
    properties (PCTGetSet, Transient, PCTConstraint = 'string')
        % The description of this profile
        Description
        % The name of the project component used by this profile
        ProjectComponent
    end
    
    properties (PCTGetSet, Transient, PCTConstraint = 'nonemptystring')
        % The name of the scheduler component used by this profile.
        % This cannot be an empty string
        SchedulerComponent
    end
    
    %---------------------------------------------------
    % Normal class properties
    %---------------------------------------------------
    properties (Dependent)
        % The validation status of this profile.
        ValidationStatus 
    end
    
    properties (GetAccess = private, SetAccess = immutable)
        IsSchedulerComponentReadOnly = false
    end
    
    methods % Property get/set methods
        function status = get.ValidationStatus(obj)
            import parallel.internal.settings.AbstractValidator
            import parallel.settings.Profile

            status = AbstractValidator.getDefaultStatus();
            validator = Profile.currentValidator();
            if ~isempty(validator) && obj.NodeFacade.IsValid
                status = validator.getStatus(obj.Name);
            end
            status = status.Name;
        end
    end

    methods (Hidden, Static) % Implementation of NamedNode Hidden, Static methods
        function obj = hBuildFromSettings(name, settingsNode, parentSettingsNode, varargin) 
            obj = parallel.settings.Profile(name, settingsNode, parentSettingsNode, varargin{:});
        end
    end
    
    methods (Hidden) %Override of methods defined in CustomSettingsGetSet
        function hSetProperty(obj, props, values, varargin)
            if obj.IsSchedulerComponentReadOnly && ~isempty(props) && strcmp(props, 'SchedulerComponent')
                throwAsCaller(MException(message('parallel:settings:ProfileCannotChangeSchedulerComponent', obj.Name)));
            end
            hSetProperty@parallel.internal.settings.CustomSettingsGetSet(...
                obj, props, values, varargin{:});
        end
    end
    
    methods (Access = private) % Constructor
        function obj = Profile(name, settingsNode, parentSettingsNode, readOnlySchedNames) 
            import parallel.internal.types.SettingsLevel
            import parallel.settings.Profile
            
            obj@parallel.internal.settings.NamedNode(name, settingsNode, parentSettingsNode);
            obj.addlistener('NameChanged', @obj.handleNameChanged);
            obj.addlistener('SettingsNodeDestroyed', @obj.handleNodeDestroyed);
            
            if nargin == 4
                obj.IsSchedulerComponentReadOnly = strcmp(name, readOnlySchedNames);
            end
        end
    end
    
    methods  % destructor
        function delete(objOrObjs, varargin)
        % NB delete must define more than 1 arg to distinguish it from 
        % "normal" handle delete
            import parallel.settings.Profile
            
            % TODO:later once Profiles API is documented, work out how
            % to ensure that the status is not removed from the validator
            % if delete() is called with invalid syntax.
            validator = Profile.currentValidator();
            if ~isempty(validator)
                for ii = 1:length(objOrObjs)
                    % Remove the validation status
                    validator.deleteStatus(objOrObjs(ii).Name);
                end
            end
            % Call the superclass delete.
            delete@parallel.internal.settings.NamedNode(objOrObjs, varargin{:});
        end
    end
       
    methods (Hidden, Static)
        function oldValidator = hRegisterValidator(validator)
            import parallel.settings.Profile
            if ~isempty(validator)
                validateattributes(validator, {'parallel.internal.settings.AbstractValidator'}, {'scalar'});
            end
            oldValidator = Profile.currentValidator(validator);
        end
        
        function validator = hGetRegisteredValidator()
            import parallel.settings.Profile
            validator = Profile.currentValidator();
        end
    end
       
    methods
        function validate(obj)
        % validate(obj) - run validation on the current profile
            import parallel.internal.settings.ValidationDisplayer
            import parallel.settings.Profile

            validator = Profile.currentValidator();
            if isempty(validator)
                warning(message('parallel:settings:NoRegisteredProfileValidator'));
            else
                % Ensure that validation results are displayed to the command 
                % windows when validation is launched through the API.
                displayer = ValidationDisplayer(validator); %#ok<NASGU> - hold onto displayer
                validator.validate(obj.Name);
            end
        end
        
        function val = getSchedulerComponent(obj)
        % component = getSchedulerComponent(obj) - get the SchedulerComponent
        % object whose name is the same as the SchedulerComponent property
        % of the current profile.  This method throws an error if there is 
        % no scheduler component with that name.
        
            if parallel.settings.DefaultValue.isDefault(obj.SchedulerComponent)
                error(message('parallel:settings:ProfileSchedulerComponentNotDefined', obj.Name));
            end
            
            p = parallel.Settings;
            val = p.findSchedulerComponent('Name', obj.SchedulerComponent);
            if isempty(val)
                error(message('parallel:settings:ProfileCannotFindSchedulerComponent', obj.Name, obj.SchedulerComponent));
            end
        end
        
        function val = getProjectComponent(obj)
        % component = getProjectComponent(obj) - get the ProjectComponent
        % object whose name is the same as the ProjectComponent property
        % of the current profile.  If the project component is unset,
        % then an empty project component is returned.  If the project 
        % component is not unset and there is no project component with that
        % name, then this errors.
        
            if parallel.settings.DefaultValue.isDefault(obj.ProjectComponent)
                % It is OK not to have a project component
                val = parallel.settings.ProjectComponent.empty();
                return;
            end
            
            p = parallel.Settings;
            val = p.findProjectComponent('Name', obj.ProjectComponent);
            if isempty(val)
                error(message('parallel:settings:ProfileCannotFindProjectComponent', obj.Name, obj.ProjectComponent));
            end
        end

        function export(objOrObjs, filename)
        % export(obj, filename) - export the collapsed profile and 
        % associated scheduler components to the specified file.
            validateattributes(filename, {'char'}, {'row'}, 'export', 'filename');
            try
                % Get the components for all the profiles
                allSchedComps = arrayfun(@(x) x.getSchedulerComponent(), ...
                    objOrObjs, 'UniformOutput', false);
                allProjComps  = arrayfun(@(x) x.getProjectComponent(), ...
                    objOrObjs, 'UniformOutput', false);
                allSchedComps = [allSchedComps{:}];
                allProjComps  = [allProjComps{:}];
                    
                nodeFacadesToExport = [objOrObjs.NodeFacade, ...
                    allSchedComps.NodeFacade, allProjComps.NodeFacade];
                nodeFacadesToExport.exportNode(filename);
            catch err
                throw(err);
            end
        end
    end

    methods (Access = protected) % overrides of NamedNode Abstract methods
        function errorIfInvalidName(~, newName)
            p = parallel.Settings;
            if p.hProfileExists(newName)
                error(message('parallel:settings:ProfileNameAlreadyExists', ... 
                    newName));
            end
        end
    end
    
    methods (Static, Access = private) % methods defined in other files    
        currValidator = currentValidator(newValidator)
    end
    
    methods (Access = private)
        function handleNameChanged(~, ~, namedChangedEventData)
            import parallel.settings.Profile
            validateattributes(namedChangedEventData, ...
                {'parallel.internal.settings.NameChangedEventData'}, {'scalar'})
                
            % Update the validator with our new name
            validator = Profile.currentValidator();
            if ~isempty(validator)
                oldStatus = validator.getStatus(namedChangedEventData.OldName);
                validator.deleteStatus(namedChangedEventData.OldName);
                validator.setStatus(namedChangedEventData.NewName, oldStatus);
            end
        end

        function handleNodeDestroyed(obj, ~, ~)
            import parallel.settings.Profile
            % Remove our status from the validator since the underlying Settings Node
            % has disappeared and the validation status is no longer valid
            validator = Profile.currentValidator();
            if ~isempty(validator)
                validator.deleteStatus(obj.Name);
            end
        end
    end
end
%#ok<*ATUNK> custom attributes
