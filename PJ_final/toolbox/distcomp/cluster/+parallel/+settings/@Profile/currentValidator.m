% currentValidator - get or set the current validator

%   Copyright 2011 The MathWorks, Inc.

function currValidator = currentValidator(newValidator)
% mlock the file to prevent the validator from being cleared
mlock
persistent validator
currValidator = validator;
if nargin == 1
    validator = newValidator;
end
end
