function hSubmitCommunicatingJob( pbs, job, jobSupport, jobSupportID )
% hSubmitCommunicatingJob - submit communicating job to PBS

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.internal.cluster.SubmissionChecks
import parallel.internal.cluster.CJSJobMethods
import parallel.internal.apishared.PbsHelper
import parallel.internal.apishared.WorkerCommand

pbs.ensureLicenseNumberIsSetOrError();
SubmissionChecks.checkCommunicatingTasksBeforeSubmission( job );

if ~pbs.SharedFilesystem
    error(message('parallel:cluster:PbsCommunicatingSharedFilesystem'));
end

maxW = SubmissionChecks.checkWorkerLimsForCommunicatingJob( ...
    pbs, job );
CJSJobMethods.duplicateTasksOfCommunicatingJob( ...
    job, jobSupport, jobSupportID, maxW );
jobSupport.prepareJobForSubmission( job, jobSupportID );

jobIsCommunicating = true;
tasks    = job.Tasks;
numTasks = numel( tasks );

[envNames, envVals] = jobSupport.getJobSubmissionEnvVars( jobSupportID );
[cNames, cVals]     = pbs.getClusterEnvVarsForJobSubmission();
envNames = [envNames(:); cNames(:)];
envVals  = [envVals(:);  cVals(:)];
envMap = [ envNames(:), envVals(:) ];
% Set up the decode function.
envMap = [envMap; { 'MDCE_DECODE_FUNCTION', 'parallel.internal.decode.pbsSimpleParallelTask' }];

% For parallel execution, we simply forward ClusterMatlabRoot to the cluster
% using an environment variable. This means we don't need to worry about
% quoting it here.
envMap = [envMap; { 'MDCE_CMR', pbs.ClusterMatlabRoot }];

% For parallel execution, the wrapper script chooses which matlab executable
% to launch via mpiexec, so we only pass the options that need to be added
% to the MATLAB command-line.
[~, matlabExe, matlabArgs] = WorkerCommand.defaultCommand( ...
    pbs.OperatingSystem, pbs.ClusterMatlabRoot, jobIsCommunicating );
envMap = [envMap; { 'MDCE_MATLAB_EXE', matlabExe }];
envMap = [envMap; { 'MDCE_MATLAB_ARGS', matlabArgs }];
envMap = [envMap; { 'MDCE_TOTAL_TASKS', num2str( numTasks ) }];
envMap = [envMap; { 'MDCE_REMSH', pbs.RshCommand }];
if pbs.UsePbsAttach
    envMap = [envMap; { 'MDCE_USE_ATTACH', 'on' }];
else
    envMap = [envMap; { 'MDCE_USE_ATTACH', 'off' }];
end

% Everything to do with setting up the qsub command-line arguments
selectStr = PbsHelper.calculateResourceArg( pbs.ResourceTemplate, numel( job.Tasks ) );

actuallyUseJobArrays = false;
helper = PbsHelper( pbs.OperatingSystem, pbs.ResourceTemplate, actuallyUseJobArrays, ...
                    pbs.UsePbsAttach, pbs.RcpCommand, pbs.Support.getRootStorageStruct(), ...
                    job.Id, {} );

[logLocation, relLog, absLog] = helper.chooseCommJobLog( jobSupportID );

[clientWrapper, doAppend] = helper.copyParallelWrapper( ...
    jobSupportID, pbs.ResolvedCommunicatingJobWrapper );

cmdLineArgs = sprintf( '%s %s %s ', selectStr, pbs.SubmitArguments, logLocation );

% Make the shelled out call to bsub.
[FAILED, out, pbsJobId] = helper.submitCommunicatingJob( clientWrapper, cmdLineArgs, doAppend, envMap );
if FAILED
    error(message('parallel:cluster:PbsUnableToCallQsub', out));
end

schedulerData = struct('type', 'pbs', ...
                       'usingJobArray', actuallyUseJobArrays, ...
                       'skippedTaskIDs', [], ...
                       'pbsJobIds', { pbsJobId }, ...
                       'absLogLocation', absLog, ...
                       'relLogLocation', relLog );

job.JobSchedulerData = schedulerData;

% Now set the job actually running 
helper.releaseHold( pbsJobId );
end
