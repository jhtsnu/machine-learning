function hSubmitIndependentJob( pbs, job, jobSupport, jobSupportID )
% hSubmitIndependentJob - submit an independent job

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.internal.cluster.SubmissionChecks
import parallel.internal.apishared.PbsHelper
import parallel.internal.apishared.WorkerCommand

pbs.ensureLicenseNumberIsSetOrError();
jobIsCommunicating = false;
SubmissionChecks.checkIndependentTasksBeforeSubmission( job );
tasks = job.Tasks;

jobSupport.prepareJobForSubmission( job, jobSupportID );

[jsNames, jsVals] = jobSupport.getJobSubmissionEnvVars( jobSupportID );
envNames = jsNames; envVals = jsVals;
[~, matlabExe, matlabArgs] = WorkerCommand.defaultCommand( ...
    pbs.OperatingSystem, pbs.ClusterMatlabRoot, jobIsCommunicating );

envNames{end+1} = 'MDCE_MATLAB_EXE'; envVals{end+1} = matlabExe;
envNames{end+1} = 'MDCE_MATLAB_ARGS'; envVals{end+1} = matlabArgs;

if pbs.SharedFilesystem
    decodeFcn = 'parallel.internal.decode.pbsSingleTask';
else
    decodeFcn = 'parallel.internal.decode.pbsSingleZippedTask';
end
envNames{end+1} = 'MDCE_DECODE_FUNCTION'; envVals{end+1} = decodeFcn;

[cNames, cVals] = pbs.getClusterEnvVarsForJobSubmission();
envNames = [envNames(:); cNames(:)];
envVals  = [envVals(:);  cVals(:)];
envMap   = [envNames(:), envVals];
% Only use job arrays if there's more than one task. PBSPro cannot submit a
% single-element job array.
actuallyUseJobArrays = pbs.UseJobArrays && numel( tasks ) > 1;

% Build the PbsHelper we're going to use henceforth
taskIDs = cell2mat( get( job.Tasks, {'Id'} ) );
helper = PbsHelper( pbs.OperatingSystem, pbs.ResourceTemplate, actuallyUseJobArrays, ...
                    pbs.UsePbsAttach, pbs.RcpCommand, pbs.Support.getRootStorageStruct(), ...
                    job.Id, taskIDs );

% Everything to do with setting up complex replacements for the templates
% and the other command-line arguments.

headerEls = helper.calcHeaderElements();

% Define the location for logs to be returned NB: the function in PbsHelper must
% be able to calculate the same log location as CJSSupport. However, the code
% cannot be completely shared.
% TODO:later try to do something better with logs here.
[logArgs, relLog, absLog] = helper.chooseIndTaskLog( jobSupportID );

[cmdLineDirective, directive] = helper.getDirective();
cmdLineArgs = [ pbs.SubmitArguments, ' ', logArgs, ' ', cmdLineDirective ];

% Build the submission script from template elements
submissionScript = helper.chooseSubmitScriptName( jobSupportID );

fhSubScript = fopen( submissionScript, 'wt' );
if fhSubScript == -1
    error(message('parallel:cluster:PbsCannotWriteSubmissionScript', submissionScript));
end
closer = onCleanup( @() fclose( fhSubScript ) );

helper.addHeaderToScript( fhSubScript, headerEls, directive );

if ~pbs.SharedFilesystem
    helper.addCopyInToScript( fhSubScript );
end

helper.addExecutionToScript( fhSubScript );

if ~pbs.SharedFilesystem
    helper.addCopyOutToScript( fhSubScript );
end

% Ensure script has a trailing newline
fprintf( fhSubScript, '\n' );

% Before submitting we need to ensure that certain environment variables
% are no longer set otherwise PBS will copy them across and break the
% remote MATLAB startup - these are used by MATLAB startup to pick the
% matlabroot and toolbox path and they explicitly override any local
% settings.
storedEnv = distcomp.pClearEnvironmentBeforeSubmission();
cleanup = onCleanup( @() distcomp.pRestoreEnvironmentAfterSubmission( storedEnv ) );

if ~pbs.SharedFilesystem
    pbs.Support.serializeForSubmission( jobSupportID, job.Variant );
end

% Make the shelled out call to qsub
[FAILED, out, jobIDs] = helper.submitIndependentJob( submissionScript, cmdLineArgs, envMap );
if FAILED
    error(message('parallel:cluster:PBSUnableToFindService', out));
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Build the scheduler data
schedulerData = struct( 'type', 'pbs', ...
                        'usingJobArray', actuallyUseJobArrays, ...
                        'pbsJobIds', {jobIDs}, ...
                        'skippedTaskIDs', helper.SkippedTaskIDs, ...
                        'absLogLocation', absLog, ...
                        'relLogLocation', relLog );

job.JobSchedulerData = schedulerData;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Release the hold on the jobs
helper.releaseHold( jobIDs );
end
