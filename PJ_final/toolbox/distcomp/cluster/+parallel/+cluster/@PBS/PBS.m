% PBS Base class for PBSPro and Torque clusters

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) PBS < parallel.cluster.CJSCluster
    properties ( PCTGetSet, Transient, PCTConstraint = 'string' )
        %SubmitArguments Specify additional arguments to use when submitting jobs
        %   SubmitArguments is a string that is passed via the qsub command when
        %   submitting jobs.
        SubmitArguments

        %ResourceTemplate Define resources to request for communicating jobs
        %   The value of this property is used to build the resource selection
        %   portion of the qsub command, generally identified by the -l
        %   flag. The toolbox uses this to identify the number of tasks in a
        %   communicating job, and you might want to fill out other selection
        %   subclauses (such as the OS type of the workers). You should specify
        %   a value for this property that includes the literal string ^N^,
        %   which the toolbox will replace with the number of workers in the
        %   communicating job prior to submission.
        ResourceTemplate

        %RcpCommand Command to copy files to and from client
        RcpCommand

        %RshCommand Remote execution command used on worker nodes during communicating job
        RshCommand

        %CommunicatingJobWrapper Script that cluster runs to start workers
        %   When communicating jobs are submitted, this script file is submitted
        %   for execution. The script must contain commands to launch the
        %   communicating job. CommunicatingJobWrapper can be set to several
        %   special values which will automatically be expanded at submission
        %   time. These values are:
        %   'windows'           - use a built-in default wrapper script suitable for PBS
        %                         clusters with worker machines running Windows.
        %   'windowsNoDelegate' - use a built-in default wrapper script suitable for PBS
        %                         clusters with worker machines running Windows, where
        %                         mpiexec delegation is not supported.
        %   'unix'              - use a built-in default wrapper script suitable for PBS
        %                         clusters with worker machines running UNIX.
        %
        %   If you specify a value for 'OperatingSystem' in your profile or when
        %   building an PBS cluster, the CommunicatingJobWrapper will
        %   automatically be set to the same value. Otherwise,
        %   CommunicatingJobWrapper will be set to the same operating system as
        %   the client.
        %
        %   See also parallel.cluster.PBS.OperatingSystem.
        CommunicatingJobWrapper
    end
    properties ( GetAccess = protected, SetAccess = immutable )
        UseJobArrays
        UsePbsAttach
        StateIndicators
    end
    properties ( SetAccess = immutable )
        %Host Host of the cluster's headnode
        Host
    end

    properties ( Dependent, Hidden )
        ResolvedCommunicatingJobWrapper
    end
    methods
        function rcjw = get.ResolvedCommunicatingJobWrapper( cluster )
            doResolve = true;
            rcjw      = cluster.CommunicatingJobWrapper;
            switch lower( rcjw )
              case {'pc', 'pcnodelegate', 'windows', 'windowsnodelegate'}
                if any( strcmpi( rcjw, {'pcnodelegate', 'windowsnodelegate'} ) )
                    scriptTail = 'pbsParallelWrapperNoDelegate.bat';
                else
                    scriptTail = 'pbsParallelWrapper.bat';
                end
              case 'unix'
                scriptTail = 'pbsParallelWrapper.sh';
              otherwise
                % Treat as literal value
                doResolve = false;
            end
            if doResolve
                rcjw = fullfile( toolboxdir('distcomp'), 'bin', 'util', 'pbs', scriptTail );
            end
        end
    end
    
    methods ( Hidden )
        % Add Cluster Properties to Display
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            clusterPropertyMap = hGetDisplayItems@parallel.Cluster(obj, diFactory);
            
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'Profile', ...
                'Modified', ...
                'Host', ...
                'NumWorkers', ...
                'Separator', ...
                'JobStorageLocation', ...
                'ClusterMatlabRoot', ...
                'OperatingSystem', ...
                'SubmitArguments', ...
                'ResourceTemplate', ...
                'RcpCommand', ...
                'RshCommand', ...
                'CommunicatingJobWrapper', ...
                'RequiresMathWorksHostedLicensing'};
          
            % Add cluster specific items to map for display
            names = {...
                'SubmitArguments',...
                'ResourceTemplate', ...
                'RcpCommand', ...
                'RshCommand', ...
                'CommunicatingJobWrapper'};
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            clusterPropertyMap = [clusterPropertyMap; containers.Map(names, values)];                   
        end
        
        % Get the data necessary to create a new cluster equivalent to this one.
        function data = hGetMementoData(obj)
            data = struct('JobStorageLocation', obj.JobStorageLocation);
        end
        
        function hDestroyJob( cluster, job )
            cluster.hCancelJob( job );
        end
        function hDestroyTask( cluster, task )
            cluster.hCancelTask( task );
        end
        function ok = hCancelJob( cluster, job )
            ok = cluster.cancelHelper( job, [] );
        end
        function ok = hCancelTask( cluster, task )
            job = task.Parent;

            if isa( job, 'parallel.CommunicatingJob' )
                % For a communicating job, cancel the whole job.
                ok = cluster.cancelHelper( job, [] );
            else
                ok = cluster.cancelHelper( job, task.Id );
            end
        end
        function hSetProperty( obj, names, values )
        % Extra checks for properties that this object does not allow to be set.
            if ~iscell( names )
                names = { names };
                values = { values };
            end
            for ii = 1:length(names)
                name  = names{ii};
                value = values{ii};
                if strcmp( 'OperatingSystem', name ) && obj.isNotCurrentValue( name, value )
                    throwAsCaller( obj.buildInvalidValue( name ) );
                end
                obj.hSetProperty@parallel.cluster.CJSCluster( name, value );
            end
        end

        hSubmitCommunicatingJob( varargin )
        hSubmitIndependentJob( varargin )
    end
    methods ( Access = protected )
        function obj = PBS( configStruct, varargin )
            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.cluster.CJSCluster

            try
                [propsFromArgs, cProf] = ...
                    ConstructorArgsHelper.interpretClusterCtorArgs( ...
                        ?parallel.cluster.PBS, varargin{:} );
            catch E
                throw( E );
            end

            [dataLoc, dataLocValType] = CJSCluster.chooseDataLocation( propsFromArgs );

            obj@parallel.cluster.CJSCluster( configStruct.Type, ...
                                             cProf, dataLoc, dataLocValType );

            obj.PropStorage.addFactoryValue( 'SubmitArguments', '' );
            obj.PropStorage.addFactoryValue( 'ResourceTemplate', '-l nodes=^N^' );
            obj.PropStorage.addFactoryValue( 'RcpCommand', configStruct.FactoryRcp );
            obj.PropStorage.addFactoryValue( 'RshCommand', configStruct.FactoryRsh );

            % NB: OperatingSystem cannot be specified for PBS, so the following
            % is safe:
            obj.PropStorage.addFactoryValue( 'CommunicatingJobWrapper', ...
                                             obj.OperatingSystem );
            obj.applyConstructorArgs( propsFromArgs );

            obj.UseJobArrays    = configStruct.UseJobArrays;
            obj.UsePbsAttach    = configStruct.UsePbsAttach;
            obj.StateIndicators = configStruct.StateIndicators;
            obj.Host            = configStruct.Host;
        end

        function jobState = getJobState( obj, job, ~ )
            import parallel.internal.apishared.PbsHelper
            import parallel.internal.types.States
            import parallel.internal.cluster.JobStateHelper

            % If we get here, we know that the job is queued/running, and was
            % submitted by PBS.

            jobState = States.Unknown;
            jobData = job.JobSchedulerData;

            [anyRunning, anyPending, FAILED] = PbsHelper.getRunningPending( ...
                obj.StateIndicators, jobData.pbsJobIds );

            if FAILED
                % Already warned in iGetRunningPending
                return
            end

            % Now deal with the logic surrounding these
            % Any running indicates that the job is running
            if anyRunning
                jobState = States.Running;
                return
            end

            % We know numRunning == 0 so if there are some still pending then the
            % job must be queued again, even if there are some finished
            if anyPending
                jobState = States.Queued;
                return
            end
        end
        function [tf, jobData] = wasSubmittedByThisType( ~, job )
            jobData = job.JobSchedulerData;
            tf      = ~isempty( jobData ) && isequal( jobData.type, 'pbs' );
        end

        function logText = getDebugLogForIndependentTask( obj, iJob, task )
        % Unfortunately, we must override the CJSCluster default here to handle
        % ^array_index^ in the task log location.

            [tf, jobData] = obj.wasSubmittedByThisType( iJob );
            if ~tf
                % Not our job, go back to superclass
                logText = obj.getDebugLogForIndependentTask@parallel.cluster.CJSCluster( ...
                    iJob, task );
            else
                % Work out the array index from the task index
                import parallel.internal.apishared.PbsHelper

                taskSID = task.hGetSupportID();
                jobSID  = iJob.hGetSupportID();

                [~, arrayIndex] = PbsHelper.calcTaskIdentifier( task.Id, jobData );
                fnameRel = jobData.relLogLocation;
                fnameAbs = jobData.absLogLocation;
                fnameRel = strrep( fnameRel, '^array_index^', num2str( arrayIndex ) );
                fnameAbs = strrep( fnameAbs, '^array_index^', num2str( arrayIndex ) );

                logRoot  = obj.Support.getRootStorageDir();
                if isempty( fnameRel )
                    fnameCanonical = fnameAbs;
                else
                    fnameCanonical = fullfile( logRoot, fnameRel );
                end
                jobStateFromDisk = iJob.hGetProperty( 'StateEnum' );
                [logText{1}, logExists] = ...
                    obj.Support.readSpecificLogFile( jobStateFromDisk, fnameCanonical );
                logText{2} = obj.Support.readTaskJavaLogsIfPresent( ...
                    jobSID, {taskSID}, {task.Id} );
                logText{3}   = obj.getSpecificDebugLogForIndependentTask( iJob, task, logExists );
                logText      = sprintf( '%s', logText{:} );
            end
        end

        function logText = getSpecificDebugLogForCommunicatingJob( ~, job, ~ )
        % If we get here, we know that 'wasSubmittedByThisType' returned 'true',
        % so we can check with PBS to see where we're up to.
            import parallel.internal.apishared.PbsHelper

            jobData = job.JobSchedulerData;
            pbsId   = jobData.pbsJobIds{1};
            logText = PbsHelper.readQstatInfo( pbsId );
        end
        function logText = getSpecificDebugLogForIndependentTask( ~, job, task, ~ )
        % If we get here, we know that 'wasSubmittedByThisType' returned 'true',
        % so we can check with PBS to see where we're up to.
            import parallel.internal.apishared.PbsHelper

            jobData = job.JobSchedulerData;
            pbsId   = PbsHelper.calcTaskIdentifier( task.Id, jobData );
            logText = PbsHelper.readQstatInfo( pbsId );
        end

        function [names, values] = getClusterSpecificEnvVars( obj )
            % Store the scheduler type so the runprop on the far end can be correctly
            % constructed.
            mdceDebugName = 'MDCE_DEBUG';
            mdceDebug = getenv(mdceDebugName);
            if ~isempty(mdceDebug)
                names  = {mdceDebugName};
                values = {mdceDebug};
            else
                names  = {};
                values = {};
            end
            names  = ['MDCE_SCHED_TYPE', names];
            values = [class( obj ), values];
        end
    end

    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end

        function ok = cancelHelper( cluster, job, taskId )
        % cancelHelper - cancel a whole job, or a single task.

            import parallel.internal.apishared.PbsHelper

            try
                [typeOk, jobData] = cluster.wasSubmittedByThisType( job );
                if isempty( jobData )
                    ok = true;
                    return
                elseif ~typeOk
                    % Not our job
                    ok = false;
                    warning(message('parallel:cluster:PbsCannotCancelWrongType'));
                    return
                end

                allPbsJobIds = jobData.pbsJobIds;

                if isempty( taskId )
                    pbsJobIdsToCancel = allPbsJobIds;
                else
                    pbsJobIdsToCancel = {PbsHelper.calcTaskIdentifier( taskId, jobData )};
                end

                for ii = 1:length( pbsJobIdsToCancel )
                    killCommand = sprintf( 'qdel "%s"', pbsJobIdsToCancel{ii} );
                    [FAILED, out] = PbsHelper.system( killCommand );
                    ok = ~FAILED;
                    % Did PBS think it managed to kill this job?
                    if FAILED
                        % Some returns from PBS shouldn't really be treated as failures
                        acceptableFailure = PbsHelper.isAcceptableQdelError( out );
                        % Cancel succeeded if it was an ACCEPTABLE_ERROR
                        ok = acceptableFailure;
                        % Not an acceptable error - issue a warning, and return - don't attempt to
                        % qdel others.
                        if ~acceptableFailure
                            warning(message('parallel:cluster:PbsQdelFailed', out));
                            return
                        end
                    end
                end
            catch E
                ok = false;
                warning(message('parallel:cluster:PbsCancellationFailed', E.message));
            end
        end
    end
end
%#ok<*ATUNK>
