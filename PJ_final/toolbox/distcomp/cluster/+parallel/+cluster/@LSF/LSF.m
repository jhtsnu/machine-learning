%LSF Interact with a Platform LSF cluster
%    The LSF cluster type allows you to submit jobs to Platform LSF. The
%    Platform LSF client tools must be installed on your client machine.
%
%   parallel.cluster.LSF methods:
%      LSF                    - Create an LSF cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      getDebugLog            - Read output messages from communicating job or independent task
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.cluster.LSF properties:
%      ClusterMatlabRoot                - Path to MATLAB on the cluster
%      ClusterName                      - Name of the Platform LSF cluster
%      CommunicatingJobWrapper          - Script that cluster runs to start workers
%      HasSharedFilesystem              - Specify whether the client and cluster nodes share JobStorageLocation
%      Host                             - Host of the cluster's headnode
%      JobStorageLocation               - Folder where cluster stores job and task information
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use with MathWorks hosted licensing
%      Modified                         - True if any properties in this cluster have been modified
%      NumWorkers                       - Number of workers in the cluster
%      OperatingSystem                  - Operating System of the cluster
%      Profile                          - Profile used to build this cluster
%      RequiresMathWorksHostedLicensing - Cluster uses MathWorks hosted licensing
%      SubmitArguments                  - Specify additional arguments to use when submitting jobs
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%
%    See also parcluster, parallel.Cluster.

% Copyright 2011-2012 The MathWorks, Inc.

classdef (ConstructOnLoad = true) LSF < parallel.cluster.CJSCluster
    properties ( PCTGetSet, Transient, PCTConstraint = 'string' )
        %SubmitArguments Specify additional arguments to use when submitting jobs
        %   SubmitArguments is a string that is passed via the bsub command when
        %   submitting jobs.
        SubmitArguments

        %CommunicatingJobWrapper Script that cluster runs to start workers
        %   When communicating jobs are submitted, this script file is submitted
        %   for execution. The script must contain commands to launch the
        %   communicating job. CommunicatingJobWrapper can be set to several
        %   special values which will automatically be expanded at submission
        %   time. These values are:
        %   'windows'           - use a built-in default wrapper script suitable for Platform
        %                         Platform LSF clusters with worker machines running Windows.
        %   'windowsNoDelegate' - use a built-in default wrapper script suitable for Platform
        %                         LSF clusters with worker machines running Windows, where
        %                         mpiexec delegation is not supported.
        %   'unix'              - use a built-in default wrapper script suitable for Platform
        %                         LSF clusters with worker machines running UNIX.
        %
        %   If you specify a value for 'OperatingSystem' in your profile or when
        %   building an LSF cluster, the CommunicatingJobWrapper will
        %   automatically be set to the same value. Otherwise,
        %   CommunicatingJobWrapper will be set to the same operating system as
        %   the client.
        %
        %   See also parallel.cluster.LSF.OperatingSystem.
        CommunicatingJobWrapper
    end
    properties ( SetAccess = immutable )
        %Host Host of the cluster's headnode
        Host

        %ClusterName Name of the Platform LSF cluster
        ClusterName
    end

    properties ( Dependent, Hidden )
        ResolvedCommunicatingJobWrapper
    end
    methods
        function rcjw = get.ResolvedCommunicatingJobWrapper( cluster )
            doResolve = true;
            rcjw      = cluster.CommunicatingJobWrapper;
            switch lower( cluster.CommunicatingJobWrapper )
              case {'pc', 'pcnodelegate', 'windows', 'windowsnodelegate'}
                if any( strcmpi( rcjw, {'pcnodelegate', 'windowsnodelegate'} ) )
                    scriptTail = 'lsfWrapMpiexecNoDelegate.bat';
                else
                    scriptTail = 'lsfWrapMpiexec.bat';
                end
              case 'unix'
                scriptTail = 'lsfWrapMpiexec.sh';
              otherwise
                % Treat as literal value
                doResolve = false;
            end
            if doResolve
                rcjw = fullfile( toolboxdir('distcomp'), 'bin', 'util', scriptTail );
            end
        end
    end

    methods ( Hidden )
        % Add Cluster Properties to Display
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            clusterPropertyMap = hGetDisplayItems@parallel.Cluster(obj, diFactory);
            
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed. 
            propNames = {...
                'Profile', ...
                'Modified', ...
                'Host', ...
                'NumWorkers', ...
                'Separator', ...
                'JobStorageLocation', ...
                'ClusterMatlabRoot', ...
                'OperatingSystem', ...
                'ClusterName', ...
                'SubmitArguments', ...
                'CommunicatingJobWrapper', ...
                'RequiresMathWorksHostedLicensing'};
          
            % Add cluster specific items to map for display
            names = {...
                'ClusterName', ...
                'SubmitArguments', ...
                'CommunicatingJobWrapper'};
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            clusterPropertyMap = [clusterPropertyMap; containers.Map(names, values)];
        end
        
        % Get the data necessary to create a new cluster equivalent to this one.
        function data = hGetMementoData(obj)
            data = struct('JobStorageLocation', obj.JobStorageLocation);
        end
        
        function hDestroyJob( cluster, job )
            cluster.hCancelJob( job );
        end
        function hDestroyTask( cluster, task )
            cluster.hCancelTask( task );
        end
        function ok = hCancelJob( cluster, job )
            ok = cluster.cancelHelper( job, [] );
        end
        function ok = hCancelTask( cluster, task )
            job = task.Parent;

            if isa( job, 'parallel.CommunicatingJob' )
                % For a communicating job, cancel the whole job.
                ok = cluster.cancelHelper( job, [] );
            else
                ok = cluster.cancelHelper( job, task.Id );
            end
        end

        hSubmitCommunicatingJob( varargin )

        function hSubmitIndependentJob( lsf, job, jobSupport, jobSupportID )
            import parallel.internal.cluster.SubmissionChecks
            import parallel.internal.apishared.LsfUtils

            lsf.ensureLicenseNumberIsSetOrError();
            SubmissionChecks.checkIndependentTasksBeforeSubmission( job );
            jobSupport.prepareJobForSubmission( job, jobSupportID );

            % Create taskIDString from the Id's of the tasks in this job
            tasks = job.Tasks;
            numTasks = numel( tasks );
            if tasks(end).Id == numTasks
                taskIDString = sprintf( '1-%d', numTasks );
            else
                taskIDString = LsfUtils.makeTaskIdString( cell2mat( get( job.Tasks, {'Id'} ) ) );
            end

            % Note that in the ~HasSharedFilesystem case, the storage location will
            % be set, but ignored by the decode function.
            [jsNames, jsVals] = jobSupport.getJobSubmissionEnvVars( jobSupportID );
            [cNames, cVals]   = lsf.getClusterEnvVarsForJobSubmission();
            envNames = [jsNames(:); cNames(:)];
            envVals  = [jsVals(:);  cVals(:)];
            cellfun( @setenv, envNames, envVals );

            if lsf.HasSharedFilesystem
                [submitString, logRelToStorage] = ...
                    lsf.submitIndSharedJob( jobSupport, jobSupportID, taskIDString );
            else
                [submitString, logRelToStorage] = ...
                    lsf.submitIndNonSharedJob( jobSupport, jobSupportID, taskIDString );
            end

            lsfID = LsfUtils.callBsub( submitString );
            schedulerData = struct('type', 'lsf', ...
                                   'lsfID', lsfID, ...
                                   'logRelToStorage', logRelToStorage);
            job.JobSchedulerData = schedulerData;

            if ~lsf.HasSharedFilesystem
                lsf.Support.serializeForSubmission( jobSupportID, job.Variant );
            end

            LsfUtils.callBresume( lsfID, taskIDString );
        end
    end
    methods
        function obj = LSF( varargin )
        %LSF Create an LSF cluster instance
        %   parallel.cluster.LSF(p1, v1, p2, v2, ...) builds an LSF
        %   cluster with the specified property values. The properties can
        %   include any of the properties of the LSF cluster class, or a
        %   profile.
        %
        %   Example:
        %   % Create an LSF cluster using the 'myLSF' profile.
        %   myCluster = parallel.cluster.LSF('Profile', 'myLSF');
        %
        %   See also parcluster, parallel.Cluster.

            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.cluster.CJSCluster
            import parallel.internal.apishared.LsfUtils

            try
                [propsFromArgs, cProf] = ...
                    ConstructorArgsHelper.interpretClusterCtorArgs( ...
                        ?parallel.cluster.LSF, varargin{:} );
            catch E
                throw( E );
            end

            [dataLoc, dataLocValType] = CJSCluster.chooseDataLocation( propsFromArgs );

            obj@parallel.cluster.CJSCluster( 'LSF', ...
                                             cProf, dataLoc, dataLocValType );

            LsfUtils.environmentSetup();
            [masterName, clusterName] = LsfUtils.getMasterAndClusterNames();

            if isequal( obj.OperatingSystem, 'mixed' )
                % Can't auto-expand 'mixed', so pick client OS.
                thisOs     = parallel.internal.apishared.OsType.forClient();
                factoryCJW = thisOs.Api2Name;
            else
                factoryCJW = obj.OperatingSystem;
            end

            obj.PropStorage.addFactoryValue( 'SubmitArguments', '' );
            obj.PropStorage.addFactoryValue( 'CommunicatingJobWrapper', ...
                                             factoryCJW );
            obj.applyConstructorArgs( propsFromArgs );

            obj.Host        = masterName;
            obj.ClusterName = clusterName;
        end
    end
    methods ( Access = protected )

        [submitString, logRelToStorage] = submitIndSharedJob( varargin );
        [submitString, logRelToStorage] = submitIndNonSharedJob( varargin );

        function jobState = getJobState( ~, job, ~ )
            import parallel.internal.apishared.LsfUtils
            import parallel.internal.types.States
            import parallel.internal.cluster.JobStateHelper


            % If we get here, we know that the job is queued/running, and was
            % submitted by LSF.

            jobState = States.Unknown;
            jobData = job.JobSchedulerData;

            [FAILED, numPendRunFail, failedLsfIndex] = ...
                LsfUtils.getStateInformation( jobData.lsfID );

            if FAILED
                % Already warned in getStateInformation
                return
            end

            % Now deal with the logic surrounding these
            % Any running indicates that the job is running
            if numPendRunFail(2) > 0
                jobState = States.Running;
                return
            end

            % We know numRunning == 0 so if there are some still pending then the
            % job must be queued again, even if there are some finished
            if numPendRunFail(1) > 0
                jobState = States.Queued;
                return
            end

            % Deal with failed tasks
            if numPendRunFail(3) > 0

                jobState = States.Failed;

                % Find the correct tasks with those IDs
                tasks = job.Tasks;
                taskIds = cell2mat( get( tasks, {'Id'} ) );
                [~, failedTaskIdxs] = intersect( taskIds, failedLsfIndex );

                % note that in the case where failedTaskIdxs is empty
                % (e.g. parallel job), the for-loop still performs one iteration
                % with ii empty, which is not what we want.
                if ~isempty( failedTaskIdxs )
                    for ii = failedTaskIdxs
                        if tasks(ii).StateEnum ~= States.Finished
                            tasks(ii).hSetProperty( 'StateEnum', States.Failed );
                        end
                    end
                end
            end
            % CJSCluster takes care of hSetTerminalStateFromCluster if necessary.
        end
        function [tf, jobData] = wasSubmittedByThisType( ~, job )
            jobData = job.JobSchedulerData;
            tf      = ~isempty( jobData ) && isequal( jobData.type, 'lsf' );
        end

        function logText = getSpecificDebugLogForCommunicatingJob( ~, job, logExists )
        % If we get here, we know that 'wasSubmittedByThisType' returned 'true',
        % so we can check with LSF to see where we're up to.
            import parallel.internal.apishared.PbsHelper

            jobData = job.JobSchedulerData;
            lsfId   = jobData.lsfID;
            logText = iMaybeCallBread( job, lsfId, logExists );
        end
        function logText = getSpecificDebugLogForIndependentTask( ~, job, task, logExists )
        % If we get here, we know that 'wasSubmittedByThisType' returned 'true',
        % so we can check with LSF to see where we're up to.
            import parallel.internal.apishared.PbsHelper

            jobData = job.JobSchedulerData;
            lsfId   = sprintf( '%d[%d]', jobData.lsfID, task.Id );
            logText = iMaybeCallBread( task, lsfId, logExists );
        end

        function [names, values] = getClusterSpecificEnvVars( ~ )
            % No common environment variables to set for this LSF
            names = {};
            values = {};
        end
    end

    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end

        function ok = cancelHelper( cluster, job, taskId )
        % cancelHelper - cancel a whole job, or a single task.

            import parallel.internal.apishared.LsfUtils

            try
                [typeOk, jobData] = cluster.wasSubmittedByThisType( job );
                if isempty( jobData )
                    ok = true;
                    return
                elseif ~typeOk
                    % Not our job
                    ok = false;
                    warning(message('parallel:cluster:LsfCannotCancelWrongType'));
                    return
                end

                jobID = jobData.lsfID;

                if isempty( taskId )
                    jobIdToCancel = sprintf( '%d', jobID );
                else
                    jobIdToCancel = sprintf( '%d[%d]', jobID, taskId );
                end

                killCommand = sprintf( 'bkill "%s"', jobIdToCancel );
                [FAILED, out] = dctSystem( killCommand );
                ok = ~FAILED;
                % Did LSF think it managed to kill this job?
                if FAILED
                    % Some returns from LSF shouldn't really be treated as failures
                    acceptableFailure = LsfUtils.isAcceptableBkillError( out );
                    % Cancel succeeded if it was an ACCEPTABLE_ERROR
                    ok = acceptableFailure;
                    % Not an acceptable error - issue a warning
                    if ~acceptableFailure
                        warning(message('parallel:cluster:LsfBkillFailed', out));
                        return
                    end
                end
            catch E
                ok = false;
                warning(message('parallel:cluster:LsfCancellationFailed', E.message));
            end
        end
    end
end
%#ok<*ATUNK>

% --------------------------------------------------------------------------
function logText = iMaybeCallBread( entity, lsfId, logFileExists )
    entityState = entity.hGetProperty( 'StateEnum' );
    entityTerminal = entityState.isTerminal();
    logText = '';

    if ~logFileExists && ~entityTerminal
        % No log, could use 'bpeek'
        warning(message('parallel:cluster:LsfLogNotYetPresent', lsfId));
    elseif entityTerminal && ~logFileExists
        % Attempt to use 'bread'
        warning(message('parallel:cluster:LsfLogNotPresent'));

        done = false;
        msgIdx = 0;
        while ~done
            [status, out] = dctSystem( sprintf( 'bread -i %d "%s"', msgIdx, lsfId ) );
            if status == 0
                logText = sprintf( '%s\n%s', logText, out );
                msgIdx = msgIdx + 1;
            else
                done = true;
            end
        end
    else
        % Nothing
    end
end
