function [submitString, logRelToStorage] = submitIndSharedJob( lsf, jobSupport, jobSupportID, taskIDString )
% submitIndSharedJob - shared filesystem independent job submission

% Copyright 2011 The MathWorks, Inc.

import parallel.internal.apishared.FilenameUtils
import parallel.internal.apishared.LsfUtils
import parallel.internal.apishared.WorkerCommand

% Define the function that will be used to decode the environment variables
setenv('MDCE_DECODE_FUNCTION', 'parallel.internal.decode.lsfSingleTask');

% Needed for calls to jobSpecificFilename
dataLocStruct = jobSupport.getRootStorageStruct();

% Log names and arguments
[~, clusterLog] = FilenameUtils.jobSpecificFilename( ...
    lsf.OperatingSystem, dataLocStruct, jobSupportID, 'Task%I.log', true );
logRelToStorage = [jobSupportID, '/Task%I.log'];

if isempty( clusterLog )
    logArg = '';
else
    logArg = ['-o ', FilenameUtils.quoteForClient( clusterLog )];
end

jobIsCommunicating = false;
[~, matlabExe, matlabArgs] = WorkerCommand.defaultCommand( ...
    lsf.OperatingSystem, lsf.ClusterMatlabRoot, jobIsCommunicating );
matlabExe = FilenameUtils.quoteForClient( matlabExe );

submitString = sprintf(...
    'bsub -H -J "%s[%s]" %s  %s  %s  %s'  , ...
    jobSupportID, ...
    taskIDString, ...
    logArg, ...
    lsf.SubmitArguments, ...
    matlabExe, ...
    matlabArgs);

end
