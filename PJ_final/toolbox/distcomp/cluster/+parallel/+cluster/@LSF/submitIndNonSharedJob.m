function [submitString, logRelToStorage] = submitIndNonSharedJob( lsf, jobSupport, jobSupportID, taskIDString )
% submitIndNonSharedJob - submit independent, non-shared-filesystem job

% Copyright 2011 The MathWorks, Inc.

import parallel.internal.apishared.FilenameUtils
import parallel.internal.apishared.LsfUtils
import parallel.internal.apishared.WorkerCommand

% Define the function that will be used to decode the environment variables
setenv('MDCE_DECODE_FUNCTION', 'parallel.internal.decode.lsfSingleZippedTask');

jobIsCommunicating = false;
[~, matlabExe, matlabArgs] = WorkerCommand.defaultCommand( ...
    lsf.OperatingSystem, lsf.ClusterMatlabRoot, jobIsCommunicating );
matlabExe = FilenameUtils.quoteForClient( matlabExe );

% Storage on client
storageLocationOnClient = jobSupport.getRootStorageDir();

% Currently LSF doesn't correctly transfer files if we place a space in
% the -f argument to bsub so we need to throw an error rather than 
% proceed if this is the case - see GECK 322231
if any(isspace(storageLocationOnClient))
    error(message('parallel:cluster:LsfInvalidDataLocation'));
end

[filesToCopy, remoteTaskLoc, logRelToStorage] = ...
    LsfUtils.calcFilesToCopy( storageLocationOnClient, jobSupportID );

logLocation = ['-o ' FilenameUtils.quoteForClient( [remoteTaskLoc '.log'] ) ];

submitString = sprintf(...
    'bsub -H -J "%s[%s]" %s  %s  %s  %s %s'  , ...
    jobSupportID, ...
    taskIDString, ...
    logLocation, ...
    filesToCopy, ...
    lsf.SubmitArguments, ...
    matlabExe, matlabArgs);
end
