function hSubmitCommunicatingJob( lsf, job, jobSupport, jobSupportID )
% hSubmitCommunicatingJob - submit a communicating job to LSF.

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.internal.apishared.LsfUtils
import parallel.internal.apishared.FilenameUtils
import parallel.internal.cluster.SubmissionChecks
import parallel.internal.cluster.CJSJobMethods
import parallel.internal.apishared.WorkerCommand

if ~lsf.SharedFilesystem
    error(message('parallel:cluster:LsfCommJobSharedFilesystem'));
end
if strcmp( lsf.OperatingSystem, 'mixed' )
    error(message('parallel:cluster:LsfCommJobHomogeneousOs'));
end

lsf.ensureLicenseNumberIsSetOrError();
SubmissionChecks.checkCommunicatingTasksBeforeSubmission( job );

maxW = SubmissionChecks.checkWorkerLimsForCommunicatingJob( ...
    lsf, job );
CJSJobMethods.duplicateTasksOfCommunicatingJob( ...
    job, jobSupport, jobSupportID, maxW );
jobSupport.prepareJobForSubmission( job, jobSupportID );

[envNames, envVals] = jobSupport.getJobSubmissionEnvVars( jobSupportID );
[cNames, cVals]     = lsf.getClusterEnvVarsForJobSubmission();
envNames = [envNames(:); cNames(:)];
envVals  = [envVals(:);  cVals(:)];
% Set the environment immediately
cellfun( @setenv, envNames, envVals );

% Set up the decode function.
setenv( 'MDCE_DECODE_FUNCTION', 'parallel.internal.decode.lsfSimpleParallelTask' );

% For parallel execution, we simply forward ClusterMatlabRoot to the cluster
% using an environment variable. This means we don't need to worry about
% quoting it here.
if ~isempty( lsf.ClusterMatlabRoot )
    setenv( 'MDCE_CMR', lsf.ClusterMatlabRoot );
else
    setenv( 'MDCE_CMR', '' );
end

% For parallel execution, the wrapper script chooses which matlab executable
% to launch via mpiexec, so we only pass the options that need to be added
% to the MATLAB command-line.
jobIsCommunicating = true;
matlabCommand = WorkerCommand.defaultCommand( ...
    lsf.OperatingSystem, lsf.ClusterMatlabRoot, jobIsCommunicating );

scriptOnCluster = LsfUtils.copyWrapperScript( ...
    lsf.ResolvedCommunicatingJobWrapper, lsf.OperatingSystem, ...
    jobSupport.getRootStorageStruct(), ...
    jobSupportID );
[absLog, relLog] = jobSupport.getCommJobLogFile( jobSupportID );

logArg = ['-o ', FilenameUtils.quoteForClient( absLog )];

% Here, we need to quote the scriptOnCluster in such a way that LSF will
% correctly execute that script. We need to quote the script name twice to
% protect against the system command AND LSF's execution of that command.
if ispc
    % Use a backslash-protected double-quote
    quotedScript = ['"\"', scriptOnCluster, '\""'];
else
    % Use a single-quote-protected double-quote
    quotedScript = ['''"', scriptOnCluster, '"'''];
end

submitString = sprintf( 'bsub -H -J %s -n %d,%d %s %s %s %s', ...
                        jobSupportID, job.NumWorkersRange(1), ...
                        maxW, ...
                        logArg, ...
                        lsf.SubmitArguments, ...
                        quotedScript, matlabCommand );

lsfID = LsfUtils.callBsub( submitString );

schedulerData = struct('type', 'lsf', 'lsfID', lsfID, 'logRelToStorage', relLog);
job.JobSchedulerData = schedulerData;
LsfUtils.callBresume( lsfID )
end
