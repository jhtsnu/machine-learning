%MJS Interact with an MJS cluster
%   The MJS cluster type provides access to the MATLAB Job Scheduler, which
%   controls the job queue, distributes job tasks to workers or labs for
%   execution, and maintains job results. 
%
%   parallel.cluster.MJS methods:
%      MJS                    - Create a MJS cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      changePassword         - Prompt to change password
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      demote                 - Demote job in the queue
%      findJob                - Find job objects belonging to a cluster object
%      isequal                - True if clusters have same property values
%      logout                 - Log user out
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      pause                  - Pause processing of job queue
%      promote                - Promote job in the queue
%      resume                 - Resume processing of job queue
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.cluster.MJS properties:
%      AllHostAddresses                 - IP addresses of the cluster host
%      BusyWorkers                      - Workers currently running tasks
%      ClusterMatlabRoot                - Specifies the path to MATLAB that the cluster is using
%      HasSecureCommunication           - True if the cluster is using secure communication
%      Host                             - Host on which this cluster is running
%      IdleWorkers                      - Workers that are free to run tasks
%      JobStorageLocation               - Location where job and task data is stored
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use when running jobs with this cluster
%      Modified                         - True if any properties in this cluster have been modified
%      Name                             - Name of this cluster
%      NumBusyWorkers                   - Number of workers currently running tasks
%      NumIdleWorkers                   - Number of workers that are free to run tasks
%      NumWorkers                       - Number of workers running on this cluster
%      OperatingSystem                  - Operating system of the nodes used by the cluster
%      Profile                          - Profile used to build this cluster
%      PromptForPassword                - True if the system should prompt for a password when authenticating user
%      RequiresMathWorksHostedLicensing - True if the cluster is using the MathWorks Hosted License Manager
%      SecurityLevel                    - Indicates the degree of security applied to the cluster and its jobs
%      State                            - Current state of the cluster
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%      Username                         - User who is using the cluster
%
%   See also PARCLUSTER, parallel.Cluster.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true, Sealed ) MJS < parallel.Cluster
    
    % An MJSSupport providing access to the underlying functionality
    properties ( Transient, GetAccess = private, SetAccess = immutable )
        Support
    end

    % Profile/Construction properties
    properties ( PCTGetSet, Transient, SetAccess = immutable, PCTConstraint = 'string' )
        %Name Name of this cluster 
        %   The descriptive name of an MJS cluster is set when its service
        %   is started, as described in "Customize Services" in the
        %   MATLAB Distributed Computing Server System Administrator's
        %   Guide. You can use the name of the cluster in a profile to
        %   search for the particular service when creating a cluster
        %   object with the parcluster function or the cluster constructor.
        %
        %    See also PARCLUSTER, parallel.cluster.MJS.MJS.
        Name = '';
        
        %Host Host on which this cluster is running
        %   You can use the host of the cluster in a profile to find a
        %   desired cluster when creating the cluster object with
        %   parcluster or the cluster constructor.
        %
        %   See also PARCLUSTER, parallel.cluster.MJS.MJS,
        %            parallel.cluster.MJS.AllHostAddresses,
        %            parallel.cluster.MJS.OperatingSystem.
        Host = '';
        
    end
    % Profile/Construction/Settable properties
    properties ( PCTGetSet, Transient, PCTConstraint = 'string' )
        %Username User who is using the cluster
        %   The Username property value indicates the user who created the
        %   cluster object or who is using the object to access jobs in its
        %   queue.
        %
        %   See also parallel.cluster.MJS.SecurityLevel,
        %            parallel.cluster.MJS.PromptForPassword,
        %            parallel.cluster.MJS.changePassword,
        %            parallel.cluster.MJS.logout.
        Username = '';

        %LicenseNumber License number to use when running jobs with this cluster
        %   License number specifies the license number to use on the
        %   workers when the cluster is using MathWorks hosted licensing.
        %   This string must be the same as one of the licenses listed in
        %   your MathWorks account.
        %
        %   LicenseNumber cannot be defined if
        %   RequiresMathWorksHostedLicensing is false.
        %    
        %   See also parallel.cluster.MJS.RequiresMathWorksHostedLicensing
        LicenseNumber = '';
    end
    
    properties ( PCTGetSet, Transient, SetAccess = immutable, PCTConstraint = 'string', Hidden )
        Certificate
    end
    properties ( Transient, Hidden )
        ClusterLogLevel;
    end
    
    % MJS Clusters have a variety of properties that are Dependent and derived by
    % querying the Support object.
    properties ( Dependent, SetAccess = private )
        %BusyWorkers Workers currently running tasks
        %   The BusyWorkers property value indicates which workers are
        %   currently running tasks for the cluster.
        %   (read-only)
        %
        %   As workers complete tasks and assume new ones, the lists of
        %   workers in BusyWorkers and IdleWorkers can change rapidly. If
        %   you examine these two properties at different times, you might
        %   see the same worker on both lists if that worker has changed
        %   its status between those times. If a worker stops unexpectedly,
        %   the cluster's knowledge of that as a busy or idle worker does
        %   not get updated until the cluster runs the next job and tries
        %   to send a task to that worker
        % 
        %   See also parallel.cluster.MJS.NumWorkers,
        %            parallel.cluster.MJS.IdleWorkers,
        %            parallel.cluster.MJS.NumBusyWorkers, 
        %            parallel.cluster.MJS.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        BusyWorkers                = [];
        
        %IdleWorkers Workers that are free to run tasks
        %   The IdleWorkers property value indicates which workers are
        %   currently not working on any tasks and are available to the
        %   cluster for the performance of job tasks. 
        %   (read-only)
        %
        %   As workers complete tasks and assume new ones, the lists of
        %   workers in BusyWorkers and IdleWorkers can change rapidly. If
        %   you examine these two properties at different times, you might
        %   see the same worker on both lists if that worker has changed
        %   its status between those times. If a worker stops unexpectedly,
        %   the cluster's knowledge of that as a busy or idle worker does
        %   not get updated until the cluster runs the next job and tries
        %   to send a task to that worker.
        % 
        %   See also parallel.cluster.MJS.NumWorkers,
        %            parallel.cluster.MJS.BusyWorkers,
        %            parallel.cluster.MJS.NumBusyWorkers, 
        %            parallel.cluster.MJS.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        IdleWorkers                = [];
        
        %NumBusyWorkers Number of workers currently running tasks
        %   The NumBusyWorkers property value indicates how many workers
        %   are currently running tasks for the cluster.
        %   (read-only)
        %
        %   The value of NumBusyWorkers can range from 0 up to the total
        %   number of workers registered with the cluster.
        %
        %   See also parallel.cluster.MJS.NumWorkers,
        %            parallel.cluster.MJS.BusyWorkers,
        %            parallel.cluster.MJS.IdleWorkers, 
        %            parallel.cluster.MJS.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        NumBusyWorkers             = 0;
        
        %NumIdleWorkers Number of workers that are free to run tasks
        %   The NumIdleWorkers property value indicates how many workers
        %   are currently not working on any tasks and are available to the
        %   cluster for the performance of job tasks. If the NumIdleWorkers
        %   is equal to or greater than the lower value of NumWorkersRange
        %   of the job at the front of the queue, that job can start
        %   running. (read-only)
        %
        %   The value of NumIdleWorkers can range from 0 up to the total
        %   number of workers registered with the cluster.
        %
        %   See also parallel.cluster.MJS.NumWorkers,
        %            parallel.cluster.MJS.BusyWorkers,
        %            parallel.cluster.MJS.IdleWorkers, 
        %            parallel.cluster.MJS.NumBusyWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        NumIdleWorkers             = 0;
        
        %State Current state of the cluster
        %   A cluster can be in state paused, running, or unavailable.
        %   (read-only)
        %
        %   See also parallel.cluster.MJS.pause,
        %            parallel.cluster.MJS.resume.
        State                      = 'pending';
    end
    properties ( Dependent )
        %NumWorkers Number of workers running on this cluster
        %   NumWorkers indicates the total number of workers available to
        %   the cluster for running your jobs. NumWorkers must fall within
        %   the range defined by a job's NumWorkersRange. 
        %   (read-only)
        %
        %   See also parallel.cluster.MJS.BusyWorkers,
        %            parallel.cluster.MJS.IdleWorkers, 
        %            parallel.cluster.MJS.NumBusyWorkers, 
        %            parallel.cluster.MJS.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        NumWorkers

        %ClusterMatlabRoot Specifies the path to MATLAB that the cluster is using
        %   ClusterMatlabRoot specifies the path name to MATLAB for the
        %   cluster to use for starting MATLAB worker processes. 
        %   (read-only)
        %
        %   See also parallel.cluster.MJS.JobStorageLocation
        ClusterMatlabRoot

        %OperatingSystem Operating system of the nodes used by the cluster
        %   OperatingSystem can be pc, unix or mixed.
        %   (read-only)
        %
        %   See also parallel.cluster.MJS.Name, parallel.cluster.MJS.Host.
        OperatingSystem

        %JobStorageLocation Location where job and task data is stored
        %   (read-only)
        %   
        %   See also parallel.cluster.MJS.ClusterMatlabRoot
        JobStorageLocation  
        
        %PromptForPassword True if the system should prompt for a password when authenticating user
        %   The PromptForPassword property is true by default, so that when
        %   you access a cluster object, if you do not already have a
        %   password stored, the system prompts you to enter it.
        %
        %   Setting PromptForPassword to false causes the system to
        %   generate an error when a password is required. This can be
        %   useful when you have a non-interactive script or function that
        %   programmatically accesses the cluster, and you might prefer an
        %   error rather than a password prompt.
        %
        %   See also parallel.cluster.MJS.SecurityLevel,
        %            parallel.cluster.MJS.Username,
        %            parallel.cluster.MJS.changePassword,
        %            parallel.cluster.MJS.logout.
        PromptForPassword         

        %RequiresMathWorksHostedLicensing True if the cluster is using the MathWorks Hosted License Manager
        %   RequiresMathWorksHostedLicensing indicates whether the cluster
        %   uses MathWorks hosted licensing.  You must provide your
        %   MathWorks account login information if this value is true.  If
        %   you have more than one MDCS license associated with your
        %   MathWorks account you will also need to supply the
        %   LicenseNumber to use.
        %   (read-only)
        %
        %   See also parallel.cluster.MJS.LicenseNumber.
        RequiresMathWorksHostedLicensing = false;
    end

    properties ( Transient, SetAccess = immutable )
        %AllHostAddresses IP addresses of the cluster host
        %   AllHostAddresses indicates the IP addresses of the computer
        %   running the cluster. You can use one of the IP addresses as the
        %   value for the host of the cluster in a profile to find a
        %   desired cluster when creating the cluster object with
        %   parcluster or the cluster constructor.
        %   (read-only)
        %
        %   See also PARCLUSTER, parallel.cluster.MJS.MJS, 
        %            parallel.cluster.MJS.Host,
        %            parallel.cluster.MJS.OperatingSystem.
        AllHostAddresses           = {};
        
        %HasSecureCommunication True if the cluster is using secure communication
        %   The HasSecureCommunication property indicates whether secure
        %   communication is being used between the job manager and the
        %   workers. The mdce_def file sets the parameter that controls
        %   secure communication when the mdce process starts on the
        %   cluster nodes.  
        %   
        %   Secure communication is required when running
        %   with SecurityLevel set to 3. It is optional at other security
        %   levels.
        %   (read-only)
        %
        %   See also parallel.cluster.MJS.SecurityLevel, 
        %            parallel.cluster.MJS.Username,
        %            parallel.cluster.MJS.PromptForPassword,
        %            parallel.cluster.MJS.changePassword,
        %            parallel.cluster.MJS.logout.
        HasSecureCommunication = false;
        
        %SecurityLevel Indicates the degree of security applied to the cluster and its jobs
        %   The SecurityLevel property indicates the degree of security
        %   applied to the cluster and its jobs. The mdce_def file sets
        %   the parameter that controls security level when the mdce
        %   process starts on the cluster nodes.
        %   (read-only)
        %
        %   The job manager and the workers should run at the same security
        %   level. A worker running at too low a security level will fail
        %   to register with the job manager, because the job manager does
        %   not trust it. 
        %
        %   SecurityLevel can have the following values:
        %   0 - No security. All users can access all jobs; the
        %       AuthorizedUsers property of the job is ignored.
        %   1 - You are warned when you try to access other users' jobs and
        %       tasks, but can still perform all actions. You can suppress
        %       the warning by adding your user name to the AuthorizedUsers
        %       property of the job.
        %   2 - Authentication required. You must enter a password to
        %       access any jobs and tasks. You cannot access other users'
        %       jobs unless your user name is included in the job's
        %       AuthorizedUsers property.
        %   3 - Same as level 2, but in addition, tasks run on the workers
        %       as the user to whom the job belongs. The user name and
        %       password for authentication in the client session need to
        %       be the same as the system password used to log on to a
        %       worker machine. NOTE: This level requires secure
        %       communication between job manager and workers. Secure
        %       communication is also set in the mdce_def file, and is
        %       indicated by a cluster's IsUsingSecureCommunication
        %       property.
        %
        %   See also parallel.cluster.MJS.HasSecureCommunication,
        %            parallel.cluster.MJS.Username,
        %            parallel.cluster.MJS.PromptForPassword,
        %            parallel.cluster.MJS.changePassword,
        %            parallel.cluster.MJS.logout.
        SecurityLevel              = 0;
    end

    % API2(orig) compatibility
    properties ( Dependent, Hidden )
        IsUsingSecureCommunication
    end
    methods
        function v = get.IsUsingSecureCommunication( obj )
            v = obj.HasSecureCommunication;
        end
    end
    
    % Access properties via the support. iGetClusterProps incorporates
    % handleJavaException.
    methods
        function v = get.State( obj )
            se = iGetClusterProps( obj, 'StateEnum' );
            v  = se.Name;
        end
        function v = get.NumBusyWorkers( obj )
            v = iGetClusterProps( obj, 'NumBusyWorkers' );
        end
        function v = get.NumIdleWorkers( obj )
            v = iGetClusterProps( obj, 'NumIdleWorkers' );
        end
        function cmr = get.ClusterMatlabRoot( obj )
            cmr = iGetClusterProps( obj, 'ClusterMatlabRoot' );
        end
        function set.ClusterMatlabRoot( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'ClusterMatlabRoot' ) );
        end
        function set.NumWorkers( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'NumWorkers' ) );
        end
        function set.JobStorageLocation( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'JobStorageLocation' ) );
        end
        function v = get.JobStorageLocation( obj )
            v = sprintf( 'Database on %s', obj.Host );
        end
        function v = get.OperatingSystem( obj )
            try 
                v = obj.Support.getOperatingSystem( obj );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end
        function set.OperatingSystem( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'OperatingSystem' ) );
        end
        function nw = get.NumWorkers( obj )
            nw = iGetClusterProps( obj, 'NumWorkers' );
        end
        function v = get.ClusterLogLevel( obj )
            v = iGetClusterProps( obj, 'ClusterLogLevel' );
        end
        function set.ClusterLogLevel( obj, logLevel )
            try
                validateattributes( logLevel, {'numeric'}, {'scalar', '>=', 0, '<=', 6}, 'setClusterLogLevel' );
                iSetClusterProps( obj, 'ClusterLogLevel', logLevel );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end

        % The MJSSupport.getWorkers() method does not handleJavaException, so we
        % must.
        function bw = get.BusyWorkers( obj )
            try
                bw = obj.Support.getWorkers( obj, 'BusyWorkers' );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end
        function iw = get.IdleWorkers( obj )
            try
                iw = obj.Support.getWorkers( obj, 'IdleWorkers' );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end
        function prompt = get.PromptForPassword( obj )
            prompt = iGetClusterProps( obj, 'PromptForPassword' );
        end
        function set.PromptForPassword( obj, promptForPassword )
            try
                parallel.internal.customattr.checkConstraint( 'logicalscalar', obj, 'PromptForPassword', promptForPassword );
                iSetClusterProps( obj, 'PromptForPassword', promptForPassword );
            catch E
                throwAsCaller( E );
            end
        end
        function tf = get.RequiresMathWorksHostedLicensing(obj)
            tf = iGetClusterProps( obj, 'RequiresMathWorksHostedLicensing' );
        end
        function set.RequiresMathWorksHostedLicensing(obj, ~)
            throwAsCaller( obj.buildCannotModify( 'RequiresMathWorksHostedLicensing' ) );
        end
    end

    methods
        function obj = MJS( varargin )
            %MJS Create a MJS cluster instance
            %   parallel.cluster.MJS(p1, v1, p2, v2, ...) builds an MJS
            %   cluster with the specified property values. The properties can
            %   include any of the properties of the MJS cluster class, or a
            %   profile.
            %
            %   See also PARCLUSTER, parallel.Cluster.

            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.internal.settings.ValueType
            import parallel.internal.cluster.MJSSupport

            if nargin == 1 ...
                    && isa( varargin{1}, ...
                        'parallel.internal.cluster.MJSSupport' )
                propsFromArgs   = containers.Map();
                cProf           = '';
                hostType        = ValueType.User;
                nameType        = ValueType.User;
                usernameType    = ValueType.Factory;   
                certificateType = ValueType.Factory;
                supportBuildFcn = @() varargin{1} ;
            elseif nargin == 2 ...
                    && isa( varargin{1}, ...
                    'com.mathworks.toolbox.distcomp.jobmanager.JobManagerLocal' ) ...
                    && isa( varargin{2}, ...
                    'com.mathworks.toolbox.distcomp.auth.credentials.Credentials' )
                % Constructor used by distcomp_evaluate_task
                proxy       = varargin{1};
                credentials = varargin{2};
                % The following properties are faked out since we don't have
                % normal arguments.
                propsFromArgs   = containers.Map();
                cProf           = '';
                hostType        = ValueType.Factory;
                nameType        = ValueType.Factory;
                usernameType    = ValueType.Factory;
                certificateType = ValueType.Factory;
                % Defer building the support object until we have a MJS
                % instance to pass to handleJavaException                
                supportBuildFcn = @()MJSSupport.buildOnWorker( proxy, credentials );
            else
                try
                    [propsFromArgs, cProf] = ConstructorArgsHelper.interpretClusterCtorArgs( ...
                        ?parallel.cluster.MJS, varargin{:} );
                catch E
                    % User error in args
                    throw( E );
                end
                [host, hostType] = iMaybeGetFromMapAndRemove( propsFromArgs, 'Host', [] );
                [name, nameType] = iMaybeGetFromMapAndRemove( propsFromArgs, 'Name', [] );
                [username, usernameType]       = iMaybeGetFromMapAndRemove( propsFromArgs, 'Username', iGetFactoryUsername() );
                [certificate, certificateType] = iMaybeGetFromMapAndRemove( propsFromArgs, 'Certificate', '' );

                promptForPassword = iMaybeGetFromMapAndRemove( propsFromArgs, 'PromptForPassword', true );                

                % Defer building the support object until we have a MJS
                % instance to pass to handleJavaException                
                supportBuildFcn = @()MJSSupport.build( host, name, ...
                    username, promptForPassword, certificate );
            end

            obj@parallel.Cluster( 'MJS', cProf );
            
            try 
                obj.Support = supportBuildFcn();
            catch E
                % Ideally wouldn't pass obj out until it is fully
                % constructed, but we know that handleJavaException is only
                % calling "isa"
                if strcmp( E.identifier, 'parallel:cluster:MJSMultipleFound' )
                    if isempty( cProf )
                        ex = MException(message('parallel:cluster:MJSMultipleFoundSpecifyHostInConstructor'));
                    else
                        ex = MException(message('parallel:cluster:MJSMultipleFoundSpecifyHostInProfile', cProf));
                    end
                    ex = ex.addCause( E );
                    throw( ex );
                else
                    throw( distcomp.handleJavaException( obj, E ) );
                end
            end

            factoryLicenseNumber    = '';
            actualHost              = iGetClusterProps( obj, 'Host');
            actualName              = iGetClusterProps( obj, 'Name');
            actualUsername          = iGetClusterProps( obj, 'Username');
            actualCertificate       = obj.Support.getEncodedCertificate();
            
            % Set up the PropStorage with the actual name and host, but indicate
            % where we got those values from.
            obj.PropStorage.addFactoryValue( 'Name', actualName );
            obj.PropStorage.setValueType(    'Name', nameType );
            obj.PropStorage.addFactoryValue( 'Host', actualHost );
            obj.PropStorage.setValueType(    'Host', hostType );
            obj.PropStorage.addFactoryValue( 'Username', actualUsername );
            obj.PropStorage.setValueType(    'Username', usernameType );
            obj.PropStorage.addFactoryValue( 'Certificate', actualCertificate );
            obj.PropStorage.setValueType(    'Certificate', certificateType );
            obj.PropStorage.addFactoryValue( 'LicenseNumber', factoryLicenseNumber );
            
            % Set up immutable properties
            obj.AllHostAddresses = ...
                iGetClusterProps( obj, 'AllHostAddresses' );
            obj.HasSecureCommunication = ...
                iGetClusterProps( obj, 'HasSecureCommunication' );
            obj.SecurityLevel = ...
                iGetClusterProps( obj, 'SecurityLevel' );
            
            % Ensure remaining constructor args go through the correct set methods.
            obj.applyConstructorArgs(propsFromArgs);

            % Finally, check two-way communications. We do not force a new check here
            % as the check will already have been performed by the ServiceAccessor.
            forceCheck = false;
            [commsOk, msg] = obj.hCheckTwoWayCommunications( forceCheck );
            if ~commsOk
                warning(message('parallel:cluster:MJSCommunicationProblem', msg));
            end
        end

        function pause( mjs )
            % PAUSE Pause processing of job queue
            %   PAUSE(myCluster) pauses the cluster's queue so that jobs
            %   waiting in the queued state will not run. Jobs that are
            %   already running also pause, after completion of tasks that
            %   are already running. No further jobs or tasks will run
            %   until the resume function is called for the cluster.
            %
            %   The pause function does nothing if the cluster is already
            %   paused.
            %
            %   See also parallel.cluster.MJS.resume,
            %            parallel.cluster.MJS.State.
            try
                mjs.Support.pauseQueue();
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end

        function resume( mjs )
            %RESUME Resume processing of job queue
            %   RESUME(myCluster) resumes processing of the cluster's queue
            %   so that jobs waiting in the queued state will be run. This
            %   call will do nothing if the cluster is not paused.
            %
            %   See also parallel.cluster.MJS.pause,
            %            parallel.cluster.MJS.State.
            try
                mjs.Support.resumeQueue();
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end
        
        function logout( mjs )
            %LOGOUT Log user out
            %    LOGOUT(myCluster) logs you out of the cluster. Any
            %    subsequent call to a privileged action will require you to
            %    re-authenticate with a valid password. Logging out might
            %    be useful after you have finished working on a shared
            %    machine.
            %
            %    See also parallel.cluster.MJS.changePassword,
            %             parallel.cluster.MJS.Username
            
            try
                % Empty UserIdentity means the "current user"
                userIdentity = [];
                mjs.Support.logout( userIdentity );
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end
        
        function changePassword( mjs, username )
            % changePassword Prompt to change password
            %   changePassword(myCluster) prompts you to change your
            %   password. Your current password must be entered as well as
            %   the new password.
            %
            %   changePassword(myCluster, userName) prompts the cluster
            %   admin user to change the password for the specified user.
            %   The admin user's password must be entered as well as the
            %   user's new password. The enables the admin user to reset a
            %   password if a user has forgotten it.
            %   
            %   See also parallel.cluster.MJS.logout,
            %            parallel.cluster.MJS.Username.
             
            import com.mathworks.toolbox.distcomp.auth.credentials.UserIdentity;
            if nargin > 1
                validateattributes( username, {'char'}, {}, 'changePassword', 'username' );
                userIdentity = UserIdentity( username );
            else
                % Empty UserIdentity means the "current user"                
                userIdentity = [];
            end
            try
                mjs.Support.changePassword( userIdentity );
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end            
        end
        
        function promote( mjs, job )
            %PROMOTE Promote job in the queue
            %   PROMOTE(myCluster, job) if job is not the first job in the
            %   queue, the position of job and the job that precedes it are
            %   exchanged.
            %
            %   After a call to promote or demote, there is no change in
            %   the order of job objects contained in the Jobs property of
            %   the cluster. To see the scheduled order of execution for
            %   jobs in the queue, use the findJob function in the form
            %   [pending queued running finished] = findJob(myCluster).
            %
            %   See also parallel.cluster.MJS.demote,
            %            parallel.cluster.MJS.findJob.
            validateattributes( job, {'parallel.Job'}, {'scalar'}, 'promote', 'job' );
            
            if mjs ~= job.Parent
                error(message('parallel:cluster:InvalidJobToPromote'));                
            end
            
            try
                % Call the job's promote method
                job.hPromote();
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end
        
        function demote( mjs, job )       
            %DEMOTE Demote job in the queue
            %   DEMOTE(myCluster, job) if job is not the last job in the
            %   queue, the position of job and the job that follows it are
            %   exchanged.
            %
            %   After a call to promote or demote, there is no change in
            %   the order of job objects contained in the Jobs property of
            %   the cluster. To see the scheduled order of execution for
            %   jobs in the queue, use the findJob function in the form
            %   [pending queued running finished] = findJob(myCluster).
            %
            %   See also parallel.cluster.MJS.promote,
            %            parallel.cluster.MJS.findJob.
            
            validateattributes( job, {'parallel.Job'}, {'scalar'}, 'demote', 'job' );
            
            if mjs ~= job.Parent
                error(message('parallel:cluster:InvalidJobToDemote'));                
            end
            
            try
                % Call the job's demote method
                job.hDemote();
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
                
        end        
    end

    methods ( Hidden )
        function hSetProperty( obj, names, values )
            if ~iscell( names )
                names  = { names };
                values = { values };
            end
            clusterProperties = {'PromptForPassword', 'Username'};
            licenseNumberProperty = {'LicenseNumber'};
            for ii = 1:length( names )
                if ismember( names{ii}, clusterProperties )
                    iSetClusterProps( obj, names{ii}, values{ii} );
                elseif ismember( names{ii}, licenseNumberProperty ) && ...
                        obj.isNotCurrentValue('LicenseNumber', values{ii})
                    % If the license number is different to the currently
                    % stored number then check its validity before setting
                    % and make sure we stash the corresponding entitlement.
                    try
                        obj.LicenseEntitlement = obj.checkLicenseNumber( values{ii} );
                    catch E
                        throwAsCaller( E )
                    end
                end                
                obj.hSetPropertyNoCheck( names{ii}, values{ii} );
            end
        end

        function [job, task] = hGetJobAndTaskFromUUIDs( obj, jobUUID, jobType, taskUUID )
        % Used by distcomp_evaluate_task to get the job and task to execute.
            try
                job  = obj.Support.getJobFromUUID( obj, jobUUID, jobType );
                task = obj.Support.getTaskFromUUID( job, jobUUID, taskUUID );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end

        function newjob = hBuildChild( mjs, variant, jobID, jobSId )
        % Build a new job object of the requested variant. Called by the support
        % which caches the resulting new job.

            import parallel.job.MJSIndependentJob
            import parallel.job.MJSCommunicatingJob
            import parallel.internal.types.Variant

            try
                switch variant
                  case Variant.IndependentJob
                    newjob = MJSIndependentJob( ...
                        mjs, jobID, jobSId, mjs.Support );
                  case { Variant.CommunicatingSPMDJob, ...
                         Variant.CommunicatingPoolJob }
                    newjob = MJSCommunicatingJob( ...
                        mjs, jobID, variant, jobSId, mjs.Support );
                  otherwise
                    error(message('parallel:cluster:MJSUnexpectedType'));
                end
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end
        
        % Add Cluster Properties to Display
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            clusterPropertyMap = hGetDisplayItems@parallel.Cluster(obj, diFactory);

            % The order the properties appear in the propNames array is the
            % order in which they will be displayed. 
            propNames = {...
                'Name', ...
                'Profile', ...
                'Modified', ...
                'Host', ...
                'Username', ...
                'Separator', ...
                'NumWorkers', ...
                'NumBusyWorkers', ...
                'NumIdleWorkers', ...
                'Separator', ...
                'JobStorageLocation', ...
                'ClusterMatlabRoot', ...
                'OperatingSystem', ...
                'AllHostAddresses', ...
                'SecurityLevel', ...
                'HasSecureCommunication', ...
                'RequiresMathWorksHostedLicensing'};

            % Use iGetClusterProps to get values for cluster props when 
            % possible, as requesting multiple properties simultaneously 
            % is much more efficient.
            names = {...
                'Name', ...
                'AllHostAddresses', ...
                'NumBusyWorkers', ...
                'NumIdleWorkers',...
                'Username', ...
                };
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names, @iGetClusterProps));
            clusterPropertyMap = [clusterPropertyMap; containers.Map(names, values)];
            
            % Add properties that are stored locally and/or not gotten with iGetClusterProps
            clusterPropertyMap('SecurityLevel') = diFactory.createDefaultItem(iSecurityLevelInfo(obj.hGetDisplayPropertiesNoError('SecurityLevel')));
            clusterPropertyMap('HasSecureCommunication') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('HasSecureCommunication'));
        end
        
        % Get the data necessary to create a new cluster equivalent to this one.
        function data = hGetMementoData(obj)
            data = struct('Host',     obj.Host,...
                          'Name',     obj.Name,...
                          'Username', obj.Username);
        end
        
         function hWaitForMJS( obj )	                
             try	               
                 obj.Support.waitForMJS( obj.Host, obj.Name );	                 
             catch E	                 
                 throw( distcomp.handleJavaException( obj, E ) )	     
             end
         end
        
        function [ok, msg] = hCheckTwoWayCommunications( obj, forceCheck )
        % Simply defer to the support.
            try
                [ok, msg] = obj.Support.checkTwoWayCommunications( forceCheck );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end

        function getClusterLogs( mjs, saveLocation )
        % Get the MJS logs from the job manager and save as a zip file
        % saveLocation.
            if nargin < 2
                saveLocation = '.';
            end
            filename = [mjs.Name '-logs.zip'];
            filename = fullfile( saveLocation, filename );
            try
                mjs.Support.getClusterLogs( filename );
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end
        
        function [p, q, r, f] = hEnumerateJobsInStates(obj)
        % Get the number of pending, queued, running and finished jobs
            try
                [p, q, r, f] = obj.Support.enumerateJobsInStates();
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end

    end

    methods ( Access = protected )
        function job = buildIndependentJob( mjs )
            import parallel.internal.types.Variant

            % Check that we are logged in as required.
            mjs.ensureLicenseNumberIsSetOrError();
            try
                job = mjs.Support.buildJob( mjs, Variant.IndependentJob, mjs.LicenseEntitlement );
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end
        function job = buildCommunicatingJob( mjs, variant )
            % Check that we are logged in as required.
            mjs.ensureLicenseNumberIsSetOrError();
            try
                job = mjs.Support.buildJob( mjs, variant, mjs.LicenseEntitlement );
            catch E
                throw( distcomp.handleJavaException( mjs, E ) );
            end
        end
        function jl = getJobs( obj )
            try
                jl = obj.Support.getJobs( obj );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function varargout = getJobsByState( obj )
            try
                [varargout{1:nargout}] = obj.Support.getJobsByState( obj );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function connMgr = buildConnectionManager( obj )
            try
                connMgr = obj.Support.buildConnectionManager( ...
                    obj.HasSecureCommunication );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function tf = isEquivalentWorkers( obj1, obj2 )
        % isEquivalentWorkers TRUE if Host and Name match.
            tf = isequal( obj1.Host, obj2.Host ) && ...
                 isequal( obj1.Name, obj2.Name );
        end

        function tf = isEquivalentStorage( obj1, obj2 )
        % isEquivalentStorage TRUE if Host and Name match.
            tf = isequal( obj1.Host, obj2.Host ) && ...
                 isequal( obj1.Name, obj2.Name );
        end
    end

    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
end
%#ok<*ATUNK> custom attributes

% --------------------------------------------------------------------------
function v = iGetClusterProps( obj, names )
% Simple wrapper to include handleJavaException
    try
        v = obj.Support.getClusterProperties( names );
    catch E
        throwAsCaller( distcomp.handleJavaException( obj, E ) );
    end
end

% --------------------------------------------------------------------------
function iSetClusterProps( obj, names, values )
% Simple wrapper to include handleJavaException
    try
        obj.Support.setClusterProperties( names, values );
    catch E
        throwAsCaller( distcomp.handleJavaException( obj, E ) );
    end
end

% --------------------------------------------------------------------------
% iMaybeGetFromMapAndRemove - simply return the value from the propsFromArgs map; return
% the source - 'Factory' if not in the map.  Remove the property from
% the map if it is in there.
function [val, type] = iMaybeGetFromMapAndRemove( map, prop, val )
    if map.isKey( prop )
        valtmp = map(prop);
        val    = valtmp{1};
        type   = valtmp{2};
        map.remove(prop);
    else
        type = parallel.internal.settings.ValueType.Factory;
    end
end

% --------------------------------------------------------------------------
function username = iGetFactoryUsername()
    import com.mathworks.toolbox.distcomp.auth.credentials.UserIdentity
    username = char( UserIdentity.createDefaultUserIdentity().getSimpleUsername );
end

% --------------------------------------------------------------------------
% iSecurityLevelInfo - return a string to be displayed for each security
% level.
function info = iSecurityLevelInfo( securityLevel )
    switch securityLevel
        case 0
            info = '0 (No security)';
        case 1
            info = {'1 (Jobs are identified with submitting user;', ...
                    'access by other users allowed, but generates', ...
                    'warning)'};
        case 2
            info = '2 (Jobs are password protected)';
        case 3
            info = {'3 (Jobs are password protected; passwords must', ...
                    'match credentials on worker machines because', ...
                    'tasks are executed as user on the worker)'};
        otherwise
            info = [];
    end
end
