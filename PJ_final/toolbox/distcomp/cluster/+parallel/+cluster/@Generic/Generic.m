%Generic Interact with a Generic cluster type
%   The Generic cluster type allows you to specify MATLAB functions to interact
%   with your cluster. You can specify functions to define how to submit jobs,
%   how to query job states, how to cancel running jobs, and how to perform any
%   additional clean-up required when deleting a job.
%
%   When you submit an independent job to a cluster, the function identified
%   by the cluster object's IndependentSubmitFcn property executes in the MATLAB
%   client session. You set the cluster's IndependentSubmitFcn property to
%   identify the submit function and any arguments you want to send to it.
%
%   Likewise, when you submit a communicating job to cluster, the function
%   identified by the cluster object's CommunicatingSubmitFcn property is used.
%
%   parallel.cluster.Generic methods:
%      Generic                - Create a generic cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      getDebugLog            - Read output messages from communicating job or independent task
%      getJobClusterData      - Get specific userdata for a job on a generic cluster
%      getJobFolder           - Return folder on the client where jobs are stored
%      getJobFolderOnCluster  - Return folder on cluster where jobs are stored
%      getLogLocation         - Get log location for a job or task
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%      setJobClusterData      - Set specific userdata for a job on a generic cluster
%
%   parallel.cluster.Generic properties:
%      CancelJobFcn                     - Function to run when cancelling a job
%      CancelTaskFcn                    - Function to run when cancelling a task
%      ClusterMatlabRoot                - Path to MATLAB on the cluster
%      CommunicatingSubmitFcn           - Function to run when submitting a communicating job
%      DeleteJobFcn                     - Function to run when deleting a job
%      DeleteTaskFcn                    - Function to run when deleting a task
%      GetJobStateFcn                   - Function to run when querying job state
%      HasSharedFilesystem              - Specify whether the client and cluster nodes share JobStorageLocation
%      Host                             - Identifies the headnode of this cluster
%      IndependentSubmitFcn             - Function to run when submitting an independent job
%      JobStorageLocation               - Folder where cluster stores job and task information
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use with MathWorks hosted licensing
%      Modified                         - True if any properties in this cluster have been modified
%      NumWorkers                       - Number of workers in the cluster
%      OperatingSystem                  - Operating System of the cluster
%      Profile                          - Profile used to build this cluster
%      RequiresMathWorksHostedLicensing - Cluster uses MathWorks hosted licensing
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%
%   See also parallel.Cluster, parcluster.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true ) Generic < parallel.cluster.CJSCluster

    properties ( PCTGetSet, Transient, PCTConstraint = 'callback' )
        %IndependentSubmitFcn Function to run when submitting an independent job
        %   IndependentSubmitFcn identifies the function to run when you submit
        %   an independent job to the generic cluster. The function runs in the
        %   MATLAB client. This user-defined submit function provides certain
        %   job and task data for the MATLAB worker, and identifies a
        %   corresponding decode function for the MATLAB worker to run.
        IndependentSubmitFcn   = [];

        %CommunicatingSubmitFcn Function to run when submitting a communicating job
        %   CommunicatingSubmitFcn identifies the function to run when you submit
        %   an communicating job to the generic cluster. The function runs in the
        %   MATLAB client. This user-defined submit function provides certain
        %   job and task data for the MATLAB worker, and identifies a
        %   corresponding decode function for the MATLAB worker to run.
        CommunicatingSubmitFcn = [];

        %GetJobStateFcn Function to run when querying job state
        %   Called when you call when you call wait, or any other function that
        %   queries the state of a job running on a generic cluster. This
        %   function lets you communicate with the cluster, to provide any
        %   instructions beyond the normal toolbox action of retrieving the job
        %   state from disk. To identify the job for the cluster, the function
        %   should include a call to getJobClusterData.
        GetJobStateFcn         = [];

        %CancelJobFcn Function to run when cancelling a job
        %   Called when you call CANCEL for a job running on a generic
        %   cluster. This function lets you communicate with the cluster, to
        %   provide any instructions beyond the normal toolbox action of
        %   changing the state of the job. To identify the job for the cluster,
        %   the function should include a call to getJobClusterData.
        CancelJobFcn           = [];

        %CancelTaskFcn Function to run when cancelling a task
        %   Called when you call CANCEL for a task running on a generic
        %   cluster. This function lets you communicate with the cluster, to
        %   provide any instructions beyond the normal toolbox action of
        %   changing the state of the task. To identify the task for the cluster,
        %   the function should include a call to getJobClusterData.
        CancelTaskFcn          = [];

        %DeleteJobFcn Function to run when deleting a job
        %   Called when you call DELETE for a job running on a generic
        %   cluster. This function lets you communicate with the cluster, to
        %   provide any instructions beyond the normal toolbox action of
        %   deleting the job from disk. To identify the job for the cluster,
        %   the function should include a call to getJobClusterData.
        DeleteJobFcn          = [];

        %DeleteTaskFcn Function to run when deleting a task
        %   Called when you call DELETE for a task running on a generic
        %   cluster. This function lets you communicate with the cluster, to
        %   provide any instructions beyond the normal toolbox action of
        %   deleting the task from disk. To identify the task for the cluster,
        %   the function should include a call to getJobClusterData.
        DeleteTaskFcn         = [];
    end
    properties ( Constant, GetAccess = private )
        AllCallbackNames = { ...
            'IndependentSubmitFcn', ...
            'CommunicatingSubmitFcn', ...
            'GetJobStateFcn', ...
            'CancelJobFcn', ...
            'CancelTaskFcn', ...
            'DeleteJobFcn', ...
            'DeleteTaskFcn' };
    end
    properties ( Transient, Access = private )
        JobsWithGetJobStateFcnRunning = [];
    end
    properties ( SetAccess = immutable )
        %Host Identifies the headnode of this cluster
        Host
    end

    methods
        function obj = Generic( varargin )
        %Generic Create a generic cluster instance
        %   parallel.cluster.Generic(p1, v1, p2, v2, ...) builds a generic
        %   cluster with the specified property values. The properties can
        %   include any of the properties of the Generic cluster class, or a
        %   profile.
        %
        %   Example:
        %   % Create a Generic cluster using the 'myGeneric' profile.
        %   myCluster = parallel.cluster.Generic('Profile', 'myGeneric');
        %
        %   See also parcluster, parallel.Cluster.

            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.cluster.CJSCluster
            import parallel.cluster.Generic

            try
                [propsFromArgs, cProf] = ...
                    ConstructorArgsHelper.interpretClusterCtorArgs( ...
                        ?parallel.cluster.Generic, varargin{:} );
            catch E
                throw( E );
            end

            [dataLoc, dataLocValType] = CJSCluster.chooseDataLocation( propsFromArgs );

            obj@parallel.cluster.CJSCluster( 'Generic', cProf, dataLoc, dataLocValType );
            obj.Host = char( java.net.InetAddress.getLocalHost.getCanonicalHostName() );

            % Seed factory values of callback properties
            cellfun( @(x) obj.PropStorage.addFactoryValue( x, [] ), ...
                     Generic.AllCallbackNames );

            obj.applyConstructorArgs( propsFromArgs );
        end

        function setJobClusterData( cluster, job, userdata )
        %setJobClusterData Set specific userdata for a job on a generic cluster
        %   setJobClusterData(C, J, DATA) stores DATA for the job J that is
        %   running under the generic cluster C. You can later retrieve this
        %   information with the function getJobClusterData. For example, it
        %   might be useful to store the third-party cluster's external ID for
        %   this job, so that the function specified in GetJobStateFcn can later
        %   query the cluster about the state of the job.
        %
        %   See also parallel.cluster.Generic/getJobClusterData.

            validateattributes( cluster, {'parallel.Cluster'}, {'scalar'} );
            validateattributes( job, {'parallel.job.CJSIndependentJob', ...
                                'parallel.job.CJSCommunicatingJob'}, {'scalar'} );
            try
                job.JobSchedulerData = struct( 'type', 'generic', ...
                                               'userdata', {userdata} );
            catch E
                throw( E );
            end
        end

        function userdata = getJobClusterData( cluster, job )
        %getJobClusterData Get specific userdata for a job on a generic cluster
        %   userdata = getJobClusterData(C, J) returns data stored for
        %   the job J that was derived from the generic cluster C. The
        %   information was originally stored with the function
        %   setJobClusterData. For example, it might be useful to store the
        %   third-party scheduler's external ID for this job, so that the
        %   function specified in GetJobStateFcn can later query the scheduler
        %   about the state of the job.
        %
        %
        %   See also parallel.cluster.Generic/setJobClusterData.

            validateattributes( cluster, {'parallel.Cluster'}, {'scalar'} );
            validateattributes( job, {'parallel.job.CJSIndependentJob', ...
                                'parallel.job.CJSCommunicatingJob'}, {'scalar'} );

            userdata = [];
            try
                if cluster.wasSubmittedByThisType( job )
                    struc    = job.JobSchedulerData;
                    userdata = struc.userdata;
                end
            catch E
                throw( E );
            end
        end

        function fname = getLogLocation( cluster, jobOrTask )
        %getLogLocation Get log location for a job or task
        %   logfile = getLogLocation(C, cJob) for a generic cluster C and
        %   communicating job cJob returns the location where the log data should
        %   be stored for the whole job cJob.
        %
        %   logfile = getLogLocation(C, IT) for a generic cluster C and task IT of
        %   an independent job returns the location where the log data should
        %   be stored for the task IT.
        %
        %   This function can be useful during submission to instruct the
        %   third-party cluster to put worker output logs in the correct
        %   location.

            validateattributes( cluster, {'parallel.cluster.Generic'}, {'scalar'} );

            if numel( jobOrTask ) == 1
                if isa( jobOrTask, 'parallel.CommunicatingJob' )
                    jobSupportID = jobOrTask.hGetSupportID();
                    fname = cluster.Support.getCommJobLogFile( jobSupportID );
                    return
                elseif isa( jobOrTask, 'parallel.Task' )
                    task = jobOrTask;
                    job  = task.Parent;
                    if isa( job, 'parallel.IndependentJob' )
                        jobSupportID = job.hGetSupportID();
                        fname = cluster.Support.getIndJobTaskLogFile( ...
                            jobSupportID, task.hGetSupportID() );
                        return
                    end
                end
            end
            % If we get here, there's a problem
            error(message('parallel:cluster:GenericLogLocation'));
        end

        function dirname = getJobFolder( cluster, job )
        %getJobFolder Return folder on the client where jobs are stored
        %   getJobFolder(c, j) for a generic cluster c and job j returns the
        %   folder on disk where files relating to job j are stored. This folder
        %   is valid only at the MATLAB client.
        %
        %   See also parallel.cluster.Generic/getJobFolderOnCluster.

            validateattributes( cluster, {'parallel.cluster.Generic'}, {'scalar'} );
            validateattributes( job, {'parallel.Job'}, {'scalar'} );
            dirname = cluster.Support.getJobDirOnClient( job.hGetSupportID() );
        end
        function dirname = getJobFolderOnCluster( cluster, job )
        %getJobFolderOnCluster Return folder on cluster where jobs are stored
        %   getJobFolder(c, j) for a generic cluster c and job j returns the
        %   folder on disk where files relating to job j are stored. This folder
        %   is valid on the cluster. An error is reported if the
        %   HasSharedFilesystem property of the cluster is FALSE.
        %
        %   See also parallel.cluster.Generic/getJobFolder.

            validateattributes( cluster, {'parallel.cluster.Generic'}, {'scalar'} );
            validateattributes( job, {'parallel.Job'}, {'scalar'} );
            if ~cluster.HasSharedFilesystem
                error(message('parallel:cluster:GenericJobDirOnCluster'));
            end
            try
                dirname = cluster.Support.getJobDirForOs( ...
                    job.hGetSupportID(), cluster.OperatingSystem );
            catch E
                throw( E );
            end
        end
    end
    
    methods ( Hidden )
        % Add Cluster Properties to Display
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            import parallel.cluster.Generic
            
            clusterPropertyMap = hGetDisplayItems@parallel.Cluster(obj, diFactory);
            
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'Profile', ...
                'Modified', ...
                'Host', ...
                'NumWorkers', ...
                'Separator', ...
                'JobStorageLocation', ...
                'ClusterMatlabRoot', ...
                'OperatingSystem', ...
                'Separator', ...
                'IndependentSubmitFcn', ...
                'CommunicatingSubmitFcn', ...
                'GetJobStateFcn', ...
                'CancelJobFcn', ...
                'CancelTaskFcn', ...
                'DeleteJobFcn', ...
                'DeleteTaskFcn', ...
                'RequiresMathWorksHostedLicensing'};
            
            % Add all callback functions to map for display
            names = Generic.AllCallbackNames;
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            clusterPropertyMap = [clusterPropertyMap; containers.Map(names, values)];
        end
        
        % Get the data necessary to create a new cluster equivalent to this one.
        function data = hGetMementoData(obj)
            data = struct('JobStorageLocation',  obj.JobStorageLocation, ...
                          'HasSharedFilesystem', obj.HasSharedFilesystem, ...
                          'GetJobStateFcn',      obj.GetJobStateFcn, ...
                          'OperatingSystem',     obj.OperatingSystem);
        end
        
        function hDestroyJob( cluster, job )
            cluster.deleteJobOrTask( 'DeleteJobFcn', job, job );
        end
        function hDestroyTask( cluster, task )
            cluster.deleteJobOrTask( 'DeleteTaskFcn', task.Parent, task );
        end
        function OK = hCancelJob( cluster, job )
            OK = cluster.cancelJobOrTask( 'CancelJobFcn', job, job );
        end
        function OK = hCancelTask( cluster, task )
            OK = cluster.cancelJobOrTask( 'CancelTaskFcn', task.Parent, task );
        end

        function hSubmitIndependentJob( cluster, job, jobSupport, jobSupportID )
            if isempty( cluster.IndependentSubmitFcn )
                error(message('parallel:cluster:GenericIndependentSubmitFcnRequired'));
            end
            jobIsCommunicating = false;
            fcnName            = 'IndependentSubmitFcn';
            cluster.submitJobCommon( job, jobSupport, jobSupportID, ...
                                     jobIsCommunicating, fcnName );
        end


        function hSubmitCommunicatingJob( cluster, job, jobSupport, jobSupportID )
            import parallel.internal.cluster.SubmissionChecks
            import parallel.internal.cluster.CJSJobMethods

            if isempty( cluster.CommunicatingSubmitFcn )
                error(message('parallel:cluster:GenericCommunicatingSubmitFcnRequired'));
            end

            SubmissionChecks.checkCommunicatingTasksBeforeSubmission( job );
            maxW = SubmissionChecks.checkWorkerLimsForCommunicatingJob( ...
                cluster, job );
            CJSJobMethods.duplicateTasksOfCommunicatingJob( ...
                job, jobSupport, jobSupportID, maxW );

            jobIsCommunicating = true;
            fcnName            = 'CommunicatingSubmitFcn';
            cluster.submitJobCommon( job, jobSupport, jobSupportID, ...
                                     jobIsCommunicating, fcnName );
        end
    end
    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end

        function tf = isSubmittedGenericClusterJob( cluster, job )
        % isSubmittedGenericClusterJob - job submitted by generic cluster
        % c.f. pIsSubmittedGenericSchedulerJob
            import parallel.internal.types.States

            jobStateEnumInStorage = job.hGetProperty( 'StateEnum' );
            tf = cluster.wasSubmittedByThisType( job ) && ...
                 jobStateEnumInStorage > States.Pending;
        end

        function submitJobCommon( cluster, job, jobSupport, jobSupportID, ...
                                  jobIsCommunicating, fcnName )
        % Common code for job submission, calls the appropriate user-supplied function.
            import parallel.internal.apishared.WorkerCommand

            cluster.ensureLicenseNumberIsSetOrError();
            taskSIds = arrayfun( @hGetSupportID, job.Tasks, 'UniformOutput', false );
            jobSupport.prepareJobForSubmission( job, jobSupportID );

            [submitFcn, args] = iGetFunction( cluster.(fcnName) );

            % TODO:later move this?
            storedEnv = distcomp.pClearEnvironmentBeforeSubmission();
            cleanup = onCleanup( @() distcomp.pRestoreEnvironmentAfterSubmission( storedEnv ) );

            % NB: we still use distcomp.setprop to transmit information to the SubmitFcn.
            setprop = jobSupport.getJobSubmissionSetprop( jobSupportID, taskSIds );
            [~, matlabExe, matlabArgs] = WorkerCommand.defaultCommand( ...
                cluster.OperatingSystem, cluster.ClusterMatlabRoot, jobIsCommunicating );
            setprop.pSetExecutableStrings( matlabExe, matlabArgs );
            token = '';
            webID = '';
            licenseNumber = '';
            if cluster.RequiresMathWorksHostedLicensing
                desktopClient = parallel.internal.webclients.currentDesktopClient();
                token = desktopClient.LoginToken;
                webID = cluster.LicenseEntitlement.Id;
                licenseNumber = cluster.LicenseEntitlement.LicenseNumber;
            end
            setprop.pSetLicensingInformation( cluster.RequiresMathWorksHostedLicensing, ...
                                              token, webID, licenseNumber);

            cluster.setJobClusterData( job, [] );
            try
                feval( submitFcn, cluster, job, setprop, args{:} );
            catch submitError
                cluster.setJobClusterData( job, [] );
                ME = MException(message('parallel:cluster:GenericSubmissionFailed', fcnName, iFunc2Str( submitFcn )));
                ME = addCause( ME, submitError );
                throw( ME );
            end
        end
        function OK = cancelJobOrTask( cluster, fcnName, job, entity )
            [userFcn, args] = iGetFunction( cluster.(fcnName) );

            % Define situations where we are NOT going to run any user function
            % 1. No callback function defined
            % 2. The job has not a submitted generic scheduler job
            shortcutCallbackFcn = isempty(userFcn) || ...
                ~cluster.isSubmittedGenericClusterJob(job);

            if shortcutCallbackFcn
                OK = true;
                return
            end

            % Call the user supplied function
            try
                OK = feval(userFcn, cluster, entity, args{:});
            catch err
                OK = false;
                warning(message('parallel:cluster:GenericCancelFcnError', fcnName, iFunc2Str( userFcn ), err.getReport()));
            end
            % Check that the user has returned a logical
            if ~(islogical(OK) && numel(OK) == 1)
                warning(message('parallel:cluster:GenericCancelFcnReturnError', fcnName, iFunc2Str( userFcn )));
                OK = false;
            end
        end

        function deleteJobOrTask( cluster, fcnName, job, entity )
            [userFcn, args] = iGetFunction( cluster.(fcnName) );
            % Define situations where we are NOT going to run any user function

            % 1. No callback function defined
            % 2. The job has not a submitted generic scheduler job
            shortcutCallbackFcn = isempty(userFcn) || ...
                ~cluster.isSubmittedGenericClusterJob(job);

            if shortcutCallbackFcn
                return
            end

            % Call the user supplied function
            try
                feval(userFcn, cluster, entity, args{:});
            catch err
                warning(message('parallel:cluster:GenericDeleteFcnError', fcnName, iFunc2Str( userFcn ), err.getReport()));
            end
        end
    end

    methods ( Access = protected )
        function jobState = getJobState( obj, job, jobStateInStorage )

            import parallel.internal.types.States

            [getJobStateFcn, args] = iGetFunction( obj.GetJobStateFcn );

            % Define situations where we are NOT going to run any user function
            % 1. No callback function defined
            % 2. If the job is still pending then we have not called submit yet
            % (NB this check is unnecessary as isSubmittedGenericClusterJob will
            % return false for a Pending job)
            % 3. If the job state on disk is already terminal
            % 4. If we are already in the user function for this job
            % 5. The job has not a submitted generic scheduler job
            shortcutCallbackFcn = isempty(getJobStateFcn) || ...
                jobStateInStorage == States.Pending || ...
                jobStateInStorage.isTerminal() || ...
                ismember(job.Id, obj.JobsWithGetJobStateFcnRunning) || ...
                ~obj.isSubmittedGenericClusterJob(job);

            if shortcutCallbackFcn
                % Return with the state that has been supplied from storage,
                % without calling any user supplied function.
                jobState = jobStateInStorage;
                return
            end

            % If we are going to run the user function we need to ensure we
            % don't re-enter the user code, so store the job Id in
            % JobsWithGetJobStateFcnRunning and make sure we remove it at the
            % end
            obj.JobsWithGetJobStateFcnRunning(end+1) = job.Id;
            cleanup = onCleanup(@() iRemoveJobFromRunningList(obj, job.Id));

            % Having done the above it is safe to call the user function, and
            % for the user to CTRL-C out of their code, as the cleanup will
            % ensure that the job is removed
            try
                userSuppliedStateStr = feval(getJobStateFcn, obj, job, ...
                                             jobStateInStorage.Name, args{:});
            catch err
                warning(message('parallel:cluster:GenericGetJobStateFcnError', iFunc2Str( getJobStateFcn ), err.getReport()));
                userSuppliedStateStr = jobStateInStorage.Name;
            end

            % Check that the user has returned a string
            if ~( ischar( userSuppliedStateStr ) && isrow( userSuppliedStateStr ) )
                warning(message('parallel:cluster:GenericInvalidStateType', iFunc2Str( getJobStateFcn )));
                userSuppliedStateStr = jobStateInStorage.Name;
            end

            shouldWarnInvalidState = false;
            try
                jobState = States.fromName( userSuppliedStateStr );
            catch E %#ok<NASGU>
                shouldWarnInvalidState = true;
            end
            
            allStates = enumeration('parallel.internal.types.States');
            allowedStatesIdx = arrayfun(@(x) (x > States.Pending && x <= States.Failed) || x == States.Unknown, allStates);
            allowedStates = allStates(allowedStatesIdx);
            if shouldWarnInvalidState || ~ismember({jobState.Name}, {allowedStates.Name})
                warning(message('parallel:cluster:GenericInvalidStateString', ...
                    iFunc2Str( getJobStateFcn ), userSuppliedStateStr, ...
                    sprintf('%s ', allowedStates.Name)));
                jobState = jobStateInStorage;
            end
        end

        function tf = wasSubmittedByThisType( cluster, job )
            validateattributes( cluster, {'parallel.cluster.Generic'}, {'scalar'} );
            validateattributes( job, {'parallel.job.CJSIndependentJob', ...
                                'parallel.job.CJSCommunicatingJob'}, {'scalar'} );

            jobData = job.JobSchedulerData;
            tf = ~isempty( jobData ) && isequal( jobData.type, 'generic' );
        end

        function [names, values] = getClusterSpecificEnvVars( ~ )
        % Return names/values of environment variables that this cluster must
        % send down to workers.  Since all of this work is done in the user's
        % submit functions, there is nothing to do here.
            names = {};
            values = {};
        end
    end
end
%#ok<*ATUNK>

% --------------------------------------------------------------------------
function iRemoveJobFromRunningList( obj, jobId )
    obj.JobsWithGetJobStateFcnRunning = setdiff( obj.JobsWithGetJobStateFcnRunning, jobId );
end

% --------------------------------------------------------------------------
function [fcn, args] = iGetFunction( clusterFcn )
% If it is a cell array then get the first argument which is a function handle
% or string and subsequent arguments are the additional args to the function.
    if iscell( clusterFcn )
        fcn  = clusterFcn{1};
        args = clusterFcn(2:end);
    else
        fcn  = clusterFcn;
        args = {};
    end
end

% --------------------------------------------------------------------------
function str = iFunc2Str(fcn)
% Return a string from a function handle or char
    if ischar( fcn )
        str = fcn;
    elseif isa( fcn, 'function_handle' )
        str = func2str( fcn );
    else
        % Should never get here because of the "MATLAB Callback" check from
        % checkSimpleConstraint
        str = sprintf( 'object of type "%s"', class( fcn ) );
    end
end
