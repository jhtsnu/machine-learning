%Torque Interact with a Torque cluster
%   The Torque cluster type allows you to submit jobs to Torque. The Torque
%   client tools must be installed on your client machine. The client and
%   cluster machines must all be running UNIX.
%
%   parallel.cluster.Torque methods:
%      Torque                 - Create a Torque cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      getDebugLog            - Read output messages from communicating job or independent task
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.cluster.Torque properties:
%      ClusterMatlabRoot                - Path to MATLAB on the cluster
%      CommunicatingJobWrapper          - Script that cluster runs to start workers
%      HasSharedFilesystem              - Specify whether the client and cluster nodes share JobStorageLocation
%      Host                             - Host of the cluster's headnode
%      JobStorageLocation               - Folder where cluster stores job and task information
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use with MathWorks hosted licensing
%      Modified                         - True if any properties in this cluster have been modified
%      NumWorkers                       - Number of workers in the cluster
%      OperatingSystem                  - Operating System of the cluster
%      Profile                          - Profile used to build this cluster
%      RcpCommand                       - Command to copy files to and from client
%      RequiresMathWorksHostedLicensing - Cluster uses MathWorks hosted licensing
%      ResourceTemplate                 - Define resources to request for communicating jobs
%      RshCommand                       - Remote execution command used on worker nodes during communicating job
%      SubmitArguments                  - Specify additional arguments to use when submitting jobs
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%
%   See also parcluster, parallel.Cluster.

% Copyright 2011-2012 The MathWorks, Inc.

classdef (ConstructOnLoad = true, Sealed) Torque < parallel.cluster.PBS

    methods
        function obj = Torque( varargin )
        %Torque Create a Torque cluster instance
        %   parallel.cluster.Torque(p1, v1, p2, v2, ...) builds a Torque
        %   cluster with the specified property values. The properties can
        %   include any of the properties of the Torque cluster class, or a
        %   profile.
        %
        %   Example:
        %   % Create a Torque cluster using the 'myTorque' profile.
        %   myCluster = parallel.cluster.Torque('Profile', 'myTorque');
        %
        %   See also parcluster, parallel.Cluster.
            import parallel.internal.apishared.PbsHelper

            if ispc
                error(message('parallel:cluster:TorquePcNotSupported'));
            end

            try
                pbsInfo = PbsHelper.getClusterInfo();
            catch E
                throw( E );
            end
            if iCouldUsePBSPro( pbsInfo )
                warning(message('parallel:cluster:TorqueCouldUsePbsPro', pbsInfo.pbs_version));
            end

            pbsConfig = struct( 'Type', 'Torque', ...
                                'Host', pbsInfo.server, ...
                                'UseJobArrays', false, ...
                                'UsePbsAttach', false, ...
                                'StateIndicators', { {'RE', 'HQW' } }, ...
                                'FactoryRsh', 'rsh', ...
                                'FactoryRcp', 'rcp' );

            obj@parallel.cluster.PBS( pbsConfig, varargin{:} );
        end
    end

    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function tf = iCouldUsePBSPro( infoStruc )
    tf = ~isempty( strfind( infoStruc.pbs_version, 'PBSPro' ) );
end
