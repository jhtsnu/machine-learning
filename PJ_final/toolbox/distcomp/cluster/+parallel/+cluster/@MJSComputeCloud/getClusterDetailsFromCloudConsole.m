function clusterDetails = getClusterDetailsFromCloudConsole( identifier )
% Update details of a cluster from the Cloud Console. This is static because it needs to be
% callable from the MJSComputeCloud constructor before the parallel.Cluster constructor has been called.

%   Copyright 2011-2012 The MathWorks, Inc.

    if isdeployed
        error(message('parallel:cluster:CannotDeployWithCloud'));
    end
    
    cloudCenterClient = parallel.internal.webclients.currentCloudCenterClient;
    getClusterDetailsFcn = @(token) cloudCenterClient.getClusterDetails( identifier, token );
    
    clusterDetails = parallel.internal.webclients.invokeAndRetryOnAuthFailure( ...
        getClusterDetailsFcn );
    
end
