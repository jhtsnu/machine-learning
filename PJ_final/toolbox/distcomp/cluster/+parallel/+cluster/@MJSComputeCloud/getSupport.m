function support = getSupport(obj)

%   Copyright 2011-2012 The MathWorks, Inc.

    import com.mathworks.toolbox.distcomp.wsclients.cloudconsole.CloudConsoleClusterDetails
    import parallel.internal.types.CloudConsoleStates
    
    if ~isempty(obj.Support)
        support = obj.Support;
        return;
    end
    
    % No existing support object. Try to create one.
        
    if obj.canCreateSupport()
    % We have everything including the headnode name and jobmanager name. Save a web services call.
        try
            obj.Support = iCreateSupport(obj);
            iCheckComms(obj);
        catch E
            throwAsCaller(E);
        end
    else
        % Get details of the cluster from Cloud Console. getClusterDetailsFromCloudConsole will throw
        % if there is a communication error with the web service. This is the case where we only have the
        % cluster identifier and certificate.
        try
            clusterDetails = parallel.cluster.MJSComputeCloud.getClusterDetailsFromCloudConsole(obj.Identifier);
        catch E
            throwAsCaller(E);
        end
        
        if ~isempty(clusterDetails)
            obj.ClusterDetails = clusterDetails;
        end
        obj.ClusterState = obj.getVisibleClusterStateFromClusterDetails();
        
        % Now try again with the updated/new details from Cloud Console
        if obj.canCreateSupport()
            try
                obj.Support = iCreateSupport(obj);
                iCheckComms(obj);
            catch E
                throwAsCaller(E);
            end
        else
        % Cluster is stopped, deleted or not in a good state. We should not try create a support in this case
            cloudConsoleClusterState = CloudConsoleStates.fromName( char( obj.ClusterDetails.getClusterState() ) );
            if (cloudConsoleClusterState == CloudConsoleStates.Defined) || (cloudConsoleClusterState == CloudConsoleStates.Stopped)
                err = obj.clusterStoppedErr();
            elseif cloudConsoleClusterState == CloudConsoleStates.Deleted
                err = obj.clusterDeletedErr();
            else
                err = obj.clusterNotReadyErr();
            end
            throwAsCaller(err);
        end
    end

    support = obj.Support;
end
% --------------------------------------------------------------------------
function support = iCreateSupport(obj)
% Create the MJS support object

    import parallel.internal.cluster.MJSSupport
     
    headnodeDNSName   = char( obj.ClusterDetails.getClusterHeadNodeDNSName() );
    peerLookupUri     = char( obj.ClusterDetails.getPeerLookupUri() );
    jobManagerName    = char( obj.ClusterDetails.getJobManagerName() );
    userName          = obj.Username;
    certificate       = obj.Certificate;
    promptForPassword = obj.PromptForPassword;
    %todo rename field
    decoratedHostName = obj.decorateHostName(headnodeDNSName,peerLookupUri);
    
    try
        support = MJSSupport.build(decoratedHostName, jobManagerName, userName, promptForPassword, certificate);
    catch E
        err = MException(message('parallel:cluster:MJSComputeCloud:FailedToBuildSupport', obj.Name));
        err = err.addCause(E);
        obj.StashedErrorMessage = E.message;
        throw(err);
    end
end
% --------------------------------------------------------------------------
function iCheckComms(obj)
    
    % We are checking newly created support objects, the communication test will
    % have already been done as part of that, so use the cached results.
    forceCheck = false;
    commsOk = obj.checkTwoWayCommunications( forceCheck );
    if ~commsOk
        err = obj.commsErr();
        throw(err);
    end
     
end
