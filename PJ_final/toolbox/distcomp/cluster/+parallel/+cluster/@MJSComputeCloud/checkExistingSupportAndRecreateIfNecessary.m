function newSupport = checkExistingSupportAndRecreateIfNecessary(obj,varargin)

%   Copyright 2011-2012 The MathWorks, Inc.

    import parallel.internal.types.CloudConsoleStates
    
    % We are checking the existing support, so we must force a new check of the
    % two-way communications rather than using the cached results.
    forceCheck = true;
    ok = obj.checkTwoWayCommunications( forceCheck );
    if ok
    % No problem. Return the existing support
        newSupport = obj.getSupport();
        return;
    else
    % Something went wrong with communications. Try to do the right thing.
    % Firstly refresh the cluster info from Cloud Console unless we have
    % been provided with cluster details to use
        if nargin > 1 && isa(varargin{1},'com.mathworks.toolbox.distcomp.wsclients.cloudconsole.CloudConsoleClusterDetails')
            clusterDetails = varargin{1};
        else 
            clusterDetails = parallel.cluster.MJSComputeCloud.getClusterDetailsFromCloudConsole(obj.Identifier);
        end
        if ~isempty(clusterDetails)
            newHeadnodeName = char( clusterDetails.getClusterHeadNodeDNSName() );
            currentHeadnodeName = char( obj.ClusterDetails.getClusterHeadNodeDNSName() );
            obj.ClusterDetails = clusterDetails;
            obj.ClusterState = obj.getVisibleClusterStateFromClusterDetails();
           
            if ~isempty(newHeadnodeName) && ~strcmp(currentHeadnodeName,newHeadnodeName)
            % New headnode. Cluster has been shut down and restarted. Nothing we can do here
                newSupport = iWarnAndCreateNewSupport(obj);
                return;
            end                   
        end

        if obj.canCreateSupport()
        % Same head node. Check for a new instance of the job manager
        % Try to get a new Jobmanager proxy
            jmName = char( obj.ClusterDetails.getJobManagerName() );
            hostName = char( obj.ClusterDetails.getClusterHeadNodeDNSName() );
            peerLookupUri = char( obj.ClusterDetails.getPeerLookupUri() );

            decoratedHostName = obj.decorateHostName(hostName,peerLookupUri);
            try
                updated = obj.Support.updateJmAccess(decoratedHostName,jmName);
            catch E
                % New instance of the jobmanager
                if strcmp(E.identifier,'parallel:cluster:MJSChanged')
                    % This is a cluster stop and restart. Recreate the support
                    newSupport = iWarnAndCreateNewSupport(obj);
                    return;
                else
                    rethrow(E);
                end
            end

            % This should be the same instance of the jobmanager. We do not
            % need to force a new check here if the JmAccess has been updated
            % as this will have been done as part of updating it.
            forceCheck = ~updated;
            ok = obj.checkTwoWayCommunications( forceCheck );
            if ~ok
                err = obj.commsErr();
                throw(err);
            end
            
            newSupport = obj.Support;     
        else
            % Cluster has no Job manager, so there can be no jobs or tasks associated with it
            if ~isempty(obj.Support)
                obj.Support = parallel.internal.cluster.MJSSupport.empty();
                cloudConsoleClusterState = CloudConsoleStates.fromName( char( obj.ClusterDetails.getClusterState() ) );
                if cloudConsoleClusterState == CloudConsoleStates.Deleted
                    err = obj.clusterDeletedErr();
                else
                    err = obj.clusterNotReadyErr();
                end
                throw(err);
            end
        end     
    end
end
% --------------------------------------------------------------------------
function support = iWarnAndCreateNewSupport(obj)
    obj.Support = parallel.internal.cluster.MJSSupport.empty();
    s=warning('off','backtrace');
    warning(message('parallel:cluster:MJSComputeCloud:ClusterRestarted', char( obj.ClusterDetails.getClusterName() )));
    warning(s);
    support = obj.getSupport(); % New support object will be cached
end
