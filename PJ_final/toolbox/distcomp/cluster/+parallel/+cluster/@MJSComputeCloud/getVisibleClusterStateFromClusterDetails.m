function newState = getVisibleClusterStateFromClusterDetails(obj)
% Helper function to map Cloud Console states to user-visible states

%   Copyright 2011 The MathWorks, Inc.

    import parallel.internal.types.CloudConsoleStates
    import parallel.internal.types.MJSComputeCloudStates

    clusterDetails = obj.ClusterDetails;
    cloudConsoleClusterState = CloudConsoleStates.fromName( char( clusterDetails.getClusterState() ) );
    
    % Set the public cluster state as best we can without relying on the support object
    % The final transition to a user-visible Ready state can only be done once a successful communication test
    % has been done on the support object. We don't want to do that check here though as these updates need to
    % be able to run without a support in place.
    if (cloudConsoleClusterState == CloudConsoleStates.Defined) || (cloudConsoleClusterState == CloudConsoleStates.Stopped)
        newState = MJSComputeCloudStates.Stopped;
    elseif (cloudConsoleClusterState > CloudConsoleStates.Defined) && (cloudConsoleClusterState <= CloudConsoleStates.Ready)
        newState = MJSComputeCloudStates.Starting;
    elseif (cloudConsoleClusterState > CloudConsoleStates.Ready) && (cloudConsoleClusterState < CloudConsoleStates.Deleted)
        newState = MJSComputeCloudStates.Stopping;
    elseif cloudConsoleClusterState == CloudConsoleStates.Deleted
        newState = MJSComputeCloudStates.Deleted;
    elseif (cloudConsoleClusterState > CloudConsoleStates.Deleted) && (cloudConsoleClusterState < CloudConsoleStates.Unknown)
        newState = MJSComputeCloudStates.Error;
    else
        newState = MJSComputeCloudStates.Stopped;
    end

end
