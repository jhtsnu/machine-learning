function [ values, handled ] = hVectorisedGet( obj, propsToAccess, handled, existingValues )

%   Copyright 2011-2012 The MathWorks, Inc.

    import parallel.internal.customattr.Reflection
    import parallel.internal.types.MJSComputeCloudStates
    
    publicProps = Reflection.getPublicProperties(obj);
    
    obj.refreshState();
    
    if nargin < 3
        handled = num2cell( false(1,numel(propsToAccess)) );
        existingValues = cell(1,numel(propsToAccess));
    elseif nargin < 4
        existingValues = cell(1,numel(propsToAccess));        
    end
    
    if isempty(obj.Support) || obj.ClusterState ~= MJSComputeCloudStates.Ready
        [values, handled] = iGetDefaultProps(obj,publicProps,propsToAccess,handled,existingValues);
    else
        [values, handled] = iGetProps(obj,publicProps,propsToAccess,handled,existingValues);
    end
    [values, handled] = hVectorisedGet@parallel.Cluster(obj,propsToAccess,handled,values);
end
% -------------------------------------------------------------------------
%
% iGetDefaultProps: Get properties when there is no support to defer to OR
% the cluster is not in the state 'ready', i.e. the MJS is being started up
% or built and we don't want to send any calls to it yet, but we can still
% talk to the cloud console
%
function [newValues, handledHere] = iGetDefaultProps(obj,myProps,propsToAccess,handled,existingValues)

    import parallel.Cluster

    superProps = {'Type','Profile','Modified'};
    specialHandlingProps = {...
                            'Name',...
                            'Host',...
                            'MatlabVersion',...
                            'NumWorkers',...
                            'Jobs',...
                            'State',...
                            'RequiresMathWorksHostedLicensing'};
                        % We need a logical value for RequiresMathWorksHostedLicensing 
                        % for the display, so we default to true when the cluster isn't 
                        % ready to get properties using the support  
    specialHandlingFuncs = { @() char( obj.ClusterDetails.getClusterName ), ...
                             @() char( obj.ClusterDetails.getClusterHeadNodeDNSName ), ...
                             @() char( obj.ClusterDetails.getMatlabReleaseVersion ), ...
                             @() obj.ClusterDetails.getNumberOfWorkers, ...
                             @() parallel.job.MJSIndependentJob.empty(), ...
                             @() obj.ClusterState.Name, ...
                             @() parallel.cluster.MJSComputeCloud.DefaultRequiresMathWorksHostedLicensing};
    lookupMap = containers.Map(specialHandlingProps,specialHandlingFuncs);
    
    validPropsToLookup = setdiff(myProps,superProps,'legacy');
    
    helperFunc = @(a,b,c) iVectorisedGetHelper(lookupMap,validPropsToLookup,a,b,c);
    
    [newValues, handledHere] = cellfun(helperFunc, propsToAccess, handled, existingValues, 'UniformOutput', false);
    
end
%
function [val, handled] = iVectorisedGetHelper(map, validNameList, name, alreadyHandled, existingValue)
    if alreadyHandled
        val = existingValue;
        handled = alreadyHandled;
    else
        if ismember(name,validNameList,'legacy')
            if map.isKey(name)
                func = map(name);
                val = func();
                handled = true;
            else
                val = '';
                handled = true;
            end
        else
            val = existingValue;
            handled = alreadyHandled;
        end
    end
end
%
% iGetProps: Get properties using the support
%
function [vals, handled] = iGetProps(obj,myProps,propsToAccess,handled,vals)

    propsToHandleLocally = {...
                            'Certificate',...
                            'Identifier',...
                            'JobStorageLocation',...
                            'Modified',...
                            'Profile',...
                            'Type',...
                            'UserData', ...
                            'LicenseNumber'};
    specialHandlingProps = {...
                            'MatlabVersion',...
                            'Jobs',...
                            'State'};
    specialHandlingFuncs = { @() char( obj.ClusterDetails.getMatlabReleaseVersion ), ...
                             @() obj.Jobs, ...
                             @() obj.ClusterState.Name};
    specialHandlingMap = containers.Map(specialHandlingProps,specialHandlingFuncs);
    
    propsNotLookedUpOnSupport = union(propsToHandleLocally,specialHandlingProps,'legacy');
    
    propsToHandleIndex  = ~cell2mat(handled);
    propsUnhandledSoFar = propsToAccess(propsToHandleIndex);
    [~, iPropsToAccess] = ismember(propsUnhandledSoFar,propsToAccess,'legacy');
    [validPropsToLookup,~,iPropsUnhandled] = intersect(myProps,propsUnhandledSoFar,'legacy');   
    
    [nonLookupProps, iValidProps1]  = intersect(validPropsToLookup, propsNotLookedUpOnSupport,'legacy');     
    if ~isempty(nonLookupProps)
        localLookupFunc = @(x) iLocalLookupHelper(obj,specialHandlingMap,x);
        nonLookupVals   = cellfun(localLookupFunc, nonLookupProps, 'UniformOutput', false);
    else
        nonLookupVals = {};
    end
    
    [clusterLookupProps, iValidProps2] = setdiff(validPropsToLookup, propsNotLookedUpOnSupport,'legacy');
    if ~isempty(clusterLookupProps)
        lookedUpVals = obj.getClusterProps(clusterLookupProps);
     else
        lookedUpVals = {};
    end
    
    propIds  = [ iValidProps1, iValidProps2 ];
    propVals = [ nonLookupVals, lookedUpVals ];
    
    propsUnhandledIds = iPropsUnhandled(propIds);
    accessIds = iPropsToAccess(propsUnhandledIds);
    handled(accessIds) = {true};
    vals(accessIds) = propVals;
end   
%
function val = iLocalLookupHelper(obj,map,name)
    if map.isKey(name)
        getFunc = map(name);
        val = getFunc();
    else
        getFunc = @(x) obj.(x);
        val = getFunc(name);
    end    
end

