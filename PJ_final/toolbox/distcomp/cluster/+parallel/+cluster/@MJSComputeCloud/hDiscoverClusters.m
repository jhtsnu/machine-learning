function clusterList = hDiscoverClusters()
% Discover all configured clusters

%   Copyright 2011-2012 The MathWorks, Inc.

    import com.mathworks.toolbox.distcomp.wsclients.cloudconsole.CloudConsoleClusterInfo
    import com.mathworks.toolbox.distcomp.wsclients.cloudconsole.MatlabVersionInformation
    
    if isdeployed
        error(message('parallel:cluster:CannotDeployWithCloud'));
    end

    cloudCenterClient = parallel.internal.webclients.currentCloudCenterClient();
    
    discoverClustersFcn = @(token) cloudCenterClient.discoverMyClusters( token );
    try
        discoveredClusters = parallel.internal.webclients.invokeAndRetryOnAuthFailure( ...
            discoverClustersFcn );
    catch E
        throwAsCaller( E )
    end
    if isempty( discoveredClusters )
        clusterList = parallel.cluster.MJSComputeCloud.empty();
        return;
    end
    
    % Version of Matlab
    v = version();
    % Remove everything up to and including the first ( and the last ) if it's the final character of the string
    matlabClientVersion = regexprep(v,'(^.*\(|\)$)','');

    clusterList = parallel.cluster.MJSComputeCloud.empty();  
    for i=1:length(discoveredClusters)
        cluster = discoveredClusters(i);
        if cluster.getMatlabReleaseVersion().isCompatibleWith(matlabClientVersion)
            try
                newCluster = parallel.cluster.MJSComputeCloud(cluster);
                clusterList = [clusterList newCluster]; %#ok<AGROW>
            catch E
                iShowWarnings(E,cluster);
            end
        end
    end
end
%
function iShowWarnings(E,cluster)
    clusterName = char( cluster.getClusterName() );
    s=warning('off','backtrace');
    if strcmp(E.identifier,'parallel:cluster:MJSComputeCloud:FailedToBuildSupport')
        if ~isempty(E.cause)
            warning(message('parallel:cluster:MJSComputeCloud:SupportBuildFailedDiscoveryWarning', ...
                    clusterName,E.cause{1}.message));
        else
            warning(message('parallel:cluster:MJSComputeCloud:DiscoveryWarning', clusterName));
        end
    else
        warning(message('parallel:cluster:MJSComputeCloud:DiscoveryWarning', clusterName));
    end
    warning(s);
end

