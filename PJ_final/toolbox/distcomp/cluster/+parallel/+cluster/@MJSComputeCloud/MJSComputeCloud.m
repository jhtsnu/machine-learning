%MJSComputeCloud Interact with an MJS cluster that was started on the cloud
%
%   parallel.cluster.MJSComputeCloud methods:
%      MJSComputeCloud        - Create a MJSComputeCloud cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.cluster.MJSComputeCloud properties:
%      BusyWorkers                      - Workers currently running tasks
%      Certificate                      - Cluster SSL certificate
%      ClusterMatlabRoot                - Specifies the path to MATLAB that the cluster is using
%      HasSecureCommunication           - True if the cluster is using secure communication
%      Host                             - Host on which this cluster is running
%      Identifier                       - Unique cluster identifier
%      IdleWorkers                      - Workers that are free to run tasks
%      JobStorageLocation               - Location where job and task data is stored
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use when running jobs with this cluster
%      MatlabVersion                    - Version of MATLAB running on the workers
%      Modified                         - True if any properties in this cluster have been modified
%      Name                             - Name of this cluster
%      NumBusyWorkers                   - Number of workers currently running tasks
%      NumIdleWorkers                   - Number of workers that are free to run tasks
%      NumWorkers                       - Number of workers running on this cluster
%      OperatingSystem                  - Operating system of the nodes used by the cluster
%      Profile                          - Profile used to build this cluster
%      RequiresMathWorksHostedLicensing - True if the cluster is using the MathWorks Hosted License Manager
%      State                            - Current state of the cluster
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%      Username                         - User who is using the cluster
%
%   See also PARCLUSTER, parallel.Cluster.

%   Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true, Sealed ) MJSComputeCloud < parallel.Cluster

    % Profile/Construction properties
    properties ( Transient, PCTGetSet, GetAccess = public, SetAccess = private, PCTConstraint = 'string' )

        %Certificate Cluster SSL certificate
        %   The SSL certificate used to secure communication between the
        %   client and the cluster.
        Certificate='';

        %Identifier Unique cluster identifier
        %   A unique string that identifies the cluster.
        Identifier='';
       
    end

    properties ( Transient, PCTGetSet, PCTConstraint = 'string' )

        %LicenseNumber License number to use when running jobs with this cluster
        %   License number specifies the license number to use on the
        %   workers when the cluster is using MathWorks hosted licensing.
        %   This string must be the same as one of the licenses listed in
        %   your MathWorks account.
        %
        %   LicenseNumber cannot be defined if
        %   RequiresMathWorksHostedLicensing is false.
        %    
        %   See also
        %   parallel.cluster.MJSComputeCloud.RequiresMathWorksHostedLicensing
        LicenseNumber = '';

    end
    
    properties ( Dependent, SetAccess = private )
        
        %BusyWorkers Workers currently running tasks
        %   The BusyWorkers property value indicates which workers are
        %   currently running tasks for the cluster.
        %   (read-only)
        %
        %   As workers complete tasks and assume new ones, the lists of
        %   workers in BusyWorkers and IdleWorkers can change rapidly. If
        %   you examine these two properties at different times, you might
        %   see the same worker on both lists if that worker has changed
        %   its status between those times. If a worker stops unexpectedly,
        %   the cluster's knowledge of that as a busy or idle worker does
        %   not get updated until the cluster runs the next job and tries
        %   to send a task to that worker
        % 
        %   See also parallel.cluster.MJSComputeCloud.NumWorkers,
        %            parallel.cluster.MJSComputeCloud.IdleWorkers,
        %            parallel.cluster.MJSComputeCloud.NumBusyWorkers, 
        %            parallel.cluster.MJSComputeCloud.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        BusyWorkers = [];
        
        %IdleWorkers Workers that are free to run tasks
        %   The IdleWorkers property value indicates which workers are
        %   currently not working on any tasks and are available to the
        %   cluster for the performance of job tasks.
        %   (read-only)
        %
        %   As workers complete tasks and assume new ones, the lists of
        %   workers in BusyWorkers and IdleWorkers can change rapidly. If
        %   you examine these two properties at different times, you might
        %   see the same worker on both lists if that worker has changed
        %   its status between those times. If a worker stops unexpectedly,
        %   the cluster's knowledge of that as a busy or idle worker does
        %   not get updated until the cluster runs the next job and tries
        %   to send a task to that worker.
        % 
        %   See also parallel.cluster.MJSComputeCloud.NumWorkers,
        %            parallel.cluster.MJSComputeCloud.BusyWorkers,
        %            parallel.cluster.MJSComputeCloud.NumBusyWorkers, 
        %            parallel.cluster.MJSComputeCloud.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        IdleWorkers = [];
        
        %HasSecureCommunication True if the cluster is using secure communication
        %   The HasSecureCommunication property indicates whether secure
        %   communication is being used between the job manager and the
        %   workers. The mdce_def file sets the parameter that controls
        %   secure communication when the mdce process starts on the
        %   cluster nodes.  
        %   (read-only)
        %
        %   See also parallel.cluster.MJSComputeCloud.Username.
        HasSecureCommunication = true;
        
        %MatlabVersion Version of MATLAB running on the workers
        MatlabVersion= '';
        
        %Name Name of this cluster 
        %   The descriptive name of an MJSComputeCloud cluster is set when
        %   the cluster is created in Cloud Center.  If you have downloaded
        %   and imported your cluster's profile from Cloud Center then you
        %   will have a profile of the same name.  You can use this profile
        %   to create a cluster object with the parcluster function or the
        %   cluster constructor.
        %
        %    See also PARCLUSTER, 
        %             parallel.cluster.MJSComputeCloud.MJSComputeCloud.
        Name = '';
              
        %NumBusyWorkers Number of workers currently running tasks
        %   The NumBusyWorkers property value indicates how many workers
        %   are currently running tasks for the cluster.
        %   (read-only)
        %
        %   The value of NumBusyWorkers can range from 0 up to the total
        %   number of workers registered with the cluster.
        %
        %   See also parallel.cluster.MJSComputeCloud.NumWorkers,
        %            parallel.cluster.MJSComputeCloud.BusyWorkers,
        %            parallel.cluster.MJSComputeCloud.IdleWorkers, 
        %            parallel.cluster.MJSComputeCloud.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        NumBusyWorkers = 0;
        
        %NumIdleWorkers Number of workers that are free to run tasks
        %   The NumIdleWorkers property value indicates how many workers
        %   are currently not working on any tasks and are available to the
        %   cluster for the performance of job tasks. If the NumIdleWorkers
        %   is equal to or greater than the lower value of NumWorkersRange
        %   of the job at the front of the queue, that job can start
        %   running. (read-only)
        %
        %   The value of NumIdleWorkers can range from 0 up to the total
        %   number of workers registered with the cluster.
        %
        %   See also parallel.cluster.MJSComputeCloud.NumWorkers,
        %            parallel.cluster.MJSComputeCloud.BusyWorkers,
        %            parallel.cluster.MJSComputeCloud.IdleWorkers, 
        %            parallel.cluster.MJSComputeCloud.NumBusyWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        NumIdleWorkers = 0;
        
        %Username User who is using the cluster
        %   The Username property value indicates the user who created the
        %   cluster object or who is using the object to access jobs in its
        %   queue.
        %
        %   See also parallel.cluster.MJSComputeCloud.HasSecureCommunication
        Username = '';
        
        %State Current state of the cluster
        %   A cluster can be in state offline, starting, online, stopping, 
        %   deleted, communicationerror, or error.
        %   (read-only)
        State = 'offline';

    end

    properties ( Dependent )
                
        %ClusterMatlabRoot Specifies the path to MATLAB that the cluster is using
        %   ClusterMatlabRoot specifies the path name to MATLAB for the
        %   cluster to use for starting MATLAB worker processes.
        %   (read-only)
        %
        %   See also parallel.cluster.MJSComputeCloud.JobStorageLocation
        ClusterMatlabRoot = '';
        
        %JobStorageLocation Location where job and task data is stored
        %   (read-only)
        %   
        %   See also parallel.cluster.MJSComputeCloud.ClusterMatlabRoot
        JobStorageLocation = '';

        %NumWorkers Number of workers running on this cluster
        %   NumWorkers indicates the total number of workers available to
        %   the cluster for running your jobs. NumWorkers must fall within
        %   the range defined by a job's NumWorkersRange. 
        %   (read-only)
        %
        %   See also parallel.cluster.MJSComputeCloud.BusyWorkers,
        %            parallel.cluster.MJSComputeCloud.IdleWorkers, 
        %            parallel.cluster.MJSComputeCloud.NumBusyWorkers, 
        %            parallel.cluster.MJSComputeCloud.NumIdleWorkers, 
        %            parallel.job.MJSIndependentJob.NumWorkersRange,
        %            parallel.job.MJSCommunicatingJob.NumWorkersRange.
        NumWorkers = 0;

        %OperatingSystem Operating system of the nodes used by the cluster
        %   OperatingSystem is always unix.
        %   (read-only)
        OperatingSystem   = 'unix';
        
        %RequiresMathWorksHostedLicensing True if the cluster is using the MathWorks Hosted License Manager
        %   RequiresMathWorksHostedLicensing indicates whether the cluster
        %   uses MathWorks hosted licensing.  You must provide your
        %   MathWorks account login information if this value is true.  If
        %   you have more than one MDCS license associated with your
        %   MathWorks account you will also need to supply the
        %   LicenseNumber to use.
        %   (read-only)
        %
        %   See also parallel.cluster.MJSComputeCloud.LicenseNumber.
        RequiresMathWorksHostedLicensing = parallel.cluster.MJSComputeCloud.DefaultRequiresMathWorksHostedLicensing;
    end

    properties ( Dependent, SetAccess = immutable )
    
        %Host Host on which this cluster is running
        %
        %   See also PARCLUSTER,
        %            parallel.cluster.MJSComputeCloud.Name.
        Host = '';

    end
    
    properties ( Transient, GetAccess = private, SetAccess = private )

        % The MJSSupport providing access to underlying functionality
        Support = parallel.internal.cluster.MJSSupport.empty;
        
        % Local cache of cluster state
        ClusterState = parallel.internal.types.MJSComputeCloudStates.Stopped;
        
        % Local copy of the cluster details fetched from cloud console
        ClusterDetails = [];
        
        % Error message to show as part of cluster display (if necessary)
        StashedErrorMessage = '';
        
    end
    
    properties (Dependent, Hidden)
        
        PromptForPassword = true;
        
    end

    properties (Transient, Hidden)

        % Switch to control use of PeerLookup or RMI 
        UsePeerLookup = true;
        
        ClusterLogLevel = 2;

    end
    
    properties ( Constant, Hidden, GetAccess = private )
        % Default to true for RequiresMathWorksHostedLicensing for the display
        DefaultRequiresMathWorksHostedLicensing = true;
    end


    % API2(orig) compatibility
    properties ( Dependent, Hidden )
        IsUsingSecureCommunication
    end
    methods
        function v = get.IsUsingSecureCommunication( obj )
            v = obj.HasSecureCommunication;
        end
    end
    
% Public, Hidden discoveryClusters method
% -------------------------------------------------------------------------
    methods ( Static, Hidden )

        clusterList = hDiscoverClusters()

    end

% Getters and Setters
% -------------------------------------------------------------------------
    methods

        function currentState = get.State( obj )
        % Query the Cloud Center for the current state of this cluster
            obj.refreshState();
            currentState = obj.ClusterState.Name;
        end

        function val = get.NumWorkers( obj )
            try
                val = obj.getClusterProps( 'NumWorkers' );
            catch E %#ok<NASGU>
                clusterDetails = obj.ClusterDetails;
                val = clusterDetails.getNumberOfWorkers();
            end
        end
        function set.NumWorkers( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'NumWorkers' ) );
        end

        function val = get.ClusterMatlabRoot( obj )
            try
                val = obj.getClusterProps( 'ClusterMatlabRoot' );
            catch E %#ok<NASGU>
                val = '';
            end
        end
        function set.ClusterMatlabRoot( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'ClusterMatlabRoot' ) );
        end


        function val = get.Host( obj )
            try
                val = obj.getClusterProps( 'Host' );
            catch E %#ok<NASGU>
                clusterDetails = obj.ClusterDetails;
                val = char( clusterDetails.getClusterHeadNodeDNSName() );
            end
        end

        function val = get.RequiresMathWorksHostedLicensing( obj )
            try
                val = obj.getClusterProps( 'RequiresMathWorksHostedLicensing' );
            catch E %#ok<NASGU>
                val = true;
            end
        end

        function set.RequiresMathWorksHostedLicensing(obj, ~)
            throwAsCaller( obj.buildCannotModify( 'RequiresMathWorksHostedLicensing' ) );
        end

        function val = get.MatlabVersion( obj )
            clusterDetails = obj.ClusterDetails;
            val = char( clusterDetails.getMatlabReleaseVersion().getMatlabVersion() );
        end


        function val = get.NumBusyWorkers( obj )
            try
                val = obj.getClusterProps( 'NumBusyWorkers' );
            catch E %#ok<NASGU>
                val = 0;
            end
        end

        function val = get.NumIdleWorkers( obj )
            try
                val = obj.getClusterProps( 'NumIdleWorkers' );
            catch E %#ok<NASGU>
                val = 0;
            end
        end

        % The MJSSupport.getWorkers() method does not handleJavaException,
        % so we must.
        function bw = get.BusyWorkers( obj )

            supportErrFcn = @( err ) parallel.cluster.MJSWorker.empty();
            fcn = @( s, mjs ) s.getWorkers( mjs, 'BusyWorkers' );
            try
                bw = obj.invokeWithSupportAndRetry( fcn, supportErrFcn );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end

        function iw = get.IdleWorkers( obj )

            supportErrFcn = @( err ) parallel.cluster.MJSWorker.empty();
            fcn = @( s, mjs ) s.getWorkers( mjs, 'IdleWorkers' );
            try
                iw = obj.invokeWithSupportAndRetry( fcn, supportErrFcn );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end

        function val = get.Name( obj )
            val = char( obj.ClusterDetails.getClusterName() );
        end

        function val = get.HasSecureCommunication( obj )
            try
                val = obj.getClusterProps( 'HasSecureCommunication' );
            catch E %#ok<NASGU>
                val = [];
            end
        end

        function val = get.JobStorageLocation( obj )
            try
                propsToGet = {'Name','Host'};
                propVals = obj.getClusterProps( propsToGet );

                cloudConsoleJobManagerName = propVals{1};
                host = propVals{2};
                dataLocForDisplay = strcat(cloudConsoleJobManagerName, '@', host);
                val = sprintf( 'Database on %s', dataLocForDisplay );
            catch E %#ok<NASGU>
                val = '';
            end
        end
        function set.JobStorageLocation( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'JobStorageLocation' ) );
        end

        function val = get.OperatingSystem( obj )
            % Return an empty string if getting the support fails.
            supportErrFcn = @( err ) '';
            fcn = @( s, mjs ) s.getOperatingSystem( mjs );
            val = obj.invokeWithSupportAndRetry( fcn, supportErrFcn );
        end
        function set.OperatingSystem( obj, ~ )
            throwAsCaller( obj.buildCannotModify( 'OperatingSystem' ) );
        end

        function username = get.Username( ~ )
            desktopClient = parallel.internal.webclients.currentDesktopClient();
            username = desktopClient.UserName;
        end
        
        function prompt = get.PromptForPassword( obj )
            if ~isempty(obj.Support)
                prompt = obj.getClusterProps( 'PromptForPassword' );
            else
                prompt = true;
            end
        end
        function set.PromptForPassword( obj, promptForPassword )
            try
                parallel.internal.customattr.checkConstraint( 'logicalscalar', obj, 'PromptForPassword', promptForPassword );
                obj.setClusterProps( 'PromptForPassword', promptForPassword );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end
        
        function v = get.ClusterLogLevel( obj )
            if ~isempty(obj.Support)
                v = obj.getClusterProps( 'ClusterLogLevel' );
            else
                v = 2;
            end
        end
        function set.ClusterLogLevel( obj, logLevel )
            try
                validateattributes( logLevel, {'numeric'}, {'scalar', '>=', 0, '<=', 6}, 'setClusterLogLevel' );
                obj.setClusterProps( 'ClusterLogLevel', logLevel );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end
    end

% Overrides
% -------------------------------------------------------------------------
    methods ( Access = protected )

        function jobs = getJobs( obj )
        % Get jobs. Note that this method really shouldn't throw or display
        % errors if the cluster isn't started as it is used for the summary
        % output.
        
            fcn = @( s, mjs ) s.getJobs( mjs );
            try
                jobs = obj.invokeWithSupportAndRetry( fcn );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end

        function varargout = getJobsByState( obj )

            fcn = @( s, mjs ) s.getJobsByState( mjs );
            try
                [varargout{1:nargout}] = obj.invokeWithSupportAndRetry( fcn );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end

        function job = buildIndependentJob( obj )

            import parallel.internal.types.Variant

            job = obj.buildGenericJob(Variant.IndependentJob);
        end

        function job = buildCommunicatingJob( obj, variant )

            job = obj.buildGenericJob(variant);

        end

        function connMgr = buildConnectionManager( obj )

            hasSecureCommunication = obj.HasSecureCommunication;
            fcn = @( s, mjs ) s.buildConnectionManager( hasSecureCommunication );
            try
                connMgr = obj.invokeWithSupportAndRetry( fcn );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end

        end

        function tf = isEquivalentWorkers( obj1, obj2 )
        % isEquivalentWorkers TRUE if Host and Name match.
            tf = isequal( obj1.Host, obj2.Host ) && ...
                 isequal( obj1.Name, obj2.Name );
        end

        function tf = isEquivalentStorage( obj1, obj2 )
        % isEquivalentStorage TRUE if Host and Name match.
            tf = isequal( obj1.Host, obj2.Host ) && ...
                 isequal( obj1.Name, obj2.Name );
        end
    end

% Static helpers
% -------------------------------------------------------------------------
    methods ( Static, Access = private )

        clusterDetails = getClusterDetailsFromCloudConsole( identifier )
        % Query the Cloud Console for details about this cluster and return
        % the new ClusterDetails
    end

% Private methods
% -------------------------------------------------------------------------
    methods ( Access = private )

        support = getSupport( obj )

        newState = getVisibleClusterStateFromClusterDetails( obj )

        newSupport = checkExistingSupportAndRecreateIfNecessary(obj,varargin)

        function varargout = invokeWithSupportAndRetry( obj, fcn, errFcn )
            % Invokes the provided function on the support object and retries if this
            % fails. If the provided function throws an error on the second attempt
            % we rethrow the error. If getting the support fails then the provided
            % error function is called (for example displayable items may wish to
            % swallow the error and return a default value).

            try
                support = obj.getSupport();
            catch err
                if nargin < 3
                    rethrow( err );
                else
                    [varargout{1:nargout}] = errFcn( err );
                    return;
                end
            end

            attemptsLeft = 2;
            while attemptsLeft > 0
                attemptsLeft = attemptsLeft - 1;
                try
                    [varargout{1:nargout}] = fcn( support, obj );
                    return;
                catch E
                    if attemptsLeft > 0
                        support = obj.checkExistingSupportAndRecreateIfNecessary();
                    else
                        rethrow( E );
                    end
                end
            end
        end

        function delete( obj )
            delete@handle( obj );
        end

        function [ok,msg] = checkTwoWayCommunications( obj, forceCheck )
        % Simply defer to the support.
            assert(~isempty(obj.Support));
            try
                [ok, msg] = obj.Support.checkTwoWayCommunications( forceCheck );
                % TODO: This check may fail due to a race condition (g861728). As a short-term
                % fix we will retry here if the communication test fails. This should be removed
                % as part of g862303.
                if ~ok
                    [ok, msg] = obj.Support.checkTwoWayCommunications( true );
                end
            catch E
                % Don't throw here as we want to be able to return
                % meaningful status and errors
                ok = false;
                msg = E.message;
                obj.StashedErrorMessage = msg;
            end

            if ok
                obj.ClusterState = parallel.internal.types.MJSComputeCloudStates.Ready;
            else
                obj.ClusterState = parallel.internal.types.MJSComputeCloudStates.CommunicationError;
            end
        end

        function v = getClusterProps( obj, names )
        % Simple wrapper to include handleJavaException
            fcn = @( s, mjs ) s.getClusterProperties( names );
            try
                v = obj.invokeWithSupportAndRetry( fcn );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end

        function setClusterProps( obj, names, values )
            fcn = @( s, mjs ) s.setClusterProperties( names, values );
            try
                obj.invokeWithSupportAndRetry( fcn );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end

        function job = buildGenericJob( obj, variant )
            % Check that we are logged in as required.
            obj.ensureLicenseNumberIsSetOrError();

            fcn = @(s, mjs) s.buildJob( mjs, variant, obj.LicenseEntitlement );
            try
                job = obj.invokeWithSupportAndRetry( fcn );
            catch E
                throwAsCaller( distcomp.handleJavaException( obj, E ) );
            end
        end

        function ok = canCreateSupport( obj )

            import com.mathworks.toolbox.distcomp.wsclients.cloudconsole.CloudConsoleClusterDetails
            import parallel.internal.types.CloudConsoleStates
            import parallel.internal.types.MJSComputeCloudStates

            if ~isempty(obj.ClusterDetails)

                headNodeName = char( obj.ClusterDetails.getClusterHeadNodeDNSName() );
                cloudConsoleClusterState = CloudConsoleStates.fromName( char( obj.ClusterDetails.getClusterState() ) );

                % Make sure that only states with a running job manager are
                % allowed
                ok = ( (cloudConsoleClusterState >= CloudConsoleStates.HeadnodeReady) && ...
                       (cloudConsoleClusterState <= CloudConsoleStates.Ready) ) &&  ...
                        ~isempty(headNodeName);
            else
                ok = false;
            end
        end

        function refreshState( obj )
       
            try
                clusterDetails = parallel.cluster.MJSComputeCloud.getClusterDetailsFromCloudConsole(obj.Identifier);
            catch E
                throw(E);
            end

            if ~isempty(clusterDetails)
                obj.ClusterDetails = clusterDetails;
            end
            obj.ClusterState = obj.getVisibleClusterStateFromClusterDetails();

            % If we have a support, but the new state means it will not work, we must not
            % try to communicate with it, otherwise the reported state will be wrong
            if obj.canCreateSupport    
                if isempty(obj.Support)
                    % We have updated state. Try to get a support just in
                    % case we now can
                    try
                        obj.getSupport();
                    catch E %#ok<NASGU>
                        obj.ClusterState = parallel.internal.types.MJSComputeCloudStates.Error;
                    end                        
                else
                    % Check the support we have to make sure the
                    % user-visible state is correct. Pass in the new
                    % cluster details to save another call to cloud console
                    try
                        obj.checkExistingSupportAndRecreateIfNecessary(clusterDetails);
                    catch E %#ok<NASGU>
                        % OK, don't throw here as this is just a state
                        % refresh
                    end
                end
            else
                if ~isempty(obj.Support)
                    obj.Support = parallel.internal.cluster.MJSSupport.empty();
                end
            end

        end

        function err = clusterStoppedErr( obj )
            err = MException(message('parallel:cluster:MJSComputeCloud:ClusterNotRunning', char( obj.ClusterDetails.getClusterName() )));
        end

        function err = commsErr( obj )
            clusterName = char( obj.ClusterDetails.getClusterName() );
            err = MException(message('parallel:cluster:MJSComputeCloud:ConnectionToClusterFailed', clusterName));
        end

        function err = clusterNotReadyErr(obj)
            clusterName = char( obj.ClusterDetails.getClusterName() );
            err = MException(message('parallel:cluster:MJSComputeCloud:ClusterNotReady', clusterName));
        end
        
        function err = clusterDeletedErr(obj)
            clusterName = char( obj.ClusterDetails.getClusterName() );
            err = MException(message('parallel:cluster:MJSComputeCloud:ClusterDeleted', clusterName));
        end

        function decoratedHostName = decorateHostName(obj, hostname, peerLookupUri)
            if obj.UsePeerLookup
                decoratedHostName = peerLookupUri;
            else
                decoratedHostName = hostname;
            end
        end
    end

% Public hidden methods
% -------------------------------------------------------------------------
    methods ( Access = public, Hidden )
        
        [ values, handled ] = hVectorisedGet( obj, propsToAccess, handled, existingValues )

        function hSetProperty( obj, names, values )
        % Only for PCTGetSet properties. Add any non-default behaviour here
            if ~iscell( names )
                names  = { names };
                values = { values };
            end
            licenseNumberProperty = {'LicenseNumber'};
            for ii = 1:length( names )
                if ismember( names{ii}, licenseNumberProperty ) && ...
                        ~strcmp( values{ii}, obj.LicenseNumber )
                    % If the license number is different to the currently
                    % stored number then check its validity before setting
                    % and make sure we stash the corresponding entitlement.
                    try
                        obj.LicenseEntitlement = obj.checkLicenseNumber( values{ii} );
                    catch E
                        throwAsCaller( E );
                    end
                end
                obj.hSetPropertyNoCheck( names{ii}, values{ii} );
            end
        end

        function newjob = hBuildChild( obj, variant, jobID, jobSId )
        % Build a new job object of the requested variant. Called by the
        % support which caches the resulting new job.

            import parallel.job.MJSIndependentJob
            import parallel.job.MJSCommunicatingJob
            import parallel.internal.types.Variant

            try
                switch variant
                  case Variant.IndependentJob
                    newjob = MJSIndependentJob( ...
                        obj, jobID, jobSId, obj.Support );
                  case { Variant.CommunicatingSPMDJob, ...
                         Variant.CommunicatingPoolJob }
                    newjob = MJSCommunicatingJob( ...
                        obj, jobID, variant, jobSId, obj.Support );
                  otherwise
                    error(message('parallel:cluster:MJSUnexpectedType'));
                end
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end

        % Add Cluster Properties to Display
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            import parallel.internal.types.MJSComputeCloudStates
            
            % For MJS compute cloud clusters, we want to insure that we
            % carefully use hVectorizedGet when possible for remote calls,
            % as requesting multiple properties simultaneously is much
            % more efficient, so we do not call the base class map.
            % We build our own.
            % Create an empty map to hold displayable cluster properties
            clusterPropertyMap = containers.Map('KeyType', 'char', 'ValueType', 'any');
            
            % 'Separator' is not a typical name-value object. However, it
            % needs to be added to the propNames list in between property
            % groups, so it is put into the map with the other
            % displayable items.
            clusterPropertyMap('Separator') = diFactory.createSeparator();
            
            % If we are not attached to the clusters and cannot make remote
            % calls there is a limited list of interesting properties
            if isempty(obj.Support)
                % The order the properties appear in the propNames array is the
                % order in which they will be displayed. NOTE: There are
                % different display items for  attached and non-attached
                % clusters, so there are different propNames arrays.
                propNames = {...
                    'Name', ...
                    'State', ...
                    'Profile', ...
                    };
                clusterPropertyMap('Name') = diFactory.createDefaultItem(char(obj.ClusterDetails.getClusterName()));
                clusterPropertyMap('State') = diFactory.createDefaultItem(obj.ClusterState.Name);
                clusterPropertyMap('Profile') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('Profile'));
                               
            else
                % The order the properties appear in the propNames array is the
                % order in which they will be displayed. NOTE: There are
                % different display items for  attached and non-attached
                % clusters, so there are different propNames arrays.
                propNames = {...
                    'Name', ...
                    'State', ...
                    'Profile', ...
                    'Modified', ...
                    'Host', ...
                    'Separator', ...
                    'NumWorkers', ...
                    'NumBusyWorkers', ...
                    'NumIdleWorkers', ...
                    'Separator', ...
                    'JobStorageLocation', ...
                    'ClusterMatlabRoot', ...
                    'OperatingSystem', ...
                    'RequiresMathWorksHostedLicensing'};
                
                % Add cluster specific items to map for display
                names = {...
                    'Name', ...
                    'State', ...
                    'Profile', ...
                    'Modified', ...
                    'Host', ...
                    'NumWorkers', ...
                    'NumBusyWorkers', ...
                    'NumIdleWorkers'...
                    'JobStorageLocation', ...
                    'ClusterMatlabRoot', ...
                    'OperatingSystem', ...
                    'RequiresMathWorksHostedLicensing', ...
                    'LicenseNumber'};
                values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names, @hVectorisedGet));
                clusterPropertyMap = [clusterPropertyMap; containers.Map(names, values)];
                
                % If there is a communication error with a stashed message, add
                % it to the display and wrap it so we don't lose any information
                % in the text of the message.
                stashedErrorMessage = obj.hGetDisplayPropertiesNoError('StashedErrorMessage');
                if obj.ClusterState >= MJSComputeCloudStates.CommunicationError && ~isempty(stashedErrorMessage)
                    clusterPropertyMap('ErrorMessage') = diFactory.createWrappedTextDisplay(stashedErrorMessage);
                    % NOTE: Also add the error message to propNames because
                    % we don't want it there unless we need it.
                    propNames{end+1} = 'ErrorMessage';
                end
            end
        end
        
        % Get the data necessary to create a new cluster equivalent to this one.
        function data = hGetMementoData(obj)
            data = struct('Identifier',  obj.Identifier,...
                          'Certificate', obj.Certificate);
        end
        
        function getClusterLogs( obj, saveLocation )
        % Get the MJS logs from the job manager and save as a zip file
        % in the saveLocation.
            
            if nargin < 2
                saveLocation = '.';
            end
            filename = [obj.Name '-logs.zip'];
            filename = fullfile( saveLocation, filename );

            fcn = @( s, mjs ) s.getClusterLogs( filename );
            try
                obj.invokeWithSupportAndRetry( fcn );
            catch E
                error(message('parallel:cluster:MJSComputeCloud:FailedToGetLogs',...
                      obj.Name, E.getReport));
            end
        end
        
        function [p, q, r, f] = hEnumerateJobsInStates(obj)
            
            % This function is only used in display, so we don't want to
            % error. The State field will be set to 'communicationerror'
            fcn = @( s, mjs ) s.enumerateJobsInStates();
            try
                [p, q, r, f] = obj.invokeWithSupportAndRetry( fcn );
            catch E %#ok<NASGU>
                [p, q, r, f] = deal(0,0,0,0);
                return;
            end
        end
   
    end

% Public, user-visible methods
% -------------------------------------------------------------------------
    methods ( Access = public )

        function obj = MJSComputeCloud( varargin )
        %MJSComputeCloud Create a MJSComputeCloud cluster instance
        %   parallel.cluster.MJSComputeCloud(p1, v1, p2, v2, ...) builds an
        %   MJSComputeCloud cluster with the specified property values. The
        %   properties can include any of the properties of the
        %   MJSComputeCloud cluster class, or a profile.
        %   
        %   Example:
        %   % Create an MJSComputeCloud cluster using the 'myCloud' profile.
        %   myCluster = parallel.cluster.MJSComputeCloud('Profile', 'myCloud');
        %
        %   See also PARCLUSTER, parallel.Cluster.

            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.internal.settings.ValueType
            import com.mathworks.toolbox.distcomp.wsclients.cloudconsole.CloudConsoleClusterInfo
            import com.mathworks.toolbox.distcomp.wsclients.cloudconsole.CloudConsoleClusterDetails

            % Constructor called by hDiscoverClusters
            if nargin >= 1 && isa( varargin{1}, 'com.mathworks.toolbox.distcomp.wsclients.cloudconsole.CloudConsoleClusterInfo' )

                clusterInfo = varargin{1};
                clusterDetailsFunc = @() clusterInfo.getClusterDetails();

                % Extract the certificate and identifier from the
                % ClusterInfo
                clusterCert = char( clusterInfo.getSSLCertificate() );
                clusterCertType = ValueType.User;
                identifier  = char( clusterInfo.getClusterID() );
                identifierType = ValueType.User;

                % Fake up the profile and constructor args
                propsFromArgs   = containers.Map();
                cProf = '';
                
                usePeerLookup = true;
                
            else
                % Profile or name-value pairs
                try
                    [propsFromArgs, cProf] = ...
                        ConstructorArgsHelper.interpretClusterCtorArgs( ...
                            ?parallel.cluster.MJSComputeCloud, varargin{:} );
                catch E
                    throw( E );
                end

                [ok, identifier, identifierType] = iMustGetPropFromMapAndRemove(propsFromArgs,'Identifier');
                if ~ok
                    error(message('parallel:cluster:MJSComputeCloud:MissingIdentifier'));
                end

                [ok, clusterCert, clusterCertType] = iMustGetPropFromMapAndRemove(propsFromArgs,'Certificate');
                if ~ok
                    error(message('parallel:cluster:MJSComputeCloud:MissingClusterCertificate'));
                end

                % Allow override of UsePeerLookup for testing with local
                % MJS
                usePeerLookup = iGetPropFromMapAndRemoveWithDefaultValue(propsFromArgs,'UsePeerLookup',true);

                % Get the cluster details from Cloud Console
                clusterDetailsFunc = @() parallel.cluster.MJSComputeCloud.getClusterDetailsFromCloudConsole( identifier );
                
            end

            % Construct the cluster object
            obj@parallel.Cluster('MJSComputeCloud', cProf);
            
            factoryLicenseNumber = '';
            % Put all PCTGetSet properties into PropStorage.
            obj.PropStorage.addFactoryValue( 'Identifier',  identifier );
            obj.PropStorage.setValueType(    'Identifier',  identifierType);
            obj.PropStorage.addFactoryValue( 'Certificate', clusterCert );
            obj.PropStorage.setValueType(    'Certificate', clusterCertType );
            obj.PropStorage.addFactoryValue( 'LicenseNumber', factoryLicenseNumber );
            
            % Make sure we set the non-PCTGetSet parameters
            obj.UsePeerLookup      = usePeerLookup;
            
            % Ensure any remaining constructor args go through the correct set methods.
            obj.applyConstructorArgs(propsFromArgs);

            % Finally get the cluster details.
            try
                obj.ClusterDetails = clusterDetailsFunc();
            catch E
                throw( E );
            end
            % The cluster details will be empty if the user cancelled the login
            % dialog (g823541).
            if isempty( obj.ClusterDetails )
                error(message('parallel:cluster:MJSComputeCloud:LoginRequired'));
            end
            obj.ClusterState   = obj.getVisibleClusterStateFromClusterDetails();
                       
            % Try to create the support object
            try
                obj.getSupport();
            catch E
                % Catch the case where we can have a valid MJSComputeCloud
                % but no support object
                if ~( strcmp(E.identifier,'parallel:cluster:MJSComputeCloud:ClusterNotReady') || ...
                      strcmp(E.identifier,'parallel:cluster:MJSComputeCloud:ClusterNotRunning') || ...
                      strcmp(E.identifier,'parallel:cluster:MJSComputeCloud:ClusterDeleted') )
                    throw(E);
                end
                obj.Support = parallel.internal.cluster.MJSSupport.empty();
            end
        end
    end
end
%#ok<*ATUNK> custom attributes

% -------------------------------------------------------------------------
% iGetPropValAndRemove - simply return the value from the propsFromArgs map
% and remove it from the map it exists.
function [val, type] = iGetPropValAndRemove( map, prop )
    if map.isKey( prop )
        valtmp = map(prop);
        val    = valtmp{1};
        type   = valtmp{2};
        map.remove(prop);
    else
        val    = '';
    end
end

% iMustGetPropFromMapAndRemove - return the value and type from the map if possible,
% setting ok to indicate a successful lookup
function [ok, val, type] = iMustGetPropFromMapAndRemove( map, prop )
    if ~map.isKey(prop)
        ok = false;
        val='';
        type=[];
    else
        [val, type] = iGetPropValAndRemove( map, prop );
        ok     = true;
    end
end

% iGetPropFromMapAndRemoveWithDefaultValue - return the value if it's in the map,
% otherwise return the supplied default value
% Also remove the prop from the map if it exists.
function val = iGetPropFromMapAndRemoveWithDefaultValue( map, prop, default )
    if map.isKey(prop)
        val = iGetPropValAndRemove(map,prop);
    else
        val = default;
    end
end
