% CJSCluster - base class for Clusters other than MJS.
% Manages:
% - Support, SupportID - the CJSSupport object and ID
% - HasSharedFilesystem - simple user-visible flag
% - JobStorageLocation

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Hidden ) CJSCluster < parallel.Cluster

    % --------------------------------------------------------------------------
    % Property definitions
    properties ( Transient, GetAccess = protected, SetAccess = private )
        Support     % Our CJSSupport
        SupportID   % Our ID for the CJSSupport
    end

    properties ( PCTGetSet, Transient, PCTConstraint = 'logicalscalar' )
        %HasSharedFilesystem Specify whether the client and cluster nodes share JobStorageLocation
        %   HasSharedFilesystem indicates whether the location identified by
        %   the JobStorageLocation property can be accessed from the client
        %   machine and all nodes in the cluster.
        % 
        %   See also parallel.cluster.CJSCluster.JobStorageLocation
        HasSharedFilesystem = true;
        
        %RequiresMathWorksHostedLicensing Cluster uses MathWorks hosted licensing
        %   RequiresMathWorksHostedLicensing indicates whether the cluster
        %   uses MathWorks hosted licensing.  You must provide your MathWorks
        %   account login information if this value is true.  If you have more
        %   than one MDCS license associated with your MathWorks account
        %   you will also need to supply the LicenseNumber to use.
        %
        %   See also parallel.cluster.CJSCluster.LicenseNumber
        RequiresMathWorksHostedLicensing
    end

    % Everything to do with JobStorageLocation - we stash the actual struct in props.
    properties ( PCTGetSet, Transient, PCTConstraint = 'datalocation' )
        %JobStorageLocation Folder where cluster stores job and task information
        %   JobStorageLocation is a string or structure specifying a folder
        %   where the cluster stores job information. In a shared file
        %   system, the client and all cluster nodes, must have access to
        %   this folder. In a nonshared file system, only the MATLAB client
        %   accesses job information in this folder.
        %   
        %   If JobStorageLocation is not set, the default folder for job
        %   information is the current working directory of the MATLAB
        %   client the first time you create the cluster, either using
        %   parcluster or the cluster construction.
        %   
        %   Use a structure to specify the JobStorageLocation in an
        %   environment of mixed platforms. The fields for the structure
        %   are named windows and unix. Each node then uses the field
        %   appropriate for its platform.
        %   
        %   The job information stored in the folder identified by
        %   JobStorageLocation might not be compatible between different
        %   versions of MATLAB Distributed Computing Server and different
        %   cluster types. Therefore, JobStorageLocation should not be
        %   shared by different cluster types nor by parallel computing
        %   products running different versions; each version on your
        %   cluster should have its own JobStorageLocation.
        %
        %   See also parallel.cluster.OperatingSystem,
        %            parallel.cluster.Type.
        JobStorageLocation = '';
    end

    properties ( PCTGetSet, Transient, PCTConstraint = 'string' )
        %ClusterMatlabRoot Path to MATLAB on the cluster
        %   ClusterMatlabRoot specifies the path to MATLAB for the cluster
        %   to use for starting MATLAB worker processes. The path must be
        %   accessible to all cluster nodes on which worker sessions will
        %   run.
        %
        %   ClusterMatlabRoot is a string; it must be structured
        %   appropriately for the file system of the cluster nodes. If the
        %   value is empty, the MATLAB executable must be on the path of
        %   the cluster nodes.
        ClusterMatlabRoot = matlabroot;

        %LicenseNumber License number to use with MathWorks hosted licensing
        %   License number specifies the license number to use on the workers
        %   when the cluster is using MathWorks hosted licensing.  This string
        %   must be the same as one of the licenses listed in your MathWorks
        %   account.
        %
        %   LicenseNumber cannot be defined if RequiresMathWorksHostedLicensing is
        %   false.
        %    
        %   See also parallel.cluster.CJSCluster.RequiresMathWorksHostedLicensing
        LicenseNumber
    end
    properties ( PCTGetSet, Transient, ...
                 PCTConstraint = 'enum:parallel.internal.types.OperatingSystem' )
        %OperatingSystem Operating System of the cluster
        %   OperatingSystem specifies the operating system of the nodes in
        %   the cluster. Valid values are 'windows', 'unix', and 'mixed'.
        %
        %   See also parallel.Cluster.Host
        OperatingSystem
    end
    properties ( PCTGetSet, Transient, PCTConstraint = 'positiveintscalar' )
        %NumWorkers Number of workers in the cluster
        %   NumWorkers specifies the maximum number of workers that this
        %   cluster can start for running a job. NumWorkers must fall
        %   within the range defined by a communicating job's
        %   NumWorkersRange.
        NumWorkers = Inf;
    end
    % --------------------------------------------------------------------------
    % Public/Hidden/Abstract methods: This class defines all the properties and
    % methods that CJSSupport and CJS*Job can rely on. These are the methods
    % that subclasses must override
    methods ( Hidden, Abstract )

        % hDestroyJob - contact the underlying scheduler to remove the job
        hDestroyJob( cluster, job )

        % hDestroyTask - contact the underlying scheduler to remove the task
        hDestroyTask( cluster, task )

        % hCancelJob - contact the underlying scheduler to cancel execution of a
        % job. Return true if execution was cancelled.
        didCancel = hCancelJob( cluster, job )

        % hCancelTask - contact the underlying scheduler to cancel execution of
        % a task. Return true if execution was cancelled.
        didCancel = hCancelTask( cluster, task )

        % hSubmit*Job - contact the underlying scheduler to schedule execution
        % of the job.
        hSubmitIndependentJob( cluster, job, jobSupport, jobSupportID )
        hSubmitCommunicatingJob( cluster, job, jobSupport, jobSupportID )
    end


    % --------------------------------------------------------------------------
    % Protected/Abstract methods that must be provided by CJSCluster
    % implementations.
    methods ( Abstract, Access = protected )
        % Use the underlying cluster mechanisms to see if there's a better value
        % for the jobState given the state in the storage
        jobState = getJobState( cluster, job, jobStateInStorage )

        % This check simply tests whether a job was submitted by this Cluster
        % type.
        tf       = wasSubmittedByThisType( cluster, job )
    end

    % --------------------------------------------------------------------------
    % Debug Log methods - can be overridden by subclasses if necessary.
    methods ( Access = protected )
        function logText = getDebugLogForCommunicatingJob( obj, cJob )
        % Return CommunicatingJob debug log
            jobSID       = cJob.hGetSupportID();
            taskSIDs     = arrayfun( @hGetSupportID, cJob.Tasks, ...
                                     'UniformOutput', false );
            taskIDs      = arrayfun( @(x) x.Id, cJob.Tasks, ...
                                     'UniformOutput', false );
            [logText{1}, logExists] = obj.Support.readCommJobLogFile( ...
                jobSID, cJob.Variant );
            logText{2}   = obj.Support.readTaskJavaLogsIfPresent( ...
                jobSID, taskSIDs, taskIDs );
            logText{3}   = obj.getSpecificDebugLogForCommunicatingJob( cJob, logExists );
            logText      = sprintf( '%s', logText{:} );
        end
        
        function logText = getSpecificDebugLogForCommunicatingJob( varargin )
            logText = '';
        end

        function logText = getDebugLogForIndependentTask( obj, iJob, task )
        % Return IndependentJob.Task debug log
            taskSID      = task.hGetSupportID();
            jobSID       = iJob.hGetSupportID();
            [logText{1}, logExists] = obj.Support.readIndJobTaskLogFile( ...
                jobSID, iJob.Variant, taskSID );
            logText{2}   = obj.Support.readTaskJavaLogsIfPresent( ...
                jobSID, {taskSID}, {task.Id} );
            logText{3}   = obj.getSpecificDebugLogForIndependentTask( iJob, task, logExists );
            logText      = sprintf( '%s', logText{:} );
        end

        function logText = getSpecificDebugLogForIndependentTask( varargin )
            logText = '';
        end
    end

    % --------------------------------------------------------------------------
    % Public/Hidden methods implemented once and for all for CJSClusters.
    methods ( Sealed, Hidden )

        function support = hGetSupport( obj )
        % Return the underlying CJSSupport.
            support = obj.Support;
        end
        function sid = hGetSupportID( obj )
        % Return the identifier to be used with the support.
            sid = obj.SupportID;
        end

        function newjob = hBuildChild( obj, variant, jobid, jobsid )
        % hBuildChild is called by the Support to actually construct the MCOS
        % Job representation from information on disk.
            import parallel.internal.types.Variant;
            switch variant
              case Variant.IndependentJob
                newjob = parallel.job.CJSIndependentJob( ...
                    obj, jobid, jobsid, obj.Support );
              case { Variant.CommunicatingSPMDJob, Variant.CommunicatingPoolJob }
                newjob = parallel.job.CJSCommunicatingJob( ...
                    obj, jobid, variant, jobsid, obj.Support );
              otherwise
                assert( false );
            end
        end

        function state = hGetJobState( cluster, job, jobStateInStorage )
        % hGetJobState starts from the information in the storage, and uses
        % various means to work out
            import parallel.internal.types.States
            import parallel.internal.cluster.CJSJobMethods
            import parallel.internal.cluster.JobStateHelper

            if ~ ( jobStateInStorage == States.Queued || ...
                   jobStateInStorage == States.Running || ...
                   jobStateInStorage == States.Unavailable )
                % job is not queued/running/unavailable, so don't bother asking the cluster
                % object.
                state = jobStateInStorage;
                return
            end

            stateFromTasks = job.hGetStateFromTasks();
            if jobStateInStorage == States.Unavailable && isvalid( job )
                % We've got a valid job that we do not know the state of. If we got a useful 
                % state from looking at the tasks, let's write it to disk.
                try
                    if stateFromTasks.isTerminal()
                        job.hSetTerminalStateFromCluster( stateFromTasks );
                    else
                        job.hSetPropertyNoCheck( 'StateEnum', stateFromTasks );
                    end
                    jobStateInStorage = stateFromTasks;
                catch E
                    dctSchedulerMessage( 1, 'Failed to write new state %s for job %d: %s', ...
                                         stateFromTasks.Name, job.ID, getReport( E ) );
                end
            end

            isOurJob = false;
            try
                isOurJob = cluster.wasSubmittedByThisType( job );
            catch err
                % This might error if storage is in a bad state, in which
                % case we have to assume that it isn't our job.
                dctSchedulerMessage( 4, 'Failed to determine if job %d belongs to this cluster: %s', ...
                    job.ID, err.getReport());
            end
            
            if ~isOurJob
                % Not our job
                state = jobStateInStorage;
                return
            end

            % If we get here, we've got a Queued/Running/Unavailable job that we know how to
            % deal with, so defer to our protected method to deal with talking
            % to the mechanisms behind the cluster.
            clusterState = States.Unknown;
            try
                clusterState = cluster.getJobState( job, jobStateInStorage );
            catch err
                % This might error if storage is in a bad state, in which
                % case we have to fall back to the Unknown state
                dctSchedulerMessage( 4, 'Failed to get the state for job %d from the cluster: %s', ...
                    job.ID, err.getReport());
            end
            
            % Nobody knows anything about the job at this point, so bail.
            if clusterState == States.Unknown && jobStateInStorage == States.Unknown
                state = States.Unknown;
                return;
            end
            
            if clusterState == States.Unknown 
                if jobStateInStorage.isTerminal()
                    clusterState = jobStateInStorage; 
                else
                    % If the cluster doesn't know anything about the job, then 
                    % defer to JobStateHelper to figure out job state.  Note that
                    % JobStateHelper has logic to wait for the state files to update
                    % before really passing judgment on the job state.
                    % In the absence of a real UUID for the job, just use the cluster
                    % type and the job ID. 
                    clusterState = JobStateHelper.getJobStateFromTasks( ...
                        job, jobStateInStorage, sprintf( '%s:%d', class(job.Parent), job.ID ) );
                    % If the cluster doesn't know about a job, it must be in a 
                    % terminal state, so if we didn't get a terminal state
                    % back, the tasks must indicate the job is still
                    % running/queued and we must fail the job.
                    if ~clusterState.isTerminal()
                        clusterState = States.Failed; 
                    end
                end
            end
            
            % Use the state that is furthest ahead, but if the cluster said
            % that the job was failed, then fall back to the disk state if it
            % is terminal as we know that MATLAB can still exit with non-zero 
            % exit status and be OK.
            shouldIgnoreClusterFailed = numel(job.Tasks) > 0 && ...
                                        clusterState == States.Failed && ...
                                        stateFromTasks.isTerminal();
            if shouldIgnoreClusterFailed || ...
                stateFromTasks > clusterState
                state = stateFromTasks;
            else
                state = clusterState;
            end

            % If the state is terminal, we should ensure that the job and tasks
            % have the right state.
            if state.isTerminal()
                job.hSetTerminalStateFromCluster( state );
            end
        end

        function [job, task] = hGetJobAndTaskFromLocations( obj, jobLoc, taskLoc )
        % Used by distcomp_evaluate_filetask to bring up the job and task
        % Note that 'location' is basically the supportId.
            job = obj.Support.getJobFromLocation( obj, jobLoc );
            task = obj.Support.getTaskFromLocation( job, taskLoc );
        end

        function filename = hGetTaskJavaLogIfPossible( obj, task )
        % hGetTaskJavaLogIfPossible - return a java logfile location, or ''.
        % Called from distcomp_evaluate_filetask to initiate java logging.
        % Returns job3/Task7.java.log - call site might choose to append ".0", ".1" etc.
            taskSID  = task.hGetSupportID();
            filename = obj.Support.getTaskJavaLogBaseIfPossible( taskSID );
        end

    end
    methods ( Hidden )
        function hSetProperty( obj, names, values )
            if ~iscell( names )
                names  = { names };
                values = { values };
            end
            for ii = 1:length( names )
                if isequal( names{ii}, 'JobStorageLocation' )
                    try
                        legacyDataLoc = iDataLocToLegacyStruct( values{ii} );
                        stor          = distcomp.filestorage( legacyDataLoc );
                    catch E
                        throwAsCaller( E );
                    end

                    obj.Support.invalidateAllChildren( obj.SupportID );
                    obj.Support   = parallel.internal.cluster.CJSSupport( stor );
                    obj.SupportID = stor.pGetRootLocation();
                end
                
                if isequal( names{ii}, 'RequiresMathWorksHostedLicensing' ) 
                    if isdeployed && values{ii} == true
                        % Can't use MHLM workers from a deployed application
                        error(message('parallel:cluster:CannotUseMHLMFromDeployed'));
                    end
                    if values{ii} == false && ...
                        obj.isNotCurrentValue('RequiresMathWorksHostedLicensing', values{ii}) && ...
                        ~isempty( obj.LicenseNumber )
                        % If switching RequiresMathWorksHostedLicensing from true to false, 
                        % then we must clear out the license number as it no longer
                        % makes sense.
                        obj.hSetPropertyNoCheck( 'LicenseNumber', '' );
                    end
                end

                if isequal( names{ii}, 'LicenseNumber' ) && ...
                    obj.isNotCurrentValue('LicenseNumber', values{ii})
                    % If the license number is different to the currently
                    % stored number then check its validity before setting
                    % and make sure we stash the corresponding entitlement.
                    try
                        obj.LicenseEntitlement = obj.checkLicenseNumber( values{ii} );
                    catch E
                        throwAsCaller( E )
                    end
                end    

                obj.hSetPropertyNoCheck( names{ii}, values{ii} );
            end
        end
        function hPreJobEvaluate( ~, ~, ~ ) % obj, job, task
        % hPreJobEvaluate - perform any pre-job evaluation for this cluster type.
        end
    end

    % --------------------------------------------------------------------------
    % Implementation of some abstract/protected methods
    methods ( Sealed, Access = protected )
        function jl = getJobs( obj )
            jl = obj.Support.getJobs( obj, obj.SupportID );
        end
        function job = buildIndependentJob( obj )
            variant = parallel.internal.types.Variant.IndependentJob;
            job     = obj.Support.buildJob( obj, obj.SupportID, variant );
        end
        function job = buildCommunicatingJob( obj, variant )
            job = obj.Support.buildJob( obj, obj.SupportID, variant );
            % Communicating jobs need to have their NumWorkersRange set
            job.NumWorkersRange = [1 obj.NumWorkers];
        end
        function connMgr = buildConnectionManager( ~ )
            connMgr = parallel.internal.apishared.ConnMgrBuilder.buildForCJS();
        end
        function varargout = getJobsByState( obj )
            import parallel.internal.types.States;
            statesStr = { States.Pending.Name, ...
                          States.Queued.Name, ...
                          States.Running.Name, ...
                          States.Finished.Name, ...
                          States.Failed.Name };
            pqrff = obj.Support.getJobsByState( obj, obj.SupportID, statesStr );
            % Need to concatenate Finished/Failed
            if nargout == 4
                varargout = { pqrff(1), pqrff(2), pqrff(3), {[pqrff{4:end}]} };
            else
                varargout = { [ pqrff(1:3), {[pqrff{4:end}]} ] };
            end
        end
    end

    methods ( Static, Access = protected )
        function [dataLoc, dataLocValType] = chooseDataLocation( propsFromArgs )
        % Look through the arguments we've got and see if there's a JobStorageLocation, else
        % derive the default. We will remove the JobStorageLocation from the propsFromArgs
        % map.
            if propsFromArgs.isKey( 'JobStorageLocation' )
                dataLocAndType = propsFromArgs('JobStorageLocation');
                dataLoc        = dataLocAndType{1};
                dataLocValType = dataLocAndType{2};
                propsFromArgs.remove( 'JobStorageLocation' );
            else
                dataLoc        = pwd;
                dataLocValType = parallel.internal.settings.ValueType.Factory;
            end
        end
    end

    methods ( Access = protected )
        function obj = CJSCluster( type, cProf, dataLoc, dataLocType )
            import parallel.internal.apishared.OsType

            obj@parallel.Cluster( type, cProf );

            try
                legacyDataLoc = iDataLocToLegacyStruct( dataLoc );
            catch E
                throwAsCaller( E );
            end

            % We must seed all our PCTGetSet properties into PropStorage
            factorySharedFilesystem = true;
            factoryMatlabroot       = matlabroot;
            factoryNumWorkers       = Inf;
            % Factory 'OperatingSystem' is local OS.
            clientOsEnum            = OsType.forClient();
            factoryOs               = clientOsEnum.Api2Name;
            
            factoryRequiresMHLM     = false;
            factoryLicenseNumber    = '';

            obj.PropStorage.addFactoryValue( 'ClusterMatlabRoot', factoryMatlabroot );
            obj.PropStorage.addFactoryValue( 'NumWorkers', factoryNumWorkers );
            obj.PropStorage.addFactoryValue( 'JobStorageLocation', dataLoc );
            obj.PropStorage.setValueType(    'JobStorageLocation', dataLocType );
            obj.PropStorage.addFactoryValue( 'HasSharedFilesystem', factorySharedFilesystem );
            obj.PropStorage.addFactoryValue( 'OperatingSystem', factoryOs );
            obj.PropStorage.addFactoryValue( 'RequiresMathWorksHostedLicensing', factoryRequiresMHLM );
            obj.PropStorage.addFactoryValue( 'LicenseNumber', factoryLicenseNumber );

            try
                stor          = distcomp.filestorage( legacyDataLoc );
                obj.Support   = parallel.internal.cluster.CJSSupport( stor );
                obj.SupportID = stor.pGetRootLocation();
            catch E
                throwAsCaller( E );
            end
        end
    end

    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
    
    % --------------------------------------------------------------------------
    % Environment Variable methods
    methods ( Sealed, Access = protected )
        function [names, values] = getClusterEnvVarsForJobSubmission( obj )
        % Get environment variables that are required for job submission.
        % This is called by the hSubmit*Job methods.  
            [licNames,  licVars]    = obj.getWebLicensingEnvVars();
            [clusNames, clusValues] = obj.getClusterSpecificEnvVars();
            names                   = [ licNames(:); clusNames(:); ];
            values                  = [ licVars(:); clusValues(:); ];
        end
    end
    
    methods ( Abstract, Access = protected )
        % Return names/values of environment variables that this cluster must
        % send down to workers
        [names, values] = getClusterSpecificEnvVars( obj )
    end
    
    methods ( Access = private )
        function [names, values] = getWebLicensingEnvVars( obj )
            names  = {};
            values = {};
            if ~obj.RequiresMathWorksHostedLicensing
                return;
            end

            % It is safe to get obj.LicenseEntitlement because create*Job will already
            % have called obj.ensureLicenseNumberIsSetOrError();
            desktopClient = parallel.internal.webclients.currentDesktopClient();
            names  = { 'MLM_WEB_LICENSE', ...
                       'MLM_WEB_USER_CRED', ...
                       'MLM_WEB_ID', ...
                       'MDCE_LICENSE_NUMBER'};
            values = { 'true', ...
                       desktopClient.LoginToken, ...
                       obj.LicenseEntitlement.Id, ...
                       obj.LicenseEntitlement.LicenseNumber };
        end
    end

    % --------------------------------------------------------------------------
    % User-visible methods - getDebugLog.
    methods (Sealed)
        function varargout = getDebugLog( cluster, jobOrTask )
        %getDebugLog Read output messages from communicating job or independent task
        %
        %   str = getDebugLog(myCluster, jobOrTask) returns any output
        %   written to the standard output or standard error stream by the
        %   communicating job or independent task identified by jobOrTask,
        %   being run by the cluster identified by myCluster.
        %
        %   Example:
        %   % Use batch to run a script that uses a the local cluster and
        %   % a matlabpool of size 2.
        %   myJob = batch('aScript', 'Profile', 'local', 'Matlabpool', '2'); 
        %   wait(myJob);
        %
        %   % Look at the debug log. 
        %   myCluster = parcluster('local');
        %   getDebugLog(myCluster, myJob);

            import parallel.internal.types.Variant
            validateattributes( cluster, {'parallel.cluster.CJSCluster'}, ...
                                {'scalar'}, 'getDebugLog', 'cluster', 1 );

            narginchk( 2, 2 );
            if numel( jobOrTask ) ~= 1
                error(message('parallel:cluster:GetDebugLogScalarJobOrTask'));
            end
            switch jobOrTask.Variant
              case { Variant.CommunicatingPoolJob, Variant.CommunicatingSPMDJob }
                job        = jobOrTask;
                jobCluster = job.Parent;
                fcnH       = @() getDebugLogForCommunicatingJob( ...
                    cluster, job );
              case Variant.Task
                task       = jobOrTask;
                job        = task.Parent;
                jobCluster = job.Parent;
                fcnH       = @() getDebugLogForIndependentTask( ...
                    cluster, job, task );
                if job.Variant ~= Variant.IndependentJob
                    error(message('parallel:cluster:GetDebugLogJobMustBeIndependent'));
                end
              otherwise
                error(message('parallel:cluster:GetDebugLogScalarJobOrTask'));
            end
            if cluster ~= jobCluster
                error(message('parallel:cluster:GetDebugLogWrongCluster'));
            end
            if ~ cluster.wasSubmittedByThisType( job )
                % No debug log / not our job etc.
                varargout = cell( 1, nargout );
                return;
            end
            % Actually invoke the underlying method. Note we have captured the
            % different arg-lists into the function handle.
            try
                logData = fcnH();
                if nargout > 0
                    varargout = { logData };
                else
                    disp( logData );
                end
            catch E
                throw( E );
            end
        end
    end

    % --------------------------------------------------------------------------
    % API2(orig) compatibility for distcomp_evaluate_filetask
    properties ( Hidden, Dependent )
        SharedFilesystem
    end
    methods
        function set.SharedFilesystem( obj, val )
            obj.HasSharedFilesystem = val;
        end
        function val = get.SharedFilesystem( obj )
            val = obj.HasSharedFilesystem;
        end
    end

    % --------------------------------------------------------------------------
    % API1 compatibility for RemoteClusterAccess
    methods ( Hidden )
        function result = pGetJobFilesInfo( obj, job )
            assert( job.Parent == obj );
            result = obj.Support.getJobFilesInfo( job.hGetSupportID(), numel( job.Tasks ) );
        end
    end
end
%#ok<*ATUNK> custom attributes


% --------------------------------------------------------------------------
% iDataLocToStruct - canonicalize the JobStorageLocation struct, return the value
% required by distcomp.filestorage/serializer.
function legacyVal = iDataLocToLegacyStruct( val )
% Helper to canonicalize datalocation.

% First, check the value is OK
    parallel.internal.customattr.checkSimpleConstraint( ...
        'datalocation', 'JobStorageLocation', val );

    fields = { 'windows', 'unix' };
    if isunix
        fields = fields(2:-1:1);
    end
    if ischar( val )
        val = struct( fields{1}, val, fields{2}, '' );
    end
    % Create the API1-compatible version for the filestorage constructor
    legacyVal = struct( 'pc', val.windows, ...
                        'unix', val.unix );
end
