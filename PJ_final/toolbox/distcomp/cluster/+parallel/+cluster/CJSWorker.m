% CJSWorker Worker in a CJS cluster
%
%   parallel.cluster.CJSWorker properties:
%      ComputerType - Type of computer on the worker
%      Host         - Host where the worker process executed a task
%      ProcessId    - The process identifier for the worker

% Copyright 2011-2012 The MathWorks, Inc.
classdef ( Sealed ) CJSWorker < parallel.Worker
    properties ( SetAccess = immutable )
        % ProcessId The process identifier for the worker
        ProcessId
    end
    methods ( Hidden )
        function struc = hGetStruct( worker )
        % Return a struct to persist to disk.
            struc = struct( 'Host',         worker.Host, ...
                            'ComputerType', worker.ComputerType, ...
                            'ProcessId',    worker.ProcessId );
        end
    end
    methods ( Access = private )
        function obj = CJSWorker( hostName, computerType, pid )
        % Must use one of the static methods to build instances of CJSWorker.
            obj@parallel.Worker( hostName, computerType );
            obj.ProcessId = pid;
        end

        function delete( obj )
            delete@handle( obj );
        end
    end
    methods ( Hidden )
        function x = hGetProperty( ~, nameOrNames )
        % This class has no PCTGetSet properties, so these methods are
        % unimplemented.
            assert( iscell( nameOrNames ) && isempty( nameOrNames ) );
            x = [];
        end
        function hSetProperty( ~, ~, ~ )
            assert( false );
        end
        function hSetPropertyNoCheck( ~, ~, ~ )
            assert( false );
        end
        
        function [workerPropertyMap, propNames]  = hGetDisplayItems(obj, diFactory)
            workerPropertyMap = hGetDisplayItems@parallel.Worker(obj, diFactory);

            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'Host', ...
                'ComputerType', ...
                'Separator', ...
                'ProcessId'};
              
            % Add CJSWorker specific displayable items to map. 
            workerPropertyMap('ProcessId') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('ProcessId'));
        end
    end
    
    methods ( Hidden, Static )
        function worker = hBuildForCurrentProcess()
        % Build a worker instance for this process
            hostName     = char( java.net.InetAddress.getLocalHost.getCanonicalHostName() );
            computerType = upper( dct_arch() );
            pid          = feature( 'getpid' );
            worker       = parallel.cluster.CJSWorker( hostName, computerType, pid );
        end
        function worker = hBuildFromStruct( struc )
        % Reconstitute from disk structure.
            if isa( struc, 'struct' ) && ...
                isfield( struc, 'Host' ) && ...
                isfield( struc, 'ComputerType' ) && ...
                isfield( struc, 'ProcessId' )
                worker = parallel.cluster.CJSWorker( struc.Host, ...
                                                     struc.ComputerType, ...
                                                     struc.ProcessId );
            else
                % Information not yet available
                worker = [];
            end
        end
    end
end
