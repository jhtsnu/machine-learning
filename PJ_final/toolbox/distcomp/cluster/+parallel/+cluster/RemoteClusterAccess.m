classdef RemoteClusterAccess < handle
%RemoteClusterAccess Connect to clusters when the cluster's client utilities are not available locally
%   RemoteClusterAccess allows you to establish a connection and run
%   commands on a remote host. This class is intended for use with the
%   generic scheduler interface when using remote submission of jobs or on
%   nonshared file systems.
%
%   r = RemoteClusterAccess(username) uses the supplied username when
%   connecting to the remote host.  You will be prompted for a password
%   when establishing the connection.
%
%   r = RemoteClusterAccess(username, P1, V1, ..., Pn, Vn) allows
%   additional parameter-value pairs that modify the behavior of the
%   connection. The accepted parameters are:
%    - 'IdentityFilename' - A string containing the full path to the
%      identity file to use when connecting to a remote host.  If
%      IdentityFilename is not supplied, you will be prompted for a
%      password when establishing the connection.
%    - 'IdentityFileHasPassphrase' - A boolean indicating whether or not
%      the identity file requires a passphrase.  If true, you will be
%      prompted for a password when establishing a connection.  If an
%      identity file is not supplied, this property is ignored.  This value
%      is false by default.
%
%   Examples:
%
%       Mirror files from the remote data location. Assume the job object
%       represents the job on the generic scheduler.
%
%           remoteConnection = parallel.cluster.RemoteClusterAccess('testname');
%           remoteConnection.connect('headnode1','/tmp/filemirror');
%           remoteConnection.startMirrorForJob(job);
%           job.submit();
%           % Wait for the job to finish
%           job.wait();
%           % Ensure that all the local files are up to date and remove the
%           % remote files
%           remoteConnection.doLastMirrorForJob(job);
%           % Get the output arguments for the job
%           results = job.getAllOutputArguments();
%
%   For more information and detailed examples, see the integration scripts
%   provided in matlabroot/toolbox/distcomp/examples/integration. For
%   example, the scripts for PBS in a nonshared file system are in
%   matlabroot/toolbox/distcomp/examples/integration/pbs/nonshared
%
%   parallel.cluster.RemoteClusterAccess methods:
%      RemoteClusterAccess          - Connect to clusters when the cluster's client utilities are not available locally
%      connect                      - Connect to a remote host
%      disconnect                   - Disconnect from a remote host
%      doLastMirrorForJob           - Perform a last mirror for the supplied job
%      getRemoteJobLocation         - Get the remote location for the supplied job ID
%      isJobUsingConnection         - Query the connection to see if the job is being mirrored
%      resumeMirrorForJob           - Resume the mirroring of files for the supplied job
%      runCommand                   - Run the supplied command on the remote host
%      startMirrorForJob            - Start the mirroring of files
%      stopMirrorForJob             - Stop the mirroring of files for the supplied job
%      getConnectedAccess           - Return a connected RemoteClusterAccess instance
%      getConnectedAccessWithMirror - Return a connected RemoteClusterAccess instance
%
%   parallel.cluster.RemoteClusterAccess properties:
%      Hostname                  - Name of the remote host to access
%      IdentityFileHasPassphrase - True if the identity file requires a passphrase
%      IdentityFilename          - Full path to the identity file used when connecting to the remote host
%      IsConnected               - True if there is an active connection to the remote host
%      IsFileMirrorSupported     - True if file mirroring is supported for this connection
%      JobStorageLocation        - Location on the remote host for files that are being mirrored
%      UseIdentityFile           - True if an identity file should be used when connecting to the remote host
%      Username                  - User name for connecting to the remote host
%
%   See also parallel.Job/submit, parallel.Job/fetchOutputs, parcluster,
%            parallel.cluster.Generic.

%  Copyright 2010-2012 The MathWorks, Inc.

    properties (SetAccess = private)
        %HOSTNAME Name of the remote host to access
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect.
        Hostname = '';

        %JobStorageLocation Location on the remote host for files that are being mirrored
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect.
        JobStorageLocation = '';
        % NB We store the remote JobStorageLocation using slashes as provided
        % by the user.

        %IsConnected True if there is an active connection to the remote host
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect,
        %            parallel.cluster.RemoteClusterAccess/disconnect.
        IsConnected = false;

        %IsFileMirrorSupported True if file mirroring is supported for this connection
        %   File mirroring is not supported if no remote JobStorageLocation is supplied
        %   to the connect() method.
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect.
        IsFileMirrorSupported = false;

        %USERNAME User name for connecting to the remote host
        %
        %   See also parallel.cluster.RemoteClusterAccess.
        Username = '';

        %IdentityFilename Full path to the identity file used when connecting to the remote host
        %
        %   See also parallel.cluster.RemoteClusterAccess.UseIdentityFile,
        %            parallel.cluster.RemoteClusterAccess.IdentityFileHasPassphrase,
        %            parallel.cluster.RemoteClusterAccess.
        IdentityFilename = '';

        %UseIdentityFile True if an identity file should be used when connecting to the remote host
        %
        %   See also parallel.cluster.RemoteClusterAccess.IdentityFilename,
        %            parallel.cluster.RemoteClusterAccess.IdentityFileHasPassphrase,
        %            parallel.cluster.RemoteClusterAccess.RemoteClusterAccess.
        UseIdentityFile = false;

        %IdentityFileHasPassphrase True if the identity file requires a passphrase
        %
        %   See also parallel.cluster.RemoteClusterAccess.UseIdentityFile,
        %            parallel.cluster.RemoteClusterAccess.IdentityFilename,
        %            parallel.cluster.RemoteClusterAccess.RemoteClusterAccess.
        IdentityFileHasPassphrase = false;
    end

    properties ( Hidden, SetAccess = private )
        DataLocation
    end
    methods
        function dl = get.DataLocation( obj )
            dl = obj.JobStorageLocation;
        end
    end

    properties (Access = private)
        UserCredentialsParameterMap;
        % The java code requires all remote data locations to use
        % unix slashes.
        UnixSlashJobStorageLocation = '';
        % The (undocumented) PCT QE supplied credential map
        PCTQECredentialMap;
    end

    properties (Constant, GetAccess = private)
        % We never want to mirror the file that contains the JobSchedulerData because
        % the JobSchedulerData may change after the mirror has been started for the job.
        % NB Properties must have the same case as those defined in
        % @distcomp/@filestorage/schema.m
        DoNotMirrorJobProperties = {'jobschedulerdata'};
        % Never delete local files if they are deleted from the remote end
        DoNotDeleteLocalIfRemoteDeleted = false;
        % Prefix to add to all dctSchedulerMessages
        SchedulerMessagePrefix = 'RemoteClusterAccess:';
        WindowsSlash = '\';
        UnixSlash = '/';

        % A map of connected RemoteClusterAccess instances. Populated via the
        % static method 'getConnectedAccess'. When 'disconnect' is called,
        % objects should remove themselves from this map.
        ConnectedMap = containers.Map();
    end

    methods ( Static, Access = private )
        function key = chooseKey( clusterHost, remoteJobStorageLocation, username )
        % Return the key into the ConnectedMap for a given combination of
        % clusterHost, remoteJobStorageLocation and username.
            key = sprintf( '%s|%s|%s', clusterHost, remoteJobStorageLocation, username );
        end

        function obj = getConnectedAccessImpl( clusterHost, remoteJobStorageLocation, username, varargin )
            import parallel.cluster.RemoteClusterAccess

            narginchk(3, Inf);
            validateattributes( clusterHost, {'char'}, {} );
            validateattributes( remoteJobStorageLocation, {'char'}, {} );
            validateattributes( username, {'char'}, {} );
            key = RemoteClusterAccess.chooseKey( clusterHost, remoteJobStorageLocation, username );

            if RemoteClusterAccess.ConnectedMap.isKey( key )
                obj = RemoteClusterAccess.ConnectedMap( key );
                if obj.IsConnected
                    return
                else
                    RemoteClusterAccess.ConnectedMap.remove( key );
                end
            end

            % Not in cache, need to build
            obj = RemoteClusterAccess( username, varargin{:} );
            obj.connect( clusterHost, remoteJobStorageLocation );

            m = RemoteClusterAccess.ConnectedMap;
            m(key) = obj; %#ok<NASGU> Indirect modification of Constant property
        end
    end
    methods ( Static, Access = public )
        function obj = getConnectedAccess( clusterHost, username, varargin )
        %parallel.cluster.RemoteClusterAccess.getConnectedAccess Return a connected RemoteClusterAccess instance
        %   RemoteClusterAccess.getConnectedAccess(host,username) returns a
        %   RemoteClusterAccess instance that is connected to the supplied
        %   host. This function may return a previously constructed
        %   RemoteClusterAccess instance if one exists.
        %
        %   RemoteClusterAccess.getConnectedAccess(...,P1,V1,...PN,VN)
        %   passes the additional parameters to the RemoteClusterAccess
        %   constructor.
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect.
            import parallel.cluster.RemoteClusterAccess
            try
                obj = RemoteClusterAccess.getConnectedAccessImpl( ...
                    clusterHost, '', username, varargin{:} );
            catch E
                throw( E );
            end
        end
        function obj = getConnectedAccessWithMirror( clusterHost, remoteJobStorageLocation, username, varargin )
        %parallel.cluster.RemoteClusterAccess.getConnectedAccessWithMirror Return a connected RemoteClusterAccess instance
        %   RemoteClusterAccess.getConnectedAccessWithMirror(host,location,username)
        %   returns a RemoteClusterAccess instance that is connected to the
        %   supplied host, using the location as the mirror location. This function
        %   may return a previously constructed RemoteClusterAccess instance 
        %   if one exists.
        %
        %   RemoteClusterAccess.getConnectedAccessWithMirror(...,P1,V1,...PN,VN)
        %   passes the additional parameters to the RemoteClusterAccess
        %   constructor.
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect.
            import parallel.cluster.RemoteClusterAccess
            try
                obj = RemoteClusterAccess.getConnectedAccessImpl( ...
                    clusterHost, remoteJobStorageLocation, username, varargin{:} );
            catch E
                throw( E );
            end
        end
    end

    methods
        % -------------------------------------------------------------------------
        % constructor
        % -------------------------------------------------------------------------
        % Undocumented P-V pair for testing purposes
        %   - 'PCTQECredentialMap' - A com.mathworks.toolbox.distcomp.remote.ParameterMap containing
        %     the credentials to use.
        %
        function obj = RemoteClusterAccess(username, varargin)

            narginchk(1, Inf);

            if ~ischar(username)
                error(message('parallel:cluster:RemoteClusterAccessArgumentNotString', 'Username'));
            end
            obj.Username = username;

            % Convert the args to a parseable format
            [allProps, allValues] = parallel.internal.convertToPVArrays(varargin{:});
            allowedProps = {'IdentityFilename', 'IdentityFileHasPassphrase'};
            % Check that each property is unique amongst the allowed parameters
            for i = 1:numel(allProps)
                thisProp  = allProps{i};
                thisValue = allValues{i};

                % To guard against users accidentally stumbling on PCTQECredentialMap,
                % this property name must be matched EXACTLY and is case-sensitive
                if strcmp(thisProp, 'PCTQECredentialMap')
                    if ~isa(thisValue, 'com.mathworks.toolbox.distcomp.remote.ParameterMap')
                        error(message('parallel:cluster:RemoteClusterAccessInvalidCredentialMap'));
                    end
                    obj.PCTQECredentialMap = thisValue;
                    continue;
                end

                % Find this property name in each of the sets of properties
                indexInProps  = find(strncmpi(thisProp, allowedProps, numel(thisProp)));
                if isempty(indexInProps)
                    error(message('parallel:cluster:RemoteClusterAccessInvalidInput', thisProp));
                elseif numel(indexInProps) > 1
                    error(message('parallel:cluster:RemoteClusterAccessAmbiguousInput', thisProp));
                end
                % We know that only one property was matched by thisProp and
                % thus that one of the indexInProps holds a single value
                switch indexInProps
                    case 1 % IdentityFilename
                        if ~ischar(thisValue)
                            error(message('parallel:cluster:RemoteClusterAccessArgumentNotString', 'IdentityFilename'));
                        end
                        % Check that this is a valid file.  Note that exist(..., 'file') checks
                        % both files and directories, so we explicitly ensure that the file
                        % is a file and not a dir.
                        fileExists = exist(thisValue, 'file');
                        if fileExists == 0
                            error(message('parallel:cluster:RemoteClusterAccessNonExistentIdentityFile', thisValue));
                        end
                        if fileExists == 7
                            error(message('parallel:cluster:RemoteClusterAccessIdentityFileDir', thisValue));
                        end
                        obj.UseIdentityFile = true;
                        obj.IdentityFilename = thisValue;
                    case 2 % IdentityFileHasPassphrase
                        if ~islogical(thisValue)
                            error(message('parallel:cluster:RemoteClusterAccessArgumentNotLogical', 'FileHasPassphrase'));
                        end
                        obj.IdentityFileHasPassphrase = thisValue;
                    otherwise
                        error(message('parallel:cluster:RemoteClusterAccessInternalError'));
                end
            end
            % Error if we were given both an Identity file and Parameter Map
            if ~isempty(obj.PCTQECredentialMap) && obj.UseIdentityFile
                error(message('parallel:cluster:RemoteClusterAccessIdentityFileAndCredentialMapSupplied'));
            end
        end

        % -------------------------------------------------------------------------
        % connect
        % -------------------------------------------------------------------------
        function connect(obj, clusterHost, remoteJobStorageLocation)
        %CONNECT Connect to a remote host
        %   connect(remoteClusterAccess, clusterHost) establishes a connection to the
        %   specified host using the user credential options supplied in the constructor.
        %   File Mirroring is not supported.
        %
        %   connect(remoteClusterAccess, clusterHost, remoteJobStorageLocation) establishes a
        %   connection to the specified host using the user credential options supplied
        %   in the constructor.  remoteJobStorageLocation identifies a folder on the clusterHost
        %   that is used for file mirroring.  The user credentials supplied in the
        %   constructor must have write access to this folder.
        %
        %   See also parallel.cluster.RemoteClusterAccess/disconnect,
        %            parallel.cluster.RemoteClusterAccess/startMirrorForJob,
        %            parallel.cluster.RemoteClusterAccess/runCommand.

            narginchk(2, 3);
            if obj.IsConnected
                error(message('parallel:cluster:RemoteClusterAccessAlreadyConnected', clusterHost, obj.Hostname));
            end

            % remoteJobStorageLocation is an optional input arg
            if nargin < 3
                remoteJobStorageLocation = '';
            end

            userCredentials = obj.getOrCreateCredentialsParameterMap(clusterHost);
            clusterAccess = obj.getClusterAccess;
            try
                clusterAccess.connect(clusterHost, userCredentials);
            catch err
                ex = MException(message('parallel:cluster:RemoteClusterAccessFailedToConnect', clusterHost));
                ex = ex.addCause(err);
                throw(ex);
            end

            % Set the data once we know we are connected
            obj.Hostname = clusterHost;
            obj.JobStorageLocation = remoteJobStorageLocation;
            obj.UserCredentialsParameterMap = userCredentials;
            obj.IsConnected = true;
            obj.IsFileMirrorSupported = ~isempty(remoteJobStorageLocation);
            % Convert remoteJobStorageLocation to unix filesep
            obj.UnixSlashJobStorageLocation = strrep(remoteJobStorageLocation, obj.WindowsSlash, obj.UnixSlash);
        end

        % -------------------------------------------------------------------------
        % disconnect
        % -------------------------------------------------------------------------
        function disconnect(obj)
        % DISCONNECT Disconnect from a remote host
        %
        %   RemoteClusterAccess.disconnect() disconnects the existing remote connection.
        %   The CONNECT method must have already been called.
        %
        %   See also CONNECT

            obj.errorIfNotConnected();

            % Remove ourselves from the cache - if we were added there.
            key = obj.chooseKey( obj.Hostname, obj.JobStorageLocation, obj.Username );
            map = parallel.cluster.RemoteClusterAccess.ConnectedMap;
            if map.isKey( key )
                map.remove( key );
            end

            % NB disconnect does not throw any errors
            clusterAccess = obj.getClusterAccess;
            clusterAccess.disconnect(obj.Hostname, obj.UserCredentialsParameterMap);

            obj.Hostname = '';
            obj.JobStorageLocation = '';
            obj.UnixSlashJobStorageLocation = '';
            obj.IsConnected = false;
            obj.IsFileMirrorSupported = false;
            obj.UserCredentialsParameterMap = [];
        end

        % -------------------------------------------------------------------------
        % startMirrorForJob
        % -------------------------------------------------------------------------
        function startMirrorForJob(obj, job)
        %startMirrorForJob Start the mirroring of files
        %   startMirrorForJob(remoteClusterAccess, job) copies all the job files
        %   from the local JobStorageLocation to the remote JobStorageLocation,
        %   and starts mirroring files so that any changes to the files in the
        %   remote JobStorageLocation are copied back to the local
        %   JobStorageLocation.  The CONNECT method must have already been
        %   called.
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect,
        %            parallel.cluster.RemoteClusterAccess/doLastMirrorForJob.

            narginchk(2, 2);
            obj.errorIfNotConnected();
            obj.errorIfMirroringNotSupported('parallel:cluster:RemoteClusterAccessCannotStartMirroringNotSupported');
            obj.errorIfAlreadyBeingMirrored('parallel:cluster:RemoteClusterAccessCannotStartJobAlreadyMirrored', job.ID);

            % Use a 1s poll interval for file copying chores
            pollInterval = 1;
            clusterAccess = obj.getClusterAccess;
            mirrorFilesInfo = obj.createMirrorFilesInfo(job);
            try
                sendAndMirrorChore = clusterAccess.sendAndMirrorJobFiles(...
                    obj.Hostname, obj.UserCredentialsParameterMap, mirrorFilesInfo);
                obj.waitForChoreToFinishOrError(sendAndMirrorChore, pollInterval);
            catch err
                % If anything went wrong, try to remove the remote files
                dctSchedulerMessage(6, '%s Removing files for job %d because of error when starting mirror', ...
                    obj.SchedulerMessagePrefix, job.ID);
                try
                    removeFilesChore = clusterAccess.removeFilesForJob(obj.Hostname, ...
                        obj.UserCredentialsParameterMap, mirrorFilesInfo);
                    obj.waitForChoreToFinishOrError(removeFilesChore);
                catch removeErr
                    warning(message('parallel:cluster:RemoteClusterAccessFailedToRemovePossiblyDelete', job.ID, obj.JobStorageLocation, removeErr.getReport));
                end
                ex = MException(message('parallel:cluster:RemoteClusterAccessStartMirrorFailed', job.ID));
                ex = ex.addCause(err);
                throw(ex);
            end
        end

        % -------------------------------------------------------------------------
        % stopMirrorForJob
        % -------------------------------------------------------------------------
        % This will cancel any running mirrors and remove the remote files.
        % This provides an abrupt termination of the mirroring process: no
        % attempt is made to ensure that the client has up-to-date files.
        function stopMirrorForJob(obj, job)
        %stopMirrorForJob Stop the mirroring of files for the supplied job
        %   stopMirrorForJob(remoteClusterAccess, job) immediately stops the
        %   mirroring of files from the remote JobStorageLocation to the local
        %   JobStorageLocation for the supplied job.  The startMirrorForJob or
        %   resumeMirrorForJob method must have already been called.  This
        %   cancels the running mirror and removes the files for the job from
        %   the remote location.  This is similar to doLastMirrorForJob except
        %   that stopMirrorForJob makes no attempt to ensure that the local job
        %   files are up-to-date.  For normal mirror stoppage, use
        %   doLastMirrorForJob.
        %
        %   See also parallel.cluster.RemoteClusterAccess/startMirrorForJob,
        %            parallel.cluster.RemoteClusterAccess/resumeMirrorForJob,
        %            parallel.cluster.RemoteClusterAccess/doLastMirrorForJob.

            narginchk(2, 2);
            obj.errorIfNotConnected();
            obj.errorIfMirroringNotSupported('parallel:cluster:RemoteClusterAccessCannotStopMirroringNotSupported');
            obj.errorIfNotBeingMirrored('parallel:cluster:RemoteClusterAccessCannotStopJobNotMirrored', job.ID);

            clusterAccess = obj.getClusterAccess;
            % Ensure we cancel the mirroring first before removing the
            % remote files
            dctSchedulerMessage(6, '%s Cancelling mirror for job %d.', ...
                obj.SchedulerMessagePrefix, job.ID);
            errorToThrow = [];
            try
                clusterAccess.cancelMirrorForJob(obj.Hostname, ...
                    obj.UserCredentialsParameterMap, job.ID);
            catch err
                % Save up the error for later because we need to remove the
                % remote files
                errorToThrow = MException(message('parallel:cluster:RemoteClusterAccessFailedToCancelMirror', job.ID));
                errorToThrow = errorToThrow.addCause(err);
            end

            dctSchedulerMessage(6, '%s Removing remote files for job %d.', ...
                obj.SchedulerMessagePrefix, job.ID);
            try
                obj.removeRemoteFiles(job);
            catch removeErr
                % Just warn if we couldn't remove the remote files
                warning(message('parallel:cluster:RemoteClusterAccessFailedToRemovePossiblyDelete', job.ID, obj.JobStorageLocation, removeErr.getReport));
            end

            % The mirror has been cancelled and the remote files removed
            % so the job is no longer using the connection

            if ~isempty(errorToThrow)
                throw(errorToThrow);
            end
        end

        % -------------------------------------------------------------------------
        % resumeMirrorForJob
        % -------------------------------------------------------------------------
        function resumeMirrorForJob(obj, job)
        %resumeMirrorForJob Resume the mirroring of files for the supplied job
        %   resumeMirrorForJob(remoteClusterAccess, job) resumes the mirroring
        %   of files from the remote JobStorageLocation to the local
        %   JobStorageLocation for the supplied job.  This is similar to the
        %   startMirrorForJob method but does not first copy the files from the
        %   local JobStorageLocation to the remote JobStorageLocation.  The
        %   CONNECT method must have already been called.  This is useful if the
        %   original client MATLAB session has ended, and you are accessing the
        %   same files from a new client session.
        %
        %   See also parallel.cluster.RemoteClusterAccess/connect,
        %            parallel.cluster.RemoteClusterAccess/startMirrorForJob,
        %            parallel.cluster.RemoteClusterAccess/doLastMirrorForJob.

            narginchk(2, 2);
            obj.errorIfNotConnected();
            obj.errorIfMirroringNotSupported('parallel:cluster:RemoteClusterAccessCannotResumeMirroringNotSupported');
            obj.errorIfAlreadyBeingMirrored('parallel:cluster:RemoteClusterAccessCannotResumeJobAlreadyMirrored', job.ID);

            clusterAccess = obj.getClusterAccess;
            mirrorFilesInfo = obj.createMirrorFilesInfo(job);
            try
                mirrorFilesChore = clusterAccess.resumeMirrorForJob(obj.Hostname, ...
                    obj.UserCredentialsParameterMap, mirrorFilesInfo);
                obj.waitForChoreToFinishOrError(mirrorFilesChore);
            catch err
                % Don't attempt any cleanup at the remote end if resume errored.  We should
                % preserve the files on the remote end until user asks us to stop the mirror.
                ex = MException(message('parallel:cluster:RemoteClusterAccessResumeMirrorFailed', job.ID));
                ex = ex.addCause(err);
                throw(ex);
            end
        end

        % -------------------------------------------------------------------------
        % doLastMirrorForJob
        % -------------------------------------------------------------------------
        % This will do a complete mirror for the job and stop any running mirrors
        % and then remove the remote files.  This provides a graceful
        % termination of the mirroring process: we only remove the remote
        % files if we could successfully do the last mirror.
        function doLastMirrorForJob(obj, job)
        %doLastMirrorForJob Perform a last mirror for the supplied job
        %   doLastMirrorForJob(remoteClusterAccess, job) performs a final copy
        %   of changed files from the remote JobStorageLocation to the local
        %   JobStorageLocation for the supplied job.  Any running mirrors for
        %   the job also stop and the job files are removed from the remote
        %   JobStorageLocation.  The startMirrorForJob or resumeMirrorForJob
        %   method must have already been called.
        %
        %   See also parallel.cluster.RemoteClusterAccess/startMirrorForJob,
        %            parallel.cluster.RemoteClusterAccess/resumeMirrorForJob,
        %            parallel.cluster.RemoteClusterAccess/stopMirrorForJob.

            narginchk(2, 2);
            obj.errorIfNotConnected();
            obj.errorIfMirroringNotSupported('parallel:cluster:RemoteClusterAccessCannotDoLastMirroringNotSupported');

            % It is OK to do the last mirror even if the job is not currently being mirrored
            lastMirrorDelay = 5*60*1000; %milliseconds to keep mirroring files.
            clusterAccess = obj.getClusterAccess;
            mirrorFilesInfo = obj.createMirrorFilesInfo(job);
            pollInterval = 1;

            try
                mirrorFilesChore = clusterAccess.stopMirroringJobGracefully(obj.Hostname, ...
                    obj.UserCredentialsParameterMap, mirrorFilesInfo, lastMirrorDelay);
                obj.waitForChoreToFinishOrError(mirrorFilesChore, pollInterval);
            catch err
                ex = MException(message('parallel:cluster:RemoteClusterAccessDoLastMirrorFailed', job.ID));
                ex = ex.addCause(err);
                throw(ex);
            end

        end

        % -------------------------------------------------------------------------
        % isJobUsingConnection
        % -------------------------------------------------------------------------
        % Do we think the job is being mirrored and is therefore using the connection?
        % NB this always returns false if file mirroring has not been configured.
        function foundJob = isJobUsingConnection(obj, jobID)
        % isJobUsingConnection Query the connection to see if the job is being mirrored
        %
        %   RemoteClusterAccess.isJobUsingConnection(jobID) returns true if the job's
        %   files are currently being mirrored.
        %
        %   See also startMirrorForJob, doLastMirrorForJob

            narginchk(2, 2);
            obj.errorIfNotConnected();

            clusterAccess = obj.getClusterAccess;

            foundJob = obj.IsFileMirrorSupported ...
                            && clusterAccess.isJobBeingMirrored(obj.Hostname, ...
                                                                obj.UserCredentialsParameterMap, ...
                                                                jobID);
        end

        % -------------------------------------------------------------------------
        % getRemoteJobLocation
        % -------------------------------------------------------------------------
        function remoteJobDirectory = getRemoteJobLocation(obj, jobID, remoteOS)
        % getRemoteJobLocation Get the remote location for the supplied job ID
        %   RemoteClusterAccess.getRemoteJobLocation(jobID, remoteOS) returns the
        %   full path to the remote job location for the supplied jobID.  Valid
        %   values of remoteOS are 'windows', 'pc' and 'unix'.

            narginchk(2, 3);
            obj.errorIfNotConnected();
            obj.errorIfMirroringNotSupported('parallel:cluster:RemoteClusterAccessCannotGetRemoteLocationMirroringNotSupported');
            allowedOS = {'unix', 'pc', 'windows'};
            if ~any(strcmpi(remoteOS, allowedOS))
                error(message('parallel:cluster:RemoteClusterAccessIncorrectRemoteOS', sprintf( '%s ', allowedOS{ : } )));
            end

            if strcmpi(remoteOS, 'unix')
                remoteFileSeparator = obj.UnixSlash;
            else
                remoteFileSeparator = obj.WindowsSlash;
            end

            % Build up the remote directory using the remote OS's slashes
            % We assume that the user provided the correctly slashed
            % JobStorageLocation from their cluster OS type in the first place.
            remoteJobDirectory = sprintf('%s%sJob%d', obj.JobStorageLocation, remoteFileSeparator, jobID);
        end

        % -------------------------------------------------------------------------
        % runCommand
        % -------------------------------------------------------------------------
        function [cmdStatus, cmdOut] = runCommand(obj, commandToRun)
        % runCommand Run the supplied command on the remote host
        %
        %   [status, result] = RemoteClusterAccess.runCommand(command) runs the
        %   supplied command on the remote host and returns the resulting status
        %   and standard output.  The CONNECT method must have already been called.
        %
        %   See also CONNECT

            narginchk(2, 2);
            obj.errorIfNotConnected();

            clusterAccess = obj.getClusterAccess;
            try
                executeCommandChore = clusterAccess.executeCommand(obj.Hostname, ...
                    obj.UserCredentialsParameterMap, commandToRun);
                obj.waitForChoreToFinishOrError(executeCommandChore);
                cmdStatus = double(executeCommandChore.getExitStatus());

                % Check if stdout/stderr are reliable and warn if not.  Note that
                % we will still proceed to get the stdout and stderr because they
                % may still contain something interesting.
                if ~executeCommandChore.isStdoutReliable
                    warning(message('parallel:cluster:RemoteClusterAccessStdOutUnreliable', commandToRun));
                end
                if ~executeCommandChore.isStderrReliable
                    warning(message('parallel:cluster:RemoteClusterAccessStdErrUnreliable', commandToRun));
                end
                % NB getStdOut and getStdErr come out as columns, so
                % transpose them as well as converting to char
                cmdOut = char(executeCommandChore.getStdOut())';
                cmdErr = char(executeCommandChore.getStdErr())';
                if ~isempty(cmdErr)
                    cmdOut = sprintf('%s\n%s', cmdOut, cmdErr);
                end
            catch err
                ex = MException(message('parallel:cluster:RemoteClusterAccessFailedToRunCommand', commandToRun, obj.Hostname));
                ex = ex.addCause(err);
                throw(ex);
            end
        end
    end

    methods (Access = private)
        % -------------------------------------------------------------------------
        % errorIfNotConnected
        % -------------------------------------------------------------------------
        function errorIfNotConnected(obj)
            if ~obj.IsConnected
                ex = MException(message('parallel:cluster:RemoteClusterAccessNotConnected'));
                throwAsCaller(ex);
            end
        end

        % -------------------------------------------------------------------------
        % errorIfMirroringNotSupported
        % -------------------------------------------------------------------------
        function errorIfMirroringNotSupported(obj, errID)
            if ~obj.IsFileMirrorSupported
                ex = MException(message(errID));
                throwAsCaller(ex);
            end
        end

        % -------------------------------------------------------------------------
        % errorIfAlreadyBeingMirrored
        % -------------------------------------------------------------------------
        function errorIfAlreadyBeingMirrored(obj, errID, jobID)
            if obj.isJobUsingConnection(jobID)
                ex = MException(message(errID, jobID));
                throwAsCaller(ex);
            end
        end

        % -------------------------------------------------------------------------
        % errorIfNotBeingMirrored
        % -------------------------------------------------------------------------
        function errorIfNotBeingMirrored(obj, errID, jobID)
            if ~obj.isJobUsingConnection(jobID)
                ex = MException(message(errID, jobID));
                throwAsCaller(ex);
            end
        end

        % -------------------------------------------------------------------------
        % removeRemoteFiles
        % -------------------------------------------------------------------------
        function removeRemoteFiles(obj, job)
            clusterAccess = obj.getClusterAccess;
            mirrorFilesInfo = obj.createMirrorFilesInfo(job);

            try
                removeFilesChore = clusterAccess.removeFilesForJob(obj.Hostname, ...
                    obj.UserCredentialsParameterMap, mirrorFilesInfo);
                obj.waitForChoreToFinishOrError(removeFilesChore);
            catch err
                ex = MException(message('parallel:cluster:RemoteClusterAccessFailedToRemoveRemoteFiles', job.ID));
                ex = ex.addCause(err);
                throw(ex);
            end
        end

        % -------------------------------------------------------------------------
        % getOrCreateCredentialsParameterMap
        % -------------------------------------------------------------------------
        % Prompts the user for their username and password and converts them into a
        % com.mathworks.toolbox.distcomp.clusteraccess.ParameterMap
        function credentials = getOrCreateCredentialsParameterMap(obj, hostname)
            if ~isempty(obj.PCTQECredentialMap)
                credentials = obj.PCTQECredentialMap;
                return;
            end

            import com.mathworks.toolbox.distcomp.clusteraccess.*;
            import com.mathworks.toolbox.distcomp.remote.*;
            import com.mathworks.toolbox.distcomp.remote.spi.plugin.SshParameter;

            dctSchedulerMessage(6, '%s Using username %s to connect to %s', ...
                obj.SchedulerMessagePrefix, obj.Username, hostname);

            % Do an explicit conversion of the username to a java string because
            % something goes wrong with the char->string conversion when the MATLAB
            % char has only 1 character.
            javaUsername = java.lang.String(obj.Username);

            credentials = ParameterMap();
            credentials.put(SshParameter.STRICT_HOST_KEY_CHECKING, ...
                SshParameter.STRICT_HOST_KEY_CHECKING.getSuggestedValue());
            credentialParameters = ParameterMap();

            if obj.UseIdentityFile
                keyFile = java.io.File(obj.IdentityFilename);
                credentialParameters.put(IdentityFileCredentialDescription.USERNAME, javaUsername);
                credentialParameters.put(IdentityFileCredentialDescription.IDENTITY_FILE, keyFile);

                if obj.IdentityFileHasPassphrase
                    passphraseMsg = getString(message('parallel:cluster:EnterPassphraseForIdentityFile', obj.IdentityFilename));
                    keyfilePassphrase = obj.solicitPassword(passphraseMsg);
                    credentialParameters.put(IdentityFileCredentialDescription.PASSPHRASE, keyfilePassphrase);
                end

                credentials.put(SshParameter.SSH_CREDENTIAL, ...
                    IdentityFileCredentialDescription.INSTANCE.create(credentialParameters));
            else
                passwordMsg = getString(message('parallel:cluster:EnterPasswordForUserOnMachine', obj.Username, hostname));
                password = obj.solicitPassword(passwordMsg);

                credentialParameters.put(PasswordCredentialDescription.USERNAME, javaUsername);
                credentialParameters.put(PasswordCredentialDescription.PASSWORD, password);
                credentials.put(SshParameter.SSH_CREDENTIAL, ...
                    PasswordCredentialDescription.INSTANCE.create(credentialParameters));
            end
        end

        % -------------------------------------------------------------------------
        % createMirrorFilesInfo
        % -------------------------------------------------------------------------
        % Creates a com.mathworks.toolbox.distcomp.clusteraccess.MirrorFilesInfo
        % for a particular job.
        function filesInfo = createMirrorFilesInfo(obj, job)

            import parallel.cluster.RemoteClusterAccess

            sched        = job.Parent;
            jobFilesInfo = sched.pGetJobFilesInfo( job );
            stateFiles   = jobFilesInfo.JobStateFiles;
            dataFiles    = [jobFilesInfo.JobInputFiles; ...
                            jobFilesInfo.JobOutputFiles; ...
                            jobFilesInfo.ClusterConfigFiles; ...
                            jobFilesInfo.TaskFiles];

            readOnlyFiles = RemoteClusterAccess.getReadOnlyFiles( ...
                jobFilesInfo.RootDirectory, [stateFiles; dataFiles] );

            excludeFromMirrorFiles = [ jobFilesInfo.JobInputFiles ; ...
                                jobFilesInfo.ClusterConfigFiles ; ...
                                readOnlyFiles ];

            doNotDeleteRemoteFiles = jobFilesInfo.ClusterConfigFiles;

            dctSchedulerMessage(6, '%s State files to upload for job %d:\n%s', ...
                obj.SchedulerMessagePrefix, job.ID, sprintf('\t%s\n', stateFiles{:}));
            dctSchedulerMessage(6, '%s Data files to upload for job %d:\n%s', ...
                obj.SchedulerMessagePrefix, job.ID, sprintf('\t%s\n', dataFiles{:}));
            dctSchedulerMessage(6, '%s Files to exclude from mirror for job %d:\n%s', ...
                obj.SchedulerMessagePrefix, job.ID, sprintf('\t%s\n', excludeFromMirrorFiles{:}));
            dctSchedulerMessage(6, '%s Files that will not be removed for job %d:\n%s', ...
                obj.SchedulerMessagePrefix, job.ID, sprintf('\t%s\n', doNotDeleteRemoteFiles{:}));

            stateFileSet = iCellStringToSetString(stateFiles);
            dataFileSet = iCellStringToSetString(dataFiles);
            excludeFromMirrorSet = iCellStringToSetString(excludeFromMirrorFiles);
            excludeFromUploadSet = iCellStringToSetString({});
            excludeFromRemoveSet = iCellStringToSetString(doNotDeleteRemoteFiles);

            localJobStorageLocation = jobFilesInfo.RootDirectory;

            % if the remote data location does not exist, create it.
            makeRemoteDataLocationOnFirstUpload = true;
            
            filesInfo = com.mathworks.toolbox.distcomp.clusteraccess.MirrorFilesInfo(job.ID, ...
                java.io.File(localJobStorageLocation), ...
                obj.UnixSlashJobStorageLocation, ...
                makeRemoteDataLocationOnFirstUpload, ...
                dataFileSet, ...
                stateFileSet, ...
                excludeFromUploadSet, ...
                excludeFromMirrorSet, ...
                excludeFromRemoveSet, ...
                obj.DoNotDeleteLocalIfRemoteDeleted, ...
                obj.DoNotDeleteLocalIfRemoteDeleted);
        end
    end


    methods (Static, Access = private)
        % -------------------------------------------------------------------------
        % getClusterAccess
        % -------------------------------------------------------------------------
        function clusteraccess = getClusterAccess()
            clusteraccess = com.mathworks.toolbox.distcomp.clusteraccess.ClusterAccess.INSTANCE;
        end

        % -------------------------------------------------------------------------
        % getReadOnlyFiles
        % -------------------------------------------------------------------------
        % Returns the readOnlyFiles relative the dirPrefix.
        function readOnlyFiles = getReadOnlyFiles(dirPrefix, filenames)
            readOnlyFiles = {};
            for ii = 1:numel(filenames)
                currFile = filenames{ii};
                currFullFile = fullfile(dirPrefix, currFile);
                if isdir(currFullFile)
                    listing = dir(currFullFile);
                    % Remove '.' and '..' from the listing
                    filesInDir = {listing.name};
                    filesInDir = filesInDir(~strcmp('.', filesInDir));
                    filesInDir = filesInDir(~strcmp('..', filesInDir));
                    % And recursively call this method to get the read only files
                    readOnlyInDir = parallel.cluster.RemoteClusterAccess.getReadOnlyFiles(currFullFile, filesInDir);
                    % Make sure we put the current relative dir back onto the list of files
                    if ~isempty(readOnlyInDir)
                        readOnlyFiles = [readOnlyFiles; strcat(currFile, filesep, readOnlyInDir)]; %#ok<AGROW>
                    end
                else
                    [~, attribs] = fileattrib(currFullFile);
                    if isstruct(attribs) && ~attribs.UserWrite
                        readOnlyFiles = [readOnlyFiles; currFile];  %#ok<AGROW>
                    end
                end
            end
        end

        % -------------------------------------------------------------------------
        % getDoNotDeleteRemoteFiles
        % -------------------------------------------------------------------------
        % Never delete the metadata file because it is required for other jobs
        function doNotDeleteRemoteFiles = getDoNotDeleteRemoteFiles(job)
            scheduler = job.Parent;
            storage = scheduler.pReturnStorage;
            doNotDeleteRemoteFiles = {storage.MetadataFilename};
        end

        % -------------------------------------------------------------------------
        % waitForChoreToFinishOrError
        % -------------------------------------------------------------------------
        function waitForChoreToFinishOrError(chore, pollInterval)
            validateattributes(chore, {'com.mathworks.toolbox.distcomp.clusteraccess.RemoteMachineChore'}, {'scalar'});
            
            % use a default poll interval of 0.1s
            if nargin < 2
                pollInterval = 0.1;
            end
            
            choreCleanup = onCleanup(@() iCancelChoreIfNotFinished(chore));
            
            % This function only returns normally when the chore has completed
            % with no problems.
            % TODO - should save up errors until chore has actually ended?
            while true
                err = chore.getProblems;
                if ~isempty(err) && err.size > 0
                    ex = MException(message('parallel:cluster:RemoteClusterAccessErrorDuringChore', class(chore)));
                    for ii = 0:err.size-1
                        ex = ex.addCause(err.get(ii));
                    end
                    throw(ex);
                end
                if chore.hasEnded()
                    break;
                end
                pause(pollInterval);
            end
        end
        
% -------------------------------------------------------------------------
% solicitPassword
        % -------------------------------------------------------------------------
        % Prompts a user for a password.
        function password = solicitPassword(promptMsg)

            if com.mathworks.jmi.Support.useSwing()
                passwordField = javaObjectEDT('javax.swing.JPasswordField',40);
                result = javaMethodEDT('showConfirmDialog','javax.swing.JOptionPane', ...
                    [], passwordField, promptMsg, ...
                    javax.swing.JOptionPane.OK_CANCEL_OPTION, ...
                    javax.swing.JOptionPane.PLAIN_MESSAGE);
                if result == javax.swing.JOptionPane.OK_OPTION
                    passwordString =  java.lang.String(passwordField.getPassword());
                else
                    error(message('parallel:cluster:RemoteClusterAccessNoPassword'));
                end
            else
                passwordChars = parallel.internal.readPassword(promptMsg);

                %if passwordChars is a java.lang.Exception then error
                if isa(passwordChars, 'java.lang.Exception')
                    msg = char(passwordChars.getMessage());
                    error(message('parallel:cluster:RemoteClusterAccessProblemGettingPassword', msg));
                end
                passwordString = java.lang.String(passwordChars);
            end

            password = com.mathworks.toolbox.distcomp.remote.Password(passwordString);
        end
    end
end

% -------------------------------------------------------------------------
% iCellStringToSetString
% -------------------------------------------------------------------------
% Convert a MATLAB cell array of strings into a Set of java Strings
function setOfStrings = iCellStringToSetString(cellOfStrings)
    setOfStrings = java.util.LinkedHashSet(numel(cellOfStrings));
    for ii = 1:numel(cellOfStrings)
        setOfStrings.add(cellOfStrings{ii});
    end
end

% -------------------------------------------------------------------------
% iCancelChoreIfNotFinished
% -------------------------------------------------------------------------
function iCancelChoreIfNotFinished(chore)
    if ~chore.hasEnded()
        chore.cancel();
    end
end
