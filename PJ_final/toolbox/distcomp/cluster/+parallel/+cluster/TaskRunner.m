% TaskRunner - simple parallel.cluster.CJSCluster subclass to support job execution.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true, Sealed ) TaskRunner < parallel.cluster.CJSCluster
    properties ( SetAccess = immutable )
        Host
    end

    methods ( Hidden )
        function hDestroyJob( ~, ~ )
        end
        function hDestroyTask( ~, ~ )
        end
        function ok = hCancelJob( ~, ~ )
            warning(message('parallel:cluster:TaskRunnerCancellation'));
            ok = false;
        end
        function ok = hCancelTask( ~, ~ )
            warning(message('parallel:cluster:TaskRunnerCancellation'));
            ok = false;
        end
        function hSubmitIndependentJob( varargin )
            error(message('parallel:cluster:TaskRunnerSubmission'));
        end
        function hSubmitCommunicatingJob( varargin )
            error(message('parallel:cluster:TaskRunnerSubmission'));
        end
        function hSetProperty( obj, names, values )
            if ~iscell( names )
                names = { names };
                values = { values };
            end
            for ii = 1:length( names )
                if isequal( names{ii}, 'OperatingSystem' )
                    throwAsCaller( obj.buildCannotModify( 'OperatingSystem' ) );
                end
                obj.hSetProperty@parallel.cluster.CJSCluster(names{ii}, values{ii});
            end
        end
        % Getting a memento to restore a cluster for display is something 
        % that all clusters need to implement. Since this runner inherits 
        % from CJSCluster, it must have an implementation.  
        function data = hGetMementoData( obj ) 
            data.JobStorageLocation = obj.JobStorageLocation;
        end
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            clusterPropertyMap = hGetDisplayItems@parallel.Cluster(obj, diFactory);
            propNames = {'Host', 'JobStorageLocation'};            
        end
    end

    methods
        function obj = TaskRunner( varargin )
        % Expect only args to be 'JobStorageLocation', <dataLoc>
            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.cluster.CJSCluster

            try
                [propsFromArgs, cProf] = ...
                    ConstructorArgsHelper.interpretClusterCtorArgs( ...
                        ?parallel.cluster.TaskRunner, varargin{:} );
            catch E
                throw( E );
            end

            [dataLoc, dataLocValType] = CJSCluster.chooseDataLocation( propsFromArgs );

            obj@parallel.cluster.CJSCluster( 'TaskRunner', cProf, dataLoc, dataLocValType );
            obj.Host = char( java.net.InetAddress.getLocalHost.getCanonicalHostName() );
        end
    end
    methods ( Hidden, Access = protected )
        function jobState = getJobState( ~, ~, jobState )
        end
        function tf = wasSubmittedByThisType( ~, ~ )
            tf = false;
        end
        function [names, values] = getClusterSpecificEnvVars( ~ )
            names  = {};
            values = {};
        end
    end
    
    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
end
%#ok<*ATUNK>
