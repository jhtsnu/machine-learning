function didCancel = cancelOrDestroyTask( obj, task, fcnH )
% cancelOrDestroyJob for parallel.cluster.Local
% - obj: the Local scheduler
% - task: the (scalar) job object
% - fcnH: a function handle to invoke on the "command"
%
% Algorithm:
% didCancel = false
% taskNeedsCancelling = ( pending < task.State < finished )
% if ( ~my task ) && taskNeedsCancelling
%     warn "not my task"
%     return
% end
% fcnH( command corresponding to taskUUID )
% didCancel = true

% Copyright 2011 The MathWorks, Inc.

import parallel.internal.types.States;
import parallel.cluster.Local;

job                     = task.Parent;
didCancel               = false;
[~, correctClient, jobData] = ...
    Local.getJobSchedDataIfAvailable( job );
taskState               = task.StateEnum;
taskNeedsCancelling     = taskState > States.Pending && taskState < States.Finished;

if taskNeedsCancelling
    if ~correctClient
        warning(message('parallel:cluster:LocalCannotCancelOrDestroyTask'));
        return
    end
    try
        didCancel = iCancelOrDestroyOneTask( obj.LocalSched, jobData, task.Id, fcnH );
    catch E
        warning(message('parallel:cluster:LocalFailedToCancelOrDestroyTask', E.message));
    end
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function didCancel = iCancelOrDestroyOneTask( localSched, jobData, taskID, fcnH )

didCancel = false;
IDindex   = find( jobData.taskIDs == taskID, 1, 'first' );
taskUUID  = jobData.taskUUIDs{IDindex};

if isempty( taskUUID )
    warning(message('parallel:cluster:LocalFailedToFindTaskToCancel'));
else
    command = localSched.getCommand( taskUUID );
    if ~isempty( command )
        fcnH( command );
    end
    % Only set didCancel true here after we're sure we succeeded.
    didCancel = true;
end
end
