function state = getIndependentJobState( obj, job, jobData )
% getIndependentJobState - look at task states and compute the job state.

% Copyright 2011 The MathWorks, Inc.

import parallel.internal.types.States

numQueued   = 0;
numRunning  = 0;
numFinished = 0;
numFailed   = 0;

tasks = job.Tasks;

for ii = 1:numel( tasks )
    thisTask = tasks(ii);
    taskState = iGetStateForTask( obj, jobData, thisTask );

    switch taskState
      case States.Queued
        numQueued      = numQueued   + 1;
      case States.Running
        numRunning     = numRunning  + 1;
      case States.Finished
        numFinished    = numFinished + 1;
      case States.Failed
        numFailed      = numFailed   + 1;
      otherwise
        warning(message('parallel:cluster:LocalUnexpectedTaskState', char( taskState )));
    end
end

if numQueued > 0 && ~any( [ numRunning, numFinished, numFailed ] )
    state = States.Queued;
elseif any( [numQueued, numRunning] )
    state = States.Running;
elseif numFailed == 0
    state = States.Finished;
else
    state = States.Failed;
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function taskState = iGetStateForTask( cluster, jobData, thisTask )

import parallel.internal.types.States

idIndex  = find( jobData.taskIDs == thisTask.Id, 1, 'first' );
taskUUID = jobData.taskUUIDs{idIndex};

% gotState should always be true.
gotState = ~isempty( taskUUID );
if ~gotState
    % This test should never succeed but if it does we should just use
    % the value on disk
    taskState = thisTask.hGetProperty( 'StateEnum' );
    return
else
    command   = cluster.LocalSched.getCommand( taskUUID );
    if isempty( command )
        % Default to Failed, we'll check in a moment.
        taskState = States.Failed;
    else
        taskState = States.fromName( char( command.getState() ) );
    end
end

if taskState == States.Failed
    % When the local scheduler says that a task is in the failed state it is
    % possible that PCT code has finished executing correctly but that
    % MATLAB has exited with a non-zero exit status after we have finished
    % up. To pro-actively search for this situation we get the actual task
    % state here and we DO NOT set a task as failed if it indicates it has
    % finished correctly
    taskStateInStorage = thisTask.hGetProperty( 'StateEnum' );
    if taskStateInStorage == States.Finished
        taskState = States.Finished;
    else
        % Only set the task state if it hasn't already been set.
        if taskStateInStorage ~= States.Failed
            thisTask.hSetPropertyNoCheck( 'StateEnum', States.Failed );
        end
    end
end

end
