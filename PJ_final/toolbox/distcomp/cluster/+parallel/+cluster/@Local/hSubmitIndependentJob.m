function hSubmitIndependentJob( obj, job, jobSupport, jobSupportID )
% hSubmitIndependentJob - submit tasks for execution

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.cluster.Local;
import parallel.internal.cluster.SubmissionChecks;
import parallel.internal.apishared.EnvVars;
import parallel.internal.apishared.FilenameUtils;
import com.mathworks.toolbox.distcomp.local.TaskCommand;

% Independent jobs always need the license information in the command
needLicenseArgInCommand = true;

[tasks, numTasks]   = SubmissionChecks.checkIndependentTasksBeforeSubmission( job );
jobSupport.prepareJobForSubmission( job, jobSupportID );
[jsNames, jsVals]   = jobSupport.getJobSubmissionEnvVars( jobSupportID );
[cNames,  cVals ]   = obj.getClusterEnvVarsForJobSubmission();
[ijNames, ijVals]   = iIndJobEnvVars();
jobNames            = [ jsNames(:); cNames(:); ijNames ];
jobVals             = [ jsVals(:); cVals(:); ijVals ];
commandArray        = obj.calculateMatlabCommandForJob( job, needLicenseArgInCommand );
taskUUIDs           = cell(numTasks, 1);
javaTasks           = cell(numTasks, 1);
taskIDs             = zeros(numTasks, 1);
taskLogs            = cell( numTasks, 1 );

for i = 1:numTasks
    % Set the task location environment variable correctly
    taskSID = tasks(i).hGetSupportID();
    [taskNames, taskVals] = jobSupport.getTaskSubmissionEnvVars( taskSID );

    taskLogs{i} = jobSupport.getIndJobTaskLogFile( jobSupportID, taskSID );
    javaTask = TaskCommand.getNewInstance( commandArray, ...
                                           [jobNames; taskNames(:)], ...
                                           [jobVals; taskVals(:)], ...
                                           taskLogs{i} );
    javaTasks{i} = javaTask;
    % Mechanism to track the UUID against the actual task
    taskUUIDs{i} = javaTask.getUUID;
    taskIDs(i) = tasks(i).Id;
end

% Calculate the client environment to forward once we know the taskNames - these
% must not be overridden. Also note that job submission can only proceed if
% numTasks > 0, so taskNames will be defined.
[clientEnvNames, clientEnvValues] = EnvVars.createLocalClusterEnvironment( [jobNames; taskNames(:)] );

% Data returned from local -
% localTaskUUIDs : the local task Id for each task - NOTE for parallel jobs where
%                  there is only one actual task every real task will map to
%                  the same local task
% taskIDs        : the Id of the parallel.task.CJSTask associated with a local
%                  task in the field above. It is an error for taskIDs and
%                  localTaskUUIDs to be of different lengths.

% TODO:later remove this abomination.
logRelToStorage = FilenameUtils.fixSlashes(regexprep(tasks(1).hGetSupportID(), '[0-9]*$', ''));
schedulerData = struct('type', 'local', ...
                       'taskUUIDs', {taskUUIDs} , ...
                       'taskIDs', {taskIDs}, ...
                       'submitProcInfo', obj.ProcessInformation, ...
                       'logRelToStorage', {logRelToStorage}, ...
                       'clientEnvNames', {clientEnvNames}, ...
                       'clientEnvValues', {clientEnvValues});

job.JobSchedulerData = schedulerData;
% Now submit the actual java tasks
for i = 1:numTasks
    javaTasks{i}.submit;
end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function [ names, vals ] = iIndJobEnvVars()
names = { 'MDCE_DECODE_FUNCTION' };
vals  = { func2str( @parallel.internal.decode.localSingleTask ) };
end
