%Local Interact with a Local cluster
%   The Local cluster type allows you to submit jobs to be executed by workers
%   running on the local machine.
%
%   The NumWorkers property of the Local cluster is automatically configured to
%   match the number of cores in your computer. You can override this either by
%   setting the property on the cluster object or by modifying the profile using
%   the profiles manager.
%
%   parallel.cluster.Local methods:
%      Local                  - Create a local cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      getDebugLog            - Read output messages from communicating job or independent task
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.cluster.Local properties:
%      Host                             - Host this local cluster is running on
%      JobStorageLocation               - Folder where cluster stores job and task information
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use with MathWorks hosted licensing
%      Modified                         - True if any properties in this cluster have been modified
%      NumWorkers                       - Number of workers in the cluster
%      Profile                          - Profile used to build this cluster
%      RequiresMathWorksHostedLicensing - Cluster uses MathWorks hosted licensing
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%
%    See also parcluster, parallel.Cluster.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true, Sealed ) Local < parallel.cluster.CJSCluster

    properties ( SetAccess = immutable )
        %Host Host this local cluster is running on
        %   (read-only)
        Host
    end

    % --------------------------------------------------------------------------
    % Private instance properties.
    properties ( Transient, GetAccess = private, SetAccess = immutable )
        % Java local scheduler object
        LocalSched
    end

    % Private constants
    properties ( Constant, GetAccess = private )
        % Used to check if jobs belong to this process
        ProcessInformation   = iGetProcInfo();
        % MaxNumWorkers must not exceed this
        MaxAllowedNumWorkers = iGetMaxAllowedNumWorkers();
        % We pass this through to the CJSCluster constructor
        ShortLocalHostName   = iGetShortLocalHostName();
    end

    % --------------------------------------------------------------------------
    % Private/Static implementations
    methods ( Access = private, Static )
        function [correctType, correctClient, jobData] = getJobSchedDataIfAvailable( job )
        % Return the job data, and information about whether or not it's valid.
        % returns:
        % - correctType - was the job submitted by a Local cluster
        % - correctClient - was the job submitted by a Local cluster in this process
        % - jobData - the underlying job scheduler data.
            import parallel.cluster.Local;

            jobData        = job.JobSchedulerData;
            correctType    = ~isempty( jobData ) && isequal( jobData.type, 'local' );
            correctClient  = correctType && isequal( ...
                jobData.submitProcInfo, Local.ProcessInformation );
        end
    end

    % --------------------------------------------------------------------------
    % Private/Instance methods
    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end

        didCancel = cancelOrDestroyJob( obj, job, fcnH )
        didCancel = cancelOrDestroyTask( obj, task, fcnH )
        state     = getIndependentJobState( obj, job, jobData )

        function cmdArray = calculateMatlabCommandForJob( obj, ~, needLicenseArg )
        % Return the command array needed during submission
            import parallel.internal.apishared.WorkerCommand;

            [ mlcmd, argsCell ] = WorkerCommand.localCommand( obj.ClusterMatlabRoot, needLicenseArg );
            cmdArray            = [ {mlcmd}, argsCell ];
        end

        function state = getCommunicatingJobState( obj, ~, jobData )
        % Calculate the state of a CommunicatingJob.
            import parallel.internal.types.States

            command = obj.LocalSched.getCommand( jobData.taskUUIDs{1} );
            if ~isempty( command )
                state = States.fromName( char( command.getState() ) );
            else
                state = States.Failed;
            end
        end
    end

    % --------------------------------------------------------------------------
    % Protected implementations
    methods ( Access = protected )
        function tf = wasSubmittedByThisType( ~, job )
        % Defers to getJobSchedDataIfAvailable
            import parallel.cluster.Local
            tf = Local.getJobSchedDataIfAvailable( job );
        end

        function state = getJobState( obj, job, jobStateInStorage )
        % getJobState - talk to the underlying cluster to derive the job
        % state. By the time we get here, we know that the jobData is plausible,
        % but we can't be sure it's exactly right - so we need to do some extra
        % checks as per localscheduler/pGetJobState.

            import parallel.cluster.Local

            [~, correctClient, jobData] = Local.getJobSchedDataIfAvailable( job );
            if ~correctClient
                if ~iShouldQueryJobState( Local.ProcessInformation, ...
                                          jobData.submitProcInfo );
                    % Get here if the job was submitted on another host, or if
                    % the old client process is still alive.
                    % Note that we don't ever want Local to return an unknown 
                    % state because each client process has its own "scheduler", and 
                    % we shouldn't pass judgment on the state for other clients, so
					% just return the state that was passed in.
                    state = jobStateInStorage;
                    return
                end
            end
            if isa( job, 'parallel.CommunicatingJob' )
                state = obj.getCommunicatingJobState( job, jobData );
            else
                state = obj.getIndependentJobState( job, jobData );
            end
        end

        function updateDerivedProfileProperties( obj )
        % We need to ensure that the propStorage has the right value for
        % NumWorkers, but without overriding where it thinks that value comes
        % from.
            actualNumWorkers = double( obj.LocalSched.getMaximumNumberOfWorkers() );
            obj.PropStorage.setValueAtSameLevel( 'NumWorkers', actualNumWorkers );
        end

        function tf = isEquivalentWorkers( ~, ~ )
        % isEquivalentWorkers for Local always returns TRUE because all
        % Local clusters use the same pool of workers.
            tf = true;
        end
        
        function [names, values] = getClusterSpecificEnvVars( ~ )
        % Return names/values of environment variables that this cluster must
        % send down to workers
            names  = { 'MDCE_PID_TO_WATCH', ...
                       'MDCE_USE_ML_LICENSING' };
            values = { sprintf( '%d', feature( 'getpid' ) ), ...
                       'true' };

            mdceDebugName = 'MDCE_DEBUG';
            mdceDebug = getenv(mdceDebugName);
            if ~isempty(mdceDebug)
                names{end+1}  = mdceDebugName;
                values{end+1} = mdceDebug;
            end

            [isLicensedOnline, token, entitlementID] = parallel.internal.cluster.getOnlineLicenseInfo();
            if isLicensedOnline
                names = [names, ...
                         'MLM_WEB_ID', ...
                         'MLM_WEB_USER_CRED'];
                 values = [values, ...
                          entitlementID, ...
                          token];
            end
        end
    end

    % --------------------------------------------------------------------------
    % Public/Hidden implementations
    methods ( Hidden )
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            
            clusterPropertyMap = hGetDisplayItems@parallel.Cluster(obj, diFactory);
           
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'Profile', ...
                'Modified', ...
                'Host', ...
                'NumWorkers', ...
                'Separator', ...
                'JobStorageLocation', ...
                'RequiresMathWorksHostedLicensing'};
        end
     
        % Get the data necessary to create a new cluster equivalent to this one. 
        function data = hGetMementoData(obj) 
            data = struct('JobStorageLocation', obj.JobStorageLocation); 
        end
          
        % Actually submit jobs of either type
        hSubmitIndependentJob( obj, job, jobSupport, jobSupportID )
        hSubmitCommunicatingJob( obj, job, jobSupport, jobSupportID )

        % Cancellation/destruction on the Java scheduler
        function didCancel = hCancelJob( obj, job )
            didCancel = obj.cancelOrDestroyJob( job, @cancel );
        end
        function hDestroyJob( obj, job )
            obj.cancelOrDestroyJob( job, @destroy );
        end
        function didCancel = hCancelTask( obj, task )
            didCancel = obj.cancelOrDestroyTask( task, @cancel );
        end
        function hDestroyTask( obj, task )
            obj.cancelOrDestroyTask( task, @destroy );
        end

        function hSetProperty( obj, names, values )
        % Extra checks for properties that this object does not allow to be set.
            if ~iscell( names )
                names  = { names };
                values = { values };
            end
            for ii = 1:length( names )
                name  = names{ii};
                value = values{ii};
                switch name
                  case 'NumWorkers'
                    if value > obj.MaxAllowedNumWorkers
                        error(message('parallel:cluster:LocalInvalidNumWorkers', obj.MaxAllowedNumWorkers));
                    else
                        obj.LocalSched.setMaximumNumberOfWorkers( double( value ) );
                    end
                  case { 'ClusterMatlabRoot', 'OperatingSystem', 'HasSharedFilesystem', ...
                         'LicenseNumber', 'RequiresMathWorksHostedLicensing' }
                    if obj.isNotCurrentValue( name, value )
                        throwAsCaller( obj.buildInvalidValue( name ) );
                    end
                end
                obj.hSetProperty@parallel.cluster.CJSCluster( name, value );
            end
        end

        function hPreJobEvaluate( ~, job, ~ )
            jobSchedulerData = job.JobSchedulerData;

            % Make our environment match the client
            cellfun( @(n,v) setenv( n, v ), ...
                     jobSchedulerData.clientEnvNames, ...
                     jobSchedulerData.clientEnvValues );
        end
    end
    
    % --------------------------------------------------------------------------
    % Public/documented implementations
    methods
        function obj = Local( varargin )
        %Local Create a local cluster instance
        %   parallel.cluster.Local(p1, v1, p2, v2, ...) builds a generic
        %   cluster with the specified property values. The properties can
        %   include any of the properties of the Local cluster class, or a
        %   profile.
        %
        %   Example:
        %   % Create a Local cluster using the 'myLocal' profile.
        %   myCluster = parallel.cluster.Local('Profile', 'myLocal');
        %
        %   See also parcluster, parallel.Cluster.

            import parallel.internal.apishared.LocalUtils
            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.cluster.Local

            try
                [propsFromArgs, cProf] = ...
                    ConstructorArgsHelper.interpretClusterCtorArgs( ...
                        ?parallel.cluster.Local, varargin{:} );
            catch E
                throw( E );
            end

            % Get args ready for CJSCluster constructor, removing JobStorageLocation
            % from propsFromArgs if set.
            [dataLoc, dataLocValType] = iChooseDataLocation( propsFromArgs );

            obj@parallel.cluster.CJSCluster( 'Local', ...
                                             cProf, dataLoc, dataLocValType );

            obj.Host = Local.ShortLocalHostName;

            obj.LocalSched = LocalUtils.getJavaScheduler();

            % The 'factory' value for this instance is whatever the Java layer
            % is currently set to before we do anything else.
            localFactoryNumWorkers = obj.LocalSched.getMaximumNumberOfWorkers();

            obj.PropStorage.setValue( ...
                'NumWorkers', parallel.internal.settings.ValueType.Factory, ...
                localFactoryNumWorkers );

            obj.applyConstructorArgs( propsFromArgs );
        end
    end
end

% --------------------------------------------------------------------------
% Information to uniquely identify this process.
function procInfo = iGetProcInfo()
    pid      = feature('getpid');
    pidname  = dct_psname( pid );
    hostname = char( java.net.InetAddress.getLocalHost.getCanonicalHostName );
    procInfo = struct( 'pid',      pid, ...
                       'pidname',  pidname, ...
                       'hostname', hostname );
end

% --------------------------------------------------------------------------
% Look through the arguments we've got and see if there's a JobStorageLocation, else
% derive the default. We will remove the JobStorageLocation from the propsFromArgs
% map.
function [dataLoc, dataLocValType] = iChooseDataLocation( propsFromArgs )
    if propsFromArgs.isKey( 'JobStorageLocation' )
        dataLocAndType = propsFromArgs('JobStorageLocation');
        dataLoc        = dataLocAndType{1};
        dataLocValType = dataLocAndType{2};
        propsFromArgs.remove( 'JobStorageLocation' );
    else
        subDir      = fullfile('local_cluster_jobs', ...
                               ['R' version('-release')]);
        try
            baseDir = fileparts(prefdir);
            dataLoc = fullfile(baseDir, subDir);
        catch E %#ok<NASGU> "This is a conceivable failure that we deal with"
            dataLoc = fullfile(tempdir, subDir);
        end
        if exist( dataLoc, 'dir' ) ~= 7
            dataLoc = iBuildDirectoryForDataLoc( dataLoc, subDir );
        end
        dataLocValType = parallel.internal.settings.ValueType.Factory;
    end
end

% --------------------------------------------------------------------------
% iBuildDirectoryForDataLoc - construct a directory for the data location with
% fallback.
function dataLoc = iBuildDirectoryForDataLoc( dataLoc, subDir )
    mkdir( dataLoc );
    gotDir = exist( dataLoc, 'dir' ) == 7;
    if ~gotDir
        % Fallback - use tempdir
        dataLoc = fullfile( tempdir, subDir );
        if exist( dataLoc, 'dir' ) ~= 7
            mkdir( dataLoc );
            gotDir = exist( dataLoc, 'dir' ) == 7;
            if ~gotDir
                % Cannot find a suitable data location
                error(message('parallel:cluster:LocalNoDataLocation'));
            end
        end
    end
end

% --------------------------------------------------------------------------
% Return the maximum possible value of NumWorkers
function maxNum = iGetMaxAllowedNumWorkers()
    import com.mathworks.toolbox.distcomp.local.LocalConstants
    maxNum = double( LocalConstants.sMAX_NUMBER_OF_WORKERS );
end

% --------------------------------------------------------------------------
% iGetShortLocalHostName - return the short name of the host
function shortName = iGetShortLocalHostName()
    shortName = char( java.net.InetAddress.getLocalHost.getHostName );
end

% --------------------------------------------------------------------------
% iShouldQueryJobState - should we query the job state of a job
function tf = iShouldQueryJobState( ourProcInfo, jobProcInfo )

    tf = false;
    % Was the job submitted from this host
    jobSubmittedOnThisHost = isequal( ourProcInfo.hostname, jobProcInfo.hostname );

    if jobSubmittedOnThisHost
        % We can query to see if the old client is still alive
        [pidName, pidIsAlive] = dct_psname( jobProcInfo.pid );
        oldClientIsAlive = pidIsAlive && isequal( pidName, jobProcInfo.pidname );

        % We should query the job state only if the old client is not alive.
        tf = ~oldClientIsAlive;
    end
end
