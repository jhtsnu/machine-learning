function hSubmitCommunicatingJob( obj, job, jobSupport, jobSupportID )
% hSubmitCommunicatingJob - submit job for execution

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.cluster.Local;
import parallel.internal.cluster.SubmissionChecks;
import parallel.internal.cluster.CJSJobMethods;
import parallel.internal.apishared.SmpdGateway;
import parallel.internal.apishared.EnvVars;

USE_MPIEXEC = SmpdGateway.canUseSmpd();

SubmissionChecks.checkCommunicatingTasksBeforeSubmission( job );

workerLims = job.NumWorkersRange;
minW       = workerLims(1);

% Local cluster has a complicated series of tests on NumWorkers.
iLocalNumWorkersCheck( obj, minW );

% If we get here, we know that minW >= obj.NumWorkers, so we can be sure maxW >=
% minW:
maxW = min( workerLims(2), obj.NumWorkers );

CJSJobMethods.duplicateTasksOfCommunicatingJob( job, jobSupport, jobSupportID, maxW );
jobSupport.prepareJobForSubmission( job, jobSupportID );

% Get the duplicated tasks
tasks    = job.Tasks;
numTasks = numel( tasks );

% discover the directory to use for temporary stuff relating to this job
jobDir = jobSupport.getJobDirOnClient( jobSupportID );

% Communicating jobs need the license information in the command if they're
% being launched directly. The SmpdGateway handles licenses for mpiexec jobs.
needLicenseArgInCommand = ~USE_MPIEXEC;

% Set up the environment variables
[jsNames, jsVals]     = jobSupport.getJobSubmissionEnvVars( jobSupportID );
[cNames,  cVals ]     = obj.getClusterEnvVarsForJobSubmission();
[cjNames, cjVals]     = iCommJobEnvVars( USE_MPIEXEC, jobDir );
envNames              = [jsNames(:); cNames(:); cjNames(:)];
envValues             = [jsVals(:); cVals(:); cjVals(:)];
[clientEnvNames, clientEnvValues] = EnvVars.createLocalClusterEnvironment( envNames );
commandArray          = obj.calculateMatlabCommandForJob( job, needLicenseArgInCommand );
[absLog, relLog]      = jobSupport.getCommJobLogFile( jobSupportID );

if USE_MPIEXEC
    import com.mathworks.toolbox.distcomp.local.MpiexecJobCommand;
    mpiexecCmdArray = SmpdGateway.getMpiexecArgs( envNames );
    javaTask        = MpiexecJobCommand.getNewInstance( ...
        mpiexecCmdArray, commandArray, envNames, envValues, ...
        jobDir, absLog, [minW maxW] );
else
    import com.mathworks.toolbox.distcomp.local.ParallelJobCommand;
    javaTask = ParallelJobCommand.getNewInstance( ...
        commandArray, envNames, envValues, absLog, [minW maxW] );
end

job.JobSchedulerData = struct('type', 'local', ...
                              'taskUUIDs', {repmat({javaTask.getUUID}, numTasks, 1)} , ...
                              'taskIDs', {1:numTasks}, ...
                              'submitProcInfo', obj.ProcessInformation, ...
                              'logRelToStorage', {relLog}, ...
                              'clientEnvNames', {clientEnvNames}, ...
                              'clientEnvValues', {clientEnvValues});
javaTask.submit;

end

% --------------------------------------------------------------------------
% iCommJobEnvVars - communicating job environment variables, dependent on
% whether mpiexec is being used.
function [ names, vals ] = iCommJobEnvVars( useMpiExec, jobDir )
names = { 'MDCE_PARALLEL'};
vals  = { 'true' };

if isempty( getenv( 'MDCE_OVERRIDE_CLIENT_HOST' ) )
    names{end+1} = 'MDCE_OVERRIDE_CLIENT_HOST'; % append these to envNames
    vals{end+1}  = '127.0.0.1';
end
if isempty( getenv( 'MPICH_INTERFACE_HOSTNAME' ) )
    names{end+1} = 'MPICH_INTERFACE_HOSTNAME';
    vals{end+1}  = '127.0.0.1';
end

% Use func2str on the decode function to allow dependencies to be tracked.
if useMpiExec
    % Just define extraNames /values in one go here
    names{end+1} = 'MDCE_DECODE_FUNCTION';
    vals{end+1}  = func2str( @parallel.internal.decode.localMpiexecTask );
else
    % Use the connect/accept decode function, and force the "sock" MPI build.
    names = [names, { 'MDCE_DECODE_FUNCTION', ...
                      'MDCE_SENTINAL_LOCATION', ...
                      'MDCE_FORCE_MPI_OPTION' } ];

    vals  = [vals, { func2str( @parallel.internal.decode.localParallelTask ), ...
                     fullfile(jobDir, 'mpi'), ...
                     'sock' } ];
end
end

% --------------------------------------------------------------------------
% iLocalNumWorkersCheck - check the minimum number of workers requested for the
% job and throw an error if necessary.
function iLocalNumWorkersCheck( obj, minW )
if minW > obj.MaxAllowedNumWorkers
    % Never going to run
    error(message('parallel:cluster:LocalMaxAllowedWorkersExceeded', minW, obj.MaxAllowedNumWorkers));
elseif minW > obj.NumWorkers
    % NumWorkers must be < obj.MaxAllowedNumWorkers, so could be increased
    if isempty( obj.Profile )
        error(message('parallel:cluster:LocalNumWorkersExceeded', minW, obj.NumWorkers, obj.MaxAllowedNumWorkers));
    else
        error(message('parallel:cluster:LocalProfileNumWorkersExceeded', minW, obj.Profile, obj.NumWorkers, obj.MaxAllowedNumWorkers));
    end
end
end
