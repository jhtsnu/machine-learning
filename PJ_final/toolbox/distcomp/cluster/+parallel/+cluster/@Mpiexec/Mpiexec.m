%Mpiexec Interact with mpiexec on the local host
%   The Mpiexec cluster uses the 'mpiexec' command on your local system to run
%   communicating jobs. This simple cluster type can be useful if you do not
%   have any other cluster type available.
%
%   parallel.cluster.Mpiexec methods:
%      Mpiexec                - Create an Mpiexec cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      getDebugLog            - Read output messages from communicating job or independent task
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.cluster.Mpiexec properties:
%      ClusterMatlabRoot                - Path to MATLAB on the cluster
%      EnvironmentSetMethod             - Method by which environment variables are forwarded
%      HasSharedFilesystem              - Specify whether the client and cluster nodes share JobStorageLocation
%      Host                             - Host on which this Mpiexec cluster is running
%      JobStorageLocation               - Folder where cluster stores job and task information
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use with MathWorks hosted licensing
%      Modified                         - True if any properties in this cluster have been modified
%      MpiexecFilename                  - Full path to mpiexec command to be used
%      NumWorkers                       - Number of workers in the cluster
%      OperatingSystem                  - Operating System of the cluster
%      Profile                          - Profile used to build this cluster
%      RequiresMathWorksHostedLicensing - Cluster uses MathWorks hosted licensing
%      SubmitArguments                  - Additional command-line arguments to mpiexec
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%
%   See also parallel.Cluster, parcluster.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true, Sealed ) Mpiexec < parallel.cluster.CJSCluster

    properties ( PCTGetSet, Transient, PCTConstraint = 'string' )
        %MpiexecFilename Full path to mpiexec command to be used
        MpiexecFilename

        %SubmitArguments Additional command-line arguments to mpiexec
        SubmitArguments
    end
    properties ( PCTGetSet, Transient, ...
                 PCTConstraint = 'enum:parallel.internal.types.MpiexecSetEnvMethod')
        %EnvironmentSetMethod Method by which environment variables are forwarded
        %   The Mpiexec cluster needs to forward environment variables to the
        %   workers. The following values for EnvironmentSetMethod can be specified:
        %   '-env'   : the Mpiexec cluster automatically appends a series of
        %              '-env' command-line arguments. This is the default.
        %   'setenv' : the Mpiexec cluster simply calls 'setenv' in the MATLAB
        %              client to set up the environment.
        EnvironmentSetMethod
    end

    properties ( SetAccess = immutable )
        %Host Host on which this Mpiexec cluster is running
        %   For the Mpiexec cluster, this is always the local host.
        Host
    end

    methods ( Static, Access = private )
        function tf = shouldCancelJobBeforeDestruction( job )
            import parallel.internal.apishared.MpiexecUtils
            import parallel.internal.types.States

            jobState = job.StateEnum;
            switch jobState
              case { States.Pending, States.Unavailable, States.Destroyed, States.Failed }
                tf = false;
              case { States.Queued, States.Running }
                tf = true;
              case States.Finished
                tf = MpiexecUtils.shouldCancelFinishedJob( job.FinishTime );
            end
        end
    end
    
    methods ( Hidden )
        % Add Cluster Properties to Display
        function [clusterPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            clusterPropertyMap = hGetDisplayItems@parallel.Cluster(obj, diFactory);

            % The order the properties appear in the propNames array is the
            % order in which they will be displayed. 
            propNames = {...
                'Profile', ...
                'Modified', ...
                'Host', ...
                'NumWorkers', ...
                'Separator', ...
                'JobStorageLocation', ...
                'ClusterMatlabRoot', ...
                'OperatingSystem', ...
                'MpiexecFilename', ...
                'SubmitArguments', ...
                'EnvironmentSetMethod', ...
                'RequiresMathWorksHostedLicensing'};
            
            % Add cluster specific items to map for display
            names = {...
                'MpiexecFilename', ...
                'SubmitArguments', ...
                'EnvironmentSetMethod'};
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            clusterPropertyMap = [clusterPropertyMap; containers.Map(names, values)];
        end
        
        % Get the data necessary to create a new cluster equivalent to this one.
        function data = hGetMementoData(obj)
            data = struct('JobStorageLocation', obj.JobStorageLocation);
        end
        
        function hDestroyJob( cluster, job )
            import parallel.cluster.Mpiexec
            
            if Mpiexec.shouldCancelJobBeforeDestruction( job )
                cluster.hCancelJob( job );
            end
        end
        function hDestroyTask( cluster, task )
            cluster.hDestroyJob( task.Parent );
        end
        function OK = hCancelJob( cluster, job )

            import parallel.internal.apishared.MpiexecUtils

            OK = false;

            [wasSubmitted, isMpi, isAlive, pid, whyNotAlive] = ...
                MpiexecUtils.interpretJobSchedulerData( ...
                    cluster.Host, job.JobSchedulerData, job.Id );

            if ~wasSubmitted
                OK = true;
                return
            end
            if ~isMpi
                % Not an MPIEXEC job - we really shouldn't cancel this
                warning(message('parallel:cluster:MpiexecCannotCancel', job.Id));
                return
            end

            if isAlive
                % yes, we can cancel
                try
                    dct_psfcns( 'kill', pid );
                catch err
                    % Failed to kill - maybe permissions?
                    warning(message('parallel:cluster:MpiexecFailedToKillProcess', job.Id, pid, err.message));
                    % return false
                    return
                end
                if dct_psfcns( 'isalive', pid )
                    % then the "kill" will have warned - return OK == false
                    return
                else
                    % Set the PID to -1 so that it never gets checked again
                    jsd = job.JobSchedulerData;
                    jsd.pid = -1;
                    job.JobSchedulerData = jsd;
                    OK = true;
                    return
                end
            else
                % Why wasn't the job alive?
                if strcmp( whyNotAlive.reason, 'wrongclient' )
                    % Not OK - warn and return false
                    warning(message('parallel:cluster:MpiexecUnableToCancel', whyNotAlive.description));
                    return
                else
                    % The pid simply isn't alive any more, nothing for us to do
                    OK = true;
                    return
                end
            end
        end
        function ok = hCancelTask( cluster, task )
            ok = cluster.hCancelJob( task.Parent );
        end

        hSubmitCommunicatingJob( varargin )
        function hSubmitIndependentJob( varargin )
            error(message('parallel:cluster:MpiexecIndependentNotSupported'));
        end

        function tf = hIsVariantSupported(~, variant)
            validateattributes(variant, {'parallel.internal.types.Variant'}, {'scalar'});
            tf = variant ~= parallel.internal.types.Variant.IndependentJob;
        end
    end
    methods
        function obj = Mpiexec( varargin )
        %Mpiexec Create an Mpiexec cluster instance
        %   parallel.cluster.Mpiexec(p1, v1, p2, v2, ...) builds an Mpiexec
        %   cluster with the specified property values. The properties can
        %   include any of the properties of the Mpiexec cluster class, or a
        %   profile.
        %
        %   Example:
        %   % Create an Mpiexec cluster using the 'myMpiexec' profile.
        %   myCluster = parallel.cluster.Mpiexec('Profile', 'myMpiexec');
        %
        %   See also parcluster, parallel.Cluster.

            import parallel.internal.cluster.ConstructorArgsHelper
            import parallel.cluster.CJSCluster

            try
                [propsFromArgs, cProf] = ...
                    ConstructorArgsHelper.interpretClusterCtorArgs( ...
                        ?parallel.cluster.Mpiexec, varargin{:} );
            catch E
                throw( E );
            end

            [dataLoc, dataLocValType] = CJSCluster.chooseDataLocation( propsFromArgs );

            obj@parallel.cluster.CJSCluster( 'Mpiexec', ...
                                             cProf, dataLoc, dataLocValType );
            factoryMpiexec = fullfile( matlabroot, 'bin', dct_arch, 'mpiexec' );
            obj.PropStorage.addFactoryValue( 'MpiexecFilename', factoryMpiexec );
            obj.PropStorage.addFactoryValue( 'SubmitArguments', '' );
            obj.PropStorage.addFactoryValue( 'EnvironmentSetMethod', '-env' );
            obj.applyConstructorArgs( propsFromArgs );

            obj.Host        = char( java.net.InetAddress.getLocalHost.getCanonicalHostName );
        end
    end
    methods ( Access = protected )
        function jobState = getJobState( obj, job, ~ )
            import parallel.internal.apishared.MpiexecUtils
            import parallel.internal.types.States
            import parallel.internal.cluster.JobStateHelper

            % If we get here, we know that the job is queued/running, and was
            % submitted by Mpiexec.

            [~, ~, isAlive] = MpiexecUtils.interpretJobSchedulerData( ...
                obj.Host, job.JobSchedulerData, job.Id );

            if isAlive
                % Ok
                jobState = States.Running;
            else
                jobState = States.Unknown;
            end
            % CJSCluster takes care of hSetTerminalStateFromCluster if necessary.
        end
        function [tf, jobData] = wasSubmittedByThisType( ~, job )
            jobData = job.JobSchedulerData;
            tf      = ~isempty( jobData ) && isequal( jobData.type, 'mpiexec' );
        end
        
        function [names, values] = getClusterSpecificEnvVars( ~ )
        % Return names/values of environment variables that this cluster must
        % send down to workers
            names  = {};
            values = {};
            mdceDebugName = 'MDCE_DEBUG';
            mdceDebug = getenv(mdceDebugName);
            if ~isempty(mdceDebug)
                names  = {mdceDebugName};
                values = {mdceDebug};
            end
        end
    end
    
    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
end
%#ok<*ATUNK>

