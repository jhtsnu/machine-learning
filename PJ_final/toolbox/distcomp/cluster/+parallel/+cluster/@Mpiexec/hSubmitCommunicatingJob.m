function hSubmitCommunicatingJob( mpiexec, job, jobSupport, jobSupportID )
% hSubmitCommunicatingJob - submit a communicating job to Mpiexec.

% Copyright 2011-2012 The MathWorks, Inc.

import parallel.internal.apishared.MpiexecUtils
import parallel.internal.apishared.FilenameUtils
import parallel.internal.cluster.SubmissionChecks
import parallel.internal.cluster.CJSJobMethods
import parallel.internal.apishared.WorkerCommand

if ~mpiexec.SharedFilesystem
    error(message('parallel:cluster:MpiexecRequiresSharedFilesystem'));
end
% mpiexec cannot work with 'mixed' workers
if strcmp( mpiexec.OperatingSystem, 'mixed' )
    error(message('parallel:cluster:MpiexecNoMixedWorkers'));
end

mpiexec.ensureLicenseNumberIsSetOrError();
SubmissionChecks.checkCommunicatingTasksBeforeSubmission( job );
maxW = SubmissionChecks.checkWorkerLimsForCommunicatingJob( ...
    mpiexec, job );
CJSJobMethods.duplicateTasksOfCommunicatingJob( ...
    job, jobSupport, jobSupportID, maxW );
jobSupport.prepareJobForSubmission( job, jobSupportID );

% Get the cluster environment variables
[envNames, envVals] = jobSupport.getJobSubmissionEnvVars( jobSupportID );
envNames{end+1} = 'MDCE_DECODE_FUNCTION';
envVals{end+1}  = 'parallel.internal.decode.mpiexecParallelTask';

[cNames, cVals] = mpiexec.getClusterEnvVarsForJobSubmission();
envNames = [envNames(:); cNames(:)];
envVals  = [envVals(:);  cVals(:)];

jobIsCommunicating = true;
[~, matlabExe, matlabArgs] = WorkerCommand.defaultCommand( ...
    mpiexec.OperatingSystem, mpiexec.ClusterMatlabRoot, jobIsCommunicating );
matlabExe = FilenameUtils.quoteForClient( matlabExe );
mpiexecCommand = FilenameUtils.quoteForClient( mpiexec.MpiexecFilename );

envString = iCalcEnvString( envNames, envVals, mpiexec.EnvironmentSetMethod );

submitString = sprintf( '%s %s -n %d %s %s %s', ...
                        mpiexecCommand, mpiexec.SubmitArguments, ...
                        maxW, envString, matlabExe, matlabArgs );
absLog = jobSupport.getCommJobLogFile( jobSupportID );
batName = FilenameUtils.jobSpecificFilename( ...
    mpiexec.OperatingSystem, jobSupport.getRootStorageStruct(), ...
    jobSupportID, 'Job.bat', true );
job.JobSchedulerData = MpiexecUtils.submit( submitString, absLog, batName, ...
                                            mpiexec.Host, mpiexec.OperatingSystem );

end

function envString = iCalcEnvString( names, values, setMethod )
%iCalcEnvString - return a string to put into the command-line.

import parallel.internal.apishared.FilenameUtils

values = cellfun( @FilenameUtils.quoteForClient, ...
                  values, 'UniformOutput', false );
switch setMethod
  case '-env'
    if ispc
        % Unfortunately, because we write this stuff out to a file on PC
        % clients, we need to defend against '%' in environment values (such as
        % might be introduced by URL-encoding the JobStorageLocation...)
        values = strrep( values, '%', '%%' );
    end
    eachStr = cellfun( @(n, v) sprintf( '-env %s %s', n, v ), ...
                       names, values, 'UniformOutput', false );
    envString = strtrim( sprintf( '%s ', eachStr{:} ) );
  case 'setenv'
    envString = '';
    cellfun( @(n, v) setenv( n, v ), names, values );
  otherwise
    % Can't happen because of the enum!
    error(message('parallel:cluster:MpiexecUnknownEnvironmentMethod', setMethod));
end

end
