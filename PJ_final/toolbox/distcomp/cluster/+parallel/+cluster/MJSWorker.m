% MJSWorker Worker in a MJS cluster
%
%   parallel.cluster.MJSWorker properties:
%      AllHostAddresses - IP addresses of the worker host
%      Name             - Name of the worker
%      Parent           - The cluster to which this worker belongs

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( Sealed ) MJSWorker < parallel.Worker

    properties ( SetAccess = immutable )
        %AllHostAddresses IP addresses of the worker host
        %   AllHostAddresses indicates the IP addresses of the computer
        %   running the worker.
        %   (read-only)
        %
        %   See also parallel.cluster.MJSWorker.Name
        AllHostAddresses
        
        %Name Name of the worker
        %   The descriptive name of a worker is set when its service is
        %   started, as described in "Customize Services" in the
        %   MATLAB Distributed Computing Server System Administrator's
        %   Guide. 
        %
        %   See also parallel.cluster.MJSWorker.AllHostAddresses
        Name
    end

    properties ( Transient, SetAccess = immutable )
        %Parent The cluster to which this worker belongs
        %
        %   See also parallel.cluster.MJS,
        %            parallel.cluster.MJSComputeCloud.
        Parent
    end

    methods ( Hidden )
        function hSetProperty( ~, ~, ~ )
            assert( false );
        end
        function hSetPropertyNoCheck( ~, ~, ~ )
            assert( false );
        end
        function v = hGetProperty( ~, names )
        % No PCTGetSet properties
            assert( iscell( names ) && isempty( names ) );
            v = {};
        end 
        function [workerPropertyMap, propNames]  = hGetDisplayItems(obj, diFactory)
            workerPropertyMap = hGetDisplayItems@parallel.Worker(obj, diFactory);
          
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'Host', ...
                'ComputerType', ...
                'Separator', ...
                'Name', ...
                'AllHostAddresses'};
          
            % Add MJSWorker specific displayable items to map
            workerPropertyMap('Name') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('Name'));
            workerPropertyMap('AllHostAddresses') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('AllHostAddresses'));
        end
        
        function obj = MJSWorker( mjs, worker )
            import parallel.internal.types.Variant
            import parallel.internal.cluster.MJSMethods

            validateattributes( mjs, ...
                {'parallel.cluster.MJS', 'parallel.cluster.MJSComputeCloud'}, ...
                {'scalar'} );
            
            validateattributes( worker, ...
                {'com.mathworks.toolbox.distcomp.worker.WorkerProperties', ...
                 'com.mathworks.toolbox.distcomp.worker.WorkerProxy'}, ...
                {'scalar'} );


            obj@parallel.Worker( char(worker.getHostName), char(worker.getComputerMLType) );
            obj.Parent           = mjs;
            obj.AllHostAddresses = cell( worker.getAllHostAddresses() );
            obj.Name             = char( worker.getName );                        
        end
    end    

    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
end
%#ok<*ATUNK> custom attributes
