function runprop = communicatingDecodeFcn(runprop)
%parallel.cluster.generic.communicatingDecodeFcn Prepare a worker to run a MATLAB task in a communicating job

% Copyright 2011-2012 The MathWorks, Inc.

dctSchedulerMessage(2, 'In parallel.cluster.generic.communicatingDecodeFcn');

% Read environment variables into local variables. The names of
% the environment variables were determined by the submit function
storageConstructor = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation = getenv('MDCE_STORAGE_LOCATION');
jobLocation = getenv('MDCE_JOB_LOCATION');
% For a communicating job, the task location is derived from labindex
taskLocation = fullfile(jobLocation, sprintf('Task%d', labindex));

% Set runprop properties from the local variables:
set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ...
    'TaskLocation', taskLocation);
