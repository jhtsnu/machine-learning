function runprop = independentDecodeFcn(runprop)
%parallel.cluster.generic.independentDecodeFcn Prepare a worker to run a MATLAB task in an independent job

% Copyright 2011-2012 The MathWorks, Inc.

dctSchedulerMessage(2, 'In parallel.cluster.generic.independentDecodeFcn');

% Read environment variables into local variables. The names of
% the environment variables were determined by the submit function
storageConstructor = getenv('MDCE_STORAGE_CONSTRUCTOR');
storageLocation = getenv('MDCE_STORAGE_LOCATION');
jobLocation = getenv('MDCE_JOB_LOCATION');
taskLocation = getenv('MDCE_TASK_LOCATION');

% Set runprop properties from the local variables:
set(runprop, ...
    'LocalSchedulerName', 'parallel.cluster.TaskRunner', ...
    'StorageConstructor', storageConstructor, ...
    'StorageLocation', storageLocation, ...
    'JobLocation', jobLocation, ...
    'TaskLocation', taskLocation);
