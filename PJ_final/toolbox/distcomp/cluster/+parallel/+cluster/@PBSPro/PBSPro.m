%PBSPro Interact with a PBS Pro cluster
%   The PBSPro cluster type allows you to submit jobs to PBS Pro. The PBS Pro
%   client tools must be installed on your client machine. The cluster machines
%   must be running the same operating system type as the client.
%
%   parallel.cluster.PBSPro methods:
%      PBSPro                 - Create a PBSPro cluster instance
%      batch                  - Run MATLAB script or function as batch job
%      createCommunicatingJob - Create a new communicating job
%      createJob              - Create a new independent job
%      findJob                - Find job objects belonging to a cluster object
%      getDebugLog            - Read output messages from communicating job or independent task
%      isequal                - True if clusters have same property values
%      matlabpool             - Open a pool of MATLAB workers on cluster
%      saveAsProfile          - Save modified properties to a profile
%      saveProfile            - Save modified properties to the current profile
%
%   parallel.cluster.PBSPro properties:
%      ClusterMatlabRoot                - Path to MATLAB on the cluster
%      CommunicatingJobWrapper          - Script that cluster runs to start workers
%      HasSharedFilesystem              - Specify whether the client and cluster nodes share JobStorageLocation
%      Host                             - Host of the cluster's headnode
%      JobStorageLocation               - Folder where cluster stores job and task information
%      Jobs                             - List of jobs contained in this cluster
%      LicenseNumber                    - License number to use with MathWorks hosted licensing
%      Modified                         - True if any properties in this cluster have been modified
%      NumWorkers                       - Number of workers in the cluster
%      OperatingSystem                  - Operating System of the cluster
%      Profile                          - Profile used to build this cluster
%      RcpCommand                       - Command to copy files to and from client
%      RequiresMathWorksHostedLicensing - Cluster uses MathWorks hosted licensing
%      ResourceTemplate                 - Define resources to request for communicating jobs
%      RshCommand                       - Remote execution command used on worker nodes during communicating job
%      SubmitArguments                  - Specify additional arguments to use when submitting jobs
%      Type                             - Type of this cluster
%      UserData                         - Data associated with a cluster object in this client session
%
%   See also parcluster, parallel.Cluster.

% Copyright 2011-2012 The MathWorks, Inc.

classdef (ConstructOnLoad = true, Sealed) PBSPro < parallel.cluster.PBS

    methods
        function obj = PBSPro( varargin )
        %PBSPro Create a PBSPro cluster instance
        %   parallel.cluster.PBSPro(p1, v1, p2, v2, ...) builds a PBSPro
        %   cluster with the specified property values. The properties can
        %   include any of the properties of the PBSPro cluster class, or a
        %   profile.
        %
        %   Example:
        %   % Create a PBSPro cluster using the 'myPBSPro' profile.
        %   myCluster = parallel.cluster.PBSPro('Profile', 'myPBSPro');
        %
        %   See also parcluster, parallel.Cluster.

            import parallel.internal.apishared.PbsHelper

            try
                pbsInfo = PbsHelper.getClusterInfo();
                useJA   = iCheckPBSProVersion( pbsInfo );
            catch E
                throw( E );
            end

            if isunix
                factoryRcp = 'rcp';
                useAttach  = true;
                stateinds = {'BRE', ...
                             'HQSTUW'};
            else
                factoryRcp = 'rcp -b';
                useAttach  = false;
                stateinds = {'RE', ...
                             'HQW'};
            end

            pbsConfig = struct( 'Type', 'PBSPro', ...
                                'Host', pbsInfo.server, ...
                                'UseJobArrays', useJA, ...
                                'UsePbsAttach', useAttach, ...
                                'StateIndicators', { stateinds }, ...
                                'FactoryRsh', 'pbs_remsh', ...
                                'FactoryRcp', factoryRcp );

            obj@parallel.cluster.PBS( pbsConfig, varargin{:} );
        end
    end

    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Return the PBSPro version number, or NaN if not found.
function verNum = iGetPBSProVersionNumber( infoStruc )
    PBSPro_major_ver = regexp( infoStruc.pbs_version, ...
                               '(?<=PBSPro_)[0-9]+', 'match', 'once' );
    verNum           = str2double( PBSPro_major_ver );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
function useJA = iCheckPBSProVersion( infoStruc )

% Pick the first set of digits following "PBSPro_"
    verNum = iGetPBSProVersionNumber( infoStruc );
    if isnan( verNum )
        warning(message('parallel:cluster:PbsProVersionCompatibility', infoStruc.pbs_version));
        useJA  = false;
    else
        if verNum >= 7
            useJA = true;
        else
            useJA = false;
            warning(message('parallel:cluster:PbsProJobArraysDisabled', infoStruc.pbs_version));
        end
    end
end
