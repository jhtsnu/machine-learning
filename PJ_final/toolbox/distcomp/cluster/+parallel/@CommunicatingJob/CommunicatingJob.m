%CommunicatingJob - base class for all communicating job objects
%   A communicating job object contains all the tasks that define what each
%   worker does as part of complete job execution. Workers executing a
%   communicating job all run at the same time, and can communicate with each
%   other. The nature of the communication allowed is determined by the 'Type'
%   of communicating job.
%
%   All communicating jobs may have only a single task defined before
%   submission.
%
%   There are two types of communicating job: 'Pool' and 'SPMD'. These can
%   be selected by using the 'Type' argument to the createCommunicatingJob
%   method.
%
%   'Pool' jobs run the task function on a single worker, and that task function
%   can use the remaining workers as a MATLABPOOL to run PARFOR loops and SPMD
%   blocks.
%
%   'SPMD' jobs run the task function on each worker simultaneously, as if the
%   entire task function was being executed within a single SPMD block.
%
%   parallel.CommunicatingJob methods:
%      cancel                - Cancel a pending, queued, or running job
%      createTask            - Create a new task in a job
%      delete                - Remove a job object from its cluster and memory
%      diary                 - Display or save text of batch job
%      fetchOutputs          - Retrieve output arguments from all tasks in a job
%      findTask              - Find task objects belonging to a job
%      listAutoAttachedFiles - List the files that are automatically attached to the job.
%      load                  - Load workspace variables from batch job
%      submit                - Submit job for execution in the cluster
%      wait                  - Wait for job execution to change state
%
%   parallel.CommunicatingJob properties:
%      AdditionalPaths - Folders to add to MATLAB search path of workers
%      AttachedFiles   - Files and folders that are sent to the workers
%      AutoAttachFiles - Determines whether dependent files will be automatically sent to the workers
%      CreateTime      - Time at which this job was created
%      FinishTime      - Time at which this job finished running
%      ID              - Job's numeric identifier
%      JobData         - Data made available to all workers for job's tasks
%      Name            - Name of this job
%      NumWorkersRange - Specify limits for number of workers
%      Parent          - Cluster object containing this job
%      StartTime       - Time at which this job started running
%      State           - State of the job: pending, queued, running, finished, or failed
%      SubmitTime      - Time at which this job was submitted
%      Tag             - Label associated with this job
%      Tasks           - Array of task objects contained in this job
%      Type            - Job's type: Independent, Pool, or SPMD
%      UserData        - Data associated with a job object
%      Username        - Name of the user who created this job
%
%   See also parallel.Job, parallel.Cluster/createCommunicatingJob.

% Copyright 2011-2012 The MathWorks, Inc.

classdef CommunicatingJob < parallel.Job

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public / documented properties
    properties ( Dependent, PCTConstraint = 'workerlimits|presubmission' )
        %NumWorkersRange Specify limits for number of workers
        %   NumWorkersRange specifies an upper and lower bound for the number of
        %   workers to be used for communicating job execution. If a scalar
        %   value is specified, then this is used as both the lower and upper
        %   bound, and the job runs on exactly that number of workers.
        NumWorkersRange
    end
    methods
        function v = get.NumWorkersRange( job )
            v = [ job.hGetProperty( 'MinNumWorkers' ), ...
                  job.hGetProperty( 'MaxNumWorkers' ) ];
        end
        function set.NumWorkersRange( job, args )
            job.hSetPropertyNoCheck( 'MinNumWorkers', args(1) );
            job.hSetPropertyNoCheck( 'MaxNumWorkers', args(end) );
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Protected/Hidden methods and properties
    properties ( Access = protected )
        % Iff this is a PoolJob, this property is true if the currently executing
        % process is running as part of the pool (rather than the "lead task" in
        % a non-interactive PoolJob).
        IsPoolTask = false;

        % Default PoolShutdownSuccessful to 'true' so that non-pool jobs don't
        % think something has gone wrong.
        PoolShutdownSuccessful = true;
    end

    methods ( Access = protected )
        function obj = CommunicatingJob( parent, id, variant )
            obj@parallel.Job( parent, id, variant );
        end
    end

    methods ( Hidden )
        function [jobPropertyMap, propNames]  = hGetDisplayItems(obj, diFactory)
            jobPropertyMap = hGetDisplayItems@parallel.Job(obj, diFactory);
            
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'ID', ...
                'Type', ...
                'Username', ...
                'State', ...
                'SubmitTime', ...
                'StartTime', ...
                'Running Duration', ...
                'NumWorkersRange', ...
                'Separator', ...
                'AutoAttachFiles', ...
                'Auto Attached Files', ...
                'AttachedFiles', ...
                'AdditionalPaths' ...
                };
           
            % Add Communicating job specific displayable items to map.
            jobPropertyMap('NumWorkersRange') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('NumWorkersRange'));
        end
        
        function tf = hIsPoolInteractive( job )
        % Return true if this is a CommunicatingPoolJob to be used interactively.
            tf = ( job.hGetProperty( 'ExecutionMode' ) == 1 );
        end
        function hSetInteractive( job, tf )
        % Indicate that this job is to be used for interactive purposes
            validateattributes( tf, {'logical'}, {'scalar'} );
            mode = double(tf);
            job.hSetPropertyNoCheck( 'ExecutionMode', mode );
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % API1 compatibility execution framework and properties
    methods ( Hidden ) % API1 compatibility layer
        function tf = pCurrentTaskIsPartOfAPool( job )
            tf = job.IsPoolTask;
        end
        function pInstantiatePool( job, ~, ~, ~, args )
            import parallel.internal.apishared.Pool

            job.IsPoolTask = Pool.instantiate( job.pIsInteractivePool(), args );
        end
        function OK = pShutdownPool( job )
            import parallel.internal.apishared.Pool

            job.PoolShutdownSuccessful = Pool.shutdown( job.IsPoolTask );
            OK = job.PoolShutdownSuccessful;
        end
        function tf = pIsInteractivePool( job )
            tf = job.hIsPoolInteractive();
        end
    end
end
%#ok<*ATUNK> custom attributes
