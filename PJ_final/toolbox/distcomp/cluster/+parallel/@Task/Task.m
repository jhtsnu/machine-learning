%Task Base class for all Task objects
%   Task objects define behavior and properties for task execution.
%
%   Task objects are constructed using the createTask method of a job.
%
%   parallel.Task methods:
%      cancel                - Cancel a pending or running task
%      delete                - Remove a task object from its job and memory
%      listAutoAttachedFiles - List the files that are automatically attached for this task.
%      wait                  - Wait for task object to change state
%
%   parallel.Task properties:
%      CaptureDiary       - Specify whether to return diary output
%      CreateTime         - When task was created
%      Diary              - Text produced by execution of task object's function
%      Error              - Task error information
%      ErrorIdentifier    - Task error identifier
%      ErrorMessage       - Message from task error
%      FinishTime         - When task finished running
%      Function           - Function called when evaluating task
%      ID                 - Task's numeric identifier
%      InputArguments     - Input arguments to task function
%      Name               - Name of this task
%      NumOutputArguments - Number of arguments returned by task function
%      OutputArguments    - Output arguments from running Function on a worker
%      Parent             - Job containing this task
%      StartTime          - When task started running
%      State              - Current state of task
%      UserData           - Data associated with a task object
%      Worker             - Object representing the worker for this task
%
%   See also parallel.Cluster, parallel.Job.

% Copyright 2011-2012 The MathWorks, Inc.

classdef Task < handle & ...
        matlab.mixin.Heterogeneous & ... % Different task subclasses may be concatenated
        parallel.internal.cluster.CustomPropDisp & ...
        parallel.internal.customattr.CustomGetSet

    % NB the only time we expect to concatenate tasks is during 'findTask' which
    % can accept a heterogeneous array of Job.

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public, documented properties
    properties ( PCTGetSet, Transient, PCTConstraint='function|presubmission' )
        %Function Function called when evaluating task
        %   Function can be specified as a function handle or a string.
        %   (read-only after job submission)
        Function = @() 0;
    end
    properties ( PCTGetSet, Transient, PCTConstraint='positivescalar|presubmission' )
        %NumOutputArguments Number of arguments returned by task function
        %   (read-only after job submission)
        NumOutputArguments = 1;
    end
    properties ( PCTGetSet, Transient, PCTConstraint='cell|presubmission' )
        %InputArguments Input arguments to task function
        %   (read-only after job submission)
        InputArguments = {};
    end
    properties ( PCTGetSet, Transient, PCTConstraint='logicalscalar|presubmission' )
        %CaptureDiary Specify whether to return diary output
        %   The diary output is available in the Diary property of the task.
        %   (read-only after job submission)
        CaptureDiary = false;
    end
    properties ( PCTGetSet, Transient, PCTConstraint='string' )
        %Name Name of this task
        Name = '';
    end
    properties ( PCTGetSet, Transient, SetAccess = private )
        %Diary Text produced by execution of task object's function
        %   Diary output is produced only if CaptureDiary is true.
        %   (read-only)
        Diary = '';
    end
    properties ( Dependent, SetAccess = private )
        %State Current state of task
        %   A task may be in state 'pending', 'running', or 'finished'.
        %   (read-only)
        State = 'pending';
        %Worker Object representing the worker for this task
        %   The worker object is an instance of parallel.Worker.
        %   (read-only)
        Worker = [];
    end
    methods
        function s = get.State( task )
            s = task.StateEnum.Name;
        end
        function w = get.Worker( task )
            w = task.getWorker();
        end
    end
    properties ( PCTGetSet, Transient, SetAccess = private, Hidden )
        StateEnum = parallel.internal.types.States.Pending; % Hidden backing for State property
    end

    properties ( PCTGetSet, Transient, SetAccess = ?parallel.Job, Hidden )
        DependentFiles; % Hidden cache of auto attached files.
    end

    properties ( Hidden, SetAccess = immutable )
        Variant = parallel.internal.types.Variant.Task;
    end

    properties ( PCTGetSet, Transient, SetAccess = private )
        %Error Task error information
        %   The Error property contains an MException, which is empty if no
        %   error occurred.
        %   (read-only)
        Error           = MException.empty();

        %ErrorMessage Message from task error
        %   (read-only)
        ErrorMessage    = '';

        %ErrorIdentifier Task error identifier
        %   (read-only)
        ErrorIdentifier = '';

        %CreateTime When task was created
        %   (read-only)
        CreateTime      = '';

        %StartTime When task started running
        %   (read-only)
        StartTime       = '';

        %FinishTime When task finished running
        %   (read-only)
        FinishTime      = '';

        %OutputArguments Output arguments from running Function on a worker
        %   When the task is in state 'finished', if no error occurred during
        %   task evaluation, OutputArguments contains the results of evaluating
        %   the task's Function.
        %   (read-only)
        OutputArguments = {};
    end

    % non-stored properties:
    properties ( Transient )
        %UserData Data associated with a task object
        %   UserData can be used to store any data associated with a task
        %   object. This data is stored in the client MATLAB session, and is
        %   not available on the workers. UserData is not stored in a profile.
        UserData = [];
    end

    properties ( Transient, SetAccess = immutable )
        %Parent Job containing this task
        Parent = [];
        %ID Task's numeric identifier
        ID     = -1;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Hidden/Public methods
    methods ( Hidden )
        function hSetProperty( obj, propName, val )
        % Tasks have no special coupled properties, so simply plumb through to
        % hSetPropertyNoCheck. (NB not need to validate scalar properties here as
        % higher-level APIs "set()" and property access will already have done
        % this)
            obj.hSetPropertyNoCheck( propName, val );
        end
        function hCheckTimingForSet( task, timing, propName )
            % Task defines the State, so it is in a position to check
            % the timing.
            parallel.internal.cluster.checkTimingForSet( task.StateEnum, ...
                timing, propName );
        end
        
        function [taskPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            % Create an empty map to hold displayable task properties
            taskPropertyMap = containers.Map('KeyType', 'char', 'ValueType', 'any');
            
            % The task base class is never displayed, so it does not
            % have a specified property display order to put into propNames
            propNames = {};
            
            % Add common task displayable items to map. Function is present in all task
            % displays, but must be gotten carefully for MJS Tasks, so it
            % is not added to the map in the base class.
            names = {...
                'ID', ...
                'State', ...
                'StartTime', ...
                'FinishTime', ...
                };
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            taskPropertyMap = [taskPropertyMap; containers.Map(names, values)];
            
            % Add non-default displayable items to map
            taskPropertyMap('Running Duration') = diFactory.createDurationItem(obj.hGetDisplayPropertiesNoError('StartTime'), obj.hGetDisplayPropertiesNoError('FinishTime'));
            taskPropertyMap('Parent') = diFactory.createLinkItem( sprintf('Job %d', obj.Parent.ID), obj.Parent );
            taskPropertyMap('Error') = diFactory.createTaskErrorItem(obj.hGetDisplayPropertiesNoError('Error'), obj.hGetDisplayPropertiesNoError('Parent'));
             % 'Separator' is not a typical name-value object. However, it
            % needs to be added to the propNames list in between property 
            % groups, so it is put into the map with the other 
            % displayable items.
            taskPropertyMap('Separator') = diFactory.createSeparator();
        end
    end
    
    methods ( Hidden, Sealed )
        
        function link = hGetLink(obj, displayValue)
            serializedMemento = serialize(parallel.internal.display.TaskMemento(obj));
            matlabFunction = sprintf('%s(''%s'')', ...
                parallel.internal.display.Displayer.DisplayObjectFunction, serializedMemento);
            link = parallel.internal.display.HTMLDisplayType(displayValue, matlabFunction);
        end
        
    end

    % Name-changed compatibility
    properties ( Hidden, Dependent )
        Id
    end
    methods
        function Id = get.Id( task )
            Id = task.ID;
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Protected/Abstract methods
    methods ( Access = protected )
        function obj = Task( parent, id )
        % Constructor simply sets immutable fields.
            obj.Parent = parent;
            obj.ID     = id;
        end
        function ok = doWait( task, varargin )
        % Default implementation of task.wait() - override if you have a more
        % efficient means.
            ok = parallel.internal.cluster.wait( task, varargin{:} );
        end
        function warnIfDiaryIncomplete( task )
            % We need to check the state of the object first to see if we need to warn
            state = task.StateEnum;
            if state == parallel.internal.types.States.Running && task.CaptureDiary
                s = warning( 'query', 'backtrace' );
                warning( 'backtrace', 'off' );
                backtrace = onCleanup( @() warning( s ) );
                if parallel.internal.apishared.BatchJobMethods.isBatchJob( task.Parent )
                    warning( message( 'parallel:batch:DiaryIncomplete' ) );
                else
                    warning( message( 'parallel:task:DiaryIncomplete' ) );
                end
            end
        end
    end

    methods ( Access = protected, Abstract )
        % Concrete subclasses must define how to destroy tasks.
        destroyOneTask( task );

        % Concrete subclasses must define how to cancel tasks.
        cancelOneTask( task, cancelException );

        % Return the worker that operated on this task
        getWorker( task );
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % protected static methods
    methods ( Access = protected, Static )
        function loadCheck( ctorNargin )
        %loadCheck if the constructor is being called with zero arguments,
        %assumption is that the task is being loaded. Therefore error.
            if ctorNargin == 0
                error( message( 'parallel:task:CannotLoadFromMatFile' ) );
            end
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Static, sealed protected methods
    methods (Access = protected, Static, Sealed)
        function obj = getDefaultScalarElement() %#ok<STOUT>
            % getDefaultScalarElement is required for matlab.mixin.Heterogeneous
            % to ensure that gaps in arrays are filled in with valid objects.
            error(message('parallel:cluster:ArrayWithGapsNotAllowed', 'parallel.Task'));
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public/documented methods.
    methods ( Sealed )
        function okout = wait( task, state, timeout )
        %WAIT Wait for task object to change state
        %    WAIT(T) blocks execution in the client session until the task
        %    identified by the task object T reaches the 'finished' state. This
        %    occurs when the task is finished processing on a remote worker.
        %
        %    WAIT(T, STATE) blocks execution in the client session until the
        %    task object T changes state to STATE. For a task object the
        %    valid states are 'running' and 'finished'.
        %
        %    OK = WAIT(T, STATE, TIMEOUT) blocks execution in the client session
        %    until the task object T changes state to STATE or until TIMEOUT
        %    seconds elapsed, whichever happens first. OK is TRUE if state has
        %    been reached or FALSE in case of a timeout.
        %
        %    If a task has previously been in state STATE, then wait will return
        %    immediately. For example, if a task in the 'finished' state is
        %    asked to WAIT(T, 'running'), then the call will return immediately.
        %
        %    Example:
        %    % Create a job object.
        %    myCluster = parcluster; % access default cluster
        %    j = createJob(myCluster);
        %    % Add a task object that generates a 10x10 random matrix.
        %    t = createTask(j, @rand, 1, {10,10});
        %    % Run the job.
        %    submit(j);
        %    % Wait until the task is finished.
        %    wait(t);
        %
        %    See also parallel.Job/Wait, parcluster.
            import parallel.internal.types.States;

            validateattributes( task, {'parallel.Task'}, {'scalar'}, ...
                'wait', 'task', 1 );

            if nargin < 2
                state = States.Finished;
            else
                try
                    state = States.fromName( state );
                catch E
                    ME = MException(message('parallel:cluster:FailedToInterpretState'));
                    ME = addCause( ME, E );
                    throw( ME );
                end
                okWaitStates = [ States.Running, States.Finished ];
                okWaitState  = any( state == okWaitStates );
                if ~okWaitState
                    okNames = arrayfun( @(x) x.Name, okWaitStates, 'UniformOutput', false );
                    okNamesStr = strtrim( sprintf( '%s ', okNames{:} ) );
                    error(message('parallel:cluster:InvalidStateToWait', state.Name, okNamesStr));
                end
            end

            if nargin < 3
                timeout = Inf;
            end

            try
                ok = task.doWait( state, timeout );
            catch E
                throw( E );
            end
            if nargout > 0
                okout = ok;
            end
        end

        function delete( taskOrTasks, varargin )
        %DELETE  Remove a task object from its job and memory
        %    DELETE(T) removes the task object, T, from the local session, and
        %    removes the task from the cluster's JobStorageLocation.  When the
        %    task is deleted, references to it become invalid. Invalid objects
        %    should be removed from the workspace with the CLEAR command. If
        %    multiple references to an object exist in the workspace, deleting
        %    one reference to that object invalidates the remaining references
        %    to it. These remaining references should be cleared from the
        %    workspace with the CLEAR command.
        %
        %    If t is an array of task objects and one of the objects cannot be
        %    deleted, the other objects in the array will be deleted and a
        %    warning will be returned.
        %
        %    Because its data is lost when you delete an object, delete should be
        %    used after output data has been retrieved from a task object.
        %
        %    myCluster = parcluster; % access the default cluster
        %    j = createJob(myCluster, 'Name', 'myJob');
        %    t = createTask(j, @rand, 1, {10});
        %    % Delete the task object.
        %    delete(t);
        %    clear t
        %
        %    See also parallel.Cluster/createJob, parallel.Job/delete, parcluster.

        % varargin is specified to ensure we get dispatched correctly; but,
        % we don't support any additional arguments other than the magic
        % string 'invalidateOnly' which allows us to invalidate the handle
        % but not destroy the underlying data.
            narginchk( 1, 2 );
            if nargin == 2
                if ~isequal( varargin{1}, 'invalidateOnly' )
                    error(message('parallel:task:InvalidDeleteSyntax'));
                end
                delete@parallel.internal.customattr.CustomGetSet( taskOrTasks );
                return
            end

            for ii = 1:numel( taskOrTasks )
                try
                    destroyOneTask( taskOrTasks(ii) );
                catch E
                    throw( E );
                end
                % Use the built-in delete method to invalidate the objects
                delete@parallel.internal.customattr.CustomGetSet( taskOrTasks(ii) );
            end
        end

        function cancel( taskOrTasks, optMessage )
        %CANCEL  Cancel a pending or running task
        %    CANCEL(T) stops the task object, T, that is currently in the pending
        %    or running state. The task's State property is set to finished, and
        %    no output arguments are returned. An error message stating that the
        %    task was canceled is placed in the task object's Error
        %    property.
        %
        %    CANCEL(T,'MESSAGE') cancels the task with an additional
        %    user-specified message. This message will be added to the default
        %    cancellation message. The cancellation message is used to build the
        %    task's 'Error' property.
        %
        %    Cancelling a single task in a job allows all other tasks in the
        %    same job to complete normally.
        %
        %    If the task is already in the finished state, no action is taken.
        %
        %    Example:
        %    % Create a task and later cancel it.
        %    myCluster = parcluster; % access the default cluster
        %    j  = createJob(myCluster);
        %    t  = createTask(j, @rand, 1, {3, 4});
        %    cancel(t);
        %
        %    See also parallel.Job/cancel

            import parallel.internal.cluster.CancelException
            if nargin < 2
                cancelException = CancelException.getDefaultForTask();
            else
                validateattributes( optMessage, {'char'}, {}, ...
                                    'cancel', 'message', 2 );
                cancelException = CancelException.getWithUserMessageForTask( optMessage );
            end
            try
                % unpick and call the scalar method
                arrayfun( @(t1) cancelOneTask(t1, cancelException), taskOrTasks );
            catch E
                throw( E );
            end
        end

        function listAutoAttachedFiles( task )
        %listAutoAttachedFiles List the files that are automatically attached for this task
        %   listAutoAttachedFiles(t) lists the files of the task object, t, that will be or
        %   have already been attached to this task's job, following a dependency analysis
        %   performed in the task function.
        %
        %   See also parallel.Job/listAutoAttachedFiles, parallel.Job/AttachedFiles,
        %   parallel.Job/AutoAttachFiles.
        
            job = task.Parent;
            if job.hShouldCalculateAttachedFiles()
                % Before job submission we always calculate the attached files, if AutoAttachFiles
                % is true.
                attachedFiles = task.hCalculateDependentFiles();
            else
                % Once the job is submitted the auto-attached files are cached on the task.
                % In that case we just list the cached files.
                attachedFiles = task.DependentFiles;
            end

            % Do the display of the calculated attached files.
            taskFcnStr = task.hGetUserTaskFunctionForDisplay();
            if isempty( attachedFiles )
                m = message( 'parallel:task:NoAutoAttachedFiles', taskFcnStr );
            else
                formattedFiles = parallel.internal.apishared.AttachedFiles.formatAttachedFilesForDisplay( attachedFiles );
                m = message( 'parallel:task:AutoAttachedFiles', taskFcnStr, formattedFiles );
            end
            disp( m.getString() );
        end
    end

    methods ( Sealed, Hidden )
        function hInvalidate( taskOrTasks )
        %hInvalidate Invalidate task objects without destroying underlying data.
            delete( taskOrTasks, 'invalidateOnly' );
        end

        function taskFcnStr = hGetUserTaskFunctionForDisplay( task )
            % hGetUserTaskFunctionForDisplay Get the representation of the task in a string
            % suitable for including in display.
            
            taskFcn = task.getUserTaskFunction();

            % Convert a function handle to a string.
            if isa( taskFcn, 'function_handle' )
                taskFcnStr = func2str( taskFcn );
            else
                taskFcnStr = taskFcn;
            end
        end

    end

    methods ( Hidden, Access = ?parallel.Job )
        function dependentFiles = hCalculateDependentFiles( task )
            taskFcn = task.getUserTaskFunction();
            if parallel.internal.apishared.BatchJobMethods.isBatchScript( task.Parent )
                workspace = task.InputArguments{2};
                taskFcn = parallel.internal.apishared.whatToAnalyze( taskFcn, workspace );
            end
            if isa( taskFcn, 'function_handle' )
                functionsToAnalyze = ...
                    parallel.internal.apishared.AttachedFiles.convertFunctionHandleForDependencyAnalysis( taskFcn );
            else
                functionsToAnalyze = taskFcn;
            end
            dependentFiles = ...
                parallel.internal.apishared.AttachedFiles.calculateAttachedFiles( functionsToAnalyze );
        end

        function attachedFiles = hCalculateAndCacheDependentFiles( task )
        % Helper to calculate the dependent files for the task and cache them on the task.
            attachedFiles = task.hCalculateDependentFiles();
            task.DependentFiles = attachedFiles; 
        end

    end

    methods ( Access = private )

        function taskFcn = getUserTaskFunction( task )
            % hGetUserTaskFunction Get the task function that the user supplied, including
            % batch jobs (where the actual task function is executeScript or executeFunction.)
            if parallel.internal.apishared.BatchJobMethods.isBatchJob( task.Parent )
                % If we've got a batch job the 1st input argument is the task function.
                taskFcn = task.InputArguments{1};
            else
                taskFcn = task.Function;
            end
        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % API1 compatibility layer
    properties ( Hidden, Dependent )
        CaptureCommandWindowOutput
    end
    methods
        function v = get.CaptureCommandWindowOutput( task )
            v = task.CaptureDiary;
        end
    end
    methods ( Hidden, Abstract )
        pPreTaskEvaluate( task )
    end
    methods ( Hidden )
        function varargout = pGetEvaluationData( task )
            varargout = task.hGetProperty( {...
                'Function', ...
                'NumOutputArguments', ...
                'InputArguments' } );
        end
    end
end
%#ok<*ATUNK> custom attributes
