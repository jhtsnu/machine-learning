%Worker Base class for all Worker objects
%   Worker objects contain information about the MATLAB worker which evaluated a
%   task. Worker objects are available on the client as task properties after
%   the task evaluation has completed.

% Copyright 2011-2012 The MathWorks, Inc.

classdef Worker < ...
        handle & ...
        parallel.internal.cluster.CustomPropDisp & ... % Use our display methods
        parallel.internal.customattr.CustomGetSet      % Use custom attributes

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Public read-only properties
    properties ( SetAccess = immutable )
        %Host Host where the worker process executed a task
        Host

        %ComputerType Type of computer on the worker
        %   This property has the value of the MATLAB function COMPUTER
        %   executed on the worker.
        ComputerType
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Protected constructor
    methods ( Access = protected )
        function obj = Worker( hostName, computerType )
            obj.Host         = hostName;
            obj.ComputerType = computerType;
        end
    end
    
    methods ( Access = private )
        function delete( obj )
            delete@handle( obj );
        end
    end
    methods (Hidden)
        function [workerPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            % Create an empty map to hold displayable worker properties
            workerPropertyMap = containers.Map('KeyType', 'char', 'ValueType', 'any');
            
            % The worker base class is never displayed, so it does not
            % have a specified property display order to put into propNames
            propNames = {};
            
            % Add common worker displayable items to map
            names = {'Host', 'ComputerType'};
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            workerPropertyMap = [workerPropertyMap; containers.Map(names, values)];
            
            % Add non-default displayable items to map
            % 'Separator' is not a typical name-value object. However, it
            % needs to be added to the propNames list in between property 
            % groups, so it is put into the map with the other 
            % displayable items.
            workerPropertyMap('Separator') = diFactory.createSeparator();
        end
    end
end
