function [allNames, defaultName] = clusterProfiles
%parallel.clusterProfiles Return the name of all valid parallel computing cluster profiles
%   ALLPROFILES = parallel.clusterProfiles returns a cell array
%   containing the names of all available profiles.
%
%   [ALLPROFILES, DEFAULTPROFILE] = parallel.clusterProfiles returns a 
%   cell array containing the names of all available profiles, and 
%   separately the name of the default profile.
%
%   The cell array ALLPROFILES always contains a profile called 'local'
%   for the local cluster.  The default profile returned by
%   parallel.clusterProfiles is guaranteed to be found in ALLPROFILES.
%
%   If the default profile has been deleted, or if it has never been
%   set, parallel.clusterProfiles returns 'local' as the default
%   profile.
%
%   Profiles can be created and changed using the saveProfile or 
%   saveAsProfile methods on a cluster object.  Profiles can be created, 
%   deleted, and changed using  the Cluster Profiles Manager.  On the Home
%   tab, in the Environment section, click Parallel > Manage Cluster
%   Profiles... to open the Cluster Profiles Manager.
%
%   Examples: 
%   Display the names of all the available profiles and set the first in the
%   list to be the default.
%      allNames = parallel.clusterProfiles()
%      parallel.defaultClusterProfile(allNames{1});
%
%   Display the names of all the available profiles and get the cluster 
%   identified by the last in the list.
%      allNames = parallel.clusterProfiles()
%      myCluster = parcluster(allNames{end});
%
%   See also MATLABPOOL, BATCH, PARCLUSTER, parallel.defaultClusterProfile,
%   parallel.Cluster.saveProfile, parallel.Cluster.saveAsProfile.

%  Copyright 2011-2012 The MathWorks, Inc.

try
    p = parallel.Settings;
    allNames = p.hGetAllProfileNames();
    defaultName = p.DefaultProfile;
catch err
    throw(err);
end