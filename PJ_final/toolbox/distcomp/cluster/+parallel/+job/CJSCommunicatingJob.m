% CJSCommunicatingJob Communicating job for CJS clusters
%   A CJSCommunicatingJob object is a type of parallel.CommunicatingJob
%   that is created by calling createCommunicatingJob(cluster) or findJob(cluster) 
%   on a cluster of type Local, LSF, Generic, HPCServer, Mpiexec, 
%   PBSPro, or Torque.
%
%   parallel.job.CJSCommunicatingJob methods:
%      cancel                - Cancel a pending, queued, or running job
%      createTask            - Create a new task in a job
%      delete                - Remove a job object from its cluster and memory
%      diary                 - Display or save text of batch job
%      fetchOutputs          - Retrieve output arguments from all tasks in a job
%      findTask              - Find task objects belonging to a job
%      listAutoAttachedFiles - List the files that are automatically attached to the job.
%      load                  - Load workspace variables from batch job
%      submit                - Submit job for execution in the cluster
%      wait                  - Wait for job execution to change state
%
%   parallel.job.CJSCommunicatingJob properties:
%      AdditionalPaths - Folders to add to MATLAB search path of workers
%      AttachedFiles   - Files and folders that are sent to the workers
%      AutoAttachFiles - Determines whether dependent files will be automatically sent to the workers
%      CreateTime      - Time at which this job was created
%      FinishTime      - Time at which this job finished running
%      ID              - Job's numeric identifier
%      JobData         - Data made available to all workers for job's tasks
%      Name            - Name of this job
%      NumWorkersRange - Specify limits for number of workers
%      Parent          - Cluster object containing this job
%      StartTime       - Time at which this job started running
%      State           - State of the job: pending, queued, running, finished, or failed
%      SubmitTime      - Time at which this job was submitted
%      Tag             - Label associated with this job
%      Tasks           - Array of task objects contained in this job
%      Type            - Job's type: Independent, Pool, or SPMD
%      UserData        - Data associated with a job object
%      Username        - Name of the user who created this job
%
%   See also parallel.CommunicatingJob, parallel.Job, 
%            parallel.Cluster/createCommunicatingJob, parallel.Cluster/findJob.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true ) CJSCommunicatingJob < parallel.CommunicatingJob & ...
        parallel.internal.cluster.CJSJobMixin

    properties ( PCTGetSet, Transient, Hidden )
        JobSchedulerData
    end

    methods ( Hidden )
        function obj = CJSCommunicatingJob( parent, jobid, pvar, jobsid, support )
            parallel.Job.loadCheck( nargin );
            %{
            validateattributes( parent, {'parallel.cluster.CJSCluster'}, ...
                                {'scalar'} );
            validateattributes( support, {'parallel.internal.cluster.CJSSupport' }, ...
                                {'scalar'} );
            %}
            obj@parallel.CommunicatingJob( parent, jobid, pvar );
            obj@parallel.internal.cluster.CJSJobMixin( support, jobsid );
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % abstract job methods
    methods ( Access = protected )
        function t = createTaskOnOneJob( job, varargin )
            import parallel.internal.cluster.CJSJobMethods

            if isempty( job.Tasks )
                allowMultipleTasks = false;
                t = CJSJobMethods.createTask( job, job.Support, job.SupportID, ...
                                              allowMultipleTasks, varargin{:} );
            else
                error(message('parallel:job:CommunicatingJobOneTask'));
            end
        end
        function submitOneJob( job )
            parallel.internal.cluster.SubmissionChecks.warnIfPoolOpenOnThisCluster( ...
                job.Parent, job.NumWorkersRange(1) );
            job.Parent.hSubmitCommunicatingJob( job, job.Support, job.SupportID );
        end

        function destroyOneJob( job )
            import parallel.internal.cluster.CJSJobMethods;
            CJSJobMethods.destroyOneJob( job.Parent, job, ...
                                         job.Support, job.SupportID );
        end
        function cancelOneJob( job, cancelException )
            import parallel.internal.cluster.CJSJobMethods;
            CJSJobMethods.cancelOneJob( job.Parent, job, ...
                                        cancelException );
        end
        function tl = getTasks( obj )
            tl = obj.Support.getTasks( obj, obj.SupportID );
        end
        function s = getStateEnum( job )
            s = job.Parent.hGetJobState( job, job.hGetProperty( 'StateEnum' ) );
        end
        function varargout = getTasksByState( obj )
            [varargout{1:nargout}] = obj.getTasksByStateImpl();
        end
        function attachFileDataToJob( obj, filedata )
            obj.Support.attachFileDataToJob( obj.SupportID, obj.Variant, filedata );
        end
    end

    methods ( Access = private )
        function trimExcessTasks( job )
        % Called prior to execution on lab 1 to remove any excess tasks that
        % might have been created.
            try
                tasks = job.Tasks;
                for ii = numlabs+1:numel(tasks)
                    % We cannot use the full 'delete' here, as this causes the
                    % task (and hence job) to be cancelled. So, we use a secret
                    % method to rid ourselves of the troublesome tasks.
                    hRemoveOneExcessTask( tasks(ii) );
                end
            catch E %#ok<NASGU> swallow errors in this method
            end
        end
        function tf = canModifyJobOnWorker( ~ )
        % On the worker only, can we modify the job?
            tf = ( labindex == 1 );
        end
    end

    % API1 compatibility
    methods ( Hidden )
        function pPreJobEvaluate( job, task )
            canModifyJob  = ( labindex == 1 );
            idForRandInit = labindex;
            if canModifyJob
                % The following line of code is not permitted to throw:
                job.trimExcessTasks();
            end
            doBarrier = true;
            preCJSJob( job, canModifyJob, idForRandInit, doBarrier );
            job.Parent.hPreJobEvaluate( job, task );
        end
        function pPostJobEvaluate( job )
            import parallel.internal.types.Variant
            task           = getCurrentTask();
            taskHasError   = ~isempty( task.Error );
            doExtraBarrier = (job.Variant == Variant.CommunicatingPoolJob);
            iPostJobEvaluate( job, taskHasError, ...
                              job.PoolShutdownSuccessful, doExtraBarrier );
        end
    end

end
%#ok<*ATUNK> custom attributes

% --------------------------------------------------------------------------
% iPostJobEvaluate - a close mimic of simpleparalleljob/pPostJobEvaluate
function iPostJobEvaluate( job, taskHasError, ...
                           poolShutdownSuccessful, doExtraBarrier )
    if taskHasError || ~ poolShutdownSuccessful
        iAbortJob( job );
    end
    if doExtraBarrier
        labBarrier();
    end

    % Get the parent of this job and make sure it's a CJSCluster
    cluster = job.Parent;
    if ~isa( cluster, 'parallel.cluster.CJSCluster' )
        return
    end

    % Ensure all tasks get here, with error detection
    mpigateway( 'setidle' );
    mpigateway( 'setrunning' );
    labBarrier;

    % Decide now whether this task should modify the job state, because after
    % we've called mpiParallelSessionEnding, we wont be able to tell (all
    % tasks will then think that they have labindex==1)
    shouldSetJobState = (cluster.HasSharedFilesystem && ...
                         job.canModifyJobOnWorker());

    % This is the end of a parallel session
    mpiParallelSessionEnding;
    mpiFinalize;

    % Now that all tasks are here, we can define the job to be finished. (This is
    % different to how a standard job behaves)
    if shouldSetJobState
        dctSchedulerMessage(4, 'About to set job state to finished');
        iFinishJob( job );
    else
        % If we aren't setting the job state then we may want to wait until
        % the task that is setting it has done so. Particularly this fixes an
        % issue on the mac with mpiexec not working correctly. To decide if
        % we want to go down this path ensure that the MDCS_MPIEXEC_WAIT_FOR_JOB_FINISHED
        % environment variable is set to the value 'true' and then we will
        % wait. We will only wait for 30s for this transition to happen.
        if strcmpi( getenv('MDCS_MPIEXEC_WAIT_FOR_JOB_FINISHED'), 'true' )
            dctSchedulerMessage(4, 'About to wait for job to be finished before proceeding');
            job.wait( 'finished', 30 );
            dctSchedulerMessage(4, 'Finished waiting');
        end
    end
end

% --------------------------------------------------------------------------
% iFinishJob - set job.State and job.FinishTime
function iFinishJob( job )
    dctSchedulerMessage(4, 'Writing job FinishTime and State');
    job.hSetProperty( {'FinishTime', 'StateEnum'}, ...
                      { char(java.util.Date), ...
                        parallel.internal.types.States.Finished } );
end

% --------------------------------------------------------------------------
% iAbortJob - abort execution of a job
function iAbortJob( job )
    import parallel.internal.types.States

    jobSupport = job.Support;
    % This is a race for all the workers to try and put the job state
    % and finish time in the job file - so we need to lock the serializer
    % beforehand
    aLock = jobSupport.lock( job.SupportID, job.Variant );
    if job.hGetProperty( 'StateEnum' ) ~= States.Finished
        % An error occurred - abort!
        iFinishJob( job );
    end
    jobSupport.release( aLock );
    % This quits MATLAB hard on all the workers, providing workers were started
    % using mpiexec.
    dctSchedulerMessage( 0, 'About to invoke MPI_Abort' );
    mpigateway( 'abort' );
end
