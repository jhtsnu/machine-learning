% MJSCommunicatingJob Communicating job for MJS clusters
%
%   parallel.job.MJSCommunicatingJob methods:
%      cancel                - Cancel a pending, queued, or running job
%      createTask            - Create a new task in a job
%      delete                - Remove a job object from its cluster and memory
%      diary                 - Display or save text of batch job
%      fetchOutputs          - Retrieve output arguments from all tasks in a job
%      findTask              - Find task objects belonging to a job
%      listAutoAttachedFiles - List the files that are automatically attached to the job.
%      load                  - Load workspace variables from batch job
%      submit                - Submit job for execution in the cluster
%      wait                  - Wait for job execution to change state
%
%   parallel.job.MJSCommunicatingJob properties:
%      AdditionalPaths - Folders to add to MATLAB search path of workers
%      AttachedFiles   - Files and folders that are sent to the workers
%      AuthorizedUsers - Users authorized to access job
%      AutoAttachFiles - Determines whether dependent files will be automatically sent to the workers
%      CreateTime      - Time at which this job was created
%      FinishTime      - Time at which this job finished running
%      FinishedFcn     - Callback executed when the job or task finishes
%      ID              - Job's numeric identifier
%      JobData         - Data made available to all workers for job's tasks
%      Name            - Name of this job
%      NumWorkersRange - Specify limits for number of workers
%      Parent          - Cluster object containing this job
%      QueuedFcn       - Callback executed when the job is queued
%      RestartWorker   - True if MATLAB workers are restarted before evaluating the job's tasks
%      RunningFcn      - Callback executed when the job or task starts running
%      StartTime       - Time at which this job started running
%      State           - State of the job: pending, queued, running, finished, or failed
%      SubmitTime      - Time at which this job was submitted
%      Tag             - Label associated with this job
%      Tasks           - Array of task objects contained in this job
%      Timeout         - Time limit to complete job
%      Type            - Job's type: Independent, Pool, or SPMD
%      UserData        - Data associated with a job object
%      Username        - Name of the user who created this job

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true ) MJSCommunicatingJob < parallel.CommunicatingJob & ...
                             parallel.internal.cluster.MJSQueuedCallbackMixin & ...
                             parallel.internal.cluster.MJSRunningCallbackMixin & ...
                             parallel.internal.cluster.MJSFinishedCallbackMixin

    properties (Transient, SetAccess = immutable, GetAccess = private)
        Support
        SupportID
    end

    properties ( PCTGetSet, Transient, PCTConstraint = 'logicalscalar|presubmission' )
        %RestartWorker True if MATLAB workers are restarted before evaluating the job's tasks        
        %   In some cases, you might want to restart MATLAB on the workers
        %   before they evaluate any tasks in a job. This action resets
        %   defaults, clears the workspace, frees available memory, and so
        %   on.
        %
        %   Set RestartWorker to true if you want the job to restart the
        %   MATLAB session on any workers before they evaluate their first
        %   task for that job. The workers are not reset between tasks of
        %   the same job. Set RestartWorker to false if you do not want
        %   MATLAB restarted on any workers. The default value is false,
        %   which does not restart the workers.
        %
        %   See also parallel.job.MJSCommunicatingJob.submit
        RestartWorker
    end

    properties ( PCTGetSet, Transient, PCTConstraint = 'positivescalar|presubmission' )
        %Timeout Time limit to complete job        
        %   Timeout holds a double value specifying the number of seconds
        %   to wait before giving up on a job. The time for timeout begins
        %   counting when the job state property value changes from queued
        %   to running. When a job times out, the behavior of the job is
        %   the same as if the job were stopped using the cancel function,
        %   except all pending and running tasks are treated as having
        %   timed out.
        %
        %   The default value for Timeout is large enough so that in
        %   practice, jobs will never time out. You should set the value of
        %   Timeout to the number of seconds you want to allow for
        %   completion of jobs.
        %
        %   See also parallel.job.MJSCommunicatingJob.State,
        %            parallel.job.MJSCommunicatingJob.submit.
        Timeout
    end
    
    properties ( PCTGetSet, Transient, PCTConstraint = 'cellstr' )
        %AuthorizedUsers Users authorized to access job
        %   The AuthorizedUsers property value is a cell array of strings
        %   which identify the users who are allowed to access the job.
        %   This controls who can set properties on the job, add tasks,
        %   destroy the job, etc. The person identified as the owner by the
        %   job's Username property does not have to be listed in the
        %   AuthorizedUsers property value.
        %
        %   The effect of AuthorizedUsers at different security levels is:
        %   0       - No effect. All users can access the job without
        %             hindrance.
        %   1       - For users included in the property value, the system
        %             suppresses the dialog box that requires
        %             acknowledgment that the job belongs to another user.
        %             All other users must acknowledge job ownership every
        %             time they access the job.
        %   2 and 3 - Only users who are authenticated in this session and
        %             are listed in AuthorizedUsers can access the job.
        %
        %   See also parallel.job.MJSCommunicatingJob.Username,
        %            parallel.cluster.MJS.SecurityLevel.
        AuthorizedUsers
    end
    
    properties (Constant, Access = private)
        StateEnumToEventName = iBuildStateEnumToEventMap();
    end

    methods ( Hidden )
        function obj = MJSCommunicatingJob( parent, jobID, pvar, supportID, support )
            parallel.Job.loadCheck( nargin );
            obj@parallel.CommunicatingJob( parent, jobID, pvar );

            obj.Support   = support;
            obj.SupportID = supportID;
        end
        function hSetPropertyNoCheck( obj, propName, val )
            try
                obj.Support.setJobProperties( obj.SupportID, propName, val );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function v = hGetProperty( obj, propName )
            try
                v = obj.Support.getJobProperties( obj.SupportID, propName );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function t = hBuildChild( obj, tid, uuid )
            try
                t = parallel.task.MJSTask( obj, tid, uuid, obj.Support, obj.SupportID );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function sid = hGetSupportID( obj )
            sid = obj.SupportID;
        end        
        function hPromote( obj )
            obj.Support.promoteJob( obj.SupportID );
        end        
        function hDemote( obj )
            obj.Support.demoteJob( obj.SupportID );
        end
    end

    % MJSCallbackMixin methods
    methods ( Access = protected )
        function doRegisterForEvents( obj )
            obj.Support.registerForJobEvents( obj.Parent, obj.SupportID );
            obj.enableAutoEventUnregistration();
        end
        function doUnregisterForEvents( obj )
            obj.Support.unregisterForJobEvents( obj.Parent, obj.SupportID );
            obj.disableAutoEventUnregistration();
        end
        function doUnregisterForEventsAfterDestroyed( obj )
            obj.Support.unstashCluster( obj.Parent )
        end
    end

    methods( Hidden )
        function hFireStateEvent( obj, stateEnum )
            notify( obj, obj.StateEnumToEventName(stateEnum.Name) );
        end
    end

    methods ( Hidden )
        function hMpiInit( job, task )
            iMpiInit( job, task );
        end
        function hMpiCleanup( ~ )
        % Perform error detection
        % c.f.: @distcomp/@paralleljob/pMpiCleanup.m
            mpigateway( 'setidle' );
            mpigateway( 'setrunning' );

            % Call to labBarrier ensures that workers will get stuck in here until
            % everyone has finished. This is actually desirable because if we don't
            % ensure that all workers clean up correctly, then there is a
            % possibility that a worker could complete a task, and not get cleaned up
            labBarrier;

            % This is the end of a parallel session
            mpiParallelSessionEnding;

            mpiFinalize;
        end
    end

    methods ( Access = protected )
        function t = createTaskOnOneJob( job, varargin )
            import parallel.internal.cluster.MJSJobMethods

            if isempty( job.Tasks )
                try
                    allowMultipleTasks = false;
                    t = MJSJobMethods.createTask( ...
                        job, job.Support, job.SupportID, ...
                        allowMultipleTasks, varargin{:} );
                catch E
                    throw( distcomp.handleJavaException( job, E ) );
                end
            else
                error(message('parallel:job:CommunicatingJobOneTask'));
            end
        end

        function submitOneJob( job )
            parallel.internal.cluster.SubmissionChecks.warnIfPoolOpenOnThisCluster( ...
                job.Parent, job.NumWorkersRange(1) );
            try
                job.Support.submitJob( job, job.SupportID );
            catch E
                throw( distcomp.handleJavaException( job, E ) );
            end
        end

        function destroyOneJob( job )
        % job is scalar.
            try
                job.Support.destroyOneJob( job.SupportID );
                job.unregisterForEventsAfterDestroyed();
            catch E
                throw( distcomp.handleJavaException( job, E ) );
            end
        end

        function cancelOneJob( job, cancelException )
        % job is scalar.
            try
                job.Support.cancelOneJob( job.SupportID, cancelException );
            catch E
                throw( distcomp.handleJavaException( job, E ) );
            end
        end
        function tl = getTasks( job )
            try
                tl = job.Support.getTasks( job, job.SupportID );
            catch E
                throw( distcomp.handleJavaException( job, E ) );
            end
        end
        function s = getStateEnum( job )
            s = job.hGetProperty( 'StateEnum' );
        end

        function ok = doWait( job, stateEnum, timeout )
            waitRequiredFcn = iCreateStateComparisonFcn( stateEnum );
            ok = job.waitForEvent( job.StateEnumToEventName(stateEnum.Name), timeout, waitRequiredFcn );
        end
        function attachFileDataToJob( job, filedata )
            try
                job.Support.setJobProperties( job.SupportID, 'AttachedFileData', filedata );
            catch E
                throw( distcomp.handleJavaException( job, E ) );
            end
        end
    end

    % API1 compatibility
    methods ( Hidden )
        function pPreJobEvaluate( job, task )
            preMJSCommunicatingJob( job, task );
        end

        function pPostJobEvaluate( job )
            import parallel.internal.types.Variant
            import com.mathworks.toolbox.distcomp.nativedmatlab.ProcessManipulation

            task        = getCurrentTask();
            taskSuccess = isempty( task.Error );
            if ~( taskSuccess && job.PoolShutdownSuccessful )
                % This quits MATLAB hard
                % NB: never get here for 'SPMD' jobs
                ProcessManipulation.abortMATLAB();
            end
            job.hMpiCleanup();
        end

        function info = pGetDataStoreInfo( job )
            info = job.Support.getJobDataStoreInfo;
        end
    end
end
%#ok<*ATUNK> custom attributes

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% c.f.: @distcomp/@paralleljob/pMpiInit.m
function iMpiInit( job, task )

% Force the "sock" option, as this is the only one that will work under the
% jobmanager.
    mpiInit( 'sock' );
    dctSchedulerMessage(4, 'mpiInit(''sock'') completed ok');

    % This is a new parallel session
    mpiParallelSessionStarting;

    % "Leading task" is assigned by the JM during dispatch of the job.
    % Find out if this task is the leading task.
    taskSId = task.hGetSupportID();
    isleadingtask = job.Support.isLeadingTask( job.SupportID, taskSId );

    if isleadingtask
        USING_LINEAR_CONN   = strcmpi(getenv('MDCE_CONNECTACCEPT'), 'linear');
        USING_BIN_TREE_CONN = ~USING_LINEAR_CONN;

        if USING_LINEAR_CONN
            % Open the port and set the parallel tag
            port = mpigateway( 'openport' );
            connectMethod = 'linear';
        elseif USING_BIN_TREE_CONN
            port = 'no port';
            connectMethod = 'binary';
        end
        % Set the parallel tag
        job.hSetPropertyNoCheck( 'ParallelTag', ...
                                 sprintf( '%s\n%s\n%s', connectMethod, port, computer() ) );
        dctSchedulerMessage(4, 'Leading task parallel tag ... method : %s, port : %s, comp : %s', connectMethod, port, computer());
    end

    % All wait until the jobmanager has dispatched all the tasks - so that we
    % know how many there are in the parallel job.

    % Extract parallel job setup info
    pinfo = job.hGetProperty( 'ParallelInfo' );
    % Wait until setup info is retrieved from the job manager.
    while isempty( pinfo )
        pause( 0.5 ); % A shortish wait
        pinfo = job.hGetProperty( 'ParallelInfo' );
    end

    try
        [connectMethod, port, comp] = iDecodeParallelTag( char(pinfo.getParallelTag) );
        dctSchedulerMessage(4, 'parallel tag decode: method : %s, port : %s, comp : %s', connectMethod, port, comp);
    catch err
        dctSchedulerMessage(4, 'Error in iDecodeParallelTag:\n %s', err.getReport() );
        rethrow(err);
    end
    if ~isleadingtask
        iCompatCheck( comp );
    end
    % Based on the parallel tag we decide if we are doing a binary or linear
    % connection method.
    if strcmpi(connectMethod, 'binary')
        iBinaryTreeConnectAccept(job, task, pinfo);
    elseif strcmpi(connectMethod, 'linear')
        iLinearConnectAccept(job, task, pinfo, port, isleadingtask);
    else
        error(message('parallel:job:MpiUnknownConnectionMethod', connectMethod));
    end
end
% -------------------------------------------------------------------------
function iBinaryTreeConnectAccept(job, task, pinfo)
    dctSchedulerMessage(4, 'Doing a binary tree connection');

    N = pinfo.getNumLabs;
    dctSchedulerMessage(4, 'Got parallel info with num labs: %d', N);

    % Create a CancelWatchdog to make sure it doesn't take us too long to
    % connect/accept. Choose a timeout based on the expected number of labs.
    timeout = max( 120, 4 * N );
    cw = job.Support.buildCancelWatchdog( ...
        job.SupportID, 'Timeout exceeded during parallel connection', timeout );

    dctSchedulerMessage(4, 'MPI connect watchdog started with timeout %d seconds', timeout);
    try
        % Get my ID
        ID = task.Id;

        thisW = task.hGetWorkerProxy();
        jobToken = thisW.getCurrentJobAndTask();

        % Now get the worker to my left
        if ID > 1
            prevW = iPollForWorkerOnTask( job.Tasks(ID - 1) );
        else
            prevW = [];
        end

        iJoinRing( ID, N, prevW, thisW, jobToken );
        cw.ok();
    catch err
        dctSchedulerMessage(2, 'Caught error during iBinaryTreeConnectAccept. Report is:\n%s', err.getReport() );
        cw.ok();
        rethrow(err)
    end
end

% --------------------------------------------------------------------------
function iJoinRing(myID, N, prevW, thisW, jobToken )
% This function joins an MPI ring together using a binary tree approach.
% The only external communication needed is to the worker with an ID one
% less than our ID (i.e. lab N only needs to join in to the ring that lab
% N-1 is part of). A pictorial version of the join is as follows ...
%
%    1   2   3   4   5   6   7   8   9   10   11   12   13
% 1.  <->     <->     <->     <->     <->       <->
% 2.      <->             <->              <->
% 3.              <->                                <->
% 4.                              <->
%
% The algorithm is as follows. Firstly transform the one-based myID of the
% task to be zero-based ID for convenience. Also compute how many rounds of
% joining will occur ( ceil(log2(N)) ). Then for a given round (i) of
% connection an ID will CONNECT if the i'th bit in the ID is set. Hence the
% iConnectMask. An ID will ACCEPT if it is not CONNECTING and there is
% someone to connect to it (which can be deduced from the total number of
% IDs trying to join). Hence iAcceptMask and shouldAccept.
%
% It is possible that during a round of communication a given ID is neither
% ACCEPTING or CONNECTING. It is NOT possible to CONNECT and ACCEPT in a
% round. Hence the ~shouldConnect in the definition of shouldAccept.
%
% Once a given ID in a given round has decided that it will be ACCEPTING or
% CONNECTING it needs to decide if it is LEADER in this round. Given that
% after a round of communication we have a working MPI communicator this
% question is relatively simple. We are a CONNECTION LEADER if our labindex
% is 1 and an ACCEPTING LEADER if our labindex is numlabs.
%
% If we are an ACCEPTING LEADER then we create an MPI port and tell all the
% labs in our group what it is. They all then call accept on that port,
% whilst we post the port to our worker (setParallelPortInfo) and then also
% accept.
%
% If we are a CONNECTING LEADER then we wait for the worker with ID-1 to
% post the parallelPortInfo (see WorkerImpl for the wait code), and when we
% have received it we broadcast it around to our current peers and then
% connect.
%
% At the end of the accept/connect round we have a new MPI communicator for
% which is the size of the 2 that joined together, with the ACCEPTORS being
% the low indices and the CONNECTORS the high indices.
%
% NOTE (VERY IMPORTANT) - do not change this algorithm to one where we
% first find out the port to our (ID-1) and then go into a connect/accept
% phase. There is a bug in MPICH2 which manifests as a SEG-V - the trigger
% is the following:
%   1. Thus process opens an MPI port to accept on
%   2. Another pair of processes try to connect to this port (jointly)
%   3. This process then tries to connect to a single process
%   4. This process SEG-V's
%
% The above represents a race-condition on the binary tree connection if
% all processes know their (ID-1) port in advance. However by introducing
% the worker synchronization into the algorithm below we happen to eliminate
% the above race condition, since once you know you are an ACCEPTOR you are
% all NOT connecting, and you can't get the connection info until ALL
% ACCEPTORS are ready.

% Make the IDs zero-based for simplicity
    ID = myID - 1;

    % How many connect / accept rounds are going to be needed
    nRounds = ceil(log2(N));
    dctSchedulerMessage(5, 'Expecting %d rounds of connect/accept', nRounds);

    for i = 1:nRounds
        % Are we connecting or accepting this round?
        iConnectMask = bitset(0, i);
        iAcceptMask  = iConnectMask - 1;
        shouldConnect = logical(bitand(ID, iConnectMask));
        % If I am not connecting then compute the (zero based) ID of the worker
        % that would be connecting to me - this is the OR of my ID and
        % iAcceptMask (which is the ID of the worker that is root ACCEPTING)
        % plus 1 - giving the ID of the worker CONNECTING
        myRootConnectingID = bitor(ID, iAcceptMask) + 1;
        % I should accept if I am NOT CONNECTING and there are workers that
        % should connect. '>' is the correct test here (EE & JM agree!) because
        % an ID can only exist if it is strictly < N; so N must be strictly '>'
        % an ID for that ID to exist.
        shouldAccept  = ~shouldConnect && N > myRootConnectingID;

        dctSchedulerMessage(5, 'In round %d (A:%d,C:%d)... \n', i, shouldAccept, shouldConnect);
        % Define once since it is used as a dummy input to labBroadcast in
        portToUse = '';

        if shouldConnect
            % During the connect phase the lab with index 1 will find the port
            % to connect to by looking for the parallel port info from the
            % worker with ID-1
            leaderIndex = 1;
            isLeader = labindex == leaderIndex;
            if isLeader
                dctSchedulerMessage(5, 'Connect leader:\n  %s', char(jobToken));
                portToUse = iPollForParallelPortInfo( prevW, jobToken );
                dctSchedulerMessage(5, 'Received parallel port info:\n  %s', portToUse);
            end

            dctSchedulerMessage(5, 'About to labBroadcast ... ');
            portToUse = labBroadcast(leaderIndex, portToUse);

            dctSchedulerMessage(5, 'About to clientconnone on:\n  %s', portToUse);
            mpigateway( 'clientconnone', portToUse, leaderIndex );
        end

        if shouldAccept
            leaderIndex = numlabs;
            isLeader = labindex == leaderIndex;
            if isLeader
                portToUse = mpigateway('openport');
                dctSchedulerMessage(5, 'Accept leader : opened port:\n  %s\n  %s', portToUse, char(jobToken));
            end

            dctSchedulerMessage(4, 'About to labBroadcast ... ');
            portToUse = labBroadcast(leaderIndex, portToUse);

            if isLeader
                % It is IMPORTANT to set this AFTER the broadcast above. This
                % ensures that all ACCEPTORS are NOT connecting at the moment.
                thisW.setParallelPortInfo( jobToken, portToUse );
            end

            dctSchedulerMessage(5, 'About to servacceptone on:\n  %s', portToUse);
            mpigateway( 'servacceptone', portToUse, leaderIndex );
        end

    end
    dctSchedulerMessage(5, 'Finished %d rounds of connect/accept', nRounds);

end

% --------------------------------------------------------------------------
function worker = iPollForWorkerOnTask(task)
    while isempty(task.Worker)
        pause(0.5);
    end
    worker = task.hGetWorkerProxy();
end

% --------------------------------------------------------------------------
function port = iPollForParallelPortInfo(worker, jobToken)
    port = '';
    while isempty(port)
        % This function times out after 10s - ensure
        port = char(worker.waitforParallelPortInfo( ...
            jobToken, 10, java.util.concurrent.TimeUnit.SECONDS ));
        if isempty(port)
            dctSchedulerMessage(5, 'Still waiting for port info on (%s) ...', char(jobToken) );
        end
    end

end
% -------------------------------------------------------------------------
function iLinearConnectAccept(job, task, pinfo, port, isleadingtask)
    dctSchedulerMessage(4, 'Doing a linear connection');
    % Everyone needs to know the expected world size and the parallel tag.
    N = pinfo.getNumLabs;

    % Create a CancelWatchdog to make sure it doesn't take us too long to
    % connect/accept. Choose a timeout based on the expected number of labs.
    timeout = max( 120, 4 * N );
    cw = job.Support.buildCancelWatchdog( ...
        job.SupportID, 'Timeout exceeded during parallel connection', timeout );

    err = [];
    try
        if isleadingtask
            % accept the other connections - with timeout
            mpigateway( 'servaccept', port, N-1 );
        else
            % Connect up
            mpigateway( 'clientconn', port, N-1 );
        end
    catch exception
        err = exception;
    end
    % We're OK - so call off the watchdog.
    cw.ok();

    % If connect/accept threw an error, rethrow that.
    if ~isempty( err )
        rethrow( err );
    end

    % Set the labindex as desired
    tasksByIndex = gcat( task.Id );
    [junk, desOrder] = sort( tasksByIndex ); %#ok
    mpigateway( 'desiredlabordering', desOrder );
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iCompatCheck - check compatibility of computers. The criteria are that the
% word sizes and endianness must match.
function iCompatCheck( comp )

% We should never get a badly formatted tag, since the job is locked when
% being modified.
    if isempty( comp )
        error(message('parallel:job:MpiInitBadTag'));
    end

    import com.mathworks.toolbox.distcomp.pml.LabCompatibilityChecker;

    mycomp = computer;
    compatible = LabCompatibilityChecker.instance.areComputersCompatible(comp, mycomp);

    if ~compatible
        error(message('parallel:job:MpiCompatibilityCheckFailed', mycomp, comp));
    end
end

function [connectMethod, port, comp] = iDecodeParallelTag( ptag )
% Check that the this computer is compatible with the computer which opened
% the port
    strs = regexp(ptag, '\n+', 'split');
    [connectMethod, port, comp] = deal(strs{:});
end

function m = iBuildStateEnumToEventMap()
    import parallel.internal.types.States;
    m = containers.Map;
    m(States.Queued.Name)   = parallel.internal.cluster.MJSQueuedCallbackMixin.QueuedEventName;
    m(States.Running.Name)  = parallel.internal.cluster.MJSRunningCallbackMixin.RunningEventName;
    m(States.Finished.Name) = parallel.internal.cluster.MJSFinishedCallbackMixin.FinishedEventName;
end

function fcn = iCreateStateComparisonFcn( stateEnum )
% Returns a function that will check the state of the job when called.
% Created here to prevent the job object being sent as part of the
% workspace.
    fcn = @( obj ) obj.StateEnum < stateEnum;
end
