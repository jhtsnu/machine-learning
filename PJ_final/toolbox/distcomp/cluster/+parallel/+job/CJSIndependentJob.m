% CJSIndependentJob Independent job for CJS clusters
%   A CJSIndependentJob object is a type of parallel.IndependentJob that is
%   created by calling createJob(cluster) or findJob(cluster) on a cluster
%   of type Local, LSF, Generic, HPCServer, Mpiexec, PBSPro, or Torque.
%
%   parallel.job.CJSIndependentJob methods:
%      cancel                - Cancel a pending, queued, or running job
%      createTask            - Create a new task in a job
%      delete                - Remove a job object from its cluster and memory
%      diary                 - Display or save text of batch job
%      fetchOutputs          - Retrieve output arguments from all tasks in a job
%      findTask              - Find task objects belonging to a job
%      listAutoAttachedFiles - List the files that are automatically attached to the job.
%      load                  - Load workspace variables from batch job
%      submit                - Submit job for execution in the cluster
%      wait                  - Wait for job execution to change state
%
%   parallel.job.CJSIndependentJob properties:
%      AdditionalPaths - Folders to add to MATLAB search path of workers
%      AttachedFiles   - Files and folders that are sent to the workers
%      AutoAttachFiles - Determines whether dependent files will be automatically sent to the workers
%      CreateTime      - Time at which this job was created
%      FinishTime      - Time at which this job finished running
%      ID              - Job's numeric identifier
%      JobData         - Data made available to all workers for job's tasks
%      Name            - Name of this job
%      Parent          - Cluster object containing this job
%      StartTime       - Time at which this job started running
%      State           - State of the job: pending, queued, running, finished, or failed
%      SubmitTime      - Time at which this job was submitted
%      Tag             - Label associated with this job
%      Tasks           - Array of task objects contained in this job
%      Type            - Job's type: Independent, Pool, or SPMD
%      UserData        - Data associated with a job object
%      Username        - Name of the user who created this job
%
%   See also parallel.IndependentJob, parallel.Job, 
%            parallel.Cluster/createJob, parallel.Cluster/findJob.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true ) CJSIndependentJob < parallel.IndependentJob & ...
        parallel.internal.cluster.CJSJobMixin

    properties ( PCTGetSet, Transient, Hidden )
        JobSchedulerData
    end

    methods ( Hidden )
        function obj = CJSIndependentJob( parent, jobid, jobsid, support )
            parallel.Job.loadCheck( nargin );
            %{
            validateattributes( parent, {'parallel.cluster.CJSCluster'}, ...
                                {'scalar'} );
            validateattributes( support, {'parallel.internal.cluster.CJSSupport' }, ...
                                {'scalar'} );
            %}
            obj@parallel.IndependentJob( parent, jobid );
            obj@parallel.internal.cluster.CJSJobMixin( support, jobsid );
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % abstract job methods
    methods ( Access = protected )
        function t = createTaskOnOneJob( job, varargin )
            import parallel.internal.cluster.CJSJobMethods

            allowMultipleTasks = true;
            t = CJSJobMethods.createTask( job, job.Support, job.SupportID, ...
                                          allowMultipleTasks, varargin{:} );
        end
        function submitOneJob( job )
            minNumWorkers = 1;
            parallel.internal.cluster.SubmissionChecks.warnIfPoolOpenOnThisCluster( ...
                job.Parent, minNumWorkers );
            job.Parent.hSubmitIndependentJob( job, job.Support, job.SupportID );
        end
        function destroyOneJob( job )
            import parallel.internal.cluster.CJSJobMethods;
            CJSJobMethods.destroyOneJob( job.Parent, job, ...
                                         job.Support, job.SupportID );
        end
        function cancelOneJob( job, cancelException )
            import parallel.internal.cluster.CJSJobMethods;
            CJSJobMethods.cancelOneJob( job.Parent, job, ...
                                        cancelException );
        end
        function tl = getTasks( obj )
            tl = obj.Support.getTasks( obj, obj.SupportID );
        end
        function s = getStateEnum( job )
            s = job.Parent.hGetJobState( job, job.hGetProperty( 'StateEnum' ) );
        end
        function varargout = getTasksByState( obj )
            [varargout{1:nargout}] = obj.getTasksByStateImpl();
        end
        function attachFileDataToJob( obj, filedata )
            obj.Support.attachFileDataToJob( obj.SupportID, obj.Variant, filedata );
        end
    end

    % API1 compatibility
    methods ( Hidden )
        function pPreJobEvaluate( job, task )
            canModifyJob  = true;
            idForRandInit = task.Id;
            doBarrier     = false;
            preCJSJob( job, canModifyJob, idForRandInit, doBarrier );
            job.Parent.hPreJobEvaluate( job, task );
        end
        function pPostJobEvaluate( job )
        % Get the parent of this job and make sure it's a CJSCluster
            cluster = job.Parent;
            if ~isa( cluster, 'parallel.cluster.CJSCluster' )
                return
            end

            if job.Parent.HasSharedFilesystem
                finished = parallel.internal.types.States.Finished;
                jobStateFromTasks = job.hGetStateFromTasks();
                if jobStateFromTasks == finished
                    dctSchedulerMessage(4, 'About to set job state to finished');
                    job.hSetPropertyNoCheck( {'FinishTime', 'StateEnum'}, ...
                                             { char(java.util.Date), finished } );
                end
            end
        end
    end
end
%#ok<*ATUNK> custom attributes
