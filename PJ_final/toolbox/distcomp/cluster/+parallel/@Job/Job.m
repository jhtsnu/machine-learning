%Job Base class for all Job objects
%   A job object contains all the tasks that define what each worker does as
%   part of complete job execution.
%
%   Job objects are created by calling either createJob(cluster)
%   which creates an instance of parallel.IndependentJob, or by calling
%   createCommunicatingJob(cluster) which creates an instance of
%   parallel.CommunicatingJob.
%
%   parallel.Job methods:
%      cancel                - Cancel a pending, queued, or running job
%      createTask            - Create a new task in a job
%      delete                - Remove a job object from its cluster and memory
%      diary                 - Display or save text of batch job
%      fetchOutputs          - Retrieve output arguments from all tasks in a job
%      findTask              - Find task objects belonging to a job
%      listAutoAttachedFiles - List the files that are automatically attached to the job.
%      load                  - Load workspace variables from batch job
%      submit                - Submit job for execution in the cluster
%      wait                  - Wait for job execution to change state
%
%   parallel.Job properties:
%      AdditionalPaths - Folders to add to MATLAB search path of workers
%      AttachedFiles   - Files and folders that are sent to the workers
%      AutoAttachFiles - Determines whether dependent files will be automatically sent to the workers
%      CreateTime      - Time at which this job was created
%      FinishTime      - Time at which this job finished running
%      ID              - Job's numeric identifier
%      JobData         - Data made available to all workers for job's tasks
%      Name            - Name of this job
%      Parent          - Cluster object containing this job
%      StartTime       - Time at which this job started running
%      State           - State of the job: pending, queued, running, finished, or failed
%      SubmitTime      - Time at which this job was submitted
%      Tag             - Label associated with this job
%      Tasks           - Array of task objects contained in this job
%      Type            - Job's type: Independent, Pool, or SPMD
%      UserData        - Data associated with a job object
%      Username        - Name of the user who created this job
%
%   See also parcluster, parallel.Cluster, parallel.Task.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true ) Job < ...
        handle & ...                                   % Jobs are handle objects
        matlab.mixin.Heterogeneous & ...               % Different job subclasses may be concatenated
        parallel.internal.cluster.CustomPropDisp & ... % Use our display methods
        parallel.internal.customattr.CustomGetSet      % Use custom attributes

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % All public properties

    % Mutable at any time:
    properties ( PCTGetSet, Transient, PCTConstraint = 'string' )
        %Tag Label associated with this job
        Tag    = '';

        %Name Name of this job
        Name   = '';
    end
    properties ( PCTGetSet, Transient )
        %JobData Data made available to all workers for job's tasks
        JobData  = [];
    end

    % Mutable only before submission:
    properties ( PCTGetSet, Transient, PCTConstraint = 'cellstr|presubmission' )
        %AttachedFiles Files and folders that are sent to the workers
        %   AttachedFiles can be modified only before the job is submitted.
        %   (read-only after submission)
        AttachedFiles   = {};

        %AdditionalPaths Folders to add to MATLAB search path of workers
        %   AdditionalPaths can be modified only before the job is submitted.
        %   (read-only after submission)
        AdditionalPaths = {};
    end

    properties ( PCTGetSet, Transient, PCTConstraint = 'logicalscalar|presubmission' )
        %AutoAttachFiles Determines whether dependent files will be automatically sent to the workers
        %   If true the files required to run the job's task functions will be calculated
        %   and sent to the workers at job submission time.
        %   See also parallel.Job/listAutoAttachedFiles, parallel.Job/AttachedFiles,
        %   parallel.Job/AdditionalPaths.
        AutoAttachFiles = true;
    end

    % read-only:
    properties ( PCTGetSet, Transient, SetAccess = private )
        %CreateTime Time at which this job was created
        %   (read-only)
        CreateTime = '';

        %SubmitTime Time at which this job was submitted
        %   (read-only)
        SubmitTime = '';

        %StartTime Time at which this job started running
        %   (read-only)
        StartTime  = '';

        %FinishTime Time at which this job finished running
        %   (read-only)
        FinishTime = '';

        %Username Name of the user who created this job
        %   (read-only)
        Username   = '';
    end

    % non-stored properties:
    properties ( Transient )
        %UserData Data associated with a job object
        %   UserData can be used to store any data associated with a job
        %   object. This data is stored in the client MATLAB session, and is
        %   not available on the workers. UserData is not stored in a profile.
        UserData = [];
    end

    % Derived properties
    properties ( Dependent, SetAccess = private )
        %Tasks Array of task objects contained in this job
        %   (read-only)
        %   See also parallel.Job/createTask.
        Tasks  = [];

        %State State of the job: pending, queued, running, finished, or failed
        %   (read-only)
        State  = 'pending';

        %Type Job's type: Independent, Pool, or SPMD
        %   The type is determined at the time the job is created.
        %   (read-only)
        %
        %   See also parallel.Cluster/createJob,
        %            parallel.Cluster/createCommunicatingJob.
        Type   = '';
    end

    % Constant properties
    properties ( Transient, SetAccess = immutable )
        %Parent Cluster object containing this job
        %   (read-only)
        Parent = [];

        %ID Job's numeric identifier
        %   (read-only)
        ID     = 0;
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Hidden and/or protected properties:
    properties ( PCTGetSet, Transient, SetAccess = protected, Hidden )
        AttachedFileData    = []; % Zipped AttachedFiles data
        DependentFiles      = {}; % List of files that the attached files are dependent on
        ExecutionMode       = 0;  % Interactive or not
    end
    properties ( PCTGetSet, Transient, Hidden )
        ApiTag              = ''; % Hidden 'tag' to indicate which method was used to construct the Job
        AttachedFilePaths   = []; % Map of client paths to AttachedFiles in the zip-archive
    end
    properties ( Dependent, SetAccess = private, Hidden )
        StateEnum = parallel.internal.types.States.Pending; % Backing for 'State'
    end
    properties ( Hidden, SetAccess = immutable )
        Variant = parallel.internal.types.Variant.IndependentJob; % Backing for 'Type'
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % get/set methods for Dependent properties, typically deferring to
    % abstract/protected methods.
    methods
        function tl = get.Tasks( obj )
            try
                tl = obj.getTasks();
                % Ensure we always return a column.
                tl = reshape( tl, numel(tl), 1 );
            catch E
                throw( E );
            end
        end
        function s = get.State( obj )
            s = obj.StateEnum.Name;
        end
        function s = get.Type( obj )
            s = obj.Variant.Name;
        end
        function se = get.StateEnum( obj )
            % Getting the state of a Job can be somewhat tricky so defer to an
            % abstract method.
            se = obj.getStateEnum();
        end
    end

    methods ( Sealed )
        function t = createTask( job, varargin )
        %createTask Create a new task in a job
        %    t = createTask(j, F, N, {x1,..., xn}) creates a new task object in job j,
        %    and returns a reference, t, to the added task object. This task evaluates
        %    the function specified by a function handle or function name, F, with the
        %    given arguments, x1,...,xn, returning N output arguments.
        %
        %    t = createTask(j, F, N, {C1,...,Cm}) uses a cell array of m cell arrays
        %    to create m task objects in job j, and returns a vector of references, t,
        %    to the new task objects.  Each task evaluates the function specified by a
        %    function handle or function name F.  The cell array C1 provides the input
        %    arguments to the first task, C2 to the second task, and so on, so that
        %    there is one task per cell array.  Each task returns N output arguments.
        %    If F is a cell array, each element of F specifies a function for each
        %    task in the vector; it must have m elements.  If N is an array of doubles,
        %    each element specifies the number of output arguments for each task in the
        %    vector.  Multidimensional matrices of inputs F, N and {C1,...,Cm}  are
        %    supported; if a cell array is used for F, or a double array for N, its
        %    dimensions must match those of the input arguments cell array of cell
        %    arrays. The output t will be a vector with the same number of elements as
        %    {C1,...,Cm}.
        %
        %    t = createTask(..., 'p1',v1,'p2',v2, ...) creates a task object with the
        %    specified property values. If an invalid property name or property value
        %    is specified, the object will not be created and an error will be thrown.
        %
        %    t = createTask(..., 'Profile', 'profileName', ...) creates a task
        %    object with the property values specified in the profile
        %    'profileName'. If not specified and the cluster has a 'Profile'
        %    specified, then the cluster's profile is automatically applied.
        %
        %    Examples:
        %    Create and submit two jobs.  The first job has a task that generates a
        %    10-by-10 random matrix, the second has a task to execute the function
        %    [y1, y2, y3] = foo(6.98, 7.65) on a worker and return the three output
        %    arguments in the task property OutputArguments.
        %    % Create the first job object.
        %    myCluster = parcluster; % create a cluster object from default profile
        %    j = createJob(myCluster);
        %
        %    % Add a task object that generates a 10-by-10 random matrix.
        %    t = createTask(j, @rand, 1, {10,10});
        %
        %    % Run the job.
        %    submit(j);
        %
        %    % Get the output from the task evaluation.
        %    taskOutput = fetchOutputs(j);
        %
        %    % Show the 10-by-10 random matrix.
        %    disp(taskOutput{1});
        %
        %    % Delete the job object.
        %    delete(j)
        %
        %    % Create the second job and its task.
        %    j = createJob(myCluster);
        %    createTask(j, @foo, 3, {6.98, 7.65})
        %    submit(j);
        %
        %    Create a job with 3 tasks, each of which creates a 10-by-10 random
        %    matrix.
        %    myCluster = parcluster;
        %    j = createJob(myCluster);
        %    t = createTask(j, @rand, 1, {{10,10} {10,10} {10,10}});
        %
        %    See also parallel.Cluster/createJob,
        %             parallel.Cluster/createCommunicatingJob,
        %             parallel.Job/submit, parallel.Job/wait,
        %             parallel.Job/fetchOutputs.
            validateattributes( job, {'parallel.Job'}, {'scalar'}, 'createTask', 'job', 1 );
            job.errorIfInvalid();
            try
                t = job.createTaskOnOneJob( varargin{:} );
                % Ensure we always return a column
                t = reshape( t, numel(t), 1 );
            catch E
                throw( E );
            end
        end

        function submit( job )
        %SUBMIT Submit job for execution in the cluster
        %    SUBMIT(j) queues the job object, j, in the cluster queue.  The
        %    cluster used for this job was determined when the job was
        %    created.
        %
        %    Example:
        %    myCluster = parcluster; % create a cluster object from default profile
        %
        %    % Create a job object.
        %    j = createJob(myCluster);
        %
        %    % Add a task object to be evaluated for the job.
        %    t = createTask(j, @myFunction, 1, {10, 10});
        %
        %    % Queue the job object in the cluster.
        %    submit(j);
        %
        %    See also parallel.Job/createTask, parallel.Job/wait,
        %             parallel.Job/fetchOutputs.
            validateattributes( job, {'parallel.Job'}, {'scalar'}, 'submit', 'job', 1 );
            job.errorIfInvalid();
            job.errorIfNotPending( 'submit' );
            job.attachRequiredFiles();
            try
                job.submitOneJob();
            catch E
                throw( E );
            end
        end

        function listAutoAttachedFiles( job )
        %listAutoAttachedFiles List the files that are automatically attached to the job
        %   listAutoAttachedFiles(j) lists the files of the job object, j, that will be
        %   or have already been attached to the job, following a dependency analysis
        %   performed on the tasks of the job.
        %
        %   See also parallel.Task/listAutoAttachedFiles, parallel.Job/AttachedFiles,
        %   parallel.Job/AutoAttachFiles.
            attachedFiles = job.getAutoAttachedFiles();

            % Do the display of the calculated attached files.
            if isempty( attachedFiles )
                m = message( 'parallel:job:NoAutoAttachedFiles', job.ID );
            else
                formattedFiles = parallel.internal.apishared.AttachedFiles.formatAttachedFilesForDisplay( attachedFiles );
                m = message( 'parallel:job:AutoAttachedFiles', job.ID, formattedFiles );
            end
            disp( m.getString() );
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % protected abstract methods:
    methods ( Access = protected, Abstract )
        % getTasks must return a list of parallel.Task objects
        tl = getTasks( obj )

        % getStateEnum must return a parallel.internal.types.States for this job
        s  = getStateEnum( obj )

        % cancelOneJob must contact the cluster and cancel job execution,
        % writing the cancelException provided where appropriate
        cancelOneJob( job, cancelException )

        % destroyOneJob must not only terminate job execution, it must also
        % remove all trace of the job from disk/storage/...
        destroyOneJob( job )

        % Submit a scalar job
        submitOneJob( job )

        % Attach the job's zipped file data.
        attachFileDataToJob( job, filedata )

        % Create task on a single job
        t = createTaskOnOneJob( job, varargin )
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Protected methods
    methods ( Access = protected )
        function obj = Job( parent, id, variant )
        % The Job constructor requires the cluster and job ID as these are immutable
        % properties.
        %{
            validateattributes( parent, {'parallel.Cluster'}, {'scalar'} );
            validateattributes( id, {'numeric'}, ...
                                {'scalar', 'real', 'finite', 'integer', 'positive'} );
            validateattributes( variant, {'parallel.internal.types.Variant'}, ...
                                {'scalar'} );
            %}
            obj.Parent  = parent;
            obj.ID      = double(id);
            obj.Variant = variant; 
        end

        function varargout = getTasksByState( job )
            % Override this if you can do this more efficiently. Return a 3-cell of
            % tasks, or 3 output arguments if you'd prefer.
            import parallel.internal.types.States;

            allTasks   = job.getTasks();
            taskStates = get( allTasks, {'StateEnum'} );
            taskStates = [ taskStates{:} ];
            states     = [ States.Pending, States.Running, States.Finished, States.Failed ];
            prff       = arrayfun( @(state) allTasks(taskStates == state), ...
                                   states, 'UniformOutput', false );

            % Need to concatenate Finished/Failed
            if nargout == 3
                varargout = { prff(1), prff(2), {[prff{3:end}]} };
            else
                varargout = { [ prff(1:2), {[prff{3:end}]} ] };
            end
        end

        function ok = doWait( job, varargin )
            % Default implementation of job.wait() - override if you have a more
            % efficient means.
            ok = parallel.internal.cluster.wait( job, varargin{:} );
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Sealed protected methods
    methods ( Access = protected, Sealed )
        function errorIfInvalid( jobOrJobs )
            if any( ~isvalid( jobOrJobs ) )
                throwAsCaller( MException(message('parallel:job:JobInvalidated')) );
            end
        end
        function errorIfNotPending( jobOrJobs, method )
        %errorIfNotPending Throws an error if jobOrJobs not all pending.

            import parallel.internal.types.States

            errorIfInvalid( jobOrJobs );
            for ii = 1:numel( jobOrJobs )
                if jobOrJobs(ii).StateEnum ~= States.Pending
                    throwAsCaller( MException(message('parallel:job:OperationOnlyValidWhenPending', method)) );
                end
            end
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Static, sealed protected methods
    methods (Access = protected, Static, Sealed)
        function obj = getDefaultScalarElement() %#ok<STOUT>
            % getDefaultScalarElement is required for matlab.mixin.Heterogeneous
            % to ensure that gaps in arrays are filled in with valid objects.
            error(message('parallel:cluster:ArrayWithGapsNotAllowed', 'parallel.Job'));
        end
    end
    
    methods ( Hidden )
        function hSetProperty( obj, propName, val )
            % hSetProperty has the opportunity to ensure consistency of the
            % to-be-set properties with those properties that have already been
            % set. However, there's no job to do, so simply defer to
            % hSetPropertyNoCheck.
            hSetPropertyNoCheck( obj, propName, val );
        end
        function hCheckTimingForSet( job, timing, propName )
        % Job defines the State, so it is in a position to check
        % the timing.
            parallel.internal.cluster.checkTimingForSet( job.StateEnum, ...
                timing, propName );
        end
        function [jobPropertyMap, propNames] = hGetDisplayItems(obj, diFactory)
            % Create an empty map to hold displayable job properties
            jobPropertyMap = containers.Map('KeyType', 'char', 'ValueType', 'any');
            
            % The job base class is never displayed, so it does not
            % have a specified property display order to put into propNames
            propNames = {};
            
            % Add common job displayable items to map
            names = {...
                'ID', ...
                'Type', ...
                'Username', ...
                'State', ...
                'SubmitTime', ...
                'StartTime', ...
                'FinishTime', ...
                'AutoAttachFiles'};
            values = diFactory.makeMultipleItems(@createDefaultItem, obj.hGetDisplayPropertiesNoError(names));
            jobPropertyMap = [jobPropertyMap; containers.Map(names, values)];
            
            jobPropertyMap('Auto Attached Files') = diFactory.createAutoAttachedFilesItem( obj );

            jobPropertyMap('AttachedFiles') = diFactory.createAttachedFilesItem( ...
                obj.hGetDisplayPropertiesNoError('AttachedFiles'), obj );

            jobPropertyMap('AdditionalPaths') = diFactory.createAdditionalPathsItem( ...
                obj.hGetDisplayPropertiesNoError('AdditionalPaths'), obj );

            % Add non-default displayable items to map
            jobPropertyMap('Running Duration') =  diFactory.createDurationItem(...
                obj.hGetDisplayPropertiesNoError('StartTime'), obj.hGetDisplayPropertiesNoError('FinishTime'));
            % 'Separator' is not a typical name-value object. However, it
            % needs to be added to the propNames list in between property
            % groups, so it is put into the map with the other
            % displayable items.
            jobPropertyMap('Separator') = diFactory.createSeparator();
        end
    end
    
    methods( Hidden, Sealed )
        
        function link = hGetLink(obj, displayValue)
            serializedMemento = serialize(parallel.internal.display.JobMemento(obj));
            matlabFunction = sprintf('%s(''%s'')', ...
                parallel.internal.display.Displayer.DisplayObjectFunction, serializedMemento);
            link = parallel.internal.display.HTMLDisplayType(displayValue, matlabFunction);
        end
        
        function attachedFiles = hGetAutoAttachedFilesNoError( job )
            attachedFiles = {};
            try
                % Not only do we not want to error, we also do not want to warn,
                % which can happen if we are not able to analyze the files.
                warningState = warning('off', 'parallel:cluster:DepfunError');
                warningCleanup = onCleanup( @() warning( warningState ) );
                attachedFiles = job.getAutoAttachedFiles();
            catch err %#ok<NASGU>
            end
        end
    end
    
    methods ( Hidden, Access = ?parallel.Cluster )
        function hSetWithAppend( obj, doAppend, varargin )
            % This method should be called during job construction to build up those
            % properties which 'append' during construction.
            % Algorithm:
            % 1. split varargin into P-V pairs
            % 2. Build a struct of stuff to do
            % 3. Let the validator validate that the new values would be OK
            % 4. Call the underlying hSetPropertyNoCheck - which does no validation.

            import parallel.internal.customattr.Reflection
            import parallel.internal.cluster.ConstructorArgsHelper

            if doAppend
                propsWhichAppend = { 'AttachedFiles', 'AdditionalPaths' };
            else
                propsWhichAppend = {};
            end

            [names, values] = ConstructorArgsHelper.resolveJobBuildArguments( ....
                metaclass( obj ), propsWhichAppend, varargin{:} );

            % Check that the properties specified actually exist for this object:
            obj.checkValidVisiblePropNames( names );

            % Use GetSetImpl version of 'set' which knows how to combine
            % multiple property settings into a single call
            set( obj, names, values );
        end
    end

    methods ( Hidden, Access = ?parallel.Task )
        function doCalculation = hShouldCalculateAttachedFiles( job )
            doCalculation = job.AutoAttachFiles && ... % We are auto-attaching files
                            job.StateEnum == parallel.internal.types.States.Pending; % The job is not yet submitted
        end
    end

    methods ( Sealed, Access = private )
        % Helper for batch load/diary methods
        function errorIfNotBatch( job )
            E = [];
            if numel( job ) > 1
                E = MException(message('parallel:convenience:BatchMethodScalarJob'));
            elseif ~isvalid( job )
                E = MException(message('parallel:job:JobInvalidated'));
            elseif ~ parallel.internal.apishared.BatchJobMethods.isBatchJob( job )
                msg = message( 'parallel:convenience:NotBatchJob' );
                E   = MException( msg.Identifier, '%s', msg.getString() );
            end
            if ~isempty( E )
                throwAsCaller( E );
            end
        end

        function attachRequiredFiles( job )
        % Template method that attaches all required files to the job, automatically
        % calculating dependencies for all the job's tasks if that feature is
        % enabled. The method then delegates to its subclasses to actually attach the
        % list of files and the raw bytes of the zip archive.
            if job.AutoAttachFiles
                attachedFiles = arrayfun( @(t) t.hCalculateAndCacheDependentFiles(), job.Tasks, ...
                    'UniformOutput', false );
                % We also want to calculate the dependencies of the manually
                % attached files of the job.
                jobDependencies = iGetDependenciesOfAttachedFiles( job.AttachedFiles );
                job.DependentFiles = jobDependencies;
                attachedFiles = [ attachedFiles{:}, jobDependencies ];
            else
                attachedFiles = {};
            end

            % Join the manually attached files and their dependencies with the
            % automatically attached files.
            attachedFiles = [ attachedFiles, job.AttachedFiles{:} ];

            % And now make sure the list of attached files is unique.
            attachedFiles = unique( attachedFiles, 'stable' );

            % Toolbox functions and built-ins already exist on the worker, so
            % remove these from the list. It is also not possible to copy
            % built-ins, so these must be filtered here.
            attachedFiles = ...
                parallel.internal.apishared.AttachedFiles.filterToolboxAndBuiltin( attachedFiles );

            % If we are deployed we need to add the relevant .auth files.
            attachedFiles = ...
                parallel.internal.apishared.AttachedFiles.addAuthFilesIfDeployed( attachedFiles );

            % Transform the AttachedFile paths to relative paths, so we can
            % maintain their structure in the zip file.
            [relativePaths, filePathMap] = ...
                parallel.internal.apishared.AttachedFiles.generateTempDirectoryStructure( ...
                        attachedFiles );

            % Copy the AttachedFiles to a temporary location.
            [tempDir, tempCleanup] = ...
                parallel.internal.apishared.AttachedFiles.copyAllFilesToTempDir( ...
                        attachedFiles, relativePaths ); %#ok<NASGU>

            % zip() will only maintain relative paths, files specified by
            % absolute paths are stored in the root of the archive, which
            % we don't want. We need to pass the tempdir to zipFiles, so
            % that the relative paths can be resolved.
            filedata = ...
                parallel.internal.apishared.AttachedFiles.zipFiles( relativePaths, tempDir );

            % Set the relative file paths on the job so that the
            % worker can find the attached files at the other end.
            job.AttachedFilePaths = filePathMap;

            % Now zip up the files and attach them to the job.
            % CJS and MJS do this differently, so call a protected method.
            job.attachFileDataToJob( filedata );
        end

        function attachedFiles = getAutoAttachedFiles( job )
            if job.hShouldCalculateAttachedFiles()
                % If we haven't cached any auto-attached files and the job has
                % not yet been submitted, we need to calculate them for each
                % task function associated with the job.
                attachedFiles = arrayfun( ...
                    @(t) t.hCalculateDependentFiles(), job.Tasks, 'UniformOutput', false );
                jobDependencies = iGetDependenciesOfAttachedFiles( job.AttachedFiles );
                attachedFiles = [ attachedFiles{:}, jobDependencies ];
            else
                % Once the job is submitted the auto-attached files are cached on the tasks.
                % In that case we just list the cached files.
                attachedFiles = [ vertcat( job.Tasks.DependentFiles ); job.DependentFiles ];
            end
            % We only want to list the unique attached files from all the tasks.
            attachedFiles = unique( attachedFiles, 'stable' );
            % If we've calculated we need files that are already attached, remove
            % these from the auto-attached list.
            attachedFiles = setdiff( attachedFiles, job.AttachedFiles, 'stable' );
        end

    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % protected static methods
    methods ( Access = protected, Static )
        function loadCheck( ctorNargin )
        %loadCheck if the constructor is being called with zero arguments,
        %assumption is that the job is being loaded. Therefore error.
            if ctorNargin == 0
                error( message( 'parallel:job:CannotLoadFromMatFile' ) );
            end
        end
    end

    methods ( Sealed )
        % Note that several methods are defined in this 'Sealed' block so that they
        % can be dispatched using an array of Job, but we do not actually allow that
        % to work. (The error returned by attempting to call a non-Sealed method is
        % a little unhelpful).

        % Defined externally:
        out = load( job, varargin );
        diary( job, optionalFilename );

        function varargout = findTask( job, varargin )
        %findTask  Find task objects belonging to a job
        %    tasks = findTask(J) finds all tasks for job J. J can be an array of
        %    job objects.
        %
        %    tasks = findTask(J, 'p1', v1, 'p2', v2, ...) gets a 1 x N array of
        %    task objects belonging to job object J. The returned task objects
        %    will be only those having the specified property-value pairs.  When
        %    a property value is specified, it must use the same format that the
        %    property access returns. For example, if the Name property is
        %    'MyTask', then findTask will not find that object while searching
        %    for a Name property value of myTask as the match is case sensitive.
        %
        %    [pending, running, finished] = findTask(J) returns the tasks from the job
        %    object in three output variables, grouped by their current state.
        %
        %    If J is contained in a remote service, findTask will result in a call to
        %    the remote service. This could result in findTask taking a long time to
        %    complete, depending on the number of tasks retrieved and the network
        %    speed. Also, if the remote service is no longer available, an error will
        %    be thrown.
        %
        %    See also parallel.Job/createTask.
            import parallel.internal.customattr.WorkUnitFinder;

            validateattributes( job, {'parallel.Job'}, {}, ...
                'findTask', 'job', 1 );
            errorIfInvalid( job );

            if ~any( nargout == [ 0 1 3 ] )
                error(message('parallel:job:FindJobNargout'));
            end
            try
                if nargout == 3
                    [tp, tr, tf] = arrayfun( @(j) j.getTasksByState(), ...
                                             job, 'UniformOutput', false );
                    % each output is a cell of cells, need to unpick first to
                    % straight cell array
                    tp = [tp{:}]; tr = [tr{:}]; tf = [tf{:}];
                    % And then reconstitute into a 3-cell containing columns.
                    tasksCell = { [tp{:}].', [tr{:}].', [tf{:}].' };
                else
                    % job.Tasks gives either an array, or a comma-separated
                    % list, so concatenate all those:
                    tasksCell = { vertcat( job.Tasks ) };
                end
                varargout = cellfun( @(x) WorkUnitFinder.find(x, varargin), ...
                    tasksCell, 'UniformOutput', false );
            catch E
                throw( E );
            end
        end

        function okout = wait( job, state, timeout )
        %WAIT Wait for job execution to change state
        %    WAIT(J) blocks execution in the client MATLAB session until the
        %    single job identified by the scalar job object J reaches the
        %    'finished' state. This occurs when all its tasks are finished
        %    processing on remote workers.
        %
        %    WAIT(J, STATE) blocks execution in the client session until the
        %    job object J changes state to STATE. For a job object the
        %    valid states are 'queued', 'running' and 'finished'.
        %
        %    OK = WAIT(J, STATE, TIMEOUT) blocks execution in the client session
        %    until the job object J changes state to STATE or until TIMEOUT
        %    seconds elapsed, whichever happens first. OK is TRUE if state has
        %    been reached or FALSE in case of a timeout.
        %
        %    If a job has previously been in state STATE, then wait will
        %    return immediately. For example, if a job in the 'finished' state is asked
        %    to WAIT(JOB, 'queued'), then the call will return immediately.
        %
        %    Example:
        %    % Create a job object.
        %    myCluster = parcluster; % create a cluster object from default profile
        %    j = createJob(myCluster);
        %    % Add a task object that generates a 10x10 random matrix.
        %    t = createTask(j, @rand, 1, {10,10});
        %    % Run the job.
        %    submit(j);
        %    % Wait until the job is finished.
        %    wait(j, 'finished');
        %
        %    See also parallel.Job/submit, parallel.Job/fetchOutputs,
        %             parallel.Job/delete.

            import parallel.internal.types.States;

            validateattributes( job, {'parallel.Job'}, {'scalar'}, ...
                                'wait', 'job', 1 );
            errorIfInvalid( job );

            if nargin < 2
                state = States.Finished;
            else
                try
                    state = States.fromName( state );
                catch E
                    ME = MException(message('parallel:cluster:FailedToInterpretState'));
                    ME = addCause( ME, E );
                    throw( ME );
                end
                okWaitStates = [ States.Queued, States.Running, States.Finished ];
                okWaitState  = any( state == okWaitStates );
                if ~okWaitState
                    okNames = arrayfun( @(x) x.Name, okWaitStates, 'UniformOutput', false );
                    okNamesStr = strtrim( sprintf( '%s ', okNames{:} ) );
                    error(message('parallel:cluster:InvalidStateToWait', state.Name, okNamesStr));
                end
            end

            if nargin < 3
                timeout = Inf;
            end

            try
                ok = job.doWait( state, timeout );
            catch E
                throw( E );
            end
            if nargout > 0
                okout = ok;
            end
        end

        function outputCell = fetchOutputs( job )
        %fetchOutputs  Retrieve output arguments from all tasks in a job
        %    data = fetchOutputs(job) returns data, the output arguments
        %    contained in the tasks of a finished job. If the scalar job has M
        %    tasks, each row of the M-by-N cell array data contains the output
        %    arguments for the corresponding task in the job. Each row has N
        %    elements, where N is the greatest number of output arguments from
        %    any one task in the job. The N elements of a row are arrays
        %    containing the output arguments from that task. If a task has less
        %    than N output arguments, the excess arrays in the row for that task
        %    are empty. The order of the rows in data will be the same as the
        %    order of the tasks contained in the job's 'Tasks' property.
        %
        %    Note that issuing a call to fetchOutputs will not remove the
        %    output data from the location where it is stored. To remove the output
        %    data, use the delete function to remove individual tasks or their
        %    parent job object.
        %
        %    fetchOutputs will report an error if the job is not in the
        %    'finished' state, or if one of the Tasks encountered an error
        %    during execution. If some tasks completed successfully, then the
        %    output arguments can be accessed directly from the OutputArguments
        %    property of the task.
        %
        %    Example:
        %    % Create a job to generate a random matrix.
        %    myCluster = parcluster; % create a cluster object from default profile
        %    j = createJob(myCluster, 'Name', 'myJob');
        %    t = createTask(j, @rand, 1, {10});
        %    submit(j);
        %    % Wait until the job is finished.
        %    wait(j);
        %    % Get the random matrix.
        %    data = fetchOutputs(j);
        %
        %    See also parallel.Job/wait, parallel.Job/delete.

            validateattributes( job, {'parallel.Job'}, {'scalar'}, ...
                                'fetchOutputs', 'job', 1 );
            errorIfInvalid( job );

            jobState = job.StateEnum;
            if jobState == parallel.internal.types.States.Unavailable
                error(message('parallel:job:JobUnavailable'));
            elseif jobState ~= parallel.internal.types.States.Finished
                error(message('parallel:job:JobNotYetFinished'));
            end
            tasks      = job.Tasks;
            if job.Variant == parallel.internal.types.Variant.CommunicatingPoolJob
                tasks  = tasks(1);
            end
            outputCell = cell( numel( tasks ), max( [tasks.NumOutputArguments] ) );
            for ii = 1:numel( tasks )
                thisTask   = tasks(ii);
                thisTaskID = thisTask.ID;
                if isempty( thisTask.Error ) && isempty( thisTask.ErrorMessage )
                    thisTaskOutputs = thisTask.OutputArguments;
                    numExpectedOutputs = thisTask.NumOutputArguments;
                    numActualOutputs = numel( thisTaskOutputs );
                    if ( numActualOutputs ~= numExpectedOutputs )
                        error(message('parallel:job:OutputArgumentsMismatch', thisTaskID, ...
                            numActualOutputs, numExpectedOutputs));
                    end
                    outputCell(ii, 1:thisTask.NumOutputArguments) = thisTaskOutputs(:);
                else
                    % Throw the task error.
                    E2 = MException(message('parallel:job:ErrorDuringTaskExecution', thisTaskID));
                    if ~isempty( thisTask.Error )
                        E2 = addCause( E2, thisTask.Error );
                    else
                        % Should never get here.
                        E2 = addCause( E2, MException( thisTask.ErrorIdentifier, '%s', thisTask.ErrorMessage ) );
                    end
                    throw( E2 );
                end
            end
        end

        function delete( jobOrJobs, varargin )
        %DELETE  Remove a job object from its cluster and memory
        %    DELETE(j) removes the job object, j, from the local session, and
        %    removes the job from the cluster's JobStorageLocation.  When the
        %    job is deleted, references to it become invalid. Invalid objects
        %    should be removed from the workspace with the CLEAR command. If
        %    multiple references to an object exist in the workspace, deleting
        %    one reference to that object invalidates the remaining references
        %    to it.  These remaining references should be cleared from the
        %    workspace with the CLEAR command.
        %
        %    The task objects contained in a job will also be deleted when a job
        %    object is deleted. Any references to those task objects will also be
        %    invalid, and you should clear them from the workspace.
        %
        %    If j is an array of job objects and one of the objects cannot be
        %    deleted, the other objects in the array will be deleted and a
        %    warning will be returned.
        %
        %    Example:
        %    % Create a job object.
        %    myCluster = parcluster; % create a cluster object from default profile
        %    j = createJob(myCluster, 'Name', 'myJob');
        %    t = createTask(j, @rand, 1, {10});
        %    % Delete the job object.
        %    delete(j);
        %    clear j t
        %
        %    See also parallel.Job/wait, parallel.Job/fetchOutputs, parallel.Task/delete.

            % varargin is specified to ensure we get dispatched correctly; but,
            % we don't support any additional arguments other than the magic
            % string 'invalidateOnly' which allows us to invalidate the handle
            % but not destroy the underlying data.
            narginchk( 1, 2 );
            if nargin == 2
                if ~isequal( varargin{1}, 'invalidateOnly' )
                    error(message('parallel:job:InvalidDeleteSyntax'));
                end
                delete@parallel.internal.customattr.CustomGetSet( jobOrJobs );
                return
            end

            % Since jobOrJobs is Heterogeneous, we must unpick and call the
            % scalar version.
            for ii = 1:numel( jobOrJobs )
                tasks = jobOrJobs(ii).getTasks();

                % destroyOneJob is responsible for removing tasks too, this
                % method invalidates the handles.
                try
                    destroyOneJob( jobOrJobs(ii) );
                catch E
                    throw( E );
                end
                try
                    hInvalidate( tasks );
                catch E
                    % We are trying to natively delete the underlying
                    % tasks associated with this job, but we are
                    % sporadically seeing that the class of tasks is
                    % NOT a subclass of CustomGetSet. For debugging
                    % purposes we are going to warn (which will still
                    % cause test failures but hopefully this will give
                    % us more info on what is going on
                    disp('tasks =');
                    disp(tasks);
                    warning( 'parallel:job:UnexpectedInternalError', 'The class of tasks is : %s\n\nThe underlying error message is : \n%s', class(tasks), E.getReport );
                end

                % Use the built-in delete method to invalidate the objects
                delete@parallel.internal.customattr.CustomGetSet( jobOrJobs(ii) );
            end
        end

        function cancel( jobOrJobs, optMessage )
        %CANCEL  Cancel a pending, queued, or running job
        %    CANCEL(J) stops the job object, J, that is pending, queued or running.
        %    The job's State property is set to finished, and a cancel is executed
        %    on all tasks in the job that are not in the finished state.  Any results
        %    that have been computed for the job object are saved and may be accessed
        %    normally.  A job object that has been canceled cannot be started again.
        %
        %    CANCEL(J,'MESSAGE') cancels the job with an additional
        %    user-specified message. This message will be added to the default
        %    cancellation message. The cancellation message is used to build the
        %    tasks' 'Error' property.
        %
        %    If the job is running in a cluster, any worker sessions that are
        %    evaluating tasks belonging to the job object will be restarted.
        %
        %    If the job is already in the finished state, no action is taken.
        %
        %    Example:
        %    myCluster = parcluster; % create a cluster object from default profile
        %    j = createJob(myCluster);
        %    cancel(j);
        %
        %    See also parallel.Job/submit, parallel.Task/cancel.

            import parallel.internal.cluster.CancelException

            validateattributes( jobOrJobs, {'parallel.Job'}, {}, 'cancel', 'job', 1 );
            errorIfInvalid( jobOrJobs );

            if nargin < 2
                cancelException = CancelException.getDefaultForJob();
            else
                validateattributes( optMessage, {'char'}, {}, ...
                    'cancel', 'message', 2 );
                cancelException = CancelException.getWithUserMessageForJob( optMessage );
            end
            try
                % unpick and call the scalar method
                arrayfun( @(j1) cancelOneJob(j1, cancelException), jobOrJobs );
            catch E
                throw( E );
            end
        end
    end

    methods ( Sealed, Hidden )
        function hInvalidate( jobOrJobs )
        %hInvalidate Invalidate job objects without destroying underlying data.
        % Note that this will ONLY invalidate this set of objects - there
        % is no invalidation of tasks or other objects associated with this
        % job.
            delete( jobOrJobs, 'invalidateOnly' );
        end
    end
    % --------------------------------------------------------------------------
    % API1 property/method compatibility. Properties needed by execution
    % framework.
    properties ( Hidden, Dependent )
        PathDependencies
        FileDependencies
    end

    % Name-changed compatibility
    properties ( Hidden, Dependent )
        Id
    end

    methods
        function pd = get.PathDependencies( job )
            pd = job.AdditionalPaths;
        end
        function pd = get.FileDependencies( job )
            pd = job.AttachedFiles;
        end
        function Id = get.Id( job )
            Id = job.ID;
        end
    end

    % --------------------------------------------------------------------------
    % API1 property/method compatibility. Methods needed to provide backwards
    % compatibility for batch
    methods (Hidden)
        function destroy( jobOrJobs )
            try
                delete( jobOrJobs );
            catch err
                throw(err);
            end
        end
        function outputCell = getAllOutputArguments( job )
            try
                outputCell = job.fetchOutputs();
            catch err
                throw(err);
            end
        end
      
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Execution framework - compatibility layer
    methods ( Hidden, Abstract )
        tf = pCurrentTaskIsPartOfAPool( job )
        pPreJobEvaluate( job, task )
        pPostJobEvaluate( job )
    end
    methods ( Hidden, Sealed )
        function varargout = pReturnDependencyData( job )
            try
                % The AttachedFiles on the job only contains the files manually attached
                % by the user, so we need to add the tasks' DependentFiles. However, the
                % hidden AttachedFilePaths contain all the paths for the AttachedFiles 
                % and the DependentFiles.
                attachedProps = job.hGetProperty( ...
                    {'AttachedFiles', 'DependentFiles', 'AttachedFilePaths', 'AttachedFileData'} );
                % Add the job's tasks' dependent files.
                dependentFiles = vertcat( job.Tasks.DependentFiles );
                % Combine with the job's AttachedFiles and DependentFiles. We
                % might already have some of the dependencies as AttachedFiles
                % on the job, so make the list unique.
                allAttachedFiles = unique( ...
                    vertcat( dependentFiles, attachedProps{1}, attachedProps{2} ), 'stable' );
                % We do not send toolbox functions or built-ins to the workers, but they
                % do remain listed in the AttachedFiles, so we must filter them here.
                allAttachedFiles = ...
                    parallel.internal.apishared.AttachedFiles.filterToolboxAndBuiltin( allAttachedFiles );

                attachedPaths = attachedProps{3};
                attachedFileData = attachedProps{4};
                varargout = { allAttachedFiles, attachedPaths, attachedFileData };
            catch E
                E2 = MException(message('parallel:job:CorruptData'));
                E2 = addCause( E2, E );
                throw( E2 );
            end
        end
        function tf = pIsMatlabPoolJob( job )
            poolVariant = parallel.internal.types.Variant.CommunicatingPoolJob;
            tf = ( job.Variant == poolVariant );
        end
    end

end
%#ok<*ATUNK> custom attributes

function deps = iGetDependenciesOfAttachedFiles( paths )
% Given a set of paths to attached files (which may include directories) return
% a list of all the dependencies of these files.
if isempty( paths )
    deps = {};
    return
end
allFiles = parallel.internal.apishared.AttachedFiles.expandAttachedFiles( paths );
deps = cellfun( ...
    @parallel.internal.apishared.AttachedFiles.calculateAttachedFiles, allFiles, ...
    'UniformOutput', false );
if ~isempty( deps )
    deps = [ deps{:} ]; 
end
end
