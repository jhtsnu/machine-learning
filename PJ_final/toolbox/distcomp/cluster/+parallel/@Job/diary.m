function diary( job, varargin )
%DIARY Display or save text of batch job
%   DIARY(JOB) displays the command window output from the batch job JOB in the
%   MATLAB command window. The command window output will only be captured if
%   the batch job has its CaptureDiary property set to true.
%
%   DIARY(JOB, FILENAME) causes a copy of the command window output from the
%   batch job JOB to be appended to the named file.
%
%   See also BATCH, parallel.Job/load.

% Copyright 2011-2012 The MathWorks, Inc.

job.errorIfNotBatch();

if nargin == 1 && job.Tasks(1).CaptureDiary
    startDelimiter = getString(message('parallel:cluster:StartDiary'));
    endDelimiter = getString(message('parallel:cluster:EndDiary'));
else
    startDelimiter = '';
    endDelimiter = '';
end

diaryOut = job.Tasks(1).Diary;
try
    disp(startDelimiter);
    parallel.internal.apishared.BatchJobMethods.diary( ...
        diaryOut, varargin{:} );
    disp(endDelimiter);
catch E
    throw( E );
end
end
