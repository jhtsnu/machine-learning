% CJSTask Task for CJS clusters
%   A CJSTask object is a type of parallel.Task that is created by calling
%   createTask(job) or findTask(job) on a job that was created from a 
%   cluster of type Local, LSF, Generic, HPCServer, Mpiexec, PBSPro, or 
%   Torque.
%
%   parallel.task.CJSTask methods:
%      cancel                - Cancel a pending or running task
%      delete                - Remove a task object from its job and memory
%      listAutoAttachedFiles - List the files that are automatically attached for this task.
%      wait                  - Wait for task object to change state
%
%   parallel.task.CJSTask properties:
%      CaptureDiary       - Specify whether to return diary output
%      CreateTime         - When task was created
%      Diary              - Text produced by execution of task object's function
%      Error              - Task error information
%      ErrorIdentifier    - Task error identifier
%      ErrorMessage       - Message from task error
%      FinishTime         - When task finished running
%      Function           - Function called when evaluating task
%      ID                 - Task's numeric identifier
%      InputArguments     - Input arguments to task function
%      Name               - Name of this task
%      NumOutputArguments - Number of arguments returned by task function
%      OutputArguments    - Output arguments from running Function on a worker
%      Parent             - Job containing this task
%      StartTime          - When task started running
%      State              - Current state of task
%      UserData           - Data associated with a task object
%      Worker             - Object representing the worker for this task
%
%   See also parallel.Task, parallel.Job, parallel.Job/createTask, 
%            parallel.Job/findTask.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true ) CJSTask < parallel.Task

    properties (Transient, SetAccess = immutable, GetAccess = private)
        Support   % Support object for property access
        SupportID % ID of this Task as far as Support is concerned
    end

    methods ( Hidden )
        function obj = CJSTask( parent, taskid, tasksid, support )
            parallel.Task.loadCheck( nargin );
            obj@parallel.Task( parent, taskid );
            obj.Support   = support;
            obj.SupportID = tasksid;
        end
    end

    methods ( Hidden, Access = ?parallel.job.CJSCommunicatingJob )
        function hRemoveOneExcessTask( task )
        % This method is called by the task with labindex == 1 when a given task is
        % known to be surplus to requirements
        % Ask the support to destroy the task. It needs the jobSID too.
            job = task.Parent;
            task.Support.destroyTask( job.hGetSupportID(), task.SupportID );
        end
    end

    methods ( Access = protected )
        function destroyOneTask( task )
            try
                job      = task.Parent;
                cluster  = job.Parent;
                jobState = job.hGetProperty( 'StateEnum' );
                if jobState ~= parallel.internal.types.States.Pending
                    % This is a bit weird, but it's what API1 does. Why not
                    % check the task state?
                    cluster.hDestroyTask( task );
                end
            catch E
                warning(message('parallel:task:TaskDestructionFailed', E.message));
            end
            % Ask the support to destroy the task. It needs the jobSID too.
            task.Support.destroyTask( job.hGetSupportID(), task.SupportID );
        end

        function cancelOneTask( task, cancelException )
            import parallel.internal.types.States

            % Early return for the task-already-finished case
            taskState = task.StateEnum;
            if taskState == States.Finished
                return
            end
            jobState = task.Parent.StateEnum;
            if jobState == States.Pending
                okToWriteCancellation = true;
            else
                try
                    cluster = task.Parent.Parent;
                    okToWriteCancellation = cluster.hCancelTask( task );
                catch E
                    warning(message('parallel:task:TaskCancellationFailed', E.message));
                    okToWriteCancellation = false;
                end
            end
            if okToWriteCancellation
                task.hWriteCancellation( cancelException );
            end
        end

        function w = getWorker( task )
            w = task.hGetProperty( 'Worker' );
        end
    end

    methods ( Hidden )
        function hSetPropertyNoCheck( obj, propName, val )
            obj.Support.setTaskProperties( obj.SupportID, propName, val );
        end
        function v = hGetProperty( obj, propName )
            if strcmpi( propName, 'Diary' )
                obj.warnIfDiaryIncomplete();
            end
            try
                v = obj.Support.getTaskProperties( obj.SupportID, propName );
            catch err
               throwAsCaller(err);
            end
        end
        function sid = hGetSupportID( task )
        % Public access needed when submitting Jobs. May refactor this later.
            sid = task.SupportID;
        end
        function diaryFile = hGetDiaryFile( obj, taskLoc )
            diaryFile = obj.Support.getDiaryFileForTask( taskLoc );
        end
        function hWriteCancellation( task, cancelException )
        % Called either from CJSTask.cancelOneTask or CJS*Job.cancelOneJob via
        % CJSJobMethods.
            finishTime  = char( java.util.Date );
            finishState = parallel.internal.types.States.Finished;
            namesValues = { 'ErrorIdentifier' , cancelException.identifier;
                            'ErrorMessage'    , cancelException.message;
                            'Error'           , cancelException;
                            'FinishTime'      , finishTime;
                            'StateEnum'       , finishState };
            task.hSetPropertyNoCheck( namesValues(:,1), namesValues(:,2) );
        end
        function [taskPropertyMap, propNames]  = hGetDisplayItems(obj, diFactory)
            taskPropertyMap = hGetDisplayItems@parallel.Task(obj, diFactory);
           
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'ID', ...
                'State', ...
                'Function', ...
                'Parent', ...
                'StartTime', ...
                'Running Duration', ...
                'Separator', ...
                'Error'...
                };
          
            % CJSTask specific displayable items to map. 'Function' is 
            % present in all task displays, but must be gotten carefully 
            % for MJS Tasks, so it is added to the map here and not in the 
            % base class. 
            taskPropertyMap('Function') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('Function')); 
        end
    end

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % API1 compatibility
    methods ( Hidden )
        function pPreTaskEvaluate( task )
            import parallel.internal.types.States
            worker = getCurrentWorker();
            task.hSetProperty( {'StartTime', 'StateEnum', 'Worker'}, ...
                               {char(java.util.Date), States.Running, worker} );
        end
        function pPostTaskEvaluate( task, output, mexception, diaryOut )
            namesValues = { 'OutputArguments' , output;
                            'Error'           , mexception;
                            'FinishTime'      , char(java.util.Date);
                            'StateEnum'       , parallel.internal.types.States.Finished };
            if ~isempty( mexception )
                namesValues = [ namesValues ;
                                { 'ErrorMessage'    , mexception.message;
                                  'ErrorIdentifier' , mexception.identifier } ];
            end

            if ~isempty( diaryOut )
                % This will only happen if we are using the EvalcEvaluator, which
                % is only used for taskFinish functions of a matlabpool job.
                namesValues = [ namesValues ; { 'Diary', diaryOut } ];
            end
            task.hSetPropertyNoCheck( namesValues(:,1), namesValues(:,2) );
        end
    end
end
