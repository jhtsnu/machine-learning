% MJSFailureInfo Information about a failed task attempt
%
%   parallel.task.MJSFailureInfo properties:
%      Diary           - Text produced by execution of task object's function
%      Error           - Task error information
%      ErrorIdentifier - Task error identifier
%      ErrorMessage    - Message from task error
%      StartTime       - When task started running
%      Worker          - Object representing the worker for this task
%      WorkerHost      - Host of the worker for this task
%
%      See also parallel.task.MJSTask

% Copyright 2011 The MathWorks, Inc.

classdef MJSFailureInfo < handle

    properties ( SetAccess = immutable )
        %StartTime When task started running
        %   (read-only)
        StartTime

        %ErrorMessage Message from task error
        %   (read-only)
        ErrorMessage

        %ErrorIdentifier Task error identifier
        %   (read-only)
        ErrorIdentifier

        %Error Task error information
        %   The Error property contains an MException, which is empty if no
        %   error occurred.
        %   (read-only)
        Error

        %Diary Text produced by execution of task object's function
        %   Diary output is produced only if CaptureDiary is true.
        %   (read-only)
        Diary

        %Worker Object representing the worker for this task
        %   The worker object is an instance of parallel.Worker.
        %   (read-only)
        Worker

        %WorkerHost Host of the worker for this task
        %   (read-only)
        WorkerHost
    end

    methods ( Access = private )
        function obj = MJSFailureInfo( infoStruct )
            obj.StartTime       = infoStruct.StartTime;
            obj.ErrorMessage    = infoStruct.ErrorMessage;
            obj.ErrorIdentifier = infoStruct.ErrorIdentifier;
            obj.Diary           = infoStruct.Diary;
            obj.Worker          = infoStruct.Worker;
            obj.WorkerHost      = infoStruct.WorkerHost;
            if ~isempty( obj.ErrorMessage )
                obj.Error       = MException( obj.ErrorIdentifier, '%s', ...
                                              obj.ErrorMessage );
            else
                % Don't expect to get here
                obj.Error       = MException.empty();
            end
        end
    end

    methods ( Hidden, Static )
        function fai = hBuild( infoStruct )
        % Check correct fields etc.
            fai = parallel.task.MJSFailureInfo( infoStruct );
        end
    end
end
