% MJSTask Task for MJS clusters
%
%   parallel.task.MJSTask methods:
%      cancel                - Cancel a pending or running task
%      delete                - Remove a task object from its job and memory
%      listAutoAttachedFiles - List the files that are automatically attached for this task.
%      wait                  - Wait for task object to change state
%
%   parallel.task.MJSTask properties:
%      CaptureDiary       - Specify whether to return diary output
%      CreateTime         - When task was created
%      Diary              - Text produced by execution of task object's function
%      Error              - Task error information
%      ErrorIdentifier    - Task error identifier
%      ErrorMessage       - Message from task error
%      FailureInfo        - Information returned from failed task
%      FinishTime         - When task finished running
%      FinishedFcn        - Callback executed when the job or task finishes
%      Function           - Function called when evaluating task
%      ID                 - Task's numeric identifier
%      InputArguments     - Input arguments to task function
%      MaximumRetries     - Maximum number of times to rerun failed task
%      Name               - Name of this task
%      NumFailures        - Number of times a task was rerun because it failed
%      NumOutputArguments - Number of arguments returned by task function
%      OutputArguments    - Output arguments from running Function on a worker
%      Parent             - Job containing this task
%      RunningFcn         - Callback executed when the job or task starts running
%      StartTime          - When task started running
%      State              - Current state of task
%      Timeout            - Time limit to complete task
%      UserData           - Data associated with a task object
%      Worker             - Object representing the worker for this task
%
%   See also parallel.Task, parallel.Job, parallel.Job/createTask, 
%            parallel.Job/findTask.

% Copyright 2011-2012 The MathWorks, Inc.

classdef ( ConstructOnLoad = true ) MJSTask < parallel.Task & ...
        parallel.internal.cluster.MJSRunningCallbackMixin & ...
        parallel.internal.cluster.MJSFinishedCallbackMixin

    properties (Constant, Access = private)
        StateEnumToEventName = iBuildStateEnumToEventMap();
    end
        
    properties ( PCTGetSet, Transient, PCTConstraint = 'positivescalar|presubmission' )
        %MaximumRetries Maximum number of times to rerun failed task
        %   If a task cannot complete because of certain system failures,
        %   the cluster can attempt to rerun the task. MaximumRetries
        %   specifies how many times to try to run the task after such
        %   failures. The task reruns until it succeeds or until it reaches
        %   the specified maximum number of attempts.
        %
        %   See also parallel.task.MJSFailureInfo,
        %            parallel.task.MJSTask.FailureInfo,
        %            parallel.task.MJSTask.NumFailures.
        MaximumRetries = 1;
        
        %Timeout Time limit to complete task
        %   Timeout is a double value specifying the time limit in seconds
        %   before giving up on the task. The time for timeout begins
        %   counting when the task state changes from pending to running.
        %   When a task times out, the behavior of the task is the same as
        %   if the task were stopped with the cancel function, except a
        %   different message is placed in the task object's Error
        %   property.
        %
        %   The default value for Timeout is large enough so that in
        %   practice, tasks will never time out. You should set the value
        %   of Timeout to the number of seconds you want to allow for
        %   completion of tasks.
        %
        %   See also parallel.task.MJSTask.ErrorMessage,
        %            parallel.task.MJSTask.State.
        Timeout       = Inf;
    end
    properties ( PCTGetSet, Transient, SetAccess = private )
        %FailureInfo Information returned from failed task
        %   If a task reruns because of certain system failures, the task
        %   property FailureInfo stores information related to the failure
        %   and rerun attempts.
        %
        %   FailureInfo is an array of parallel.task.MJSFailureInfo
        %   objects, one for each rerun of the task.
        %   (read-only)
        %
        %   See also parallel.task.MJSFailureInfo,
        %            parallel.task.MJSTask.NumFailures,
        %            parallel.task.MJSTask.MaximumRetries.
        FailureInfo
        
        %NumFailures Number of times a task was rerun because it failed
        %   If a task reruns because of certain system failures, the task
        %   property NumFailures stores a count of the number of attempted
        %   reruns. 
        %   (read-only)
        %
        %   See also parallel.task.MJSFailureInfo,
        %            parallel.task.MJSTask.FailureInfo,
        %            parallel.task.MJSTask.MaximumRetries.
        NumFailures
    end

    properties (Transient, SetAccess = immutable, GetAccess = private)
        Support
        SupportID
        JobSupportID   
    end
    
    properties ( PCTGetSet, Transient, SetAccess = private, Hidden )
        FunctionName
    end

    properties ( Hidden, Dependent )
        MaxNumRetries
    end
    methods
        function set.MaxNumRetries( obj, val )
            obj.MaximumRetries = val;
        end
        function v = get.MaxNumRetries( obj )
            v = obj.MaximumRetries;
        end
    end
    
    methods ( Hidden )
        function obj = MJSTask( parent, tid, sid, support, jobSId )
            parallel.Task.loadCheck( nargin );

            obj@parallel.Task( parent, tid );

            obj.Support      = support;
            obj.SupportID    = sid;
            obj.JobSupportID = jobSId;
        end        
    end
    
    methods ( Access = protected )
        function destroyOneTask( task )
            try
                task.Support.destroyOneTask( task.JobSupportID, task.SupportID );      
                task.unregisterForEventsAfterDestroyed();
            catch E
                throw( distcomp.handleJavaException( task, E ) );
            end
        end
        
        function cancelOneTask( task, cancelException )
            try
                task.Support.cancelOneTask( task.SupportID, cancelException );
            catch E
                throw( distcomp.handleJavaException( task, E ) );
            end
        end
        function w = getWorker( task )
            try
                mjs = task.Parent.Parent;
                w = task.Support.getWorkerForTask( mjs, task.SupportID );
            catch E
                throw( distcomp.handleJavaException( task, E ) );
            end
        end
        function ok = doWait( task, stateEnum, timeout )
            waitRequiredFcn = iCreateStateComparisonFcn( stateEnum );
            ok = task.waitForEvent( task.StateEnumToEventName(stateEnum.Name), timeout, waitRequiredFcn );
        end                
    end
    methods ( Hidden )
        function hSetPropertyNoCheck( obj, propName, val )
            try
                obj.Support.setTaskProperties( obj.SupportID, propName, val );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function v = hGetProperty( obj, propName )
            try
                if strcmpi( propName, 'Diary' )
                    obj.warnIfDiaryIncomplete();
                end
                v = obj.Support.getTaskProperties( obj.SupportID, propName );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function sid = hGetSupportID( obj )
            sid = obj.SupportID;
        end
        function workerProxy = hGetWorkerProxy( obj )
            try
                workerProxyArray = obj.Support.getTaskProperties( obj.SupportID, 'WorkerProxies' );
                workerProxy = workerProxyArray(1);
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function hRerunOrCancel( obj, messageStr )
        % This is called during task evaluation on the workers
            import parallel.internal.cluster.CancelException

            try
                cancelException = CancelException.getWithUserMessageForTask( messageStr );
                obj.Support.rerunOrCancelOneTask( obj.SupportID, cancelException );
            catch E
                throw( distcomp.handleJavaException( obj, E ) );
            end
        end
        function [taskPropertyMap, propNames]  = hGetDisplayItems(obj, diFactory)
            taskPropertyMap = hGetDisplayItems@parallel.Task(obj, diFactory);
            
            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'ID', ...
                'State', ...
                'Function', ...
                'Parent', ...
                'StartTime', ...
                'Running Duration', ...
                'Separator', ...
                'Error', ...
                };
           
            % MJSTask specific displayable items to map. Function is 
            % present in all task displays, but must be gotten via the
            % special cached property FunctionName for MJS tasks. 
            taskPropertyMap('Function') = diFactory.createDefaultItem(obj.hGetDisplayPropertiesNoError('FunctionName'));
        end
    end

    % MJSCallbackMixin methods
    methods ( Access = protected )
        function doRegisterForEvents( obj )            
            obj.Support.registerForTaskEvents( obj.Parent.Parent, obj.SupportID );
            obj.enableAutoEventUnregistration();
        end        
        function doUnregisterForEvents( obj )
            obj.Support.unregisterForTaskEvents( obj.Parent.Parent, obj.SupportID );
            obj.disableAutoEventUnregistration();
        end
        function doUnregisterForEventsAfterDestroyed( obj )
            obj.Support.unstashCluster( obj.Parent.Parent );
        end        
    end
    
    methods( Hidden )
        function hFireStateEvent( obj, stateEnum )
            notify( obj, obj.StateEnumToEventName(stateEnum.Name) );
        end        
    end
    
    % API1
    methods ( Hidden )
        function pPreTaskEvaluate( ~ )
        % Do nothing
        end
        
        % pSubmitResult
        function pSubmitResult( task, output, m_exception, diaryOut )
            workerProxy = task.hGetWorkerProxy;

            if ~workerProxy.okayToSubmitResult()
                % Cancelled
                return
            end
            workerProxy.notifyFunctionEvaluationComplete();

            try
                retry = true;
                iSubmitResult( task, output, m_exception, diaryOut, retry );
            catch E
                % Behave differently depending on the actual error thrown
                switch E.identifier
                    case {'parallel:cluster:MJSHeadnodeNotEnoughJavaMemory', ...
                          'parallel:cluster:MJSNotEnoughJavaMemoryToTransfer', ...
                          'parallel:cluster:MJSNotEnoughJavaMemoryToAllocate'}
                        % Send an empty result with an error message to indicate that the output data
                        % cannot be transferred to the job manager.
                        output  = [];
                        retry = true;
                        iSubmitResult( task, output, E, diaryOut, retry );
                    case 'parallel:task:MJSTaskNotFound'
                        % No action should be taken.
                        % It is possible that the task has been cancelled or destroyed whilst the
                        % worker was working on it. This worker will become idle and work on the
                        % next task sent to it by the job manager.
                    otherwise
                        % This is a case that shouldn't be hit in practice. Try to submit an empty result.
                        % If it fails, just error out instead of retrying the submit result.
                        output  = [];
                        retry = false;
                        err = MException(message('parallel:task:MJSUnableToSubmitResult', E.message));
                        err = err.addCause( E );
                        iSubmitResult( task, output, err, diaryOut, retry );                        
                end
                
            end
        end
        function info = pGetDataStoreInfo( task )
            info = task.Support.getTaskDataStoreInfo;
        end
    end
end
%#ok<*ATUNK>

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iSubmitResult
function iSubmitResult( task, output, exception, diaryOutput, doRetry ) 

    % Serialized the output and command window output. Need to hold a 
    % reference to the serialized data - this is in the "DoNotTouch" 
    % variables below.
    [outputItemArray, serializedOutputDoNotTouch]       = ...
        iSerializeToByteBufferItemArray( output ); %#ok<NASGU>

    [diaryItemArray, diaryBackingDataDoNotTouch] = ...
        iStringToByteBufferItemArray( diaryOutput ); %#ok<NASGU>

    % Serialized the MException, and extract the identifier and message
    [serializedException, errorIdentifier, errorMessage] = ...
        iSerializeException( exception );    
         
    % This re-submits the result if submission fails because the jobmanager 
    % was unavailable (for example: it's been stopped, or the network is down). 
    while true
        try
            task.Support.submitTaskResult( task.SupportID, ...
                outputItemArray, ...
                serializedException, errorMessage, errorIdentifier, ...
                diaryItemArray );
            % Success - stop looping
            break;
        catch E
            E = distcomp.handleJavaException( task, E );
            if doRetry && ...
               any( strcmp( E.identifier, ...
                    {'parallel:cluster:MJSUnreachable', ...
                    'parallel:cluster:MJSReachableInvalidData'}) )
                dctSchedulerMessage( 4, ...
                    ['Failed to submit result as cluster is currently unavailable. ', ...
                    'Task result will be submitted when cluster can be contacted.']);
                % Wait until we can contact the cluster
                mjs = task.Parent.Parent;
                mjs.hWaitForMJS();
            else
                dctSchedulerMessage( 4, 'Failed to submit task result. Error was: "%s"', E.message );
                % If submit failed for any other reason, or if not retrying 
                % just throw the error
                throw( E );
            end
        end
    end
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iSerializeException
function [serializedException, error_id, error_message] = iSerializeException( exception )
    
    % Get the error identifier and message
    if isempty( exception )
        error_id      = '';
        error_message = '';
    else
        error_id      = exception.identifier;
        error_message = exception.message;
    end
    
    % Serialize the MException
    serializedException = distcompserialize( exception );    
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iSerializeToByteBufferItemArray
function [byteBufferItemArray, backingData] = iSerializeToByteBufferItemArray( in )    
    import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem
    backingData         = distcompserialize( in );
    byteBufferItem      = ByteBufferItem( distcompMxArray2ByteBuffer( backingData ) );
    byteBufferItemArray = dctJavaArray( byteBufferItem, ...
        'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData' );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iStringToByteBufferItemArray
function [byteBufferItemArray, backingData] = iStringToByteBufferItemArray( in )
    import com.mathworks.toolbox.distcomp.mjs.datastore.ByteBufferItem
    backingData = unicode2native( in, 'UTF-16LE' );
    byteBufferItem = ByteBufferItem( distcompMxArray2ByteBuffer( backingData ) );
    byteBufferItemArray = dctJavaArray( byteBufferItem, ...
        'com.mathworks.toolbox.distcomp.mjs.datastore.LargeData' );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% iBuildStateEnumToEventMap
function m = iBuildStateEnumToEventMap()
    import parallel.internal.types.States;
    m = containers.Map;
    m(States.Running.Name)  = parallel.internal.cluster.MJSRunningCallbackMixin.RunningEventName;
    m(States.Finished.Name) = parallel.internal.cluster.MJSFinishedCallbackMixin.FinishedEventName;
end

function fcn = iCreateStateComparisonFcn( stateEnum )
% Returns a function that will check the state of the job when called.
% Created here to prevent the job object being sent as part of the
% workspace.
    fcn = @( obj ) obj.StateEnum < stateEnum;
end
