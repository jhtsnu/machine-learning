%IndependentJob - base class for all independent Job objects
%   Independent job objects are created using the createJob method of
%   a cluster.
%
%   parallel.IndependentJob methods:
%      cancel                - Cancel a pending, queued, or running job
%      createTask            - Create a new task in a job
%      delete                - Remove a job object from its cluster and memory
%      diary                 - Display or save text of batch job
%      fetchOutputs          - Retrieve output arguments from all tasks in a job
%      findTask              - Find task objects belonging to a job
%      listAutoAttachedFiles - List the files that are automatically attached to the job.
%      load                  - Load workspace variables from batch job
%      submit                - Submit job for execution in the cluster
%      wait                  - Wait for job execution to change state
%
%   parallel.IndependentJob properties:
%      AdditionalPaths - Folders to add to MATLAB search path of workers
%      AttachedFiles   - Files and folders that are sent to the workers
%      AutoAttachFiles - Determines whether dependent files will be automatically sent to the workers
%      CreateTime      - Time at which this job was created
%      FinishTime      - Time at which this job finished running
%      ID              - Job's numeric identifier
%      JobData         - Data made available to all workers for job's tasks
%      Name            - Name of this job
%      Parent          - Cluster object containing this job
%      StartTime       - Time at which this job started running
%      State           - State of the job: pending, queued, running, finished, or failed
%      SubmitTime      - Time at which this job was submitted
%      Tag             - Label associated with this job
%      Tasks           - Array of task objects contained in this job
%      Type            - Job's type: Independent, Pool, or SPMD
%      UserData        - Data associated with a job object
%      Username        - Name of the user who created this job
%
%   See also parallel.Cluster/createJob.

% Copyright 2011-2012 The MathWorks, Inc.

classdef IndependentJob < parallel.Job

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Hidden/protected methods
    methods ( Access = protected )
        function obj = IndependentJob( parent, id )
            import parallel.internal.types.Variant
            obj@parallel.Job( parent, id, Variant.IndependentJob );
        end
    end
    methods ( Hidden )
        function [jobPropertyMap, propNames]  = hGetDisplayItems(obj, diFactory)
            jobPropertyMap = hGetDisplayItems@parallel.Job(obj, diFactory);

            % The order the properties appear in the propNames array is the
            % order in which they will be displayed.
            propNames = {...
                'ID', ...
                'Type', ...
                'Username', ...
                'State', ...
                'SubmitTime', ...
                'StartTime', ...
                'Running Duration', ...
                'Separator', ...
                'AutoAttachFiles', ...
                'Auto Attached Files', ...
                'AttachedFiles', ...
                'AdditionalPaths'...
                };
        end
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % API1 compatibility layer
    methods ( Hidden )
        function tf = pCurrentTaskIsPartOfAPool( ~ )
            tf = false;
        end
    end
end
