function profile = createProfile(obj, profileName, schedulerComponentName, varargin) 
% profile = createProfile(obj, profileName, schedulerComponentName) 
% Create a profile with the specified name and scheduler component.
%
% profile = createProfile(obj, profileName, schedulerComponentName, P1, V1, ..., Pn, Vn) 
% allows additional parameter-value pairs that modify the properties 
% of the profile. The accepted parameters are:
%   - any property of a profile.  Refer to the help for the profile object
%     for more details.
%   - 'Level' - the Settings level at which the profile should be created.
%     Valid values for 'Level' are 'workgroup' and 'user'.

%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc

validateattributes(profileName, {'char'}, {'row'}, '', 'profileName');
validateattributes(schedulerComponentName, {'char'}, {'row'}, '', 'schedulerComponentName');

if obj.hProfileExists(profileName)
    error(message('parallel:settings:ProfileNameAlreadyExists', ...
        profileName));
end

% Add the Scheduler Component to the P-Vs.  Make sure it goes on the 
% end because right-most wins
pvPairs = [varargin, 'SchedulerComponent', schedulerComponentName];
try
    profile = obj.ProfilesCollection.createNew(profileName, pvPairs);
catch err
    throw(err);
end
end
