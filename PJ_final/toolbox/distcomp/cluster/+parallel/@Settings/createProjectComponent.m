function component = createProjectComponent(obj, name, varargin)
% component = createProjectComponent(obj, name)
% Create a project component with the specified name.
%
% component = createProjectComponent(obj, name, P1, V1, ..., Pn, Vn) 
% allows additional parameter-value pairs that modify the properties 
% of the project component. The accepted parameters are:
%   - any property of the project component.  Refer to the help of the
%     project component for more details
%   - 'Level' - the Settings level at which the profile should be created.
%     Valid values for 'Level' are 'workgroup' and 'user'.

%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc

validateattributes(name, {'char'}, {'row'}, '', 'projectComponentName');

if obj.hProjectComponentExists(name)
    error(message('parallel:settings:ProjectComponentNameAlreadyExists', ...
        name));
end

try
    component = obj.ProjectComponentsCollection.createNew(name, varargin);
catch err
    throw(err);
end
end
