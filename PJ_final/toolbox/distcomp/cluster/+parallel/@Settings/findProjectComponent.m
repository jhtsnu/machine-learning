function components = findProjectComponent(obj, varargin)
% components = findProjectComponent(obj, P1, V1, ..., Pn, Vn)
% Find a project component with the specified parameters.  The accepted parameters
% are any property of a scheduler component.  If no parameters are specified, then
% all scheduler components are returned.

%   Copyright 2011 The MathWorks, Inc.

% TODO:doc

% Optimize the case where only 'Name' is specified in the P-V pairs
[nameIsSpecified, name, pvPairs] = parallel.Settings.amalgamateAndExtractName(varargin);
if nameIsSpecified
    components = obj.ProjectComponentsCollection.getFromName(name);
else
    components = obj.ProjectComponents;
end

if isempty(pvPairs)
    return;
end

try
    components = parallel.internal.customattr.WorkUnitFinder.find(components, pvPairs);
catch err
    throw(err);
end
end
