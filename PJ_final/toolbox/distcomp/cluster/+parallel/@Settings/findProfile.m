function profiles = findProfile(obj, varargin)
% profiles = findProfile(obj, P1, V1, ..., Pn, Vn)
% Find a profile with the specified parameters.  The accepted parameters
% are any property of a profile.  If no parameters are specified, then
% all profiles are returned.

%   Copyright 2011 The MathWorks, Inc.

% TODO:doc

% Optimize the case where only 'Name' is specified in the P-V pairs
[nameIsSpecified, name, pvPairs] = parallel.Settings.amalgamateAndExtractName(varargin);
if nameIsSpecified
    profiles = obj.ProfilesCollection.getFromName(name);
else
    profiles = obj.Profiles;
end

if isempty(pvPairs)
    return;
end

try
    profiles = parallel.internal.customattr.WorkUnitFinder.find(profiles, pvPairs);
catch err
    throw(err);
end
end
