function component = createSchedulerComponent(obj, type, name, varargin)
% component = createSchedulerComponent(obj, type, name)
% Create a scheduler component with the specified type and name.
%
% component = createSchedulerComponent(obj, type, name, P1, V1, ..., Pn, Vn) 
% allows additional parameter-value pairs that modify the properties 
% of the scheduler component. The accepted parameters are:
%   - any property of the scheduler component.  Refer to the help of the
%     scheduler component for more details
%   - 'Level' - the Settings level at which the profile should be created.
%     Valid values for 'Level' are 'workgroup' and 'user'.

%   Copyright 2011-2012 The MathWorks, Inc.

% TODO:doc

validateattributes(type, {'char'}, {'row'}, '', 'type');
validateattributes(name, {'char'}, {'row'}, '', 'schedulerComponentName');
if obj.hSchedulerComponentExists(name);
    error(message('parallel:settings:SchedulerComponentNameAlreadyExists', ...
        name));
end

try
    component = obj.SchedulerComponentsCollection.createNew(type, name, varargin);
catch err
    throw(err);
end
end
