function components = findSchedulerComponent(obj, varargin) 
% components = findSchedulerComponent(obj, P1, V1, ..., Pn, Vn)
% Find a scheduler component with the specified parameters.  The accepted parameters
% are any property of a scheduler component.  If no parameters are specified, then
% all scheduler components are returned.

%   Copyright 2011 The MathWorks, Inc.

% TODO:doc

% Optimize the case where only 'Name' is specified in the P-V pairs
[nameIsSpecified, name, pvPairs] = parallel.Settings.amalgamateAndExtractName(varargin);
if nameIsSpecified
    components = obj.SchedulerComponentsCollection.getFromName(name);
else
    components = obj.SchedulerComponents;
end

if isempty(pvPairs)
    return;
end

try
    components = parallel.internal.customattr.WorkUnitFinder.find(components, pvPairs);
catch err
    throw(err);
end
end
