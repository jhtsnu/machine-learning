function oldName = defaultClusterProfile(newName)
%parallel.defaultClusterProfile Get or set the default parallel computing cluster profile
%   PROFILE = parallel.defaultClusterProfile returns the name of the
%   default profile
%
%   OLDPROFILE = parallel.defaultClusterProfile(NEWPROFILE) sets the
%   default profile to be NEWPROFILE and returns the previous default 
%   profile.
%
%   If the default profile has been deleted, or if it has never been
%   set, parallel.defaultClusterProfile returns 'local' as the default
%   profile.
%
%   Profiles can be created and changed using the saveProfile() or 
%   saveAsProfile() methods on a cluster object.  Profiles can be created, 
%   deleted, and changed using  the Cluster Profiles Manager.  On the Home
%   tab, in the Environment section, click Parallel > Manage Cluster
%   Profiles... to open the Cluster Profiles Manager.
%
%   Examples: 
%   Display the names of all the available profiles and set the first in the
%   list to be the default.
%      allNames = parallel.clusterProfiles()
%      parallel.defaultClusterProfile(allNames{1});
%
%   First set the profile named 'MyProfile' to be the default and then set
%   the profile named 'Profile2' to be the default.
%       parallel.defaultClusterProfile('MyProfile');
%       oldDefault = parallel.defaultClusterProfile('Profile2');
%       strcmp(oldDefault, 'MyProfile') % returns true.
%
%   See also MATLABPOOL, BATCH, PARCLUSTER, parallel.clusterProfiles,
%   parallel.Cluster.saveProfile, parallel.Cluster.saveAsProfile.

%  Copyright 2011-2012 The MathWorks, Inc.

try
    p = parallel.Settings;
    oldName = p.DefaultProfile;

    if nargin > 0
        if p.hProfileExists(newName)
            p.DefaultProfile = newName;
        else
            p.hThrowNonExistentProfile(newName);
        end
    end
catch err
    throw(err);
end
