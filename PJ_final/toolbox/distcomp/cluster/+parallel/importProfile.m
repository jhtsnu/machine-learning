function profileName = importProfile(filename)
%parallel.importProfile Import cluster profiles from file
%   PROFILENAME = parallel.importProfile(FILENAME) imports the profiles
%   stored in the specified file and returns the name of the imported
%   profiles.  If FILENAME has no extension, .settings is assumed;
%   configuration files must be specified with the .mat extension.
%   If only one profile is defined in the file, then PROFILENAME is a 
%   string; if multiple profiles are defined in the file, then PROFILENAME
%   is a cell array of strings.  Note that configuration .mat files contain 
%   only one profile, but profile .settings files can contain one or more 
%   profiles.  If a profile with the same name already exists, an extension 
%   is added to the name of the imported profile.
%
%   The imported profile can be used with any functions that support the
%   use of profiles.  parallel.importProfile does not set any of the imported
%   profiles as the default; you can set the default profile by using the 
%   parallel.defaultClusterProfile function.
%
%   To export a profile, use parallel.exportProfile() or the Cluster Profiles 
%   Manager.  On the Home tab, in the Environment section, click
%   Parallel > Manage Cluster Profiles... to open the Cluster Profiles
%   Manager.
%
%   Profiles that were exported in a previous release are upgraded during import.
%   Configurations are automatically converted to cluster profiles.
%
%   Note that the profiles imported using parallel.importProfile are
%   saved as a part of the user's MATLAB settings, so these profiles
%   are available in your subsequent MATLAB sessions without importing again.
%
%   Examples:
%   Import a profile from file ProfileMaster.settings and set it as the default
%   parallel profile.
%     profile_master = parallel.importProfile('ProfileMaster');
%     parallel.defaultClusterProfile(profile_master)
%
%   Import some profiles from the file ManyProfiles.settings and use the first one
%   to open a pool of MATLAB workers.
%     profs = parallel.importProfile('ManyProfiles');
%     matlabpool('open', profs{1})
%
%   Import a configuration from the file OldConfiguration.mat and set it as
%   the default parallel profile.
%     old_conf = parallel.importProfile('OldConfiguration.mat')
%     parallel.defaultClusterProfile(old_conf)
%
%   See also MATLABPOOL, parallel.defaultClusterProfile, parallel.exportProfile.

% Copyright 2011-2012 The MathWorks, Inc.

narginchk(1, 1);
validateattributes(filename, {'char'}, {'row'}, 'importProfile', 'filename', 1);

suppliedFilename = filename;
% Find out if the filename contains an extension
[~, ~, ext] = fileparts(suppliedFilename);
% Assume .settings extension if none is supplied
if isempty(ext)
    ext = '.settings';
    filename = sprintf('%s%s', filename, ext);
end

% Check that the file exists
if exist(filename, 'file') ~= 2
    error(message('parallel:settings:ImportProfileInvalidFilename', filename));
end

loadProfile = ~strcmpi(ext, '.mat');
try
    profileNames = parallel.internal.settings.importProfileOrConfiguration(filename, loadProfile);
catch err
    throw(err);
end

if isempty(profileNames)
    profileName = '';
elseif numel(profileNames) == 1
    profileName = profileNames{1};
else
    profileName = profileNames;
end