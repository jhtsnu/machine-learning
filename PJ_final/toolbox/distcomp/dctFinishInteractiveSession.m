function dctFinishInteractiveSession
; %#ok Undocumented

% Copyright 2006-2012 The MathWorks, Inc.

% This function should be called to finish any interactive session
% that has been started by a parallel job that has pIsInteractiveJob
% true. The behaviour is to pick up the saved post functions that 
% would have normally run in a non-interactive job and run them now.

root = distcomp.getdistcompobjectroot;

try
    % Make sure that the session has the correct MPI functions to terminate 
    dctRegisterMpiFunctions('mwmpi');
    dctStoreFunctionArray('run');
catch e
    if isa(e, 'distcomp.ExitException')
        m = message( 'parallel:cluster:UnexpectedExitFailure', e.message );
        root.CurrentErrorHandlers.errorFcn( e.CauseException, '%s', m.getString() );
    else
        m = message( 'parallel:cluster:UnexpectedExitFailureNoRestart' );
        root.CurrentErrorHandlers.errorFcn( e, '%s', m.getString() );
    end
end
