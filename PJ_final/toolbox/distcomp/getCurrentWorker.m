function currentWorker = getCurrentWorker
%getCurrentWorker Get worker object currently running this session
%    worker = getCurrentWorker returns the worker object representing the
%    session that is currently evaluating the task that calls this
%    function.
%
%    If the function is executed in a MATLAB session that is not a worker,
%    you get an empty result.
%
%    Example:
%    % Find the current worker
%    worker = getCurrentWorker;
%    % Display the worker properties
%    disp(worker);
%
%    See also parcluster, getCurrentJob, getCurrentTask.

%  Copyright 2000-2012 The MathWorks, Inc.

try
    root = distcomp.getdistcompobjectroot;
    currentWorker = root.CurrentWorker;
catch E %#ok<NASGU>
    warning(message('parallel:cluster:GetCurrentWorkerFailed'));
    currentWorker = [];
end
