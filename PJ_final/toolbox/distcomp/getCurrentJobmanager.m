function currentJobmanager = getCurrentJobmanager
%getCurrentJobmanager Get job manager object that distributed current task
%
%    getCurrentJobmanager will be removed in a future release.
%    Use getCurrentCluster instead.
%
%    jobmanager = getCurrentJobmanager returns the job manager object that has
%    sent the task currently being evaluated by the worker session.
%
%    If the function is executed in a MATLAB session that is not a worker,
%    you get an empty result.
%
%    If your tasks are distributed by a third-party scheduler instead of a
%    job manager, getCurrentJobmanager returns a distcomp.taskrunner
%    object.
%
%    Example:
%    % Find the current job manager 
%    jm = getCurrentJobmanager;
%    % Get the name of the jobmanager
%    name = get(jm, 'Name');
%
% See also getCurrentCluster, findResource, getCurrentWorker, getCurrentJob,
%          getCurrentTask.

%  Copyright 2000-2012 The MathWorks, Inc.

cleanup = parallel.internal.apishared.apiDeprecationMessage( 'getCurrentJobmanager', 'getCurrentCluster' ); %#ok<NASGU>

try
    root = distcomp.getdistcompobjectroot;
    currentJobmanager = root.CurrentJobmanager;
catch E %#ok<NASGU>
    warning('distcomp:getCurrentJobmanager:InvalidState', 'Unexpected error trying to invoke getCurrentJobmanager');
    currentJobmanager = [];
end
