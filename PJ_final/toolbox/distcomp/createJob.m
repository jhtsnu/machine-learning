function job = createJob(varargin)
%createJob  Create distributed job
%
%    This form of createJob will be removed in a future release.
%    Use createJob with a cluster as the first argument instead.
%
%    job = createJob() creates a job using the scheduler identified 
%    by the default parallel configuration and sets the property values 
%    of the job as specified in the default configuration.
%    
%    job = createJob('p1', v1, 'p2', v2, ...) creates a job object with
%    the specified property values. If an invalid property name or property 
%    value is specified, the object is not created. These values will
%    override the values in the default configuration
%    
%    If you are using a third-party scheduler instead of a job manager,
%    the job's data is stored in the location specified by the scheduler's
%    DataLocation property.
%    
%    job = createJob(..., 'configuration', 'ConfigurationName', ...)
%    creates a job using the scheduler identified by the configuration 
%    and sets the property values of the job as specified in that
%    configuration. 
%
%    Example:
%    % Construct a job object with a specific name.
%    j = createJob('Name', 'testjob');
%    % Add tasks to the job.
%    for i = 1:10
%        createTask(j, @rand, 1, {10});
%    end
%    % Run the job.
%    submit(j);
%    % Wait until the job is finished.
%    waitForState(j, 'finished');
%    % Retrieve job results.
%    out = getAllOutputArguments(j);
%    % Display the random matrix generated by one of the job's tasks.
%    disp(out{1});
%    % Destroy the job.
%    destroy(j);
%
%    See also distcomp.job/createTask, distcomp.jobmanager/findJob, 
%             distcomp.job/submit, defaultParallelConfig,
%             parallel.Cluster/createJob.

%  Copyright 2007-2012 The MathWorks, Inc.

cleanup = parallel.internal.apishared.apiDeprecationMessage( 'createJob' ); %#ok<NASGU>

try 
    job = generalCreateJob(@createJob, varargin);
catch exception
    throw(exception);
end
