function dev = gpuDevice( optIdx )
%GPUDEVICE Query or select a GPU device
%   D = GPUDEVICE returns the currently selected GPU device
%
%   D = GPUDEVICE(IDX) selects the GPU device by index IDX. IDX must be between
%   1 and GPUDEVICECOUNT. An error may occur if the GPU device is not supported
%   for use. Note that selecting a device, even if it is already selected,
%   resets that device and invalidates any existing gpuArrays and CUDAKernels.
%
%   GPUDEVICE([]) deselects the currently selected GPU device, leaving no
%   device selected. Deselecting the device will invalidate any existing 
%   gpuArrays and CUDAKernels.
%
%   See also gpuDeviceCount, gpuArray, parallel.gpu.GPUDevice,
%   parallel.gpu.CUDADevice.

% Copyright 2010-2011 The MathWorks, Inc.

try
    if nargin == 1
        dev = parallel.gpu.GPUDevice.select( optIdx );
    else
        dev = parallel.gpu.GPUDevice.current();
    end
catch E
    throw(E); % Strip off the stack.
end
end
