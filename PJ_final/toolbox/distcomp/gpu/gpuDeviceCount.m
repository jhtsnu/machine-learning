function n = gpuDeviceCount
%GPUDEVICECOUNT - return how many GPU devices are present
%   N = GPUDEVICECOUNT returns how many GPU devices are present in your
%   system. If no CUDA driver is installed or the version of the driver is
%   too old then zero is returned.
%
%   See also gpuDevice, parallel.gpu.GPUDevice, parallel.gpu.CUDADevice.

% Copyright 2010-2011 The MathWorks, Inc.
% $Revision: 1.1.10.2 $   $Date: 2011/02/01 18:29:00 $

% This can't be allowed to fail. If CUDA isn't available or there's some
% other problem we catch it and return 0
try
    n = parallel.gpu.GPUDevice.count;
catch err %#ok<NASGU>
    n = 0;
end
