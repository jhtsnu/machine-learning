function display( obj )
%DISPLAY Display gpuArray
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       display(D);
%   
%   See also DISPLAY, GPUARRAY, GPUARRAY/DISP.
%   


%   Copyright 2008-2011 The MathWorks, Inc.

if ~existsOnGPU( obj )
    % Use to disp to show the appropriate message
    disp( obj );
else
    objName = inputname( 1 );
    thisClassName = class( obj );

    % dispInternal knows how to truncate the object and build the DisplayHelper
    % object
    dh = dispInternal( obj, thisClassName, objName );
    dh.doDisplay();
end
end
