%SORT Sort gpuArray in ascending or descending order.
%   Y = SORT(X)
%   Y = SORT(X, MODE)
%   Y = SORT(X, DIM, MODE)   
%   [Y,I] = SORT(X, ...) also returns an index matrix I.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.randn(N, N);
%       E = sort(D);
%   gpuArray Array E contains the elements of D sorted by columns
%   See also SORT, GPUARRAY, GPUARRAYRANDN


%   Copyright 2011 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/04/12 20:24:58 $
