%EPS Spacing of floating point numbers for gpuArray
%   E = EPS(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N,'single');
%       E = eps(D)
%   
%   returns E = eps('single')*gpuArray.ones(N).
%   
%   See also EPS, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $  $Date: 2010/10/07 15:44:56 $
