%RESHAPE Change size of gpuArray
%   RESHAPE(G,M,N) returns the M-by-N gpuArray whose elements are 
%   taken columnwise from G. An error results if G does not 
%   have M*N elements.
%   
%   RESHAPE(G,M,N,P,...) returns an N-D array with the same
%   elements as G but reshaped to have the size M-by-N-by-P-by-...
%   M*N*P*... must be the same as PROD(SIZE(G)).
%   
%   RESHAPE(G,[M N P ...]) is the same thing.
%   
%   RESHAPE(G,...,[],...) calculates the length of the dimension
%   represented by [], such that the product of the dimensions 
%   equals PROD(SIZE(G)). PROD(SIZE(G)) must be evenly divisible 
%   by the product of the known dimensions. You can use only one 
%   occurrence of [].
%   
%   In general, RESHAPE(G,SIZ) returns an N-D array with the same
%   elements as G but reshaped to the size SIZ.  PROD(SIZ) must be
%   the same as PROD(SIZE(G)). 
%   
%   
%   Example:
%    
%       x = gpuArray.colon(1,1000);
%       y = reshape(x,10,10,10)
%   
%   See also RESHAPE, GPUARRAY, GPUARRAY/COLON.


%   Copyright 2008-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $   $Date: 2010/07/19 12:52:08 $
