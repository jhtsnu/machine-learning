%ISEMPTY True for empty gpuArray
%   TF = ISEMPTY(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.zeros(N,0,N);
%       t = isempty(D)
%   
%   returns t = true.
%   
%   See also ISEMPTY, GPUARRAY, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:16 $
