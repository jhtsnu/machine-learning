%^   Matrix power of gpuArray
%   C = A^B
%   C = MPOWER(A,B)
%   
%   Example:
%    
%       N = 1000;
%       A = gpuArray.rand(N);
%       C = A^1.3;
%    
%   See also MPOWER, GPUARRAY.


%   Copyright 2012 The MathWorks, Inc.
