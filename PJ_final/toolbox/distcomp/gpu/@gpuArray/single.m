%SINGLE Convert gpuArray to single precision
%   S = SINGLE(X)
%   
%   Example:
%    
%       N = 1000;
%       Du = gpuArray.ones(N,'uint32');
%       Ds = single(Du)
%       classDu = classUnderlying(Du)
%       classDs = classUnderlying(Ds)
%   
%   converts the N-by-N uint32 gpuArray Du to the
%   single gpuArray Ds.
%   classDu is 'uint32' while classDs is 'single'.
%   
%   See also SINGLE, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:37 $
