%GPUARRAY.TRUE True gpuArray
%   D = GPUARRAY.TRUE(N) is an N-by-N gpuArray matrix 
%   of logical ones.
%   
%   D = GPUARRAY.TRUE(M,N) is an M-by-N gpuArray matrix
%   of logical ones.
%   
%   D = GPUARRAY.TRUE(M,N,P, ...) or GPUARRAY.TRUE([M,N,P, ...])
%   is an M-by-N-by-P-by-... gpuArray of logical ones.
%   
%   Examples:
%    
%       N  = 1000;
%       D1 = gpuArray.true(N) % 1000-by-1000 true logical gpuArray
%       D2 = gpuArray.true(N, N*2) % 1000-by-2000
%       D3 = gpuArray.true([N, N*2]) % 1000-by-2000
%   
%   See also TRUE, GPUARRAY, GPUARRAY/FALSE, GPUARRAY/ONES.


%   Copyright 2008-2011 The MathWorks, Inc.
