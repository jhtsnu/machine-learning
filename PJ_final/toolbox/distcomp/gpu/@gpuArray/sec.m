%SEC Secant of gpuArray in radians
%   Y = SEC(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.zeros(N);
%       E = sec(D)
%   
%   See also SEC, GPUARRAY, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:40 $
