%EIG Eigenvalues and eigenvectors of gpuArray
%   D = EIG(A)
%   [V,D] = EIG(A)
%   
%   The following syntaxes are not supported for full gpuArray:
%   [...] = EIG(A,'nobalance')
%   [...] = EIG(A,B)
%   [...] = EIG(A,B,'chol')
%   [...] = EIG(A,B,'qz')
%   
%   Example:
%    
%       N = 1000;
%       A = gpuArray.rand(N);
%       A = A+A';  % create a real, symmetric matrix A
%       [V,D] = eig(A);
%       norm(A*V-V*D, 1)  % A*V is within round-off error of V*D 
%   
%   See also EIG, GPUARRAY.


%   Copyright 2006-2012 The MathWorks, Inc.
