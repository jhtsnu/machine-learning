%ERFCINV Inverse complementary error function of gpuArray
%   Y = ERFCINV(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = erfcinv(D)
%   
%   See also ERF, ERFC, ERFCX, ERFINV, GPUARRAY, GPUARRAY/ONES.
%   


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.3 $ $Date: 2010/10/07 15:45:00 $
