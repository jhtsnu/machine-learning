%ISNUMERIC True if gpuArray's underlying elements are numeric
%   TF = isnumeric(X) returns true if classUnderlying(X) is a 
%   built-in numeric type and false otherwise.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray(N);
%       t = isnumeric(D)
%   
%   returns t = true.
%   
%   See also ISNUMERIC, CLASSUNDERLYING, GPUARRAY.
%   


%   Copyright 2012 The MathWorks, Inc.

