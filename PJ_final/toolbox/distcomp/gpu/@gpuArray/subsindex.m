function idx = subsindex( idx )
%SUBSINDEX Subscript index for gpuArray
%   
%   OUTIDX = SUBSINDEX(INIDX) accepts a gpuArray input INIDX, and returns the 
%   index OUTIDX of zero-based integer values for use in indexing.  The 
%   class of OUTIDX is the same as the underlying class of INIDX.  
%   
%   See also GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.

if islogical( idx )
    idx = find( idx );
end

idx = gather( idx - 1 );
