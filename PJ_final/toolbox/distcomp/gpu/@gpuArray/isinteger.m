%ISINTEGER True if gpuArray's underlying elements are integer
%   TF = isinteger(X) returns true if classUnderlying(X) is an
%   integer data type and false otherwise.
%   
%   Example:
%    
%       N = int8(10);
%       D = gpuArray(N);
%       t = isinteger(D)
%   
%   returns t = true.
%   
%   See also ISINTEGER, CLASSUNDERLYING, GPUARRAY.
%   


%   Copyright 2012 The MathWorks, Inc.

