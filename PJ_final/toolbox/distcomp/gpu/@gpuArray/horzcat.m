%HORZCAT Horizontal concatenation of gpuArrays
%   C = HORZCAT(A,B,...) implements [A B ...] for gpuArrays.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.eye(N);
%       D2 = [D D] % a 1000-by-2000 gpuArray matrix
%   
%   See also HORZCAT, GPUARRAY, GPUARRAY/CAT.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.12.2 $   $Date: 2010/09/20 14:37:33 $

