%ASECH Inverse hyperbolic secant of gpuArray
%   Y = ASECH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.inf(N);
%       E = asech(D)
%   
%   See also ASECH, GPUARRAY, GPUARRAY/INF.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:31 $
