%POW2 Base 2 power and scale floating point number for gpuArray
%   X = POW2(Y)
%   X = POW2(F,E)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.colon(1, N)
%       E = pow2(D)
%   
%   See also POW2, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2012 The MathWorks, Inc.
