%COND Condition number with respect to inversion for gpuArray.
%   COND(X)
%   COND(X,P) 
%    
%   Class support for input X:
%   float: double, single
%   
%   Example:
%    
%       N = 1024;
%       A = diag(gpuArray.colon(1,N));
%       C = cond(A, 1) % returns N
%   
%   See also COND, GPUARRAY.


%   Copyright 2012 The MathWorks, Inc.
