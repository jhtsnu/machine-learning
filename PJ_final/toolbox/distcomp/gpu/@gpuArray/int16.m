%INT16 Convert gpuArray to signed 16-bit integer
%   I = INT16(X)
%   
%   Example:
%    
%       N = 1000;
%       Du = gpuArray.ones(N,'uint16');
%       Di = int16(Du)
%       classDu = classUnderlying(Du)
%       classDi = classUnderlying(Di)
%   
%   converts the N-by-N uint16 gpuArray Du to the
%   int16 gpuArray Di.
%   classDu is 'uint16' while classDi is 'int16'.
%   
%   See also INT16, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:12 $
