function A = repmat(A, M, N)
%REPMAT Replicate and tile a gpuArray
%   B = repmat(A,M,N) creates a large gpuArray B consisting of an M-by-N
%   tiling of copies of A. The size of B is [size(A,1)*M, size(A,2)*N].  
%   
%   B = repmat(A,N) creates an N-by-N tiling.
%   B = repmat(A,[M N]) accomplishes the same result as repmat(A,M,N).
%    
%   B = repmat(A,[M N P ...]) tiles the gpuArray A to produce a 
%   multidimensional gpuArray B composed of copies of A. The size of B is 
%   [size(A,1)*M, size(A,2)*N, size(A,3)*P, ...].
%    
%   Class support for input A:
%   float: double, single
%   
%   Example:
%    
%       A = gpuArray.rand(10);
%       B = repmat( A, 200, 300 );
%       size(B) % B is a 2000 x 3000 array of tiled copies of A
%   
%   See also REPMAT, GPUARRAY.


%   Copyright 2011-2012 The MathWorks, Inc.
    
    narginchk(2, 3);
    if nargin == 2
        N = [];
    end
    className = 'gpuArray';
    zerosFcn = @iGetEmpty;
    catDimOrderingFcn = @iCatDimensionOrdering;
    A = parallel.internal.array.repmatImpl(A, M, N, className, zerosFcn, ...
        catDimOrderingFcn);
end
    
function A = iCatDimensionOrdering(A, sz, replFcn)
    for d = length(sz):-1:1
        if sz(d) ~= 1
            A = replFcn(A, d, sz(d));
        end
    end
end

function A = iGetEmpty(A, outSz)
    A = gpuArray.zeros(outSz, classUnderlying(A));
end
