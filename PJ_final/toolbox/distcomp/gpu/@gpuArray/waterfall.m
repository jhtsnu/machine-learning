%WATERFALL overloaded for gpuArrays
%   WATERFALL can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard WATERFALL function. All valid syntaxes for WATERFALL are
%   supported.
%   
%   See also WATERFALL, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:26:00 $
