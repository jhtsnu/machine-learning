%classUnderlying Class of elements contained within a gpuArray
%   C = classUnderlying(D) returns the name of the class of the elements
%   contained within the gpuArray D.
%   
%   Examples:
%    
%       N        = 1000;
%       D_uint8  = gpuArray.ones(1, N, 'uint8');
%       D_single = gpuArray.nan(1, N, 'single');
%       c_uint8  = classUnderlying(D_uint8) % returns 'uint8'
%       c_single = classUnderlying(D_single)  % returns 'single'
%   
%   See also CLASS, GPUARRAY.


%   Copyright 2008-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:40:50 $
