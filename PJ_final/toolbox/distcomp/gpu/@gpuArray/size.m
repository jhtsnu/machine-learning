%SIZE Size of gpuArray
%   S = SIZE(D), for the M-by-N gpuArray D, returns the two-element
%   row vector D = [M,N] containing the number of rows and columns in the
%   matrix. For N-D gpuArrays, SIZE(D) returns a 1-by-N vector of
%   dimension lengths. Trailing singleton dimensions are ignored.
%   
%   [M,N] = SIZE(D) for gpuArray D, returns the number of rows and
%   columns in D as separate output variables.
%   
%   [M1,M2,M3,...,MN] = SIZE(D) for N>1 returns the sizes of the first N 
%   dimensions of the gpuArray D.  If the number of output arguments N does
%   not equal NDIMS(D), then for:
%   
%   N > NDIMS(D), SIZE returns ones in the "extra" variables, i.e., outputs
%                 NDIMS(D)+1 through N.
%   N < NDIMS(D), MN contains the product of the sizes of dimensions N
%                 through NDIMS(D).
%   
%   M = SIZE(D,DIM) returns the length of the dimension specified
%   by the scalar DIM.  For example, SIZE(D,1) returns the number
%   of rows. If DIM > NDIMS(D), M will be 1.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N, N*2);
%       n = size(D, 2)
%   
%   returns n = 2000.
%   
%   See also SIZE, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.2 $   $Date: 2010/07/19 12:52:09 $
