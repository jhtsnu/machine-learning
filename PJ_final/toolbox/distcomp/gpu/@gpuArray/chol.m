%CHOL Cholesky factorization of gpuArray
%   R = CHOL(D)
%   [R,p] = CHOL(D)
%   L = CHOL(D, 'lower')
%   [L,p] = CHOL(D, 'lower')
%   
%   D must be a full gpuArray matrix of floating point numbers (single or double).
%   
%   Example:
%    
%       N = 1000;
%       D = 1 + gpuArray.eye(N);
%       [R,p] = chol(D);
%       isequal(p, 0) % true
%       norm(R'*R - D, 1)
%   
%   See also CHOL, GPUARRAY, GPUARRAY/EYE.


%   Copyright 2006-2011 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2011/03/14 02:49:23 $

