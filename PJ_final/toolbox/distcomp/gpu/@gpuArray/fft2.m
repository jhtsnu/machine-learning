%FFT2 Two-dimensional discrete Fourier Transform for gpuArray
%   FFT2(X) returns the two-dimensional Fourier transform of 
%   gpuArray X. If X is a vector, the result will have the 
%   same orientation.
%   
%   FFT2(X,MROWS,NCOLS) pads gpuArray X with zeros to size MROWS-by-NCOLS
%   before transforming.
%   
%   Example:
%    
%       N = 512;
%       D = gpuArray.rand(N);
%       F = fft2(D);
%   
%   See also FFT2, GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.6.3 $  $Date: 2010/09/20 14:37:32 $
