%NORM Matrix or vector norm for gpuArray array
%   All norms supported by the built-in function have been overloaded for gpuArray arrays.
%   
%   For matrices...
%         N = NORM(D) is the 2-norm of D.
%         N = NORM(D, 2) is the same as NORM(D).
%         N = NORM(D, 1) is the 1-norm of D.
%         N = NORM(D, inf) is the infinity norm of D.
%         N = NORM(D, 'fro') is the Frobenius norm of D.
%         N = NORM(D, P) is available for matrix D only if P is 1, 2, inf, or 'fro'.
%   
%   For vectors...
%         N = NORM(D, P) is the same as sum(abs(D).^P)^(1/P) for 1 <= P < inf.
%         N = NORM(D) is the same as norm(D, 2).
%         N = NORM(D, inf) is the same as max(abs(D)).
%         N = NORM(D, -inf) is the same as min(abs(D)).
%   
%   Example:
%    
%       N = 1000;
%       D = diag(gpuArray.colon(1,N));
%       n = norm(D,1)
%   
%   returns n = 1000.
%   
%   See also NORM, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.
%   


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/04/12 20:24:57 $
