%ISSPARSE True for sparse gpuArray matrix
%   TF = ISSPARSE(D)
%   
%   Sparse arrays are not yet supported on the GPU, so ISSPARSE always returns false.
%   
%   See also ISSPARSE, GPUARRAY.


%   Copyright 2006-2012 The MathWorks, Inc.
