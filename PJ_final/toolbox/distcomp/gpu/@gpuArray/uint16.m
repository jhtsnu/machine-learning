%UINT16 Convert gpuArray to unsigned 16-bit integer
%   I = UINT16(X)
%   
%   Example:
%    
%       N = 1000;
%       Di = gpuArray.ones(N,'int16');
%       Du = uint16(Di)
%       classDi = classUnderlying(Di)
%       classDu = classUnderlying(Du)
%   
%   converts the N-by-N int16 gpuArray Di to the
%   uint16 gpuArray Du.
%   classDi is 'int16' while classDu is 'uint16'.
%   
%   See also UINT16, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:43 $
