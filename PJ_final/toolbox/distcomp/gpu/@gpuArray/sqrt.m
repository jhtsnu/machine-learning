%SQRT Square root of gpuArray
%   Y = SQRT(X)
%   
%   Example:
%    
%       N = 1000;
%       D = 2 * gpuArray.ones(N)
%       E = sqrt(D)
%   
%   See also SQRT, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:46 $
