%GPUARRAY.FALSE False gpuArray
%   D = GPUARRAY.FALSE(N) is an N-by-N gpuArray matrix 
%   of logical zeros.
%   
%   D = GPUARRAY.FALSE(M,N) is an M-by-N gpuArray matrix
%   of logical zeros.
%   
%   D = GPUARRAY.FALSE(M,N,P, ...) or GPUARRAY.FALSE([M,N,P, ...])
%   is an M-by-N-by-P-by-... gpuArray of logical zeros.
%   
%   Example:
%    
%       N  = 1000;
%       D1 = gpuArray.false(N) % 1000-by-1000 false gpuArray
%       D2 = gpuArray.false(N, 2*N) % 1000-by-2000
%       D3 = gpuArray.false([N, 2*N]) % 1000-by-2000
%   
%   See also FALSE, GPUARRAY, GPUARRAY/TRUE, GPUARRAY/ZEROS


%   Copyright 2008-2011 The MathWorks, Inc.
