%ASIN Inverse sine of gpuArray, result in radians
%   Y = ASIN(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = asin(D)
%   
%   See also ASIN, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:33 $
