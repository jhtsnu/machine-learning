%./ Right array divide for gpuArray matrix
%   C = A ./ B
%   C = RDIVIDE(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.colon(1, N)'
%       D2 = 1 ./ D1
%   
%   See also RDIVIDE, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:33 $
