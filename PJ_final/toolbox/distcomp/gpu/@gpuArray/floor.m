%FLOOR Round gpuArray towards minus infinity
%   Y = FLOOR(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.colon(1,N)./2
%       E = floor(D)
%   
%   See also FLOOR, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:08 $
