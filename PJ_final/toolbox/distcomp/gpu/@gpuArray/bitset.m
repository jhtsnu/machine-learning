%BITSET set bit of gpuArray
%   C = BITSET(A,B)
%   
%   Example:
%    
%       D1 = gpuArray(uint32(1:10));
%       D2 = 13;
%       D3 = bitset(D1,D2,1)
%   
%   See also BITSET, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2012/01/30 17:35:20 $
