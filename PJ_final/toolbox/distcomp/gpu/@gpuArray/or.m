%| Logical OR for gpuArray
%   C = A | B
%   C = OR(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.eye(N);
%       D2 = gpuArray.rand(N);
%       D3 = D1 | D2
%   
%   returns D3 = gpuArray.true(N).
%   
%   See also OR, GPUARRAY, GPUARRAY/EYE.
%   


%   Copyright 2006-2011 The MathWorks, Inc.

