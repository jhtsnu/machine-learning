%INTERP1 1-D interpolation (table lookup) for gpuArray
%   Vq = interp1(V,Xq)
%   Vq = interp1(X,V,Xq)
%   Vq = interp1(V,Xq,METHOD)
%   Vq = interp1(X,V,Xq,METHOD)
%   Vq = interp1(V,Xq,METHOD,'extrap')
%   Vq = interp1(X,V,Xq,METHOD,'extrap')
%   Vq = interp1(V,Xq,METHOD,EXTRAPVAL)
%   Vq = interp1(X,V,Xq,METHOD,EXTRAPVAL)
%   
%   X must be a finite increasing vector without repeating elements. X can be double or single.
%   V must be a double or single vector with the same length as X. V can be complex.
%   Xq must be an array of type double or single. 
%   METHOD must be either 'linear' or 'nearest'.
%   
%   Example:
%    
%       N = 50;
%       X = gpuArray.colon(1, 0.5, N);
%       V = sin(X);
%       Xq = gpuArray.colon(1, 0.01, N);
%       Vq = interp1(X, V, Xq, 'linear', 0);
%       plot(X,V,'o',Xq,Vq);
%   
%   creates a vector Vq of length N from linearly interpolating function values in V. 
%   Vq has the same size as Xq.
%   
%   See also INTERP1, GPUARRAY.
%   


%   Copyright 2012 The MathWorks, Inc.
