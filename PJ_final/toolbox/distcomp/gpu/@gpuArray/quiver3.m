%QUIVER3 overloaded for gpuArrays
%   QUIVER3 can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard QUIVER3 function. All valid syntaxes for QUIVER3 are
%   supported.
%   
%   See also QUIVER3, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:29 $
