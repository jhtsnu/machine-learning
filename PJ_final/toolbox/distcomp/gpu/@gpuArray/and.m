%& Logical AND for gpuArray
%   C = A & B
%   C = AND(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.eye(N);
%       D2 = gpuArray.rand(N);
%       D3 = D1 & D2
%   
%   returns D3 a N-by-N gpuArray logical array with the
%   diagonal populated with true values.
%   
%   See also AND, GPUARRAY, GPUARRAY/EYE.
%   


%   Copyright 2006-2011 The MathWorks, Inc.

