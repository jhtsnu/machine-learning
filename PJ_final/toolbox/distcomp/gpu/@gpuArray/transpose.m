%.' Transpose of gpuArray
%   E = D.'
%   E = TRANSPOSE(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       E = D.'
%   
%   See also TRANSPOSE, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/05/10 17:10:01 $
