%ISFINITE True for finite elements of gpuArray
%   TF = ISFINITE(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       T = isfinite(D)
%   
%   returns T = gpuArray.true(size(D)).
%   
%   See also ISFINITE, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:14 $
