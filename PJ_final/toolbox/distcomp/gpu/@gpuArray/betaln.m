%BETALN  Logarithm of beta function for gpuArrays
%   Y = BETALN(Z,W) computes the natural logarithm of the beta function for
%   corresponding elements of Z and W. The arrays Z and W must be real and 
%   nonnegative. Both arrays must be the same size, or either can be scalar.
%   
%   See also BETALN, GPUARRAY/BETA, GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.
