%ERFINV Inverse error function of gpuArray
%   Y = ERFINV(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = erfinv(D)
%   
%   See also ERF, ERFC, ERFCX, ERFCINV,  GPUARRAY, GPUARRAY/ONES.
%   


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.3 $ $Date: 2010/10/07 15:45:02 $
