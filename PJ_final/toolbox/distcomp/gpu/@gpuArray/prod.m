%PROD Product of elements of gpuArray
%   PROD(X)
%   PROD(X,'double')
%   PROD(X,'native')
%   PROD(X,DIM)
%   PROD(X,DIM,'double')
%   PROD(X,DIM,'native')
%   
%   The order of the products within the PROD operation is not defined, so
%   the PROD operation on a gpuArray might not return exactly the same 
%   answer as the PROD operation on the corresponding MATLAB numeric array.
%   In particular, the differences might be significant when X is a signed
%   integer type and its product is accumulated natively.
%   
%   Example:
%    
%       N = 1000;
%       D = 4 * (gpuArray.colon(1, N) .^ 2);
%       D2 = D ./ (D - 1);
%       p = prod(D2);
%       norm(p-pi/2,'inf')
%   
%    p is approximately pi/2 (by the Wallis product)
%   
%   See also PROD, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:34 $
