%CEIL Round gpuArray towards plus infinity
%   Y = CEIL(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.colon(1,N)./2
%       E = ceil(D)
%   
%   See also CEIL, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:44 $
