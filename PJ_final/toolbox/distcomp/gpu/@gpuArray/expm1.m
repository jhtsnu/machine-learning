%EXPM1 Compute exp(z)-1 accurately for gpuArray
%   Y = EXPM1(X)
%   
%   Example:
%    
%       N = 1000;
%       D = eps(1) .* gpuArray.ones(N);
%       E = expm1(D)
%   
%   See also EXPM1, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:04 $
