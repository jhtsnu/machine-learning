%MIN Smallest component of gpuArray
%   Y = MIN(X)
%   [Y,I] = MIN(X)
%   [Y,I] = MIN(X,[],DIM)
%   Z = MIN(X,Y)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray(magic(N))
%       m = min(D)
%       m1 = min(D,[],1)
%       m2 = min(D,[],2)
%   
%   m and m1 are both gpuArray row vectors, m2 is a gpuArray column 
%   vector.
%   
%   See also MIN, GPUARRAY, MAGIC.
%   


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.10.2 $   $Date: 2010/09/20 14:37:37 $
