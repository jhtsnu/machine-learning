%FFT Discrete Fourier transform of gpuArray
%   Y = FFT(X) is the discrete Fourier transform (DFT) of vector X.  For 
%   matrices, the FFT operation is applied to each column.  For N-D arrays,
%   the FFT operation operates on the first non-singleton dimension.
%   
%   Y = FFT(X,M) is the M-point FFT, padded with zeros if X has less than
%   M points and truncated if it has more.
%   
%   Y = FFT(X,[],DIM) or Y = FFT(X,M,DIM) applies the FFT operation across 
%   the dimension DIM.
%   
%   Example:
%    
%       Nrow = 2^16;
%       Ncol = 100;
%       D = gpuArray.rand(Nrow, Ncol);
%       F = fft(D);
%   
%   returns the FFT F of the gpuArray matrix by applying the FFT to 
%   each column.
%   
%   See also FFT, GPUARRAY.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.6.3 $  $Date: 2010/09/20 14:37:30 $
