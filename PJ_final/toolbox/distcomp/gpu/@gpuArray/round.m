%ROUND Round towards nearest integer for gpuArray
%   Y = ROUND(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.colon(1, N)./2
%       E = round(D)
%   
%   See also ROUND, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:39 $
