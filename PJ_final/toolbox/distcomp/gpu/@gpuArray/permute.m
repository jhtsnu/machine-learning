%PERMUTE Permute gpuArray dimensions
%    B = PERMUTE(A, ORDER)
%   
%    Example:
%       A = gpuArray.rand(1,2,3,4); 
%       size(permute(A,[3 2 1 4])) % now it's 3-by-2-by-1-by-4.
%   
%    See also PERMUTE, SIZE, GPUARRAY, GPUARRAY/RAND.


%   Copyright 2008-2011 The MathWorks, Inc.

