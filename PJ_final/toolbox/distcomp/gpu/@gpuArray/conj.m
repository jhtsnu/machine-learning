%CONJ Complex conjugate of gpuArray
%   Y = CONJ(X)
%   
%   Example:
%    
%       N = 1000;
%       D = complex(3*gpuArray.ones(N),4*gpuArray.ones(N))
%       E = conj(D)
%   
%   See also CONJ, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:40:53 $
