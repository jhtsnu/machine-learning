%DOT Vector dot product of gpuArray
%   C = DOT(A,B)
%   C = DOT(A,B,DIM)
%   
%   Example:
%    
%       N = 1000;
%       d1 = gpuArray.colon(1,N);
%       d2 = gpuArray.ones(N,1);
%       d = dot(d1,d2)
%   
%   returns d = N*(N+1)/2.
%   
%   See also DOT, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ONES.


%   Copyright 2006-2011 The MathWorks, Inc.
