%LOG1P Compute log(1+z) accurately of gpuArray
%   Y = LOG1P(X)
%   
%   Example:
%    
%       N = 1000;
%       D = eps(1) .* gpuArray.ones(N);
%       E = log1p(D)
%   
%   See also LOG1P, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:23 $
