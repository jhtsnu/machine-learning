%DET   Determinant for gpuArrays
%   X = DET(C) 
%    
%   Example:
%   
%    
%       N = 1000;
%       C = gpuArray.eye( N );
%       X = det( C ) % returns 1
%   
%   See also DET, GPUARRAY.


%   Copyright 2011 The MathWorks, Inc.
