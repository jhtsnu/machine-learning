%BSXFUN  Binary Singleton Expansion Function for gpuArray
%   C = BSXFUN(FUN,A,B) applies the element-by-element binary operation
%   specified by the function handle FUN to arrays A and B, with singleton
%   expansion enabled.
%   
%   The corresponding dimensions of A and B must be equal to each other, or 
%   equal to one. Whenever a dimension of A or B is singleton (equal to 
%   one), BSXFUN virtually replicates the array along that dimension to 
%   match the other array. In the case where a dimension of A or B is 
%   singleton and the corresponding dimension in the other array is zero,
%   BSXFUN virtually diminishes the singleton dimension to zero.
%    
%   The size of the output array C is equal to
%   max(size(A),size(B)).*(size(A)>0 & size(B)>0). For example, if
%   size(A) == [2 5 4] and size(B) == [2 1 4 3], then size(C) == [2 5 4 3].
%    
%   Examples:
%   %Subtract the column means from the matrix A:
%   
%    
%       A = gpuArray.rand(8);
%       A = bsxfun(@minus, A, mean(A));
%   
%   %Compute z(x, y) = x.*sin(y) on a grid:
%    
%         x = gpuArray.colon(1,10);
%         y = x.';
%         z = bsxfun(@(x, y) x.*sin(y), x, y);
%    
%   See also BSXFUN, GPUARRAY, function_handle, gather


%   Copyright 2011-2012 The MathWorks, Inc.
