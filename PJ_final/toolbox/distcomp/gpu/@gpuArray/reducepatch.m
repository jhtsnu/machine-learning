%REDUCEPATCH overloaded for gpuArrays
%   REDUCEPATCH can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard REDUCEPATCH function. All valid syntaxes for REDUCEPATCH are
%   supported.
%   
%   See also REDUCEPATCH, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:30 $
