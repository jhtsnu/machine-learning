%NUMEL Number of elements in gpuArray or subscripted array expression
%   N = NUMEL(D) returns the number of underlying elements in the gpuArray 
%   array D.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(3,4,N);
%       ne = numel(D)
%   
%   returns ne = 12000.
%   
%   See also NUMEL, GPUARRAY.


%   Copyright 2008-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:32 $
