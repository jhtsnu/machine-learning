%GPUARRAY.NAN Build gpuArray containing Not-a-Number
%   D = GPUARRAY.NAN(N) is an N-by-N gpuArray matrix of NANs.
%   
%   D = GPUARRAY.NAN(M,N) is an M-by-N gpuArray matrix of NANs.
%   
%   D = GPUARRAY.NAN(M,N,P,...) or GPUARRAY.NAN([M,N,P,...])
%   is an M-by-N-by-P-by-... gpuArray of NANs.
%   
%   D = GPUARRAY.NAN(M,N,P,..., CLASSNAME) or 
%   GPUARRAY.NAN([M,N,P,...], CLASSNAME) is an M-by-N-by-P-by-... 
%   gpuArray of NANs of class specified by CLASSNAME.  CLASSNAME
%   must be either 'single' or 'double'.
%   
%   
%   Example:
%    
%       N = 1000;
%       % Create a 1000-by-1 gpuArray of underlying class 'single'
%       % containing the value NaN.
%       D1 = gpuArray.nan(N, 1,'single')
%   
%   See also NAN, GPUARRAY, GPUARRAY/ZEROS, GPUARRAY/ONES.


%   Copyright 2008-2011 The MathWorks, Inc.
