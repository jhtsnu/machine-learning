%EZPOLAR overloaded for gpuArrays
%   EZPOLAR can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard EZPOLAR function. All valid syntaxes for EZPOLAR are
%   supported.
%   
%   See also EZPOLAR, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:57 $
