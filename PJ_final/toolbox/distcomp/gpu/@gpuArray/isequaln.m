%ISEQUALN True if gpuArrays are numerically equal
%   TF = ISEQUALN(A,B)
%   TF = ISEQUALN(A,B,C,...)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.nan(N);
%       f = isequal(D,D)
%       t = isequaln(D,D)
%   
%   returns f = false and t = true.
%   
%   See also ISEQUALN, GPUARRAY, GPUARRAY/NAN.
%   


%   Copyright 2006-2011 The MathWorks, Inc.
