%MOD Modulus after division of gpuArray
%   C = MOD(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D = mod(gpuArray.colon(1,N),2)
%   
%   See also MOD, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:28 $
