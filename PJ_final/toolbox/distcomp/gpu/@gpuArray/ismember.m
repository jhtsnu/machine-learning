%ISMEMBER True for member of gpuArray set
%       LIA = ISMEMBER(A,B)
%       [LIA,LOCB] = ISMEMBER(A, B)
%   
%       The 'rows' and 'legacy' flags to ismember are not supported for the
%       gpu version of ismember.
%   
%       Example:
%    
%          a = gpuArray([9 9 8 8 7 7 7 6 6 6 5 5 4 4 2 1 1 1])
%          b = gpuArray([1 1 1 3 3 3 3 3 4 4 4 4 4 9 9 9])
%   
%          [lia1,locb1] = ismember(a,b)
%          % returns
%          % lia1 = [1 1 0 0 0 0 0 0 0 0 0 0 1 1 0 1 1 1]
%          % locb1 = [14 14 0 0 0 0 0 0 0 0 0 0 9 9 0 1 1 1]
%   
%   See also ISMEMBER, GPUARRAY.
%   


%   Copyright 2010-2012 The MathWorks, Inc.
