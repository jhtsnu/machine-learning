%TANH Hyperbolic tangent of gpuArray
%   Y = TANH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.inf(N);
%       E = tanh(D)
%   
%   See also TANH, GPUARRAY, GPUARRAY/INF.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:49 $
