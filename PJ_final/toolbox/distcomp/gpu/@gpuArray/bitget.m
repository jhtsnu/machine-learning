%BITGET get a bit of gpuArray
%   C = BITGET(A,B)
%   
%   Example:
%    
%       D1 = gpuArray(uint32(1:10));
%       D2 = 1;
%       D3 = bitget(D1,D2)
%   
%   See also BITGET, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2012/01/30 17:35:19 $
