%INT2STR overloaded for gpuArrays
%   INT2STR can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard INT2STR function. All valid syntaxes for INT2STR are
%   supported.
%   
%   See also INT2STR, GPUARRAY.


%   Copyright 2010-2011 The MathWorks, Inc.
