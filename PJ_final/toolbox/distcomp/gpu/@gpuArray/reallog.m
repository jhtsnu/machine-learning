%REALLOG Real logarithm of gpuArray
%   Y = REALLOG(X)
%   
%   Example:
%    
%       N = 1000;
%       D = -exp(1)*gpuArray.ones(N)
%       try reallog(D), catch, disp('negative input!'), end
%       E = reallog(-D)
%   
%   See also REALLOG, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:34 $
