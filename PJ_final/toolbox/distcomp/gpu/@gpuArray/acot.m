%ACOT Inverse cotangent of gpuArray, result in radians
%   Y = ACOT(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = acot(D)
%   
%   See also ACOT, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:25 $
