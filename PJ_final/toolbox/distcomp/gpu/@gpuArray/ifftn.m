%IFFTN N-dimensional inverse discrete Fourier Transform.
%   Y = IFFTN(X) returns the N-dimensional inverse discrete Fourier transform
%   of gpuArray X.  If X is a vector, the output will have
%   the same orientation.
%   
%   Y = IFFTN(X, SIZ) pads or crops X to size SIZ before performing 
%   the transform.
%   
%   IFFTN(..., 'symmetric') causes IFFTN to treat X as multidimensionally
%   conjugate symmetric so that the output is purely real. See the reference
%   page for IFFTN for the specific mathematical definition of this symmetry.
%   If your input array is conjugate symmetric, then using the 'symmetric' 
%   parameter can speed up execution.
%   
%   IFFTN(..., 'nonsymmetric') causes IFFTN to make no assumptions about the
%   symmetry of X.
%   
%   Example:
%    
%       N = 64;
%       D = gpuArray.rand(N, N, N);
%       F = ifftn(D);
%   
%   See also IFFTN, GPUARRAY.
%   


%   Copyright 2011-2012 The MathWorks, Inc.
% $Revision: 1.1.6.1 $  $Date: 2011/09/26 23:38:02 $
