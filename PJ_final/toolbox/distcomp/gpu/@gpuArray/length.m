%LENGTH Length of gpuArray vector
%   L = LENGTH(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.zeros(0,N,0);
%       l = length(D)
%   
%   returns l = 0.
%   
%   See also LENGTH, GPUARRAY, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:22 $
