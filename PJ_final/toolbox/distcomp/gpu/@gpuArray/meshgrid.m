%MESHGRID Generate gpuArrays for 2-D functions and 3-D surface plots.
%   [X,Y] = MESHGRID(x,y)
%   [X,Y] = MESHGRID(x) is the same as [X,Y] = MESHGRID(x,x)
%   [X,Y,Z] = MESHGRID(x,y,z)
%   [X,Y,Z] = MESHGRID(x) is the same as [X,Y,Z] = MESHGRID(x,x,x)
%   
%   Class support for inputs:
%          float: double, single
%   
%   Example:
%    
%            N = 1000;
%            x = gpuArray.ones(N, 1);
%            y = gpuArray.rand(2*N,1);
%            [X, Y] = meshgrid(x, y);
%   
%   See also MESHGRID, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2010-2011 The MathWorks, Inc.
