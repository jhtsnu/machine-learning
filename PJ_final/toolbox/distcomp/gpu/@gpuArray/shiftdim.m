%SHIFTDIM Shift dimensions.
%   B = SHIFTDIM(X,N) 
%   [B, NSHIFTS] = SHIFTDIM(X)    
%   
%   Example:
%    
%       a = gpuArray.rand(1,1,3,1,2);
%       [b,n]  = shiftdim(a); % b is 3-by-1-by-2 and n is 2.
%       c = shiftdim(b,-n);   % c == a.
%       d = shiftdim(a,3);    % d is 1-by-2-by-1-by-1-by-3.
%   
%   See also SHIFTDIM, CIRCSHIFT, RESHAPE, SQUEEZE, GPUARRAY
%   


%   Copyright 2011 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/10/11 15:47:02 $
