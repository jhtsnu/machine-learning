%ISEQUAL True if gpuArrays are numerically equal
%   TF = ISEQUAL(A,B)
%   TF = ISEQUAL(A,B,C,...)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.nan(N);
%       f = isequal(D,D)
%       t = isequaln(D,D)
%   
%   returns f = false and t = true.
%   
%   See also ISEQUAL, GPUARRAY, GPUARRAY/NAN.


%   Copyright 2006-2011 The MathWorks, Inc.
