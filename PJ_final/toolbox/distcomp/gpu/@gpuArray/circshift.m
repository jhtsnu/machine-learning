%CIRCSHIFT Shift a gpuArray circularly.
%   B = circshift(A,SHIFTSIZE) 
%   
%   Example:
%    
%       A = gpuArray.rand(10);
%       B = circshift( A, 1)
%   
%   See also CIRCSHIFT, GPUARRAY.


%   Copyright 2011 The MathWorks, Inc.
