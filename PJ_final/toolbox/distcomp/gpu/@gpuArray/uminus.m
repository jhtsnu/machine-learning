%- Unary minus for gpuArrays
%   B = -A
%   B = UMINUS(A)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.eye(N);
%       D2 = -D1
%   
%   See also UMINUS, GPUARRAY, GPUARRAY/EYE.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:51 $
