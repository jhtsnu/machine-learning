%XOR Logical EXCLUSIVE OR for gpuArray
%   C = XOR(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.eye(N);
%       D2 = gpuArray.rand(N);
%       D3 = xor(D1,D2)
%   
%   See also XOR, GPUARRAY, GPUARRAY/EYE.
%   


%   Copyright 2006-2011 The MathWorks, Inc.

