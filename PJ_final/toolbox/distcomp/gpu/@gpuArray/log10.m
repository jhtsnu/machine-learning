%LOG10 Common (base 10) logarithm of gpuArray
%   Y = LOG10(X)
%   
%   Example:
%    
%       N = 1000;
%       D = 10.^gpuArray.colon(1,N);
%       E = log10(D)
%   
%   See also LOG10, GPUARRAY, GPUARRAY/COLON.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:21 $
