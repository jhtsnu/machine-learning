%ERFCX Scaled complementary error function of gpuArray
%   Y = ERFCX(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = erfcx(D)
%   
%   See also ERF, ERFINV, ERFC, ERFCINV, GPUARRAY, GPUARRAY/ONES.
%   
%   


%   Copyright 2006-2010 The MathWorks, Inc.
% : 1.1.4.1 $   : 2010/04/28 23:25:48 $
