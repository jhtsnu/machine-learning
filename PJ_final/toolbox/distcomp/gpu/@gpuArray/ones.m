%GPUARRAY.ONES Ones gpuArray
%   D = GPUARRAY.ONES(N) is an N-by-N gpuArray matrix of ones.
%   
%   D = GPUARRAY.ONES(M,N) is an M-by-N gpuArray matrix of ones.
%   
%   D = GPUARRAY.ONES(M,N,P,...) or GPUARRAY.ONES([M,N,P,...])
%   is an M-by-N-by-P-by-... gpuArray of ones.
%   
%   D = GPUARRAY.ONES(M,N,P,..., CLASSNAME) or 
%   GPUARRAY.ONES([M,N,P,...], CLASSNAME) is an M-by-N-by-P-by-... 
%   gpuArray of ones of class specified by CLASSNAME.
%   
%   Examples:
%    
%       N  = 1000;
%       D1 = gpuArray.ones(N)   % 1000-by-1000 gpuArray matrix of ones
%       D2 = gpuArray.ones(N,N*2) % 1000-by-2000
%       D3 = gpuArray.ones([N,N*2], 'int8') % underlying class 'int8'
%   
%   See also ONES, GPUARRAY, GPUARRAY/ZEROS.
%   


%   Copyright 2008-2011 The MathWorks, Inc.
