%NNZ Number of nonzero gpuArray matrix elements
%   N = NNZ(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.eye(N);
%       n = nnz(D)  % n = N
%   
%   See also NNZ, GPUARRAY, GPUARRAY/EYE.


%   Copyright 2006-2012 The MathWorks, Inc.
