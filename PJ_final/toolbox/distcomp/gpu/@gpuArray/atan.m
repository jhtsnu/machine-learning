%ATAN Inverse tangent of gpuArray, result in radians
%   Y = ATAN(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = atan(D)
%   
%   See also ATAN, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:35 $
