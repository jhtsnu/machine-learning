%.* gpuArray multiply
%   C = A .* B
%   C = TIMES(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.eye(N);
%       D2 = gpuArray.rand(N);
%       D3 = D1 .* D2
%   
%   See also TIMES, GPUARRAY, GPUARRAY/EYE.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:50 $
