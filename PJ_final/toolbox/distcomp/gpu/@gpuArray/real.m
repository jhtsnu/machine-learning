%REAL Complex real part of gpuArray
%   Y = REAL(X)
%   
%   Example:
%    
%       N = 1000;
%       D = complex(3*gpuArray.ones(N),4*gpuArray.ones(N))
%       E = real(D)
%   
%   See also REAL, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:35 $
