%UINT32 Convert gpuArray to unsigned 32-bit integer
%   I = UINT32(X)
%   
%   Example:
%    
%       N = 1000;
%       Di = gpuArray.ones(N,'int32');
%       Du = uint32(Di)
%       classDi = classUnderlying(Di)
%       classDu = classUnderlying(Du)
%   
%   converts the N-by-N int32 gpuArray Di to the
%   uint32 gpuArray Du.
%   classDi is 'int32' while classDu is 'uint32'.
%   
%   See also UINT32, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:44 $
