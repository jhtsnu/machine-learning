%GPUARRAY.COLON Build gpuArrays of the form A:D:B
%   GPUARRAY.COLON returns a gpuArray vector equivalent to the return 
%   vector of the COLON function or the colon notation. 
%   
%   D = GPUARRAY.COLON(A,B) is the same as GPUARRAY.COLON(A,1,B).
%   
%   D = GPUARRAY.COLON(A,D,B) is a gpuArray vector storing the values
%   A:D:B.
%   
%   Example:
%    
%       N = 1000;
%       d = gpuArray.colon(1,N)
%   
%   creates the vector 1:N on the GPU
%   
%   See also COLON, GPUARRAY.


%   Copyright 2008-2011 The MathWorks, Inc.
% $Revision: 1.1.10.4 $   $Date: 2010/09/20 14:37:27 $
