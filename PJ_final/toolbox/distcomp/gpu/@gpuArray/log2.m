%LOG2 Base 2 logarithm and dissect floating point number of gpuArray
%   Y = LOG2(X)
%   
%   Example:
%    
%       N = 1000;
%       D = 2.^gpuArray.colon(1, N);
%       E = log2(D)
%   
%   See also LOG2, GPUARRAY, GPUARRAY/COLON.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:24 $
