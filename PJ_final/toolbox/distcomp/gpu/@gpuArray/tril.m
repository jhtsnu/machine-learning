%TRIL Extract lower triangular part of gpuArray
%   T = TRIL(A,K) yields the elements on and below the K-th diagonal of A. 
%   K = 0 is the main diagonal, K > 0 is above the main diagonal and K < 0
%   is below the main diagonal.
%   T = TRIL(A) is the same as T = TRIL(A,0) where T is the lower triangular 
%   part of A.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       T1 = tril(D,1)
%       Tm1 = tril(D,-1)
%   
%   See also TRIL, GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.
