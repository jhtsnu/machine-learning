%EZGRAPH3 overloaded for gpuArrays
%   EZGRAPH3 can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard EZGRAPH3 function. All valid syntaxes for EZGRAPH3 are
%   supported.
%   
%   See also EZGRAPH3, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:52 $
