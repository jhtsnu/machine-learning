%ISFLOAT True if gpuArray's underlying elements are floating-point
%   TF = isfloat(X) returns true if classUnderlying(X) is a
%   floating-point data type and false otherwise.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray(N);
%       t = isfloat(D)
%   
%   returns t = true.
%   
%   See also ISFLOAT, CLASSUNDERLYING, GPUARRAY.
%   


%   Copyright 2012 The MathWorks, Inc.

