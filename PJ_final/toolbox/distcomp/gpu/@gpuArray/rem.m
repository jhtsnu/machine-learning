%REM Remainder after division for gpuArray
%   C = REM(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D = rem(gpuArray.colon(1, N),2)
%   
%   See also REM, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:38 $
