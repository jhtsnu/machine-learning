%IND2SUB overloaded for gpuArrays
%   IND2SUB is used to determine the equivalent subscript values
%   corresponding to a given single index into an array.
%   
%   Example:
%    
%       ind = gpuArray.colon(1,87,400);
%       sz = [20 20];
%       [rows,cols] = ind2sub(sz,ind)
%   
%   See also IND2SUB, GPUARRAY.SUB2IND, GPUARRAY.


%   Copyright 2011 The MathWorks, Inc.
