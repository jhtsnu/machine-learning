%\ Backslash or left matrix divide for gpuArrays
%   X = A \ B
%   X = MLDIVIDE(A,B)
%   
%   A and B must be full gpuArrays of floating point numbers (single or double). 
%   
%   The MATLAB MLDIVIDE function prints a warning message if A is
%   badly scaled, nearly singular, or rank deficient.  The gpuArray MLDIVIDE 
%   method cannot check for this condition; therefore, it is unable 
%   to warn if there is a violation. You should be aware of this 
%   possibility and take action to avoid it.
%   
%   Example:
%    
%       N = 1000;
%       A = gpuArray.rand(N);
%       B = gpuArray.rand(N,1);
%       X = A \ B;
%       norm(B-A*X, 1)
%   
%   See also MLDIVIDE, GPUARRAY.


%   Copyright 2006-2012 The MathWorks, Inc.
%     
