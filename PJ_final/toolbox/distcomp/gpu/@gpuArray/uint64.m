%UINT64 Convert gpuArray to unsigned 64-bit integer
%   I = UINT64(X)
%   
%   Example:
%    
%       N = 1000;
%       Di = gpuArray.ones(N,'int64');
%       Du = uint64(Di)
%       classDi = classUnderlying(Di)
%       classDu = classUnderlying(Du)
%   
%   converts the N-by-N int64 gpuArray Di to the
%   uint64 gpuArray Du.
%   classDi is 'int64' while classDu is 'uint64'.
%   
%   See also UINT64, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:45 $
