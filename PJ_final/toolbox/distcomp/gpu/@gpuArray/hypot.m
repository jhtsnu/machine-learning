%HYPOT Robust computation of square root of sum of squares for gpuArray
%   C = HYPOT(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = 3e300*gpuArray.ones(N);
%       D2 = 4e300*gpuArray.ones(N);
%       E = hypot(D1,D2)
%   
%   See also HYPOT, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:13 $
