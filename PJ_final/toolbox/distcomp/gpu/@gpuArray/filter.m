%FILTER One-dimensional digital filter.
%   y = filter(b,a,x,Zi,dim) filters the data in x with the
%   filter described by b and a to create the filtered data y.  
%   Currently FILTER supports only the following inputs:
%   
%   b  : must be a vector  
%   a  : must be a scalar
%   x  : can be a vector or 2D matrix
%   Zi : initial condition, must be an empty matrix
%   dim: must be the first or last dimension of x, currently can only be 1 or 2.
%   
%   The filter is a "Direct Form II Transposed" implementation of the standard 
%   difference equation:
%    
%       a*y(n) = b(1)*x(n) + b(2)*x(n-1) + ... + b(nb+1)*x(n-nb)
%       
%   FILTER always operates along the first non-singleton dimension,
%   namely dimension 1 for column vectors and non-trivial matrices,
%   and dimension 2 for row vectors.
%    
%   Currently FILTER does not support initial condition Zi, so Zi must be empty.
%   
%   y = filter(b,a,x,[],dim) operates along the dimension dim. Currently, FILTER
%   does not support 3D or higher dimension for x.
%   
%   Currently, FILTER supports operation only along the first or last dimension.
%   Currently, FILTER does not support returning the final conditions.
%   
%   Example:
%    
%       x = gpuArray.rand(8, 10);
%       b = rand(5, 1);
%       a = 3;
%       dim = 2;
%       y = filter(b,a,x,[],dim)
%   
%   returns the filtering result by applying filter b to each row of data x.
%    
%   See also FILTER, GPUARRAY, GPUARRAY/FILTER2.
%   


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.2 $   $Date: 2010/11/01 19:25:05 $


