%ISSORTED TRUE for sorted vector.
%      Y = ISSORTED(X)
%   The input array must be a vector.
%   
%      Example:
%       
%          N = 1000;
%          D = gpuArray.randn(N, 1);
%          E = issorted(D);
%      returns true if the elements of D are in sorted order (in other words, 
%      if D and SORT(D) are identical) and FALSE if not.
%   
%   See also ISSORTED, SORT, SORTROWS, UNIQUE, ISMEMBER, INTERSECT, SETDIFF, SETXOR, UNION, GPUARRAY
%   


%   Copyright 2011 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/10/11 15:46:59 $
