%> Greater than for gpuArray
%   C = A > B
%   C = GT(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       T = D > D-0.5
%       F = D > D
%   
%   returns T = gpuArray.true(N) 
%   and F = gpuArray.false(N).
%   
%   See also GT, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:12 $
