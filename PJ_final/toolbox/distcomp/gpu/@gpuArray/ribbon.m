%RIBBON overloaded for gpuArrays
%   RIBBON can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard RIBBON function. All valid syntaxes for RIBBON are
%   supported.
%   
%   See also RIBBON, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:32 $
