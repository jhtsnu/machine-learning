%TRIU Extract upper triangular part of gpuArray
%   T = TRIU(A,K) yields the elements on and above the K-th diagonal of A. 
%   K = 0 is the main diagonal, K > 0 is above the main diagonal and K < 0
%   is below the main diagonal.
%   T = TRIU(A) is the same as T = TRIU(A,0) where T is the upper triangular 
%   part of A.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       T1 = triu(D,1)
%       Tm1 = triu(D,-1)
%   
%   See also TRIU, GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.
