%ISINF True for infinite elements of gpuArray
%   TF = ISINF(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.inf(N);
%       T = isinf(D)
%   
%   returns T = gpuArray.true(size(D)).
%   
%   See also ISINF, GPUARRAY, GPUARRAY/INF.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:15 $
