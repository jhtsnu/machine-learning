%REALPOW Real power of gpuArray
%   Z = REALPOW(X,Y)
%   
%   Example:
%    
%       N = 1000;
%       D = -8*gpuArray.ones(N)
%       try realpow(D,1/3), catch, disp('complex output!'), end
%       E = realpow(-D,1/3)
%   
%   See also REALPOW, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:35 $
