%EZMESHC overloaded for gpuArrays
%   EZMESHC can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard EZMESHC function. All valid syntaxes for EZMESHC are
%   supported.
%   
%   See also EZMESHC, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:54 $
