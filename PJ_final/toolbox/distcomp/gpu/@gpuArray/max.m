%MAX Largest component of gpuArray
%   Y = MAX(X)
%   [Y,I] = MAX(X)
%   [Y,I] = MAX(X,[],DIM)
%   Z = MAX(X,Y)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray(magic(N))
%       m = max(D)
%       m1 = max(D,[],1)
%       m2 = max(D,[],2)
%   
%   m and m1 are both gpuArray row vectors, m2 is a gpuArray column 
%   vector.
%   
%   See also MAX, GPUARRAY, MAGIC.
%   


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.10.2 $   $Date: 2010/09/20 14:37:36 $
