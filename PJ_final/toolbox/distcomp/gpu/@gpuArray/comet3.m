%COMET3 overloaded for gpuArrays
%   COMET3 can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard COMET3 function. All valid syntaxes for COMET3 are
%   supported.
%   
%   See also COMET3, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:39 $
