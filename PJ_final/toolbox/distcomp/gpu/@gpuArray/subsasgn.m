%SUBSASGN Subscripted assignment for gpuArray
%   A(I) = B
%   A(I,J) = B for 2D gpuArrays
%   A(I1,I2,I3,...,IN) = B for ND gpuArrays
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.eye(N);
%       D(1,N) = pi
%   
%   See also SUBSASGN, GPUARRAY, GPUARRAY/EYE.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.2 $   $Date: 2010/10/07 15:45:47 $
