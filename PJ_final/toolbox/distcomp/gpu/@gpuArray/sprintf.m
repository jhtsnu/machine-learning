%SPRINTF overloaded for gpuArrays
%   SPRINTF can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard SPRINTF function. All valid syntaxes for SPRINTF are
%   supported.
%   
%   See also SPRINTF, GPUARRAY.


%   Copyright 2010-2011 The MathWorks, Inc.
