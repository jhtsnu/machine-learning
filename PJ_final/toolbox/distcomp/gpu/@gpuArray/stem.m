%STEM overloaded for gpuArrays
%   STEM can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard STEM function. All valid syntaxes for STEM are
%   supported.
%   
%   See also STEM, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:42 $
