%TRISURF overloaded for gpuArrays
%   TRISURF can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard TRISURF function. All valid syntaxes for TRISURF are
%   supported.
%   
%   See also TRISURF, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:56 $
