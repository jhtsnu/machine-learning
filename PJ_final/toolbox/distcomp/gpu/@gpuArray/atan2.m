%ATAN2 Four quadrant inverse tangent of gpuArray
%   Z = ATAN2(Y,X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = atan2(D,D)
%   
%   See also ATAN2, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:36 $
