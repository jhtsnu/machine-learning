%DIFF Difference and approximate derivative.
%   Y = DIFF(X)
%   Y = DIFF(X,N)
%   Y = DIFF(X,N,DIM)
%   
%   Example:
%    
%       N = 10;
%       X = gpuArray.colon(1:N).^2;
%       Y = diff(X)
%   
%   returns gpuArray.colon(3,2,19).
%   
%   See also DIFF, GPUARRAY, GPUARRAY/SUM,GPUARRAY/PROD 


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/02/09 18:53:26 $
