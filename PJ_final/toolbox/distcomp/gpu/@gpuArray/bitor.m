%BITOR Bit-wise OR of gpuArray
%   C = BITOR(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.ones(N,'uint32');
%       D2 = triu(D1);
%       D3 = bitor(D1,D2)
%   
%   See also BITOR, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:41 $
