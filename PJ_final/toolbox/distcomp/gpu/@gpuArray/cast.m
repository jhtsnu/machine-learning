%CAST Cast a gpuArray to a different data type or class
%   B = CAST(A,NEWCLASS)
%   
%   The following syntaxes are not supported for gpuArray A:
%   B = cast(A,'like',P)
%   
%   Example:
%    
%       N = 1000;
%       Du = gpuArray.ones(N,'uint32');
%       Ds = cast(Du,'single')
%       classDu = classUnderlying(Du)
%       classDs = classUnderlying(Ds)
%   
%   casts the gpuArray uint32 array Du to the gpuArray single array
%   Ds. classDu is 'uint32', while classDs is 'single'.
%   
%   See also CAST, GPUARRAY, GPUARRAY/ONES, 
%   GPUARRAY/CLASSUNDERLYING.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:40:49 $
