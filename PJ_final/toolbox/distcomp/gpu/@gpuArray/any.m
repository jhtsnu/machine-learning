%ANY True if any element of a gpuArray vector is nonzero or TRUE
%   A = ANY(D)
%   A = ANY(D,DIM)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.eye(N);
%       t = any(D,1)
%   
%   returns t the gpuArray row vector equal to
%   gpuArray.true(1,N).
%   
%   See also ANY, GPUARRAY, GPUARRAY/EYE, GPUARRAY/TRUE.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:40:47 $
