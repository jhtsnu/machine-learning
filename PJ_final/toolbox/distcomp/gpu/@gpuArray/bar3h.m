%BAR3H overloaded for gpuArrays
%   BAR3H can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard BAR3H function. All valid syntaxes for BAR3H are
%   supported.
%   
%   See also BAR3H, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:35 $
