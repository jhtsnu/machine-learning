%ERRORBAR overloaded for gpuArrays
%   ERRORBAR can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard ERRORBAR function. All valid syntaxes for ERRORBAR are
%   supported.
%   
%   See also ERRORBAR, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:49 $
