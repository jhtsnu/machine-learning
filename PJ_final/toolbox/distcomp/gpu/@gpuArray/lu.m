%LU LU factorization for gpuArray
%   [L,U] = LU(D)
%   [L,U,P] = LU(D)
%   [L,U,p] = LU(D,'vector')
%   
%   D must be a full gpuArray matrix of floating point numbers (single or double).
%   
%       N = 1000;
%       D = gpuArray.rand(N);
%       [L,U,piv] = lu(D,'vector');
%       norm(L*U-D(piv,:), 1)
%   
%   See also LU, GPUARRAY/RAND


%   Copyright 2006-2011 The MathWorks, Inc.
