function [e,cnt] = normest(S,tol)
%NORMEST Estimate the gpuArray matrix 2-norm
%   N = NORMEST(D)
%   N = NORMEST(S,TOL)
%   
%   
%   Example:
%    
%       N = 1000;
%       D = diag(gpuArray.colon(1,N));
%       n = normest(D, 1.0e-4)
%   
%   See also NORMEST, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2012 The MathWorks, Inc.

if nargin < 2, tol = 1.e-6; end

% Error checking to ensure 2D floating point S
if ~ismatrix(S) || ~isValidFloatArray(S)
    error(message('parallel:gpu:array:NormestInvalidMatrix'));
end

maxiter = 100; % should never take this many iterations.
x = sum(abs(S),1)';
cnt = 0;
e = norm(x);
if e == 0, return, end
x = x/e;
e0 = 0;
while abs(e-e0) > tol*e
    e0 = e;
    Sx = S*x;
    if nnz(Sx) == 0
        Sx = gpuArray.rand(size(Sx),classUnderlying(Sx));
    end
    x = S'*Sx;
    normx = norm(x);
    e = normx/norm(Sx);
    x = x/normx;
    cnt = cnt+1;
    if cnt > maxiter
        warning(message('MATLAB:normest:notconverge', maxiter, sprintf('%g',tol)));
        break;
    end
end
