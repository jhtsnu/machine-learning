%IFFT2 Two-dimensional inverse discrete Fourier transform.
%   IFFT2(F) returns the two-dimensional inverse Fourier transform of 
%   gpuArray F. If F is a vector, the result will have the 
%   same orientation.
%   
%   IFFT2(F,MROWS,NCOLS) pads gpuArray F with zeros to size MROWS-by-NCOLS
%   before transforming.
%    
%   IFFT2(..., 'symmetric') causes IFFT2 to treat F as conjugate symmetric in
%   two dimensions so that the output is purely real.  See the reference page
%   for IFFT2 for the specific mathematical definition of this symmetry. If 
%   your input array is conjugate symmetric, then using the 'symmetric' 
%   parameter can speed up execution.
%    
%   IFFT2(..., 'nonsymmetric') causes IFFT2 to make no assumptions about the
%   symmetry of F.
%   
%   Example:
%    
%       N = 512;
%       D = gpuArray.rand(N);
%       F = ifft2(D);
%   
%   See also IFFT2, GPUARRAY.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.6.3 $  $Date: 2010/09/20 14:37:35 $
