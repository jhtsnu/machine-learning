%+ Plus for gpuArray
%   C = A + B
%   C = PLUS(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.ones(N);
%       D2 = 2*D1
%       D3 = D1 + D2
%   
%   See also PLUS, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:31 $
