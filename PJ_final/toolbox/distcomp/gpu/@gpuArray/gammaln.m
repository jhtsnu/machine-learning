%GAMMALN Logarithm of gamma function of gpuArray
%   Y = GAMMALN(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = gammaln(D)
%   
%   See also GAMMA,  GPUARRAY, GPUARRAY/ONES.
%   


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.3 $ $Date: 2010/10/07 15:45:10 $
