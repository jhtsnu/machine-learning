%GATHER Retrieve contents of gpuArray to the CPU
%   X = GATHER(D) is a regular array formed from the contents
%   of the gpuArray D.
%   
%   Example:
%   N = 1000;
%   D = gpuArray(magic(N));
%   M = gather(D);
%   
%   retrieves M = magic(N) on the client
%   
%   See also gpuArray.


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:07 $
