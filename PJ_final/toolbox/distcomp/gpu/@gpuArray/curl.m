%CURL overloaded for gpuArrays
%   CURL can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard CURL function. All valid syntaxes for CURL are
%   supported.
%   
%   See also CURL, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:47 $
