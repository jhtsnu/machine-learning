%ACSCH Inverse hyperbolic cosecant of gpuArray
%   Y = ACSCH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.inf(N);
%       E = acsch(D)
%   
%   See also ACSCH, GPUARRAY, GPUARRAY/INF.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:29 $
