%COMPLEX Construct complex gpuArray from real and imaginary parts
%   C = COMPLEX(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = 3*gpuArray.ones(N);
%       D2 = 4*gpuArray.ones(N);
%       E = complex(D1,D2)
%   
%   See also COMPLEX, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $   $Date: 2010/09/20 14:37:28 $
