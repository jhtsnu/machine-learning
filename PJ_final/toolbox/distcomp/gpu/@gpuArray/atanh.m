%ATANH Inverse hyperbolic tangent of gpuArray
%   Y = ATANH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = atanh(D)
%   
%   See also ATANH, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:38 $
