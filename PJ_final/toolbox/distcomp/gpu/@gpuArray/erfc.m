%ERFC Complementary error function of gpuArray
%   Y = ERFC(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = erfc(D)
%   
%   See also ERF, ERFCX, ERFINV, ERFCINV, GPUARRAY, GPUARRAY/ONES.
%   
%   


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.3 $ $Date: 2010/10/07 15:44:59 $
