%ISLOGICAL True for logical gpuArray
%   TF = ISLOGICAL(X)
%   
%   Example:
%    
%       N = 1000;
%       A = gpuArray.ones(N);
%       f = islogical(A)
%       t = islogical(A > 0)
%   
%   returns f = false and t = true.
%   
%   See also ISLOGICAL, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:19 $
