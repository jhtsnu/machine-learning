%PCOLOR overloaded for gpuArrays
%   PCOLOR can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard PCOLOR function. All valid syntaxes for PCOLOR are
%   supported.
%   
%   See also PCOLOR, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:20 $
