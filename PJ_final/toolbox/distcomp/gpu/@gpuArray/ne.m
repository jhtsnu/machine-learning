%~= Not equal for gpuArray
%   C = A ~= B
%   C = NE(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       F = D ~= D
%       T = D ~= D'
%   
%   returns F = gpuArray.false(N) and T is probably the same as
%   gpuArray.true(N), but with the main diagonal all false
%   values.
%   
%   See also NE, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:30 $
