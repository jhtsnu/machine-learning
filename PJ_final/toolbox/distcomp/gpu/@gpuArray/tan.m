%TAN Tangent of gpuArray in radians
%   Y = TAN(X)
%   
%   Example:
%    
%       N = 1000;
%       D = pi/4*gpuArray.ones(N);
%       E = tan(D)
%   
%   See also TAN, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:48 $
