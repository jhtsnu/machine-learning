%ISREAL True for real gpuArray
%   TF = ISREAL(X)
%   
%   Example:
%    
%       N = 1000;
%       rp = 3 * gpuArray.ones(N);
%       ip = 4 * gpuArray.ones(N);
%       D = complex(rp, ip);
%       f = isreal(D)
%   
%   returns f = false.
%   
%   See also ISREAL, GPUARRAY, GPUARRAY/COMPLEX, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:20 $
