%CAT Concatenate gpuArrays
%   C = CAT(DIM,A,B,...) implements CAT(DIM,A,B,...) for gpuArrays.
%   
%   Example:
%    
%       N1 = 500;
%       N2 = 1000;
%       D1 = gpuArray.ones(N1,N2);
%       D2 = gpuArray.zeros(N1,N2);
%       D3 = cat(1,D1,D2) % D3 is 1000-by-1000
%       D4 = cat(2,D1,D2) % D4 is 500-by-2000
%   
%   See also CAT, VERTCAT, HORZCAT, GPUARRAY, GPUARRAY/ONES, 
%   GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.2 $   $Date: 2010/09/20 14:37:26 $

