%NDIMS Number of dimensions of gpuArray
%   N = NDIMS(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(2,N,3);
%       n = ndims(D)
%   
%   returns n = 3.
%   
%   See also NDIMS, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:31 $
