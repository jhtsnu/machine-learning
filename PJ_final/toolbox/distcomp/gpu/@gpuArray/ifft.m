%IFFT Inverse discrete Fourier transform of gpuArray
%   IFFT(X) is the inverse discrete Fourier transform of 
%   gpuArray X.
%   
%   IFFT(X,N) is the N-point inverse transform.
%    
%   IFFT(X,[],DIM) or IFFT(X,N,DIM) is the inverse discrete Fourier
%   transform of X across the dimension DIM.
%    
%   IFFT(..., 'symmetric') causes IFFT to treat X as conjugate symmetric along
%   the active dimension.  See the reference page for IFFT for the specific
%   mathematical definition of this symmetry. If your input array is conjugate
%   symmetric, then using the 'symmetric' parameter can speed up execution.
%    
%   IFFT(..., 'nonsymmetric') causes IFFT to make no assumptions about the
%   symmetry of X.
%   
%   Example:
%    
%       Nrow = 2^16;
%       Ncol = 100;
%       D = gpuArray.rand(Nrow, Ncol);
%       F = ifft(D);
%   
%   See also IFFT, GPUARRAY.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.6.3 $  $Date: 2010/09/20 14:37:34 $
