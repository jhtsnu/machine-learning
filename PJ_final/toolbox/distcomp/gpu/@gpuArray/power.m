%.^ Array power for gpuArray
%   C = A .^ B
%   C = POWER(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = 2*gpuArray.eye(N);
%       D2 = D1 .^ 2
%   
%   See also POWER, GPUARRAY, GPUARRAY/EYE.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:32 $
