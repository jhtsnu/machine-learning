%VERTCAT Vertical concatenation for gpuArray
%   C = VERTCAT(A,B,...) implements [A; B; ...] for gpuArrays.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.eye(N);
%       D2 = [D; D] % a 2000-by-1000 gpuArray matrix
%   
%   See also VERTCAT, GPUARRAY, GPUARRAY/CAT.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.12.2 $   $Date: 2010/09/20 14:37:40 $

