%ACOS Inverse cosine of gpuArray, result in radians
%   Y = ACOS(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.zeros(N);
%       E = acos(D)
%   
%   See also ACOS, GPUARRAY, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:23 $
