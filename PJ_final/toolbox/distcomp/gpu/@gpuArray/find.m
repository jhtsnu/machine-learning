%FIND Find indices of nonzero elements of gpuArray
%   If X is an m-by-n gpuArray with m ~= 1, then FIND(X) returns a p-by-1
%   gpuArray column vector containing the p indices of the nonzero or true
%   elements in X(:).
%   
%   If X is an 1-by-n gpuArray row vector, then FIND(X) returns a 1-by-p
%   gpuArray row vector containing the p indices of the nonzero or true
%   elements in X.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N) > 0.5 % build random array of ones and zeros
%       q = find(D) % find the indices where elements of D are non-zero
%   
%   See also FIND, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2011/01/07 23:13:17 $
