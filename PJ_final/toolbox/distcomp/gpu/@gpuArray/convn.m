%CONVN N-dimensional convolution of gpuArrays.
%   C = CONVN(A, B)
%   C = CONVN(A, B, SHAPE)
%   
%   A and B must be single or double arrays. Both real and complex types are supported.
%   
%   Example:
%    
%       N = 1000;
%       A = gpuArray.rand(2*N, N, 3);
%       B = gpuArray.rand(3, 3, 2);
%       C = convn(A, B);
%   
%   See also CONV, CONV2, GPUARRAY, GPUARRAY/CONV, GPUARRAY/CONV2, GPUARRAY/FILTER2.
%   


%   Copyright 2012 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2012/03/17 04:27:01 $
