function y = var(x,w,dim)
%VAR Variance.
%   Y = VAR(X) or VAR(X,0)
%   Y = VAR(X,1)
%   Y = VAR(X,W)
%   Y = VAR(X,W,DIM)
%   
%   Example:
%    
%       Nrow = 50;
%       Ncol = 100; 
%       X = gpuArray.rand(Nrow, Ncol);
%       W1 = gpuArray.rand(1, Ncol);
%       Y1 = var(X,W1,2); % Returns a 50 by 1 vector
%       W2 = gpuArray.rand(Nrow, 1); 
%       Y2 = var(X,W2,1); % Returns a 1 by 100 vector
%   
%   X and Y must be single or double arrays. Both real and complex types are supported.
%   
%   See also VAR, MEAN, STD, GPUARRAY/COV, GPUARRAY, CORRCOEF.
%   
%   


%   Copyright 2010-2012 The MathWorks, Inc.

if isIntegerData(x)
    error(message('MATLAB:var:integerClass'));
end

% If only dim is on the card, gather it and dispatch to MATLAB function.
if nargin == 3 && ~isa(x, 'gpuArray') && ~isa(w, 'gpuArray')
    dim = gather(dim);
    y = var(x, w, dim);
else
    % Bring both x and w to the card.
    if nargin >= 2 
        if ~isa(w, 'gpuArray')
            w = gpuArray(w);
        end
        
        if ~isa(x, 'gpuArray') 
            x = gpuArray(x);
        end
    end
    
    if nargin < 2 || isempty(w), w = gpuArray(0); end

    if nargin < 3
        % The output size for [] is a special case when DIM is not given.
        if isequal(x,[]), y = gpuArray.nan(classUnderlying(x)); return; end

        % Figure out which dimension sum will work along.
        dim = find(size(x) ~= 1, 1);
        if isempty(dim), dim = 1; end
    end
    n = size(x,dim);

    % Unweighted variance
    if isequal(w,0) || isequal(w,1)
        if w == 0 && n > 1
            % The unbiased estimator: divide by (n-1).  Can't do this
            % when n == 0 or 1.
            denom = n - 1;
        else
            % The biased estimator: divide by n.
            denom = n; % n==0 => return NaNs, n==1 => return zeros
        end

        if n > 0 % avoid divide-by-zero
            xbar = sum(x, dim) ./ n;
            x = bsxfun(@minus, x, xbar);
        end
        y = sum(abs(x).^2, dim) ./ denom; % abs guarantees a real result

        % Weighted variance
    elseif isvector(w) && all(w >= 0)
        if numel(w) ~= n
            if isscalar(w)
                error(message('MATLAB:var:invalidWgts'));
            else
                error(message('MATLAB:var:invalidSizeWgts'));
            end
        end

        % Normalize W, and embed it in the right number of dims.  Then
        % replicate it out along the non-working dims to match X's size.
        wresize = ones(1,max(ndims(x),dim)); wresize(dim) = n;
        w = reshape(w ./ sum(w), wresize);
        
        x0 = bsxfun(@times, w, x);
        
        x = bsxfun(@minus, x, sum(x0, dim));
        y = sum(bsxfun(@times, w, abs(x).*abs(x)), dim); % abs guarantees a real result
        
        %y = sum(arrayfun(@computeVar, x, sum(x0, dim), w), dim);
    else
        error(message('MATLAB:var:invalidWgts'));
    end
    
end

end


% ----------------------------------------------------------------------------
%function z = computeVar(x, y, w)

%    z = (abs(x - y) .* abs(x - y)) .* w;

%end



%-----------------------------------------------------------------------------
% Return true if x is not double, not single and not logical(Logical is 
% not counted as integer here).
function y = isIntegerData(x)

   if isa(x, 'gpuArray')
       y = ~hIsFloat(x) && ~strcmp(classUnderlying(x), 'logical');
   else
       y = isinteger(x);
   end

end
