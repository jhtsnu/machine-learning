%SURF overloaded for gpuArrays
%   SURF can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard SURF function. All valid syntaxes for SURF are
%   supported.
%   
%   See also SURF, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:52 $
