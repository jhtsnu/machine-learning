%SUM Sum of elements of gpuArray
%   SUM(X)
%   SUM(X,'double')
%   SUM(X,'native')
%   SUM(X,DIM)
%   SUM(X,DIM,'double')
%   SUM(X,DIM,'native')
%   
%   The order of the additions within the SUM operation is not defined, so
%   the SUM operation on a gpuArray might not return exactly the same 
%   answer as the SUM operation on the corresponding MATLAB numeric array.
%   In particular, the differences might be significant when X is a signed
%   integer type and its sum is accumulated natively.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.colon(1,N);
%       s = sum(D);
%       isequal(s, (1+N)*N/2) % true
%   
%   See also SUM, GPUARRAY, GPUARRAY/ZEROS.
%   
%   


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:39 $
