%* Matrix multiply for gpuArray
%   C = A * B
%   C = MTIMES(A,B)
%   
%   Example:
%    
%       N = 1000;
%       A = gpuArray.rand(N)
%       B = gpuArray.rand(N)
%       C = A * B
%   
%   See also MTIMES, GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.

