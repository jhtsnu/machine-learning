%GPUARRAY.RANDN gpuArray of normally distributed pseudorandom numbers
%   D = GPUARRAY.RANDN(N) is an N-by-N gpuArray matrix of normally
%   distributed pseudorandom numbers.
%   
%   D = GPUARRAY.RANDN(M,N) is an M-by-N gpuArray matrix
%   of normally distributed pseudorandom numbers.
%   
%   D = GPUARRAY.RANDN(M,N,P, ...) or GPUARRAY.RANDN([M,N,P, ...])
%   is an M-by-N-by-P-by-... gpuArray of normally distributed
%   pseudorandom numbers.
%   
%   D = GPUARRAY.RANDN(M,N,P,..., CLASSNAME) or 
%   GPUARRAY.RANDN([M,N,P,...], CLASSNAME) is an M-by-N-by-P-by-... 
%   gpuArray of normally distributed pseudorandom numbers of class 
%   specified by CLASSNAME.
%   
%   Examples:
%    
%       N  = 1000;
%       D1 = gpuArray.randn(N) % 1000-by-1000 gpuArray of randn
%       D2 = gpuArray.randn(N, N*2) % 1000-by-2000
%       D3 = gpuArray.randn([N, N*2], 'single') % underlying class 'single'
%   
%   See also RANDN, GPUARRAY, GPUARRAY/ZEROS, GPUARRAY/ONES


%   Copyright 2008-2011 The MathWorks, Inc.
