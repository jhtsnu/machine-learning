%gpuArray create data on the GPU
%   G = gpuArray( X ) copies the numeric data X to the GPU. This data can be 
%   operated on by passing it to the FEVAL method of parallel.gpu.CUDAKernel 
%   objects, or by using one of the methods defined for gpuArray objects.  See 
%   the Parallel Computing Toolbox documentation for a 
%   <a href="matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp.map'), 'GPU_BUILTINS')">list of methods supported by gpuArray</a>.  
%   
%   The MATLAB data X must be numeric (for example: single, double, int8 etc.)
%   or logical, and the GPU device must have sufficient free memory to store the
%   data. X must be full.
%   
%   Example:
%   X = rand( 10, 'single' );
%   G = gpuArray( X );
%   isequal( gather( G ), X )  % returns true
%   classUnderlying( G )       % returns 'single'
%   G2 = G .* G                % use "times" method defined for gpuArray objects
%   
%   See also GATHER
%   


%   Copyright 2010-2011 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2012/01/10 01:45:04 $
