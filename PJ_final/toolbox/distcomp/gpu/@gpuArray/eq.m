%== Equal for gpuArray
%   C = A == B
%   C = EQ(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       T = D == D
%       F = D == D'
%   
%   returns T = gpuArray.true(N) and F is probably the same as
%   logical(gpuArray.eye(N)).
%   
%   See also EQ, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:57 $
