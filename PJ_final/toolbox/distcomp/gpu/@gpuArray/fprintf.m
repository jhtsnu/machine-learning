%FPRINTF overloaded for gpuArrays
%   FPRINTF can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard FPRINTF function. All valid syntaxes for FPRINTF are
%   supported.
%   
%   See also FPRINTF, GPUARRAY.


%   Copyright 2010-2011 The MathWorks, Inc.
