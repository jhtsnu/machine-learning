%ACOSH Inverse hyperbolic cosine of gpuArray
%   Y = ACOSH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.zeros(N);
%       E = acosh(D)
%   
%   See also ACOSH, GPUARRAY, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:24 $
