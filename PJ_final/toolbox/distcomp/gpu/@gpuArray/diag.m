%DIAG Diagonal matrices and diagonals of a gpuArray matrix
%   
%   A = DIAG(D,K) when D is a gpuArray vector with N components results 
%   in a square gpuArray matrix A of order N+ABS(K) with the elements of 
%   D along the K-th diagonal of A.  Recall that K = 0 is the main diagonal, 
%   K > 0 is above the main diagonal, and K < 0 is below the main diagonal.
%   
%   A = DIAG(D) is the same as A = DIAG(D,0) and puts D along the main 
%   diagonal of A.
%   
%   D = DIAG(A,K) when A is a gpuArray matrix results in a gpuArray 
%   column vector D formed from the elements of the K-th diagonal of A.  
%   
%   D = DIAG(A) is the same as D = DIAG(A,0) and D is the main diagonal 
%   of A. Note that DIAG(DIAG(A)) results in a gpuArray diagonal matrix.
%   
%   Example:
%    
%       N = 1000;
%       d = gpuArray.colon(N,-1,1)'
%       d2 = gpuArray.colon(1,ceil(N/2))'
%       D = diag(d) + diag(d2,floor(N/2))
%   
%   creates two gpuArray column vectors d and d2 and then populates the
%   gpuArray matrix D with them as diagonals.
%   
%   See also DIAG, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.
%   


%   Copyright 2006-2011 The MathWorks, Inc.
