function dh = dispInternal( obj, className, objName )
;%#ok<NOSEM> Undocumented

% Use shared DisplayHelpers to work out how to display an array

% Copyright 2008-2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2010/12/27 01:10:54 $

if numel( obj ) == 0
    % Send the gathered empty data through to the helper so that it has the
    % correct underlying class (i.e. int8/single/whatever)
    dh = parallel.internal.shared.DisplayHelperDense( objName, className, gather( obj ), size( obj ) );
else
    N = 1000;
    dh = iFirstNNumericDisplayHelper( obj, className, objName, N );
end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% For a numeric style distributed array, display the first N entries.
function dh = iFirstNNumericDisplayHelper( x, className, name, N )

    totalEls = numel( x ); 
    if totalEls > N
        rangeStruct = parallel.internal.shared.denseDisplayRangeHelper( x, N );
        maybeTruncatedValue = transferPortion( x, rangeStruct );
    else
        maybeTruncatedValue = gather( x );
    end
    % Using the 4-arg ctor since we always truncate from 1:N in each dim
    dh = parallel.internal.shared.DisplayHelperDense( ...
                name, className, maybeTruncatedValue, size( x ) );
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Gather a portion of the array
function data = transferPortion( x, rangeStruct )
N = numel( rangeStruct.start );
szArgs = cell( N, 1 );
for ii=1:N
    szArgs{ii} = rangeStruct.start(ii):rangeStruct.end(ii);
end
% Form subsref parameters
s = struct( 'type', '()', 'subs', {szArgs} );
data = gather( subsref( x, s ) );
end
