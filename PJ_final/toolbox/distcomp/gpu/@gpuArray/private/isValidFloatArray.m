function tf = isValidFloatArray(x)

%   Copyright 2010-2011 The MathWorks, Inc.

    if isa( x, 'gpuArray' )
        tf = hIsFloat(x);
    else
        tf = isfloat(x);
    end    
end