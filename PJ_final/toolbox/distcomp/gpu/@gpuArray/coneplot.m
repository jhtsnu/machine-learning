%CONEPLOT overloaded for gpuArrays
%   CONEPLOT can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard CONEPLOT function. All valid syntaxes for CONEPLOT are
%   supported.
%   
%   See also CONEPLOT, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:41 $
