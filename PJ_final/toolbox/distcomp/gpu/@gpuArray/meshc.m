%MESHC overloaded for gpuArrays
%   MESHC can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard MESHC function. All valid syntaxes for MESHC are
%   supported.
%   
%   See also MESHC, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:17 $
