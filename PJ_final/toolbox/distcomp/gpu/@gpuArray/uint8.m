%UINT8 Convert gpuArray to unsigned 8-bit integer
%   I = UINT8(X)
%   
%   Example:
%    
%       N = 1000;
%       Di = gpuArray.ones(N,'int8');
%       Du = uint8(Di)
%       classDi = classUnderlying(Di)
%       classDu = classUnderlying(Du)
%   
%   converts the N-by-N int8 gpuArray Di to the
%   uint8 gpuArray Du.
%   classDi is 'int8' while classDu is 'uint8'.
%   
%   See also UINT8, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:46 $
