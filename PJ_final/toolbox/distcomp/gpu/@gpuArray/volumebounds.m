%VOLUMEBOUNDS overloaded for gpuArrays
%   VOLUMEBOUNDS can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard VOLUMEBOUNDS function. All valid syntaxes for VOLUMEBOUNDS are
%   supported.
%   
%   See also VOLUMEBOUNDS, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:58 $
