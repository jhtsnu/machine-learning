%ACSC Inverse cosecant of gpuArray, result in radian
%   Y = ACSC(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = acsc(D)
%   
%   See also ACSC, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:28 $
