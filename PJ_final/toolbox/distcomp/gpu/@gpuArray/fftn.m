%FFTN N-dimensional discrete Fourier Transform.
%   Y = FFTN(X) returns the N-dimensional discrete Fourier transform
%   of gpuArray X.  If X is a vector, the output will have
%   the same orientation.
%   
%   Y = FFTN(X, SIZ) pads or crops X to size SIZ before performing 
%   the transform.
%   
%   Example:
%    
%       N = 64;
%       D = gpuArray.rand(N, N, N);
%       F = fftn(D);
%   
%   See also FFTN, GPUARRAY.
%   


%   Copyright 2011 The MathWorks, Inc.
% $Revision: 1.1.6.1 $  $Date: 2011/09/26 23:38:01 $
