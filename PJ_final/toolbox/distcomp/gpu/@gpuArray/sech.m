%SECH Hyperbolic secant of gpuArray
%   Y = SECH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.zeros(N);
%       E = sech(D)
%   
%   See also SECH, GPUARRAY, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:41 $
