%MESHZ overloaded for gpuArrays
%   MESHZ can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard MESHZ function. All valid syntaxes for MESHZ are
%   supported.
%   
%   See also MESHZ, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:18 $
