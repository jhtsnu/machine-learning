%BETA  Beta function for gpuArrays
%   Y = BETA(Z,W) computes the beta function for corresponding elements
%   of Z and W. The arrays Z and W must be real and nonnegative. Both arrays
%   must be the same size, or either can be scalar.
%   
%   See also BETA, GPUARRAY/BETALN, GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.
