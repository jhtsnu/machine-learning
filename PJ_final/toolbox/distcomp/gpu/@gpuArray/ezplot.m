%EZPLOT overloaded for gpuArrays
%   EZPLOT can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard EZPLOT function. All valid syntaxes for EZPLOT are
%   supported.
%   
%   See also EZPLOT, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:55 $
