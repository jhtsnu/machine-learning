%ABS Absolute value of gpuArray
%   Y = ABS(X)
%   
%   Example:
%    
%       N = 1000;
%       D = -3*gpuArray.ones(N)
%       absD = abs(D)
%   
%   
%   See also ABS, GPUARRAY.


%   Copyright 2006-2011 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:21 $
