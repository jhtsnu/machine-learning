%' Complex conjugate transpose of gpuArray
%   E = D'
%   E = CTRANSPOSE(D)
%   
%   Example:
%    
%       N = 1000;
%       D = complex(gpuArray.rand(N),gpuArray.rand(N))
%       E = D'
%   
%   See also CTRANSPOSE, GPUARRAY, GPUARRAY/COMPLEX.


%   Copyright 2006-2010 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/05/10 17:09:09 $
