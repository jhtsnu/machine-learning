%SLICE overloaded for gpuArrays
%   SLICE can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard SLICE function. All valid syntaxes for SLICE are
%   supported.
%   
%   See also SLICE, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:38 $
