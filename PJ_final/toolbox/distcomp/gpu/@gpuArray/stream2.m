%STREAM2 overloaded for gpuArrays
%   STREAM2 can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard STREAM2 function. All valid syntaxes for STREAM2 are
%   supported.
%   
%   See also STREAM2, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:44 $
