%SUB2IND overloaded for gpuArrays
%   SUB2IND is used to determine the equivalent single index corresponding
%   to a given set of subscript values.
%   
%   Example:
%    
%       rows = gpuArray.colon(1,2,10);
%       cols = gpuArray.colon(2,2,10);
%       sz = [20 20];
%       ind = sub2ind(sz,rows,cols)
%   
%   See also SUB2IND, GPUARRAY.IND2SUB, GPUARRAY.


%   Copyright 2011 The MathWorks, Inc.
