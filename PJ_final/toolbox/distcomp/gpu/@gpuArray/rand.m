%GPUARRAY.RAND gpuArray of uniformly distributed pseudorandom numbers
%   D = GPUARRAY.RAND(N) is an N-by-N gpuArray matrix of uniformly 
%   distributed pseudorandom numbers.
%   
%   D = GPUARRAY.RAND(M,N) is an M-by-N gpuArray matrix
%   of uniformly distributed pseudorandom numbers.
%   
%   D = GPUARRAY.RAND(M,N,P, ...) or GPUARRAY.RAND([M,N,P, ...])
%   is an M-by-N-by-P-by-... gpuArray of uniformly distributed
%   pseudorandom numbers.
%   
%   D = GPUARRAY.RAND(M,N,P,..., CLASSNAME) or 
%   GPUARRAY.RAND([M,N,P,...], CLASSNAME) is an M-by-N-by-P-by-... 
%   gpuArray of uniformly distributed pseudorandom numbers of class 
%   specified by CLASSNAME.
%   
%   Examples:
%    
%       N  = 1000;
%       D1 = gpuArray.rand(N) % 1000-by-1000 gpuArray of rand
%       D2 = gpuArray.rand(N, N*2) % 1000-by-2000
%       D3 = gpuArray.rand([N, N*2], 'single') % underlying class 'single'
%   
%   See also RAND, GPUARRAY, GPUARRAY/ZEROS, GPUARRAY/ONES


%   Copyright 2008-2011 The MathWorks, Inc.
