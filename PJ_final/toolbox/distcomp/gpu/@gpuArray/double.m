%DOUBLE Convert gpuArray to double precision
%   Y = DOUBLE(X)
%   
%   Example:
%    
%       N = 1000;
%       Ds = gpuArray.ones(N,'single');
%       Dd = double(Ds)
%       classDs = classUnderlying(Ds)
%       classDd = classUnderlying(Dd)
%   
%   takes the N-by-N gpuArray single matrix Ds and converts
%   it to the gpuArray double matrix Dd.
%   classDs is 'single' while classDd is 'double'.
%   
%   See also DOUBLE, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:40:56 $
