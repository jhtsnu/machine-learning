%NUM2STR overloaded for gpuArrays
%   NUM2STR can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard NUM2STR function. All valid syntaxes for NUM2STR are
%   supported.
%   
%   See also NUM2STR, GPUARRAY.


%   Copyright 2010-2011 The MathWorks, Inc.
