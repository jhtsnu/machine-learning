%.\ Left array divide for gpuArray matrix
%   C = A .\ B
%   C = LDIVIDE(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.colon(1, N)'
%       D2 = D1 .\ 1 
%   
%   See also LDIVIDE, GPUARRAY, GPUARRAY/COLON.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.3 $ $Date: 2010/10/07 15:45:18 $
