%AREA overloaded for gpuArrays
%   AREA can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard AREA function. All valid syntaxes for AREA are
%   supported.
%   
%   See also AREA, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:32 $
