%/ Slash or right matrix divide for gpuArray
%   C = A / B
%   C = MRDIVIDE(A,B)
%   
%   A and B must be full gpuArrays of floating point numbers (single or double). 
%   
%   Example:
%    
%       N = 1000;
%       D1 = gpuArray.colon(1, N)';
%       D2 = D1 / 2;
%   
%   See also MRDIVIDE, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2012 The MathWorks, Inc.
