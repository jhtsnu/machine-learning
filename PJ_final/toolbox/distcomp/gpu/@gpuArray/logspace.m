%GPUARRAY.LOGSPACE Logspace gpuArray
%   GPUARRAY.LOGSPACE(X1, X2) generates a row vector of 50 logarithmically
%   equally spaced points between decades 10^X1 and 10^X2.  If X2
%   is pi, then the points are between 10^X1 and pi.
%   
%   GPUARRAY.LOGSPACE(X1, X2, N) generates N points.
%   For N < 2, LOGSPACE returns 10^X2.
%   
%   X1 and X2 must be a single or double scalar. Both real and complex types are supported.
%   
%   Examples:
%    
%       N  = 1000;
%       a = 0;
%       b = 10;
%       D = gpuArray.logspace(a, b, N); 
%   
%   See also LOGSPACE, GPUARRAY, GPUARRAY/LINSPACE.
%   


%   Copyright 2010-2012 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2010/08/02 23:09:43 $


