%INT8 Convert gpuArray to signed 8-bit integer
%   I = INT8(X)
%   
%   Example:
%    
%       N = 1000;
%       Du = gpuArray.ones(N,'uint8');
%       Di = int8(Du)
%       classDu = classUnderlying(Du)
%       classDi = classUnderlying(Di)
%   
%   converts the N-by-N uint8 gpuArray Du to the
%   int8 gpuArray Di.
%   classDu is 'uint8' while classDi is 'int8'.
%   
%   See also INT8, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:15 $
