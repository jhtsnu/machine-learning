function disp( obj )
%DISP Display gpuArray
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       disp(D);
%   
%   See also DISP, GPUARRAY, GPUARRAY/DISPLAY.
%   


%   Copyright 2008-2011 The MathWorks, Inc.

if ~existsOnGPU( obj )
    disp( getString(message('parallel:gpu:array:NoLongerExists')) );
    if ~isequal( get( 0, 'FormatSpacing' ), 'compact' )
        disp( ' ' );
    end
else
    thisClassName = class( obj );
    noName = '';
    dh = dispInternal( obj, thisClassName, noName );
    dh.doDisp();
end

end
