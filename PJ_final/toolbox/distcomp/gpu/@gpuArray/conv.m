%CONV Convolution and polynomial multiplication of gpuArrays.
%   C = CONV(A, B)
%   C = CONV(A, B, SHAPE)
%   
%   A and B must be single or double arrays. Both real and complex types are supported. 
%   
%   Example:
%    
%            N = 1000;
%            A = gpuArray.ones(N, 1);
%            B = gpuArray.rand(2*N,1);
%            C = conv(A, B);
%    
%   See also CONV, GPUARRAY, GPUARRAY/CONV2, GPUARRAY/FILTER.
%   


%   Copyright 2010-2012 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/10/07 15:44:45 $
