%INT32 Convert gpuArray to signed 32-bit integer
%   I = INT32(X)
%   
%   Example:
%    
%       N = 1000;
%       Du = gpuArray.ones(N,'uint32');
%       Di = int32(Du)
%       classDu = classUnderlying(Du)
%       classDi = classUnderlying(Di)
%   
%   converts the N-by-N uint32 gpuArray Du to the
%   int32 gpuArray Di.
%   classDu is 'uint32' while classDi is 'int32'.
%   
%   See also INT32, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:13 $
