%MAT2STR overloaded for gpuArrays
%   MAT2STR can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard MAT2STR function. All valid syntaxes for MAT2STR are
%   supported.
%   
%   See also MAT2STR, GPUARRAY.


%   Copyright 2010-2011 The MathWorks, Inc.
