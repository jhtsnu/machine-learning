%INT64 Convert gpuArray to signed 64-bit integer
%   I = INT64(X)
%   
%   Example:
%    
%       N = 1000;
%       Du = gpuArray.ones(N,'uint64');
%       Di = int64(Du)
%       classDu = classUnderlying(Du)
%       classDi = classUnderlying(Di)
%   
%   converts the N-by-N uint64 gpuArray Du to the
%   int64 gpuArray Di.
%   classDu is 'uint64' while classDi is 'int64'.
%   
%   See also INT64, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:14 $
