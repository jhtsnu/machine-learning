%FULL overloaded for gpuArrays
%   F = FULL(D)
%   
%   Returns the gpuArray D. Because sparse gpuArrays are not supported, D is 
%   already full.
%   See also FULL, gpuArray.


%   Copyright 2006-2011 The MathWorks, Inc.
