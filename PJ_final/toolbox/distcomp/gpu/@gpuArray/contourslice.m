%CONTOURSLICE overloaded for gpuArrays
%   CONTOURSLICE can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard CONTOURSLICE function. All valid syntaxes for CONTOURSLICE are
%   supported.
%   
%   See also CONTOURSLICE, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:46 $
