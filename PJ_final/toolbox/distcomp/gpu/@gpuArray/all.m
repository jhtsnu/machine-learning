%ALL True if all elements of a gpuArray vector are nonzero
%   A = ALL(D)
%   A = ALL(D,DIM)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.colon(1,N)
%       t = all(D)
%   
%   returns t the gpuArray logical scalar with value true.
%   
%   See also ALL, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:40:46 $
