%>= Greater than or equal for gpuArray
%   C = A >= B
%   C = GE(A,B)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       T = D >= D
%       F = D >= D+0.5
%   
%   returns T = gpuArray.true(N)
%   and F = gpuArray.false(N).
%   
%   See also GE, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:11 $
