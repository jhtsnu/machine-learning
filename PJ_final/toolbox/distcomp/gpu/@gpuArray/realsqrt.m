%REALSQRT Real square root of gpuArray
%   Y = REALSQRT(X)
%   
%   Example:
%    
%       N = 1000;
%       D = -4*gpuArray.ones(N)
%       try realsqrt(D), catch, disp('negative input!'), end
%       E = realsqrt(-D)
%   
%   See also REALSQRT, GPUARRAY.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:36 $
