%ISEQUALWITHEQUALNANS True if gpuArrays are numerically equal
%   TF = ISEQUALWITHEQUALNANS(A,B)
%   TF = ISEQUALWITHEQUALNANS(A,B,C,...)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.nan(N);
%       f = isequal(D,D)
%       t = isequalwithequalnans(D,D)
%   
%   returns f = false and t = true.
%   
%   See also ISEQUALWITHEQUALNANS, GPUARRAY, GPUARRAY/NAN.


%   Copyright 2006-2011 The MathWorks, Inc.
