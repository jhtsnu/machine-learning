%BITCMP Complement bits of gpuArray
%   C = BITCMP(G) returns the bitwise complement of G, where G is an unsigned
%   gpuArray containing integers or unsigned integers.
%   
%   Example:
%    
%      a = gpuArray.ones(10, 'uint32') .* uint32(intmax('uint8'));
%      bitcmp(a)
%   
%   See also BITCMP, GPUARRAY, GPUARRAY/ONES.
%   


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:40 $
