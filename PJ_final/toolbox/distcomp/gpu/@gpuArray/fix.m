%FIX Round gpuArray towards zero
%   Y = FIX(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.colon(1,N)./2
%       E = fix(D)
%   
%   See also FIX, GPUARRAY, GPUARRAY/COLON, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:07 $
