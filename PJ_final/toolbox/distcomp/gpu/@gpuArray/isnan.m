%ISNAN True for Not-a-Number elements of gpuArray
%   TF = ISNAN(D)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.nan(N);
%       T = isnan(D)
%   
%   returns T = gpuArray.true(size(D)).
%   
%   See also ISNAN, GPUARRAY, GPUARRAY/NAN.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:45:16 $
