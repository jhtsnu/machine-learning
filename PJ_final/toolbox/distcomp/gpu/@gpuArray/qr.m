%QR Orthogonal-triangular decomposition for gpuArray matrix
%   [Q,R] = QR(D)
%   [Q,R] = QR(D,0)
%   
%   D must be a full gpuArray matrix of floating point numbers (single or double).
%   
%   The following syntaxes are not supported for full gpuArray D:
%   
%   X = QR(D)
%   X = QR(D,0)
%   [Q,R,E] = QR(D)
%   [Q,R,e] = QR(D,0)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.rand(N);
%       [Q,R] = qr(D);
%       norm(Q*R-D,'inf')
%   
%   See also QR, GPUARRAY.


%   Copyright 2010-2012 The MathWorks, Inc.
