%SVD Singular value decomposition of gpuArray matrix
%   S = SVD(A) 
%   [U,S,V] = SVD(A)
%   [...] = SVD(A,'econ')
%   [...] = SVD(A,0)
%   
%   Example:
%       N = 1000;
%       A = gpuArray.rand(N);
%       [U,S,V] = svd(A);
%       norm(A*V-U*S,1)
%   
%   See also SVD, GPUARRAY, GPUARRAY/RAND


%   Copyright 2006-2012 The MathWorks, Inc.

