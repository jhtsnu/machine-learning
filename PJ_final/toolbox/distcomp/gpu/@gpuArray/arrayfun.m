%ARRAYFUN Apply a function to each element of gpuArray
%   This method of gpuArray is very similar in behavior to the MATLAB
%   function ARRAYFUN, except that the actual evaluation of the function
%   happens on the GPU, not on the CPU. Thus any required data not
%   already on the GPU is moved to GPU memory, and the MATLAB function
%   referenced by FUN is then executed on the GPU. All the output 
%   arguments are returned as gpuArrays whose data can be retrieved with
%   the GATHER method.
%   
%   A = ARRAYFUN(FUN, B) applies the function specified by FUN to each element
%   of the gpuArray B, and returns the results in gpuArray
%   A.  The array A is the same size as B, and the (I,J,...)th element of A is 
%   equal to FUN(B(I,J,...)). FUN is a function handle to a function that takes
%   one input argument and returns a scalar value. FUN must return values of 
%   the same class each time it is called.  The order in which ARRAYFUN computes
%   elements of A is not specified and should not be relied on.
%   The input must be an array of one of the following types:  numeric, logical, 
%   or gpuArray. 
%   
%   FUN must be a handle to a function that is written in the MATLAB
%   language (i.e., not a built-in function or a mex function).
%   
%   The subset of the MATLAB language that is currently supported for 
%   execution on the GPU can be found <a href="matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp.map'), 'GPU_SUPPORTED_MATLAB')">in the documentation.</a>
%   
%   A = ARRAYFUN(FUN, B, C,  ...) evaluates FUN using elements of the 
%   arrays B, C,  ... as input arguments with scalar expansion enabled.
%   Any of the inputs that are scalar will be virtually replicated to 
%   match the size of the other arrays.  The (I,J,...)th element of the
%   gpuArray A is then equal to FUN(B(I,J,...), C(I,J,...), ...).  
%   
%   At least one of the inputs B, C, ... must be a gpuArray.  Any other
%   inputs held in CPU memory will be converted to a gpuArray before 
%   calling the function on the GPU. If an array is to be used in several
%   different ARRAYFUN calls it is more efficient to convert that array 
%   to a gpuArray before you call the series of ARRAYFUN methods.
%   
%   [A, B, ...] = ARRAYFUN(FUN, C, ...), where FUN is a function handle
%   to a function that returns multiple outputs, returns gpuArrays
%   A, B,..., each corresponding to one of the output arguments of FUN.  ARRAYFUN
%   calls FUN each time with as many outputs as there are in the call to ARRAYFUN.
%   FUN can return output arguments having different classes, but the class of
%   each output must be the same each time FUN is called. This means that all
%   elements of A must be the same class; B can be a different class from A, but
%   all elements of B must be of the same class.
%   
%   Although the MATLAB arrayfun function allows you to specify optional 
%   parameter name/value pairs, these are not supported by the gpuArray 
%   arrayfun method.
%   
%   Examples
%   If the MATLAB function aFunction is defined as follows:
%   function [o1, o2] = aFunction(a, b, c)
%       o1 = a + b;
%       o2 = o1 .* c + 2;
%     
%   Then this function can be executed on the GPU as follows:
%    
%       N = 1000;
%       s1 = gpuArray.rand(N);
%       s2 = gpuArray.rand(N);
%       s3 = gpuArray.rand(N);
%       [o1, o2] = arrayfun(@aFunction, s1, s2, s3)
%   
%   See also  ARRAYFUN, GPUARRAY, function_handle, gather


%   Copyright 2010-2011 The MathWorks, Inc.
% $Revision: 1.1.10.2 $   $Date: 2010/09/20 14:37:25 $
