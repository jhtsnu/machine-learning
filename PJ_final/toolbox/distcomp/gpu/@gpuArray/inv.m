%INV    Matrix inverse for gpuArrays
%   X = INV(C) is the inverse of gpuArray matrix C.
%   
%   C must be a square full gpuArray matrix of floating 
%   point numbers (single or double).
%   
%   The MATLAB INV function prints a warning if C is badly scaled
%   or nearly singular. The gpuArray INV method cannot check for 
%   this condition; therefore, it is unable to warn if there is 
%   a violation. You should be aware of this possibility and take 
%   action to avoid it.
%   
%   Example:
%   C multiplied on either side by its inverse should be close to the
%   identity matrix.
%   
%    
%       N = 1000;
%       C = gpuArray.rand(N);
%       Cinv = inv( C );
%       CinvTimesC = norm(gpuArray.eye(N) - Cinv*C)
%       CTimesCinv = norm(gpuArray.eye(N) - C*Cinv)
%   
%   See also INV, GPUARRAY.


%   Copyright 2010-2012 The MathWorks, Inc.
