%SUBSREF Subscripted reference for gpuArray
%   B = A(I)
%   B = A(I,J)
%   B = A(I,J,K,...)
%   
%   The index I in A(I) must be :, scalar or a vector.
%   
%   A{...} indexing is not supported for gpuArray cell arrays.
%   A.field indexing is not supported for gpuArrays of structs.
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.eye(N);
%       one = D(N,N)
%   
%   See also SUBSREF, GPUARRAY, GPUARRAY/EYE.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.2 $   $Date: 2010/09/20 14:37:39 $
