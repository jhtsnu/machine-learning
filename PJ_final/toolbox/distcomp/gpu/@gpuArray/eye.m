%GPUARRAY.EYE Identity gpuArray matrix
%   D = GPUARRAY.EYE(N) is the N-by-N gpuArray matrix with ones on
%   the diagonal and zeros elsewhere.
%   
%   D = GPUARRAY.EYE(M,N) or GPUARRAY.EYE([M,N]) is the M-by-N 
%   gpuArray matrix with ones on the diagonal and zeros elsewhere.
%   
%   D = GPUARRAY.EYE() is the gpuArray scalar 1.
%   
%   D = GPUARRAY.EYE(M,N,CLASSNAME) or GPUARRAY.EYE([M,N],CLASSNAME)
%   is the M-by-N gpuArray identity matrix with underlying data of 
%   class CLASSNAME.
%   
%   Example:
%    
%       N = 1000;
%       % Create a 1000-by-1000 gpuArray with underlying class 'int32'.
%       D1 = gpuArray.eye(N,'int32');
%   
%   See also EYE, GPUARRAY.
%   


%   Copyright 2008-2011 The MathWorks, Inc.
