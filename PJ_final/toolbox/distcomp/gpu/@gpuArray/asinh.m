%ASINH Inverse hyperbolic sine of gpuArray
%   Y = ASINH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.zeros(N);
%       E = asinh(D)
%   
%   See also ASINH, GPUARRAY, GPUARRAY/ZEROS.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:34 $
