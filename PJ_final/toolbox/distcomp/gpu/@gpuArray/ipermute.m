%IPERMUTE Inverse permute gpuArray dimensions
%   A = IPERMUTE(B, ORDER)
%   
%   Example:
%    
%       A = gpuArray.rand(1,2,3,4);
%       B = permute(A, [3 2 1 4]); 
%       C = ipermute(B, [3 2 1 4]);
%       isequal(A, C) % A and C are equal
%   
%   See also IPERMUTE, PERMUTE, GPUARRAY, GPUARRAY/RAND.


%   Copyright 2011 The MathWorks, Inc.

