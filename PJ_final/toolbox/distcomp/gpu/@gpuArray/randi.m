%GPUARRAY.RANDI Create a gpuArray of random integers
%   
%   D = GPUARRAY.RANDI(IMAX, N) returns an N-by-N gpuArray containing
%   pseudorandom integer values drawn from the discrete uniform distribution on
%   1:IMAX.
%   
%   D = GPUARRAY.RANDI(IMAX,M,N) or D = GPUARRAY.RANDI(IMAX,[M,N]) returns an M-by-N
%   gpuArray. D = GPUARRAY.RANDI(IMAX,M,N,P,...) or D =
%   GPUARRAY.RANDI(IMAX,[M,N,P,...])  returns an M-by-N-by-P-... gpuArray.
%   
%   D = GPUARRAY.RANDI([IMIN, IMAX], ...) returns a gpuArray containing integer
%   values drawn from the discrete uniform distribution on IMIN:IMAX.
%   
%   D = GPUARRAY.RANDI(..., CLASSNAME) returns a gpuArray with underlying class
%   CLASSNAME.
%   
%   Examples:
%    
%       % Generate random integers from the uniform distribution on the set 1:10:
%       D = gpuArray.randi(10, 1, 100)
%       % Generate a gpuArray containing random integers from -10:10 
%       % of underlying class 'int32':
%       D = gpuArray.randi([-10 10], 1, 100, 'int32')
%   
%   See also RANDI, GPUARRAY.


%   Copyright 2008-2011 The MathWorks, Inc.
