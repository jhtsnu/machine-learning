%ASEC Inverse secant of gpuArray, result in radians
%   Y = ASEC(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = asec(D)
%   
%   See also ASEC, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:30 $
