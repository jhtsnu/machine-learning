%COTH Hyperbolic cotangent of gpuArray
%   Y = COTH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.inf(N);
%       E = coth(D)
%   
%   See also COTH, GPUARRAY, GPUARRAY/INF.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:50 $
