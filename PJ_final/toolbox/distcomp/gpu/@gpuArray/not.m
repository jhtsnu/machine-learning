%~ Logical NOT for gpuArray
%   B = ~A
%   B = NOT(A)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.eye(N);
%       E = ~D
%   
%   See also NOT, GPUARRAY, GPUARRAY/EYE.
%   


%   Copyright 2006-2011 The MathWorks, Inc.

