%CONV2 Two dimensional convolution of gpuArrays.
%   C = CONV2(A, B)
%   C = CONV2(H1, H2, A)
%   C = CONV2(..., SHAPE)
%   
%   A and B must be single or double arrays. Both real and complex types are supported.
%   
%   Example:
%    
%            N = 500;
%            A = gpuArray.ones(2*N, N);
%            B = gpuArray.rand(N);
%            C = conv2(A, B);
%   
%   See also CONV2, GPUARRAY, GPUARRAY/CONV, GPUARRAY/FILTER2.
%   


%   Copyright 2010-2012 The MathWorks, Inc.
%   $Revision: 1.1.6.1 $  $Date: 2010/10/07 15:44:46 $
