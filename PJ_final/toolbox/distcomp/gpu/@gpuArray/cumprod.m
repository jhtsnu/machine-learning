%CUMPROD Cumulative product of elements of gpuArray
%   CUMPROD(X)
%   CUMPROD(X,DIM)
%   
%   The order of the products within the CUMPROD operation is not defined, so
%   the CUMPROD operation on a gpuArray might not return exactly the same 
%   answer as the CUMPROD operation on the corresponding MATLAB numeric array.
%   In particular, the differences might be significant when X is a signed
%   integer type.
%   
%   Example:
%    
%       N = 1000;
%       D = 4 * (gpuArray.colon(1, N) .^ 2);
%       D2 = D ./ (D - 1);
%       c = cumprod(D2);
%       c1 = cumprod(D2,1);
%       c2 = cumprod(D2,2);
%       isequal(c1, D2) % true
%       isequal(c, c2)  % true
%       norm(c(end)-pi/2,'inf')
%   
%   c(end) is approximately pi/2 (by the Wallis product)
%   
%   See also CUMPROD, GPUARRAY, GPUARRAY/COLON.


%   Copyright 2006-2012 The MathWorks, Inc.
% $Revision: 1.1.6.1 $   $Date: 2010/10/07 15:44:54 $
