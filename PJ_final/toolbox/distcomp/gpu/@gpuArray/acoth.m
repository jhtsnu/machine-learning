%ACOTH Inverse hyperbolic cotangent of gpuArray
%   Y = ACOTH(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.inf(N);
%       E = acoth(D)
%   
%   See also ACOTH, GPUARRAY, GPUARRAY/INF.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.6.3 $ $Date: 2010/10/07 15:44:26 $
