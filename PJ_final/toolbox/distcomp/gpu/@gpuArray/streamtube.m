%STREAMTUBE overloaded for gpuArrays
%   STREAMTUBE can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard STREAMTUBE function. All valid syntaxes for STREAMTUBE are
%   supported.
%   
%   See also STREAMTUBE, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:25:50 $
