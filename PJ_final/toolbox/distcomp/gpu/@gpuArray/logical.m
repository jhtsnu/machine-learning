%LOGICAL Convert numeric values of gpuArray to logical
%   L = LOGICAL(X)
%   
%   Example:
%    
%       N = 1000;
%       Du = gpuArray.ones(N,'uint8');
%       Dl = logical(Du)
%       classDu = classUnderlying(Du)
%       classDl = classUnderlying(Dl)
%   
%   converts the N-by-N uint8 gpuArray Du to the
%   logical gpuArray Dl.
%   classDu is 'uint8' while classDl is 'logical'.
%   
%   See also LOGICAL, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.1 $   $Date: 2010/07/06 16:41:23 $
