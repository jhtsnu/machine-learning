%NDGRID Generate gpuArrays for N-D functions and interpolation.
%   [X1,X2,X3,...] = NDGRID(x1,x2,x3,...) 
%   [X1,X2,...] = NDGRID(x) is the same as [X1,X2,...] = NDGRID(x,x,...)
%       
%   NDGRID for gpuArrays supports the same syntax as the builtin, with one 
%   notable exception.  In the 1-D case, X = NDGRID(x) returns a gpuArray 
%   column vector X that contains the elements of the input gpuArray x for 
%   use as a one-dimensional grid.
%   
%   Class support for inputs:
%          float: double, single
%   
%   Example:
%    
%            N = 1000;
%            x = gpuArray.ones(N, 1);
%            y = gpuArray.rand(2*N,1);
%            [X, Y] = ndgrid(x, y);
%   
%   See also NDGRID, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2010-2011 The MathWorks, Inc.



