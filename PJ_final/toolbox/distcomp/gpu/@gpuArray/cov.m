function xy = cov(x,varargin)
%COV Covariance matrix.
%   xy = COV(X) or COV(X,0)
%   xy = COV(X,1)
%   xy = COV(X,Y) 
%   xy = COV(X,Y,0)
%   xy = COV(X,Y,1)
%   
%   Example:
%    
%       Nrow = 50;
%       Ncol = 100; 
%       X = gpuArray.rand(Nrow, Ncol);
%       xy = cov(X);     % Returns a 100 by 100 matrix.
%       Y = gpuArray.rand(Nrow, Ncol);
%       xy = cov(X,Y);   % Returns a 2 by 2 matrix.
%   
%   X and Y must be single or double matrics. Both real and complex types are supported.
%   
%   See also COV, CORRCOEF, GPUARRAY, GPUARRAY/VAR, STD, MEAN.
%   


%   Copyright 2010-2012 The MathWorks, Inc.
% The logic in this file copies the MATLAB cov


    if nargin==0
        error(message('MATLAB:cov:NotEnoughInputs'));
    end
    if nargin>3
        error(message('MATLAB:cov:TooManyInputs'));
    end
    if ~ismatrix(x)
        error(message('MATLAB:cov:InputDim'));
    end

    nin = nargin;

    % Check for cov(x,flag) or cov(x,y,flag)
    if nin==3
        flag = varargin{end};
        if ~iscovflag(flag)
            error(message('MATLAB:cov:notScalarFlag'));
        end
        nin = nin - 1;
    elseif nin==2 && iscovflag(varargin{end})
        flag = varargin{end};
        nin = nin - 1;
    else
        flag = 0;
    end

    scalarxy = false; % cov(scalar,scalar) is an ambiguous case
    if nin == 2
        y = varargin{1};
        if ~ismatrix(y)
            error(message('MATLAB:cov:InputDim'));
        end

        x = reshape(x, [numel(x),1]);
        y = reshape(y, [numel(y),1]);

        if length(x) ~= length(y),
            error(message('MATLAB:cov:XYlengthMismatch'));
        end
        scalarxy = isscalar(x) && isscalar(y);

        x = [x y];
    end

    if isvector(x) && ~scalarxy
        x = reshape(x, [length(x), 1]);
    end

    [m,n] = size(x);
    if isempty(x);
        if (m==0 && n==0)
            xy = gpuArray.nan(classUnderlying(x));
        else
            xy = gpuArray.nan(n,classUnderlying(x));
        end
        return;
    end

    if m == 1  % One observation

        % For single data, unbiased estimate of the covariance matrix is not defined.
        % Return the second moment matrix of the observations about their mean.
        xy = gpuArray.zeros(n, classUnderlying(x));
    else
        
        %if flag
        %    normFactor = sqrt(m);
        %else
        %    normFactor = sqrt(m-1);
        %end
        
        %xc = arrayfun(@removeMean, x, sum(x,1), m, normFactor);  % Remove mean
        %xy = xc' * xc;

        xc = bsxfun(@minus,x,sum(x,1)/m);  % Remove mean
        if flag
            xy = (xc' * xc) / m;
        else
            xy = (xc' * xc) / (m-1);
        end

    end

end

%---------------------------------------------
%function z = removeMean(x, y, m, normFactor)

%  z = ( ( x - (y./m) )./normFactor

%end

%end


%----------------------------------------------
function y = iscovflag(x)
% flag for cov must be 0 or 1.
    y = isscalar(x) && (x==0 || x==1);

end
