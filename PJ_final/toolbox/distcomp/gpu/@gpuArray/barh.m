%BARH overloaded for gpuArrays
%   BARH can operate on any combination of gpuArrays and standard
%   MATLAB arrays by gathering the gpuArrays and then invoking
%   the standard BARH function. All valid syntaxes for BARH are
%   supported.
%   
%   See also BARH, GPUARRAY.


%   Copyright 2010 The MathWorks, Inc.
% $Revision: 1.1.6.1 $ $Date: 2010/11/01 19:24:36 $
