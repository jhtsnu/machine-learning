%GAMMA Gamma function of gpuArray
%   Y = GAMMA(X)
%   
%   Example:
%    
%       N = 1000;
%       D = gpuArray.ones(N);
%       E = gamma(D)
%   
%   See also GAMMALN,  GPUARRAY, GPUARRAY/ONES.
%   


%   Copyright 2006-2010 The MathWorks, Inc.
% $Revision: 1.1.10.3 $ $Date: 2010/10/07 15:45:09 $
