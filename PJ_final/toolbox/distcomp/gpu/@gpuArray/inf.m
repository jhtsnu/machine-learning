%GPUARRAY.INF Infinity gpuArray
%   D = GPUARRAY.INF(N) is an N-by-N gpuArray matrix of INFs.
%   
%   D = GPUARRAY.INF(M,N) is an M-by-N gpuArray matrix of INFs.
%   
%   D = GPUARRAY.INF(M,N,P,...) or GPUARRAY.INF([M,N,P,...])
%   is an M-by-N-by-P-by-... gpuArray of INFs.
%   
%   D = GPUARRAY.INF(M,N,P,..., CLASSNAME) or 
%   GPUARRAY.INF([M,N,P,...], CLASSNAME) is an M-by-N-by-P-by-... 
%   gpuArray of INFs of class specified by CLASSNAME.  CLASSNAME 
%   must be either 'single' or 'double'.
%   
%   
%   Example:
%   % Create a 1000-by-1 gpuArray of underlying class 'single' 
%   % containing the value Inf:
%    
%       N = 1000;
%       D1 = gpuArray.inf(N, 1,'single')
%   
%   See also INF, GPUARRAY, GPUARRAY/ZEROS, GPUARRAY/ONES


%   Copyright 2008-2011 The MathWorks, Inc.
