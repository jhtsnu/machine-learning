%EXISTSONGPU determine if a gpuArray object is still on the GPU
%   
%   TF = existsOnGPU(DATA) returns a logical value indicating whether the  
%   gpuArray object DATA is still present on the GPU. The result will be 
%   false if DATA is no longer valid and cannot be used. This happens if the
%   GPU device has been reset using the GPUDEVICE function.
%   
%   Example:
%    
%       D = gpuArray(1);
%       existsOnGPU(D) % returns true
%       gpuDevice([]);
%       existsOnGPU(D) % returns false
%   
%   See also GPUARRAY, gpuDevice.
%   


%   Copyright 2011 The MathWorks, Inc.
