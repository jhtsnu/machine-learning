%GPUARRAY.ZEROS Zeros gpuArray
%   D = GPUARRAY.ZEROS(N) is an N-by-N gpuArray matrix of zeros.
%   
%   D = GPUARRAY.ZEROS(M,N) is an M-by-N gpuArray matrix of zeros.
%   
%   D = GPUARRAY.ZEROS(M,N,P,...) or GPUARRAY.ZEROS([M,N,P,...])
%   is an M-by-N-by-P-by-... gpuArray of zeros.
%   
%   D = GPUARRAY.ZEROS(M,N,P,..., CLASSNAME) or 
%   GPUARRAY.ZEROS([M,N,P,...], CLASSNAME) is an M-by-N-by-P-by-... 
%   gpuArray of zeros of class specified by CLASSNAME.
%   
%   Examples:
%    
%       N  = 1000;
%       D1 = gpuArray.zeros(N)   % 1000-by-1000 gpuArray matrix of zeros
%       D2 = gpuArray.zeros(N,N*2) % 1000-by-2000
%       D3 = gpuArray.zeros([N,N*2], 'int8') % underlying class 'int8'
%   
%   See also ZEROS, GPUARRAY, GPUARRAY/ONES.


%   Copyright 2008-2011 The MathWorks, Inc.
