function out = isa(varargin)
%ISA True if object is a given class.
%   isa(OBJ,'classname') returns true if OBJ is an instance of 'classname'.
%   It also returns true if OBJ is an instance of a class that is derived 
%   from 'classname'.
%   
%   See also ISA.
%   
%   


%   Copyright 2011 The MathWorks, Inc.

% We specifically allow the old class name. If we get an exact match,
% convert to the new class-name but then let built-in "isa" do the work.
if nargin>=2 && ischar(varargin{2}) ...
        && strcmp(varargin{2},'parallel.gpu.GPUArray')
    varargin{2} = 'gpuArray';
    parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray', 'gpuArray');
end
out = builtin( 'isa', varargin{:} );