%EXISTSONGPU determine if a CUDAKernel object is still on the GPU
% 
%   TF = existsOnGPU(KERNEL) returns a logical value indicating whether the  
%   CUDAKernel object KERNEL is still present on the GPU. The result will
%   be false if KERNEL is no longer valid and cannot be used. This happens
%   if the GPU device has been reset using the GPUDEVICE function.
% 
%   Example:
%     K = parallel.gpu.CUDAKernel(...);
%     existsOnGPU(K) % returns true
%     gpuDevice([]);
%     existsOnGPU(K) % returns false
% 
% See also parallel.gpu.CUDAKernel, gpuDevice.

% Copyright 2011 The MathWorks, Inc.
