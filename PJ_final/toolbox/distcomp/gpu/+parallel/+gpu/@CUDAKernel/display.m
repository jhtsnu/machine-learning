function display(obj)
%DISPLAY Display CUDAKernel
%
%   Example:
%       import parallel.gpu.CUDAKernel
%       K = CUDAKernel('mykernel.ptx','mykernel.cu');
%       display(K);
%
%   See also DISP, PARALLEL.GPU.CUDAKERNEL, PARALLEL.GPU.CUDAKERNEL/DISP.

%   Copyright 2011 The MathWorks, Inc.

    if ~existsOnGPU( obj )
        % Use disp to show the appropriate "does not exist" message
        disp( obj );
    else
        % Print the variable name then call disp to display the object
        if isequal( get( 0, 'FormatSpacing' ), 'compact' )
            spacer = '';
        else
            spacer = sprintf( '\n' );
        end
        name = inputname( 1 );
        fprintf( 1, '%s%s = \n%s', spacer, name, spacer );
        disp(obj);
    end
end
