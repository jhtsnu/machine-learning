function disp(obj)
%DISP Display CUDAKernel
%
%   Example:
%       import parallel.gpu.CUDAKernel
%       K = CUDAKernel('mykernel.ptx','mykernel.cu');
%       disp(K);
%
%   See also DISP, PARALLEL.GPU.CUDAKERNEL, PARALLEL.GPU.CUDAKERNEL/DISPLAY.

%   Copyright 2011 The MathWorks, Inc.

    if ~existsOnGPU( obj )
        disp( getString(message('parallel:gpu:kernel:NoLongerExists')) );
        if ~isequal( get( 0, 'FormatSpacing' ), 'compact' )
            disp( ' ' );
        end
    else
        % Simply call the display method built-into the "handle" base-class
        disp@handle( obj );
    end

end
