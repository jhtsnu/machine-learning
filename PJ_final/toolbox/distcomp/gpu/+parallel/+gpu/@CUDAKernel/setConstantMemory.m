%setConstantMemory set some constant memory for a GPU kernel
%   setConstantMemory(KERN, SYM, VAL) sets the constant memory for kernel
%   KERN that has symbol name SYM to contain the data in VAL. VAL can be
%   any numeric array including a gpuArray. If the named symbol does not
%   exist or is not big enough to contain the data specified an error is
%   thrown. Partially filling a constant is allowed. There is no automatic
%   data-type conversion for constant memory so it is important to make
%   sure that the supplied data is of the correct type for the constant
%   memory symbol being filled.
%
%   setConstantMemory(KERN, SYM1, VAL1, SYM2, VAL2, ...) sets multiple
%   constant symbols.
%
%   Example:
%   If the CUDA file defines the following constants:
%     __constant__ int N;
%     __constant__ double CONST_DATA[256];
% 
%   we can fill these with MATLAB data as follows:
%     kernel = parallel.gpu.CUDAKernel(ptxFile, cudaFile);
%     setConstantMemory(kernel, 'N', int32(10));
%     setConstantMemory(kernel, 'CONST_DATA', 1:10);
%
%   or:
%     setConstantMemory(kernel, 'N', int32(10), 'CONST_DATA', 1:10);
%
%   See also parallel.gpu.CUDAKernel, gpuArray.

%   Copyright 2011-2012 The MathWorks, Inc.
