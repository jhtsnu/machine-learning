function out = ones( varargin )
% parallel.gpu.GPUArray.ones will be removed in a future release. 
% Use gpuArray.ones instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.ones', 'gpuArray.ones');
out = gpuArray.ones(varargin{:});