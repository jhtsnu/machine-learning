function out = linspace( varargin )
% parallel.gpu.GPUArray.linspace will be removed in a future release. 
% Use gpuArray.linspace instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.linspace', 'gpuArray.linspace');
out = gpuArray.linspace(varargin{:});