function out = colon( varargin )
% parallel.gpu.GPUArray.colon will be removed in a future release. 
% Use gpuArray.colon instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.colon', 'gpuArray.colon');
out = gpuArray.colon(varargin{:});