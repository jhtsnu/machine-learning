function out = nan( varargin )
% parallel.gpu.GPUArray.nan will be removed in a future release. 
% Use gpuArray.nan instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.nan', 'gpuArray.nan');
out = gpuArray.nan(varargin{:});