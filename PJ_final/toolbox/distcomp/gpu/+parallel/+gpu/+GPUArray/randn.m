function out = randn( varargin )
% parallel.gpu.GPUArray.randn will be removed in a future release. 
% Use gpuArray.randn instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.randn', 'gpuArray.randn');
out = gpuArray.randn(varargin{:});