function out = randi( varargin )
% parallel.gpu.GPUArray.randi will be removed in a future release. 
% Use gpuArray.randi instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.randi', 'gpuArray.randi');
out = gpuArray.randi(varargin{:});