function out = true( varargin )
% parallel.gpu.GPUArray.true will be removed in a future release. 
% Use gpuArray.true instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.true', 'gpuArray.true');
out = gpuArray.true(varargin{:});