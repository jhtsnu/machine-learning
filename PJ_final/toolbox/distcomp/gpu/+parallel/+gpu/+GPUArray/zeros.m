function out = zeros( varargin )
% parallel.gpu.GPUArray.zeros will be removed in a future release. 
% Use gpuArray.zeros instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.zeros', 'gpuArray.zeros');
out = gpuArray.zeros(varargin{:});