function out = rand( varargin )
% parallel.gpu.GPUArray.rand will be removed in a future release. 
% Use gpuArray.rand instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.rand', 'gpuArray.rand');
out = gpuArray.rand(varargin{:});