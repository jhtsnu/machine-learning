function out = eye( varargin )
% parallel.gpu.GPUArray.eye will be removed in a future release. 
% Use gpuArray.eye instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.eye', 'gpuArray.eye');
out = gpuArray.eye(varargin{:});