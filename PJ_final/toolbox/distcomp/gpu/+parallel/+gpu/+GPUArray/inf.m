function out = inf( varargin )
% parallel.gpu.GPUArray.inf will be removed in a future release. 
% Use gpuArray.inf instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.inf', 'gpuArray.inf');
out = gpuArray.inf(varargin{:});