function out = false( varargin )
% parallel.gpu.GPUArray.false will be removed in a future release. 
% Use gpuArray.false instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.false', 'gpuArray.false');
out = gpuArray.false(varargin{:});