function out = logspace( varargin )
% parallel.gpu.GPUArray.logspace will be removed in a future release. 
% Use gpuArray.logspace instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray.logspace', 'gpuArray.logspace');
out = gpuArray.logspace(varargin{:});