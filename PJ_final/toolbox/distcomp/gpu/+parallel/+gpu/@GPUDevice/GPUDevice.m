%GPUDevice - select and query properties of GPU hardware
%   parallel.gpu.GPUDevice has a number of static methods to select and query
%   GPU devices:
%
%   c = parallel.gpu.GPUDevice.count() returns the number of GPU devices
%   present.
%
%   d = parallel.gpu.GPUDevice.current() returns the currently selected GPU
%   device.
%
%   d = parallel.gpu.GPUDevice.select(idx) selects a different GPU device.
%
%   d = parallel.gpu.GPUDevice.getDevice(idx) returns a GPU device object
%   without selecting it.
%
%   See also parallel.gpu.CUDADevice, gpuDevice, gpuDeviceCount.

% Copyright 2010-2011 The MathWorks, Inc.
classdef GPUDevice < handle
    methods ( Access = public, Static )
        
        function c = count()
        %COUNT Number of GPU Devices
        %   N = PARALLEL.GPU.GPUDEVICE.COUNT returns the number of available GPU
        %   devices.
            c = parallel.internal.gpu.deviceCount;
        end
        
        function d = current()
        %CURRENT Return currently selected GPUDevice
        %   D = PARALLEL.GPU.GPUDEVICE.CURRENT returns the currently selected GPUDevice
        %   object.
            [props, E] = parallel.internal.gpu.deviceProperties();
            if ~isempty( E ), throw( E ); end
            d = parallel.gpu.CUDADevice.hBuild( props );
        end
        
        function d = select( idx )
        %SELECT Change the currently selected GPUDevice
        %   D = PARALLEL.GPU.GPUDEVICE.SELECT(IDX) selects a GPUDevice with index
        %   IDX. IDX must be in the range 1..PARALLEL.GPU.GPUDEVICE.COUNT.
        %   PARALLEL.GPU.GPUDEVICE.SELECT([]) deselects the current GPUDevice.

            E = parallel.internal.gpu.selectDevice( idx );
            if ~isempty( E )
                throw( E );
            end
            if nargout == 1
                if isempty( idx )
                    % Deselecting, return nothing.
                    d = [];
                else
                    d = parallel.gpu.GPUDevice.current();
                end
            end
        end
        
        function d = getDevice( idx )
        %GETDEVICE Return a GPUDevice object without selecting it
        %   D = PARALLEL.GPU.GPUDEVICE.GETDEVICE(IDX) returns a GPUDevice object for
        %   index IDX without making it the currently selected GPU device.
            [props, E] = parallel.internal.gpu.deviceProperties( idx );
            if ~isempty( E ), throw( E ); end
            d = parallel.gpu.CUDADevice.hBuild( props );
        end
        
        
        function available = isAvailable( indices )
            %ISAVAILABLE Check whether a GPU device is available for use
            %
            %   AVAILABLE = PARALLEL.GPU.GPUDEVICE.ISAVAILABLE() returns true or
            %   false according to whether the currently selected GPU device is
            %   supported and available for use with MATLAB.
            %
            %   AVAILABLE = PARALLEL.GPU.GPUDEVICE.ISAVAILABLE(INDICES) returns
            %   true or false for each of the GPU devices whose indices are given in
            %   the array INDICES.
            %
            %   Examples:
            %   gpuIsOK = parallel.gpu.GPUDevice.isAvailable(1:gpuDeviceCount());
            %   if any( gpuIsOK )
            %      % Select a GPU
            %      index = find( gpuIsOK, 1, 'first' );
            %      gpuDevice( index );
            %      % perform GPU algorithm
            %   else
            %      error( 'No GPUs available' );
            %   end
            %
            %   See also: gpuDeviceCount, gpuDevice
            
            narginchk( 0, 1 );
            if nargin<1
                % No device specified, so just check the current one
                try
                    indices = parallel.internal.gpu.currentDeviceIndex();
                catch err %#ok<NASGU>
                    % Can't even get the index, so definitely not available
                    available = false;
                    return;
                end 
            else
                % Check values are all positive integers
                if ~isnumeric( indices ) || any( indices <= 0 ) || any( indices~=round(indices) )
                    error(message('parallel:gpu:device:IsAvailableNonIndexInput'));
                end
            end
            
            available = false( size( indices ) );
            try
                count = parallel.gpu.GPUDevice.count();
            catch err %#ok<NASGU>
                % Initialization failure (no CUDA etc), so no devices.
                count = 0;
            end
            
            % If no devices, just return
            if count==0
                return;
            end
            
            % Now scan the devices checking the compute capability and
            % select-ability
            for ii=1:numel( indices )
                index = indices(ii);
                if index<=count
                    device = parallel.gpu.GPUDevice.getDevice( index );
                    available(ii) = iCheckDeviceAvailable( device );
                end
            end
        end 

        
    end
    methods ( Abstract = true )
        %RESET Reset GPU device
        %   RESET resets the GPU device and invalidates any gpuArrays and
        %   CUDAKernels residing on that device.
        %
        %   See also gpuDevice, parallel.gpu.GPUDevice.
        reset( obj )

        %WAIT Wait for the GPU device to complete execution
        %   WAIT(DEV) blocks MATLAB execution until the GPU device DEV has
        %   completed all operations. This can be used before calls to
        %   'toc' when timing GPU code that does not gather its result back
        %   to the host. When gathering results from a GPU, MATLAB
        %   automatically waits until all GPU calculations are complete, so
        %   you do not need to explicitly call wait in that situation.
        %
        %   Example:
        %   dev = gpuDevice;
        %   a = gpuArray.rand(2000);
        %   tic;
        %   a = a.*a;
        %   toc   % Shows only the time taken on the CPU
        %   tic;
        %   a = a.*a;
        %   wait(dev);
        %   toc   % Shows the total time for the calculation to complete
        %
        %   See also gpuDevice, parallel.gpu.GPUDevice.
        wait( obj )
    end
end


function ok = iCheckDeviceAvailable( device )
% Check whether a specific GPU device is available to MATLAB
ok = false;
% If not supported, stop checking now
if ~device.DeviceSupported
    return;
end

% Check the compute mode using a built-in helper...
ok = parallel.internal.gpu.canSelectDevice( device.Index );
end
