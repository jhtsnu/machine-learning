function data = rand( obj, varargin )
%RAND  Build a gpuArray of uniformly distributed pseudo-random numbers
%   D = rand(STREAM,...) uses the specified parallel.gpu.RandStream to
%   generate uniformly distributed random numbers. See gpuArray.rand for
%   details of the rand syntax.
%
%   See also gpuArray.rand,
%            parallel.gpu.RandStream/randi,
%            parallel.gpu.RandStream/randn

%   Copyright 2011 The MathWorks, Inc.

data = gpuArray.rand( obj, varargin{:} );
