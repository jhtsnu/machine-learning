function reset( obj, seed )
%RESET  Reset a GPU random stream to its initial state
%     reset(S)
%     reset(S,SEED)
%     restore the generator for the GPU random stream S to its initial
%     state.  A new seed can optionally be provided.  See RandStream/reset
%     for more details.
%
%     See also randfun/RandStream/reset

%   Copyright 2011 The MathWorks, Inc.

if nargin<2
    % Just reset to the pre-stored value
    obj.CurrentState = obj.InitialState;
else
    % Need to recreate from scratch
    obj.CurrentState = [];
    obj.InitialState = [];
    obj.Seed_ = seed;
end

end
