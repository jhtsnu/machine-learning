function stream = getGlobalStream()
%getGlobalStream  Get the current global random number stream
%
%   stream = parallel.gpu.RandStream.getGlobalStream() returns the current
%   GPU global random number stream used by gpuArray.rand, gpuArray.randi,
%   and gpuArray.randn.
%
%   The parallel.gpu.rng function is a shorter alternative for many uses of
%   parallel.gpu.RandStream.setGlobalStream.
%
%   See also parallel.gpu.rng, 
%            parallel.gpu.RandStream,
%            parallel.gpu.RandStream.setGlobalStream,
%            gpuArray.rand, 
%            gpuArray.randn, 
%            gpuArray.randi

%   Copyright 2011 The MathWorks, Inc.
%      

stream = parallel.internal.gpu.randGetSetGlobalStream();

end