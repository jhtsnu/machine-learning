function list
%LIST  List available random number generator algorithms
%   parallel.gpu.RandStream.list() lists all the generator algorithms that
%   can be used when creating a random number stream with
%   parallel.gpu.RandStream or parallel.gpu.RandStream.create. See <a
%   href="matlab:helpview([docroot '\techdoc\math\math.map'],'choose_random_number_generator')">Choosing a Random Number Generator</a> 
%   for details on these generator algorithms.
%
%   See also parallel.gpu.RandStream, 
%            parallel.gpu.RandStream.create

%   Copyright 2011-2012 The MathWorks, Inc.

disp(' ');
disp(getString(message('MATLAB:RandStream:list:AvailableGenerators')));
disp(' ');

genList = parallel.gpu.RandStream.ValidGenerators;
% We want to align the descriptions, so work out the longest name.
maxLength = max(cellfun(@length,genList(:,1)));
% Print each name and description
for ii = 1:size(genList,1)
    name = genList{ii,1};
    description = getString(message(genList{ii,2}));
    padding = repmat( ' ', 1, maxLength + 2 - length(name) );
    disp([name,':',padding,description]);
end
end

