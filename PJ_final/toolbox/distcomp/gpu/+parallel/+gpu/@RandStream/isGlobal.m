function out = isGlobal( obj )
%isGlobal  Check whether the supplied GPU stream is the global GPU stream
%
%   OUT = isGlobal(STREAM) returns true if STREAM is the current global
%   GPU RandStream or false otherwise.
%
%   See also: parallel.gpu.RandStream.getGlobalStream,
%             parallel.gpu.RandStream.setGlobalStream,
%             parallel.gpu.RandStream

%   Copyright 2011 The MathWorks, Inc.
%      

globalObj = parallel.internal.gpu.randGetSetGlobalStream();
out = (obj == globalObj);
end