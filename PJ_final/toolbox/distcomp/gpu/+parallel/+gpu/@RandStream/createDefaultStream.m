function stream = createDefaultStream()
%createDefaultStream  Initialize the global random number stream
%
%   stream = parallel.gpu.RandStream.createDefaultStream() initializes a
%   new stream in a default state.

%   Copyright 2011 The MathWorks, Inc.
%      

stream = parallel.gpu.RandStream( parallel.gpu.RandStream.DefaultGenerator );

end
