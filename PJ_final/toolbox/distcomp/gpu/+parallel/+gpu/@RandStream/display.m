function display( obj )
%DISPLAY  Custom display of parallel.gpu.RandStream objects
%
%   Example:
%   display( parallel.gpu.RandStream.getGlobalStream() )
%
%   See also: parallel.gpu.RandStream

%   Copyright 2011 The MathWorks, Inc.
%      

% Determine padding character based on FormatSpacing preference
switch get( 0, 'FormatSpacing' )
    case 'compact'
        p = ''; % none
    otherwise % loose
        p = sprintf( '\n' ); % carriage return
end

% Header contains object name
name = inputname( 1 );
if isempty( name ) % the result of some operation
    name = 'ans';
end
fprintf( '%s%s =\n%s', p, name, p );

% Body contains object description
disp( obj );
end

