function validateParamName( name )
%VALIDATEPARAMNAME  Check that a parameter is a valid property name
%   validateParamName(NAME) checks that the supplied parameter is a
%   publicly settable property of RandStream and throws the appropriate
%   error if not. NAME can be a string or a cell-array of strings.
%
%   See also RandStream/create

%   Copyright 2011 The MathWorks, Inc.
%      

classInfo = ?parallel.gpu.RandStream;
allowedParams = {classInfo.PropertyList.Name}';

paramNotValid = ~ismember( name, allowedParams );
if any( paramNotValid )
    idx = find( paramNotValid, 1, 'first' );
    error( message( 'MATLAB:RandStream:UnrecognizedParamName', name{idx}) );
end

isPublicSettable =  strcmpi( {classInfo.PropertyList.SetAccess}, 'public' );
paramNotPublic = ~ismember( name, allowedParams(isPublicSettable) );
% Special case for Seed
paramNotPublic( strcmpi( name, 'Seed' ) ) = false;
% If any aren't public, error now
if any( paramNotPublic )
    idx = find( paramNotPublic, 1, 'first' );
    error( message( 'MATLAB:class:SetProhibited', ...
                    name{idx}, 'parallel.gpu.RandStream' ) );
end

