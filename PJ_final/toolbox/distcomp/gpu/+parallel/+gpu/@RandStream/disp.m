function disp( obj )
%DISP  Custom display of parallel.gpu.RandStream objects
%
%   Example:
%   disp( parallel.gpu.RandStream.getGlobalStream() )
%
%   See also: parallel.gpu.RandStream

%   Copyright 2011-2012 The MathWorks, Inc.
%      

% Determine padding character based on FormatSpacing preference
switch get( 0, 'FormatSpacing' )
    case 'compact'
        p = ''; % none
    otherwise % loose
        p = sprintf( '\n' ); % carriage return
end

% Now display a selection of our properties
if obj.isGlobal()
    fprintf( '%s\n', getString(message('parallel:gpu:randstream:DispHeaderGlobal',obj.Type)) );
else
    fprintf( '%s\n', getString(message('parallel:gpu:randstream:DispHeader',obj.Type)) );
end
if obj.NumStreams > 1
    fprintf( '      StreamIndex: %d\n', obj.StreamIndex );
    fprintf( '       NumStreams: %d\n', obj.NumStreams );
end
fprintf( '             Seed: %d\n', obj.Seed );
fprintf( '  NormalTransform: %s\n', obj.NormalTransform );
fprintf( p );
end