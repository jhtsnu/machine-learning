function data = randn( obj, varargin )
%RANDN  Build a gpuArray of normally distributed pseudo-random numbers
%   D = randn(STREAM,...) uses the specified RandStream stream to generate
%   normally distributed random numbers. See gpuArray.randn for details of
%   the randn syntax.
%
%   See also gpuArray.randn,
%            parallel.gpu.RandStream/rand,
%            parallel.gpu.RandStream/randi

%   Copyright 2011 The MathWorks, Inc.

data = gpuArray.randn( obj, varargin{:} );