function oldstream = setGlobalStream( stream )
%setGlobalStream  Replace the global GPU random number stream
%
%   PREVSTREAM = parallel.gpu.RandStream.setGlobalStream(STREAM) sets
%   STREAM as the current GPU global random number stream used by
%   gpuArray.rand, gpuArray.randi, and gpuArray.randn. PREVSTREAM is the
%   stream that was previously designated as the global random number
%   stream.
%
%   The parallel.gpu.rng function is a shorter alternative for many uses of
%   parallel.gpu.RandStream.setGlobalStream.
%
%   See also parallel.gpu.rng, 
%            parallel.gpu.RandStream,
%            parallel.gpu.RandStream.getGlobalStream,
%            gpuArray.rand, 
%            gpuArray.randn, 
%            gpuArray.randi

%   Copyright 2011-2012 The MathWorks, Inc.

if nargin<1 || ~isa( stream, 'parallel.gpu.RandStream' )
    error(message('parallel:gpu:randstream:BadStream', class( stream )));
end

s = parallel.internal.gpu.randGetSetGlobalStream( stream );
if nargout
    oldstream = s;
end

end