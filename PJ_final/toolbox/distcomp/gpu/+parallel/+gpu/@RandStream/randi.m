function data = randi( obj, varargin )
%RANDI  Build a gpuArray of uniformly distributed pseudo-random integers
%   D = randi(STREAM,...) uses the specified parallel.gpu.RandStream to
%   generate uniformly distributed integer random numbers. See 
%   gpuArray.randi for details of the randi syntax.
%
%   See also gpuArray.randi,
%            parallel.gpu.RandStream/rand,
%            parallel.gpu.RandStream/randn

%   Copyright 2011 The MathWorks, Inc.

data = gpuArray.randi( obj, varargin{:} );