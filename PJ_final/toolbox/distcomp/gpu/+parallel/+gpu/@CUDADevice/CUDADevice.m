%CUDADevice CUDA GPU device object
%   DEV = parallel.gpu.GPUDevice.getDevice(IDX) returns a CUDADevice
%   representing the device with index IDX. 
%
%   DEV = parallel.gpu.GPUDevice.current() returns the currently selected
%   CUDADevice.
%
%   The CUDADevice has various properties describing the capabilities of the
%   underlying device.
%
%   See also parallel.gpu.GPUDevice, gpuDevice, gpuDeviceCount.

% Copyright 2010-2012 The MathWorks, Inc.
classdef CUDADevice < parallel.gpu.GPUDevice
    properties ( SetAccess = immutable )
        % Name - Name of the CUDA Device
        Name;

        % Index - Index of the CUDA Device
        %   This is the index by which the device can be selected
        Index;
        
        % ComputeCapability - CUDA Compute Capability
        %   This indicates the computational capability of the device
        ComputeCapability;
        
        % SupportsDouble - Does the device support double precision data
        %   True for devices which can support double precision operations
        SupportsDouble;
        
        % DriverVersion - the CUDA driver version
        %   The CUDA device driver version currently in use
        DriverVersion;
        
        % ToolkitVersion - the CUDA toolkit version
        %   The CUDA toolkit version currently in use
        ToolkitVersion;
        
        % MaxThreadsPerBlock - maximum supported thread block size
        %   This is the maximum number of threads per block during
        %   CUDAKernel execution
        MaxThreadsPerBlock;
        
        % MaxShmemPerBlock - maximum amount of shared memory per block
        %   The maximum amount of shared memory that can be used by
        %   a thread block during CUDAKernel execution
        MaxShmemPerBlock;

        % MaxThreadBlockSize - maximum size in each dimension for thread block
        %   Each dimension of a thread block must not exceed these
        %   dimensions. Additionally, the product of the thread block size must
        %   not exceed MaxThreadsPerBlock.
        MaxThreadBlockSize;

        % MaxGridSize - maximum size of grid of thread blocks
        MaxGridSize;
        
        % SIMDWidth - number of simultaneously executing threads
        SIMDWidth;
        
        % TotalMemory - total available memory in bytes on the device
        TotalMemory;
    end
    properties ( Dependent = true, SetAccess = private )
        % FreeMemory - free memory in bytes on the device
        %   This property is only available for the currently selected
        %   device, and will have the value NaN for unselected devices
        FreeMemory;
    end
    properties ( SetAccess = immutable )
        % MultiprocessorCount - the number of vector processors present
        MultiprocessorCount;

        % ClockRateKHz - the peak clock rate of the GPU in KHz
        ClockRateKHz;

        % ComputeMode - the compute mode of the device
        %   Default           - the device is not restricted and can support
        %                       multiple CUDA sessions simultaneously
        %   Exclusive process - the device can be used by only one process at a
        %                       time
        %   Exclusive thread  - the device can be used by only one thread at a
        %                       time
        %   Prohibited        - the device cannot be used
        ComputeMode;
        
        % GPUOverlapsTransfers - whether the device supports overlapped transfers
        GPUOverlapsTransfers;
        
        % KernelExecutionTimeout - if true, the device may abort long-running kernels
        KernelExecutionTimeout;

        % CanMapHostMemory - if true, the device supports mapping host memory into the
        % CUDA address space
        CanMapHostMemory;
        
        % DeviceSupported - can this device be used
        %   Not all devices are supported, for example if their ComputeCapability
        %   is insufficient. 
        DeviceSupported;
    end
    properties ( Dependent = true, SetAccess = private )
        % DeviceSelected - is this the currently selected device
        DeviceSelected;
    end
    properties ( Access = private )
        ObjectValid;
    end


    methods
        function fm = get.FreeMemory( obj )
            if obj.isCurrent() && obj.DeviceSupported
                fm = parallel.internal.gpu.currentDeviceFreeMem();
            else
                fm = NaN;
            end
        end
        function tf = get.DeviceSelected( obj )
            tf = obj.isCurrent();
        end
    end

    % Deny access to static methods through CUDADevice
    methods ( Static, Hidden )
        function count()
            iStaticMethodError( 'count' );
        end
        function current()
            iStaticMethodError( 'current' );
        end
        function select( ~ )
            iStaticMethodError( 'select' );
        end
        function getDevice( ~ )
            iStaticMethodError( 'getDevice' );
        end
        function isAvailable( ~ )
            iStaticMethodError( 'isAvailable' );
        end
    end

    methods ( Access = private )
        function obj = CUDADevice( propStruc )
            obj.Name                   = propStruc.DeviceName;
            obj.Index                  = propStruc.DeviceIndex;
            obj.ComputeCapability      = propStruc.ComputeCapability;
            obj.SupportsDouble         = propStruc.DeviceSupportsDouble;
            obj.DeviceSupported        = propStruc.DeviceSupported;
            obj.DriverVersion          = propStruc.DriverVersion;
            obj.ToolkitVersion         = propStruc.ToolkitVersion;
            obj.MaxThreadsPerBlock     = propStruc.MaxThreadsPerBlock;
            obj.MaxShmemPerBlock       = propStruc.MaxShmemPerBlock;
            obj.MaxGridSize            = propStruc.MaxGridSize;
            obj.MaxThreadBlockSize     = propStruc.MaxThreadBlockSize;
            obj.SIMDWidth              = propStruc.SIMDWidth;
            obj.TotalMemory            = propStruc.TotalMemory;
            obj.MultiprocessorCount    = propStruc.MultiprocessorCount;
            obj.ClockRateKHz           = propStruc.ClockRateKHz;
            obj.ComputeMode            = propStruc.ComputeMode;
            obj.GPUOverlapsTransfers   = propStruc.GPUOverlapsTransfers;
            obj.KernelExecutionTimeout = propStruc.KernelExecutionTimeout;
            obj.CanMapHostMemory       = propStruc.CanMapHostMemory;
            obj.ObjectValid            = true;
        end
        
        function tf = isCurrent( obj )
            tf = obj.ObjectValid && ...
                 parallel.internal.gpu.isAnyDeviceSelected() && ...
                 ( obj.Index == parallel.internal.gpu.currentDeviceIndex() );
        end
        
    end

    methods ( Static, Hidden )
        function obj = hBuild( propStruct )
            obj = parallel.gpu.CUDADevice( propStruct );
        end
    end
    methods ( Static )
        function obj = loadobj( obj )
            warning(message('parallel:gpu:device:DeviceObjectLoaded'));
            % simply invalidate to ensure we don't try and access the device.
            obj.ObjectValid = false;
        end
    end

    methods
        function reset( obj )
            if ~obj.isCurrent()
                error(message('parallel:gpu:device:ResetCurrent'));
            end
            E = parallel.internal.gpu.selectDevice( obj.Index );
            if ~isempty( E )
                throw( E ); 
            end
        end
    end
end

function iStaticMethodError( name )
    m = message( 'parallel:gpu:device:CUDADeviceStaticMethod', name );
    E = MException( m.Identifier, '%s', m.getString() );
    throwAsCaller( E );
end
