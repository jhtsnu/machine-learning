function out = GPUArray( varargin )
% parallel.gpu.GPUArray will be removed in a future release. Use gpuArray
% instead.

%   Copyright 2011 The MathWorks, Inc.

parallel.internal.gpu.gpuArrayDeprecationMessage('parallel.gpu.GPUArray', 'gpuArray');
out = gpuArray(varargin{:});