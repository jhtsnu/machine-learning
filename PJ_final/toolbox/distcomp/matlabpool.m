function varargout = matlabpool(varargin)
%MATLABPOOL Open or close a pool of MATLAB workers
%   MATLABPOOL enables the parallel language features within the MATLAB
%   language (e.g., parfor) by starting a communicating job that connects this
%   MATLAB client session with a number of workers.
%
%   SYNTAX
%
%     MATLABPOOL
%     MATLABPOOL OPEN
%     MATLABPOOL OPEN <poolSize>
%     MATLABPOOL OPEN PROF
%     MATLABPOOL OPEN PROF <poolSize>
%     MATLABPOOL('OPEN', ... 'AttachedFiles', filesToAttach)
%     MATLABPOOL <poolSize>
%     MATLABPOOL PROF
%     MATLABPOOL PROF <poolSize>
%     MATLABPOOL(clusterObj)
%     MATLABPOOL(clusterObj, 'OPEN')
%     MATLABPOOL(clusterObj, 'OPEN', ...)
%     MATLABPOOL(clusterObj, poolSize)
%     MATLABPOOL CLOSE
%     MATLABPOOL CLOSE FORCE
%     MATLABPOOL CLOSE FORCE PROF
%     MATLABPOOL SIZE
%     MATLABPOOL('ADDATTACHEDFILES', filesToAttach)
%     MATLABPOOL UPDATEATTACHEDFILES
%
%   MATLABPOOL or MATLABPOOL OPEN starts a worker pool using the default
%   cluster profile with the pool size specified by that profile. You
%   can also specify the pool size using MATLABPOOL OPEN <poolSize>, but note
%   that most clusters have a maximum number of processes that they can start.
%
%   MATLABPOOL OPEN PROF or MATLABPOOL OPEN PROF <poolSize> starts a worker
%   pool using the Parallel Computing Toolbox user profile PROF rather
%   than the default profile to locate a cluster. If the pool size is
%   specified, it will override the maximum and minimum number of workers
%   specified in the profile.
%
%   MATLABPOOL('OPEN', ... , 'AttachedFiles', filesToAttach) starts a worker
%   pool and concatenates the cell array filesToAttach with the AttachedFiles
%   specified in the profile. Note that 'AttachedFiles' is case-sensitive.
%
%   MATLABPOOL PROF <poolSize> is the same as MATLABPOOL OPEN PROF
%   <poolSize> and is provided for convenience.
%
%   MATLABPOOL(clusterObj) or MATLABPOOL(clusterObj, 'OPEN') is the same as
%   MATLABPOOL OPEN, except that the worker pool is started on the cluster
%   identified by the object clusterObj.
%
%   MATLABPOOL(clusterObj, 'OPEN', ...) is the same as MATLABPOOL('OPEN',
%   ...) except that the worker pool is started on the cluster identified
%   by the object clusterObj.
%
%   MATLABPOOL(clusterObj, poolSize) is the same as MATLABPOOL <poolSize>
%   except that the worker pool is started on the cluster identified by the
%   object clusterObj.
%
%   MATLABPOOL CLOSE stops the worker pool, destroys the communicating job and
%   makes all parallel language features revert to using the MATLAB client
%   to compute their results.
%
%   MATLABPOOL CLOSE FORCE stops the worker pool and destroys all communicating
%   jobs created by MATLABPOOL for the current user under the cluster
%   specified by the default cluster profile, including any jobs currently
%   running.
%
%   MATLABPOOL CLOSE FORCE PROF stops the worker pool and destroys all
%   matlabpool jobs being run under the cluster specified in the profile
%   PROF.
%
%   MATLABPOOL SIZE returns the size of the worker pool if it is open, or 0
%   if the pool is closed.
%
%   MATLABPOOL('ADDATTACHEDFILES', filesToAttach) allows you to add extra
%   attached files to an already running pool. filesToAttach is a cell
%   array of strings, identical in form to those added to a job. Each
%   string can specify either absolute or relative files, folders, or a
%   file on the MATLAB path. These files are transferred to each worker and
%   placed in the attached files folder, exactly the same as if they
%   had been set at the time the pool was opened.
%
%   MATLABPOOL UPDATEATTACHEDFILES checks all the attached files of the
%   current pool to see if they have changed, and replicates any changes to
%   each of the labs in the pool. In this way code changes can be sent out
%   to remote labs. This checks attachments added with both the MATLABPOOL
%   ADDATTACHEDFILES command and those specified when the pool was started
%   (by a profile or command line argument).
%
%   MATLABPOOL can be invoked as either a command or a function.  For
%   example, the following are equivalent:
%       MATLABPOOL PROF 4
%       MATLABPOOL('PROF', 4)
%
%   Examples:
%   % Start a worker pool using the default profile (usually local) with
%   % a pool size specified by that profile (4 for the local cluster)
%   matlabpool
%   
%   % Start a pool of 16 MATLAB workers using the profile myProf
%   matlabpool myProf 16
%
%   % Start a worker pool using the local profile with 2 workers:
%   matlabpool local 2
%
%   % Check whether the worker pool is currently open:
%   isOpen = matlabpool('size') > 0
%
%   % Start a MATLAB pool on a cluster, using a pool size specified
%   % by that cluster:
%   myCluster = parcluster();
%   matlabpool(myCluster)
%
%   % Start a pool of 16 MATLAB workers on a cluster:
%   matlabpool(myCluster, 16)
%
%   See also parfor, batch, parcluster,
%            parallel.defaultClusterProfile, parallel.Cluster.matlabpool.

%   Copyright 2007-2012 The MathWorks, Inc.

iVerifyJava();

import parallel.internal.cluster.MatlabpoolHelper
import parallel.internal.apishared.ProfileConfigHelper

pch = ProfileConfigHelper.buildDefault();

try
    parsedArgs = MatlabpoolHelper.parseInputsAndCheckOutputsForFunction(...
        pch, nargout, varargin{:});
catch err
    % Make all errors appear from matlabpool
    throw(err);
end

% Do the actual matlabpool bit
try
    matlabpoolOut = MatlabpoolHelper.doMatlabpool(parsedArgs, ...
                                                  parsedArgs.ActionArgs.Scheduler);
catch err
    if strcmp(parsedArgs.Action, MatlabpoolHelper.OpenAction)
        ex = iGetRunValidationException(err, parsedArgs.ActionArgs.Configuration);
        throw(ex);
    else
        % Make all errors appear from matlabpool
        throw(err);
    end
end

% Have to do the varargout assignment down here because doMatlabpool may return
% empty.
[varargout{1:nargout}] = matlabpoolOut{:};

end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function iVerifyJava()
%iVerifyJava Error if swing is not present.
if iIsOnClient()
    error(javachk('jvm', 'matlabpool'));
end
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function onclient = iIsOnClient()
onclient = ~system_dependent('isdmlworker');
end

% -------------------------------------------------------------------------
%
% -------------------------------------------------------------------------
function ex = iGetRunValidationException(err, profName)
if isempty(profName)
    ex = MException(message('parallel:cluster:MatlabpoolCreateProfileRunValidation'));
else
    ex = MException(message('parallel:cluster:MatlabpoolRunValidation', profName));
end
ex = ex.addCause(err);
end
