% Parallel Computing Functions for Message Passing
%
% General Functions
%    labindex - Return the ID for this lab
%    numlabs  - Return the total number of labs operating in parallel
%
% Parallel Communication Functions
%    labBarrier     - Block execution until all labs have entered the barrier
%    labBroadcast   - Send data to all labs
%    labProbe       - Test to see if messages are ready to labReceive
%    labReceive     - Receive data from another lab
%    labSend        - Send data to another lab
%    labSendReceive - Simultaneously send to and receive from other labs
%    mpiLibConf     - Return the location of an MPI implementation
%    mpiSettings    - Set options for MPI communication
%
% Parallel Profiling Functions
%    mpiprofile  - Profile parallel communication and execution times
%    mpiprofview - Display HTML profiler interface or return the HTML code
%
% See also distcomp, distcomp/parallel, distcomp/lang.

% Copyright 2011-2013 The MathWorks, Inc.
