function [textCellArray, err] = pGetMcode(filename, bufferSize)
% modified for mpiprofview from pGetMcode original
%pGetMcode  Returns a cell array of the text in a file
%   textCellArray = pGetMcode(filename)
%   textCellArray = pGetMcode(filename, bufferSize)

% Copyright 1984-2012 The MathWorks, Inc.
if nargin < 2
    bufferSize = 30000;
end
err = '';
fid = fopen(filename,'r');
if fid < 0
    textCellArray = {};

    err = getString(message('parallel:lang:mpi:GetCodeUnableToReadFile', filename));
    return;
end
txt = textscan(fid,'%s','delimiter','\n','whitespace','','bufSize',bufferSize);
fclose(fid);

textCellArray = txt{1};