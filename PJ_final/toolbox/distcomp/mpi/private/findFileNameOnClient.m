function [clientFullName, err] = findFileNameOnClient(fullName)

%   Copyright 2007-2012 The MathWorks, Inc.

;  %#ok undocumented
% Private function used by mpiprofview to find a file on the client when it
% is not in the same path as the file on the cluster.
% Try to find the file name on the client by repeatedly calling which
err = '';
clientFullName = '';

if isempty(fullName)
    err = getString(message('parallel:lang:mpi:FindFileOnClientPathEmpty'));
    return;
end

try
    % Break the full name into individual tokens based on both UNIX and PC
    % file separators.
    a = textscan(fullName, '%s', 'Delimiter', '/\\', 'MultipleDelimsAsOne', true );
    
    clientFullName = iWhichOnClient(a{1});
    if isempty(clientFullName)
        err = getString(message('parallel:lang:mpi:FindFileOnClientNotExist'));
    end
    
catch err
    % act as if nothing was found
    err = getString(message('parallel:lang:mpi:FindFileOnClientErrorOccurred', err.getReport()));
end

function out = iWhichOnClient(filePath)
% We are going to use the 'which' function in MATLAB to find this file
% on the MATLAB path. To do this we need to partly understand some of
% the special names used in paths to MATLAB files so that we pass the
% correct input to which. Specifically, we will understand the
% following ...
%   'private'
%   Any directory beginning with an '@' or a '+'

if numel(filePath) == 1
    % No path specified - only a function.m type name - so just try
    % which normally
    out = which(filePath{1});
    return
end
% Loop from the first up level directory all the way to the top finding
% things that match our understanding of MATLAB names (as mentioned
% above).
if ispc
    strcmpFcn = @strcmpi;
else
    strcmpFcn = @strcmp;
end
isPrivate = strcmpFcn(filePath{end-1}, 'private');
if isPrivate
    partialPath = filePath(1:end-2);
    requiredPath = filePath(end-1:end);
else
    partialPath = filePath(1:end-1);
    requiredPath = filePath(end);
end

% Look specifically for a UDD pathname - everything else we will just match
% with partial paths.
classPattern = '^@[A-Za-z_]\w*$';
isUDD = numel(partialPath) > 1 && ...
    ~isempty(regexp(partialPath{end}, classPattern, 'once')) && ...
    ~isempty(regexp(partialPath{end-1}, classPattern, 'once')) && ...
    ~ ( numel(partialPath) > 2 && ~isempty(regexp(partialPath{end-2}, classPattern, 'once')));
if isUDD
    try
        uddPath = partialPath(end-1:end);
        out = iWhichUDD(requiredPath, uddPath);
    catch err %#ok<NASGU>
        % Error likely indicates that we couldn't find a package path for
        % this name.
        out = '';        
    end
else
    % If the up level directory starts with an '@' then it is required as
    % well
    if ~isempty(partialPath) && ~isempty(regexp(partialPath{end}, classPattern, 'once'))
        requiredPath = [partialPath(end) ; requiredPath];
        partialPath = partialPath(1:end-1);
    end
    mcosPackagePattern = '^\+[A-Za-z_]\w*$';
    % Now also keep everything that starts with a '+'
    while ~isempty(partialPath) && ~isempty(regexp(partialPath{end}, mcosPackagePattern, 'once'))
        requiredPath = [partialPath(end) ; requiredPath]; %#ok<AGROW>
        partialPath = partialPath(1:end-1);
    end
    out = iWhichPartial(requiredPath, partialPath);    
end

function out = iWhichPartial(required, partial)
% Function which recursively loops over shorter and shorter partial paths
% until a match with the required parts is reached.
if nargin < 2 
    partial = {};
end
required = fullfile(required{:});
for i = 1:numel(partial)
    out = which(fullfile(partial{i:end}, required));
    if ~isempty(out)
        return
    end
end
out = which(required);

function out = iWhichUDD( parts, uddParts )
% Get the one path to a UDD class - we know this cannot be spread across
% many directories 
packagePath = iGetUDDClassPath(uddParts{:});
% Construct the expected filename and ask if it exists
filename = fullfile(packagePath, uddParts{2}, parts{:});
if exist(filename, 'file')
    out = filename;
else
    out = '';
end
  
function packagePath = iGetUDDClassPath(uddPackageFilename, uddClassFilename)
% Call what on the package dir name to find all dirs on the path with that
% name
allWhats = what(uddPackageFilename);
% Loop over what we have found 
for i = 1:numel(allWhats)
    % Does it have a class with the correct name (2:end) leaves off the @
    if any(strcmp(allWhats(i).classes, uddClassFilename(2:end)))
        % If found then return it ...
        packagePath = allWhats(i).path;
        return
    end
end
% Otherwise error saying no package found with that class
error(message('parallel:lang:mpi:ProfUnknownUDDpackage'));
