function [oldConfig, allConfigs] = pctDefaultParallelConfig(newConfig)
; %#ok Undocumented
% Helper function for defaultParallelConfig.  Returns the names of all the 
% configurations and the current default configuration.  If a new default name 
% is supplied, then sets it as the default and returns the name of the old default.

%    Copyright 2009-2011 The MathWorks, Inc.

import parallel.internal.settings.ConfigurationsHelper;

ser = ConfigurationsHelper.getCurrentSerializer();
allConfigs = ser.getAllNames();
oldConfig  = ser.getDefaultName();

if (nargin > 0)
    % Set the default configuration.  This will check whether the config
    % exists.
    ser.setDefaultName(newConfig);
end
