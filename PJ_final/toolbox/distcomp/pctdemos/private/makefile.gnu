#
# Need to copy convertToPVArrays.m & file
# from toolbox/distcomp/cluster/+parallel/+internal to this folder.

# $Revision: 1.1.6.3 $   $Date: 2010/02/25 08:02:17 $

COPY_BASE := ../../cluster/+parallel/+internal
COPY_FILES := convertToPVArrays.m
include $(MAKE_INCLUDE_DIR)/copyrules.gnu

all:

# BaT needs to see a once target here ..
once : $(COPY_FILES)

.phony : all once
