%% Running Element-wise MATLAB(R) Functions on the GPU
% This example shows how the |arrayfun| method can be used to run a
% MATLAB(R) function natively on the GPU. The MATLAB function can be in its
% own file or can be a nested or anonymous function. It must contain only
% scalar operations and arithmetic.

% Copyright 2010-2012 The MathWorks, Inc.

%%
% We put the example into a function to allow nested functions:
function paralleldemo_gpu_arrayfun


%% Using Horner's Rule to Calculate Exponentials
% Horner's rule allows the efficient evaluation of power series expansions.
% We will use it to calculate the first 10 terms of the power series
% expansion for the exponential function |exp|. We can implement this as a
% MATLAB function.

function y = horner(x)
%HORNER - series expansion for exp(x) using Horner's rule
y = 1 + x.*(1 + x.*((1 + x.*((1 + ...
        x.*((1 + x.*((1 + x.*((1 + x.*((1 + ...
        x.*((1 + x./9)./8))./7))./6))./5))./4))./3))./2));
end

%% Preparing |horner| for the GPU
% To run this function on the GPU, we use a handle to the
% |horner| function, which we can evaluate using |arrayfun|.
% We can pass in |gpuArray| objects or standard MATLAB arrays. |horner|
% automatically adapts to different size and type inputs. We can compare
% the results computed on the GPU with standard MATLAB execution simply by
% evaluating the function directly.

hornerFcn = @horner;

%% Create the Input Data
% We create some inputs of different types and sizes, and use |gpuArray| to
% send them to the GPU.

data1  = rand( 2000, 'single' );
data2  = rand( 1000, 'double' );
gdata1 = gpuArray( data1 );
gdata2 = gpuArray( data2 );

%% Evaluate |horner| on the GPU
% To evaluate the |horner| function on the GPU, we simply call |arrayfun|,
% using the same calling convention as the original MATLAB function. We can
% compare the results by evaluating the original function directly in
% MATLAB. We expect some slight numerical differences because the
% floating-point arithmetic on the GPU does not precisely match the
% arithmetic performed on the CPU.

gresult1 = arrayfun( hornerFcn, gdata1 );
gresult2 = arrayfun( hornerFcn, gdata2 );

comparesingle = max( max( abs( gresult1 - horner( data1 ) ) ) );
comparedouble = max( max( abs( gresult2 - horner( data2 ) ) ) );
%%
fprintf( 'Maximum discrepancy for single precision: %g\n', comparesingle );
fprintf( 'Maximum discrepancy for double precision: %g\n', comparedouble );

%% Comparing Performance between GPU and CPU
% We can compare the performance of the GPU version to the native MATLAB
% version. Current generation GPUs have much better performance in single
% precision, so we will compare that. To accurately measure the calculation
% time on the GPU we must use |wait| to ensure that the GPU has finished
% all operations before calling |toc|. This is only required for timing
% purposes.
tic
arrayfun( hornerFcn, gdata1 ); 
wait( gpuDevice );
tgpu = toc;
%%
tic
hornerFcn( data1 );
tcpu = toc;
%%
fprintf( 'Speed-up achieved: %g\n', tcpu / tgpu );

displayEndOfDemoMessage(mfilename)

end
