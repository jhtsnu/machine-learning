function [splitVec, numTasks] = pctdemo_helper_split_vector(vec, numTasks)
%PCTDEMO_HELPER_SPLIT_VECTOR Returns a segmentation of a vector
%   [splitVec, numTasks] = PCTDEMO_HELPER_SPLIT_VECTOR(vec, numTasks) 
%   divides the vector vec into min(numTasks, length(vec))
%   cells storing vectors of roughly equal length.
%   The value numTasks returned equals min(numTasks, length(vec)).
%  
%   The input argument numTasks must be greater than or equal to zero.  If the
%   input vector vec is not empty, numTasks must be strictly greater than zero.
%   
%   The function is useful when dividing a "parameter sweep" between numTask
%   tasks.  In that case, task i should perform a parameter sweep over
%   splitVec{i}.
%  
%   See also PCTDEMO_HELPER_SPLIT_SCALAR

%   Copyright 2007-2012 The MathWorks, Inc.
    
    % Validate the input arguments.
    narginchk(2, 2);
    if ~(isvector(vec) || isempty(vec))
        error('pctexample:splitvector:SplitVectorInputMustBeVector', ...
              'First input argument must be a vector');
    end
    tc = pTypeChecker();
    if ~tc.isIntegerScalar(numTasks, 0, Inf)
        error('pctexample:splitvector:SplitVectorInvalidNumTasks', ...
              'Number of tasks must be a non-negative integer');
    end
    if (numTasks == 0 && ~isempty(vec))
        error('pctexample:splitvector:SplitVectorNumTasks', ...
              ['Number of tasks must be greater than 0 if the vector is ' ...
               'not empty']);
    end
    % Input arguments have been validated.
    
    % Find the number of elements that each cell should contain.
    [splitLength, numTasks] = pctdemo_helper_split_scalar(numel(vec), ...
                                                      numTasks);
    % For each cell, find the index of the first element that goes into that 
    % cell.  The vector splitElems will of the length numTasks + 1, and its last
    % element equals numel(vec) + 1.
    splitElems = cumsum([1, splitLength(:)']);
    
    splitVec = cell(numTasks, 1);
    for i = 1:numTasks
        splitVec{i} = vec(splitElems(i):splitElems(i+1) - 1);
    end
end % End of pctdemo_helper_split_vector
