function str = pctdemo_helper_getUniqueErrors(job)
%PCTDEMO_HELPER_GETUNIQUEERRORS Get task error messages from a job object.
%   allErrors = pctdemo_helper_getUniqueErrors(job) returns a string containing
%   all the unique task error messages from the given job.
%   If all the task error messages and error message identifiers are empty 
%   (i.e., if no task errors occurred), returns the empty array.
    
%   Copyright 2007-2012 The MathWorks, Inc.
    
    % Validate the number of input arguments.
    narginchk(1, 1);
    % Check that we have a valid job object.
    tc = pTypeChecker();
    if ~(tc.isIndependentJobObject(job) || tc.isCommunicatingJobObject(job))
        error('pctexample:helpergetuniqueerrors:InvalidArgument', ...
              'Input argument must be a single job object');
    end

    tasks = job.Tasks;
    if isempty(tasks)
        str = '';
        return;
    end
    % Get the error message and error IDs and store them in cell arrays of 
    % strings. 
    errs = [tasks.Error];
    if isempty(errs)
        % There were no task errors.
        str = '';
        return;
    end
    ids = {errs.identifier};
    msgs = {errs.message};

    % Find the unique error ids and messages, and return one message for each of
    % them.  We determine uniqueness based on both the error ID and the error 
    % message.
    combined = strcat(ids, msgs);
    [tmp, ind] = unique(combined); %#ok Tell mlint we don't need the first output arg.
    % Get the unique error messages.
    allErrors = msgs(ind);
    % Add newlines at the end of all but the last error message.
    allErrors(1:end - 1) = strcat(allErrors(1:end - 1), {sprintf('\n\n')});
    % Concatenate all the error messages into one long string.
    str = [allErrors{:}]; 
end % End of pctdemo_helper_getUniqueErrors.
