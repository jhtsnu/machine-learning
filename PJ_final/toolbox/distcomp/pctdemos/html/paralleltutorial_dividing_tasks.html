
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <!--
This HTML was auto-generated from MATLAB code.
To make changes, update the MATLAB code and republish this document.
      --><title>Dividing MATLAB&reg; Computations into Tasks</title><meta name="generator" content="MATLAB 7.14"><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"><meta name="DC.date" content="2011-10-21"><meta name="DC.source" content="paralleltutorial_dividing_tasks.m"><link rel="stylesheet" type="text/css" href="../../../matlab/helptools/private/style.css"></head><body><div class="header"><div class="left"><a href="matlab:edit paralleltutorial_dividing_tasks">Open paralleltutorial_dividing_tasks.m in the Editor</a></div><div class="right"><a href="matlab:echodemo paralleltutorial_dividing_tasks">Run in the Command Window</a></div></div><div class="content"><h1>Dividing MATLAB&reg; Computations into Tasks</h1><!--introduction--><p>The Parallel Computing Toolbox&#8482; enables us to execute our
MATLAB&reg; programs on a cluster of computers. In this example, we look
at how to divide a large collection of MATLAB operations into smaller
work units, called tasks, which the workers in our cluster can execute.
We do this programmatically using the
<tt>pctdemo_helper_split_scalar</tt> and
<tt>pctdemo_helper_split_vector</tt> functions.</p><p>Prerequisites:</p><div><ul><li><a href="paralleltutorial_defaults.html">Customizing the Settings for the Examples in the Parallel Computing Toolbox</a></li></ul></div><p>For further reading, see:</p><div><ul><li><a href="paralleltutorial_taskfunctions.html">Writing Task Functions</a></li><li><a href="paralleltutorial_network_traffic.html">Minimizing Network Traffic</a></li><li><a href="paralleltutorial_callbacks.html">Using Callback Functions</a></li></ul></div><!--/introduction--><h2>Contents</h2><div><ul><li><a href="#1">Obtaining the Profile</a></li><li><a href="#2">Starting with Sequential Code</a></li><li><a href="#3">Analyzing the Sequential Problem</a></li><li><a href="#11">Example: Dividing a Simulation into Tasks</a></li><li><a href="#19">Example: Dividing a Parameter Sweep into Tasks</a></li><li><a href="#24">More on Parameter Sweeps</a></li><li><a href="#25">Dividing MATLAB Operations into Tasks: Best Practices</a></li></ul></div><h2>Obtaining the Profile<a name="1"></a></h2><p>Like every other example in the Parallel Computing Toolbox, this example needs to
know what cluster to use.  We use the cluster identified by the default
profile.  See the
<a href="matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp_ug.map'),'profiles_help'">profile documentation</a>
for how to create new profile and how to change the default
profile.</p><pre class="codeinput">profileName = parallel.defaultClusterProfile();
</pre><h2>Starting with Sequential Code<a name="2"></a></h2><p>One of the important advantages of the Parallel Computing Toolbox is that
it builds very well on top of existing sequential code.  It is actually
beneficial to focus on sequential MATLAB code during the algorithm
development, debugging and performance evaluation stages, because we then
benefit from the rapid prototyping and interactive editing, debugging, and
execution capabilities that MATLAB offers.  During the development of
the sequential code, we should separate the computations from the pre- and the
post-processing, and make the core of the computations as simple and
independent from the rest of the code as possible.  Once our code is somewhat
stable, it is time to look at distributing the computations.  If we do a good
job of creating modular sequential code for a coarse grained application, it
should be rather easy to distribute those computations.</p><h2>Analyzing the Sequential Problem<a name="3"></a></h2><p>The Parallel Computing Toolbox supports the execution of coarse grained
applications, that is, independent, simultaneous executions of a single
program using multiple input arguments.  We now try to show examples of
what coarse grained computations often look like in MATLAB code and explain
how to distribute those kinds of computations.  We focus on two common
scenarios, arising when the original, sequential MATLAB code consists of
either</p><div><ul><li>Invoking a single function several times, using different values for the
input parameter.  Computations of this nature area sometimes referred to as
<b>parameter sweeps</b>, and the code often looks similar to the following MATLAB
code:</li></ul></div><pre>       for i = 1:n
         y(i) = f(x(i));
       end</pre><div><ul><li>Invoking a single stochastic function several times.  Suppose that the
calculations of <tt>g(x)</tt> involve random numbers, and the function thus returns a
different value every time it is invoked (even though the input parameter <tt>x</tt>
remains the same).  Such computations are sometimes referred to as <b>Monte
Carlo simulations</b>, and the code often looks similar to the following MATLAB
code:</li></ul></div><pre>       for i = 1:n
         y(i) = g(x);
       end</pre><p>It is quite possible that the parameter sweeps and simulations appear in a
slightly different form in our sequential MATLAB code.  For example, if the
function <tt>f</tt> is vectorized, the parameter sweep may simply appear as</p><pre>       y = f(x);</pre><p>and the Monte Carlo simulation may appear as</p><pre>       y = g(x, n);</pre><h2>Example: Dividing a Simulation into Tasks<a name="11"></a></h2><p>We use a very small example in what follows, using <tt>rand</tt> as our function
of interest.  Imagine that we have a cluster with four workers, and we
want to divide the function call <tt>rand(1, 10)</tt> between them.  This is by
far simplest to do with <tt>parfor</tt> because it divides the computations
between the workers without our having to make any decisions about how to
best do that.</p><p>We can expand the function call <tt>rand(1, 10)</tt> into the corresponding
<tt>for</tt> loop:</p><pre>       for i = 1:10
         y(i) = rand()
       end</pre><p>The parallelization using <tt>parfor</tt> simply consists of replacing the
<tt>for</tt> with a <tt>parfor</tt>.  If the MATLAB pool is open on the four workers,
this executes on the workers:</p><pre>       parfor i = 1:10
         y(i) = rand()
       end</pre><p>Alternatively, we can use <tt>createJob</tt> and <tt>createTask</tt> to
divide the execution of <tt>rand(1, 10)</tt> between the four workers.  We use
four tasks, and have them generate random vectors of length 3, 3, 2, and
2.  We have created a function called <tt>pctdemo_helper_split_scalar</tt> that
helps divide the generation of the 10 random numbers between the 4 tasks:</p><pre class="codeinput">numRand = 10; <span class="comment">% We want this many random numbers.</span>
numTasks = 4; <span class="comment">% We want to split into this many tasks.</span>
clust = parcluster(profileName);
job = createJob(clust);
[numPerTask, numTasks] = pctdemo_helper_split_scalar(numRand, numTasks);
</pre><p>Notice how <tt>pctdemo_helper_split_scalar</tt> splits the work of generating 10
random numbers between the <tt>numTasks</tt> tasks.  The elements of <tt>numPerTask</tt> are
all positive, the vector length is <tt>numTasks</tt>, and its sum equals <tt>numRand</tt>:</p><pre class="codeinput">disp(numPerTask)
</pre><pre class="codeoutput">     3
     3
     2
     2

</pre><p>We can now write a for-loop that creates all the tasks in the job. Task <tt>i</tt> is
to create a matrix of the size 1-by-numPerTask(i). When all the tasks have
been created, we submit the job, wait for it to finish, and then retrieve the
results.</p><pre class="codeinput"><span class="keyword">for</span> i = 1:numTasks
   createTask(job, @rand, 1, {1, numPerTask(i)});
<span class="keyword">end</span>
submit(job);
wait(job);
y = fetchOutputs(job);
cat(2, y{:})   <span class="comment">% Concatenate all the cells in y into one column vector.</span>
delete(job);
</pre><pre class="codeoutput">
ans =

  Columns 1 through 7

    0.3246    0.6618    0.6349    0.2646    0.0968    0.5052    0.8847

  Columns 8 through 10

    0.9993    0.8939    0.2502

</pre><h2>Example: Dividing a Parameter Sweep into Tasks<a name="19"></a></h2><p>For the purposes of this example, let's use the <tt>sin</tt> function as
a very simple example. We let <tt>x</tt> be a vector of length 10:</p><pre class="codeinput">x = 0.1:0.1:1;
</pre><p>and now we want to distribute the calculations of <tt>sin(x)</tt> on a cluster
with 4 workers.  As before, this is easiest to achieve with <tt>parfor</tt>:</p><pre>      parfor i = 1:length(x)
          y(i) = sin(x(i));
      end</pre><p>If we decide to achieve this using jobs and tasks, we first need to
determine how to divide the computations among the tasks.  We have the 4
workers evaluate <tt>sin(x(1:3))</tt>, <tt>sin(x(4:6))</tt>, <tt>sin(x(7:8))</tt>, and
<tt>sin(x(9:10))</tt> simultaneously. Because this kind of a division of a
parameter sweep into separate tasks occurs frequently in our examples, we
have created a function that does exactly that:</p><pre class="codeinput">numTasks = 4;
[xSplit, numTasks] = pctdemo_helper_split_vector(x, numTasks);
celldisp(xSplit);
</pre><pre class="codeoutput"> 
xSplit{1} =
 
    0.1000    0.2000    0.3000

 
 
xSplit{2} =
 
    0.4000    0.5000    0.6000

 
 
xSplit{3} =
 
    0.7000    0.8000

 
 
xSplit{4} =
 
    0.9000    1.0000

 
</pre><p>and it is now relatively easy to use <tt>createJob</tt> and
<tt>createTask</tt>, to perform the computations:</p><pre class="codeinput">job = createJob(clust);
<span class="keyword">for</span> i = 1:numTasks
   xThis = xSplit{i};
   createTask(job, @sin, 1, {xThis});
<span class="keyword">end</span>
submit(job);
wait(job);
y = fetchOutputs(job);
delete(job);
cat(2, y{:})  <span class="comment">% Concatenate all the cells in y into one column vector.</span>
</pre><pre class="codeoutput">
ans =

  Columns 1 through 7

    0.0998    0.1987    0.2955    0.3894    0.4794    0.5646    0.6442

  Columns 8 through 10

    0.7174    0.7833    0.8415

</pre><h2>More on Parameter Sweeps<a name="24"></a></h2><p>The example involving the <tt>sin</tt> function was particularly simple,
because the <tt>sin</tt> function is vectorized.  We look at how to deal
with nonvectorized functions in the
<a href="paralleltutorial_taskfunctions.html">Writing Task Functions</a> example.</p><h2>Dividing MATLAB Operations into Tasks: Best Practices<a name="25"></a></h2><p>When using jobs and tasks, we have to decide how to divide our
computations into appropriately sized tasks, paying attention to the
following:</p><div><ul><li>The number of function calls we want to make</li><li>The time it takes to execute each function call</li><li>The number of workers that we want to utilize in our cluster</li></ul></div><p>We want at least as many tasks as there are workers so that we can possibly
use all of them simultaneously, and this encourages us to break our work into
small units.  On the other hand, there is an overhead associated with each
task, and that encourages us to minimize the number of tasks.  Consequently,
we arrive at the following:</p><div><ul><li>If we only need to invoke our function a few times, and it takes only
one or two seconds to evaluate it, we are better off not using the Parallel
Computing Toolbox.  Instead, we should simply perform our computations using
MATLAB running on our local machine.</li></ul></div><div><ul><li>If we can evaluate our function very quickly, but we have to calculate many
function values, we should let a task consist of calculating a number of
function values.  This way, we can potentially use many of our workers
simultaneously, yet the task and job overhead is negligible relative to the
running time.  Note that we may have to write a new task function to do this,
see the
<a href="paralleltutorial_taskfunctions.html">Writing Task Functions</a> example.
The rule of thumb is: The quicker we can evaluate the function, the more
important it is to combine several function evaluations into a single task.</li></ul></div><div><ul><li>If it takes a long time to invoke our function, but we only need to
calculate a few function values, it seems sensible to let one task consist
of calculating one function value.  This way, the startup cost of the job
is negligible, and we can have several workers in our cluster work
simultaneously on the tasks in our job.</li></ul></div><div><ul><li>If it takes a long time to invoke our function, and we need to calculate
many function values, we can choose either of the two approaches we
have presented: let a task consist of invoking our function once or several
times.</li></ul></div><p>There is a drawback to having many tasks in a single job: Due to network
overhead, it may take a long time to create a job with a large number of
tasks, and during that time the cluster may be idle.  It is therefore
advisable to split the MATLAB operations into as many tasks as needed, but to
limit the number of tasks in a job to a reasonable number, say never more than
a few hundred tasks in a job.</p><p class="footer">Copyright 2007-2012 The MathWorks, Inc.<br>
          Published with MATLAB&reg; 7.14</p><p class="footer" id="trademarks">MATLAB and Simulink are registered trademarks of The MathWorks, Inc.  Please see <a href="http://www.mathworks.com/trademarks">www.mathworks.com/trademarks</a> for a list of other trademarks owned by The MathWorks, Inc.  Other product or brand names are trademarks or registered trademarks of their respective owners.</p></div><!--
##### SOURCE BEGIN #####
%% Dividing MATLAB(R) Computations into Tasks
% The Parallel Computing Toolbox(TM) enables us to execute our
% MATLAB(R) programs on a cluster of computers. In this example, we look
% at how to divide a large collection of MATLAB operations into smaller
% work units, called tasks, which the workers in our cluster can execute.
% We do this programmatically using the
% |pctdemo_helper_split_scalar| and
% |pctdemo_helper_split_vector| functions.
%
% Prerequisites:
% 
% * <paralleltutorial_defaults.html
% Customizing the Settings for the Examples in the Parallel Computing Toolbox>
%
% For further reading, see: 
%
% * <paralleltutorial_taskfunctions.html Writing Task Functions>
% * <paralleltutorial_network_traffic.html Minimizing Network Traffic>
% * <paralleltutorial_callbacks.html Using Callback Functions> 

%   Copyright 2007-2012 The MathWorks, Inc.

%% Obtaining the Profile
% Like every other example in the Parallel Computing Toolbox, this example needs to
% know what cluster to use.  We use the cluster identified by the default
% profile.  See the
% <matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp_ug.map'),'profiles_help'
% profile documentation>
% for how to create new profile and how to change the default
% profile.
profileName = parallel.defaultClusterProfile();

%% Starting with Sequential Code
% One of the important advantages of the Parallel Computing Toolbox is that
% it builds very well on top of existing sequential code.  It is actually
% beneficial to focus on sequential MATLAB code during the algorithm
% development, debugging and performance evaluation stages, because we then
% benefit from the rapid prototyping and interactive editing, debugging, and
% execution capabilities that MATLAB offers.  During the development of
% the sequential code, we should separate the computations from the pre- and the
% post-processing, and make the core of the computations as simple and
% independent from the rest of the code as possible.  Once our code is somewhat
% stable, it is time to look at distributing the computations.  If we do a good
% job of creating modular sequential code for a coarse grained application, it
% should be rather easy to distribute those computations.

%% Analyzing the Sequential Problem
% The Parallel Computing Toolbox supports the execution of coarse grained
% applications, that is, independent, simultaneous executions of a single
% program using multiple input arguments.  We now try to show examples of
% what coarse grained computations often look like in MATLAB code and explain
% how to distribute those kinds of computations.  We focus on two common
% scenarios, arising when the original, sequential MATLAB code consists of
% either
%
% * Invoking a single function several times, using different values for the
% input parameter.  Computations of this nature area sometimes referred to as
% *parameter sweeps*, and the code often looks similar to the following MATLAB
% code:

%%
%         for i = 1:n
%           y(i) = f(x(i));
%         end

%%
% * Invoking a single stochastic function several times.  Suppose that the
% calculations of |g(x)| involve random numbers, and the function thus returns a
% different value every time it is invoked (even though the input parameter |x|
% remains the same).  Such computations are sometimes referred to as *Monte
% Carlo simulations*, and the code often looks similar to the following MATLAB
% code:

%%
%         for i = 1:n
%           y(i) = g(x);
%         end

%%
% It is quite possible that the parameter sweeps and simulations appear in a
% slightly different form in our sequential MATLAB code.  For example, if the
% function |f| is vectorized, the parameter sweep may simply appear as

%%
%         y = f(x);

%%
% and the Monte Carlo simulation may appear as

%%
%         y = g(x, n);

%% Example: Dividing a Simulation into Tasks
% We use a very small example in what follows, using |rand| as our function
% of interest.  Imagine that we have a cluster with four workers, and we
% want to divide the function call |rand(1, 10)| between them.  This is by
% far simplest to do with |parfor| because it divides the computations
% between the workers without our having to make any decisions about how to
% best do that.

%%
% We can expand the function call |rand(1, 10)| into the corresponding
% |for| loop:

%%
%         for i = 1:10
%           y(i) = rand()
%         end

%%
% The parallelization using |parfor| simply consists of replacing the
% |for| with a |parfor|.  If the MATLAB pool is open on the four workers,
% this executes on the workers:

%%
%         parfor i = 1:10
%           y(i) = rand()
%         end

%%
% Alternatively, we can use |createJob| and |createTask| to
% divide the execution of |rand(1, 10)| between the four workers.  We use
% four tasks, and have them generate random vectors of length 3, 3, 2, and
% 2.  We have created a function called |pctdemo_helper_split_scalar| that
% helps divide the generation of the 10 random numbers between the 4 tasks:
numRand = 10; % We want this many random numbers.
numTasks = 4; % We want to split into this many tasks.
clust = parcluster(profileName);
job = createJob(clust);
[numPerTask, numTasks] = pctdemo_helper_split_scalar(numRand, numTasks);

%%
% Notice how |pctdemo_helper_split_scalar| splits the work of generating 10
% random numbers between the |numTasks| tasks.  The elements of |numPerTask| are
% all positive, the vector length is |numTasks|, and its sum equals |numRand|:

disp(numPerTask)

%%
% We can now write a for-loop that creates all the tasks in the job. Task |i| is
% to create a matrix of the size 1-by-numPerTask(i). When all the tasks have
% been created, we submit the job, wait for it to finish, and then retrieve the
% results.
for i = 1:numTasks
   createTask(job, @rand, 1, {1, numPerTask(i)});
end
submit(job);
wait(job);
y = fetchOutputs(job);
cat(2, y{:})   % Concatenate all the cells in y into one column vector.
delete(job);

%% Example: Dividing a Parameter Sweep into Tasks
% For the purposes of this example, let's use the |sin| function as 
% a very simple example. We let |x| be a vector of length 10:
x = 0.1:0.1:1;

%%
% and now we want to distribute the calculations of |sin(x)| on a cluster
% with 4 workers.  As before, this is easiest to achieve with |parfor|:

%%
%        parfor i = 1:length(x)
%            y(i) = sin(x(i));
%        end

%%
% If we decide to achieve this using jobs and tasks, we first need to
% determine how to divide the computations among the tasks.  We have the 4
% workers evaluate |sin(x(1:3))|, |sin(x(4:6))|, |sin(x(7:8))|, and
% |sin(x(9:10))| simultaneously. Because this kind of a division of a
% parameter sweep into separate tasks occurs frequently in our examples, we
% have created a function that does exactly that:
numTasks = 4;
[xSplit, numTasks] = pctdemo_helper_split_vector(x, numTasks);
celldisp(xSplit);

%%
% and it is now relatively easy to use |createJob| and
% |createTask|, to perform the computations:
job = createJob(clust);
for i = 1:numTasks
   xThis = xSplit{i};
   createTask(job, @sin, 1, {xThis});
end
submit(job);
wait(job);
y = fetchOutputs(job);
delete(job);
cat(2, y{:})  % Concatenate all the cells in y into one column vector.

%% More on Parameter Sweeps
% The example involving the |sin| function was particularly simple,
% because the |sin| function is vectorized.  We look at how to deal
% with nonvectorized functions in the 
% <paralleltutorial_taskfunctions.html Writing Task Functions> example. 

%% Dividing MATLAB Operations into Tasks: Best Practices
% When using jobs and tasks, we have to decide how to divide our
% computations into appropriately sized tasks, paying attention to the
% following:
%
% * The number of function calls we want to make
% * The time it takes to execute each function call
% * The number of workers that we want to utilize in our cluster
%
% We want at least as many tasks as there are workers so that we can possibly
% use all of them simultaneously, and this encourages us to break our work into
% small units.  On the other hand, there is an overhead associated with each
% task, and that encourages us to minimize the number of tasks.  Consequently,
% we arrive at the following:
%
% * If we only need to invoke our function a few times, and it takes only 
% one or two seconds to evaluate it, we are better off not using the Parallel
% Computing Toolbox.  Instead, we should simply perform our computations using
% MATLAB running on our local machine.
%
% * If we can evaluate our function very quickly, but we have to calculate many
% function values, we should let a task consist of calculating a number of
% function values.  This way, we can potentially use many of our workers
% simultaneously, yet the task and job overhead is negligible relative to the
% running time.  Note that we may have to write a new task function to do this,
% see the
% <paralleltutorial_taskfunctions.html Writing Task Functions> example.
% The rule of thumb is: The quicker we can evaluate the function, the more  
% important it is to combine several function evaluations into a single task.
%
% * If it takes a long time to invoke our function, but we only need to
% calculate a few function values, it seems sensible to let one task consist
% of calculating one function value.  This way, the startup cost of the job
% is negligible, and we can have several workers in our cluster work 
% simultaneously on the tasks in our job.
%
% * If it takes a long time to invoke our function, and we need to calculate 
% many function values, we can choose either of the two approaches we
% have presented: let a task consist of invoking our function once or several
% times. 
% 
% There is a drawback to having many tasks in a single job: Due to network
% overhead, it may take a long time to create a job with a large number of
% tasks, and during that time the cluster may be idle.  It is therefore
% advisable to split the MATLAB operations into as many tasks as needed, but to
% limit the number of tasks in a job to a reasonable number, say never more than
% a few hundred tasks in a job.


displayEndOfDemoMessage(mfilename)

##### SOURCE END #####
--></body></html>
