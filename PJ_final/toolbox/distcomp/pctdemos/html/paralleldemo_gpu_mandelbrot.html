
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <!--
This HTML was auto-generated from MATLAB code.
To make changes, update the MATLAB code and republish this document.
      --><title>Calculating the Mandelbrot Set on a GPU</title><meta name="generator" content="MATLAB 8.1"><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"><meta name="DC.date" content="2012-12-06"><meta name="DC.source" content="paralleldemo_gpu_mandelbrot.m"><link rel="stylesheet" type="text/css" href="../../../matlab/helptools/private/style.css"></head><body><div class="header"><div class="left"><a href="matlab:edit paralleldemo_gpu_mandelbrot">Open paralleldemo_gpu_mandelbrot.m in the Editor</a></div><div class="right"><a href="matlab:echodemo paralleldemo_gpu_mandelbrot">Run in the Command Window</a></div></div><div class="content"><h1>Calculating the Mandelbrot Set on a GPU</h1><!--introduction--><p>This example shows how a simple, well-known mathematical problem, the Mandelbrot Set, can be expressed in MATLAB&reg; code.  Using Parallel Computing Toolbox&#8482; this code is then adapted to make use of GPU hardware in three ways:</p><div><ol><li>Using the existing algorithm but with GPU data as input</li><li>Using <tt>arrayfun</tt> to perform the algorithm on each element independently</li><li>Using the MATLAB/CUDA interface to run some existing CUDA/C++ code</li></ol></div><!--/introduction--><h2>Contents</h2><div><ul><li><a href="#1">Setup</a></li><li><a href="#2">The Mandelbrot Set in MATLAB</a></li><li><a href="#3">Using <tt>gpuArray</tt></a></li><li><a href="#4">Element-wise Operation</a></li><li><a href="#5">Working with CUDA</a></li><li><a href="#6">Summary</a></li></ul></div><h2>Setup<a name="1"></a></h2><p>The values below specify a highly zoomed part of the Mandelbrot Set in the valley between the main cardioid and the p/q bulb to its left.</p><p><img vspace="5" hspace="5" src="paralleldemo_gpu_mandelbrot_location.png" alt=""> </p><p>A 1000x1000 grid of real parts (X) and imaginary parts (Y) is created between these limits and the Mandelbrot algorithm is iterated at each grid location. For this particular location 500 iterations will be enough to fully render the image.</p><pre class="codeinput">maxIterations = 500;
gridSize = 1000;
xlim = [-0.748766713922161, -0.748766707771757];
ylim = [ 0.123640844894862,  0.123640851045266];
</pre><h2>The Mandelbrot Set in MATLAB<a name="2"></a></h2><p>Below is an implementation of the Mandelbrot Set using standard MATLAB commands running on the CPU. This is based on the code provided in Cleve Moler's <a href="http://www.mathworks.com/moler/exm/chapters.html">"Experiments with MATLAB"</a> e-book.</p><p>This calculation is vectorized such that every location is updated at once.</p><pre class="codeinput"><span class="comment">% Setup</span>
t = tic();
x = linspace( xlim(1), xlim(2), gridSize );
y = linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );
z0 = xGrid + 1i*yGrid;
count = ones( size(z0) );

<span class="comment">% Calculate</span>
z = z0;
<span class="keyword">for</span> n = 0:maxIterations
    z = z.*z + z0;
    inside = abs( z )&lt;=2;
    count = count + inside;
<span class="keyword">end</span>
count = log( count );

<span class="comment">% Show</span>
cpuTime = toc( t );
set( gcf, <span class="string">'Position'</span>, [200 200 600 600] );
imagesc( x, y, count );
axis <span class="string">image</span>
colormap( [jet();flipud( jet() );0 0 0] );
title( sprintf( <span class="string">'%1.2fsecs (without GPU)'</span>, cpuTime ) );
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_mandelbrot_01.png" alt=""> <h2>Using <tt>gpuArray</tt><a name="3"></a></h2><p>When MATLAB encounters data on the GPU, calculations with that data are performed on the GPU. The class <a href="matlab:doc('gpuArray')"><tt>gpuArray</tt></a> provides GPU versions of many functions that you can use to create data arrays, including the <a href="matlab:doc('gpuArray.linspace')"><tt>linspace</tt></a>, <a href="matlab:doc('gpuArray.logspace')"><tt>logspace</tt></a>, and <a href="matlab:doc('gpuArray.meshgrid')"><tt>meshgrid</tt></a> functions needed here.  Similarly, the <tt>count</tt> array is initialized directly on the GPU using the function <a href="matlab:doc('gpuArray.ones')"><tt>gpuArray.ones</tt></a>.</p><p>With these changes to the data initialization the calculations will now be performed on the GPU:</p><pre class="codeinput"><span class="comment">% Setup</span>
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );
z0 = complex( xGrid, yGrid );
count = gpuArray.ones( size(z0) );

<span class="comment">% Calculate</span>
z = z0;
<span class="keyword">for</span> n = 0:maxIterations
    z = z.*z + z0;
    inside = abs( z )&lt;=2;
    count = count + inside;
<span class="keyword">end</span>
count = log( count );

<span class="comment">% Show</span>
count = gather( count ); <span class="comment">% Fetch the data back from the GPU</span>
naiveGPUTime = toc( t );
imagesc( x, y, count )
axis <span class="string">image</span>
title( sprintf( <span class="string">'%1.3fsecs (naive GPU) = %1.1fx faster'</span>, <span class="keyword">...</span>
    naiveGPUTime, cpuTime/naiveGPUTime ) )
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_mandelbrot_02.png" alt=""> <h2>Element-wise Operation<a name="4"></a></h2><p>Noting that the algorithm is operating equally on every element of the input, we can place the code in a helper function and call it using <a href="matlab:doc('gpuArray.arrayfun')"><tt>arrayfun</tt></a>.  For GPU array inputs, the function used with <tt>arrayfun</tt> gets compiled into native GPU code.  In this case we placed the loop in <a href="matlab:edit('pctdemo_processMandelbrotElement.m')"><tt>pctdemo_processMandelbrotElement.m</tt></a>:</p><pre>function count = pctdemo_processMandelbrotElement(x0,y0,maxIterations)
z0 = complex(x0,y0);
z = z0;
count = 1;
while (count &lt;= maxIterations) &amp;&amp; (abs(z) &lt;= 2)
    count = count + 1;
    z = z*z + z0;
end
count = log(count);</pre><p>Note that an early abort has been introduced because this function processes only a single element. For most views of the Mandelbrot Set a significant number of elements stop very early and this can save a lot of processing. The <tt>for</tt> loop has also been replaced by a <tt>while</tt> loop because they are usually more efficient. This function makes no mention of the GPU and uses no GPU-specific features - it is standard MATLAB code.</p><p>Using <tt>arrayfun</tt> means that instead of many thousands of calls to separate GPU-optimized operations (at least 6 per iteration), we make one call to a parallelized GPU operation that performs the whole calculation. This significantly reduces overhead.</p><pre class="codeinput"><span class="comment">% Setup</span>
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );

<span class="comment">% Calculate</span>
count = arrayfun( @pctdemo_processMandelbrotElement, <span class="keyword">...</span>
                  xGrid, yGrid, maxIterations );

<span class="comment">% Show</span>
count = gather( count ); <span class="comment">% Fetch the data back from the GPU</span>
gpuArrayfunTime = toc( t );
imagesc( x, y, count )
axis <span class="string">image</span>
title( sprintf( <span class="string">'%1.3fsecs (GPU arrayfun) = %1.1fx faster'</span>, <span class="keyword">...</span>
    gpuArrayfunTime, cpuTime/gpuArrayfunTime ) );
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_mandelbrot_03.png" alt=""> <h2>Working with CUDA<a name="5"></a></h2><p>In <a href="http://www.mathworks.com/moler/exm/index.html">Experiments in MATLAB</a> improved performance is achieved by converting the basic algorithm to a C-Mex function. If you are willing to do some work in C/C++, then you can use Parallel Computing Toolbox to call pre-written CUDA kernels using MATLAB data. You do this with the <a href="matlab:doc('parallel.gpu.CUDAKernel')"><tt>parallel.gpu.CUDAKernel</tt></a> feature.</p><p>A CUDA/C++ implementation of the element processing algorithm has been hand-written in <a href="matlab:edit('pctdemo_processMandelbrotElement.cu')"><tt>pctdemo_processMandelbrotElement.cu</tt></a>. This must then be manually compiled using nVidia's NVCC compiler to produce the assembly-level <tt>pctdemo_processMandelbrotElement.ptx</tt> (<tt>.ptx</tt> stands for "Parallel Thread eXecution language").</p><p>The CUDA/C++ code is a little more involved than the MATLAB versions we have seen so far, due to the lack of complex numbers in C++. However, the essence of the algorithm is unchanged:</p><pre>__device__
unsigned int doIterations( double const realPart0,
                           double const imagPart0,
                           unsigned int const maxIters ) {
   // Initialize: z = z0
   double realPart = realPart0;
   double imagPart = imagPart0;
   unsigned int count = 0;
   // Loop until escape
   while ( ( count &lt;= maxIters )
          &amp;&amp; ((realPart*realPart + imagPart*imagPart) &lt;= 4.0) ) {
      ++count;
      // Update: z = z*z + z0;
      double const oldRealPart = realPart;
      realPart = realPart*realPart - imagPart*imagPart + realPart0;
      imagPart = 2.0*oldRealPart*imagPart + imagPart0;
   }
   return count;
}</pre><p>One GPU thread is required for location in the Mandelbrot Set, with the threads grouped into blocks. The kernel indicates how big a thread-block is, and in the code below we use this to calculate the number of thread-blocks required. This then becomes the <tt>GridSize</tt>.</p><pre class="codeinput"><span class="comment">% Load the kernel</span>
cudaFilename = <span class="string">'pctdemo_processMandelbrotElement.cu'</span>;
ptxFilename = [<span class="string">'pctdemo_processMandelbrotElement.'</span>,parallel.gpu.ptxext];
kernel = parallel.gpu.CUDAKernel( ptxFilename, cudaFilename );

<span class="comment">% Setup</span>
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );

<span class="comment">% Make sure we have sufficient blocks to cover all of the locations</span>
numElements = numel( xGrid );
kernel.ThreadBlockSize = [kernel.MaxThreadsPerBlock,1,1];
kernel.GridSize = [ceil(numElements/kernel.MaxThreadsPerBlock),1];

<span class="comment">% Call the kernel</span>
count = gpuArray.zeros( size(xGrid) );
count = feval( kernel, count, xGrid, yGrid, maxIterations, numElements );

<span class="comment">% Show</span>
count = gather( count ); <span class="comment">% Fetch the data back from the GPU</span>
gpuCUDAKernelTime = toc( t );
imagesc( x, y, count )
axis <span class="string">image</span>
title( sprintf( <span class="string">'%1.3fsecs (GPU CUDAKernel) = %1.1fx faster'</span>, <span class="keyword">...</span>
    gpuCUDAKernelTime, cpuTime/gpuCUDAKernelTime ) );
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_mandelbrot_04.png" alt=""> <h2>Summary<a name="6"></a></h2><p>This example has shown three ways in which a MATLAB algorithm can be adapted to make use of GPU hardware:</p><div><ol><li>Convert the input data to be on the GPU using <a href="matlab:doc('gpuArray')"><tt>gpuArray</tt></a>, leaving the algorithm unchanged</li><li>Use <a href="matlab:doc('gpuArray.arrayfun')"><tt>arrayfun</tt></a> on a <tt>gpuArray</tt> input to perform the algorithm on each element of the input independently</li><li>Use <a href="matlab:doc('parallel.gpu.CUDAKernel')"><tt>parallel.gpu.CUDAKernel</tt></a> to run some existing CUDA/C++ code using MATLAB data</li></ol></div><p class="footer">Copyright 2011-2012 The MathWorks, Inc.<br><a href="http://www.mathworks.com/products/matlab/">Published with MATLAB&reg; R2013a</a><br><br>
		  MATLAB and Simulink are registered trademarks of The MathWorks, Inc.  Please see <a href="http://www.mathworks.com/trademarks">www.mathworks.com/trademarks</a> for a list of other trademarks owned by The MathWorks, Inc.  Other product or brand names are trademarks or registered trademarks of their respective owners.
      </p></div><!--
##### SOURCE BEGIN #####
%% Calculating the Mandelbrot Set on a GPU
% This example shows how a simple, well-known mathematical problem, the 
% Mandelbrot Set, can be expressed in MATLAB(R) code.  Using 
% Parallel Computing Toolbox(TM) this code is then adapted to make use of
% GPU hardware in three ways:
%
% # Using the existing algorithm but with GPU data as input
% # Using |arrayfun| to perform the algorithm on each element independently
% # Using the MATLAB/CUDA interface to run some existing CUDA/C++ code

% Copyright 2011-2012 The MathWorks, Inc.


%% Setup
% The values below specify a highly zoomed part of the Mandelbrot Set in
% the valley between the main cardioid and the p/q bulb to its left.  
%
% <<paralleldemo_gpu_mandelbrot_location.png>>
%
% A 1000x1000 grid of real parts (X) and imaginary parts (Y) is created
% between these limits and the Mandelbrot algorithm is iterated at each
% grid location. For this particular location 500 iterations will be enough
% to fully render the image.
maxIterations = 500;
gridSize = 1000;
xlim = [-0.748766713922161, -0.748766707771757];
ylim = [ 0.123640844894862,  0.123640851045266];


%% The Mandelbrot Set in MATLAB
% Below is an implementation of the Mandelbrot Set using
% standard MATLAB commands running on the CPU. This is based on the code
% provided in Cleve Moler's 
% <http://www.mathworks.com/moler/exm/chapters.html "Experiments with
% MATLAB"> e-book.
%
% This calculation is vectorized such that every location is updated at
% once.

% Setup
t = tic();
x = linspace( xlim(1), xlim(2), gridSize );
y = linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );
z0 = xGrid + 1i*yGrid;
count = ones( size(z0) );

% Calculate
z = z0;
for n = 0:maxIterations
    z = z.*z + z0;
    inside = abs( z )<=2;
    count = count + inside;
end
count = log( count );

% Show
cpuTime = toc( t );
set( gcf, 'Position', [200 200 600 600] );
imagesc( x, y, count );
axis image
colormap( [jet();flipud( jet() );0 0 0] );
title( sprintf( '%1.2fsecs (without GPU)', cpuTime ) );


%% Using |gpuArray|
% When MATLAB encounters data on the GPU, calculations with that data are
% performed on the GPU. The class 
% <matlab:doc('gpuArray') |gpuArray|> provides
% GPU versions of many functions that you can use to create data arrays,
% including the
% <matlab:doc('gpuArray.linspace') |linspace|>, 
% <matlab:doc('gpuArray.logspace') |logspace|>, and
% <matlab:doc('gpuArray.meshgrid') |meshgrid|> functions
% needed here.  Similarly, the |count| array is initialized directly on the
% GPU using the function 
% <matlab:doc('gpuArray.ones') |gpuArray.ones|>.
%
% With these changes to the data initialization the calculations will now
% be performed on the GPU:

% Setup
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );
z0 = complex( xGrid, yGrid );
count = gpuArray.ones( size(z0) );

% Calculate
z = z0;
for n = 0:maxIterations
    z = z.*z + z0;
    inside = abs( z )<=2;
    count = count + inside;
end
count = log( count );

% Show
count = gather( count ); % Fetch the data back from the GPU
naiveGPUTime = toc( t );
imagesc( x, y, count )
axis image
title( sprintf( '%1.3fsecs (naive GPU) = %1.1fx faster', ...
    naiveGPUTime, cpuTime/naiveGPUTime ) )


%% Element-wise Operation
% Noting that the algorithm is operating equally on every element of the
% input, we can place the code in a helper function and call it using
% <matlab:doc('gpuArray.arrayfun') |arrayfun|>.  For GPU array
% inputs, the function used with |arrayfun| gets compiled into native GPU
% code.  In this case we placed the loop in
% <matlab:edit('pctdemo_processMandelbrotElement.m') 
% |pctdemo_processMandelbrotElement.m|>:
%
%  function count = pctdemo_processMandelbrotElement(x0,y0,maxIterations)
%  z0 = complex(x0,y0);
%  z = z0;
%  count = 1;
%  while (count <= maxIterations) && (abs(z) <= 2)
%      count = count + 1;
%      z = z*z + z0;
%  end
%  count = log(count);
%
% Note that an early abort has been introduced because this function
% processes only a single element. For most views of the Mandelbrot Set a
% significant number of elements stop very early and this can save a lot of
% processing. The |for| loop has also been replaced by a |while| loop
% because they are usually more efficient. This function makes no mention
% of the GPU and uses no GPU-specific features - it is standard MATLAB
% code.
%
% Using |arrayfun| means that instead of many thousands of calls to
% separate GPU-optimized operations (at least 6 per iteration), we make one
% call to a parallelized GPU operation that performs the whole calculation.
% This significantly reduces overhead.

% Setup
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );

% Calculate
count = arrayfun( @pctdemo_processMandelbrotElement, ...
                  xGrid, yGrid, maxIterations );

% Show
count = gather( count ); % Fetch the data back from the GPU
gpuArrayfunTime = toc( t );
imagesc( x, y, count )
axis image
title( sprintf( '%1.3fsecs (GPU arrayfun) = %1.1fx faster', ...
    gpuArrayfunTime, cpuTime/gpuArrayfunTime ) );


%% Working with CUDA
% In <http://www.mathworks.com/moler/exm/index.html Experiments in MATLAB> 
% improved performance is achieved by converting the basic algorithm to a
% C-Mex function. If you are willing to do some work in C/C++, then you can
% use Parallel Computing Toolbox to call pre-written CUDA kernels using
% MATLAB data. You do this with the
% <matlab:doc('parallel.gpu.CUDAKernel') |parallel.gpu.CUDAKernel|>
% feature.
%
% A CUDA/C++
% implementation of the element processing algorithm has been hand-written
% in <matlab:edit('pctdemo_processMandelbrotElement.cu')
% |pctdemo_processMandelbrotElement.cu|>. This must then be manually
% compiled using nVidia's NVCC compiler to produce the assembly-level
% |pctdemo_processMandelbrotElement.ptx| (|.ptx| stands for "Parallel
% Thread eXecution language").
%
% The CUDA/C++ code is a little more involved than the MATLAB versions 
% we have seen so far, due to the lack of complex numbers in C++. However,
% the essence of the algorithm is unchanged:
%
%  __device__
%  unsigned int doIterations( double const realPart0, 
%                             double const imagPart0, 
%                             unsigned int const maxIters ) {
%     // Initialize: z = z0
%     double realPart = realPart0;
%     double imagPart = imagPart0;
%     unsigned int count = 0;
%     // Loop until escape
%     while ( ( count <= maxIters )
%            && ((realPart*realPart + imagPart*imagPart) <= 4.0) ) {
%        ++count;
%        // Update: z = z*z + z0;
%        double const oldRealPart = realPart;
%        realPart = realPart*realPart - imagPart*imagPart + realPart0;
%        imagPart = 2.0*oldRealPart*imagPart + imagPart0;
%     }
%     return count;
%  }
%
% One GPU thread is required for location in the Mandelbrot Set, with the
% threads grouped into blocks. The kernel indicates how big a thread-block
% is, and in the code below we use this to calculate the number of
% thread-blocks required. This then becomes the |GridSize|.

% Load the kernel
cudaFilename = 'pctdemo_processMandelbrotElement.cu';
ptxFilename = ['pctdemo_processMandelbrotElement.',parallel.gpu.ptxext];
kernel = parallel.gpu.CUDAKernel( ptxFilename, cudaFilename );

% Setup
t = tic();
x = gpuArray.linspace( xlim(1), xlim(2), gridSize );
y = gpuArray.linspace( ylim(1), ylim(2), gridSize );
[xGrid,yGrid] = meshgrid( x, y );

% Make sure we have sufficient blocks to cover all of the locations
numElements = numel( xGrid );
kernel.ThreadBlockSize = [kernel.MaxThreadsPerBlock,1,1];
kernel.GridSize = [ceil(numElements/kernel.MaxThreadsPerBlock),1];
                              
% Call the kernel
count = gpuArray.zeros( size(xGrid) );
count = feval( kernel, count, xGrid, yGrid, maxIterations, numElements );
       
% Show
count = gather( count ); % Fetch the data back from the GPU
gpuCUDAKernelTime = toc( t );
imagesc( x, y, count )
axis image
title( sprintf( '%1.3fsecs (GPU CUDAKernel) = %1.1fx faster', ...
    gpuCUDAKernelTime, cpuTime/gpuCUDAKernelTime ) );
                      

%% Summary
% This example has shown three ways in which a MATLAB algorithm can be
% adapted to make use of GPU hardware:
%
% # Convert the input data to be on the GPU using 
% <matlab:doc('gpuArray') |gpuArray|>, leaving
% the algorithm unchanged
% # Use <matlab:doc('gpuArray.arrayfun') |arrayfun|> on a
% |gpuArray| input to perform the algorithm on each element of the input
% independently
% # Use <matlab:doc('parallel.gpu.CUDAKernel') |parallel.gpu.CUDAKernel|>
% to run some existing CUDA/C++ code using MATLAB data
displayEndOfDemoMessage(mfilename)

##### SOURCE END #####
--></body></html>