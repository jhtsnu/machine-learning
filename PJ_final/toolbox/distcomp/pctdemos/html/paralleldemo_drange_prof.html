
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   
      <!--
This HTML is auto-generated from an M-file.
To make changes, update the M-file and republish this document.
      -->
      <title>Profiling Parallel Work Distribution</title>
      <meta name="generator" content="MATLAB 7.7">
      <meta name="date" content="2008-07-16">
      <meta name="m-file" content="paralleldemo_drange_prof">
      <link rel="stylesheet" type="text/css" href="../../../matlab/demos/private/style.css">
   </head>
   <body>
      <div class="header">
         <div class="left"><a href="matlab:edit paralleldemo_drange_prof">Open paralleldemo_drange_prof.m in the Editor</a></div>
         <div class="right">This example can run only in pmode</div>
      </div>
      <div class="content">
         <h1>Profiling Parallel Work Distribution</h1>
         <!--introduction-->
         <p>This example shows how to solve an embarrassingly parallel problem with uneven work distribution using <tt>for drange</tt>. The <tt>for drange</tt> splits iterations equally. As a result it can do suboptimal load balancing, which is visible using the parallel profiler.
            The procedures described here are also applicable to other work distribution problems.
         </p>
         <!--/introduction-->
         <h2>Contents</h2>
         <div>
            <ul>
               <li><a href="#3">The Algorithm</a></li>
               <li><a href="#4">Enabling the Parallel Profiler's Data Collection</a></li>
               <li><a href="#9">Running the Parallel Profiler Graphical Interface</a></li>
               <li><a href="#13">Observing Uneven Work Distribution</a></li>
               <li><a href="#17">Using PARFOR for Better Work Distribution</a></li>
               <li><a href="#21">Parallelizing Serial for Loops</a></li>
            </ul>
         </div>
         <p><b>Prerequisites</b>:
         </p>
         <div>
            <ul>
               <li>Interactive Parallel Mode in Parallel Computing Toolbox&#8482; (See <tt>pmode</tt> in the user's guide.)
               </li>
               <li><a href="paralleltutorial_parprofile.html">Using the Parallel Profiler in Pmode</a></li>
            </ul>
         </div>
         <p>The plots in this example are produced from a 12-node MATLAB&reg; cluster. If not otherwise specified, everything else is shown running
            on a 4-node local cluster. In particular, all text output is from a local cluster.
         </p>
         <p>This example uses <tt>for drange</tt> to illustrate how you use the profiler to observe  suboptimal load distribution. Let's look at this embarrassingly parallel
            <tt>for drange</tt> loop.
         </p>
         <h2>The Algorithm<a name="3"></a></h2>
         <p>The objective in the <tt>pctdemo_aux_proftaskpar</tt> example is to calculate the <tt>eig</tt> of a random matrix of increasing size, and pick the maximum value from the resulting vector. The crucial issue is the increasing
            matrix size based on the loop counter <tt>ii</tt>. Here is the basic iteration:
         </p>
         <p><tt>v(ii) = max( abs( eig( rand(ii) ) ) )</tt>;
         </p>
         <p>The actual <tt>for</tt> loop can be seen in the example code.
         </p>
         <p>The code for this example can be found in <a href="matlab:edit('pctdemo_aux_proftaskpar.m')">pctdemo_aux_proftaskpar</a>.
         </p>
         <h2>Enabling the Parallel Profiler's Data Collection<a name="4"></a></h2>
         <p>A good practice is to reset the parallel profiler on the cluster before turning on mpiprofile in <tt>pmode</tt>. It makes sure the data is cleared and the profiler is off and in default -messagedetail setting.
         </p><pre class="codeinput">P&gt;&gt; mpiprofile reset;
P&gt;&gt; mpiprofile on;
</pre><p>Inside a <tt>for drange</tt> there cannot be any communication between labs so the <tt>-messagedetail</tt> can be set to simplified (see help <tt>mpiprofile</tt>). If you do not specify the <tt>-messagedetail</tt> option and you run a program with no communication, you get 0s in the communication fields.
         </p><pre class="codeinput">P&gt;&gt; v = zeros( 1, 300, codistributor() );
P&gt;&gt; tic;pctdemo_aux_proftaskpar(<span class="string">'drange'</span>);toc;
</pre><pre>1    Start of for-drange loop.
     The computational complexity increases with the loop index.
     Done
     Elapsed time is 0.718183 seconds.</pre><pre>2    Start of for-drange loop.
     The computational complexity increases with the loop index.
     Done
     Elapsed time is 2.083491 seconds.</pre><pre>3    Start of for-drange loop.
     The computational complexity increases with the loop index.
     Done
     Elapsed time is 6.487598 seconds.</pre><pre>4    Start of for-drange loop.
     The computational complexity increases with the loop index.
     Done
     Elapsed time is 12.927539 seconds.</pre><p>In this algorithm the elapsed time should always be longest on the last lab. We use <tt>tic toc</tt> here so that we can compare the longest running time to a <tt>parfor</tt>. The use of profiling inside a <tt>parfor</tt> loop with <tt>mpiprofile</tt> is currently not supported.
         </p>
         <h2>Running the Parallel Profiler Graphical Interface<a name="9"></a></h2>
         <p>To get the profiler interface, simply type <tt>mpiprofile viewer</tt> in <tt>pmode</tt>. You can also view data from a parallel job. See the help or documentation for information on how to do this.
         </p><pre class="codeinput">P&gt;&gt; mpiprofile viewer; <span class="comment">% The viewer action also turns off the profiler</span>
</pre><pre>1    Sending  pmode lab2client  to the MATLAB client for asynchronous evaluation.</pre><p>When the profiler interface opens, by default the Function Summary Report is shown for lab 1. Click <b>Compare max vs min TotalTime</b> to see the difference in work distribution between the first and last lab for all the functions called. Look at the <tt>pctdemo_aux_proftaskpar</tt> function:
         </p>
         <p><img vspace="5" hspace="5" src="paralleldemo_drange_profsp.png" alt=""> </p>
         <h2>Observing Uneven Work Distribution<a name="13"></a></h2>
         <p>Here are a few steps for spotting uneven work distribution on the MATLAB workers. Uneven work distribution almost certainly
            prevents optimal speedup of serial algorithms.
         </p>
         <div>
            <ol>
               <li>Select <tt>max Time Aggregate</tt> from the <b>Manual Comparison Selection</b> listbox (see <a href="paralleltutorial_parprofile.html">Using the Parallel Profiler in Pmode</a>). With this selection you can observe the effective total time for a parallel program.
               </li>
               <li>Click <b>Compare max vs. min TotalTime</b>.  As you can see, this loop takes much longer on the last MATLAB worker compared to the first one. The <tt>for drange</tt> is clearly not distributing the work correctly, at least on these two labs. To confirm this is true for all the labs, you
                  can use the histogram feature of the Plot View page. Before doing so, click the <tt>pctdemo_aux_proftaskpar</tt> function to get more specific plots.
               </li>
               <li>Click <b>Plot Time Histograms</b> to see how the computation time was distributed on the four local labs. Observe the total execution time histogram.
               </li>
            </ol>
         </div>
         <p>In the top figure of this page, only the first few labs take approximately the same amount of time; the others take significantly
            longer. This large difference in the total time distribution is an indicator of suboptimal load balancing.
         </p>
         <p><img vspace="5" hspace="5" src="paralleldemo_drange_profselhist.png" alt=""> </p>
         <p><img vspace="5" hspace="5" src="paralleldemo_drange_profhist12labs.png" alt=""> </p>
         <h2>Using PARFOR for Better Work Distribution<a name="17"></a></h2>
         <p>Optimal performance for this type of parallelism requires manual distribution of the iterations in <tt>pmode</tt> or the use of <tt>parfor</tt> with <tt>matlabpool</tt>. To get better work distribution (with pmode) in this type of a problem, you need to create a random distribution of the
            tasks rather than rely on <tt>for drange</tt> to statically partition the iterations.
         </p>
         <p>Using <tt>parfor</tt> is generally better suited to this type of task. With the <tt>parfor (i=n:N)</tt> construct you get dynamic work distribution which splits the iterations at execution time, across all labs. Thus the cluster
            is better utilized. You can see this by running the same function outside of pmode using a <tt>parfor</tt> construct. This results in significantly higher speedup compared to the <tt>for drange</tt>.
         </p>
         <p>To try this with parfor, run the following commands on the MATLAB client outside of <tt>pmode</tt>.
         </p>
         <p><tt>pmode close;</tt></p>
         <p><tt>matlabpool;</tt></p>
         <p><tt>tic;pctdemo_aux_proftaskpar('parfor');toc;</tt></p>
         <p>You should get an output that looks like:  Done  Elapsed time is 6.376887 seconds.</p>
         <p>There is a significant speedup (it's nearly two times faster on the 4-node cluster) using <tt>parfor</tt> instead of <tt>for drange</tt>, with no change to the actual algorithm. Note that <tt>parfor</tt> operates as a standard <tt>for</tt> loop inside of <tt>pmode</tt>. Please ensure you try parfor outside of pmode to get the speedup. See help for <tt>matlabpool</tt> and <tt>parfor</tt>.
         </p>
         <h2>Parallelizing Serial for Loops<a name="21"></a></h2>
         <p>To make a serial (iteration independent) <tt>for</tt>-loop  parallel you need to add the <tt>drange</tt> option when inside a parallel job, or replace <tt>for</tt> with a <tt>parfor</tt>. The <tt>parfor</tt> loop will only work as intended with <tt>matlabpool</tt>. You can view the different styles of for loop parallelism in the code shown in this example. See <a href="matlab:edit('pctdemo_aux_proftaskpar.m')">pctdemo_aux_proftaskpar</a>. The <tt>parfor</tt> version is under the case <tt>'parfor'</tt> and the <tt>drange</tt> version is under the case <tt>'drange'</tt>.
         </p>
         <p class="footer">Copyright 2007-2011 The MathWorks, Inc.<br>
            Published with MATLAB&reg; 7.7
         </p>
         <p class="footer" id="trademarks">MATLAB and Simulink are registered trademarks of The MathWorks, Inc.  Please see <a href="http://www.mathworks.com/trademarks">www.mathworks.com/trademarks</a> for a list of other trademarks owned by The MathWorks, Inc.  Other product or brand names are trademarks or registered trademarks
            of their respective owners.
         </p>
      </div>
      <!--
##### SOURCE BEGIN #####
%% Profiling Parallel Work Distribution
% This example shows how to solve an embarrassingly parallel problem with uneven work
% distribution using |for drange|. The |for drange| splits iterations
% equally. As a result it can do suboptimal load balancing, which is
% visible using the parallel profiler. The procedures described here are
% also applicable to other work distribution problems.  

%   Copyright 2007-2011 The MathWorks, Inc.

%% 
% *Prerequisites*:
%
% * Interactive Parallel Mode in Parallel Computing Toolbox(TM) (See |pmode|
% in the user's guide.)
% * <paralleltutorial_parprofile.html Using the Parallel Profiler in Pmode>
%
% 
% The plots in this example are produced from a 12-node MATLAB(R) cluster. If not
% otherwise specified, everything else is shown running on a 4-node local
% clusterer. In particular, all text output is from a local cluster.

%%
% This example uses |for drange| to illustrate how you use the
% profiler to observe  suboptimal load distribution. Let's look at this
% embarrassingly parallel |for drange| loop. 

%% The Algorithm
% The objective in the |pctdemo_aux_proftaskpar| example is to calculate
% the |eig| of a random matrix of increasing size, and pick the maximum
% value from the resulting vector. The crucial issue is the increasing
% matrix size based on the loop counter |ii|. Here is the basic iteration:
%
% |v(ii) = max( abs( eig( rand(ii) ) ) )|;
%
% The actual |for| loop can be seen in the example code.
%
% The code for this example can be found in
% <matlab:edit('pctdemo_aux_proftaskpar.m') pctdemo_aux_proftaskpar>.  

%%      Enabling the Parallel Profiler's Data Collection
%
% A good practice is to reset the parallel profiler on the cluster before
% turning on mpiprofile in |pmode|. It makes sure the data is cleared and
% the profiler is off and in default -messagedetail setting.

mpiprofile reset;
mpiprofile on;

%%
% Inside a |for drange| there cannot be any communication between labs so
% the |-messagedetail| can be set to simplified (see help |mpiprofile|). If
% you do not specify the |-messagedetail| option and you run a program with
% no communication, you get 0s in the communication fields.

v = zeros( 1, 300, codistributor() );
tic;pctdemo_aux_proftaskpar('drange');toc;

%%
% In this algorithm the elapsed time should always be longest on the last
% lab. We use |tic toc| here so that we can compare the longest running
% time to a |parfor|. The use of profiling inside a |parfor| loop with
% |mpiprofile| is currently not supported. 
%

%%      Running the Parallel Profiler Graphical Interface
% To get the profiler interface, simply type |mpiprofile viewer| in
% |pmode|. You can also view data from a parallel job. See the help or
% documentation for information on how to do this.

mpiprofile viewer; % The viewer action also turns off the profiler

%%
% When the profiler interface opens, by default the Function Summary Report
% is shown for lab 1. Click *Compare max vs min TotalTime* to see the
% difference in work distribution between the first and last lab for all
% the functions called. Look at the |pctdemo_aux_proftaskpar| function: 

%%
% <<paralleldemo_drange_profsp.png>>

%% Observing Uneven Work Distribution
% Here are a few steps for spotting uneven work distribution on the MATLAB
% workers. Uneven work distribution almost certainly prevents optimal
% speedup of serial algorithms.

%%
% # Select |max Time Aggregate| from the *Manual Comparison
% Selection* listbox (see <paralleltutorial_parprofile.html
% Using the Parallel Profiler in Pmode>). 
% With this selection you can observe the effective total
% time for a parallel program. 
% # Click *Compare max vs. min TotalTime*.  As you can see, this loop takes
% much longer on the last MATLAB worker compared to the first one. The |for
% drange| is clearly not distributing the work correctly, at least on these
% two labs. To confirm this is true for all the labs, you can use the
% histogram feature of the Plot View page. Before doing so, click the
% |pctdemo_aux_proftaskpar| function to get more specific plots.
% # Click
% *Plot Time Histograms* to see how the computation time was distributed on
% the four local labs. Observe the total execution time histogram.
%
% In the top figure of this page, only the first few labs take
% approximately the same amount of time; the others take significantly
% longer. This large difference in the total time distribution is an
% indicator of suboptimal load balancing.

%%
% <<paralleldemo_drange_profselhist.png>>

%%
% <<paralleldemo_drange_profhist12labs.png>>


%% Using PARFOR for Better Work Distribution
% Optimal performance for this type of parallelism requires
% manual distribution of the iterations in |pmode| or the use of |parfor|
% with |matlabpool|. 
% To get better work distribution (with pmode) in this type of a problem,   
% you need to create a random distribution of the tasks rather than rely on
% |for drange| to statically partition the iterations.
%
% Using |parfor| is generally better suited to this type of task. With the
% |parfor (i=n:N)| construct you get dynamic work distribution which splits
% the iterations at execution time, across all labs. Thus the cluster is
% better utilized. You can see this by running the same function outside of
% pmode using a |parfor| construct. This results in significantly higher
% speedup compared to the |for drange|.


%%
% To try this with parfor, run the following commands on the MATLAB client
% outside of |pmode|.
%
% |pmode close;|
%
% |matlabpool;|
%
% |tic;pctdemo_aux_proftaskpar('parfor');toc;|

%%
% You should get an output that looks like:
%  Done
%  Elapsed time is 6.376887 seconds.
%
%

%%
% There is a significant speedup (it's nearly two times faster on the 
% 4-node cluster) using |parfor| instead of |for drange|, with no change to
% the actual algorithm. 
% Note that |parfor| operates as a standard |for| loop inside of |pmode|. 
% Please ensure you try parfor outside of pmode to get the speedup. See
% help for |matlabpool| and |parfor|.


%% Parallelizing Serial for Loops
% To make a serial (iteration independent) |for|-loop  parallel you need to
% add the |drange| option when inside a parallel job, or replace |for| with
% a |parfor|. The |parfor| loop will only work as intended with
% |matlabpool|. You can view the different styles of for loop parallelism
% in the code shown in this example. See 
% <matlab:edit('pctdemo_aux_proftaskpar.m') pctdemo_aux_proftaskpar>. 
% The |parfor| version is under the case |'parfor'| and the |drange|
% version is under the case |'drange'|.

displayEndOfDemoMessage(mfilename)

##### SOURCE END #####
-->
   </body>
</html>
