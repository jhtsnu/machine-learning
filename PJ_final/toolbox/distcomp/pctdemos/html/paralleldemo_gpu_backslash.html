
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <!--
This HTML was auto-generated from MATLAB code.
To make changes, update the MATLAB code and republish this document.
      --><title>Benchmarking A\b on the GPU</title><meta name="generator" content="MATLAB 8.0"><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"><meta name="DC.date" content="2012-04-27"><meta name="DC.source" content="paralleldemo_gpu_backslash.m"><link rel="stylesheet" type="text/css" href="../../../matlab/helptools/private/style.css"></head><body><div class="header"><div class="left"><a href="matlab:edit paralleldemo_gpu_backslash">Open paralleldemo_gpu_backslash.m in the Editor</a></div><div class="right">&nbsp;</div></div><div class="content"><h1>Benchmarking A\b on the GPU</h1><!--introduction--><p>This example looks at how we can benchmark the solving of a linear system on the GPU.  The MATLAB&reg; code to solve for <tt>x</tt> in <tt>A*x = b</tt> is very simple.  Most frequently, we use matrix left division, also known as <tt>mldivide</tt> or the backslash operator (\), to calculate <tt>x</tt> (that is, <tt>x = A\b</tt>).</p><p>Related examples:</p><div><ul><li><a href="paralleldemo_backslash_bench.html">Benchmarking A\b</a> using distributed arrays.</li></ul></div><!--/introduction--><h2>Contents</h2><div><ul><li><a href="#3">The Benchmarking Function</a></li><li><a href="#4">Choosing Problem Size</a></li><li><a href="#5">Comparing Performance: Gigaflops</a></li><li><a href="#6">Executing the Benchmarks</a></li><li><a href="#8">Plotting the Performance</a></li></ul></div><p>The code shown in this example can be found in this function:</p><pre class="codeinput"><span class="keyword">function</span> results = paralleldemo_gpu_backslash(maxMemory)
</pre><p>It is important to choose the appropriate matrix size for the computations.  We can do this by specifying the amount of system memory in GB available to the CPU and the GPU.  The default value is based only on the amount of memory available on the GPU, and you can specify a value that is appropriate for your system.</p><pre class="codeinput"><span class="keyword">if</span> nargin == 0
    g = gpuDevice;
    maxMemory = 0.4*g.FreeMemory/1024^3;
<span class="keyword">end</span>
</pre><h2>The Benchmarking Function<a name="3"></a></h2><p>We want to benchmark matrix left division (\), and not the cost of transferring data between the CPU and GPU, the time it takes to create a matrix, or other parameters.  We therefore separate the data generation from the solving of the linear system, and measure only the time it takes to do the latter.</p><pre class="codeinput"><span class="keyword">function</span> [A, b] = getData(n, clz)
    fprintf(<span class="string">'Creating a matrix of size %d-by-%d.\n'</span>, n, n);
    A = rand(n, n, clz) + 100*eye(n, n, clz);
    b = rand(n, 1, clz);
<span class="keyword">end</span>

<span class="keyword">function</span> time = timeSolve(A, b)
    tic;
    x = A\b; <span class="comment">%#ok&lt;NASGU&gt; We don't need the value of x.</span>
    time = toc;
<span class="keyword">end</span>
</pre><h2>Choosing Problem Size<a name="4"></a></h2><p>As with a great number of other parallel algorithms, the performance of solving a linear system in parallel depends greatly on the matrix size. As seen in other examples, such as <a href="paralleldemo_backslash_bench.html">Benchmarking A\b</a>, we compare the performance of the algorithm for different matrix sizes.</p><pre class="codeinput"><span class="comment">% Declare the matrix sizes to be a multiple of 1024.</span>
maxSizeSingle = floor(sqrt(maxMemory*1024^3/4));
maxSizeDouble = floor(sqrt(maxMemory*1024^3/8));
step = 1024;
<span class="keyword">if</span> maxSizeDouble/step &gt;= 10
    step = step*floor(maxSizeDouble/(5*step));
<span class="keyword">end</span>
sizeSingle = 1024:step:maxSizeSingle;
sizeDouble = 1024:step:maxSizeDouble;
</pre><h2>Comparing Performance: Gigaflops<a name="5"></a></h2><p>We use the number of floating point operations per second as our measure of performance because that allows us to compare the performance of the algorithm for different matrix sizes.</p><p>Given a matrix size, the benchmarking function creates the matrix <tt>A</tt> and the right-hand side <tt>b</tt> once, and then solves <tt>A\b</tt> a few times to get an accurate measure of the time it takes.  We use the floating point operations count of the HPC Challenge, so that for an n-by-n matrix, we count the floating point operations as <tt>2/3*n^3 + 3/2*n^2</tt>.</p><pre class="codeinput"><span class="keyword">function</span> gflops = benchFcn(A, b)
    numReps = 3;
    time = inf;
    <span class="comment">% We solve the linear system a few times and calculate the Gigaflops</span>
    <span class="comment">% based on the best time.</span>
    <span class="keyword">for</span> itr = 1:numReps
        tcurr = timeSolve(A, b);
        time = min(tcurr, time);
    <span class="keyword">end</span>
    n = size(A, 1);
    flop = 2/3*n^3 + 3/2*n^2;
    gflops = flop/time/1e9;
<span class="keyword">end</span>
</pre><h2>Executing the Benchmarks<a name="6"></a></h2><p>Having done all the setup, it is straightforward to execute the benchmarks.  However, the computations can take a long time to complete, so we print some intermediate status information as we complete the benchmarking for each matrix size.  We also encapsulate the loop over all the matrix sizes in a function, to benchmark both single- and double-precision computations.</p><pre class="codeinput"><span class="keyword">function</span> [gflopsCPU, gflopsGPU] = executeBenchmarks(clz, sizes)
    fprintf([<span class="string">'Starting benchmarks with %d different %s-precision '</span> <span class="keyword">...</span>
         <span class="string">'matrices of sizes\nranging from %d-by-%d to %d-by-%d.\n'</span>], <span class="keyword">...</span>
            length(sizes), clz, sizes(1), sizes(1), sizes(end), <span class="keyword">...</span>
            sizes(end));
    gflopsGPU = zeros(size(sizes));
    gflopsCPU = zeros(size(sizes));
    <span class="keyword">for</span> i = 1:length(sizes)
        n = sizes(i);
        [A, b] = getData(n, clz);
        gflopsCPU(i) = benchFcn(A, b);
        fprintf(<span class="string">'Gigaflops on CPU: %f\n'</span>, gflopsCPU(i));
        A = gpuArray(A);
        b = gpuArray(b);
        gflopsGPU(i) = benchFcn(A, b);
        fprintf(<span class="string">'Gigaflops on GPU: %f\n'</span>, gflopsGPU(i));
    <span class="keyword">end</span>
<span class="keyword">end</span>
</pre><p>We then execute the benchmarks in single and double precision.</p><pre class="codeinput">[cpu, gpu] = executeBenchmarks(<span class="string">'single'</span>, sizeSingle);
results.sizeSingle = sizeSingle;
results.gflopsSingleCPU = cpu;
results.gflopsSingleGPU = gpu;
[cpu, gpu] = executeBenchmarks(<span class="string">'double'</span>, sizeDouble);
results.sizeDouble = sizeDouble;
results.gflopsDoubleCPU = cpu;
results.gflopsDoubleGPU = gpu;
</pre><pre class="codeoutput">Starting benchmarks with 8 different single-precision matrices of sizes
ranging from 1024-by-1024 to 22528-by-22528.
Creating a matrix of size 1024-by-1024.
Gigaflops on CPU: 37.473921
Gigaflops on GPU: 56.051312
Creating a matrix of size 4096-by-4096.
Gigaflops on CPU: 78.050814
Gigaflops on GPU: 287.105717
Creating a matrix of size 7168-by-7168.
Gigaflops on CPU: 92.768970
Gigaflops on GPU: 387.328295
Creating a matrix of size 10240-by-10240.
Gigaflops on CPU: 98.668753
Gigaflops on GPU: 426.095161
Creating a matrix of size 13312-by-13312.
Gigaflops on CPU: 102.562978
Gigaflops on GPU: 448.525945
Creating a matrix of size 16384-by-16384.
Gigaflops on CPU: 104.779038
Gigaflops on GPU: 457.330334
Creating a matrix of size 19456-by-19456.
Gigaflops on CPU: 107.525991
Gigaflops on GPU: 465.951409
Creating a matrix of size 22528-by-22528.
Gigaflops on CPU: 108.839676
Gigaflops on GPU: 475.310313
Starting benchmarks with 6 different double-precision matrices of sizes
ranging from 1024-by-1024 to 16384-by-16384.
Creating a matrix of size 1024-by-1024.
Gigaflops on CPU: 22.012235
Gigaflops on GPU: 44.004217
Creating a matrix of size 4096-by-4096.
Gigaflops on CPU: 41.571124
Gigaflops on GPU: 178.185922
Creating a matrix of size 7168-by-7168.
Gigaflops on CPU: 47.788035
Gigaflops on GPU: 225.745820
Creating a matrix of size 10240-by-10240.
Gigaflops on CPU: 50.930527
Gigaflops on GPU: 244.189190
Creating a matrix of size 13312-by-13312.
Gigaflops on CPU: 52.887904
Gigaflops on GPU: 254.218183
Creating a matrix of size 16384-by-16384.
Gigaflops on CPU: 53.667451
Gigaflops on GPU: 260.272952
</pre><h2>Plotting the Performance<a name="8"></a></h2><p>We can now plot the results, and compare the performance on the CPU and the GPU, both for single and double precision.</p><p>First, we look at the performance of the backslash operator in single precision.</p><pre class="codeinput">fig = figure;
ax = axes(<span class="string">'parent'</span>, fig);
plot(ax, results.sizeSingle, results.gflopsSingleGPU, <span class="string">'-x'</span>, <span class="keyword">...</span>
     results.sizeSingle, results.gflopsSingleCPU, <span class="string">'-o'</span>)
grid <span class="string">on</span>;
legend(<span class="string">'GPU'</span>, <span class="string">'CPU'</span>, <span class="string">'Location'</span>, <span class="string">'NorthWest'</span>);
title(ax, <span class="string">'Single-precision performance'</span>)
ylabel(ax, <span class="string">'Gigaflops'</span>);
xlabel(ax, <span class="string">'Matrix size'</span>);
drawnow;
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_backslash_01.png" alt=""> <p>Now, we look at the performance of the backslash operator in double precision.</p><pre class="codeinput">fig = figure;
ax = axes(<span class="string">'parent'</span>, fig);
plot(ax, results.sizeDouble, results.gflopsDoubleGPU, <span class="string">'-x'</span>, <span class="keyword">...</span>
     results.sizeDouble, results.gflopsDoubleCPU, <span class="string">'-o'</span>)
legend(<span class="string">'GPU'</span>, <span class="string">'CPU'</span>, <span class="string">'Location'</span>, <span class="string">'NorthWest'</span>);
grid <span class="string">on</span>;
title(ax, <span class="string">'Double-precision performance'</span>)
ylabel(ax, <span class="string">'Gigaflops'</span>);
xlabel(ax, <span class="string">'Matrix size'</span>);
drawnow;
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_backslash_02.png" alt=""> <p>Finally, we look at the speedup of the backslash operator when comparing the GPU to the CPU.</p><pre class="codeinput">speedupDouble = results.gflopsDoubleGPU./results.gflopsDoubleCPU;
speedupSingle = results.gflopsSingleGPU./results.gflopsSingleCPU;
fig = figure;
ax = axes(<span class="string">'parent'</span>, fig);
plot(ax, results.sizeSingle, speedupSingle, <span class="string">'-v'</span>, <span class="keyword">...</span>
     results.sizeDouble, speedupDouble, <span class="string">'-*'</span>)
grid <span class="string">on</span>;
legend(<span class="string">'Single-precision'</span>, <span class="string">'Double-precision'</span>, <span class="string">'Location'</span>, <span class="string">'SouthEast'</span>);
title(ax, <span class="string">'Speedup of computations on GPU compared to CPU'</span>);
ylabel(ax, <span class="string">'Speedup'</span>);
xlabel(ax, <span class="string">'Matrix size'</span>);
drawnow;
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_backslash_03.png" alt=""> <pre class="codeinput"><span class="keyword">end</span>
</pre><pre class="codeoutput">
ans = 

         sizeSingle: [1024 4096 7168 10240 13312 16384 19456 22528]
    gflopsSingleCPU: [1x8 double]
    gflopsSingleGPU: [1x8 double]
         sizeDouble: [1024 4096 7168 10240 13312 16384]
    gflopsDoubleCPU: [1x6 double]
    gflopsDoubleGPU: [1x6 double]

</pre><p class="footer">Copyright 2010-2012 The MathWorks, Inc.<br>
          Published with MATLAB&reg; 8.0<br><br>
		  MATLAB and Simulink are registered trademarks of The MathWorks, Inc.  Please see <a href="http://www.mathworks.com/trademarks">www.mathworks.com/trademarks</a> for a list of other trademarks owned by The MathWorks, Inc.  Other product or brand names are trademarks or registered trademarks of their respective owners.
      </p></div><!--
##### SOURCE BEGIN #####
%% Benchmarking A\b on the GPU
% This example looks at how we can benchmark the solving of a linear system
% on the GPU.  The MATLAB(R) code to solve for |x| in |A*x = b| is very
% simple.  Most frequently, we use matrix left division, also known as
% |mldivide| or the backslash operator (\), to calculate |x| (that is, 
% |x = A\b|).
%
% Related examples:
%
% * <paralleldemo_backslash_bench.html Benchmarking A\b> using 
% distributed arrays.

% Copyright 2010-2012 The MathWorks, Inc.

%%
% The code shown in this example can be found in this function:
function results = paralleldemo_gpu_backslash(maxMemory)
%%
% It is important to choose the appropriate matrix size for the
% computations.  We can do this by specifying the amount of system memory
% in GB available to the CPU and the GPU.  The default value is based only
% on the amount of memory available on the GPU, and you can specify a value
% that is appropriate for your system.
if nargin == 0
    g = gpuDevice; 
    maxMemory = 0.4*g.FreeMemory/1024^3;
end

%% The Benchmarking Function
% We want to benchmark matrix left division (\), and not the cost of
% transferring data between the CPU and GPU, the time it takes to create a
% matrix, or other parameters.  We therefore separate the data generation
% from the solving of the linear system, and measure only the time it
% takes to do the latter.
function [A, b] = getData(n, clz)
    fprintf('Creating a matrix of size %d-by-%d.\n', n, n);
    A = rand(n, n, clz) + 100*eye(n, n, clz);
    b = rand(n, 1, clz);
end

function time = timeSolve(A, b)
    tic;
    x = A\b; %#ok<NASGU> We don't need the value of x.
    time = toc;
end

%% Choosing Problem Size
% As with a great number of other parallel algorithms, the performance of
% solving a linear system in parallel depends greatly on the matrix size.
% As seen in other examples, such as <paralleldemo_backslash_bench.html
% Benchmarking A\b>, we compare the performance of the algorithm for
% different matrix sizes.

% Declare the matrix sizes to be a multiple of 1024.
maxSizeSingle = floor(sqrt(maxMemory*1024^3/4));
maxSizeDouble = floor(sqrt(maxMemory*1024^3/8));
step = 1024;
if maxSizeDouble/step >= 10
    step = step*floor(maxSizeDouble/(5*step));
end
sizeSingle = 1024:step:maxSizeSingle;
sizeDouble = 1024:step:maxSizeDouble;


%% Comparing Performance: Gigaflops 
% We use the number of floating point operations per second as our measure
% of performance because that allows us to compare the performance of the
% algorithm for different matrix sizes. 
%
% Given a matrix size, the benchmarking function creates the matrix |A| and
% the right-hand side |b| once, and then solves |A\b| a few times to get
% an accurate measure of the time it takes.  We use the floating point
% operations count of the HPC Challenge, so that for an n-by-n matrix, we
% count the floating point operations as |2/3*n^3 + 3/2*n^2|.
function gflops = benchFcn(A, b)
    numReps = 3;
    time = inf;
    % We solve the linear system a few times and calculate the Gigaflops 
    % based on the best time.
    for itr = 1:numReps
        tcurr = timeSolve(A, b);
        time = min(tcurr, time);
    end
    n = size(A, 1);
    flop = 2/3*n^3 + 3/2*n^2;
    gflops = flop/time/1e9;
end

%% Executing the Benchmarks
% Having done all the setup, it is straightforward to execute the
% benchmarks.  However, the computations can take a long time to complete,
% so we print some intermediate status information as we complete the
% benchmarking for each matrix size.  We also encapsulate the loop over
% all the matrix sizes in a function, to benchmark both single- and 
% double-precision computations.
function [gflopsCPU, gflopsGPU] = executeBenchmarks(clz, sizes)
    fprintf(['Starting benchmarks with %d different %s-precision ' ...
         'matrices of sizes\nranging from %d-by-%d to %d-by-%d.\n'], ...
            length(sizes), clz, sizes(1), sizes(1), sizes(end), ...
            sizes(end));
    gflopsGPU = zeros(size(sizes));
    gflopsCPU = zeros(size(sizes));
    for i = 1:length(sizes)
        n = sizes(i);
        [A, b] = getData(n, clz);
        gflopsCPU(i) = benchFcn(A, b);
        fprintf('Gigaflops on CPU: %f\n', gflopsCPU(i));
        A = gpuArray(A);
        b = gpuArray(b);
        gflopsGPU(i) = benchFcn(A, b);
        fprintf('Gigaflops on GPU: %f\n', gflopsGPU(i));
    end
end

%%
% We then execute the benchmarks in single and double precision.
[cpu, gpu] = executeBenchmarks('single', sizeSingle);
results.sizeSingle = sizeSingle;
results.gflopsSingleCPU = cpu;
results.gflopsSingleGPU = gpu;
[cpu, gpu] = executeBenchmarks('double', sizeDouble);
results.sizeDouble = sizeDouble;
results.gflopsDoubleCPU = cpu;
results.gflopsDoubleGPU = gpu;

%% Plotting the Performance
% We can now plot the results, and compare the performance on the CPU and
% the GPU, both for single and double precision.

%%
% First, we look at the performance of the backslash operator in single
% precision.
fig = figure;
ax = axes('parent', fig);
plot(ax, results.sizeSingle, results.gflopsSingleGPU, '-x', ...
     results.sizeSingle, results.gflopsSingleCPU, '-o')
grid on;
legend('GPU', 'CPU', 'Location', 'NorthWest');
title(ax, 'Single-precision performance')
ylabel(ax, 'Gigaflops');
xlabel(ax, 'Matrix size');
drawnow;

%%
% Now, we look at the performance of the backslash operator in double
% precision.
fig = figure;
ax = axes('parent', fig);
plot(ax, results.sizeDouble, results.gflopsDoubleGPU, '-x', ...
     results.sizeDouble, results.gflopsDoubleCPU, '-o')
legend('GPU', 'CPU', 'Location', 'NorthWest');
grid on;
title(ax, 'Double-precision performance')
ylabel(ax, 'Gigaflops');
xlabel(ax, 'Matrix size');
drawnow;

%%
% Finally, we look at the speedup of the backslash operator when comparing
% the GPU to the CPU.
speedupDouble = results.gflopsDoubleGPU./results.gflopsDoubleCPU;
speedupSingle = results.gflopsSingleGPU./results.gflopsSingleCPU;
fig = figure;
ax = axes('parent', fig);
plot(ax, results.sizeSingle, speedupSingle, '-v', ...
     results.sizeDouble, speedupDouble, '-*')
grid on;
legend('Single-precision', 'Double-precision', 'Location', 'SouthEast');
title(ax, 'Speedup of computations on GPU compared to CPU');
ylabel(ax, 'Speedup');
xlabel(ax, 'Matrix size');
drawnow;

displayEndOfDemoMessage(mfilename)
end

##### SOURCE END #####
--></body></html>