
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <!--
This HTML was auto-generated from MATLAB code.
To make changes, update the MATLAB code and republish this document.
      --><title>Distributed Analysis of the Origin of the Human Immunodeficiency Virus</title><meta name="generator" content="MATLAB 7.14"><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"><meta name="DC.date" content="2011-10-21"><meta name="DC.source" content="paralleldemo_hiv_dist.m"><link rel="stylesheet" type="text/css" href="../../../matlab/helptools/private/style.css"></head><body><div class="header"><div class="left"><a href="matlab:edit paralleldemo_hiv_dist">Open paralleldemo_hiv_dist.m in the Editor</a></div><div class="right"><a href="matlab:echodemo paralleldemo_hiv_dist">Run in the Command Window</a></div></div><div class="content"><h1>Distributed Analysis of the Origin of the Human Immunodeficiency Virus</h1><!--introduction--><p>This example shows how the Parallel Computing Toolbox&#8482; can be used to perform
pairwise sequence alignment (PWSA). PWSA has multiple
applications in bioinformatics, such as multiple sequence analysis and
phylogenetic tree reconstruction. We look at a PWSA that uses a global dynamic
programming algorithm to align each pair of sequences, and we then calculate
the pairwise distances using the Tajima-Nei metric.  This gives us a matrix
of distances between sequences that we use for inferring the phylogeny of HIV
and SIV viruses. PWSA is a computationally expensive task with complexity
O(L*L*N*N), where L is the average length of the sequences and N is the number
of sequences [Durbin, et al. Cambridge University Press, 1998].</p><p>For details about the computations,
<a href="matlab:edit('pctdemo_setup_hiv.m')">view the code for pctdemo_setup_hiv</a>.</p><p>Prerequisites:</p><div><ul><li><a href="paralleltutorial_defaults.html">Customizing the Settings for the Examples in the Parallel Computing Toolbox</a></li><li><a href="paralleltutorial_dividing_tasks.html">Dividing MATLAB&reg; Computations into Tasks</a></li></ul></div><p>Related examples:</p><div><ul><li><a href="paralleldemo_hiv_seq.html">Sequential Analysis of the Origin of the Human Immunodeficiency Virus</a></li></ul></div><!--/introduction--><h2>Contents</h2><div><ul><li><a href="#1">Analyze the Sequential Problem</a></li><li><a href="#2">Load the Example Settings and the Data</a></li><li><a href="#4">Divide the Work into Smaller Tasks</a></li><li><a href="#7">Create and Submit the Job</a></li><li><a href="#9">Retrieve the Results</a></li><li><a href="#11">Measure the Elapsed Time</a></li><li><a href="#12">Plot the Results</a></li></ul></div><h2>Analyze the Sequential Problem<a name="1"></a></h2><p>First, we look at how the computations in the sequential example fit into the
model introduced in the <a href="paralleltutorial_dividing_tasks.html">Dividing MATLAB
Computations into Tasks</a> example.  The call to <tt>seqpdist</tt> is the most
computationally intensive part of the sequential example, and it basically
performs the following parameter sweep:</p><pre>  for all pairs (s1, s2) formed from the elements of seq
     distance(s1, s2) = seqpdist(s1, s2);
  end</pre><p>The distance is, of course, symmetric, i.e.,  <tt>seqpdist(s1, s2) = seqpdist(s2,
s1)</tt>.  Because <tt>seqpdist</tt> re-normalizes the distances based on the entire input
sequence <tt>seq</tt>, we have to calculate that renormalization factor ourselves in
this example, and we also need to identify the pairs between which to measure
the distance.</p><p>Since we calculate a rather large number of distances, we have each task
calculate several distances.  This requires us to write a simple wrapper task
function that invokes <tt>seqpdist</tt>.</p><h2>Load the Example Settings and the Data<a name="2"></a></h2><p>The example uses the default profile when identifying the cluster to use.
The
<a href="matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp_ug.map'),'profiles_help')">profiles documentation</a>
explains how to create new profiles and how to change the default
profile.  See
<a href="paralleltutorial_defaults.html">Customizing the Settings for the Examples in the Parallel Computing Toolbox</a>
for instructions on how to change the example difficulty level or the number of
tasks created.</p><pre class="codeinput">[difficulty, myCluster, numTasks] = pctdemo_helper_getDefaults();
</pre><p>The <tt>pctdemo_setup_hiv</tt> function retrieves the protein sequence information
from the NCBI GenBank&reg; database, and the <tt>difficulty</tt> parameter controls how
many protein sequences we retrieve.
You can
<a href="matlab:edit('pctdemo_setup_hiv.m')">view the code for pctdemo_setup_hiv</a>
for full details.</p><pre class="codeinput">[fig, pol, description] = pctdemo_setup_hiv(difficulty);
numViruses = length(pol);
startTime = clock;
</pre><pre class="codeoutput">Downloading data from the NCBI GenBank database
Finished downloading
</pre><h2>Divide the Work into Smaller Tasks<a name="4"></a></h2><p>We use the Tajima-Nei method to measure the distance between
the POL coding regions.
Tajima-Nei distances are based on the frequency of nucleotides in
the whole group of sequences.  When you call <tt>seqpdist</tt>
for only two sequences, the function would compute the
nucleotide count based on only the two sequences and not on the
whole group.  Consequently, we calculate the frequency based on
the whole group and pass that information to <tt>seqpdist</tt>.</p><pre class="codeinput">bc = basecount(strcat(pol.Sequence));
bf = [bc.A bc.C bc.G bc.T]/(bc.A + bc.C + bc.G + bc.T);
</pre><p>Let's find the parameter space that we want to traverse.  The <tt>seqpdist</tt>
documentation gives us useful information in this regard:</p><pre class="language-matlab">D = SEQPDIST(SEQS) returns <span class="string">a</span> <span class="string">vector</span> <span class="string">D</span> <span class="string">containing</span> <span class="string">biological</span> <span class="string">distances</span>
between <span class="string">each</span> <span class="string">pair</span> <span class="string">of</span> <span class="string">sequences</span> <span class="string">stored</span> <span class="string">in</span> <span class="string">the</span> <span class="string">M</span> <span class="string">elements</span> <span class="string">of</span> <span class="string">the</span> <span class="string">cell</span>
SEQS. D is <span class="string">an</span> <span class="string">(M*(M-1)/2)-by-1</span> <span class="string">vector</span>, corresponding <span class="string">to</span> <span class="string">the</span> <span class="string">M*(M-1)/2</span>
pairs <span class="string">of</span> <span class="string">sequences</span> <span class="string">in</span> <span class="string">SEQS.</span> <span class="string">The</span> <span class="string">output</span> <span class="string">D</span> <span class="string">is</span> <span class="string">arranged</span> <span class="string">in</span> <span class="string">the</span> <span class="string">order</span> <span class="string">of</span>
((2,1),(3,1),<span class="keyword">...</span><span class="comment">, (M,1),(3,2),...(M,2),.....(M,M-1), i.e., the lower</span>
left triangle of the full M-by-M distance matrix.
</pre><p>Based on this information, we create two vectors, <tt>Aseq</tt> and <tt>Bseq</tt>, that
contain the sequence pairs between which to calculate the distances.</p><pre class="codeinput">Aseq = struct;
Bseq = struct;
ind = 1;
<span class="keyword">for</span> j = 1:numViruses
    <span class="keyword">for</span> i = j+1:numViruses
       Aseq(ind).Sequence = pol(j).Sequence;
       Bseq(ind).Sequence = pol(i).Sequence;
       ind = ind + 1;
    <span class="keyword">end</span>
<span class="keyword">end</span>
</pre><p>We want to divide the vectors <tt>Asplit</tt> and <tt>Bsplit</tt> between the tasks.</p><pre class="codeinput">[Asplit, numTasks] = pctdemo_helper_split_vector(Aseq, numTasks);
Bsplit = pctdemo_helper_split_vector(Bseq, numTasks);
fprintf([<span class="string">'This example will submit a job with %d task(s) '</span> <span class="keyword">...</span>
         <span class="string">'to the cluster.\n'</span>], numTasks);
</pre><pre class="codeoutput">This example will submit a job with 4 task(s) to the cluster.
</pre><h2>Create and Submit the Job<a name="7"></a></h2><p>We create a job and the tasks in the job.  Task <tt>i</tt> calculates
the pairwise distances between the elements in <tt>Asplit{i}</tt> and <tt>Bsplit{i}</tt>.
You can
<a href="matlab:edit('pctdemo_task_hiv.m')">view the code for pctdemo_task_hiv</a>
for full details.</p><pre class="codeinput">job = createJob(myCluster);
<span class="keyword">for</span> i = 1:numTasks
   createTask(job, @pctdemo_task_hiv, 1, {Asplit{i}, Bsplit{i}, bf});
<span class="keyword">end</span>
</pre><p>We can now submit the job and wait for it to finish.</p><pre class="codeinput">submit(job);
wait(job);
</pre><h2>Retrieve the Results<a name="9"></a></h2><p>When we have obtained and verified all the results, we allow the cluster
to free its resources.  <tt>fetchOutputs</tt> will throw an error if the tasks
did not complete successfully, in which case we need to delete the job
before throwing the error.</p><pre class="codeinput"><span class="keyword">try</span>
    jobResults = fetchOutputs(job);
<span class="keyword">catch</span> err
    delete(job);
    rethrow(err);
<span class="keyword">end</span>

pold = cell2mat(jobResults(:, 1))';
</pre><p>We have now finished all the verifications, so we can delete the job.</p><pre class="codeinput">delete(job);
</pre><h2>Measure the Elapsed Time<a name="11"></a></h2><p>The time used for the distributed computations should be compared
against the time it takes to perform the same set of calculations
in the
<a href="paralleldemo_hiv_seq.html">Sequential Analysis of the Origin of the Human
Immunodeficiency Virus</a> example.
The elapsed time varies with the underlying hardware and network infrastructure.</p><pre class="codeinput">elapsedTime = etime(clock, startTime);
fprintf(<span class="string">'Elapsed time is %2.1f seconds\n'</span>, elapsedTime);
</pre><pre class="codeoutput">Elapsed time is 5.7 seconds
</pre><h2>Plot the Results<a name="12"></a></h2><p>Now that we have all the distances, we can construct the phylogenetic trees
for the POL proteins.  You can
<a href="matlab:edit('pctdemo_plot_hiv.m')">view the code for pctdemo_plot_hiv</a>
for full details.</p><pre class="codeinput">pctdemo_plot_hiv(fig, pold, description);
</pre><img vspace="5" hspace="5" src="paralleldemo_hiv_dist_01.png" alt=""> <p class="footer">Copyright 2007-2012 The MathWorks, Inc.<br>
          Published with MATLAB&reg; 7.14</p><p class="footer" id="trademarks">MATLAB and Simulink are registered trademarks of The MathWorks, Inc.  Please see <a href="http://www.mathworks.com/trademarks">www.mathworks.com/trademarks</a> for a list of other trademarks owned by The MathWorks, Inc.  Other product or brand names are trademarks or registered trademarks of their respective owners.</p></div><!--
##### SOURCE BEGIN #####
%% Distributed Analysis of the Origin of the Human Immunodeficiency Virus
% This example shows how the Parallel Computing Toolbox(TM) can be used to perform
% pairwise sequence alignment (PWSA). PWSA has multiple
% applications in bioinformatics, such as multiple sequence analysis and
% phylogenetic tree reconstruction. We look at a PWSA that uses a global dynamic
% programming algorithm to align each pair of sequences, and we then calculate
% the pairwise distances using the Tajima-Nei metric.  This gives us a matrix
% of distances between sequences that we use for inferring the phylogeny of HIV
% and SIV viruses. PWSA is a computationally expensive task with complexity
% O(L*L*N*N), where L is the average length of the sequences and N is the number
% of sequences [Durbin, et al. Cambridge University Press, 1998].
%
% For details about the computations,  
% <matlab:edit('pctdemo_setup_hiv.m') view the code for pctdemo_setup_hiv>.
%
% Prerequisites:
% 
% * <paralleltutorial_defaults.html
% Customizing the Settings for the Examples in the Parallel Computing Toolbox>
% * <paralleltutorial_dividing_tasks.html 
% Dividing MATLAB(R) Computations into Tasks>
%
% Related examples:
% 
% * <paralleldemo_hiv_seq.html 
% Sequential Analysis of the Origin of the Human Immunodeficiency Virus>

%   Copyright 2007-2012 The MathWorks, Inc.

%% Analyze the Sequential Problem
% First, we look at how the computations in the sequential example fit into the
% model introduced in the <paralleltutorial_dividing_tasks.html Dividing MATLAB
% Computations into Tasks> example.  The call to |seqpdist| is the most
% computationally intensive part of the sequential example, and it basically
% performs the following parameter sweep:
%
%    for all pairs (s1, s2) formed from the elements of seq
%       distance(s1, s2) = seqpdist(s1, s2);
%    end
%
% The distance is, of course, symmetric, i.e.,  |seqpdist(s1, s2) = seqpdist(s2,
% s1)|.  Because |seqpdist| re-normalizes the distances based on the entire input
% sequence |seq|, we have to calculate that renormalization factor ourselves in 
% this example, and we also need to identify the pairs between which to measure  
% the distance.
%
% Since we calculate a rather large number of distances, we have each task
% calculate several distances.  This requires us to write a simple wrapper task
% function that invokes |seqpdist|.

%% Load the Example Settings and the Data
% The example uses the default profile when identifying the cluster to use.
% The
% <matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp_ug.map'),'profiles_help')
% profiles documentation> 
% explains how to create new profiles and how to change the default 
% profile.  See 
% <paralleltutorial_defaults.html
% Customizing the Settings for the Examples in the Parallel Computing Toolbox> 
% for instructions on how to change the example difficulty level or the number of
% tasks created.
[difficulty, myCluster, numTasks] = pctdemo_helper_getDefaults();

%%
% The |pctdemo_setup_hiv| function retrieves the protein sequence information
% from the NCBI GenBank(R) database, and the |difficulty| parameter controls how
% many protein sequences we retrieve.
% You can 
% <matlab:edit('pctdemo_setup_hiv.m') view the code for pctdemo_setup_hiv> 
% for full details.
[fig, pol, description] = pctdemo_setup_hiv(difficulty);
numViruses = length(pol);
startTime = clock;

%% Divide the Work into Smaller Tasks
% We use the Tajima-Nei method to measure the distance between
% the POL coding regions. 
% Tajima-Nei distances are based on the frequency of nucleotides in
% the whole group of sequences.  When you call |seqpdist|
% for only two sequences, the function would compute the
% nucleotide count based on only the two sequences and not on the
% whole group.  Consequently, we calculate the frequency based on
% the whole group and pass that information to |seqpdist|.
bc = basecount(strcat(pol.Sequence));
bf = [bc.A bc.C bc.G bc.T]/(bc.A + bc.C + bc.G + bc.T);

%%
% Let's find the parameter space that we want to traverse.  The |seqpdist| 
% documentation gives us useful information in this regard:
%
%   D = SEQPDIST(SEQS) returns a vector D containing biological distances
%   between each pair of sequences stored in the M elements of the cell
%   SEQS. D is an (M*(M-1)/2)-by-1 vector, corresponding to the M*(M-1)/2
%   pairs of sequences in SEQS. The output D is arranged in the order of
%   ((2,1),(3,1),..., (M,1),(3,2),...(M,2),.....(M,M-1), i.e., the lower
%   left triangle of the full M-by-M distance matrix.  
% 
% Based on this information, we create two vectors, |Aseq| and |Bseq|, that 
% contain the sequence pairs between which to calculate the distances. 
Aseq = struct;
Bseq = struct;
ind = 1;
for j = 1:numViruses
    for i = j+1:numViruses
       Aseq(ind).Sequence = pol(j).Sequence;
       Bseq(ind).Sequence = pol(i).Sequence;
       ind = ind + 1;
    end
end

%%
% We want to divide the vectors |Asplit| and |Bsplit| between the tasks.
[Asplit, numTasks] = pctdemo_helper_split_vector(Aseq, numTasks);
Bsplit = pctdemo_helper_split_vector(Bseq, numTasks);
fprintf(['This example will submit a job with %d task(s) ' ...
         'to the cluster.\n'], numTasks);

%% Create and Submit the Job
% We create a job and the tasks in the job.  Task |i| calculates 
% the pairwise distances between the elements in |Asplit{i}| and |Bsplit{i}|.
% You can 
% <matlab:edit('pctdemo_task_hiv.m') view the code for pctdemo_task_hiv> 
% for full details.
job = createJob(myCluster);
for i = 1:numTasks
   createTask(job, @pctdemo_task_hiv, 1, {Asplit{i}, Bsplit{i}, bf});
end
%%
% We can now submit the job and wait for it to finish.
submit(job);
wait(job);

%% Retrieve the Results
% When we have obtained and verified all the results, we allow the cluster
% to free its resources.  |fetchOutputs| will throw an error if the tasks
% did not complete successfully, in which case we need to delete the job
% before throwing the error.
try
    jobResults = fetchOutputs(job);
catch err
    delete(job);
    rethrow(err);
end

pold = cell2mat(jobResults(:, 1))';

%%
% We have now finished all the verifications, so we can delete the job.
delete(job);

%% Measure the Elapsed Time
% The time used for the distributed computations should be compared
% against the time it takes to perform the same set of calculations
% in the 
% <paralleldemo_hiv_seq.html Sequential Analysis of the Origin of the Human 
% Immunodeficiency Virus> example.
% The elapsed time varies with the underlying hardware and network infrastructure.
elapsedTime = etime(clock, startTime);
fprintf('Elapsed time is %2.1f seconds\n', elapsedTime);

%% Plot the Results
% Now that we have all the distances, we can construct the phylogenetic trees
% for the POL proteins.  You can 
% <matlab:edit('pctdemo_plot_hiv.m') view the code for pctdemo_plot_hiv> 
% for full details.
pctdemo_plot_hiv(fig, pold, description);


displayEndOfDemoMessage(mfilename)

##### SOURCE END #####
--></body></html>
