
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <!--
This HTML was auto-generated from MATLAB code.
To make changes, update the MATLAB code and republish this document.
      --><title>Distributed Radar Tracking Simulation</title><meta name="generator" content="MATLAB 7.14"><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"><meta name="DC.date" content="2011-10-21"><meta name="DC.source" content="paralleldemo_radar_dist.m"><link rel="stylesheet" type="text/css" href="../../../matlab/helptools/private/style.css"></head><body><div class="header"><div class="left"><a href="matlab:edit paralleldemo_radar_dist">Open paralleldemo_radar_dist.m in the Editor</a></div><div class="right"><a href="matlab:echodemo paralleldemo_radar_dist">Run in the Command Window</a></div></div><div class="content"><h1>Distributed Radar Tracking Simulation</h1><!--introduction--><p>This example uses the Parallel Computing Toolbox&#8482; to perform a Monte Carlo
simulation of a radar station that tracks the path of an aircraft.
The radar station uses the radar equation to estimate the aircraft position.
We introduce measurement errors as a random variable, and the radar station
performs Kalman filtering to try to correct for them.
To estimate the effectiveness of the Kalman filter, we perform
repeated simulations, each time having the aircraft travel along a randomly
chosen path.</p><p>For details about the computations,
<a href="matlab:open('pctdemo_model_radar')">open the pctdemo_model_radar model</a>.</p><p>Prerequisites:</p><div><ul><li><a href="paralleltutorial_defaults.html">Customizing the Settings for the Examples in the Parallel Computing Toolbox</a></li><li><a href="paralleltutorial_dividing_tasks.html">Dividing MATLAB&reg; Computations into Tasks</a></li></ul></div><p>Related examples:</p><div><ul><li><a href="paralleldemo_radar_seq.html">Sequential Radar Tracking Simulation</a></li></ul></div><!--/introduction--><h2>Contents</h2><div><ul><li><a href="#1">Analyze the Sequential Problem</a></li><li><a href="#2">Load the Example Settings and the Data</a></li><li><a href="#4">Divide the Work into Smaller Tasks</a></li><li><a href="#5">Create and Submit the Job</a></li><li><a href="#7">Retrieve the Results</a></li><li><a href="#10">Measure the Elapsed Time</a></li><li><a href="#11">Plot the Results</a></li></ul></div><h2>Analyze the Sequential Problem<a name="1"></a></h2><p>First, we look at how the computations in the sequential example fit into the
model introduced in the <a href="paralleltutorial_dividing_tasks.html">Dividing MATLAB
Computations into Tasks</a> example.  The main computations consist of a large
number of simulations, and each simulation takes only a fraction of a second.
We therefore have each task perform many simulations.  Because the function
<tt>pctdemo_task_radar</tt> can already perform many simulations in a single
function call, we can use it directly as our task function.</p><h2>Load the Example Settings and the Data<a name="2"></a></h2><p>The example uses the default profile when identifying the cluster to use.
The
<a href="matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp_ug.map'),'profiles_help')">profiles documentation</a>
explains how to create new profiles and how to change the default
profile.  See
<a href="paralleltutorial_defaults.html">Customizing the Settings for the Examples in the Parallel Computing Toolbox</a>
for instructions on how to change the example difficulty level or the number of
tasks created.</p><pre class="codeinput">[difficulty, myCluster, numTasks] = pctdemo_helper_getDefaults();
</pre><p>We define the number of simulations and the length of each simulation in
<tt>pctdemo_setup_radar</tt>.  The example difficulty level controls the number of
simulations we perform.  The function <tt>pctdemo_setup_radar</tt> also shows
examples of the different paths that the aircraft can take, as well as the
error in the estimated aircraft location.
You can
<a href="matlab:edit('pctdemo_setup_radar.m')">view the code for pctdemo_setup_radar</a>
for full details.</p><pre class="codeinput">[fig, numSims, finishTime] = pctdemo_setup_radar(difficulty);
startClock = clock;
</pre><img vspace="5" hspace="5" src="paralleldemo_radar_dist_01.png" alt=""> <h2>Divide the Work into Smaller Tasks<a name="4"></a></h2><p>The computationally intensive part of this example consists of a
Monte Carlo simulation and we use the function <tt>pctdemo_helper_split_scalar</tt>
to divide the <tt>numSims</tt> simulations among the <tt>numTasks</tt> tasks.</p><pre class="codeinput">[taskSims, numTasks] = pctdemo_helper_split_scalar(numSims, numTasks);
fprintf([<span class="string">'This example will submit a job with %d task(s) '</span> <span class="keyword">...</span>
         <span class="string">'to the cluster.\n'</span>], numTasks);
</pre><pre class="codeoutput">This example will submit a job with 4 task(s) to the cluster.
</pre><h2>Create and Submit the Job<a name="5"></a></h2><p>Let us create the simulation job and the tasks in the job.  We
let task <tt>i</tt> perform <tt>taskSims(i)</tt> simulations. Notice that the task function
is the same function that you used in the sequential example.
You can
<a href="matlab:edit('pctdemo_task_radar.m')">view the code for pctdemo_task_radar</a>
for full details.</p><pre class="codeinput">job = createJob(myCluster);
<span class="keyword">for</span> i = 1:numTasks
    createTask(job, @pctdemo_task_radar, 1, {taskSims(i), finishTime});
<span class="keyword">end</span>
</pre><p>We can now submit the job and wait for it to finish.</p><pre class="codeinput">submit(job);
wait(job);
</pre><h2>Retrieve the Results<a name="7"></a></h2><p>Let us obtain the job results, verify that all the tasks finished
successfully, and then delete the job.  <tt>fetchOutputs</tt> will throw an
error if the tasks did not complete successfully, in which case we need
to delete the job before throwing the error.</p><pre class="codeinput"><span class="keyword">try</span>
    jobResults = fetchOutputs(job);
<span class="keyword">catch</span> err
    delete(job);
    rethrow(err);
<span class="keyword">end</span>
</pre><p>Let us format the results.  Notice how we concatenate all the arrays in
<tt>jobResults</tt> along the columns, thus obtaining a matrix of the size
(finishTime + 1)-by-numSims.</p><pre class="codeinput">residual = cat(2, jobResults{:});
</pre><p>We have now finished all the verifications, so we can delete the job.</p><pre class="codeinput">delete(job);
</pre><h2>Measure the Elapsed Time<a name="10"></a></h2><p>The time used for the distributed computations should be compared
against the time it takes to perform the same set of calculations
in the <a href="paralleldemo_radar_seq.html">Sequential Radar Tracking Simulation</a> example.
The elapsed time varies with the underlying hardware and network infrastructure.</p><pre class="codeinput">elapsedTime = etime(clock, startClock);
fprintf(<span class="string">'Elapsed time is %2.1f seconds\n'</span>, elapsedTime);
</pre><pre class="codeoutput">Elapsed time is 31.1 seconds
</pre><h2>Plot the Results<a name="11"></a></h2><p>We use the simulation results to calculate the standard deviation of the
range estimation error as a function of time.  You can
<a href="matlab:edit('pctdemo_plot_radar.m')">view the code for pctdemo_plot_radar</a>
for full details.</p><pre class="codeinput">pctdemo_plot_radar(fig, residual);
</pre><img vspace="5" hspace="5" src="paralleldemo_radar_dist_02.png" alt=""> <p class="footer">Copyright 2007-2012 The MathWorks, Inc.<br>
          Published with MATLAB&reg; 7.14</p><p class="footer" id="trademarks">MATLAB and Simulink are registered trademarks of The MathWorks, Inc.  Please see <a href="http://www.mathworks.com/trademarks">www.mathworks.com/trademarks</a> for a list of other trademarks owned by The MathWorks, Inc.  Other product or brand names are trademarks or registered trademarks of their respective owners.</p></div><!--
##### SOURCE BEGIN #####
%% Distributed Radar Tracking Simulation
% This example uses the Parallel Computing Toolbox(TM) to perform a Monte Carlo
% simulation of a radar station that tracks the path of an aircraft.
% The radar station uses the radar equation to estimate the aircraft position.
% We introduce measurement errors as a random variable, and the radar station 
% performs Kalman filtering to try to correct for them.
% To estimate the effectiveness of the Kalman filter, we perform
% repeated simulations, each time having the aircraft travel along a randomly
% chosen path.
%
% For details about the computations, 
% <matlab:open('pctdemo_model_radar') open the pctdemo_model_radar model>.
% 
% Prerequisites:
% 
% * <paralleltutorial_defaults.html
% Customizing the Settings for the Examples in the Parallel Computing Toolbox> 
% * <paralleltutorial_dividing_tasks.html
% Dividing MATLAB(R) Computations into Tasks>
%
% Related examples:
%
% * <paralleldemo_radar_seq.html Sequential Radar Tracking Simulation>

%   Copyright 2007-2012 The MathWorks, Inc.

%% Analyze the Sequential Problem
% First, we look at how the computations in the sequential example fit into the
% model introduced in the <paralleltutorial_dividing_tasks.html Dividing MATLAB
% Computations into Tasks> example.  The main computations consist of a large
% number of simulations, and each simulation takes only a fraction of a second.
% We therefore have each task perform many simulations.  Because the function
% |pctdemo_task_radar| can already perform many simulations in a single 
% function call, we can use it directly as our task function.

%% Load the Example Settings and the Data
% The example uses the default profile when identifying the cluster to use.
% The
% <matlab:helpview(fullfile(docroot,'toolbox','distcomp','distcomp_ug.map'),'profiles_help')
% profiles documentation> 
% explains how to create new profiles and how to change the default 
% profile.  See 
% <paralleltutorial_defaults.html
% Customizing the Settings for the Examples in the Parallel Computing Toolbox> 
% for instructions on how to change the example difficulty level or the number of
% tasks created.
[difficulty, myCluster, numTasks] = pctdemo_helper_getDefaults();

%%
% We define the number of simulations and the length of each simulation in
% |pctdemo_setup_radar|.  The example difficulty level controls the number of
% simulations we perform.  The function |pctdemo_setup_radar| also shows
% examples of the different paths that the aircraft can take, as well as the
% error in the estimated aircraft location.
% You can 
% <matlab:edit('pctdemo_setup_radar.m') view the code for pctdemo_setup_radar> 
% for full details.
[fig, numSims, finishTime] = pctdemo_setup_radar(difficulty);
startClock = clock;


%% Divide the Work into Smaller Tasks
% The computationally intensive part of this example consists of a
% Monte Carlo simulation and we use the function |pctdemo_helper_split_scalar| 
% to divide the |numSims| simulations among the |numTasks| tasks. 
[taskSims, numTasks] = pctdemo_helper_split_scalar(numSims, numTasks);
fprintf(['This example will submit a job with %d task(s) ' ...
         'to the cluster.\n'], numTasks);

%% Create and Submit the Job
% Let us create the simulation job and the tasks in the job.  We
% let task |i| perform |taskSims(i)| simulations. Notice that the task function 
% is the same function that you used in the sequential example.
% You can 
% <matlab:edit('pctdemo_task_radar.m') view the code for pctdemo_task_radar> 
% for full details.
job = createJob(myCluster);
for i = 1:numTasks
    createTask(job, @pctdemo_task_radar, 1, {taskSims(i), finishTime});
end

%%
% We can now submit the job and wait for it to finish.
submit(job);
wait(job);

%% Retrieve the Results
% Let us obtain the job results, verify that all the tasks finished
% successfully, and then delete the job.  |fetchOutputs| will throw an
% error if the tasks did not complete successfully, in which case we need
% to delete the job before throwing the error.
try
    jobResults = fetchOutputs(job);
catch err
    delete(job);
    rethrow(err);
end

%%
% Let us format the results.  Notice how we concatenate all the arrays in
% |jobResults| along the columns, thus obtaining a matrix of the size
% (finishTime + 1)-by-numSims.
residual = cat(2, jobResults{:});

%%
% We have now finished all the verifications, so we can delete the job.
delete(job); 

%% Measure the Elapsed Time
% The time used for the distributed computations should be compared
% against the time it takes to perform the same set of calculations
% in the <paralleldemo_radar_seq.html
% Sequential Radar Tracking Simulation> example.
% The elapsed time varies with the underlying hardware and network infrastructure.
elapsedTime = etime(clock, startClock);
fprintf('Elapsed time is %2.1f seconds\n', elapsedTime);

%% Plot the Results
% We use the simulation results to calculate the standard deviation of the
% range estimation error as a function of time.  You can 
% <matlab:edit('pctdemo_plot_radar.m') view the code for pctdemo_plot_radar> 
% for full details.
pctdemo_plot_radar(fig, residual);


displayEndOfDemoMessage(mfilename)

##### SOURCE END #####
--></body></html>
