
<!DOCTYPE html
  PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html><head>
      <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
   <!--
This HTML was auto-generated from MATLAB code.
To make changes, update the MATLAB code and republish this document.
      --><title>Exotic Option Pricing on a GPU using a Monte-Carlo Method</title><meta name="generator" content="MATLAB 8.0"><link rel="schema.DC" href="http://purl.org/dc/elements/1.1/"><meta name="DC.date" content="2012-04-27"><meta name="DC.source" content="paralleldemo_gpu_optionpricing.m"><link rel="stylesheet" type="text/css" href="../../../matlab/helptools/private/style.css"></head><body><div class="header"><div class="left"><a href="matlab:edit paralleldemo_gpu_optionpricing">Open paralleldemo_gpu_optionpricing.m in the Editor</a></div><div class="right">&nbsp;</div></div><div class="content"><h1>Exotic Option Pricing on a GPU using a Monte-Carlo Method</h1><!--introduction--><p>This example shows how prices for financial options can be calculated on a GPU using Monte-Carlo methods. Three simple types of exotic option are used as examples, but more complex options can be priced in a similar way.</p><!--/introduction--><h2>Contents</h2><div><ul><li><a href="#4">Stock Price Evolution</a></li><li><a href="#7">Running on the GPU</a></li><li><a href="#9">Pricing an Asian Option</a></li><li><a href="#11">Pricing a Lookback Option</a></li><li><a href="#13">Pricing a Barrier Option</a></li></ul></div><p>This example is a function so that the helpers can be nested inside it.</p><pre class="codeinput"><span class="keyword">function</span> paralleldemo_gpu_optionpricing
</pre><p>This example uses long-running kernels, so cannot run if kernel execution on the GPU can time-out. A time-out is usually only active if the selected GPU is also driving a display.</p><pre class="codeinput">dev = gpuDevice();
<span class="keyword">if</span> dev.KernelExecutionTimeout
    error( <span class="string">'pctexample:gpuoptionpricing:KernelTimeout'</span>, <span class="keyword">...</span>
        [<span class="string">'This example cannot run if kernel execution on the GPU can '</span>, <span class="keyword">...</span>
        <span class="string">'time-out.'</span>] );
<span class="keyword">end</span>
</pre><h2>Stock Price Evolution<a name="4"></a></h2><p>We assume that prices evolve according to a log-normal distribution related to the risk-free interest rate, the dividend yield (if any), and the volatility in the market.  All of these quantities are assumed fixed over the lifetime of the option.  This gives the following stochastic differential equation for the price:</p><p><img src="paralleldemo_gpu_optionpricing_eq10145.png" alt="$${\rm d}S = S \times&#xA;  \left[&#xA;     (r-d){\rm d}t + \sigma \epsilon \sqrt{{\rm d}t}&#xA;  \right]$$"></p><p>where <img src="paralleldemo_gpu_optionpricing_eq68961.png" alt="$S$"> is the stock price, <img src="paralleldemo_gpu_optionpricing_eq25861.png" alt="$r$"> is the risk-free interest rate, <img src="paralleldemo_gpu_optionpricing_eq51528.png" alt="$d$"> is the stock's annual dividend yield, <img src="paralleldemo_gpu_optionpricing_eq24873.png" alt="$\sigma$"> is the volatility of the price and <img src="paralleldemo_gpu_optionpricing_eq97780.png" alt="$\epsilon$"> represents a Gaussian white-noise process.  Assuming that <img src="paralleldemo_gpu_optionpricing_eq97529.png" alt="$(S+\Delta S)/S$"> is log-normally distributed, this can be discretized to:</p><p><img src="paralleldemo_gpu_optionpricing_eq83483.png" alt="$$S_{t+1} = S_{t} \times \exp{&#xA;  \left[&#xA;    \left(r - d - \frac{1}{2}\sigma^2\right)\Delta t&#xA;    + \sigma \epsilon \sqrt{ \Delta t }&#xA;  \right]&#xA;} $$"></p><p>As an example let's use $100 of stock that yields a 1% dividend each year.  The central government interest rate is assumed to be 0.5%.  We examine a two-year time window sampled roughly daily.  The market volatility is assumed to be 20% per annum.</p><pre class="codeinput">stockPrice   = 100;   <span class="comment">% Stock price starts at $100.</span>
dividend     = 0.01;  <span class="comment">% 1% annual dividend yield.</span>
riskFreeRate = 0.005; <span class="comment">% 0.5 percent.</span>
timeToExpiry = 2;     <span class="comment">% Lifetime of the option in years.</span>
sampleRate   = 1/250; <span class="comment">% Assume 250 working days per year.</span>
volatility   = 0.20;  <span class="comment">% 20% volatility.</span>
</pre><p>We reset the random number generators to ensure repeatable results.</p><pre class="codeinput">seed = 1234;
rng( seed );              <span class="comment">% Reset the CPU random number generator.</span>
parallel.gpu.rng( seed ); <span class="comment">% Reset the GPU random number generator.</span>
</pre><p>We can now loop over time to simulate the path of the stock price:</p><pre class="codeinput">price = stockPrice;
time = 0;
hold <span class="string">on</span>;
<span class="keyword">while</span> time &lt; timeToExpiry
    time = time + sampleRate;
    drift = (riskFreeRate - dividend - volatility*volatility/2)*sampleRate;
    perturbation = volatility*sqrt( sampleRate )*randn();
    price = price*exp(drift + perturbation);
    plot( time, price, <span class="string">'.'</span> );
<span class="keyword">end</span>
axis <span class="string">tight</span>;
grid <span class="string">on</span>;
xlabel( <span class="string">'Time (years)'</span> );
ylabel( <span class="string">'Stock price ($)'</span> );
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_optionpricing_01.png" alt=""> <h2>Running on the GPU<a name="7"></a></h2><p>To run stock price simulations on the GPU we first need to put the simulation loop inside a helper function:</p><pre class="codeinput">    <span class="keyword">function</span> finalStockPrice = simulateStockPrice(S,r,d,v,T,dT)
        t = 0;
        <span class="keyword">while</span> t &lt; T
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
        <span class="keyword">end</span>
        finalStockPrice = S;
    <span class="keyword">end</span>
</pre><p>We can then call it thousands of times using <tt>arrayfun</tt>.  To ensure the calculations happen on the GPU we make the input prices a GPU vector with one element per simulation.  To accurately measure the calculation time on the GPU we must use <tt>wait</tt> to ensure that the GPU has finished all operations before calling <tt>toc</tt>.  This is only required for timing purposes.</p><pre class="codeinput"><span class="comment">% Create the input data.</span>
N = 1000000;
startStockPrices = stockPrice*gpuArray.ones(N,1);
timer = tic;

<span class="comment">% Run the simulations.</span>
finalStockPrices = arrayfun( @simulateStockPrice, <span class="keyword">...</span>
    startStockPrices, riskFreeRate, dividend, volatility, <span class="keyword">...</span>
    timeToExpiry, sampleRate );
meanFinalPrice = mean(finalStockPrices);

<span class="comment">% Wait for the GPU to finish and show the results.</span>
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( <span class="string">'Calculated average price of $%1.4f in %1.3f secs.\n'</span>, <span class="keyword">...</span>
    meanFinalPrice, timeTaken );

clf;
hist( finalStockPrices, 100 );
xlabel( <span class="string">'Stock price ($)'</span> )
ylabel( <span class="string">'Frequency'</span> )
grid <span class="string">on</span>;
</pre><pre class="codeoutput">Calculated average price of $98.9563 in 0.771 secs.
</pre><img vspace="5" hspace="5" src="paralleldemo_gpu_optionpricing_02.png" alt=""> <h2>Pricing an Asian Option<a name="9"></a></h2><p>As an example, let's use a European Asian Option based on the arithmetic mean of the price of the stock during the lifetime of the option.  We can calculate the mean price by accumulating the price during the simulation. For a call option, the option is exercised if the average price is above the strike, and the payout is the difference between the two:</p><pre class="codeinput">    <span class="keyword">function</span> optionPrice = asianCallOption(S,r,d,v,x,T,dT)
        t = 0;
        cumulativePrice = 0;
        <span class="keyword">while</span> t &lt; T
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
            cumulativePrice = cumulativePrice + S;
        <span class="keyword">end</span>
        numSteps = (T/dT);
        meanPrice = cumulativePrice / numSteps;
        <span class="comment">% Express the final price in today's money.</span>
        optionPrice = exp(-r*T) * max(0, meanPrice - x);
    <span class="keyword">end</span>
</pre><p>Again we use the GPU to run thousands of simulation paths using <tt>arrayfun</tt>.  Each simulation path gives an independent estimate of the option price, and we therefore take the mean as our result.</p><pre class="codeinput">strike = 95;   <span class="comment">% Strike price for the option ($).</span>
timer = tic;

optionPrices = arrayfun( @asianCallOption, <span class="keyword">...</span>
    startStockPrices, riskFreeRate, dividend, volatility, strike, <span class="keyword">...</span>
    timeToExpiry, sampleRate );
meanOptionPrice = mean(optionPrices);

<span class="comment">% Wait for the GPU to finish and show the results.</span>
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( <span class="string">'Calculated average price of $%1.4f in %1.3f secs.\n'</span>, <span class="keyword">...</span>
    meanOptionPrice, timeTaken );
</pre><pre class="codeoutput">Calculated average price of $8.7247 in 0.778 secs.
</pre><h2>Pricing a Lookback Option<a name="11"></a></h2><p>For this example we use a European-style lookback option whose payout is the difference between the minimum stock price and the final stock price over the lifetime of the option.</p><pre class="codeinput">    <span class="keyword">function</span> optionPrice = euroLookbackCallOption(S,r,d,v,T,dT)
        t = 0;
        minPrice = S;
        <span class="keyword">while</span> t &lt; T
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
            <span class="keyword">if</span> S&lt;minPrice
                minPrice = S;
            <span class="keyword">end</span>
        <span class="keyword">end</span>
        <span class="comment">% Express the final price in today's money.</span>
        optionPrice = exp(-r*T) * max(0, S - minPrice);
    <span class="keyword">end</span>
</pre><p>Note that in this case the strike price for the option is the minimum stock price.  Because the final stock price is always greater than or equal to the minimum, the option is always exercised and is not really "optional".</p><pre class="codeinput">timer = tic;

optionPrices = arrayfun( @euroLookbackCallOption, <span class="keyword">...</span>
    startStockPrices, riskFreeRate, dividend, volatility, <span class="keyword">...</span>
    timeToExpiry, sampleRate );
meanOptionPrice = mean(optionPrices);

<span class="comment">% Wait for the GPU to finish and show the results.</span>
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( <span class="string">'Calculated average price of $%1.4f in %1.3f secs.\n'</span>, <span class="keyword">...</span>
    meanOptionPrice, timeTaken );
</pre><pre class="codeoutput">Calculated average price of $19.2692 in 0.774 secs.
</pre><h2>Pricing a Barrier Option<a name="13"></a></h2><p>This final example uses an "up and out" barrier option which becomes invalid if the stock price ever reaches the barrier level.  If the stock price stays below the barrier level then the final stock price is used in a normal European call option calculation.</p><pre class="codeinput">    <span class="keyword">function</span> optionPrice = upAndOutCallOption(S,r,d,v,x,b,T,dT)
        t = 0;
        <span class="keyword">while</span> (t &lt; T) &amp;&amp; (S &lt; b)
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
        <span class="keyword">end</span>
        <span class="keyword">if</span> S&lt;b
            <span class="comment">% Within barrier, so price as for a European option.</span>
            optionPrice = exp(-r*T) * max(0, S - x);
        <span class="keyword">else</span>
            <span class="comment">% Hit the barrier, so the option is withdrawn.</span>
            optionPrice = 0;
        <span class="keyword">end</span>
    <span class="keyword">end</span>
</pre><p>Note that we must now supply both a strike price for the option and the barrier price at which it becomes invalid:</p><pre class="codeinput">strike  = 95;   <span class="comment">% Strike price for the option ($).</span>
barrier = 150;  <span class="comment">% Barrier price for the option ($).</span>

timer = tic;

optionPrices = arrayfun( @upAndOutCallOption, <span class="keyword">...</span>
    startStockPrices, riskFreeRate, dividend, volatility, <span class="keyword">...</span>
    strike, barrier, <span class="keyword">...</span>
    timeToExpiry, sampleRate );
meanOptionPrice = mean(optionPrices);

<span class="comment">% Wait for the GPU to finish and show the results.</span>
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( <span class="string">'Calculated average price of $%1.4f in %1.3f secs.\n'</span>, <span class="keyword">...</span>
    meanOptionPrice, timeTaken );
</pre><pre class="codeoutput">Calculated average price of $6.8285 in 0.775 secs.
</pre><pre class="codeinput"><span class="keyword">end</span>
</pre><p class="footer">Copyright 2011-2012 The MathWorks, Inc.<br>
          Published with MATLAB&reg; 8.0<br><br>
		  MATLAB and Simulink are registered trademarks of The MathWorks, Inc.  Please see <a href="http://www.mathworks.com/trademarks">www.mathworks.com/trademarks</a> for a list of other trademarks owned by The MathWorks, Inc.  Other product or brand names are trademarks or registered trademarks of their respective owners.
      </p></div><!--
##### SOURCE BEGIN #####
%% Exotic Option Pricing on a GPU using a Monte-Carlo Method
% This example shows how prices for financial options can be calculated on
% a GPU using Monte-Carlo methods. Three simple types of exotic option
% are used as examples, but more complex options can be priced in a similar
% way.

% Copyright 2011-2012 The MathWorks, Inc.

%%
% This example is a function so that the helpers can be nested inside it.
function paralleldemo_gpu_optionpricing

%%
% This example uses long-running kernels, so cannot run if kernel execution on
% the GPU can time-out. A time-out is usually only active if the selected
% GPU is also driving a display.
dev = gpuDevice();
if dev.KernelExecutionTimeout
    error( 'pctexample:gpuoptionpricing:KernelTimeout', ...
        ['This example cannot run if kernel execution on the GPU can ', ...
        'time-out.'] );
end

%% Stock Price Evolution
% We assume that prices evolve according to a log-normal distribution
% related to the risk-free interest rate, the dividend yield (if any), and
% the volatility in the market.  All of these quantities are assumed fixed
% over the lifetime of the option.  This gives the following stochastic
% differential equation for the price:
%
% $${\rm d}S = S \times 
%   \left[
%      (r-d){\rm d}t + \sigma \epsilon \sqrt{{\rm d}t} 
%   \right]$$
%
% where $S$ is the stock price, $r$ is the risk-free interest rate, $d$ is
% the stock's annual dividend yield, $\sigma$ is the volatility of the
% price and $\epsilon$ represents a Gaussian white-noise process.  Assuming
% that $(S+\Delta S)/S$ is log-normally distributed, this can be
% discretized to:
%
% $$S_{t+1} = S_{t} \times \exp{ 
%   \left[ 
%     \left(r - d - \frac{1}{2}\sigma^2\right)\Delta t 
%     + \sigma \epsilon \sqrt{ \Delta t } 
%   \right] 
% } $$
%
% As an example let's use $100 of stock that yields a 1% dividend each
% year.  The central government interest rate is assumed to be 0.5%.  We
% examine a two-year time window sampled roughly daily.  The market
% volatility is assumed to be 20% per annum.
stockPrice   = 100;   % Stock price starts at $100.
dividend     = 0.01;  % 1% annual dividend yield.
riskFreeRate = 0.005; % 0.5 percent.
timeToExpiry = 2;     % Lifetime of the option in years.
sampleRate   = 1/250; % Assume 250 working days per year.
volatility   = 0.20;  % 20% volatility.

%%
% We reset the random number generators to ensure repeatable results.
seed = 1234;
rng( seed );              % Reset the CPU random number generator.
parallel.gpu.rng( seed ); % Reset the GPU random number generator.

%%
% We can now loop over time to simulate the path of the stock price:
price = stockPrice;
time = 0;
hold on;
while time < timeToExpiry
    time = time + sampleRate;
    drift = (riskFreeRate - dividend - volatility*volatility/2)*sampleRate;
    perturbation = volatility*sqrt( sampleRate )*randn();
    price = price*exp(drift + perturbation);
    plot( time, price, '.' );
end
axis tight;
grid on;
xlabel( 'Time (years)' );
ylabel( 'Stock price ($)' );


%% Running on the GPU
% To run stock price simulations on the GPU we first need to put the
% simulation loop inside a helper function:

    function finalStockPrice = simulateStockPrice(S,r,d,v,T,dT)
        t = 0;
        while t < T
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
        end
        finalStockPrice = S;
    end

%%
% We can then call it thousands of times using |arrayfun|.  To ensure the
% calculations happen on the GPU we make the input prices a GPU vector with
% one element per simulation.  To accurately measure the calculation
% time on the GPU we must use |wait| to ensure that the GPU has finished
% all operations before calling |toc|.  This is only required for timing
% purposes.

% Create the input data.
N = 1000000;
startStockPrices = stockPrice*gpuArray.ones(N,1);
timer = tic;

% Run the simulations.
finalStockPrices = arrayfun( @simulateStockPrice, ...
    startStockPrices, riskFreeRate, dividend, volatility, ...
    timeToExpiry, sampleRate );
meanFinalPrice = mean(finalStockPrices);

% Wait for the GPU to finish and show the results.
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( 'Calculated average price of $%1.4f in %1.3f secs.\n', ...
    meanFinalPrice, timeTaken );

clf;
hist( finalStockPrices, 100 );
xlabel( 'Stock price ($)' )
ylabel( 'Frequency' )
grid on;


%% Pricing an Asian Option
% As an example, let's use a European Asian Option based on the arithmetic
% mean of the price of the stock during the lifetime of the option.  We can
% calculate the mean price by accumulating the price during the simulation.
% For a call option, the option is exercised if the average price is above
% the strike, and the payout is the difference between the two:

    function optionPrice = asianCallOption(S,r,d,v,x,T,dT)
        t = 0;
        cumulativePrice = 0;
        while t < T
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
            cumulativePrice = cumulativePrice + S;
        end
        numSteps = (T/dT);
        meanPrice = cumulativePrice / numSteps;
        % Express the final price in today's money.
        optionPrice = exp(-r*T) * max(0, meanPrice - x);
    end

%%
% Again we use the GPU to run thousands of simulation paths using
% |arrayfun|.  Each simulation path gives an independent estimate of the
% option price, and we therefore take the mean as our result.
strike = 95;   % Strike price for the option ($).
timer = tic;

optionPrices = arrayfun( @asianCallOption, ...
    startStockPrices, riskFreeRate, dividend, volatility, strike, ...
    timeToExpiry, sampleRate );
meanOptionPrice = mean(optionPrices);

% Wait for the GPU to finish and show the results.
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( 'Calculated average price of $%1.4f in %1.3f secs.\n', ...
    meanOptionPrice, timeTaken );


%% Pricing a Lookback Option
% For this example we use a European-style lookback option whose payout is
% the difference between the minimum stock price and the final stock price
% over the lifetime of the option.

    function optionPrice = euroLookbackCallOption(S,r,d,v,T,dT)
        t = 0;
        minPrice = S;
        while t < T
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
            if S<minPrice
                minPrice = S;
            end
        end
        % Express the final price in today's money.
        optionPrice = exp(-r*T) * max(0, S - minPrice);
    end

%%
% Note that in this case the strike price for the option is the minimum
% stock price.  Because the final stock price is always greater than or
% equal to the minimum, the option is always exercised and is not really
% "optional".
timer = tic;

optionPrices = arrayfun( @euroLookbackCallOption, ...
    startStockPrices, riskFreeRate, dividend, volatility, ...
    timeToExpiry, sampleRate );
meanOptionPrice = mean(optionPrices);

% Wait for the GPU to finish and show the results.
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( 'Calculated average price of $%1.4f in %1.3f secs.\n', ...
    meanOptionPrice, timeTaken );


%% Pricing a Barrier Option
% This final example uses an "up and out" barrier option which becomes
% invalid if the stock price ever reaches the barrier level.  If the stock
% price stays below the barrier level then the final stock price is used
% in a normal European call option calculation.

    function optionPrice = upAndOutCallOption(S,r,d,v,x,b,T,dT)
        t = 0;
        while (t < T) && (S < b)
            t = t + dT;
            drift = (r - d - v*v/2)*dT;
            perturbation = v*sqrt( dT )*randn();
            S = S*exp(drift + perturbation);
        end
        if S<b
            % Within barrier, so price as for a European option.
            optionPrice = exp(-r*T) * max(0, S - x);
        else
            % Hit the barrier, so the option is withdrawn.
            optionPrice = 0;
        end
    end

%%
% Note that we must now supply both a strike price for the option and the
% barrier price at which it becomes invalid:
strike  = 95;   % Strike price for the option ($).
barrier = 150;  % Barrier price for the option ($).

timer = tic;

optionPrices = arrayfun( @upAndOutCallOption, ...
    startStockPrices, riskFreeRate, dividend, volatility, ...
    strike, barrier, ...
    timeToExpiry, sampleRate );
meanOptionPrice = mean(optionPrices);

% Wait for the GPU to finish and show the results.
wait( gpuDevice );
timeTaken = toc( timer );
fprintf( 'Calculated average price of $%1.4f in %1.3f secs.\n', ...
    meanOptionPrice, timeTaken );


displayEndOfDemoMessage(mfilename)
end

##### SOURCE END #####
--></body></html>