function [fig, count] = pctdemo_setup_bench(difficulty)
%PCTDEMO_SETUP_BENCH Perform the initialization for the Parallel Computing
%Toolbox Benchmarking examples.
%   [fig, count] = pctdemo_setup_bench(difficulty) returns the output figure 
%   for the Benchmarking example as well as the number of times the benchmark  
%   should be repeated.

%   Copyright 2007-2012 The MathWorks, Inc.
    
    fig = pDemoFigure();
    clf(fig);
    set(fig, 'Name', 'Benchmark');
    set(fig, 'Visible', 'off');
    
    if (abs(difficulty - 1.0) > eps)
        warning('pctexample:setupbench:CanNotSetDifficulty', ...
                'Cannot set difficulty level to %3.2f', difficulty);
    end
    count = 4;
end %End of pctdemo_setup_bench
