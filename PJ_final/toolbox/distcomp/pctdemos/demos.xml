<?xml version="1.0" encoding="utf-8"?>
<!--
Copyright 2007-2012 The MathWorks, Inc.
-->
<demos>
	<name>Parallel Computing</name>
	<type>toolbox</type>
	<icon>$toolbox/matlab/icons/matlabicon.gif</icon>
<description isCdata="no">
<p>Parallel Computing Toolbox&#8482; lets you solve computationally and data-intensive problems using multicore processors, GPUs, and computer clusters. High-level constructs&#8212;parallel for-loops, special array types, and parallelized numerical algorithms&#8212;let you parallelize MATLAB&#174; applications without CUDA or MPI programming. You can use the toolbox with Simulink&#174; to run multiple simulations of a model in parallel.  
</p><p>The toolbox provides twelve workers (MATLAB&#174; computational engines) to execute applications locally on a multicore desktop. Without changing the code, you can run the same application on a computer cluster or a grid computing service (using MATLAB&#174; Distributed Computing Server&#8482;). You can run parallel applications interactively or in batch.

</p>
</description>

	<demosection>
        <label>Parallel For Loops (PARFOR)</label>
        <demoitem>
            <label>Simple Benchmarking of PARFOR Using Blackjack</label>
            <type>M-file</type>
            <source>paralleldemo_parfor_bench</source>
        </demoitem>
    </demosection>
	
    <demosection>
        <label>Tutorials</label>
        <demoitem>
            <label>Customizing the Settings for the Examples in the Parallel Computing Toolbox</label>
            <type>M-file</type>
            <source>paralleltutorial_defaults</source>
        </demoitem>
        <demoitem>
            <label>Dividing MATLAB Computations into Tasks</label>
            <type>M-file</type>
            <source>paralleltutorial_dividing_tasks</source>
        </demoitem>
        <demoitem>
            <label>Writing Task Functions</label>
            <type>M-file</type>
            <source>paralleltutorial_taskfunctions</source>
        </demoitem>
        <demoitem>
            <label>Minimizing Network Traffic</label>
            <type>M-file</type>
            <source>paralleltutorial_network_traffic</source>
        </demoitem>
        <demoitem>
            <label>Using Callback Functions</label>
            <type>M-file</type>
            <source>paralleltutorial_callbacks</source>
        </demoitem>
        <demoitem>
            <label>Using GOP to Achieve MPI_Allreduce Functionality</label>
            <type>M-file</type>
            <source>paralleltutorial_gop</source>
        </demoitem>
        <demoitem>
            <label>Using the Parallel Profiler in Pmode</label>
            <type>M-file</type>
            <source>paralleltutorial_parprofile</source>
        </demoitem>
    </demosection>

    <demosection>
        <label>Benchmarks</label>
        <demoitem>
            <label>Benchmark the Client Machine</label>
            <type>M-file</type>
            <source>paralleldemo_bench_seq</source>
        </demoitem>
        <demoitem>
            <label>Distributed Benchmark</label>
            <type>M-file</type>
            <source>paralleldemo_bench_dist</source>
        </demoitem>
        <demoitem>
            <label>Simple Benchmarking of PARFOR Using Blackjack</label>
            <type>M-file</type>
            <source>paralleldemo_parfor_bench</source>
        </demoitem>
        <demoitem>
            <label>Resource Contention in Task Parallel Problems</label>
            <type>M-file</type>
            <source>paralleldemo_resource_bench</source>
        </demoitem>
        <demoitem>
            <label>Benchmarking Independent Jobs on the Cluster</label>
            <type>M-file</type>
            <source>paralleldemo_distribjob_bench</source>
        </demoitem>
        <demoitem>
            <label>Benchmarking A\b</label>
            <type>M-file</type>
            <source>paralleldemo_backslash_bench</source>
        </demoitem>
            <demoitem>
            <label>Benchmarking A\b on the GPU</label>
            <type>M-file</type>
            <source>paralleldemo_gpu_backslash</source>
        </demoitem>
    </demosection>

    <demosection>
      <label>GPU</label>
      <demoitem>
        <label>Using FFT on the GPU for Spectral Analysis</label>
        <type>M-file</type>
        <source>paralleldemo_gpu_fft</source>
      </demoitem>
      <demoitem>
        <label>Using FFT2 on the GPU to Simulate Diffraction Patterns</label>
        <type>M-file</type>
        <source>paralleldemo_gpu_fft2</source>
      </demoitem>
      <demoitem>
        <label>Running Element-wise MATLAB Functions on the GPU</label>
        <type>M-file</type>
        <source>paralleldemo_gpu_arrayfun</source>
      </demoitem>
      <demoitem>
        <label>Benchmarking A\b on the GPU</label>
        <type>M-file</type>
        <source>paralleldemo_gpu_backslash</source>
      </demoitem>
      <demoitem>
        <label>GPU Devices</label>
        <type>M-file</type>
        <source>paralleldemo_gpu_devices</source>
      </demoitem>
      <demoitem>
        <label>Calculating the Mandelbrot Set on a GPU</label>
        <type>M-file</type>
        <source>paralleldemo_gpu_mandelbrot</source>
      </demoitem>
      <demoitem>
        <label>Exotic Option Pricing on a GPU using a Monte-Carlo Method</label>
        <type>M-file</type>
        <source>paralleldemo_gpu_optionpricing</source>
      </demoitem>
    </demosection>

    <demosection>
        <label>Parallel Profiler Demos</label>
        <demoitem>
            <label>Profiling Parallel Work Distribution</label>
            <type>M-file</type>
            <source>paralleldemo_drange_prof</source>
        </demoitem>
        <demoitem>
            <label>Profiling Explicit Parallel Communication</label>
            <type>M-file</type>
            <source>paralleldemo_communic_prof</source>
        </demoitem>
        <demoitem>
            <label>Profiling Load Unbalanced Codistributed Arrays</label>
            <type>M-file</type>
            <source>paralleldemo_distarray_prof</source>
        </demoitem>

    </demosection>


    <demosection>
        <label>Blackjack</label>
        <demoitem>
            <label>Sequential Blackjack</label>
            <type>M-file</type>
            <source>paralleldemo_blackjack_seq</source>
        </demoitem>
        <demoitem>
            <label>Distributed Blackjack</label>
            <type>M-file</type>
            <source>paralleldemo_blackjack_dist</source>
        </demoitem>
    </demosection>

    <demosection>
        <label>Estimating Pi</label>
        <demoitem>
            <label>Numerical Estimation of Pi Using Message Passing</label>
            <type>M-file</type>
            <source>paralleldemo_quadpi_mpi</source>
        </demoitem>
    </demosection>

    <demosection>
        <label>Gene Sequence Alignment</label>
        <demoitem>
            <label>Sequential Analysis of the Origin of the Human Immunodeficiency Virus</label>
           <dependency>Bioinformatics</dependency>
           <dependency>Statistics</dependency>
            <type>M-file</type>
            <source>paralleldemo_hiv_seq</source>
        </demoitem>
        <demoitem>
            <label>Distributed Analysis of the Origin of the Human Immunodeficiency Virus</label>
           <dependency>Bioinformatics</dependency>
           <dependency>Statistics</dependency>
            <type>M-file</type>
            <source>paralleldemo_hiv_dist</source>
        </demoitem>
    </demosection>

    <demosection>
        <label>Marginal Value-at-Risk Assessment</label>
        <demoitem>
            <label>Sequential Marginal Value-at-Risk Simulation</label>
           <dependency>Statistics</dependency>
            <type>M-file</type>
            <source>paralleldemo_mvar_seq</source>
        </demoitem>
        <demoitem>
            <label>Distributed Marginal Value-at-Risk Simulation</label>
           <dependency>Statistics</dependency>
            <type>M-file</type>
            <source>paralleldemo_mvar_dist</source>
        </demoitem>
    </demosection>

    <demosection>
        <label>Portfolio Optimization</label>
        <demoitem>
            <label>Sequential Portfolio Optimization</label>
           <dependency>Optimization</dependency>
            <type>M-file</type>
            <source>paralleldemo_optim_seq</source>
        </demoitem>
        <demoitem>
            <label>Distributed Portfolio Optimization</label>
           <dependency>Optimization</dependency>
            <type>M-file</type>
            <source>paralleldemo_optim_dist</source>
        </demoitem>
    </demosection>

    <demosection>
        <label>Radar Tracking Simulations</label>
        <demoitem>
            <label>Sequential Radar Tracking Simulation</label>
           <dependency>Simulink</dependency>
            <type>M-file</type>
            <source>paralleldemo_radar_seq</source>
        </demoitem>
        <demoitem>
            <label>Distributed Radar Tracking Simulation</label>
           <dependency>Simulink</dependency>
            <type>M-file</type>
            <source>paralleldemo_radar_dist</source>
        </demoitem>
    </demosection>
</demos>
