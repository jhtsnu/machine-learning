function filename = pctdemo_helper_fullfile(networkDir, file)
%PCTDEMO_HELPER_FULLFILE Build full filename from parts.
%   PCTDEMO_HELPER_FULLFILE(networkDir, file) returns 
%   FULLFILE(networkDir.windows, file) on the PC Windows platform, and 
%   FULLFILE(networkDir.unix, file) on other platforms.
%
%   networkDir must be a structure with the field names 'windows' and
%   'unix', and the field values must be strings.
%   
%   See also FULLFILE
    
%   Copyright 2007-2012 The MathWorks, Inc.
    
    % Verify the input argument
    narginchk(2, 2);
    tc = pTypeChecker();
    if ~(tc.isStructWithFields(networkDir, 'unix', 'windows') ...
         && iscellstr(struct2cell(networkDir)))
        error('pctexample:helperfullfile:InvalidArgument', ...
              ['Network directory must be a structure with the field names '...
              'windows and unix and the field values must be strings']);
    end
    if ~ischar(file) || isempty(file)
        error('pctexample:helperfullfile:InvalidArgument', ...
              'File must be a non-empty character array');
    end
    
    if ispc
        base = networkDir.windows;
    else
        base = networkDir.unix;
    end
    filename = fullfile(base, file);
end % End of pctdemo_helper_fullfile
