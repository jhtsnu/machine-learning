function Tree=treefit(X,y,varargin)
%TREEFIT Obsolete function
%   
%   TREEFIT will be removed in a future release. Use CLASSREGTREE instead.
%
%   See also CLASSREGTREE.

%   Copyright 1993-2007 The MathWorks, Inc. 
%   $Revision: 1.1.8.3 $  $Date: 2012/09/12 11:05:29 $


Tree = classregtree(X,y,varargin{:});
