function [varargout] = subsasgn(varargin)
%SUBSASGN Subscripted reference for a PROBDIST object.
%   Subscript assignment is not allowed for a PROBDIST object.

%   Copyright 2010 The MathWorks, Inc.
%   $Revision: 1.1.6.4 $  $Date: 2012/09/12 11:07:08 $


error(message('stats:ProbDist:subsasgn:NotAllowed'))
