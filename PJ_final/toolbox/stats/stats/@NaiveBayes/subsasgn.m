function [varargout] = subsasgn(varargin)
%SUBSASGN Subscripted reference for a NaiveBayes object.
%   Subscript assignment is not allowed for a NaiveBayes object.

%   Copyright 2008 The MathWorks, Inc.
%   $Revision: 1.1.6.4 $  $Date: 2012/09/12 11:06:59 $


error(message('stats:NaiveBayes:subsasgn:NotAllowed'))
