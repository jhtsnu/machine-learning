function p = unifcdf(x,a,b)
%UNIFCDF Uniform (continuous) cumulative distribution function (cdf).
%   P = UNIFCDF(X,A,B) returns the cdf for the uniform distribution
%   on the interval [A,B] at the values in X.
%
%   The size of P is the common size of the input arguments. A scalar input
%   functions as a constant matrix of the same size as the other inputs.    
% 
%   By default, A = 0 and B = 1.
%
%   See also UNIFINV, UNIFIT, UNIFPDF, UNIFRND, UNIFSTAT, CDF.

%   Copyright 1993-2004 The MathWorks, Inc. 


if nargin < 1, 
    error(message('stats:unifcdf:TooFewInputs')); 
end

if nargin == 1
    a = 0;
    b = 1;
end

[errorcode,x,a,b] = distchck(3,x,a,b);

if errorcode > 0
    error(message('stats:unifcdf:InputSizeMismatch'));
end

% Initialize P to zero.
if isa(x,'single') || isa(a,'single') || isa(b,'single')
    p = zeros(size(x),'single');
else
    p = zeros(size(x));
end

% (1) x <= a and a < b.
p(x <= a & a < b) = 0;

% (2) x >= b and a < b.
p(x >= b & a < b) = 1;

% (3) a < x < b.
k = find(x > a & x < b & a < b);
if any(k)
    p(k) = (x(k) - a(k)) ./ (b(k) - a(k));
end

% (4) a >= b then set p to NaN.
p(a >= b) = NaN;

% (5) If x or a or b is NaN, set p to NaN.
p(isnan(x) | isnan(a) | isnan(b)) = NaN;

end