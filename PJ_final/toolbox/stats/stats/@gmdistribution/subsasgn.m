function [varargout] = subsasgn(varargin)
%SUBSASGN Subscripted reference for a gmdistribution object.
%   Subscript assignment is not allowed for a gmdistribution object.

%   Copyright 2007 The MathWorks, Inc.
%   $Revision: 1.1.6.4 $  $Date: 2012/09/12 11:08:32 $


error(message('stats:gmdistribution:subsasgn:NotAllowed'))
