addpath toolbox/optim/optim;
addpath toolbox/shared/optimlib;
addpath toolbox/stats/stats/_private;
addpath toolbox/stats/stats;

% load data
load_data;

% extract random patches to generate feature of training data
PatchedTrainImg = extract_random_patches(data_train, Const);
PatchedTrainImg = preprocessing(PatchedTrainImg, Const);
Centroid = kmeans(PatchedTrainImg, Const.NumOfCentroids);
clear PatchedTrainImg;
FeatureTrain = extract_features(data_train, Centroid, Const);
FeatureTest = extract_features(data_test, Centroid, Const);
clear data_train; clear Centroid;
% standardize data
disp 'standarize data'
tic;
FeatureMean = mean(FeatureTrain);
FeatureSVar = sqrt(var(FeatureTrain)+0.01);
FeatureTrain = bsxfun(@rdivide, bsxfun(@minus, FeatureTrain, FeatureMean), FeatureSVar);
FeatureMean = mean(FeatureTest);
FeatureSVar = sqrt(var(FeatureTest)+0.01);
FeatureTest = bsxfun(@rdivide, bsxfun(@minus, FeatureTest, FeatureMean), FeatureSVar);
toc;
clear FeatureMean; clear FeatureSVar;
% Train feature using Support Vector Machine
count = 1;
for i = Const.KindOfLabel
  Target = (labels_train == i) * 2 - 1;
  [SV AlphaHat bias, SVIndex] = SVM(FeatureTrain, Target, Const.SVM, Const.SVM.Method);
  TrainF(count,:) = Const.SVM.kernel(SV, FeatureTrain)' * AlphaHat(:) + bias;
  TestF(count,:) = Const.SVM.kernel(SV, FeatureTest)' * AlphaHat(:) + bias;
  count = count + 1;
end
[val,label] = max(TrainF, [], 1);
fprintf('Train accuracy %f%%\n', 100 * (1 - sum(Const.KindOfLabel(label)' ~= labels_train) / Const.NumOfTrainData));
[val,label] = max(TestF, [], 1);
fprintf('Test accuracy %f%%\n', 100 * (1 - sum(Const.KindOfLabel(label)' ~= labels_test) / Const.NumOfTestData));
