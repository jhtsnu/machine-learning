function [PatchedTrainImg] = extract_random_patches(InputImg, Const)
% extract random patch from unlabeled data

disp 'Generate Patch Base Index of Each Axis';
tic;
XPatchBaseIndex = mod(randperm(Const.NumOfPatches), Const.SizeOfCifar - Const.SizeOfPatch) + 1;
YPatchBaseIndex = mod(randperm(Const.NumOfPatches), Const.SizeOfCifar - Const.SizeOfPatch) + 1;

toc;

disp 'Generate Patch Index of Each Axis';
tic;

XPatchIndex = [];
YPatchIndex = [];
for i = [0 : Const.SizeOfPatch - 1]
  XPatchIndex = [XPatchIndex; XPatchBaseIndex + i];
  YPatchIndex = [YPatchIndex; YPatchBaseIndex + i];
end

clear XPatchBaseIndex;
clear YPatchBaseIndex;

toc;

disp 'Generate Patch Index';
tic;

PatchIndexOriginal = [];
for i = [1 : Const.SizeOfPatch]
  PatchIndexOriginal = [PatchIndexOriginal , (repmat(YPatchIndex(i,:), Const.SizeOfPatch, 1) * Const.SizeOfCifar + XPatchIndex)'];
end

clear XPatchIndex;
clear YPatchIndex;
toc;

disp 'Extract Patches from Training Image';
tic;

PatchOffset = repmat([1:Const.NumOfTrainData]', Const.PatchPerData, Const.SizeOfPatchSquare);
PatchIndex = [];
for i = [0:Const.NumOfChannel - 1]
  PatchIndex = [PatchIndex ,(PatchIndexOriginal - 1 + i * Const.SizeOfCifarSquare) * Const.NumOfTrainData + PatchOffset];
end

clear PatchIndexOriginal;
clear PatchOffset;

PatchedTrainImg = [];
for k = [0:Const.PatchPerData - 1]
  PatchedTrainImg = [PatchedTrainImg; InputImg(PatchIndex([k * Const.NumOfTrainData + 1:(k + 1) * Const.NumOfTrainData],:))];
end

clear PatchIndex;
clear UnpatchedTrainImg;

toc;
