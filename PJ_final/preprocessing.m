function [ PreprocessedImg, M, P ] = preprocessing(InputImg, Const)
  %PREPROCESSING Summary of this function goes here
  %   Detailed explanation goes here

  %disp 'Preprocessing...';
  %tic;
  if (Const.IsWhitening)
    %disp 'With Whitening';
  end

  PreprocessedImg = double(InputImg);
  MeanOfInputImg = mean(PreprocessedImg, 2);
  ContrastParam = sqrt(var(PreprocessedImg,[],2)+10);
  PreprocessedImg = bsxfun(@rdivide, bsxfun(@minus, PreprocessedImg, MeanOfInputImg), ContrastParam);
  % whitening
  if (Const.IsWhitening)
    [PreprocessedImg, M, P] = whitening(PreprocessedImg);
  end
  %toc;

function [OutputImg, M, P] = whitening(InputImg)
  C = cov(InputImg);
  M = mean(InputImg);
  [V,D] = eig(C);
  P = V * diag(sqrt(1./(diag(D) + 0.1))) * V';
  OutputImg = bsxfun(@minus, InputImg, M) * P;
