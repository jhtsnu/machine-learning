function [idx, mu, resp] = gmm_em (X, K, maxIter, e)
% ------------------------------------
% INPUT
% -- X: Training data (N by D)
% -- K: # of cluster
% -- maxIter: maximum number of iteration
% -- e: condition of convergence ==> logLikelihood�Ƣ� e% ��?��?��? ��?��?
% ------------------------------------
% OUTPUT
% -- idx: labels of data (N by 1)
% -- mu: means of gaussians (K by D)
% -- resp: responsibility (N by K)
% ------------------------------------
% OTHERS
% -- N: # of data
% -- D: dimension of data
% -- sigma: covariances of gaussians (D by D by K)
% -- pi: mixing coefficients of gaussians (1 by K)
% ------------------------------------

    [N, D] = size(X); 
    
    % Initialize
    mu = X(randperm(N, K), :);
    sigma = zeros(D, D, K);
    for i=1:K
        sigma(:, :, i) = eye(D);
    end
    
    for k = [1:K]
        repm_mu = repmat(mu(k,:), N, 1);
        diff = X - repm_mu;
        norms = sqrt(sum(abs(diff).^2,2));
        distances(k,:) = norms;
        sigma(:,:,k) = cov(diff);
    end
    [min_val, L] = min(distances, [], 1);
    for k = [1:K]
        if sum(L==k) > 0
            mu(k,:) = mean(X(L==k,:));
        end
    end
    
    pi = ones(1, K)/K;
    logLikelihood = -inf;
    
    for i=1:maxIter
        resp = expectation(X, mu, sigma, pi);   % e-step
        [mu, sigma, pi] = maximization(X, resp);    % m-step
        
        prev_logLikelihood = logLikelihood;
        %-----------------------------------------------
        % TODO: CALCULATE 'logLikelihood' (9.28)
        %       use log, sum function
        %-----------------------------------------------
        
        disp 'find logLikelihood'
        tic
        likelihood_param = zeros(N,K);
        for k = 1 : K
            likelihood_param(:,k) = pi(k) * mvnpdf(X(:,:), mu(k,:), sigma(:,:,k));
        end
        likelihood = sum(likelihood_param(:,:), 2);
        logLikelihood = sum(log(likelihood), 1);
        toc
        % check convergence condition
        disp 'Error is '
        error = abs(logLikelihood - prev_logLikelihood) - e*abs(logLikelihood)
        disp error
        if abs(logLikelihood - prev_logLikelihood) < e*abs(logLikelihood)
            break;
        end        
    end
    
    % labeling
    [~, idx] = max(resp, [], 2);
end




function resp = expectation(X, mu, sigma, pi)
% ------------------------------------
% INPUT
% -- X: Training data (N by D)
% -- mu: means of gaussian (K by D)
% -- sigma: D by D by K
% -- pi: 1 by K
% ------------------------------------
% OUTPUT
% -- resp: responsibility (N by K)
% ------------------------------------
% OTHERS
% -- N: # of data
% -- D: dimension of data
% -- K: # of cluster
% ------------------------------------

    [N, D] = size(X);
    K = size(mu, 1);
    
    resp = zeros(N, K);
    %-----------------------------------------------
    % TODO: CALCULATE 'resp' (9.23)
    %       use mvnpdf, bsxfun(@rdivide. A, B) or bsxfun(@times, A, B), isnan etc.
    %       ** check 'NaN', 'Inf' carefully **
    %-----------------------------------------------
    disp 'find resp'
    tic
    normalizer = zeros(N, K);
    for k = 1 : K
        resp(:,k) = pi(k) * mvnpdf(X(:,:), mu(k,:), sigma(:,:,k));
    end
    resp = resp ./ repmat(sum(resp, 2), 1, K);
    resp(isnan(resp)) = 1 / K;
    toc
end

function [mu, sigma, pi] = maximization(X, resp)
% ------------------------------------
% INPUT
% -- X: Training data (N by D)
% -- resp: responsibility (N by K)
% ------------------------------------
% OUTPUT
% -- mu: means of gaussian (K by D)
% -- sigma: D by D by K
% -- pi: 1 by K
% ------------------------------------
% OTHERS
% -- N: # of data
% -- D: dimension of data
% -- K: # of cluster
% -- Nk: sum of responsibility [sum_n=1~N resp(n, k)] (1 by K)
% ------------------------------------

    [N, D] = size(X);
    K = size(resp, 2);
    
    Nk = zeros(1, K);
    %-----------------------------------------------
    % TODO: CALCULATE 'Nk' (9.27)
    %       use sum
    %-----------------------------------------------
    Nk = sum(resp);
    
    mu = zeros(K, D);
    %-----------------------------------------------
    % TODO: CALCULATE 'mu' (9.24)
    %       use matrix operation, bsxfun(@rdivide A, B)
    %-----------------------------------------------
    disp 'find mu'
    tic
    for k = 1 : K
        mu(k,:) = resp(:,k)' * X(:,:) ./ Nk(k);
    end
    toc
    
    sigma = zeros(D, D, K);
    %-----------------------------------------------
    % TODO: CALCULATE 'sigma' (9.25)
    %       use matrix operation
    %-----------------------------------------------
    disp 'find sigma'
    tic
    for k = 1 : K
        for i = 1 : D
            for j = 1 : D
                sigma(i,j,k) = resp(:,k)' .* (X(:,i) - mu(k,i))' * (X(:,j) - mu(k,j)) / Nk(k);
            end
        end
    end
    toc

    pi = zeros(1, K);
    %-----------------------------------------------
    % TODO: CALCULATE 'pi' (9.26)
    %       just divide
    %-----------------------------------------------
    pi = Nk / size(X, 1);
end
