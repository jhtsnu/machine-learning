clear all;

image_mat = imread('practice5.jpg');

K = 3;

image_col_R = reshape(image_mat(:,:,1), 1, 1960 * 3008);
image_col_G = reshape(image_mat(:,:,2), 1, 1960 * 3008);
image_col_B = reshape(image_mat(:,:,3), 1, 1960 * 3008);

image_col = [image_col_R; image_col_G; image_col_B]';

[L, M] = gmm_em(double(image_col), K, 30, 0.001);

seg_col = M(L,:);

seg_image_mat_R = reshape(seg_col(:,1), 1960, 3008);
seg_image_mat_G = reshape(seg_col(:,2), 1960, 3008);
seg_image_mat_B = reshape(seg_col(:,3), 1960, 3008);

seg_image_mat = zeros(1960, 3008, 3);
seg_image_mat(:,:,1) = seg_image_mat_R;
seg_image_mat(:,:,2) = seg_image_mat_G;
seg_image_mat(:,:,3) = seg_image_mat_B;

imwrite(double(seg_image_mat) ./255 , 'seg_image.jpg');

