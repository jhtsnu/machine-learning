function [ rho ] = rbf(X, c, type)
% outputs radial basis functions results
% X : input data
% C : center vector
% type : type of rbf, default type is 'gaussian'
num_of_data = size(X, 1);
num_of_kernel = size(c, 1);

center_repmat = repmat(c, num_of_data, 1);

diff = X - center_repmat;
norms = sqrt(sum(abs(diff).^2,2)); % To get norm each rows;

if strcmp(type, 'gaussian') == 0
    variances = var(diff');
    rho = exp(-norms) ./ variances' * 0.5;
elseif strcmp(type, 'no-variance-gaussian') == 0
    rho = exp(-norms);
end

end