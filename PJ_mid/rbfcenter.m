function [ Y ] = rbfcenter(type, X, K, Y0)
%RBFCENTER Summary of this function goes here
% type Center를 얻는 방법
% X 데이터 벡터
% K 클러스터 갯수, type이 heuristic일 경우 input class data를 의미
% Y0 type이 kmeans일 경우 초기중심좌표
    if strcmp(type, 'random') == 1
        Y = randomlysampled(X, K);
    elseif strcmp(type, 'kmeans') == 1
        if nargin<4
            Y = kmeans(X, K);
        else
            Y = kmeans(X, K, Y0);
        end
    elseif strcmp(type, 'heuristic') == 1
        for j = [1:10]
          Y(j,:) = mean(X(K==mod(j,10),:));
        end
    end
end

