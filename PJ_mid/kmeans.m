function [Y,L,MSE] = kmeans(X,K,Y0)
%KMEANS K-means clustering
% X 데이터 벡터
% K 클러스터 갯수
% Y0 초기 중심값(옵션)
%
%출력 인자:
% Y 출력 열 벡터(K 개의 열)
% L 각 데이터 벡터의 색인된 라벨링 값
% MSE mean square error
num_of_data = size(X,1);
if nargin<3
    R = randperm(num_of_data);
    randidx = R(1:K);
    Y = X(randidx,:);
else
    Y = Y0;
end

MSE = 1e+250;
for loop = [1:1000]
    for k = [1:K]
        Y_repmat = repmat(Y(k,:), num_of_data, 1);
        diff = X - Y_repmat;
        norms = sqrt(sum(abs(diff).^2,2));
        distances(k,:) = norms;
    end
    [min_val, L] = min(distances, [], 1);
    for k = [1:K]
        if sum(L==k) > 0
            Y(k,:) = mean(X(L==k,:));
        end
    end
    
    prevMSE = MSE;
    MSE = mean(distances(1,:).^2);
    if (prevMSE - MSE) / prevMSE < 0.0001
        break;
    end
end
