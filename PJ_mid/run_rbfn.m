clear

% Set the number of training/test data
num_of_train = 10000;
num_of_test = 2000;
% Load data from MNIST dataset
load('MNIST_Dataset.mat');
train_x = trainingData.Images(:,1:num_of_train)'; 
train_c = trainingData.Labels(1:num_of_train,:); 
test_x = testingData.Images(:,1:num_of_test)'; 
test_c = testingData.Labels(1:num_of_test,:); 

% set the number of neurons in the hidden layer (kernel function)
num_of_kernel = 50;
num_of_output = 10;

% Get center vertors from k-means algorithm
% centers = rbfcenter('heuristic', train_x, train_c, train_x(1:num_of_kernel,:));
% centers = rbfcenter('random', train_x, num_of_kernel);
centers = rbfcenter('kmeans', train_x, num_of_kernel);

% Get radial basis functions
for j = [1:num_of_kernel]
    train_rbf(:, j) = rbf(train_x, centers(j,:), 'gaussian');
    test_rbf(:, j) = rbf(test_x, centers(j,:), 'gaussian');
end
train_rbf(:, num_of_kernel + 1) = 1;
test_rbf(:, num_of_kernel + 1) = 1;

% 
% Define y as real scala
% 
% weight = pinv(train_rbf' * train_rbf) * train_rbf' * train_c;
% y_real = test_rbf * weight;
% y = round(y_real);
% y(round(y) >= 10) = 9;
% y(round(y) < 0) = 0;
% accuracy = sum(test_c == (y)) / size(test_c,1) * 100

% 
% Define y as 10-binary value
for j = [1:num_of_output]
    binary_target(:,j) = train_c== (j - 1);
end
weight = pinv(train_rbf' * train_rbf) * train_rbf' * binary_target;
y = test_rbf * weight; % (weight' * test_rbf')';
[max_val predicts] = max(y');
accuracy = sum(test_c' == (predicts - 1)) / size(test_c,1)
